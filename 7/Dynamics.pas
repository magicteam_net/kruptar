unit Dynamics;

interface

Type
 PDynamic = ^TDynamic;
 TDynamic = Record
  Data: Pointer;
  Next: PDynamic;
 end;
 PDynamics = ^TDynamics;
 TDynamics = Object
  DataSize: Integer;
  Root, Cur: PDynamic;
  Count: Integer;
  Constructor Init(DS: Integer);
  Destructor Done;
  Function Add: PDynamic;
  Procedure MoveUp(Item: Pointer);
 end;

implementation

Constructor TDynamics.Init(DS: Integer);
begin
 DataSize := DS;
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

Destructor TDynamics.Done;
Var N: PDynamic;
begin
 While Root <> NIL do
 begin
  N := Root^.Next;
  With Root^ do If Data <> NIL then FreeMem(Data);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
 DataSize := 0;
end;

Function TDynamics.Add: PDynamic;
begin
 New(Result);
 If Root = NIL then Root := Result Else Cur^.Next := Result;
 Cur := Result;
 Cur^.Data := NIL;
 Inc(Count);
 Result^.Next := NIL;
end;

procedure TDynamics.MoveUp(Item: Pointer);
Var P, PR, PRR, N: PDynamic;
begin
 If Item = NIL then Exit;
 P := Root; PR := NIL; PRR := NIL;
 While P <> NIL do
 begin
  If P^.Data = Item then Break;
  PRR := PR;
  PR := P;
  P := P^.Next;
 end;
 If P = NIL then Exit;
 If PR = NIL then Exit;
 With P^ do
 begin
  N := P^.Next;
  P^.Next := PR;
  PR^.Next := N;
  If PR = Root then Root := P Else PRR^.Next := P;
  If Cur = P then Cur := PR;
 end;
end;

end.
