object InsertResultDialog: TInsertResultDialog
  Left = 837
  Top = 365
  BorderStyle = bsDialog
  Caption = 'Not all text is inserted!'
  ClientHeight = 211
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  LocCaption = 'InsertResultDialogTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 297
    Height = 161
    Shape = bsFrame
  end
  object OKBtn: TLocButton
    Left = 119
    Top = 180
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object Memo: TKruptarMemo
    Left = 16
    Top = 16
    Width = 281
    Height = 145
    ReadOnly = True
    TabOrder = 1
  end
end
