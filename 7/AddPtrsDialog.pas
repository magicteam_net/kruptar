unit AddPtrsDialog;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Buttons, Dialogs,
  LocClasses, KruptarComponents, FormatW;

type
  TAddPointersDialog = class(TKruptarForm)
    OkButton: TLocButton;
    CancelButton: TLocButton;
    Bevel: TBevel;
    BlockStartEdit: TEdit;
    BlockEndEdit: TEdit;
    BlockStartLabel: TLocLabel;
    BlockEndLabel: TLocLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OkButtonClick(Sender: TObject);
   public
    BlockStart: Integer;
    BlockEnd: Integer;
    Function Execute(const Title: WideString): Boolean;
  end;

var
 AddPointersDialog: TAddPointersDialog;

implementation

Uses MainUnit, k7Hexunit, StringConsts, LocMsgBox;

{$R *.DFM}

Var
 OkPressed: Boolean;

Function TAddPointersDialog.Execute(const Title: WideString): Boolean;
begin
 Caption := Title;
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TAddPointersDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 BlockStartEdit.SetFocus;
end;

procedure TAddPointersDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
Var F1, F2: String;
begin
 CanClose := False;
 If OkPressed then
 begin
  OkPressed := False;
  F1 := BlockStartEdit.Text;
  F2 := BlockEndEdit.Text;
  If F2 = '' then
  begin
   If HCheckOut(F1) then BlockStart := GetLDW(F1) Else
   begin
    LocMessageDlg(WideFormat(K7S(SMUInvalidIntegerValue), [F1]), mtError,
    [mbOk], 0);
    Exit;
   end;
   If BlockStart < 0 then
    LocMessageDlg(K7S(SAPFirstAddressLessThenZero), mtError, [mbOk], 0) Else
   begin
    BlockEnd := BlockStart;
    CanClose := True;
   end;
  end Else
  begin
   If HCheckOut(F1) then BlockStart := GetLDW(F1) Else
   begin
    LocMessageDlg(WideFormat(K7S(SMUInvalidIntegerValue), [F1]), mtError,
    [mbOk], 0);
    Exit;
   end;
   If HCheckOut(F2) then BlockEnd := GetLDW(F2) Else
   begin
    LocMessageDlg(WideFormat(K7S(SMUInvalidIntegerValue), [F2]), mtError,
    [mbOk], 0);
    Exit;
   end;
   If BlockStart < 0 then
   begin
    LocMessageDlg(K7S(SAPFirstAddressLessThenZero), mtError, [mbOk], 0);
    Exit;
   end;
   If BlockEnd < 0 then
   begin
    LocMessageDlg(K7S(SAPSecondAddressLessThenZero), mtError, [mbOk], 0);
    Exit;
   end;
   If BlockEnd < BlockStart then
   begin
    LocMessageDlg(K7S(SAPSecondAddressLessThenFirst), mtError, [mbOk], 0);
    Exit;
   end;
   CanClose := True;
  end;
 end Else CanClose := True;
end;

procedure TAddPointersDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

end.
