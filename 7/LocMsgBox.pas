unit LocMsgBox;

interface

uses
 Windows, SysUtils, Classes, Controls, Forms, Graphics, Dialogs, Math,
 KruptarComponents, LocClasses;

{$IFNDEF RAD_DELPHI_XE}
type
  TMsgDlgBtn = (mbYes, mbNo, mbOK, mbCancel, mbAbort, mbRetry, mbIgnore,
    mbAll, mbNoToAll, mbYesToAll, mbHelp, mbClose);
  TMsgDlgButtons = set of TMsgDlgBtn;

const
  mrClose = Windows.IDCLOSE;

resourcestring
  SMsgDlgClose = '&Close';
{$ENDIF}

function LocCreateMessageDialog(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): TKruptarForm;

function LocMessageDlg(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
function LocMessageDlgPos(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint; X, Y: Integer): Integer;
function LocMessageDlgPosHelp(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint; X, Y: Integer;
  const HelpFileName: WideString): Integer;

procedure LocShowMessage(const Msg: WideString);
procedure LocShowMessageFmt(const Msg: WideString; Params: array of const);
procedure LocShowMessagePos(const Msg: WideString; X, Y: Integer);

var
 MsgDlgLocalizer: TCustomLocalizer = NIL;

implementation

uses Consts
{$IFDEF FORCED_UNICODE}
, ClipBrd
{$ELSE}
, TntClipBrd
, TntSysUtils
, TntForms
{$ENDIF};

{ Message dialog }

function GetAveCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of WideChar;
begin
  for I := 0 to 25 do Buffer[I] := WideChar(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := WideChar(I + Ord('a'));
  GetTextExtentPointW(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

type
  TLocMessageForm = class(TKruptarForm)
  private
    Message: TKruptarLabel;
    procedure HelpButtonClick(Sender: TObject);
  protected
    procedure CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    function GetFormText: WideString;
  public
    constructor CreateNew(AOwner: TComponent); reintroduce;
  end;

constructor TLocMessageForm.CreateNew(AOwner: TComponent);
var
  NonClientMetrics: TNonClientMetrics;
begin
  inherited CreateNew(AOwner);
  NonClientMetrics.cbSize := sizeof(NonClientMetrics);
  if SystemParametersInfo(SPI_GETNONCLIENTMETRICS, 0, @NonClientMetrics, 0) then
    Font.Handle := CreateFontIndirect(NonClientMetrics.lfMessageFont);
end;

procedure TLocMessageForm.HelpButtonClick(Sender: TObject);
begin
  Application.HelpContext(HelpContext);
end;

procedure TLocMessageForm.CustomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = Word('C')) then
  begin
    Beep;
{$IFDEF FORCED_UNICODE}
    Clipboard.AsText := Self.GetFormText;
{$ELSE}
    TntClipboard.AsWideText := GetFormText;
{$ENDIF}
  end;
end;

function TLocMessageForm.GetFormText: WideString;
var
  DividerLine, ButtonCaptions: WideString;
  I: integer;
begin
  DividerLine := StringOfChar('-', 27) + sLineBreak;
  for I := 0 to ComponentCount - 1 do
    if Components[I] is TKruptarButton then
      ButtonCaptions := ButtonCaptions + TKruptarButton(Components[I]).Caption +
        StringOfChar(' ', 3);
  ButtonCaptions :=
{$IFDEF FORCED_UNICODE}
  StringReplace
{$ELSE}
  Tnt_WideStringReplace
{$ENDIF}
  (ButtonCaptions,'&','', [rfReplaceAll]);
  Result := DividerLine + Caption + sLineBreak + DividerLine + Message.Caption + sLineBreak
          + DividerLine + ButtonCaptions + sLineBreak + DividerLine;
end;

const
 SGMCError: PChar = 'Unexpected MsgType in GetMessageCaption.';
 SGBCError: PChar = 'Unexpected MsgDlgBtn in GetButtonCaption.';

type
  EInternalError = class(Exception);

function GetMessageCaption(MsgType: TMsgDlgType): WideString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 case MsgType of
  mtWarning:      Result := SMsgDlgWarning;
  mtError:        Result := SMsgDlgError;
  mtInformation:  Result := SMsgDlgInformation;
  mtConfirmation: Result := SMsgDlgConfirm;
  mtCustom:       Result := '';
  else raise EInternalError.Create(SGMCError);
 end;
 if Assigned(MsgDlgLocalizer) then
 begin
  Lang := MsgDlgLocalizer.Language;
  Item := NIL;
  if Lang <> NIL then
  case MsgType of
   mtWarning:      Item := Lang.ValueItems['MsgDlgWarning'];
   mtError:        Item := Lang.ValueItems['MsgDlgError'];
   mtInformation:  Item := Lang.ValueItems['MsgDlgInformation'];
   mtConfirmation: Item := Lang.ValueItems['MsgDlgConfirm'];
  end;
  if Item <> NIL then Result := Item.Value;
 end;
end;

function GetButtonCaption(MsgDlgBtn: TMsgDlgBtn): WideString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 case MsgDlgBtn of
  mbYes:      Result := SMsgDlgYes;
  mbNo:       Result := SMsgDlgNo;
  mbOK:       Result := SMsgDlgOK;
  mbCancel:   Result := SMsgDlgCancel;
  mbAbort:    Result := SMsgDlgAbort;
  mbRetry:    Result := SMsgDlgRetry;
  mbIgnore:   Result := SMsgDlgIgnore;
  mbAll:      Result := SMsgDlgAll;
  mbNoToAll:  Result := SMsgDlgNoToAll;
  mbYesToAll: Result := SMsgDlgYesToAll;
  mbHelp:     Result := SMsgDlgHelp;
  mbClose:    Result := SMsgDlgClose;
  else raise EInternalError.Create(SGBCError);
 end;
 if Assigned(MsgDlgLocalizer) then
 begin
  Lang := MsgDlgLocalizer.Language;
  Item := NIL;
  if Lang <> NIL then
  case MsgDlgBtn of
   mbYes:      Item := Lang.ValueItems['ButtonYes'];
   mbNo:       Item := Lang.ValueItems['ButtonNo'];
   mbOK:       Item := Lang.ValueItems['ButtonOK'];
   mbCancel:   Item := Lang.ValueItems['ButtonCancel'];
   mbAbort:    Item := Lang.ValueItems['ButtonAbort'];
   mbRetry:    Item := Lang.ValueItems['ButtonRetry'];
   mbIgnore:   Item := Lang.ValueItems['ButtonIgnore'];
   mbAll:      Item := Lang.ValueItems['ButtonAll'];
   mbNoToAll:  Item := Lang.ValueItems['ButtonNoToAll'];
   mbYesToAll: Item := Lang.ValueItems['ButtonYesToAll'];
   mbHelp:     Item := Lang.ValueItems['ButtonHelp'];
   mbClose:    Item := Lang.ValueItems['ButtonClose'];
  end;
  if Item <> NIL then Result := Item.Value;
 end;
end;

var
  IconIDs: array[TMsgDlgType] of PChar = (IDI_EXCLAMATION, IDI_HAND,
    IDI_ASTERISK, IDI_QUESTION, nil);

  ButtonNames: array[TMsgDlgBtn] of WideString = (
    'Yes', 'No', 'OK', 'Cancel', 'Abort', 'Retry', 'Ignore', 'All', 'NoToAll',
    'YesToAll', 'Help', 'Close');
  ModalResults: array[TMsgDlgBtn] of Integer = (
    mrYes, mrNo, mrOk, mrCancel, mrAbort, mrRetry, mrIgnore, mrAll, mrNoToAll,
    mrYesToAll, 0, mrClose);

function LocCreateMessageDialog(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons): TKruptarForm;
const
  mcHorzMargin = 8;
  mcVertMargin = 8;
  mcHorzSpacing = 10;
  mcVertSpacing = 10;
  mcButtonWidth = 50;
  mcButtonHeight = 14;
  mcButtonSpacing = 4;
var
  DialogUnits: TPoint;
  HorzMargin, VertMargin, HorzSpacing, VertSpacing, ButtonWidth,
  ButtonHeight, ButtonSpacing, ButtonCount, ButtonGroupWidth,
  IconTextWidth, IconTextHeight, X, ALeft: Integer;
  B, DefaultButton, CancelButton: TMsgDlgBtn;
  IconID: PChar;
  ATextRect: TRect;
  ThisButtonWidth: integer;
begin
  Result := TLocMessageForm.CreateNew(Application);
  with Result do
  begin
    BiDiMode := Application.BiDiMode;
    BorderStyle := bsDialog;
    Canvas.Font := Font;
    KeyPreview := True;
    Position := poDesigned;
    OnKeyDown := TLocMessageForm(Result).CustomKeyDown;
    DialogUnits := GetAveCharSize(Canvas);
    HorzMargin := MulDiv(mcHorzMargin, DialogUnits.X, 4);
    VertMargin := MulDiv(mcVertMargin, DialogUnits.Y, 8);
    HorzSpacing := MulDiv(mcHorzSpacing, DialogUnits.X, 4);
    VertSpacing := MulDiv(mcVertSpacing, DialogUnits.Y, 8);
    ButtonWidth := MulDiv(mcButtonWidth, DialogUnits.X, 4);
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
    begin
      if B in Buttons then
      begin
        ATextRect := Rect(0,0,0,0);
        DrawTextW(Canvas.Handle,
          PWideChar(GetButtonCaption(B)), -1,
          ATextRect, DT_CALCRECT or DT_LEFT or DT_SINGLELINE or
          DrawTextBiDiModeFlagsReadingOnly);
        with ATextRect do ThisButtonWidth := Right - Left + 8;
        if ThisButtonWidth > ButtonWidth then
          ButtonWidth := ThisButtonWidth;
      end;
    end;
    ButtonHeight := MulDiv(mcButtonHeight, DialogUnits.Y, 8);
    ButtonSpacing := MulDiv(mcButtonSpacing, DialogUnits.X, 4);
    SetRect(ATextRect, 0, 0, Screen.Width div 2, 0);
    DrawTextW(Canvas.Handle, PWideChar(Msg), Length(Msg) + 1, ATextRect,
      DT_EXPANDTABS or DT_CALCRECT or DT_WORDBREAK or
      DrawTextBiDiModeFlagsReadingOnly);
    IconID := IconIDs[DlgType];
    IconTextWidth := ATextRect.Right;
    IconTextHeight := ATextRect.Bottom;
    if IconID <> nil then
    begin
      Inc(IconTextWidth, 32 + HorzSpacing);
      if IconTextHeight < 32 then IconTextHeight := 32;
    end;
    ButtonCount := 0;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then Inc(ButtonCount);
    ButtonGroupWidth := 0;
    if ButtonCount <> 0 then
      ButtonGroupWidth := ButtonWidth * ButtonCount +
        ButtonSpacing * (ButtonCount - 1);
    ClientWidth := Max(IconTextWidth, ButtonGroupWidth) + HorzMargin * 2;
    ClientHeight := IconTextHeight + ButtonHeight + VertSpacing +
      VertMargin * 2;
    Left := (Screen.Width div 2) - (Width div 2);
    Top := (Screen.Height div 2) - (Height div 2);
    if DlgType <> mtCustom then
      Caption := GetMessageCaption(DlgType)
    else
{$IFDEF FORCED_UNICODE}
      Caption := Application.Title;
{$ELSE}
      Caption := TntApplication.Title;
{$ENDIF}
    if IconID <> nil then
      with TKruptarImage.Create(Result) do
      begin
        Name := 'Image';
        Parent := Result;
        Picture.Icon.Handle := LoadIcon(0, IconID);
        SetBounds(HorzMargin, VertMargin, 32, 32);
      end;
    TLocMessageForm(Result).Message := TKruptarLabel.Create(Result);
    with TLocMessageForm(Result).Message do
    begin
      Name := 'Message';
      Parent := Result;
      WordWrap := True;
      Caption := Msg;
      BoundsRect := ATextRect;
      BiDiMode := Result.BiDiMode;
      ALeft := IconTextWidth - ATextRect.Right + HorzMargin;
      if UseRightToLeftAlignment then
        ALeft := Result.ClientWidth - ALeft - Width;
      SetBounds(ALeft, VertMargin,
        ATextRect.Right, ATextRect.Bottom);
    end;
    if mbOk in Buttons then DefaultButton := mbOk else
      if mbClose in Buttons then DefaultButton := mbClose else
        if mbYes in Buttons then DefaultButton := mbYes else
          DefaultButton := mbRetry;

    if mbCancel in Buttons then CancelButton := mbCancel else
      if mbClose in Buttons then CancelButton := mbClose else
        if mbNo in Buttons then CancelButton := mbNo else
          CancelButton := mbOk;
    X := (ClientWidth - ButtonGroupWidth) div 2;
    for B := Low(TMsgDlgBtn) to High(TMsgDlgBtn) do
      if B in Buttons then
        with TKruptarButton.Create(Result) do
        begin
          Name := ButtonNames[B];
          Parent := Result;
          Caption := GetButtonCaption(B);
          ModalResult := ModalResults[B];
          if B = DefaultButton then Default := True;
          if B = CancelButton then Cancel := True;
          SetBounds(X, IconTextHeight + VertMargin + VertSpacing,
            ButtonWidth, ButtonHeight);
          Inc(X, ButtonWidth + ButtonSpacing);
          if B = mbHelp then
            OnClick := TLocMessageForm(Result).HelpButtonClick;
        end;
  end;
end;

function LocMessageDlg(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
begin
  Result := LocMessageDlgPosHelp(Msg, DlgType, Buttons, HelpCtx, -1, -1, '');
end;

function LocMessageDlgPos(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint; X, Y: Integer): Integer;
begin
  Result := LocMessageDlgPosHelp(Msg, DlgType, Buttons, HelpCtx, X, Y, '');
end;

function LocMessageDlgPosHelp(const Msg: WideString; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint; X, Y: Integer;
  const HelpFileName: WideString): Integer;
begin
  with LocCreateMessageDialog(Msg, DlgType, Buttons) do
  try
    HelpContext := HelpCtx;
    HelpFile := HelpFileName;
    if X >= 0 then Left := X;
    if Y >= 0 then Top := Y;
    if (Y < 0) and (X < 0) then Position := poScreenCenter;
    Result := ShowModal;
  finally
    Free;
  end;
end;

procedure LocShowMessage(const Msg: WideString);
begin
  LocShowMessagePos(Msg, -1, -1);
end;

procedure LocShowMessageFmt(const Msg: WideString; Params: array of const);
begin
  LocShowMessage(WideFormat(Msg, Params));
end;

procedure LocShowMessagePos(const Msg: WideString; X, Y: Integer);
begin
  LocMessageDlgPos(Msg, mtCustom, [mbOK], 0, X, Y);
end;


end.
