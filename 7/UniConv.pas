unit UniConv deprecated;

interface

function ShiftJIS(u: WideChar): AnsiString;
function SJIStext(const u: WideString): AnsiString;
function Unicode(const sj: AnsiString): WideChar;
function Unitext(const s: AnsiString): WideString;

implementation

uses Windows, sysutils;

function ShiftJIS(u: WideChar): AnsiString;
begin
 SetLength(Result, 2);
 SetLength(Result, WideCharToMultiByte(932, 0, @u, 1,
                   Addr(Result[1]), 2, NIL, NIL));
end;

function SJIStext(const u: WideString): AnsiString;
begin
 If U <> '' then
 begin
  SetLength(Result, Length(U) shl 1);
  SetLength(Result, WideCharToMultiByte(932, 0, Pointer(U), Length(U),
                    Pointer(Result), Length(Result), NIL, NIL));
 end Else Result := '';
end;

function Unicode(const sj: AnsiString): WideChar;
begin
 If Length(SJ) = 2 then
  MultiByteToWideChar(932, 0, Pointer(SJ), 2, @Result, 1) Else
  Word(Result) := Byte(SJ[1]);
end;

function Unitext(const S: AnsiString): WideString;
begin
 If S <> '' then
 begin
  SetLength(Result, Length(S));
  SetLength(Result, MultiByteToWideChar(932, 0, Pointer(S),
                    Length(S), Pointer(Result), Length(Result)));
 end Else Result := S;
end;

end.
