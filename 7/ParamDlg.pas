unit ParamDlg;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Grids, ValEdit, Dynamics,
  ComCtrls, Spin, ActnList, Buttons, Dialogs,
  LocActnList, LocClasses, KruptarComponents
  {$IFDEF FORCED_UNICODE}
  , System.Actions
  {$ELSE}
  , TntStdCtrls, TntButtons, TntComCtrls
  {$ENDIF};

type
  TParamDialog = class(TLocForm)
    OkButton: TLocButton;
    CancelButton: TLocButton;
    AfterEdit: TSpinEdit;
    VariablesDialogAfterPointerLabel: TLocLabel;
    VariablesDialogBeforePointerLabel: TLocLabel;
    BeforeEdit: TSpinEdit;
    AfterComboBox: TComboBox;
    AfterCheckBox: TLocCheckBox;
    PageControl: TKruptarPageControl;
    AfterTab: TLocTabSheet;
    BeforeTab: TLocTabSheet;
    BeforeCheckBox: TLocCheckBox;
    BeforeComboBox: TComboBox;
    BeforeTreeView: TTreeView;
    AfterTreeView: TTreeView;
    AddButton: TKruptarSpeedButton;
    ActionList: TLocActionList;
    AddItem: TLocAction;
    MoveUpItem: TLocAction;
    MoveDownItem: TLocAction;
    DeleteItem: TLocAction;
    MoveUpButton: TKruptarSpeedButton;
    MoveDownButton: TKruptarSpeedButton;
    DeleteButton: TKruptarSpeedButton;
    DeleteBeforeButton: TKruptarSpeedButton;
    MoveDownBeforeButton: TKruptarSpeedButton;
    MoveUpBeforeButton: TKruptarSpeedButton;
    AddBeforeButton: TKruptarSpeedButton;
    AfterFixLabel: TLocLabel;
    BeforeFixLabel: TLocLabel;
    Timer: TTimer;
    ApplyButton: TLocButton;
    AStringSizeCheck: TLocCheckBox;
    BStringSizeCheck: TLocCheckBox;
    TotalIntervalL: TKruptarLabel;
    procedure OkButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AfterTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure BeforeTreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure AfterEditChange(Sender: TObject);
    procedure BeforeEditChange(Sender: TObject);
    procedure AddItemExecute(Sender: TObject);
    procedure MoveUpItemExecute(Sender: TObject);
    procedure MoveDownItemExecute(Sender: TObject);
    procedure DeleteItemExecute(Sender: TObject);
    procedure AfterTabShow(Sender: TObject);
    procedure BeforeTabShow(Sender: TObject);
    procedure AfterComboBoxSelect(Sender: TObject);
    procedure AfterTreeViewClick(Sender: TObject);
    procedure BeforeTreeViewClick(Sender: TObject);
    procedure AfterCheckBoxClick(Sender: TObject);
    procedure BeforeComboBoxSelect(Sender: TObject);
    procedure BeforeCheckBoxClick(Sender: TObject);
    procedure AfterTreeViewEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure BeforeTreeViewEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure AfterTreeViewEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure BeforeTreeViewEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerTimer(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BStringSizeCheckClick(Sender: TObject);
    procedure AStringSizeCheckClick(Sender: TObject);
    public
    function Execute(Data: Pointer; Int: Integer): Boolean;
  end;

var
  ParamDialog: TParamDialog;

implementation

{$R *.DFM}

Uses MainUnit, k7Hexunit, StringConsts, LocMsgBox;

Var
 OkPressed: Boolean;
 Params: TParameters;
 Parameters: PParameters;
 Interval: Integer;
 CanChange: Boolean;
 AfterFix: Integer;
 BeforeFix: Integer;
 DataChanged: Boolean;
 DataAccepted: Boolean;

procedure TParamDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
 If not DataAccepted then ApplyButtonClick(Sender);
end;

procedure TParamDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 PageControl.ActivePageIndex := 0;
 AfterTreeView.SetFocus;
 Timer.Enabled := True;
end;

function TParamDialog.Execute(Data: Pointer; Int: Integer): Boolean;
Var P: PDynamic;
begin
 Result := False;
 Interval := Int;
 If Data = NIL then Exit;
 if Interval <= 0 then
 begin
  VariablesDialogAfterPointerLabel.Enabled := False;
  VariablesDialogBeforePointerLabel.Enabled := False;
  AfterEdit.Enabled := False;
  BeforeEdit.Enabled := False;
 end else
 begin
  VariablesDialogAfterPointerLabel.Enabled := True;
  VariablesDialogBeforePointerLabel.Enabled := True;
  AfterEdit.Enabled := True;
  BeforeEdit.Enabled := True;
 end;
 TotalIntervalL.Caption := WideFormat(K7S(SPDTotalInterval), [Interval]);
 CanChange := False;
 Params.Clear;
 Parameters := Data;
 With Parameters^ do
 begin
  Params.Before := Before;
  Params.AfterFix := AfterFix;
  Params.BeforeFix := BeforeFix;
  P := Root;
  While P <> NIL do With P^ do
  begin
   With Params.Add^ do
   begin
    paName := PParameter(Data)^.paName;
    paBefore := PParameter(Data)^.paBefore;
    paParamType := PParameter(Data)^.paParamType;
    paFlags := PParameter(Data)^.paFlags;
   end;
   P := Next;
  end;
 end;
 AfterFix := Params.AfterFix;
 BeforeFix := Params.BeforeFix;
 With Params do
 begin
  AfterComboBox.ItemIndex := 0;
  BeforeComboBox.ItemIndex := 0;
  AfterCheckBox.Checked := False;
  BeforeCheckBox.Checked := False;
  With AfterEdit do
  begin
   Value := Interval - Before;
   MinValue := 0;
   MaxValue := Interval;
  end;
  With BeforeEdit do
  begin
   Value := Before;
   MinValue := 0;
   MaxValue := Interval;
  end;
  AfterTreeView.Items.Clear;
  BeforeTreeView.Items.Clear;
  P := Root;
  While P <> NIL do With P^, PParameter(Data)^ do
  begin
   If paBefore then
   begin
    BeforeTreeView.Items.AddNode(NIL, NIL, paName, Data, naAdd);
  //  Inc(BeforeFix, CParamSize[paParamType]);
   end Else
   begin
    AfterTreeView.Items.AddNode(NIL, NIL, paName, Data, naAdd);
 //   Inc(AfterFix, CParamSize[paParamType]);
   end;
   P := Next;
  end;
  AfterFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [AfterFix]);
  BeforeFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [BeforeFix]);
  With AfterTreeView do
  begin
   Selected := Items.GetFirstNode;
   AfterTreeViewChange(AfterTreeView, Selected);
  end;
  With BeforeTreeView do
  begin
   Selected := Items.GetFirstNode;
   BeforeTreeViewChange(BeforeTreeView, Selected);
  end;
  CanChange := True;
 end;
 DataChanged := False;
 DataAccepted := True; 
 ShowModal;
 Result := DataChanged;
end;

procedure TParamDialog.AfterTreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
 If (Node <> NIL) and (Node.Data <> NIL) then With PParameter(Node.Data)^ do
 begin
  AfterComboBox.Enabled := True;
  AfterCheckBox.Enabled := True;
  AStringSizeCheck.Enabled := True;
  AfterComboBox.ItemIndex := Integer(paParamType);
  AfterCheckBox.Checked := paFlags and PAR_BIGENDIAN = PAR_BIGENDIAN;
  AStringSizeCheck.Checked := paFlags and PAR_AUTOSIZE = PAR_AUTOSIZE;
 end Else
 begin
  AfterComboBox.Enabled := False;
  AfterCheckBox.Enabled := False;
  AStringSizeCheck.Enabled := False;
  AStringSizeCheck.Checked := False;
 end;
end;

procedure TParamDialog.BeforeTreeViewChange(Sender: TObject;
  Node: TTreeNode);
begin
 If (Node <> NIL) and (Node.Data <> NIL) then With PParameter(Node.Data)^ do
 begin
  BeforeComboBox.Enabled := True;
  BeforeCheckBox.Enabled := True;
  BStringSizeCheck.Enabled := True;
  BeforeComboBox.ItemIndex := Integer(paParamType);
  BeforeCheckBox.Checked := paFlags and PAR_BIGENDIAN = PAR_BIGENDIAN;
  BStringSizeCheck.Checked := paFlags and PAR_AUTOSIZE = PAR_AUTOSIZE;
 end Else
 begin
  BeforeComboBox.Enabled := False;
  BeforeCheckBox.Enabled := False;
  BeforeComboBox.ItemIndex := 0;
  BStringSizeCheck.Enabled := False;
  BeforeCheckBox.Checked := False;
  BStringSizeCheck.Checked := False;
 end;
end;

procedure TParamDialog.AfterEditChange(Sender: TObject);
Var S: String; D: Integer;
begin
 If not CanChange then Exit;
 S := AfterEdit.Text;
 If not UnsCheckOut(S) then AfterEdit.Value := AfterFix Else
 begin
  CanChange := False;
  D := GetLDW(S);
  If D < AfterFix then D := AfterFix;
  AfterEdit.Value := D;
  CanChange := True;
  BeforeEdit.Value := Interval - D;
  DataChanged := True;
  DataAccepted := False;  
 end;
end;

procedure TParamDialog.BeforeEditChange(Sender: TObject);
Var S: String; D: Integer;
begin
 If not CanChange then Exit;
 S := BeforeEdit.Text;
 If not UnsCheckOut(S) then BeforeEdit.Value := BeforeFix Else
 begin
  CanChange := False;
  D := GetLDW(S);
  If D < BeforeFix then D := BeforeFix;
  BeforeEdit.Value := D;
  CanChange := True;
  AfterEdit.Value := Interval - D;
  DataChanged := True;
  DataAccepted := False;  
 end;
end;

procedure TParamDialog.AddItemExecute(Sender: TObject);
Var P: PParameter; TV: TTreeView; S: String; I: Integer;
begin
 If PageControl.ActivePageIndex = 1 then
 begin
  If BeforeFix + 1 > BeforeEdit.Value then Exit;
  TV := BeforeTreeView;
  Inc(BeforeFix);
  BeforeFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [BeforeFix]);
 end Else
 begin
  If AfterFix + 1 > AfterEdit.Value then Exit;
  TV := AfterTreeView;
  Inc(AfterFix);
  AfterFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [AfterFix]);
 end;
 P := Params.Add;
 P^.paBefore := PageControl.ActivePageIndex = 1;
 I := 1;
 Repeat
  S := 'Value' + IntToStr(I);
  Inc(I);
 Until Params.Find(S) = NIL;
 P^.paName := S;
 P^.paParamType := ptByte;
 P^.paFlags := 0;
 TV.Selected := TV.Items.AddNode(NIL, NIL, S, P, naAdd);
 If PageControl.ActivePageIndex = 0 then
  AfterTreeViewClick(Sender) Else
  BeforeTreeViewClick(Sender);
 DataChanged := True;
 DataAccepted := False;   
end;

procedure TParamDialog.MoveUpItemExecute(Sender: TObject);
Var D: TTreeNode;
begin
 If PageControl.ActivePageIndex = 1 then With BeforeTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  If D.GetPrevSibling = NIL then Exit;
  D.MoveTo(D.GetPrevSibling, naInsert);
  DataChanged := True;
  DataAccepted := False;
 end Else With AfterTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  If D.GetPrevSibling = NIL then Exit;
  D.MoveTo(D.GetPrevSibling, naInsert);
  DataChanged := True;
  DataAccepted := False;  
 end;
end;

procedure TParamDialog.MoveDownItemExecute(Sender: TObject);
Var D: TTreeNode;
begin
 If PageControl.ActivePageIndex = 1 then With BeforeTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  If D.GetNextSibling = NIL then Exit;
  D.GetNextSibling.MoveTo(D, naInsert);
  DataChanged := True;
  DataAccepted := False;
 end Else With AfterTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  If D.GetNextSibling = NIL then Exit;
  D.GetNextSibling.MoveTo(D, naInsert);
  DataChanged := True;
  DataAccepted := False;    
 end;
end;

procedure TParamDialog.DeleteItemExecute(Sender: TObject);
Var N, D: TTreeNode;
begin
 If PageControl.ActivePageIndex = 1 then With BeforeTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  N := D.GetNextSibling;
  If N = NIL then N := D.GetPrevSibling;
  Selected := N;
  Dec(BeforeFix, CParamSize[PParameter(D.Data)^.paParamType]);
  Params.Remove(D.Data);
  Items.Delete(D);
  BeforeFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [BeforeFix]);
  BeforeTreeViewClick(Sender);
  DataChanged := True;
  DataAccepted := False;
 end Else With AfterTreeView do
 begin
  D := Selected;
  If D = NIL then Exit;
  N := D.GetNextSibling;
  If N = NIL then N := D.GetPrevSibling;
  Selected := N;
  Dec(AfterFix, CParamSize[PParameter(D.Data)^.paParamType]);
  Params.Remove(D.Data);
  Items.Delete(D);
  AfterFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [AfterFix]);
  AfterTreeViewClick(Sender);
  DataChanged := True;
  DataAccepted := False;
 end;
end;

procedure TParamDialog.AfterTabShow(Sender: TObject);
begin
 AfterTreeViewChange(Sender, AfterTreeView.Selected);
end;

procedure TParamDialog.BeforeTabShow(Sender: TObject);
begin
 BeforeTreeViewChange(Sender, BeforeTreeView.Selected);
end;

procedure TParamDialog.AfterComboBoxSelect(Sender: TObject);
Var PT: TParamType;
begin
 With PParameter(AfterTreeView.Selected.Data)^ do
 begin
  PT := TParamType(AfterComboBox.ItemIndex);
  If ((AfterFix - CParamSize[paParamType]) + CParamSize[PT] > AfterEdit.Value) then
  begin
   AfterComboBox.ItemIndex := Integer(paParamType);
  end Else
  begin
   If PT = paParamType then Exit;
   Dec(AfterFix, CParamSize[paParamType]);
   paParamType := PT;
   Inc(AfterFix, CParamSize[paParamType]);
   AfterFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [AfterFix]);
   DataChanged := True;
   DataAccepted := False;
  end;
 end;
end;

procedure TParamDialog.AfterTreeViewClick(Sender: TObject);
begin
 AfterTreeViewChange(Sender, AfterTreeView.Selected);
end;

procedure TParamDialog.BeforeTreeViewClick(Sender: TObject);
begin
 BeforeTreeViewChange(Sender, BeforeTreeView.Selected);
end;

procedure TParamDialog.AfterCheckBoxClick(Sender: TObject);
begin
 With PParameter(AfterTreeView.Selected.Data)^ do
 begin
  If AfterCheckBox.Checked then
   paFlags := paFlags or PAR_BIGENDIAN Else
   paFlags := paFlags and not PAR_BIGENDIAN;
  DataChanged := True;
  DataAccepted := False;
 end;
end;

procedure TParamDialog.BeforeComboBoxSelect(Sender: TObject);
Var PT: TParamType;
begin
 With PParameter(BeforeTreeView.Selected.Data)^ do
 begin
  PT := TParamType(BeforeComboBox.ItemIndex);
  If ((BeforeFix - CParamSize[paParamType]) + CParamSize[PT] > BeforeEdit.Value) then
  begin
   BeforeComboBox.ItemIndex := Integer(paParamType);
  end Else
  begin
   If paParamType = PT then Exit;
   Dec(BeforeFix, CParamSize[paParamType]);
   paParamType := PT;
   Inc(BeforeFix, CParamSize[paParamType]);
   BeforeFixLabel.Caption := WideFormat(K7S(SPDBytesOccupied), [BeforeFix]);
   DataChanged := True;
   DataAccepted := False;
  end;
 end;
end;

procedure TParamDialog.BeforeCheckBoxClick(Sender: TObject);
begin
 With PParameter(BeforeTreeView.Selected.Data)^ do
 begin
  If BeforeCheckBox.Checked then
   paFlags := paFlags or PAR_BIGENDIAN Else
   paFlags := paFlags and not PAR_BIGENDIAN;
  DataChanged := True;
  DataAccepted := False;
 end;
end;

procedure TParamDialog.AfterTreeViewEdited(Sender: TObject;
  Node: TTreeNode; var S: String);
begin
 OkButton.Enabled := True;
 If S = PParameter(Node.Data)^.paName then Exit;
 If Params.Find(S) = NIL then
 begin
  PParameter(Node.Data)^.paName := S;
  DataChanged := True;
  DataAccepted := False;
 end Else
 begin
  AddStateString(etNone, '', 0, K7S(SPDValueIsExist));
  S := PParameter(Node.Data)^.paName;
 end;
end;

procedure TParamDialog.BeforeTreeViewEdited(Sender: TObject;
  Node: TTreeNode; var S: String);
begin
 OkButton.Enabled := True;
 If S = PParameter(Node.Data)^.paName then Exit;
 If Params.Find(S) = NIL then
 begin
  PParameter(Node.Data)^.paName := S;
  DataChanged := True;
  DataAccepted := False;
 end Else
 begin
  AddStateString(etNone, '', 0, K7S(SPDValueIsExist));
  S := PParameter(Node.Data)^.paName;
 end;
end;

procedure TParamDialog.AfterTreeViewEditing(Sender: TObject;
  Node: TTreeNode; var AllowEdit: Boolean);
begin
 OkButton.Enabled := False;
end;

procedure TParamDialog.BeforeTreeViewEditing(Sender: TObject;
  Node: TTreeNode; var AllowEdit: Boolean);
begin
 OkButton.Enabled := False;
end;

procedure TParamDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 Timer.Enabled := False;
 Params.Clear;
end;

procedure TParamDialog.TimerTimer(Sender: TObject);
begin
 If not AfterTreeView.IsEditing and
    not BeforeTreeView.IsEditing then OkButton.Enabled := True;
 ApplyButton.Enabled := not DataAccepted;
end;

procedure TParamDialog.ApplyButtonClick(Sender: TObject);
Var N: TTreeNode;
begin
 Parameters^.Clear;
 Parameters^.Before := BeforeEdit.Value;
 Parameters^.AfterFix := AfterFix;
 Parameters^.BeforeFix := BeforeFix;
 With Parameters^ do
 begin
  N := AfterTreeView.Items.GetFirstNode;
  While N <> NIL do
  begin
   With Add^ do
   begin
    paName := PParameter(N.Data)^.paName;
    paBefore := False;
    paParamType := PParameter(N.Data)^.paParamType;
    paFlags := PParameter(N.Data)^.paFlags;
   end;
   N := N.GetNextSibling;
  end;
  N := BeforeTreeView.Items.GetFirstNode;
  While N <> NIL do
  begin
   With Add^ do
   begin
    paName := PParameter(N.Data)^.paName;
    paBefore := True;
    paParamType := PParameter(N.Data)^.paParamType;
    paFlags := PParameter(N.Data)^.paFlags;
   end;
   N := N.GetNextSibling;
  end;
 end;
 DataAccepted := True;
 with MainForm do SetSaved(SelectedRoot.Data, False);
end;

procedure TParamDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If DataAccepted then Exit;
 If LocMessageDlg(K7S(SPDCloseConfirmation), mtConfirmation,
                 [mbYes, mbNo], 0) = mrYes then
 begin
  CanClose := True;
 end Else CanClose := False;
end;

procedure TParamDialog.BStringSizeCheckClick(Sender: TObject);
begin
 With PParameter(BeforeTreeView.Selected.Data)^ do
 begin
  If BStringSizeCheck.Checked then
   paFlags := paFlags or PAR_AUTOSIZE Else
   paFlags := paFlags and not PAR_AUTOSIZE;
  DataChanged := True;
  DataAccepted := False;
 end;
end;

procedure TParamDialog.AStringSizeCheckClick(Sender: TObject);
begin
 With PParameter(AfterTreeView.Selected.Data)^ do
 begin
  If AStringSizeCheck.Checked then
   paFlags := paFlags or PAR_AUTOSIZE Else
   paFlags := paFlags and not PAR_AUTOSIZE;
  DataChanged := True;
  DataAccepted := False;
 end;
end;

initialization
 Params.Init;
finalization
 Params.Done;
end.
