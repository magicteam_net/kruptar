unit SearchText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, LocClasses, KruptarComponents;

type
  TSearchTextDialog = class(TLocForm)
    SearchText: TKruptarComboBox;
    SearchLabel: TLocLabel;
    OptionsBox: TLocGroupBox;
    CaseCheckBox: TLocCheckBox;
    WholeCheckBox: TLocCheckBox;
    OkButton: TLocButton;
    CancelButton: TLocButton;
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
   SearchString: WideString;
   Function Execute: Boolean;
  end;

var
  SearchTextDialog: TSearchTextDialog;
  OkPressed: Boolean;

implementation

Uses MainUnit, ReplaceText, StringConsts, LocMsgBox;

{$R *.dfm}

Function TSearchTextDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
 If Result then
 begin
  SearchString := SearchText.Text;
  SearchText.Items.Add(SearchString);
  ReplaceTextDialog.SearchText.Items := SearchText.Items;
 end Else SearchString := ''; 
end;

procedure TSearchTextDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 SearchText.SetFocus;
end;

procedure TSearchTextDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TSearchTextDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If OkPressed then
 begin
  If SearchText.Text = '' then
  begin
   LocMessageDlg(K7S(SRTInputSearchText), mtError, [mbOk], 0);
   CanClose := False;
   OkPressed := False;
  end;
 end;
end;

end.
