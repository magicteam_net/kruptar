unit about;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, LocClasses, KruptarComponents;

type
  TAboutBox = class(TLocForm)
    Panel: TKruptarPanel;
    OKButton: TLocButton;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    SiteLabel: TLabel;
    MailLabel: TLabel;
    DateLabel: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

Procedure AboutDialog;

implementation

uses MainUnit, StringConsts;//, MMSystem;

{$R *.dfm}
//{$R SOUND.RES}

procedure TAboutBox.FormCreate(Sender: TObject);
var
 S: String; N, Len: Cardinal; Buf: PChar; Value: PChar;
begin
 S := ParamStr(0);
 DateLabel.Caption := DateTimeToStr(FileDateToDateTime(FileAge(S)));
 N := GetFileVersionInfoSize(PChar(S), N);
 If N > 0 then
 begin
  Buf := AllocMem(N);
  GetFileVersionInfo(PChar(S), 0, n, Buf);
  If not VerQueryValue(Buf, PChar('StringFileInfo\040904E4\FileVersion'),
   Pointer(Value), Len) then Value := PChar('7.0.0.0');
  Version.Caption := Format(String(K7S(SMUVersion)), [Value]);
  If VerQueryValue(Buf, PChar('StringFileInfo\040904E4\LastCompiledTime'),
   Pointer(Value), Len) then
   DateLabel.Caption := Value;
  FreeMem(Buf, n);
 end;
{ WaveHandle := FindResource(hInstance, 'KRUPTAR_WAV', RT_RCDATA);
 if WaveHandle <> 0 then
 begin
  WaveHandle := LoadResource(hInstance,WaveHandle);
  if WaveHandle <> 0 then
  begin
   WavePointer := LockResource(WaveHandle);
   SndPlaySound(WavePointer, SND_MEMORY or SND_ASYNC);
   UnlockResource(WaveHandle);
   FreeResource(WaveHandle);
  end;
 end;}
end;

Procedure AboutDialog;
var AboutBox: TAboutBox;
begin
 Application.CreateForm(TAboutBox, AboutBox);
 AboutBox.ShowModal;
 AboutBox.Free;
end;

end.

