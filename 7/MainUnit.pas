unit MainUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolWin, ComCtrls, StdCtrls,
  Menus, StringConsts, LocClasses, ExtCtrls,
  ImgList, ActnList, ValEdit, Needs, Dynamics, ShellApi, KaZip,
  LocActnList, Grids, Masks,
  KruptarComponents, UnicodeUtils, FormatW
  {$IFNDEF FORCED_UNICODE}
  , TntSystem
  , TntForms
  , TntComCtrls
  , TntClasses
  , TntStdCtrls
  , TntMenus
  , TntDialogs
  , TntExtCtrls
  , TntSysUtils
  , TntActnList
  , TntClipBrd
  {$ELSE}
  , Clipbrd
  , UITypes
  , Actions
  {$ENDIF};

type
  TMainForm = class(TKruptarForm)
    ToolBar: TKruptarToolBar;
    MainMenu: TKruptarMainMenu;
    MenuFileNewButton: TKruptarToolButton;
    MenuFileOpenButton: TKruptarToolButton;
    MenuFileSaveButton: TKruptarToolButton;
    MenuProjectGroupAddButton: TKruptarToolButton;
    MenuProjectItemUpButton: TKruptarToolButton;
    MenuProjectItemDownButton: TKruptarToolButton;
    MenuProjectItemRemoveButton: TKruptarToolButton;
    ImageList: TImageList;
    FileMenu: TKruptarMenuItem;
    MenuFileNewItem: TKruptarMenuItem;
    MenuFileOpenItem: TKruptarMenuItem;
    MenuFileSaveItem: TKruptarMenuItem;
    MenuFileSaveAsItem: TKruptarMenuItem;
    MenuFileCloseItem: TKruptarMenuItem;
    FileSeparator2: TKruptarMenuItem;
    FileRecent1Item: TKruptarMenuItem;
    FileRecent2Item: TKruptarMenuItem;
    FileRecent3Item: TKruptarMenuItem;
    FileRecent4Item: TKruptarMenuItem;
    FileSeparator3: TKruptarMenuItem;
    FileExitItem: TKruptarMenuItem;
    ProjectMenu: TKruptarMenuItem;
    Separator3: TKruptarToolButton;
    ActionList: TLocActionList;
    MenuFileNewAction: TLocAction;
    StatusBar: TKruptarStatusBar;
    MenuFileOpenAction: TLocAction;
    MenuFileSaveAction: TLocAction;
    MenuFileSaveAsAction: TLocAction;
    MenuFileCloseAction: TLocAction;
    MenuFileExitAction: TLocAction;
    MenuProjectGroupAddAction: TLocAction;
    MenuProjectPointersAddAction: TLocAction;
    MenuProjectGroupTextReloadAction: TLocAction;
    MenuProjectGroupTextResetAction: TLocAction;
    MenuProjectItemUpAction: TLocAction;
    MenuProjectItemDownAction: TLocAction;
    MenuProjectItemRemoveAction: TLocAction;
    MenuProjectGroupTextInsertAction: TLocAction;
    MenuProjectTextInsertAction: TLocAction;
    FontDialog: TFontDialog;
    MenuFileSaveAllButton: TKruptarToolButton;
    MenuFileSaveAllAction: TLocAction;
    MenuFileSaveAllItem: TKruptarMenuItem;
    MenuEditLoadButton: TKruptarToolButton;
    MenuEditLoadAction: TLocAction;
    MenuProjectTableLoadItem: TKruptarMenuItem;
    Separator1: TKruptarToolButton;
    OpenProjectDialog: TLocOpenDialog;
    MenuProjectTableAddItem: TKruptarMenuItem;
    MenuProjectTableAddButton: TKruptarToolButton;
    MenuProjectItemAddAction: TLocAction;
    StatesPopup: TKruptarPopupMenu;
    MenuOtherClearMessageListAction: TLocAction;
    ClearStateMemoPopItem: TKruptarMenuItem;
    FileSeparator1: TKruptarMenuItem;
    MenuFileSaveAsButton: TKruptarToolButton;
    OpenDialog: TLocOpenDialog;
    MenuProjectTableAnsiFillAction: TLocAction;
    OpenTableDialog: TLocOpenDialog;
    MenuProjectTablesItem: TKruptarMenuItem;
    MenuProjectGroupsItem: TKruptarMenuItem;
    MenuEditSaveAction: TLocAction;
    MenuProjectTableSaveItem: TKruptarMenuItem;
    MenuProjectGroupAddItem: TKruptarMenuItem;
    MenuProjectItemUpItem: TKruptarMenuItem;
    MenuProjectItemDownItem: TKruptarMenuItem;
    MenuProjectItemRemoveItem: TKruptarMenuItem;
    ProjectSeparator1: TKruptarMenuItem;
    GroupsSeparator1: TKruptarMenuItem;
    MenuProjectPointersItem: TKruptarMenuItem;
    GroupsSeparator3: TKruptarMenuItem;
    MenuProjectPointersAddItem: TKruptarMenuItem;
    MenuEditSaveButton: TKruptarToolButton;
    MenuProjectGroupTextReloadItem: TKruptarMenuItem;
    MenuProjectGroupTextResetItem: TKruptarMenuItem;
    MenuProjectItemAddItem: TKruptarMenuItem;
    GroupsSeparator4: TKruptarMenuItem;
    ProjectSeparator2: TKruptarMenuItem;
    MenuProjectTextInsertItem: TKruptarMenuItem;
    MenuProjectItemAddButton: TKruptarToolButton;
    MenuProjectGroupTextInsertItem: TKruptarMenuItem;
    MenuProjectTableAnsiFillItem: TKruptarMenuItem;
    MenuProjectPointersTableLinksResetAction: TLocAction;
    MenuProjectPointersTableLinksResetItem: TKruptarMenuItem;
    MenuProjectPointersVarOptionsAction: TLocAction;
    MenuProjectPointersVarOptionsItem: TKruptarMenuItem;
    MenuEditClearAction: TLocAction;
    PointersSeparator2: TKruptarMenuItem;
    MenuProjectPointersClearItem: TKruptarMenuItem;
    TextEditPopup: TKruptarPopupMenu;
    MenuEditCutAction: TLocAction;
    MenuEditCopyAction: TLocAction;
    MenuEditPasteAction: TLocAction;
    MenuEditSelectAllAction: TLocAction;
    CutItem: TKruptarMenuItem;
    CopyItem: TKruptarMenuItem;
    PasteItem: TKruptarMenuItem;
    N7: TKruptarMenuItem;
    SelectAllItem: TKruptarMenuItem;
    PointersSeparator1: TKruptarMenuItem;
    SaveProjectDialog: TLocSaveDialog;
    MenuFileCloseAllAction: TLocAction;
    MenuFileCloseAllItem: TKruptarMenuItem;
    MenuEditUndoAction: TLocAction;
    N4: TKruptarMenuItem;
    N5: TKruptarMenuItem;
    SaveTableDialog: TLocSaveDialog;
    MenuProjectGroupDictOptionsAction: TLocAction;
    GroupsSeparator2: TKruptarMenuItem;
    MenuProjectGroupDictOptionsItem: TKruptarMenuItem;
    MenuProjectGroupDictGenerateAction: TLocAction;
    MenuProjectGroupDictToTableAction: TLocAction;
    MenuProjectGroupDictGenerateItem: TKruptarMenuItem;
    MenuProjectGroupDictToTableItem: TKruptarMenuItem;
    Timer: TTimer;
    MenuSearchFindAction: TLocAction;
    MenuSearchFindNextAction: TLocAction;
    MenuSearchFindReplaceAction: TLocAction;
    SearchMenu: TKruptarMenuItem;
    MenuSearchFindItem: TKruptarMenuItem;
    MenuSearchFindNextItem: TKruptarMenuItem;
    MenuSearchFindReplaceItem: TKruptarMenuItem;
    MenuProjectGroupTextInsertButton: TKruptarToolButton;
    MenuProjectTextInsertButton: TKruptarToolButton;
    Separator4: TKruptarToolButton;
    HelpMenu: TKruptarMenuItem;
    MenuHelpPluginsItem: TKruptarMenuItem;
    OpenTextDialog: TLocOpenDialog;
    MenuHelpAboutAction: TLocAction;
    HelpSeparator1: TKruptarMenuItem;
    MenuHelpAboutItem: TKruptarMenuItem;
    MenuProjectRunRom1Action: TLocAction;
    MenuProjectRunRom2Action: TLocAction;
    MenuProjectEmulatorSelectAction: TLocAction;
    ProjectSeparator3: TKruptarMenuItem;
    MenuProjectEmulatorSelectItem: TKruptarMenuItem;
    MenuProjectRunRom1Item: TKruptarMenuItem;
    MenuProjectRunRom2Item: TKruptarMenuItem;
    Separator5: TKruptarToolButton;
    MenuProjectRunRom1Button: TKruptarToolButton;
    MenuProjectRunRom2Button: TKruptarToolButton;
    Localizer: TLocalizer;
    MenuFileAction: TLocAction;
    MenuProjectAction: TLocAction;
    MenuSearchAction: TLocAction;
    MenuLanguageAction: TLocAction;
    LanguageMenu: TKruptarMenuItem;
    MenuHelpAction: TLocAction;
    MenuHelpPluginsAction: TLocAction;
    MenuProjectTablesAction: TLocAction;
    MenuProjectGroupsAction: TLocAction;
    MenuProjectPointersAction: TLocAction;
    EditMenu: TKruptarMenuItem;
    MenuEditAction: TLocAction;
    MenuEditUndoItem: TKruptarMenuItem;
    EditSeparator1: TKruptarMenuItem;
    MenuEditCutItem: TKruptarMenuItem;
    MenuEditCopyItem: TKruptarMenuItem;
    MenuEditPasteItem: TKruptarMenuItem;
    MenuEditSelectAllItem: TKruptarMenuItem;
    EditSeparator2: TKruptarMenuItem;
    EditSeparator3: TKruptarMenuItem;
    N1: TKruptarMenuItem;
    MenuEditTextResetAction2: TKruptarMenuItem;
    TabsPopup: TKruptarPopupMenu;
    absLoadFromFileAction1: TKruptarMenuItem;
    absSaveToFileAction1: TKruptarMenuItem;
    TabsSourceSaveToFileAction: TLocAction;
    N2: TKruptarMenuItem;
    absSourceSaveToFileAction1: TKruptarMenuItem;
    SaveTextDialog: TLocSaveDialog;
    MenuOptionsAction: TLocAction;
    OptionsMenu: TKruptarMenuItem;
    LanguageMenuSeparator: TKruptarMenuItem;
    MenuOptionsFontAction: TLocAction;
    Font1: TKruptarMenuItem;
    MenuProjectPointersToggleInsertFieldsAction: TLocAction;
    MenuProjectPointersAddInsertInfoMenuItem: TKruptarMenuItem;
    MenuProjectPointerFixPointersPositionsAction: TLocAction;
    FixPointersPositions2: TKruptarMenuItem;
    N8: TKruptarMenuItem;
    MenuEditClearButton: TKruptarToolButton;
    TntToolButton2: TKruptarToolButton;
    TntToolButton3: TKruptarToolButton;
    TntToolButton4: TKruptarToolButton;
    TntToolButton5: TKruptarToolButton;
    TntToolButton6: TKruptarToolButton;
    TntToolButton7: TKruptarToolButton;
    TntToolButton8: TKruptarToolButton;
    TntToolButton9: TKruptarToolButton;
    N25: TKruptarMenuItem;
    Clear5: TKruptarMenuItem;
    N26: TKruptarMenuItem;
    LoadFromFile2: TKruptarMenuItem;
    SaveToFile3: TKruptarMenuItem;
    TntToolButton1: TKruptarToolButton;
    TntToolButton10: TKruptarToolButton;
    N27: TKruptarMenuItem;
    Clear6: TKruptarMenuItem;
    Cut4: TKruptarMenuItem;
    Copy4: TKruptarMenuItem;
    Paste4: TKruptarMenuItem;
    TabsCopySourceTextAction: TLocAction;
    CopySourceText1: TKruptarMenuItem;
    N28: TKruptarMenuItem;
    N30: TKruptarMenuItem;
    extReload1: TKruptarMenuItem;
    extReset1: TKruptarMenuItem;
    TntToolButton11: TKruptarToolButton;
    TntToolButton12: TKruptarToolButton;
    TntToolButton14: TKruptarToolButton;
    CustomPopup: TKruptarPopupMenu;
    ReloadSourceText1: TKruptarMenuItem;
    MenuProjectItemsAddFromFileAction: TLocAction;
    TntToolButton15: TKruptarToolButton;
    AddFromFile1: TKruptarMenuItem;
    N3: TKruptarMenuItem;
    MenuProjectTableAddAction: TLocAction;
    ProjectTabs: TKruptarTabControl;
    Panel: TKruptarPanel;
    GroupsTabControl: TKruptarTabControl;
    PTablesTabControl: TKruptarTabControl;
    ListBox: TKruptarListBox;
    ListStatusBar: TKruptarStatusBar;
    BottomPanel: TKruptarPanel;
    StatesMemo: TKruptarMemo;
    OriginalPanel: TKruptarPanel;
    OriginalMemo: TKruptarMemo;
    OriginalStatusBar: TKruptarStatusBar;
    TranslatePanel: TKruptarPanel;
    TranslateMemo: TKruptarMemo;
    TranslateStatusBar: TKruptarStatusBar;
    LeftPanel: TKruptarPanel;
    TreeView: TKruptarTreeView;
    ValuePalette: TKruptarPanel;
    ValueListBox: TValueListEditor;
    PointersSortAscAction: TLocAction;
    PointersSortDescAction: TLocAction;
    PointersSeparator3: TKruptarMenuItem;
    AscendingSortMenuItem: TKruptarMenuItem;
    DescendingSortMenuItem: TKruptarMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PTablesTabControlChange(Sender: TObject);
    procedure PTablesTabControlMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ListBoxDblClick(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure ListBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FileRecentItemClick(Sender: TObject);
    procedure MenuItemClick(Sender: TObject);
    procedure MenuEditLoadActionUpdate(Sender: TObject);
    procedure MenuEditSaveActionUpdate(Sender: TObject);
    procedure MenuProjectTableAnsiFillActionUpdate(Sender: TObject);
    procedure MenuProjectTableAnsiFillActionExecute(Sender: TObject);
    procedure MenuEditSaveActionExecute(Sender: TObject);
    procedure MenuActionsUpdate1(Sender: TObject);
    procedure MenuProjectGroupAddActionExecute(Sender: TObject);
    procedure MenuProjectGroupTextReloadActionUpdate(Sender: TObject);
    procedure MenuProjectGroupTextReloadActionExecute(Sender: TObject);
    procedure MenuFileSaveActionExecute(Sender: TObject);
    procedure MenuProjectGroupTextResetActionUpdate(Sender: TObject);
    procedure MenuProjectGroupTextResetActionExecute(Sender: TObject);
    procedure MenuProjectGroupTextInsertActionUpdate(Sender: TObject);
    procedure MenuProjectGroupTextInsertActionExecute(Sender: TObject);
    procedure MenuProjectTextInsertActionUpdate(Sender: TObject);
    procedure MenuProjectTextInsertActionExecute(Sender: TObject);
    procedure MenuDictActionsUpdate(Sender: TObject);
    procedure MenuProjectGroupDictToTableActionUpdate(Sender: TObject);
    procedure MenuProjectGroupDictOptionsActionExecute(Sender: TObject);
    procedure MenuProjectGroupDictGenerateActionExecute(Sender: TObject);
    procedure MenuProjectGroupDictToTableActionExecute(Sender: TObject);
    procedure MenuProjectPointersToggleInsertFieldsActionUpdate(
      Sender: TObject);
    procedure MenuProjectPointersToggleInsertFieldsActionExecute(
      Sender: TObject);
    procedure MenuProjectPointersAddActionUpdate(Sender: TObject);
    procedure MenuProjectPointersAddActionExecute(Sender: TObject);
    procedure MenuEditCopyCutActionUpdate(Sender: TObject);
    procedure MenuEditCopyActionExecute(Sender: TObject);
    procedure MenuEditClearActionUpdate(Sender: TObject);
    procedure MenuEditClearActionExecute(Sender: TObject);
    procedure MenuEditSelectAllActionExecute(Sender: TObject);    
    procedure MenuFileSaveAsActionExecute(Sender: TObject);
    procedure MenuFileSaveAllActionUpdate(Sender: TObject);
    procedure MenuFileSaveAllActionExecute(Sender: TObject);
    procedure MenuFileCloseActionExecute(Sender: TObject);
    procedure MenuFileCloseAllActionUpdate(Sender: TObject);
    procedure MenuFileCloseAllActionExecute(Sender: TObject);
    procedure MenuProjectPointersVarOptionsActionUpdate(Sender: TObject);
    procedure MenuProjectPointersVarOptionsActionExecute(
      Sender: TObject);
    procedure MenuProjectPointersTableLinksResetActionUpdate(
      Sender: TObject);
    procedure MenuProjectPointersTableLinksResetActionExecute(
      Sender: TObject);
    procedure MenuProjectItemAddActionUpdate(Sender: TObject);
    procedure MenuProjectItemAddActionExecute(Sender: TObject);
    procedure MenuProjectItemsAddFromFileActionUpdate(Sender: TObject);
    procedure MenuEditLoadAddActionExecute(Sender: TObject);
    procedure MenuProjectItemUpActionUpdate(Sender: TObject);
    procedure MenuProjectItemUpActionExecute(Sender: TObject);
    procedure MenuProjectItemDownActionUpdate(Sender: TObject);
    procedure MenuProjectItemDownActionExecute(Sender: TObject);
    procedure MenuProjectItemRemoveActionUpdate(Sender: TObject);
    procedure MenuProjectItemRemoveActionExecute(Sender: TObject);
    procedure MenuProjectEmulatorSelectActionExecute(Sender: TObject);
    procedure MenuProjectRunRom1ActionExecute(Sender: TObject);
    procedure MenuProjectRunRom2ActionExecute(Sender: TObject);
    procedure MenuProjectRunRom1ActionUpdate(Sender: TObject);
    procedure MenuProjectRunRom2ActionUpdate(Sender: TObject);
    procedure MenuOtherClearMessageListActionUpdate(Sender: TObject);
    procedure MenuOtherClearMessageListActionExecute(Sender: TObject);
    procedure MenuEditUndoActionUpdate(Sender: TObject);
    procedure MenuEditUndoActionExecute(Sender: TObject);
    procedure MenuEditCutActionExecute(Sender: TObject);
    procedure MenuEditPasteActionUpdate(Sender: TObject);
    procedure MenuEditPasteActionExecute(Sender: TObject);
    procedure MenuEditSelectAllActionUpdate(Sender: TObject);
    procedure MenuSearchActionsUpdate(Sender: TObject);
    procedure MenuSearchFindActionExecute(Sender: TObject);
    procedure MenuSearchFindReplaceActionExecute(Sender: TObject);
    procedure MenuSearchFindNextActionExecute(Sender: TObject);
    procedure MenuHelpAboutActionExecute(Sender: TObject);
    procedure TabsSourceSaveToFileActionExecute(Sender: TObject);
    procedure TabsCopySourceTextActionExecute(Sender: TObject);
    procedure MenuProjectPointerFixPointersPositionsActionUpdate(
      Sender: TObject);
    procedure MenuProjectPointerFixPointersPositionsActionExecute(
      Sender: TObject);
    procedure MenuProjectTableAddActionExecute(Sender: TObject);
    procedure MenuFileExitActionExecute(Sender: TObject);
    procedure MenuFileNewActionExecute(Sender: TObject);
    procedure MenuFileOpenActionExecute(Sender: TObject);    
    procedure PointersSortActionExecute(Sender: TObject);
    procedure PointersSortActionUpdate(Sender: TObject);
    procedure MenuHelpPluginsItemClick(Sender: TObject);
    procedure ConfigFontItemClick(Sender: TObject);
    procedure TranslateMemoKeyPress(Sender: TObject; var Key: Char);
    procedure TranslateMemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TranslateMemoChange(Sender: TObject);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure TreeViewClick(Sender: TObject);
    procedure TreeViewCollapsing(Sender: TObject; Node: TTreeNode;
      var AllowCollapse: Boolean);
    procedure TreeViewDblClick(Sender: TObject);
{$IFNDEF FORCED_UNICODE}
    procedure TreeViewEdited(Sender: TObject; Node: TTntTreeNode; var S: WideString);
{$ELSE}
    procedure TreeViewEdited(Sender: TObject; Node: TTreeNode; var S: String);
{$ENDIF}
    procedure TreeViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TreeViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ValueListBoxClick(Sender: TObject);
    procedure ValueListBoxEditButtonClick(Sender: TObject);
    procedure ValueListBoxExit(Sender: TObject);
    procedure ValueListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ValueListBoxKeyPress(Sender: TObject; var Key: Char);
    procedure ValueListBoxSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ProjectTabsChange(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure GroupsTabControlChange(Sender: TObject);
    procedure GroupsTabControlMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CustomPopupPopup(Sender: TObject);
    procedure SplitCanResize(Sender: TObject; var NewSize: Integer;
    var Accept: Boolean);
    procedure NewSplitCanResize(Sender: TObject; var NewSize: Integer;
    var Accept: Boolean);
    procedure PSplitCanResize(Sender: TObject; var NewSize: Integer;
    var Accept: Boolean);
    procedure LangActionClick(Sender: TObject);
  protected
    procedure WMDropFiles(var Msg: TMessage); message wm_DropFiles;
  public
    SelectedData: Pointer;
    SelectedRoot: TKruptarTreeNodeType;
    InsertMode: Boolean;
    FLang: WideString;
    FSaveTab1Index: Integer;
    FSaveTab2Index: Integer;
    AllSaved: Boolean;
    procedure AllSavedCheck;
    procedure RefreshProjectTabs(ProjectIndex: Integer);
    procedure RefreshTree(ProjectIndex: Integer);
    procedure CheckValues;
    procedure RefreshList;
    procedure RefreshProject(Node: TKruptarTreeNodeType; Current: Boolean);
    procedure RefreshTables(Node: TKruptarTreeNodeType);
    procedure ChangeGroups(Node: TKruptarTreeNodeType);
    procedure RefreshGroups(Node: TKruptarTreeNodeType; Current: Boolean);
    procedure RefreshTable(Node: TKruptarTreeNodeType);
    procedure RefreshGroup(Node: TKruptarTreeNodeType; Current: Boolean);
    procedure RefreshBlocks(Node: TKruptarTreeNodeType);
    procedure RefreshPtrTables(Node: TKruptarTreeNodeType);
    procedure RefreshPtrTable(Node: TKruptarTreeNodeType);
    procedure SetSaved(PRJ: Pointer; S: Boolean);
    procedure OpenFile(const FileName: WideString);
    procedure OpenFiles(Files: TWideStrings);
    function SaveProject(PRJ: Pointer): Boolean;
    procedure LoadLanguages(const CurrentLanguage: WideString);
    procedure AddPointersList(BlockStart, BlockEnd: LongInt;
      Project, DLL_, PtrsList: Pointer;
     const ListFileName: WideString = ''; ListIndex: Integer = -1);
    function GetEditHandle(CheckReadOnly: Boolean = False): HWND;
    function CopyStringData(Clr: Boolean): WideString;
    procedure ClearData;
    procedure PasteStringData(const S, FileName: WideString; Clr: Boolean);
    procedure PointersListAdd(List: TWideStringList;
              const FileName: WideString);
    procedure BlocksListAdd(List: TWideStringList; const FileName: WideString);
    procedure AddRecent(const FileName: WideString);
  end;

function FSplit(const S: WideString; var Dir: WideString): WideString;
function GetExt(const Src: WideString; var Dst: WideString): WideString;
function BoolStr(B: Boolean): String;
function FieldFileExists(const FieldName, FileName: String; ConsoleMode: Boolean): Boolean;
function MsgHexToInt(const FileName: WideString; Index: Integer;
                     const Value: String): LongInt;

var
 MainForm: TMainForm;
 PanelSplit: TSplitter;
 MemoSplit: TSplitter;
 ListSplit: TSplitter;
 NewSplit: TSplitter;

type
 PString = ^TString;
 TString = record
  stOriginal: RawByteString;
  stNew: WideString;
  Next: PString;
 end;
 PKStrings = ^TKStrings;
 TKStrings = object
  Root, Cur: PString;
  Count: Integer;
  constructor Init;
  function Add: PString;
  function Get(I: Integer): PString;
  destructor Done;
 end;
 PTable = ^TTable;
 TTableAddMode = (taTBL, taCodes, taEls, taEnds);
 TTable = object
  Owner: PDynamic;
  tbName: WideString;
  Root, Cur: PTableItem;
  EndRoot, EndCur: PTableItem;
  ElRoot, ElCur: PTableItem;
  CMaxCodes, EndMaxCodes, ElMaxCodes: Integer;
  CMaxChars: Integer;
  Count, EndCount, ElCount: Integer;
  FLastFileName: WideString;
  constructor Init;
  destructor Done;
  procedure RefreshMax;
  procedure RemoveByIndex(R: PTableItem; Index: Integer);
  procedure RemoveByItem(R: PTableItem; Item: PTableItem);
  function Add: PTableItem;
  function FindEnd(C: WideChar): PTableItem;
  function FindEl(C: WideChar): PTableItem;
  function AddEnd: PTableItem;
  function AddEl: PTableItem;
  function GetString(var Data; Len: LongInt): PTableItem;
  function GetCodes(var Data; Len: LongInt): PTableItem;
  function GetByCodes(const Codes: RawByteString): PTableItem;
  procedure ClearNodes(RootNode: PTableItem);
  procedure ClearAll;
  procedure MoveUpByItem(R, Item: PTableItem);
  function DataToString(var Data; Len, Align: LongInt; CRLF: Boolean): WideString;
  function StringToData(S: WideString; Align: LongInt; CRLF: Boolean): RawByteString;
  procedure LoadFromStream(Stream: TStream; CodePage: LongWord);
  procedure LoadFromList(List: TWideStringList);
  procedure AddFromStringList(List: TWideStringList; Mode: TTableAddMode = taTBL);
  procedure LoadFromFile(const FileName: WideString; CodePage: LongWord);
  procedure SaveToList(List: TWideStringList; RootNode: PTableItem);
  procedure SaveToStream(Stream: TStream; CodePage: LongWord; WithBOM: Boolean = True);
  procedure SaveToFile(const FileName: WideString; CodePage: LongWord);
  function AnsiTbName: String;
 end;
 PBlock = ^TBlock;
 TBlock = record
  blStart: LongInt;
  blCount: LongInt;
//  blUsed: Boolean;
  Next: PBlock;
 end;
 PBlocks = ^TBlocks;
 TBlocks = object
  Root, Cur: PBlock;
  Count: Integer;
  constructor Init;
  function Add: PBlock;
  destructor Done;
  procedure Clear;
  procedure Remove(Item: PBlock);
  procedure MoveUp(Item: PBlock);
 end;
 TCoding = (cdANSI, cdUNICODE, cdSHIFTJIS);

type
 TParamType = (ptByte, ptWord, ptShortInt, ptSmallInt, ptLongInt);
const
 CParamSize: array[TParamType] of Byte =
 (1, 2, 1, 2, 4);
 PAR_BIGENDIAN = 1;
 PAR_AUTOSIZE = 2;
type
 PParameter = ^TParameter;
 TParameter = object
  Owner: PDynamic;
  paName: String;
  paBefore: Boolean;
  paParamType: TParamType;
  paFlags: LongInt;
  constructor Init;
  destructor Done;
 end;
 PPointerTable = ^TPointerTable;
 PPointer = ^TPointer;
 TPointer = record
  pnPos: LongInt;
  pnPtr: LongInt;

  pnTable1: PTable;
  pnTable2: PTable;
  pnReference: LongInt;
  pnFixed: Boolean;
  pnInsertPos: LongInt;
  pnInsertRef: LongInt;
  pnParamData: RawByteString;
  pnStrings: PKStrings;
  Next: PPointer;
 end;
 PPointers = ^TPointers;
 TPointers = object
  Root, Cur: PPointer;
  Count: Integer;
  constructor Init;
  function Add: PPointer;
  function Get(I: Integer): PPointer;
  function GetByPos(Pos: LongInt): PPointer;
  destructor Done;
  procedure Remove(Item: PPointer);
  procedure MoveUp(Item: PPointer);
  procedure Clear;
 end;
 PParameters = ^TParameters;
 TParameters = object(TDynamics)
  Before: LongInt;
  AfterFix: LongInt;
  BeforeFix: LongInt;
  constructor Init;
  function Add: PParameter;
  destructor Done;
  procedure Clear;
  function Find(const Name: String): PParameter;
  procedure Remove(Item: PParameter);
 end;
 PInserter = ^TInserter;
 TInserter = record
  piReference: LongInt;
  piPointerSize: LongInt;
  piStringLength: LongInt;
  piAlignment: LongInt;
  piCharSize: LongInt;
  piShiftLeft: LongInt;
  piAutoPtrStart: Boolean;
  piPunType: Boolean;
  piSnesLoRom: Boolean;
 end;
 PGroup = ^TGroup;
 TPointerTable = object
  Owner: PDynamic;
  OwnedBy: PGroup;
  ptName: WideString;
  ptTable1: PTable;
  ptTable2: PTable;
  ptDictionary: PGroup;
  ptReference: LongInt;
  FptStringLength: LongInt;
  FptPointerSize: LongInt;
  FptMultiple: LongInt;
  FptAlign: LongInt;
  FptInterval: LongInt;
  FptShiftLeft: LongInt;
  ptMotorola: Boolean;
  ptSigned: Boolean;
  ptPunType: Boolean;
  ptSnesLoROM: Boolean;
  ptPtrToPtr: Boolean;
  ptAutoStart: Boolean;
  ptSeekSame: Boolean;
  ptNotInSource: Boolean;
  ptParameters: PParameters;
  ptPointers: PPointers;
  ptInserter: PInserter;
  ptItemIndex: LongInt;
  FLastPointer: PPointer;
  constructor Init;
  destructor Done;
  function FillParamData(ROM: PBytes; Index: LongInt): RawByteString;
  function GetPointerPun
(ROM: PBytes; pnPos, C, BlockStart: LongInt; var pnReference: LongInt): LongInt;
  function GetPointer
(var Data; pnPos, BlockStart: LongInt; var pnReference: LongInt): LongInt;
  procedure SetPointer(ROM: PBytes; X, pnPos, pnPtr, pnReference: LongInt);
  procedure SetPointerPun(ROM: PBytes; X, pnPos, pnPtr, pnReference: LongInt);
  function AnsiPtName: String;
  procedure ReloadFromStringList(List: UnicodeUtils.TWideStrings; var Index: LongInt);
  procedure SaveToStringList(List: UnicodeUtils.TWideStrings; Flags: LongInt = 0;
   FromSource: Boolean = False);
  procedure ClearStrings;
  procedure SetStringLength(Value: LongInt);
  procedure SetPointerSize(Value: LongInt);
  procedure SetAlignment(Value: LongInt);
  procedure SetCharSize(Value: LongInt);
  procedure SetShl(Value: LongInt);
  function GetDestRef: LongInt;
  function GetDestPtrSz: LongInt;
  function GetDestStrLen: LongInt;
  function GetDestMult: LongInt;
  function GetDestCharSz: LongInt;
  function GetDestShl: LongInt;
  function GetDestAPS: Boolean;
  function GetDestPun: Boolean;
  function GetDestLoRom: Boolean;
  procedure SetDestRef(Value: LongInt);
  procedure SetDestPtrSz(Value: LongInt);
  procedure SetDestStrLen(Value: LongInt);
  procedure SetDestMult(Value: LongInt);
  procedure SetDestCharSz(Value: LongInt);
  procedure SetDestShl(Value: LongInt);
  procedure SetDestAPS(Value: Boolean);
  procedure SetDestPun(Value: Boolean);
  procedure SetDestLoRom(Value: Boolean);

  property ptStringLength: LongInt read FPtStringLength write SetStringLength;
  property ptPointerSize: LongInt read FptPointerSize write SetPointerSize;
  property ptAlignment: LongInt read FptMultiple write SetAlignment;
  property ptCharSize: LongInt read FptAlign write SetCharSize;
  property ptInterval: LongInt read FptInterval write FptInterval;
  property ptShiftLeft: LongInt read FptShiftLeft write SetShl;

  property ptDestReference: LongInt read GetDestRef write SetDestRef;
  property ptDestPointerSize: LongInt read GetDestPtrSz write SetDestPtrSz;
  property ptDestStringLength: LongInt read GetDestStrLen write SetDestStrLen;
  property ptDestAlignment: LongInt read GetDestMult write SetDestMult;
  property ptDestCharSize: LongInt read GetDestCharSz write SetDestCharSz;
  property ptDestShiftLeft: LongInt read GetDestShl write SetDestShl;
  property ptDestAutoPtrStart: Boolean read GetDestAPS write SetDestAPS;
  property ptDestPunType: Boolean read GetDestPun write SetDestPun;
  property ptDestLoRom: Boolean read GetDestLoRom write SetDestLoRom;

  function GetStringByIndex(Index: Integer): PString;
  function ConvertString(TBL: PTable; const S: RawByteString;
            CRLF: Boolean): WideString;
 end;
 PPointerTables = ^TPointerTables;
 TPointerTables = object(TDynamics)
  constructor Init;
  function Add(Owner: PGroup): PPointerTable;
  destructor Done;
  procedure Clear;
  procedure Remove(Item: PPointerTable);
  function GetIndex(Item: PPointerTable): Integer;
  function Get(Index: Integer): PPointerTable;
  function Find(Name: WideString): PPointerTable;
 end;
 PTables = ^TTables;
 TTables = object(TDynamics)
  constructor Init;
  function Add: PTable;
  destructor Done;
  procedure Clear;
  function Find(Name: WideString): PTable;
  function GetIndex(Item: PTable): Integer;
  function Get(Index: Integer): PTable;
  procedure Remove(Item: PTable);
 end;
 TDescription = function: RawByteString; stdcall;
 TGetMethod = function: TMethod; stdcall;
 TSetVariables = procedure(ROM: PBytes; Size: LongInt;
   EndsRoot: PTableItem; MaxCodes, Align: LongInt); stdcall;
 TNeedEnd = function: LongBool; stdcall;
 TGetData = function(TextStrings: PTextStrings): RawByteString; stdcall;
 TGetStrings = function
 (X, Size: LongInt): PTextStrings; stdcall;
 TDisposeStrings = procedure(TextStrings: PTextStrings); stdcall;
 PDLL = ^TDLL;
 TDLL = record
  Name: String;
  Handle: THandle;
  Description: TDescription;
  NeedEnd: TNeedEnd;
  GetMethod: TGetMethod;
  SetVariables: TSetVariables;
  GetData: TGetData;
  GetStrings: TGetStrings;
  DisposeStrings: TDisposeStrings;
  Next: PDLL;
 end;
 PDLLs = ^TDLLs;
 TDLLs = object
  Root, Cur: PDLL;
  Count: Integer;
  constructor Init;
  destructor Done;
  function Add(const FName: String): PDLL;
  function Get(const FName: String): PDLL;
  procedure GetByMethod(List: TStrings);
  procedure Remove(Item: PDLL);
  function MethodGet(M: TMethod): PDLL;
 end;
 TGroup = object
  Owner: PDynamic;
  grName: WideString;
  grDLLName: String;
  grBlocks: PBlocks;
  grPointerTables: PPointerTables;
  grCurPtrTable: PPointerTable;
  grIsDictionary: Boolean;
  grMaxWordSize: LongInt;
  grDictTable: PTable;
  grDictMotorola: Boolean;
  grDictItemStart: LongInt;
  grDictItemSize: Byte;
  grDictItemFirstBytes: RawByteString;
  constructor Init;
  destructor Done;
  function AnsiGrName: String;
  procedure ReloadFromStringList(List: UnicodeUtils.TWideStrings; var Index: LongInt);
  procedure SaveToStringList(List: UnicodeUtils.TWideStrings; Flags: LongInt = 0;
   FromSource: Boolean = False);
  procedure ClearStrings;
 end;
 PGroups = ^TGroups;
 TGroups = object(TDynamics)
  constructor Init;
  function Add: PGroup;
  destructor Done;
  procedure Clear;
  procedure Remove(Item: PGroup);
  function Find(Name: WideString): PGroup;
  function GetIndex(Item: PGroup): Integer;
  function Get(Index: Integer): PGroup;
 end;
const
 KP_CRLF_CONVERT  = 1;
 KP_NOEND_DIC_TBL = 1 shl 1;
type
 PKruptarProject =^TKruptarProject;
 TKruptarProject = object
  Owner: PDynamic;
  fNamed: Boolean;
  fSaved: Boolean;
  InputROM: String;
  OutputROM: String;
  ProjectFile: WideString;
  EmulSelected: WideString;
  Dir: WideString;
  Groups: PGroups;
  Tables: PTables;
  CodePageIndex: LongInt;
  Flags: LongInt;
  ROM: PBytes;
  ROMSize: LongInt;
  constructor Init;
  function Save: Boolean;
  function Open(const FileName: WideString; Console: Boolean = False): Boolean;
  function LoadROM(ConsoleMode: Boolean = false;
  FileName: String = ''; NewSize: LongInt = -1): Boolean;
  procedure FreeROM;
  destructor Done;
  function Open6(const FileName: WideString; Console: Boolean = False): Boolean;
  function GetCodePage: LongWord;
  procedure SetCodePage(Value: LongWord);
  procedure ReloadStrs(PointerTable: PPointerTable; DLL: PDLL);
  function CheckFile(const Path: WideString; const FileName: RawByteString;
    Console, ExceptIfNotLoad: Boolean; CP: LongWord = 0): String;

  property CodePage: LongWord read GetCodePage write SetCodePage;
 end;
 PProjects = ^TProjects;
 TProjects = object(TDynamics)
  constructor Init;
  function Add: PKruptarProject;
  destructor Done;
  procedure Remove(Item: PKruptarProject);
  function Find(Name: WideString): PKruptarProject;
  function GetIndex(KP: PKruptarProject): Integer;
 end;
 TErrorType = (etNone, etHint, etWarning, etError, etFatalError);
 TTreeStateType =
 (tsNone, tsProject, tsGroup, tsDir, tsTable, tsBlock, tsFile, tsPtrTable,
  tsTables, tsGroups, tsBlocks, tsPtrTables, tsEnds, tsEls, tsCodes, tsPointer,
  tsTableItem);
const
 Icons: array[TTreeStateType] of ShortInt =
 (-1, //tsNone
  0,  //tsProject
  3,  //tsGroup
  26, //tsDir
  23, //tsTable
  5,  //tsBlock
  5,  //tsFile
  7,  //tsPtrTable
  -1, -1, -1, -1, -1, -1, -1, -1,
  24  //tsTableItem
  );
 StatesReadOnly: array[TTreeStateType] of Boolean =
 (True, True, False, True, False, False, True, False,
  True, True, True, True, True, True, True, False, False);
type
 TValueType =
 (vtByte, vtWord, vtShortInt, vtSmallInt, vtInteger,
  vtHex, vtString, vtFileName, vtDynamic, vtGroup, vtDynamics);
 PValue = ^TValue;
 TValue = record
  Name: String;
  ValueType: TValueType;
  CanEdit: Boolean;
  Next: PValue;
  Min: LongInt;
  Max: LongInt;
  MatchCase: Boolean;
  Case TValueType of
   vtByte: (ValByte: ^Byte);
   vtWord: (ValWord: ^Word);
   vtShortInt: (ValShortInt: ^ShortInt);
   vtSmallInt: (ValSmallInt: ^SmallInt);
   vtInteger, vtHex: (ValInteger: ^LongInt);
   vtString, vtFileName: (ValString: ^String);
   vtDynamic: (ValPointer: ^Pointer;
               Dynamics: PDynamics);
   vtGroup: (ValGroup: ^Pointer;
             Groups: PGroups;
             Grp: PGroup);
   vtDynamics: (ValDynamics: PDynamics);
 end;
 PValues = ^TValues;
 TValues = object
  Proj: PKruptarProject;
  Root, Cur: PValue;
  Count: Integer;
  constructor Init;
  function Add: PValue;
  function Get(I: Integer): PValue;
  destructor Done;
  procedure Clear;
  procedure FillValueListBox(Node: TKruptarTreeNodeType);
 end;

type
 TWriteProc = procedure(Str: String);

var
 ErrorStr: array[TErrorType] of WideString;

var
 Projects: PProjects = NIL;
 Values: PValues = NIL;
 DLLs: PDLLs = NIL;
 CanCheck: Boolean = False;
 NotChange: Boolean = False;
 SelRoot: PKruptarProject = NIL;
 CurGroup: PGroup = NIL;
 MemoCanChange: Boolean = False;
 CurString: PString = NIL;
 CurPointer: PPointer = NIL;
 SelectedGroup: PGroup = NIL;
 SelectedList: PPointerTable = NIL;
 NormalEnter: Boolean = False;
 DblClickPressed: Boolean = False;
 ExeDir: WideString = '';
 AddStateString: procedure (ErrorType: TErrorType;
                            const FileName: WideString; Line: Integer;
                            const Txt: WideString; Mess: Boolean = False);
 ConsoleWrite: TWriteProc;
 ConsoleWriteln: TWriteProc;

function HexDataToString(const S: RawByteString): WideString;
function WordMotorola(X: DWord): LongInt;
function TripleMotorola(X: DWord): LongInt;
function DWordMotorola(X: DWord): LongInt;

function GroupAddToRom(GRP: Pointer; Project: PKruptarProject): Boolean;
function StringsToTextStrings(Table: Pointer; Strings: Pointer;
                           Align: LongInt; KP: PKruptarProject): PTextStrings;
procedure LoadDlls;

type
 TProgressInitProc = procedure(const LName: WideString; MaxVal: Integer);
 TIncProgressProc  = procedure(IncValue: Integer);
 TInsertErrorProc = procedure(const LName: WideString; Size: Integer);
var
 GroupProgressInitProc: TProgressInitProc;
 PtrTableProgressInitProc: TProgressInitProc;
 IncPtrTableProgressProc: TIncProgressProc;
 IncGroupProgressProc: TIncProgressProc;
 ProgressInsertErrorProc: TInsertErrorProc;

implementation

uses
  k7Hexunit, AddPtrsDialog, ParamDlg, CommCtrl, MyUtils,
  ProgressDialogUnit, DictDlg, WordsDlg, ReplaceText, SearchText, DllsDialog,
  insertresultdlg, about, LocMsgBox, MyClasses, MyClassesEx;

{$R *.dfm}

procedure SetRecentFileName(Item: TKruptarMenuItem; const FileName: WideString);
begin
 with Item do Caption := '&' + IntToStr(Tag) + ' ' + FileName;
end;

function GetRecentFileName(Item: TKruptarMenuItem): WideString;
begin
 Result := Item.Caption;
 Delete(Result, 1, 3);
end;

var
 CodePageList: TCodePageList;

function ParseStr(List: TWideStrings; var Index: LongInt): WideString;
var
 S: WideString;
begin
 Result := '';
 if List <> nil then
 repeat
  if Index >= List.Count then
   S := sStringEnd else
   S := List[Index];
  Inc(Index);
  if S = sStringEnd then
  begin
   Inc(Index);
   Break;
  end else Result := Result + S + sLineBreak;
 until False;
 if Result <> '' then SetLength(Result, Length(Result) - Length(sLineBreak));
end;

procedure InFormIncGroupProgress(IncValue: Integer);
begin
 with MyProgressDialog do
 begin
  if not Visible then Show;
  GroupProgress := GroupProgress + IncValue;
 end;
end;

procedure InFormProgressInsertError(const LName: WideString; Size: Integer);
begin
 InsertResultDialog.Memo.Lines.Add(WideFormat(K7S(SMUSpaceLeft),
 [LName, Size, Size,
 DeclinateNumeralString(Size,
 K7S(SMUBytesCount1), K7S(SMUBytesCount2), K7S(SMUBytesCount3))
 ]));
end;

procedure InFormGroupProgressInit(const LName: WideString; MaxVal: Integer);
begin
 ProgressCanClose := False;
 with MyProgressDialog do
 begin
  GroupLabel.Caption := LName;
  GroupProgress := 0;
  GroupGauge.MaxValue := MaxVal;
 end;
end;

procedure InFormIncPtrTableProgress(IncValue: Integer);
begin
 with MyProgressDialog do
 begin
  if not Visible then Show;
  PtrTableProgress := PtrTableProgress + IncValue;
 end;
end;

procedure InFormPtrTableProgressInit(const LName: WideString; MaxVal: Integer);
begin
 with MyProgressDialog do
 begin
  PtrTableLabel.Caption := LName;
  PtrTableProgress := 0;
  PtrTableGauge.MaxValue := MaxVal;
 end;
end;

function TKStrings.Get(I: Integer): PString;
var
 N: PString;
 J: Integer;
begin
 N := Root;
 J := 0;
 while N <> NIL do
 begin
  if J = I then
  begin
   Result := N;
   Exit;
  end;
  Inc(J);
  N := N.Next;
 end;
 Result := NIL;
end;

constructor TKStrings.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TKStrings.Add: PString;
begin
 New(Result);
 if Root = NIL then Root := Result else Cur^.Next := Result;
 Cur := Result;
 Inc(Count);
 Result.stOriginal := 'el';
 Result.stNew := '';
 Result.Next := NIL;
end;

destructor TKStrings.Done;
var
 N: PString;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Root.stOriginal := '';
  Root.stNew := '';
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;


procedure TTable.RefreshMax;
var
 R: PTableItem;
begin
 R := Root; CMaxCodes := 0; CMaxChars := 0;
 while R <> NIL do with R^ do
 begin
  if CMaxCodes < Length(tiCodes) then
   CMaxCodes := Length(tiCodes);
  if CMaxChars < Length(tiString) then
   CMaxChars := Length(tiString);
  R := Next;
 end;
 R := EndRoot; EndMaxCodes := 0;
 while R <> NIL do with R^ do
 begin
  if EndMaxCodes < Length(tiCodes) then
   EndMaxCodes := Length(tiCodes);
  R := Next;
 end;
 R := ElRoot; ElMaxCodes := 0;
 while R <> NIL do with R^ do
 begin
  if ElMaxCodes < Length(tiCodes) then
   ElMaxCodes := Length(tiCodes);
  R := Next;
 end;
end;

constructor TKruptarProject.Init;
begin
 Flags := KP_CRLF_CONVERT;
 fNamed := False;
 fSaved := False;
 Dir := '';
 InputROM := '';
 OutputROM := '';
 ProjectFile := '';
 New(Groups, Init);
 New(Tables, Init);
 CodePage := GetACP;
 ROM := NIL;
 ROMSize := 0;
end;

function TKruptarProject.LoadROM(ConsoleMode: Boolean; FileName: String;
  NewSize: LongInt): Boolean;
var
 F: File;
 R: LongInt;
begin
 if ROMSize > 0 then
 begin
  FreeMem(ROM);
  ROM := NIL;
  ROMSize := 0;
 end;
 if FileName = '' then
 begin
  Result := FieldFileExists(KP_SRC_ROM, InputRom, ConsoleMode);
  if not Result then Exit;
  FileName := InputROM;
 end;
 try
  Assignfile(F, FileName);
  Reset(F, 1);
  try
   ROMSize := FileSize(F);
   if NewSize > RomSize then RomSize := NewSize;
   GetMem(ROM, ROMSize);
   FillChar(ROM^, ROMSize, 0);
   BlockRead(F, ROM^, ROMSize, R);
   Result := True;
  finally
   CloseFile(F);
  end;
 except
  AddStateString(etError, '', 0,
   WideFormat(K7S(SMUUnableToLoad), [FileName]),
   ConsoleMode);
  Result := False;
 end;
end;

procedure TKruptarProject.FreeROM;
begin
 if ROMSize > 0 then
 begin
  FreeMem(ROM);
  ROM := NIL;
  ROMSize := 0;
 end;
end;

destructor TKruptarProject.Done;
begin
 if Groups <> NIL then Dispose(Groups, Done);
 if Tables <> NIL then Dispose(Tables, Done);
 FreeROM;
 fNamed := False;
 fSaved := False;
 Dir := '';
 InputROM := '';
 OutputROM := '';
 ProjectFile := '';
 EmulSelected := '';
 Groups := NIL;
 Tables := NIL;
 ROM := NIL;
 ROMSize := 0;
end;

procedure AddStateStringWin(ErrorType: TErrorType; const FileName: WideString;
  Line: Integer; const Txt: WideString; Mess: Boolean = False);
var
 S: WideString;
 MsgDlgType: TMsgDlgType;
begin
 with MainForm.StatesMemo do
 begin
  if not Mess then
  begin
   case ErrorType of
    etHint: MsgDlgType := mtInformation;
    etWarning: MsgDlgType := mtWarning;
    else MsgDlgType := mtError;
   end;
   LocMessageDlg(Txt, MsgDlgType, [mbOk], 0);
  end else
  begin
   if FileName = '' then
   begin
    if Line < 0 then
     S := Txt else
     S := WideFormat('(%s%d): %s', [K7S(smuLineIndex), Line + 1, Txt]);
   end else
   begin
    if Line < 0 then
     S := WideFormat('%s: %s', [FileName, Txt]) else
     S := WideFormat('%s (%s%d): %s',[FileName, K7S(smuLineIndex), Line + 1, Txt]);
   end;
   if ErrorType <> etNone then
    S := WideFormat('[%s] %s', [ErrorStr[ErrorType], S]);
   Lines.Add(S);
   if not Visible then Visible := True;
  end;
 end;
end;

constructor TPointerTables.Init;
begin
 inherited Init(SizeOf(TPointerTable));
end;

function TPointerTables.Add(Owner: PGroup): PPointerTable;
begin
 inherited Add;
 New(Result, Init);
 Cur.Data := Result;
 Result.Owner := Cur;
 Result.OwnedBy := Owner;
end;

destructor TPointerTables.Done;
begin
 Clear;
end;

constructor TGroup.Init;
begin
 grName := '';
 grDLLName := '';
 grBlocks := NIL;
 grIsDictionary := False;
 grMaxWordSize := 2;
 grDictTable := NIL;
 grDictMotorola := False;
 grDictItemStart := 0;
 grDictItemSize := 1;
 grDictItemFirstBytes := '';
 grCurPtrTable := NIL;
 New(grBlocks, Init);
 New(grPointerTables, Init);
end;

destructor TGroup.Done;
begin
 if grBlocks <> NIL then Dispose(grBlocks, Done);
 if grPointerTables <> NIL then Dispose(grPointerTables, Done);
 grName := '';
 grDLLName := '';
 grBlocks := NIL;
 grPointerTables := NIL;
 grCurPtrTable := NIL;
end;

constructor TProjects.Init;
begin
 inherited Init(SizeOf(TKruptarProject));
end;

function TProjects.Add: PKruptarProject;
begin
 inherited Add;
 New(Result, Init);
 Cur.Data := Result;
 Result.Owner := Cur;
end;

destructor TProjects.Done;
var
 N: PDynamic;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  with Root^ do if Data <> NIL then Dispose(PKruptarProject(Data), Done);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
 DataSize := 0;
end;

constructor TGroups.Init;
begin
 inherited Init(SizeOf(TGroup));
end;

function TGroups.Add: PGroup;
begin
 inherited Add;
 New(Result, Init);
 Cur.Data := Result;
 Result.Owner := Cur;
end;

destructor TGroups.Done;
begin
 Clear;
end;

procedure TMainForm.PSplitCanResize(Sender: TObject; var NewSize: Integer;
    var Accept: Boolean);
begin
 if NewSize < StatesMemo.Height + 32 then
 begin
  NewSize := StatesMemo.Height + 32;
  Accept := False;
 end
end;

procedure TMainForm.SplitCanResize(Sender: TObject; var NewSize: LongInt;
    var Accept: Boolean);
begin
 if NewSize < 48 then
 begin
  NewSize := 48;
  Accept := False;
 end
end;

procedure TMainForm.NewSplitCanResize(Sender: TObject; var NewSize: LongInt;
    var Accept: Boolean);
begin
 if NewSize < 48 then
 begin
  NewSize := 48;
  Accept := False;
 end else
 begin
  if NewSplit.Left <> ValueListBox.Left then
  NewSplit.Left := ValueListBox.Left;
 end;
end;

procedure WideGetDir(D: Byte; var S: WideString);
var
{$IFNDEF FORCED_UNICODE}
 Drive: array[0..3] of WideChar;
 DirBuf, SaveBuf: array[0..MAX_PATH] of WideChar;
{$ENDIF}
 Temp: String;
begin
{$IFNDEF FORCED_UNICODE}
 if Win32PlatformIsUnicode then
 begin
  if D <> 0 then
  begin
   Drive[0] := WideChar(D + Ord('A') - 1);
   Drive[1] := ':';
   Drive[2] := #0;
   GetCurrentDirectoryW(SizeOf(SaveBuf), SaveBuf);
   SetCurrentDirectoryW(Drive);
  end;
  GetCurrentDirectoryW(SizeOf(DirBuf), DirBuf);
  if D <> 0 then SetCurrentDirectoryW(SaveBuf);
  S := DirBuf;
 end else
{$ENDIF}
 begin
  GetDir(D, Temp);
  S := Temp;
 end;
end;

function FSplit(const S: WideString; var Dir: WideString): WideString;
var
 D: WideString;
begin
 D := WideExtractFilePath(S);
 if D = '' then
 begin
  WideGetDir(0, D);
  if D[Length(D)] <> PathDelim then D := D + PathDelim;
 end;
 Result := WideExtractFileName(S);
 Dir := D + Result;
 if Dir = D + Result then
  Dir := D else
  Dir := WideExtractShortPathName(D);
end;

function GetExt(const Src: WideString; var Dst: WideString): WideString;
begin
 Dst := Src;
 Result := WideUpperCase(WideExtractFileExt(Src));
 SetLength(Dst, Length(Dst) - Length(Result));
end;

procedure TPointers.Clear;
var
 N: PPointer;
begin
 while Root <> NIL do
 begin
  N := Root^.Next;
  with Root^ do
  begin
   pnParamData := '';
   if pnStrings <> NIL then Dispose(pnStrings, Done);
  end;
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

procedure TPointerTable.SetPointerPun(ROM: PBytes;
  X, pnPos, pnPtr, pnReference: LongInt);
var
 I, SZ: LongInt;
 B: PByte;
begin
 Dec(pnPtr, pnReference);
 pnPtr := pnPtr shr ptDestShiftLeft;
 if ptDestLoRom then
  pnPtr := (pnPtr and $7FFF) or ((pnPtr shr 15) shl 16) or $8000;
 if ptDestPointerSize > 4 then
  SZ := ptDestPointerSize - 4 else
  SZ := ptDestPointerSize;
 if ptMotorola then Case ptDestPointerSize of
  2, 6: pnPtr := WordMotorola(pnPtr);
  3: pnPtr := TripleMotorola(pnPtr);
  4: pnPtr := DWordMotorola(pnPtr);
 end;
 if SZ <= 2 then
 begin
  B := Addr(pnPtr);
  for I := 0 to SZ - 1 do
  begin
   ROM^[pnPos + I * X] := B^;
   Inc(DWord(B));
  end;
 end else if ptMotorola then
 begin
  case ptDestPointerSize of
   3:
   begin
    ROM[pnPos] := pnPtr;
    Word(Addr(ROM[pnPos + X])^) := pnPtr shr 8;
   end;
   4:
   begin
    Word(Addr(ROM[pnPos])^) := pnPtr;
    Word(Addr(ROM[pnPos + X])^) := pnPtr shr 16;
   end;
  end;
 end else
 begin
  Word(Addr(ROM[pnPos])^) := pnPtr;
  case ptDestPointerSize of
   3: ROM[pnPos + X] := pnPtr shr 16;
   4: Word(Addr(ROM[pnPos + X])^) := pnPtr shr 16;
  end;
 end;
end;

procedure TPointerTable.SetPointer(ROM: PBytes;
   X, pnPos, pnPtr, pnReference: LongInt);
var
 SZ: LongInt;
begin
 Dec(pnPtr, pnReference);
 if ptDestPointerSize = 6 then
  LongInt(pnPtr) := SmallInt(pnPtr) else
 if ptDestPointerSize = 5 then
  LongInt(pnPtr) := ShortInt(pnPtr);
 if ptDestAutoPtrStart then Dec(pnPtr, X);
 pnPtr := pnPtr shr ptDestShiftLeft;
 if ptDestLoRom then
  pnPtr := (pnPtr and $7FFF) or ((pnPtr shr 15) shl 16) or $8000;
 if ptMotorola then Case ptDestPointerSize of
  2, 6: pnPtr := WordMotorola(pnPtr);
  3: pnPtr := TripleMotorola(pnPtr);
  4: pnPtr := DWordMotorola(pnPtr);
 end;
 if ptDestPointerSize > 4 then
  SZ := ptDestPointerSize - 4 else
  SZ := ptDestPointerSize;
 Move(pnPtr, ROM[pnPos], SZ);
end;

type
 TSearchOption = (soMatchCase, soOriginal, soPromt);
 TSearchOptions = set of TSearchOption;
 TSearchTextRec = record
  CanSearch: Boolean;
  Replace: Boolean;
  ReplaceAll: Boolean;
  SearchString: WideString;
  ReplaceString: WideString;
  Options: TSearchOptions;
 end;

var
 SearchTextRec: TSearchTextRec;

procedure LoadDlls;
var
 SR: TSearchRecW;
begin
 if WideFindFirst(ExeDir + 'Lib' + PathDelim + '*.kpl', $21, SR) = 0 then
 try
  with DLLs^ do
  repeat
   if not FileExists(ExeDir + 'Lib' + PathDelim + SR.Name) then
    Add(PWideChar(Addr(SR.FindData.cAlternateFileName))) else
    Add(SR.Name);
  until WideFindNext(SR) <> 0;
 finally
  WideFindClose(SR);
 end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
 Ini: TStreamIniFileW;
 FS: LongInt; FntStyle: TFontStyles absolute FS;
 Section, SW: WideString;
 I, J: Integer;
 RecentArray: array[1..4] of TKruptarMenuItem;
begin
 AllSaved := True;
 MsgDlgLocalizer := Localizer;
 ConstsLocalizer := Localizer;
 InsertMode := True;
 ExeDir := WideExtractFilePath(WideParamStr(0));
 try
  Ini := TStreamIniFileW.Create(WideChangeFileExt(WideParamStr(0), '.cfg'));
  try
   Section := 'Main';
   FLang := Ini.ReadString(Section, 'Language', 'English');
   with FontDialog, Font do
   begin
    Name := Ini.ReadString(Section, 'FontName', 'Courier New');
    Height := Ini.ReadInteger(Section, 'FontHeight', -13);
    Size := Ini.ReadInteger(Section, 'FontSize', 10);
    Color := Ini.ReadInteger(Section, 'FontColor', 0);
    Pitch := TFontPitch(Ini.ReadInteger(Section, 'FontPitch', 0));
    FS := Ini.ReadInteger(Section, 'FontStyle', 0);
    Style := FntStyle;
    OriginalMemo.Font := Font;
    TranslateMemo.Font := Font;
    ListBox.Font := Font;
   end;
   ProgressAutoClose := Ini.ReadBool(Section, 'InsertProgressAutoClose', False);
   (* Read recent opened files *)
   RecentArray[1] := FileRecent1Item;
   RecentArray[2] := FileRecent2Item;
   RecentArray[3] := FileRecent3Item;
   RecentArray[4] := FileRecent4Item;
   J := 0;
   for I := 1 to 4 do
   begin
    SW := Ini.ReadString(Section, WideFormat('RecentFile%d', [I]), '');
    if WideFileExists(SW) then
    begin
     Inc(J);
     with RecentArray[J] do
     begin
      SetRecentFileName(RecentArray[J], SW);
      Visible := True;
      Enabled := True;
     end;
    end;
   end;
   If J > 0 then FileSeparator2.Visible := True;
  finally
   Ini.Free;
  end;
 except
 end;
 GroupProgressInitProc := InFormGroupProgressInit;
 PtrTableProgressInitProc := InFormPtrTableProgressInit;
 IncPtrTableProgressProc := InFormIncPtrTableProgress;
 IncGroupProgressProc := InFormIncGroupProgress;
 ProgressInsertErrorProc := InFormProgressInsertError;

 WindowState := wsMaximized;
 Caption := Application.Title;
 PanelSplit := TSplitter.Create(MainForm);
 PanelSplit.Parent := Panel;
 PanelSplit.Top := BottomPanel.Top;
 PanelSplit.Align := BottomPanel.Align;
 panelsplit.ResizeStyle := rsUpdate;
 PanelSplit.OnCanResize := PSplitCanResize;

 MemoSplit := TSplitter.Create(MainForm);
 MemoSplit.Parent := BottomPanel;
 MemoSplit.Top := StatesMemo.Top;
 MemoSplit.Align := StatesMemo.Align;
 MemoSplit.ResizeStyle := rsUpdate;
 MemoSplit.OnCanResize := SplitCanResize;

 ListSplit := TSplitter.Create(MainForm);
 ListSplit.Parent := ProjectTabs;
 ListSplit.Left := LeftPanel.Left + LeftPanel.Width + 1;
 ListSplit.Align := LeftPanel.Align;
 ListSplit.ResizeStyle := rsUpdate;
 ListSplit.OnCanResize := SplitCanResize;

 NewSplit := TSplitter.Create(MainForm);
 NewSplit.Parent := LeftPanel;
 NewSplit.Top := TreeView.Top + TreeView.Height + 1;
 NewSplit.Align := ValuePalette.Align;
 NewSplit.ResizeStyle := rsUpdate;
 NewSplit.OnCanResize := SplitCanResize;

 SelectedRoot := NIL;
 LoadDLLs;
 DragAcceptFiles(Handle, True);
 TreeView.DoubleBuffered := True;
 ListBox.DoubleBuffered := False;
 BottomPanel.DoubleBuffered := True;
 OriginalMemo.DoubleBuffered := True;
 TranslateMemo.DoubleBuffered := True;
 SearchTextRec.SearchString := '';
 SearchTextRec.Options := [];
 CodePageList := MakeCodePageList;
end;

procedure TMainForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
 if NewWidth < 256 then NewWidth := 256 else
 begin
  if OriginalPanel.Width = (NewWidth - (LeftPanel.Width + 8)) div 2 then Exit;
  OriginalPanel.Width := (NewWidth - (LeftPanel.Width + 8)) div 2;
 end;
end;

procedure TMainForm.MenuFileExitActionExecute(Sender: TObject);
begin
 Close;
end;

procedure TMainForm.ConfigFontItemClick(Sender: TObject);
begin
 FontDialog.Font := OriginalMemo.Font;
 if FontDialog.Execute then
 begin
  OriginalMemo.Font := FontDialog.Font;
  TranslateMemo.Font := FontDialog.Font;
  ListBox.Font := FontDialog.Font;
 end;
end;

destructor TPointerTable.Done;
begin
 if ptPointers <> NIL then Dispose(ptPointers, Done);
 if ptParameters <> NIL then Dispose(ptParameters, Done);
 ptName := '';
 ptTable1 := NIL;
 ptTable2 := NIL;
 ptPointers := NIL;
 ptParameters := NIL;
end;

function TPointerTable.FillParamData(ROM: PBytes; Index: LongInt): RawByteString;
var
 SZ: LongInt;
begin
 if ptPointerSize < 1 then Result := '' else with ptParameters^ do
 begin
  if ptPointerSize > 4 then
   SZ := ptPointerSize - 4 else
   SZ := ptPointerSize;
  if ptInterval > 0 then
  begin
   SetLength(Result, ptInterval);
   Move(ROM[Index + SZ], Pointer(Result)^, ptInterval - Before);
   Move(ROM[Index - Before], PBytes(Result)[(ptInterval - Before)], Before);
  end;
 end;
end;

constructor TPointerTable.Init;
begin
 ptName := '';
 ptTable1 := NIL;
 ptTable2 := NIL;
 New(ptPointers, Init);
 New(ptParameters, Init);
 ptItemIndex := -1;
 ptStringLength := 0;
 ptReference := 0;
 ptPointerSize := 0;
 ptDictionary := NIL;
 ptAlignment := 1;
 ptCharSize := 1;
 ptInterval := 0;
 ptShiftLeft := 0;
 ptMotorola := False;
 ptSigned := False;
 ptPunType := False;
 ptSnesLoRom := False;
 ptAutoStart := False;
 ptPtrToPtr := False;
 ptSeekSame := True;
 ptNotInSource := False;
 ptInserter := nil;
end;

function TValues.Get(I: Integer): PValue;
var
 N: PValue;
 J: Integer;
begin
 N := Root;
 J := 0;
 while N <> NIL do
 begin
  if J = I then
  begin
   Result := N;
   Exit;
  end;
  Inc(J);
  N := N.Next;
 end;
 Result := NIL;
end;

constructor TValues.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TValues.Add: PValue;
begin
 New(Result);
 if Root = NIL then
  Root := Result else
  Cur.Next := Result;
 Cur := Result;
 Inc(Count);
 FillChar(Result^, SizeOf(TValue), 0);
 with Result^ do
 begin
  Min := 0;
  Max := High(LongInt);
  MatchCase := False;
  Dynamics := NIL;
  CanEdit := True;
 end;
end;

destructor TValues.Done;
begin
 Clear;
end;

procedure TValues.Clear;
var
 N: PValue;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Root.Name := '';
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

procedure TTable.ClearNodes(RootNode: PTableItem);
var
 N: PTableItem;
begin
 if RootNode = Root then
 begin
  Count := 0;
  Root := NIL;
  Cur := NIL;
  CMaxCodes := 0;
  CMaxChars := 0;
 end else
 if RootNode = ElRoot then
 begin
  ElCount := 0;
  ElRoot := NIL;
  ElCur := NIL;
  ElMaxCodes := 0;
 end else
 if RootNode = EndRoot then
 begin
  EndCount := 0;
  EndRoot := NIL;
  EndCur := NIL;
  EndMaxCodes := 0;
 end;
 while RootNode <> NIL do
 begin
  N := RootNode.Next;
  with RootNode^ do
  begin
   tiCodes := '';
   tiString := '';
  end;
  Dispose(RootNode);
  RootNode := N;
 end;
end;

procedure TTable.ClearAll;
begin
 ClearNodes(Root);
 ClearNodes(ElRoot);
 ClearNodes(EndRoot);
end;

destructor TTable.Done;
begin
 tbName := '';
 ClearAll;
end;

function TTable.Add: PTableItem;
begin
 New(Result);
 if Root = NIL then
  Root := Result else
  Cur.Next := Result;
 Cur := Result;
 Inc(Count);
 with Result^ do
 begin
  Next := NIL;
  tiCodes := '';
  tiString := '';
  tiState := isCode;
 end;
end;

function TTable.AddEnd: PTableItem;
begin
 New(Result);
 if EndRoot = NIL then
  EndRoot := Result else
  EndCur.Next := Result;
 EndCur := Result;
 Inc(EndCount);
 with Result^ do
 begin
  Next := NIL;
  tiCodes := '';
  tiString := '';
  tiState := isTerminate;
 end;
end;

function TTable.AddEl: PTableItem;
begin
 New(Result);
 if ElRoot = NIL then
  ElRoot := Result else
  ElCur.Next := Result;
 ElCur := Result;
 Inc(ElCount);
 with Result^ do
 begin
  Next := NIL;
  tiCodes := '';
  tiString := '';
  tiState := isBreak;
 end;
end;

function TTable.GetString(var Data; Len: LongInt): PTableItem;
var
 N: PTableItem;
 J: Integer;
begin
 Result := NIL;
 if Len <= 0 then Exit;
 J := EndMaxCodes;
 if J < ElMaxCodes then J := ElMaxCodes;
 if J < CMaxCodes then J := CMaxCodes;
 if J > Len then J := Len;
 while J > 0 do
 begin
  N := EndRoot;
  while N <> NIL do with N^ do
  begin
   if (J = Length(tiCodes)) and CompareMem(@Data, Pointer(tiCodes), J) then
   begin
    Result := N;
    Exit;
   end;
   N := Next;
  end;
  N := ElRoot;
  while N <> NIL do with N^ do
  begin
   if (J = Length(tiCodes)) and CompareMem(@Data, Pointer(tiCodes), J) then
   begin
    Result := N;
    Exit;
   end;
   N := Next;
  end;
  N := Root;
  while N <> NIL do with N^ do
  begin
   if (J = Length(tiCodes)) and CompareMem(@Data, Pointer(tiCodes), J) then
   begin
    Result := N;
    Exit;
   end;
   N := Next;
  end;
  Dec(J);
 end;
end;

function TTable.GetCodes(var Data; Len: LongInt): PTableItem;
var
 N: PTableItem;
 J: LongInt;
begin
 Result := NIL;
 if Len = 0 then Exit;
 J := CMaxChars;
 if J > Len then J := Len;
 while J > 0 do
 begin
  N := Root;
  while N <> NIL do with N^ do
  begin
   if (J = Length(tiString)) and
      CompareMem(@Data, Pointer(tiString), J shl 1) then
   begin
    Result := N;
    Exit;
   end;
   N := Next;
  end;
  Dec(J);
 end;
end;

function GetByte(var S: WideString): Integer;
var
 A, B: Word;
begin
 Result := -1;
 if Length(S) < 2 then Exit;
 A := Word(S[1]);
 B := Word(S[2]);
 if (((A >= Word('0')) and (A <= Word('9'))) or
     ((A >= Word('A')) and (A <= Word('F'))) or
     ((A >= Word('a')) and (A <= Word('f')))) and
    (((B >= Word('0')) and (B <= Word('9'))) or
     ((B >= Word('A')) and (B <= Word('F'))) or
     ((B >= Word('a')) and (B <= Word('f')))) then
 begin
  Delete(S, 1, 2);
  if A >= Word('a') then Dec(A, Word('a') - 10) else
  if A >= Word('A') then Dec(A, Word('A') - 10) else
  if A >= Word('0') then Dec(A, Word('0'));
  if B >= Word('a') then Dec(B, Word('a') - 10) else
  if B >= Word('A') then Dec(B, Word('A') - 10) else
  if B >= Word('0') then Dec(B, Word('0'));
  Result := (A shl 4) or B;
 end;
end;

procedure TTable.LoadFromFile(const FileName: WideString; CodePage: LongWord);
var
 Stream: TStream;
begin
 Stream := TWideFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
 try
  FLastFileName := FileName;
  LoadFromStream(Stream, CodePage);
 finally
  Stream.Free;
 end;
end;

procedure TTable.LoadFromStream(Stream: TStream; CodePage: LongWord);
var
 List: TCodePagedStringList;
begin
 List := TCodePagedStringList.Create(CodePage);
 try
  List.LoadFromStream_BOM(Stream, False);
  LoadFromList(List);
 finally
  List.Free;
 end;
end;

procedure TTable.SaveToFile(const FileName: WideString; CodePage: LongWord);
var
 Stream: TStream;
begin
 Stream := TWideFileStream.Create(FileName, fmCreate);
 try
  SaveToStream(Stream, CodePage);
 finally
  Stream.Free;
 end;
end;

procedure TTable.SaveToStream(Stream: TStream; CodePage: LongWord;
 WithBOM: Boolean);
var
 List: TCodePagedStringList;
begin
 List := TCodePagedStringList.Create(CodePage);
 try
  List.SaveWithBOM := WithBOM;
  SaveToList(List, Root);
  SaveToList(List, ElRoot);
  List.Add('ends');
  SaveToList(List, EndRoot);
  List.SaveToStream_BOM(Stream, False);
 finally
  List.Free;
 end;
end;

constructor TBlocks.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TBlocks.Add: PBlock;
begin
 New(Result);
 if Root = NIL then
  Root := Result else
  Cur.Next := Result;
 Cur := Result;
 Inc(Count);
 with Result^ do
 begin
  blStart := 0;
  blCount := 1;
//  blUsed := False;
  Next := NIL;
 end;
end;

destructor TBlocks.Done;
begin
 Clear;
end;

constructor TTables.Init;
begin
 inherited Init(SizeOf(TTable));
end;

function TTables.Add: PTable;
begin
 inherited Add;
 New(Result, Init);
 Cur^.Data := Result;
 Result^.Owner := Cur;
end;

destructor TTables.Done;
begin
 Clear;
end;

type
 TDatHeader = array[0..7] of AnsiChar;

const
 DatHeader: TDatHeader = 'KRUPTAR7';

function TKruptarProject.Save: Boolean;

 var
  MainStream: TStream;

(* Local functions *)
 procedure WriteLenString(const S: RawByteString);
 var
  L: LongInt;
 begin
  L := Length(S);
  MainStream.WriteBuffer(L, 4);
  if L > 0 then
   MainStream.WriteBuffer(Pointer(S)^, L);
 end;

 procedure WriteString(const S: RawByteString);
 var
  Tag: LongInt;
 begin
  Tag := 1;
  MainStream.WriteBuffer(Tag, 1);
  WriteLenString(S);
 end;

 procedure WriteWideString(const S: WideString);
 var
  Tag: LongInt;
 begin
  Tag := 1;
  MainStream.WriteBuffer(Tag, 2);
  WriteLenString(UTF16toUTF8(S));
 end;

 procedure WriteInt(X: LongInt);
 begin
  MainStream.WriteBuffer(X, 4);
 end;

(* End of local functions *)

 var
  Zipper: TKaZIP;
  List: TCodePagedStringList;
  ZStream, Stream: TStream;
  P, PP, PAR: PDynamic;
  B: PBlock;
  PTR: PPointer;
  STR: PString;
  SW: WideString;
  ZStreamCreated: Boolean;

(* TKruptarProject.Save *)
begin
 Result := False;
 ZStreamCreated := False;
 Zipper := TKaZIP.Create(NIL);
 try
  Zipper.CompressionType := ctNormal;
  ZStream := TWideFileStream.Create(Dir + ProjectFile, fmCreate);
  try
   Zipper.CodePage := CP_UTF8;
   Zipper.CreateZip(ZStream);
   Zipper.Open(ZStream);
   try
    MainStream := TMemoryStream.Create;
    try
     MainStream.WriteBuffer(DatHeader, SizeOf(TDatHeader));
     WriteString(UTF16toUTF8(WideExtractRelativePath(Dir, InputRom)));
     WriteString(UTF16toUTF8(WideExtractRelativePath(Dir, OutputRom)));
     WriteInt(3 or (CodePage shl 2) or (1 shl 24));
     WriteInt(Flags);
     List := TCodePagedStringList.Create(CodePage);
     try
      (* Save tables *)
      if Tables = NIL then WriteInt(0) else
      with Tables^ do
      begin
       WriteInt(Count);
       P := Root;
       while P <> NIL do with P^, PTable(Data)^ do
       begin
        WriteString(UTF16toUTF8(tbName));
        Stream := TMemoryStream.Create;
        try
         SaveToStream(Stream, CodePage);
         Stream.Position := 0;
         Zipper.AddStream(tbName + '.tbl', Stream);
        finally
         Stream.Free;
        end;
        P := Next;
       end;
      end;
      (* Save groups *)
      if Groups = NIL then WriteInt(0) else
      with Groups^ do
      begin
       WriteInt(Count);
       P := Root;
       while P <> NIL do with P^, PGroup(Data)^ do
       begin
        WriteString(UTF16toUTF8(grName));
        WriteString(UTF16toUTF8(grDLLName));
        WriteInt(LongInt(grIsDictionary));
        if grIsDictionary then
        begin
         WriteInt(grMaxWordSize);
         WriteInt(Tables.GetIndex(grDictTable));
         WriteInt(LongInt(grDictMotorola));
         WriteInt(grDictItemStart);
         WriteInt(grDictItemSize);
         WriteLenString(grDictItemFirstBytes);
        end;
        (* Save text insert range list *)
        if grBlocks = NIL then WriteInt(0) else
        with grBlocks^ do
        begin
         WriteInt(Count);
         B := Root;
         while B <> NIL do with B^ do
         begin
          WriteInt(blStart);
          WriteInt(blCount);
          B := Next;
         end;
        end;
        (* Save pointer tables *)
        if grPointerTables = NIL then WriteInt(0) else
        with grPointerTables^ do
        begin
         WriteInt(Count);
         PP := Root;
         while PP <> NIL do with PP^, PPointerTable(Data)^do
         begin
          WriteWideString(ptName);
          with Tables^ do
          begin
           WriteInt(GetIndex(ptTable1));
           WriteInt(GetIndex(ptTable2));
          end;
          WriteInt(Groups.GetIndex(ptDictionary));
          WriteInt(ptItemIndex);
          WriteInt(ptStringLength);
          WriteInt(ptReference);
          WriteInt(ptPointerSize);
          WriteInt(ptAlignment);
          WriteInt(ptCharSize);
          WriteInt(ptInterval);
          WriteInt(ptShiftLeft);
          WriteInt(LongInt(ptMotorola) +
                   LongInt(ptSigned) shl 1 +
                   LongInt(ptPunType) shl 2 +
                   LongInt(ptAutoStart) shl 3 +
                   LongInt(ptPtrToPtr) shl 4 +
                   LongInt(not ptSeekSame) shl 5 +
                   LongInt(ptNotInSource) shl 6 +
                   LongInt(ptInserter <> nil) shl 7 +
                   LongInt(ptSnesLoRom) shl 8 +
                   1 shl 31);
          if ptInserter <> nil then
          with ptInserter^ do
          begin
           WriteInt(piReference);
           WriteInt(piPointerSize);
           WriteInt(piStringLength);
           WriteInt(piAlignment);
           WriteInt(piCharSize);
           WriteInt(piShiftLeft);
           WriteInt(LongInt(piAutoPtrStart));
           WriteInt(LongInt(piPunType) +
                    LongInt(piSnesLoROM) shl 1);
          end;
          (* Save near-pointer variables list *)
          if ptParameters = NIL then WriteInt(0) else
          with ptParameters^ do
          begin
           WriteInt(Count);
           WriteInt(Before);
           WriteInt(AfterFix);
           WriteInt(BeforeFix);
           PAR := Root;
           while PAR <> NIL do with PAR^, PParameter(Data)^ do
           begin
            WriteString(AnsiString(paName));
            WriteInt(LongInt(paBefore));
            WriteInt(LongInt(paParamType));
            WriteInt(paFlags);
            PAR := Next;
           end;
          end;
          (* Save text *)
          if ptPointers = NIL then WriteInt(0) else
          with ptPointers^ do
          begin
           WriteInt(Count);
           PTR := Root;
           while PTR <> NIL do with PTR^ do
           begin
            WriteInt(pnPos);
            WriteInt(pnPtr);
            WriteInt(pnReference);
            WriteInt(LongInt(pnFixed) + 1 shl 31);
            WriteInt(pnInsertPos);
            WriteInt(pnInsertRef);
            with Tables^ do
            begin
             WriteInt(GetIndex(pnTable1));
             WriteInt(GetIndex(pnTable2));
            end;
            WriteLenString(pnParamData);
            if pnStrings = NIL then WriteInt(0) else
            with pnStrings^ do
            begin
             WriteInt(Count);
             STR := Root;
             while STR <> NIL do with STR^ do
             begin
              List.Add(stNew);
              List.Add(sStringEnd + sLineBreak);
              STR := Next;
             end;
            end;
            PTR := Next;
           end;
          end;
          PP := Next;
         end;
        end;
        P := P.Next;
        Stream := TMemoryStream.Create;
        try
         List.SaveToStream_BOM(Stream, False);
         List.Clear;
         Stream.Position := 0;
         Zipper.AddStream(grName + '.txt', Stream);
        finally
         Stream.Free;
        end;
       end;
      end;
      (* Final saves *)
      SW := WideChangeFileExt(ProjectFile, '');
      MainStream.Position := 0;
      Zipper.AddStream(SW + '.kdt', MainStream);
      with TStreamIniFileW.Create do
      try
       WriteString('Project', 'Emulator', EmulSelected);
       WriteBool('Project', 'DOS_FNames', False);
       Stream := TMemoryStream.Create;
       try
        SaveToStream(Stream);
        Stream.Position := 0;
        Zipper.Comment.LoadFromStream(Stream);
        Zipper.UpdateComment;
       finally
        Stream.Free;
       end;
      finally
       Free;
      end;
     finally
      List.Free;
     end;
    finally
     MainStream.Free;
    end;
   finally
    Zipper.Close;
   end;
  finally
   ZStream.Free;
  end;
  ZStreamCreated := True;
  Result := True;
 except
  SW := Dir + ProjectFile;
  if ZStreamCreated then DeleteFile(SW);
  LocMessageDlg(WideFormat(K7S(SMUUnableToSave), [SW]),
   mtError, [mbOk], 0);
 end;
 Zipper.Free;
end;

function TKruptarProject.CheckFile(const Path: WideString; const FileName: RawByteString;
 Console, ExceptIfNotLoad: Boolean; CP: LongWord): String;
var
 SW, Error, DirSave: WideString;
 OK: Boolean;
begin
 Result := '';

 if FileName <> '' then
 begin
   if CP <> 0 then
   begin
     SW := ExpandFileNameEx(Dir, CodePageStringDecode(CP, FileName));
     OK := WideFileExists(SW);
   end else
   begin
     SW := ExpandFileNameEx(Dir, UTF8toUTF16(FileName));
     OK := WideFileExists(SW);

     if not OK then
     begin
       SW := ExpandFileNameEx(Dir, CodePageStringDecode(GetACP, FileName));
       OK := WideFileExists(SW);
     end;
   end;
 end else
   OK := False;

 if not OK then
 begin
  if FileName = '' then
   Error := 'File name is not specified.' else
   Error := FormatW.WideFormat('File "%s" not found.', [SW]);
  if Console then
  begin
   ConsoleWriteln(Error);
   if ExceptIfNotLoad then
     raise WideException.Create(Error);
  end else with MainForm do
  begin
   if ExceptIfNotLoad then
   begin
    OK := LocMessageDlg(WideFormat(K7S(SMUFileNotFoundLoadOtherX),
      [SW]), mtWarning, [mbOk, mbCancel], 0) = mrOK;
   end else
   begin
    OK := LocMessageDlg(WideFormat(K7S(SMUFileNotFoundLoadOther),
      [SW]), mtConfirmation, [mbYes, mbNo], 0) = mrYes;
   end;
   WideGetDir(0, DirSave);
   if OK then
   begin
    OpenDialog.FileName := SW;
    if OpenDialog.Execute then
    begin
     Result := GetShortFileName(OpenDialog.FileName);
     fSaved := False;
     ExceptIfNotLoad := False;
    end;
    SetCurrentDirectoryW(Pointer(DirSave));
   end;
   if ExceptIfNotLoad then
     raise WideException.Create(Error);
  end;
 end else
  Result := GetShortFileName(SW);
end; (* procedure CheckFile *)

function TKruptarProject.Open(const FileName: WideString;
  Console: Boolean): Boolean;
(* Function local variables *)
 var
  Stream: TMemoryStream;
  Zipper: TKaZIP;
(* Local functions *)
 function FindFile(const WildCard: WideString): TKAZipEntriesEntry;
 var
  I: Integer;
  SearchEntry: TKAZipEntriesEntry;
 begin
  Result := NIL;
  with Zipper.Entries do
  begin
   for I := 0 to Count - 1 do
   begin
    SearchEntry := Items[I];
    if WideMatchesMask(ToDosName(SearchEntry.FileName), WildCard) then
    begin
     Result := SearchEntry;
     Break;
    end;
   end;
  end;
 end;

 function ReadLenString: RawByteString;
 var
  C: LongInt;
 begin
  Stream.ReadBuffer(C, 4);
  if C > 0 then
  begin
   SetLength(Result, C);
   Stream.ReadBuffer(Pointer(Result)^, C)
  end else Result := '';
 end;

 function ReadString: RawByteString;
 var
  C: AnsiChar;
 begin
  Result := '';
  repeat
   if Stream.Read(C, 1) <> 1 then Break;
   if C = #1 then
   begin
    Result := ReadLenString;
    Exit;
   end;
   if C <> #0 then Result := Result + C;
  until C = #0;
 end;

 function ReadWideString: WideString;
 var
  C: WideChar;
 begin
  Result := '';
  repeat
   if Stream.Read(C, 2) <> 2 then Break;
   if C = #1 then
   begin
    Result := UTF8toUTF16(ReadLenString);
    Exit;
   end;
   if C <> #0 then Result := Result + C;
  until C = #0;
 end;

 function ReadInteger: LongInt;
 begin
  Stream.ReadBuffer(Result, 4);
 end;
(* End of local functions *)

 var
  H: LongInt;
  List: TCodePagedStringList;
  S, CS: RawByteString;
  SS: WideString;
  WS: WideString;
  DLL: PDLL;
  TextStrings: PTextStrings;
  TS: PTextString;
  Header: TDatHeader;
  Cnt, I, II, JJ, J, K, NN, _XCNT, XX: Longint;
  G, P: PDynamic;
  XY: Longint;
  ZipEntry: TKAZipEntriesEntry;
  DOS_FNames: Boolean;
  TextStream: TMemoryStream;

(* TKruptarProject.Open *)
begin
 if WideUpperCase(WideExtractFileExt(FileName)) = '.KPJ' then
    Result := Open6(FileName, Console) else
 begin
  Result := False;
  Zipper := TKaZIP.Create(NIL);
  try
   Zipper.CodePage := CP_UTF8;
   Zipper.Open(FileName);
   try
    ProjectFile := FSplit(FileName, Dir);
    ZipEntry := FindFile('*.cfg');
    EmulSelected := '';
    if ZipEntry <> NIL then
    begin
     Stream := TMemoryStream.Create;
     try
      Zipper.ExtractToStream(ZipEntry, Stream);
      Stream.Position := 0;
      with TWideStringList.Create do
      try
       LoadFromStream(Stream);
       if Count > 0 then
       begin
        WS := Strings[0];
        if WideFileExists(WS) then EmulSelected := WS;
       end;
      finally
       Free;
      end;
     finally
      Stream.Free;
     end;
    end;
    Stream := TMemoryStream.Create;
    try
     Zipper.Comment.SaveToStream(Stream);
     Stream.Position := 0;
     with TStreamIniFileW.Create(Stream) do
     try
      EmulSelected := ReadString('Project', 'Emulator', EmulSelected);
      DOS_FNames := ReadBool('Project', 'DOS_FNames', True);
     finally
      Free;
     end;
    finally
     Stream.Free;
    end;
    if DOS_FNames then
     Zipper.CodePage := CP_OEM_RUS;
    ZipEntry := FindFile('*.kdt');
    if ZipEntry <> NIL then
    begin
     Stream := TMemoryStream.Create;
     try
      Zipper.ExtractToStream(ZipEntry, Stream);
      List := TCodePagedStringList.Create;
      try
       Stream.Position := 0;
       Stream.ReadBuffer(Header, SizeOf(TDatHeader));
       fSaved := True;
       if Header = DatHeader then
       begin
        MainForm.OpenDialog.Filter := K7S(SMURomsFilter);
        InputRom := CheckFile(Dir, ReadString, Console, False);
        OutputRom := CheckFile(Dir, ReadString, Console, False);

        (* Continue *)
        Cnt := ReadInteger;
        case TCoding(Cnt and 3) of
         cdAnsi: CodePage := GetACP;
         cdUnicode: CodePage := CP_UTF16;
         cdShiftJis: CodePage := CP_SHIFT_JIS;
         else CodePage := (Cnt shr 2) and $FFFF;
        end;
        List.CodePage := CodePage;
        if Cnt shr 24 = 1 then
         Flags := ReadInteger else
         Flags := 0;
        (* Loading tables *)
        Cnt := ReadInteger;
        with Tables^ do for I := 0 to Cnt - 1 do with Add^ do
        begin
         S := ReadString;
         if DOS_FNames then
           tbName := CodePageStringDecode(GetACP, S)
         else
           tbName := CodePageStringDecode(CP_UTF8, S);
         ZipEntry := FindFile(tbName + '.tbl');
         if ZipEntry <> NIL then
         begin
          TextStream := TMemoryStream.Create;
          try
           Zipper.ExtractToStream(ZipEntry, TextStream);
           TextStream.Position := 0;
           FLastFileName := ZipEntry.FileName;
           LoadFromStream(TextStream, CodePage);
          finally
           TextStream.Free;
          end;
         end else
         begin
          SS := WideFormat('File "%s" not found in project', [tbName + '.tbl']);
          if Console then ConsoleWriteln(SS) else AddStateString
          (
           etError,
           ProjectFile,
           -1,
           WideFormat
           (
            K7S(SMUFileNotFoundInProject),
            [tbName + '.tbl']
           ),
           True
          );
          raise WideException.Create(SS);
         end;
        end; //Finish loading tables
        ////Loading groups///
        Cnt := ReadInteger;
        LoadRom(True);
        try
         with Groups^ do for I := 0 to Cnt - 1 do with Add^ do
         begin
          S := ReadString;
          if DOS_FNames then
           grName := CodePageStringDecode(GetACP, S)
          else
           grName := CodePageStringDecode(CP_UTF8, S);

          ZipEntry := FindFile(grName + '.txt');
          if ZipEntry = NIL then
          begin
           SS := WideFormat('File "%s" not found in project', [grName + '.txt']);
           if Console then ConsoleWriteln(SS) else AddStateString
           (
            etError,
            ProjectFile,
            -1,
            WideFormat
            (
             K7S(SMUFileNotFoundInProject),
             [grName + '.txt']
            ),
            True
           );
           raise WideException.Create(SS);
          end;
          H := 0;
          TextStream := TMemoryStream.Create;
          try
           Zipper.ExtractToStream(ZipEntry, TextStream);
           TextStream.Position := 0;
           List.LoadFromStream_BOM(TextStream, False);
          finally
           TextStream.Free;
          end;
          S := ReadString;
          grDLLName := CodePageStringDecode(CP_UTF8, S);
          DLL := Dlls.Get(grDLLName);
          if DLL = nil then
          begin
            grDLLName := CodePageStringDecode(GetACP, S);
            DLL := Dlls.Get(grDLLName);
          end;
          if DLL = NIL then
          begin
           SS := WideFormat('Library "%s" not found.', [grDLLName]);
           if Console then ConsoleWriteln(SS) else AddStateString
           (
            etError,
            ProjectFile,
            -1,
            WideFormat
            (
             K7S(SMULibraryNotFound),
             [grDLLName]
            ),
            True
           );
           raise WideException.Create(SS);
          end;
          grIsDictionary := Boolean(ReadInteger);
          if grIsDictionary then
          begin
           grMaxWordSize := ReadInteger;
           grDictTable := Tables.Get(ReadInteger);
           grDictMotorola := Boolean(ReadInteger);
           grDictItemStart := ReadInteger;
           grDictItemSize := ReadInteger;
           grDictItemFirstBytes := ReadLenString;
          end;
          //Loading Blocks
          J := ReadInteger;
          with grBlocks^ do for II := 0 to J - 1 do with Add^ do
          begin
           blStart := ReadInteger;
           blCount := ReadInteger;
          end;
          //Loading Pointer Tables
          J := ReadInteger;
          with grPointerTables^ do for II := 0 to J - 1 do
          with Add(Owner.Data)^ do
          begin
           ptName := ReadWideString;
           with Tables^ do
           begin
            ptTable1 := Get(ReadInteger);
            ptTable2 := Get(ReadInteger);
           end;
           LongInt(ptDictionary) := ReadInteger;
           ptItemIndex := ReadInteger;
           ptStringLength := ReadInteger;
           ptReference := ReadInteger;
           ptPointerSize := ReadInteger;
           ptAlignment := ReadInteger;
           ptCharSize := ReadInteger;
           ptInterval := ReadInteger;
           ptShiftLeft := ReadInteger;
           XY := ReadInteger;
           if XY shr 31 = 0 then
           begin
            ptMotorola := Boolean(XY);
            ptSigned := Boolean(ReadInteger);
            ptPunType := Boolean(ReadInteger);
            ptAutoStart := Boolean(ReadInteger);
           end else
           begin
            ptMotorola := Boolean(XY and 1);
            XY := XY shr 1;
            ptSigned := Boolean(XY and 1);
            XY := XY shr 1;
            ptPunType := Boolean(XY and 1);
            XY := XY shr 1;
            ptAutoStart := Boolean(XY and 1);
            XY := XY shr 1;
            ptPtrToPtr := Boolean(XY and 1);
            XY := XY shr 1;
            ptSeekSame := not Boolean(XY and 1);
            XY := XY shr 1;
            ptNotInSource := Boolean(XY and 1);
            XY := XY shr 1;
            if XY and 1 <> 0 then
            begin
             New(ptInserter);
             ptDestReference := ReadInteger;
             ptDestPointerSize := ReadInteger;
             ptDestStringLength := ReadInteger;
             ptDestAlignment := ReadInteger;
             ptDestCharSize := ReadInteger;
             ptDestShiftLeft := ReadInteger;
             ptDestAutoPtrStart := Boolean(ReadInteger);
             K := ReadInteger;
             ptDestPunType := K and 1 <> 0;
             ptDestLoRom := K and 2 <> 0;
            end;
            XY := XY shr 1;
            ptSnesLoRom := Boolean(XY and 1);
           end;
           K := ReadInteger;
           with ptParameters^ do
           begin
            Before := ReadInteger;
            AfterFix := ReadInteger;
            BeforeFix := ReadInteger;
            for JJ := 0 to K - 1 do with Add^ do
            begin
             paName := String(AnsiString(ReadString));
             paBefore := Boolean(ReadInteger);
             paParamType := TParamType(ReadInteger);
             paFlags := ReadInteger;
            end;
           end;
           K := ReadInteger;
           if RomSize <= 0 then ptNotInSource := True;
           with ptPointers^ do for JJ := 0 to K - 1 do with Add^ do
           begin
            pnPos := ReadInteger;
            pnPtr := ReadInteger;
            pnReference := ReadInteger;
            XY := ReadInteger;
            pnFixed := Boolean(XY and 1);
            if XY shr 31 <> 0 then
            begin
             pnInsertPos := ReadInteger;
             pnInsertRef := ReadInteger;
            end else
            begin
             pnInsertPos := pnPos;
             pnInsertRef := pnReference;
            end;
            with Tables^ do
            begin
             pnTable1 := Get(ReadInteger);
             pnTable2 := Get(ReadInteger);
            end;
            pnParamData := ReadLenString;
            if pnTable1 <> NIL then with pnTable1^ do
             DLL.SetVariables(ROM, ROMSize, EndRoot, EndMaxCodes, ptCharSize);
            _XCNT := ReadInteger;
            New(pnStrings, Init);
            if ptPointerSize > 0 then
            begin
             if not ptPunType then
              XX := GetPointer(ROM^[pnPos], pnPos, pnReference, XY) else
              XX := GetPointerPun(ROM, pnPos, ptInterval, pnReference, XY);
            end else XX := pnPos;
            if ptNotInSource or (pnTable1 = NIL) then
            begin
             New(TextStrings, Init);
             TextStrings.Add;
            end else with pnTable1^ do
            begin
             if (ptPointerSize > 0) and ptPtrToPtr then
              TextStrings := DLL.GetStrings(GetPointer(ROM[XX], XX,
                                 pnReference, XY), ptStringLength) else
              TextStrings := DLL.GetStrings(XX, ptStringLength);
            end;
            NN := 0;
            if TextStrings <> NIL then
            try
             TS := TextStrings.Root;
             while TS <> NIL do with pnStrings.Add^, TS^ do
             begin
              stOriginal := Str;
              TS := Next;
              if NN < _XCNT then
              begin
               stNew := ParseStr(List, H);
               Inc(NN);
              end else
               stNew := ConvertString(pnTable1, Str,
               Flags and KP_CRLF_CONVERT <> 0);
             end;
            finally
             if ptNotInSource then
              Dispose(TextStrings, Done) else
             DLL.DisposeStrings(TextStrings);
            end else pnStrings.Add;
            while NN < _XCNT do
            begin
             ParseStr(List, H);
             Inc(NN);
            end;
           end;
          end; //Finish loading pointer tables
         end; //Finish loading groups
        finally
         FreeRom;
        end;
       end else raise Exception.Create('Bad header.');
      finally
       List.Free;
      end;
     finally
      Stream.Free;
     end;
     G := Groups.Root;
     while G <> NIL do with G^, PGroup(Data)^ do
     begin
      P := grPointerTables.Root;
      while P <> NIL do with P^, PPointerTable(Data)^ do
      begin
       ptDictionary := Groups.Get(LongInt(ptDictionary));
       P := Next;
      end;
      G := Next;
     end;
     Result := True;
     fNamed := True;
    end else
    begin
     SS := 'File *.kdt not found in project.';
     if Console then ConsoleWriteln(SS) else
     AddStateString(etError, ProjectFile, -1,
       WideFormat(K7S(SMUFileNotFoundInProject), ['*.kdt']), True);
     raise WideException.Create(SS);
    end;
   finally
    Zipper.Close;
   end;
  except
   if Console then
    ConsoleWriteln(WideFormat('Unable to load file ''%s''.', [Zipper.FileName]));
  end;
  Zipper.Free;
 end;
end;

function TPointers.Get(I: Integer): PPointer;
var
 N: PPointer;
 J: Integer;
begin
 N := Root;
 J := 0;
 while N <> NIL do
 begin
  if J = I then
  begin
   Result := N;
   Exit;
  end;
  Inc(J);
  N := N^.Next;
 end;
 Result := NIL;
end;

function TPointers.GetByPos(Pos: LongInt): PPointer;
var
 N: PPointer;
begin
 N := Root;
 while N <> NIL do with N^ do
 begin
  if pnPos = Pos then
  begin
   Result := N;
   Exit;
  end;
  N := Next;
 end;
 Result := NIL;
end;

constructor TPointers.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TPointers.Add: PPointer;
begin
 New(Result);
 if Root = NIL then Root := Result else Cur^.Next := Result;
 Cur := Result;
 Inc(Count);
 FillChar(Result^, SizeOf(TPointer), 0);
end;

destructor TPointers.Done;
begin
 Clear;
end;

procedure TMainForm.MenuFileNewActionExecute(Sender: TObject);
var
 P: PDynamic;
 I: Integer;
 Founded: Boolean;
 S: WideString;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 I := 1;
 repeat
  Founded := False;
  S := WideFormat('Project%d', [I]);
  P := Projects.Root;
  while P <> NIL do with P^, PKruptarProject(Data)^ do
  begin
   if not fNamed and (S = WideChangeFileExt(ProjectFile, '')) then
   begin
    Inc(I);
    Founded := True;
    Break;
   end;
   P := Next;
  end;
 until not Founded;
 with Projects^, Add^ do
 begin
  fNamed := False;
  fSaved := True;
  InputROM := '';
  OutputROM := '';
  ProjectFile := S;
  Dir := '';
  CodePage := CP_UTF8;
  SelectedData := Cur.Data;
  RefreshProjectTabs(Count - 1);
  RefreshTree(Count - 1);
  TreeViewChange(nil, TreeView.Selected);
 end;
end;

var
 SR: TKruptarTreeNodeType;

procedure TMainForm.TreeViewClick(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 with TreeView do
 begin
  Node := Selected;
  if Node = NIL then Exit;
  SelectedData := Node.Data;
  SelectedRoot := Node;
  SelectedGroup := NIL;
  SelectedList := NIL;
  while SelectedRoot.Level > 0 do
  begin
   with SelectedRoot do
   if StateIndex = Integer(tsGroup) then
    SelectedGroup := Data else
   if StateIndex = Integer(tsPtrTable) then
    SelectedList := Data;
   SelectedRoot := SelectedRoot.Parent;
  end;
  if (SR <> NIL) and (SelectedRoot <> SR) then
  begin
   GroupsTabControl.Tabs.Clear;
   ChangeGroups(SelectedRoot.GetLastChild);
   GroupsTabControlChange(Sender);
   NotChange := False;
  end;
  SR := SelectedRoot;
  ReadOnly := StatesReadOnly[TTreeStateType(Node.StateIndex)];
 end;
end;

procedure TMainForm.MenuProjectTableAddActionExecute(Sender: TObject);
var
 P: PDynamic;
 I: Integer;
 Founded: Boolean;
 S: WideString;
 Node: TKruptarTreeNodeType;
begin
 if SelectedRoot = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with PKruptarProject(SelectedRoot.Data)^ do
 begin
  I := 1;
  repeat
   Founded := False;
   S := WideFormat('Table%d', [I]);
   P := Tables.Root;
   while P <> NIL do with P^, PTable(Data)^ do
   begin
    if S = tbName then
    begin
     Inc(I);
     Founded := True;
     Break;
    end;
    P := Next;
   end;
  until not Founded;
 end;
 with PKruptarProject(SelectedRoot.Data)^ do
 begin
  SelectedData := Tables.Add;
  PTable(SelectedData).tbName := S;
  {RefreshTables(SelectedRoot.GetFirstChild);
  TreeViewChange(Sender, TreeView.Selected);
  TreeView.Selected.Expand(False);}
  Node := TreeView.Items.AddChild(SelectedRoot.getFirstChild, S);
  Node.ImageIndex := Icons[tsTable];
  Node.SelectedIndex := Icons[tsTable];
  Node.StateIndex := Integer(tsTable);
  Node.Data := SelectedData;
  TreeView.Selected := Node;
  RefreshTable(Node);
  Node.Expand(False);
  TreeViewChange(Sender, Node);
  SetSaved(SelectedRoot.Data, False);
 end;
end;

function GetBlockString(Start, Count: LongInt): WideString;
begin
 Result := WideFormat('%7.7x-%7.7x', [Start, Start + Count - 1]);
end;

procedure TMainForm.MenuProjectItemAddActionExecute(Sender: TObject);
var
 P: PTableItem;
 Node: TKruptarTreeNodeType;
 B: PBlock;
 PT, PR: PPointerTable;
 D: PDynamic;
 I: Integer;
 Founded: Boolean;
 S: WideString;
begin
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if TTreeStateType(Node.StateIndex) in [tsTableItem, tsBlock, tsPtrTable] then
 begin
  Node := Node.Parent;
  if Node = NIL then Exit;
 end;
 if Node.Parent = NIL then Exit;
 Case TTreeStateType(Node.StateIndex) of
  tsPtrTables: with PGroup(Node.Parent.Data)^ do
  begin
   I := 1;
   repeat
    Founded := False;
    S := WideFormat('List%d', [I]);
    D := grPointerTables.Root;
    while D <> NIL do with D^, PPointerTable(Data)^ do
    begin
     if S = ptName then
     begin
      Inc(I);
      Founded := True;
      Break;
     end;
     D := Next;
    end;
   until not Founded;
   with grPointerTables^ do
   begin
    if Cur <> NIL then
     PR := Cur.Data else
     PR := NIL;
    PT := Add(Owner.Data);
    if PR <> NIL then with PT^ do
    begin
     ptTable1 := PR.ptTable1;
     ptTable2 := PR.ptTable2;
     ptStringLength := PR.ptStringLength;
     ptReference := PR.ptReference;
     ptPointerSize := PR.ptPointerSize;
     ptDictionary := PR.ptDictionary;
     ptAlignment := PR.ptAlignment;
     ptCharSize := PR.ptCharSize;
     ptInterval := PR.ptInterval;
     ptShiftLeft := PR.ptShiftLeft;
     ptMotorola := PR.ptMotorola;
     ptSigned := PR.ptSigned;
     ptPunType := PR.ptPunType;
     ptAutoStart := PR.ptAutoStart;
     ptSeekSame := PR.ptSeekSame;
     ptPtrToPtr := PR.ptPtrToPtr;
     ptNotInSource := PR.ptNotInSource;
     ptSnesLoRom := PR.ptSnesLoROM;
     if PR.ptInserter <> nil then
     begin
      New(ptInserter);
      ptDestReference := PR.ptDestReference;
      ptDestPointerSize := PR.ptDestPointerSize;
      ptDestStringLength := PR.ptDestStringLength;
      ptDestAlignment := PR.ptDestAlignment;
      ptDestCharSize := PR.ptDestCharSize;
      ptDestShiftLeft := PR.ptDestShiftLeft;
      ptDestAutoPtrStart := PR.ptDestAutoPtrStart;
      ptDestPunType := PR.ptDestPunType;
      ptDestLoRom := PR.ptDestLoRom;
     end;
    end;
   end;
   SelectedData := PT;
   with PT^ do
   begin
    ptName := S;
    ptParameters.Before := 0;
   end;
   with TreeView do
   begin
    Node := Items.AddChild(Node, PT.ptName);
    with Node do
    begin
     Data := PT;
     StateIndex := Integer(tsPtrTable);
     ImageIndex := Icons[tsPtrTable];
     SelectedIndex := Icons[tsPtrTable];
    end;
    Selected := Node;
   end;
   GroupsTabControlChange(Sender);
   TreeViewChange(Sender, Node);
   SetSaved(SelectedRoot.Data, False);
  end;
  tsBlocks: with PGroup(Node.Parent.Data)^ do
  begin
   B := grBlocks.Add;
   SelectedData := B;
   with TreeView do
   begin
    with B^ do
     Node := Items.AddChild(Node, GetBlockString(blStart, blCount));
    with Node do
    begin
     Data := B;
     StateIndex := Integer(tsBlock);
     ImageIndex := Icons[tsBlock];
     SelectedIndex := Icons[tsBlock];
    end;
    Selected := Node;
   end;
   SetSaved(SelectedRoot.Data, False);
   TreeViewChange(Sender, TreeView.Selected);
  end;
  tsEnds: with PTable(Node.Parent.Data)^ do
  begin
   New(P);
   if EndRoot = NIL then
    EndRoot := P else
    EndCur.Next := P;
   EndCur := P;
   with P^ do
   begin
    Next := NIL;
    tiCodes := AnsiChar(EndCount);
    tiString := '';
    tiState := isTerminate;
   end;
   Inc(EndCount);
   RefreshMax;
   SelectedData := P;
   RefreshTable(Node.Parent);
   SetSaved(SelectedRoot.Data, False);
   ListBoxClick(Sender);
  end;
  tsEls: with PTable(Node.Parent.Data)^ do
  begin
   New(P);
   if ElRoot = NIL then
    ElRoot := P else
    ElCur.Next := P;
   ElCur := P;
   with P^ do
   begin
    Next := NIL;
    tiCodes := AnsiChar(ElCount);
    tiString := '';
    tiState := isBreak;
   end;
   Inc(ElCount);
   RefreshMax;
   SelectedData := P;
   RefreshTable(Node.Parent);
   SetSaved(SelectedRoot.Data, False);
   ListBoxClick(Sender);
  end;
  tsCodes: with PTable(Node.Parent.Data)^ do
  begin
   New(P);
   if Root = NIL then
    Root := P else
    Cur.Next := P;
   Cur := P;
   with P^ do
   begin
    Next := NIL;
    tiCodes := AnsiChar(Count);
    tiString := '';
    tiState := isCode;
   end;
   Inc(Count);
   RefreshMax;
   SelectedData := P;
   RefreshTable(Node.Parent);
   SetSaved(SelectedRoot.Data, False);
   ListBoxClick(Sender);
  end;
 end;
end;

{$IFNDEF FORCED_UNICODE}
procedure TMainForm.TreeViewEdited(Sender: TObject; Node: TTntTreeNode; var S: WideString);
{$ELSE}
procedure TMainForm.TreeViewEdited(Sender: TObject; Node: TTreeNode; var S: String);
{$ENDIF}
var
 CD: RawByteString;
 PT: PTable;
 C: Integer;
 SS: WideString;
 DY: PDynamic;

 procedure Reset(const SS: WideString; B: Boolean);
 var
  CC: Integer;
 begin
  CD := '';
  AddStateString(etError, '', 0, SS);
  with PTableItem(Node.Data)^ do
  begin
   S := '';
   for CC := 1 to Length(tiCodes) do
    S := S + IntToHex(Byte(tiCodes[CC]), 2);
   if B then S := S + '=' + EscapeStringEncode(tiString);
  end;
 end; //Reset End

var
 SX, SO: String;
 D: Integer;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 if Node.Parent = NIL then Exit;
 if Node.Parent.Data = NIL then Exit;
 Case TTreeStateType(Node.StateIndex) of
  tsTable: with PTable(Node.Data)^ do
  begin
   if (S = '') or not CheckFileName(S) then S := tbName else
   if S <> tbName then
   begin
    if PTables(Node.Parent.Data).Find(S) <> NIL then
    begin
     S := tbName;
     AddStateString(etNone, '', 0, K7S(SMUSuchTableIsExist));
     Exit;
    end;
    tbName := S;
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
  tsPointer: with PPointer(Node.Data)^ do
  begin
   if not UnsCheckOut('h' + S) then
   begin
    S := IntToHex(pnPtr, 7);
    AddStateString(etNone, '', 0, K7S(SMUHexError));
    Exit;
   end else
   begin
    C := GetLDW('h' + S);
    if pnPtr <> C then
    begin
     pnPtr := C;
     SetSaved(SelectedRoot.Data, False);
    end;
   end;
  end;
  tsPtrTable: with PPointerTable(Node.Data)^ do
  begin
   if S = '' then S := ptName else if S <> ptName then
   begin
    if PPointerTables(Node.Parent.Data)^.Find(S) <> NIL then
    begin
     S := ptName;
     AddStateString(etNone, '', 0, K7S(SMUSuchListIsExist));
     Exit;
    end;
    ptName := S;
    SetSaved(SelectedRoot.Data, False);
    GroupsTabControlChange(Sender);
    NotChange := False;
   end;
  end;
  tsGroup: with PGroup(Node.Data)^ do
  begin
   if (S = '') or not CheckFileName(S) then S := grName else
   if S <> grName then
   begin
    if PGroups(Node.Parent.Data).Find(S) <> NIL then
    begin
     S := grName;
     AddStateString(etNone, '', 0, K7S(SMUSuchGroupIsExist));
     Exit;
    end;
    grName := S;
    SetSaved(SelectedRoot.Data, False);
    with GroupsTabControl do
    begin
     C := 0;
     DY := PGroups(Node.Parent.Data).Root;
     while DY <> NIL do with DY^ do
     begin
      if DY.Data = Node.Data then
      begin
       Tabs[C] := grName;
       Break;
      end;
      Inc(C);
      DY := Next;
     end;
    end;
   end;
  end;
  tsBlock:
  begin
   with PBlocks(Node.Parent.Data)^, PBlock(Node.Data)^ do
   begin
    C := 1; SX := '';
    SS := S;
    D := Length(SS);
    while (C <= D) and (C <= 7) and (SS[1] <> '-') do
    begin
     SX := SX + SS[1];
     Delete(SS, 1, 1);
     Inc(C);
    end;
    if (SS = '') or (SS[1] <> '-') then
    begin
     AddStateString(etError, '', 0, K7S(SMUInvalidString));
     S := GetBlockString(blStart, blCount);
    end else
    begin
     Delete(SS, 1, 1);
     SO := '';
     D := HexVal(SX);
     if HexError then SO := SX;
     C := HexVal(SS);
     if HexError then SO := SS;
     if SO <> ''then
     begin
      AddStateString(etError, '', 0,
      WideFormat(K7S(SMUInvalidHexadecimalValue), [SO]));
      S := GetBlockString(blStart, blCount);
     end else
     begin
      if D > C then
      begin
       blStart := C;
       blCount := D - C + 1;
      end else
      begin
       blStart := D;
       blCount := C - D + 1;
      end;
      S := GetBlockString(blStart, blCount);
      SetSaved(SelectedRoot.Data, False);
     end;
    end;
   end;
  end;
  tsTableItem:
  begin
   if Node.Parent.Parent = NIL then Exit;
   PT := Node.Parent.Parent.Data;
   if PT = NIL then Exit;
   with PT^ do
   Case TTreeStateType(Node.Parent.StateIndex) of
    tsEnds: with PTableItem(Node.Data)^ do
    begin
     SS := S;
     CD := '';
     repeat
      C := GetByte(SS);
      if C >= 0 then CD := CD + AnsiChar(C) else Break;
     until False;
     if (Length(CD) > 0) and (SS = '') then
     begin
      tiCodes := CD;
      tiString := '';
      PTable(Node.Parent.Parent.Data).RefreshMax;
     end else Reset(K7S(SMUHexError), False);
    end;
    tsEls: with PTableItem(Node.Data)^ do
    begin
     SS := S;
     CD := '';
     repeat
      C := GetByte(SS);
      if C >= 0 then CD := CD + AnsiChar(C) else Break;
     until False;
     if (Length(CD) > 0) and (SS = '') then
     begin;
      tiCodes := CD;
      tiString := '';
      PTable(Node.Parent.Parent.Data).RefreshMax;
     end else Reset(K7S(SMUHexError), False);
    end;
    tsCodes: with PTableItem(Node.Data)^ do
    begin
     SS := S;
     if Pos('=', SS) > 0 then
     begin
      CD := '';
      repeat
       C := GetByte(SS);
       if C >= 0 then CD := CD + AnsiChar(C) else Break;
      until False;
      if (Length(CD) > 0) and (Length(SS) > 1) and
         ((SS[1] = ' ') or (SS[1] = '=')) then
      begin
       while (SS[1] = ' ') and (Length(SS) > 1) do Delete(SS, 1, 1);
       if (Length(SS) > 1) and (SS[1] = '=') then
       begin
        Delete(SS, 1, 1);
        tiCodes := CD;
        tiString := EscapeStringDecode(SS);
        PTable(Node.Parent.Parent.Data).RefreshMax;
       end else Reset(K7S(SMUInvalidTableItem), True);
      end else Reset(K7S(SMUInvalidTableItem), True);
     end else Reset(K7S(SMUInvalidTableItem), True);
    end;
   end;
   SetSaved(SelectedRoot.Data, False);
   ListBoxClick(Sender);
  end;
 end;
end;

procedure TMainForm.MenuOtherClearMessageListActionExecute(Sender: TObject);
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
end;

procedure TMainForm.TreeViewChange(Sender: TObject; Node: TTreeNode);
begin
 if NotChange then Exit;
 SelectedRoot := NIL;
 SelectedData := NIL;
 SelectedGroup := NIL;
 SelectedList := NIL;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 TreeViewClick(Sender);
 Values.FillValueListBox(TKruptarTreeNode(Node));
end;

procedure TMainForm.TreeViewCollapsing(Sender: TObject; Node: TTreeNode;
  var AllowCollapse: Boolean);
begin
 with TreeView do if DblClickPressed then
 begin
  DblClickPressed := False;
  AllowCollapse := False;
 end;
end;

procedure TMainForm.MenuProjectItemUpActionExecute(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 Exp: Boolean;
begin
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 if Node.GetPrevSibling = NIL then Exit;
 Case TTreeStateType(Node.StateIndex) of
  tsProject:
  begin
   with Projects^ do
   begin
    MoveUp(SelectedData);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
   end;
  end;
  tsTable:
  begin
   if SelectedRoot = NIL then Exit;
   if SelectedRoot.Data = NIL then Exit;
   with PKruptarProject(SelectedRoot.Data)^.Tables^ do
   begin
    MoveUp(SelectedData);
    SetSaved(SelectedRoot.Data, False);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
   end;
  end;
  tsPointer:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PPointerTable(Node.Parent.Data)^.ptPointers^ do
   begin
    MoveUp(SelectedData);
    SetSaved(SelectedRoot.Data, False);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
    if PGroup(Node.Parent.Parent.Parent.Data).grCurPtrTable =
       Node.Parent.Data then
     RefreshList;
   end;
  end;
  tsPtrTable:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PPointerTables(Node.Parent.Data)^ do
   begin
    MoveUp(SelectedData);
    SetSaved(SelectedRoot.Data, False);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
    GroupsTabControlChange(Sender);
   end;
  end;
  tsGroup:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PGroups(Node.Parent.Data)^ do
   begin
    MoveUp(SelectedData);
    SetSaved(SelectedRoot.Data, False);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
    GroupsTabControl.Tabs.Clear;
    ChangeGroups(Node.Parent);
    GroupsTabControlChange(Sender);
   end;
  end;
  tsBlock:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PBlocks(Node.Parent.Data)^ do
   begin
    MoveUp(SelectedData);
    SetSaved(SelectedRoot.Data, False);
    Exp := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    if Exp then Node.Expand(False);
   end;
  end;
  tsTableItem:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Parent = NIL then Exit;
   if Node.Parent.Parent.Data = NIL then Exit;
   with PTable(Node.Parent.Parent.Data)^ do
   begin
    Case TTreeStateType(Node.Parent.StateIndex) of
     tsEnds:
     begin
      MoveUpByItem(EndRoot, SelectedData);
      Exp := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      if Exp then Node.Expand(False);
     end;
     tsEls:
     begin
      MoveUpByItem(ElRoot, SelectedData);
      Exp := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      if Exp then Node.Expand(False);
     end;
     tsCodes:
     begin
      MoveUpByItem(Root, SelectedData);
      Exp := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      if Exp then Node.Expand(False);
     end;
    end;
    SetSaved(SelectedRoot.Data, False);
    ListBoxClick(Sender);
   end;
  end;
 end;
end;

procedure TMainForm.MenuProjectItemDownActionExecute(Sender: TObject);
var
 Node, N: TKruptarTreeNodeType;
 Exp, Ex: Boolean;
begin
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 N := Node;
 Node := Node.GetNextSibling;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 Case TTreeStateType(Node.StateIndex) of
  tsProject:
  begin
   with Projects^ do
   begin
    MoveUp(Node.Data);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
   end;
  end;
  tsTable:
  begin
   if SelectedRoot = NIL then Exit;
   if SelectedRoot.Data = NIL then Exit;
   with PKruptarProject(SelectedRoot.Data).Tables^ do
   begin
    MoveUp(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
   end;
  end;
  tsPointer:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PPointerTable(Node.Parent.Data).ptPointers^ do
   begin
    MoveUp(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
    if PGroup(Node.Parent.Parent.Parent.Data).grCurPtrTable =
       Node.Parent.Data then
     RefreshList;
   end;
  end;
  tsPtrTable:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PPointerTables(Node.Parent.Data)^ do
   begin
    MoveUp(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
    GroupsTabControlChange(Sender);
   end;
  end;
  tsGroup:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PGroups(Node.Parent.Data)^ do
   begin
    MoveUp(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
    GroupsTabControl.Tabs.Clear;
    ChangeGroups(Node.Parent);
    GroupsTabControlChange(Sender);
   end;
  end;
  tsBlock:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Data = NIL then Exit;
   with PBlocks(Node.Parent.Data)^ do
   begin
    MoveUp(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Exp := N.Expanded;
    Ex := Node.Expanded;
    Node.MoveTo(Node.GetPrevSibling, naInsert);
    TreeView.Selected := N;
    if Ex then Node.Expand(False);
    if Exp then N.Expand(False);
   end;
  end;
  tsTableItem:
  begin
   if Node.Parent = NIL then Exit;
   if Node.Parent.Parent = NIL then Exit;
   if Node.Parent.Parent.Data = NIL then Exit;
   with PTable(Node.Parent.Parent.Data)^ do
   begin
    Case TTreeStateType(Node.Parent.StateIndex) of
     tsEnds:
     begin
      MoveUpByItem(EndRoot, Node.Data);
      Exp := N.Expanded;
      Ex := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      TreeView.Selected := N;
      if Ex then Node.Expand(False);
      if Exp then N.Expand(False);
      SetSaved(SelectedRoot.Data, False);
      ListBoxClick(Sender);
     end;
     tsEls:
     begin
      MoveUpByItem(ElRoot, Node.Data);
      Exp := N.Expanded;
      Ex := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      TreeView.Selected := N;
      if Ex then Node.Expand(False);
      if Exp then N.Expand(False);
      SetSaved(SelectedRoot.Data, False);
      ListBoxClick(Sender);
     end;
     tsCodes:
     begin
      MoveUpByItem(Root, Node.Data);
      Exp := N.Expanded;
      Ex := Node.Expanded;
      Node.MoveTo(Node.GetPrevSibling, naInsert);
      TreeView.Selected := N;
      if Ex then Node.Expand(False);
      if Exp then N.Expand(False);
      SetSaved(SelectedRoot.Data, False);
      ListBoxClick(Sender);
     end;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.MenuProjectItemRemoveActionExecute(Sender: TObject);
var
 Node, N: TKruptarTreeNodeType;
 PT: PTable;
 G, P: PDynamic;
 PP: PPointer;
begin
 if not MenuProjectItemRemoveAction.Enabled then Exit;
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 N := Node.Parent;
 Case TTreeStateType(Node.StateIndex) of
  tsTable:
  begin
   if N = NIL then Exit;
   if N.Data = NIL then Exit;
   PT := Node.Data;
   with PKruptarProject(SelectedRoot.Data)^ do
   begin
    G := Groups.Root;
    while G <> NIL do with G^, PGroup(Data)^ do
    begin
     P := grPointerTables.Root;
     while P <> NIL do with P^, PPointerTable(Data)^ do
     begin
      if (PT = ptTable1) or (PT = ptTable2) then
      begin
       LocMessageDlg(WideFormat(K7S(SMUTableExistIn), [grName + '->' + ptName]),
         mtInformation, [mbOk], 0);
       Exit;
      end;
      PP := ptPointers^.Root;
      while PP <> NIL do with PP^ do
      begin
       if (PT = pnTable1) or (PT = pnTable2) then
       begin
        LocMessageDlg(WideFormat(K7S(SMUTableExistIn),
        [grName + '->' + ptName + '->' + IntToHex(pnPtr, 8)]),
         mtInformation, [mbOk], 0);
        Exit;
       end;
       PP := Next;
      end;
      P := Next;
     end;
     G := Next;
    end;
   end;
   if LocMessageDlg(K7S(SMUDeleteTableConfirmation),
    mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
   with PTables(N.Data)^ do
   begin
    if Count = 1 then TreeView.Selected := N else
    begin
     if SelectedData = Cur.Data then
      TreeView.Selected := Node.GetPrevSibling else
      TreeView.Selected := Node.GetNextSibling;
    end;
    Remove(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Node.Delete;
    RefreshTables(N);
    TreeViewChange(Sender, TreeView.Selected);
    TreeView.Selected.Expand(False);
   end;
  end;
  tsPointer:
  begin
   if N = NIL then Exit;
   if N.Data = NIL then Exit;
   with PPointerTable(N.Data).ptPointers^ do
   begin
    if Count = 1 then TreeView.Selected := N else
    begin
     if SelectedData = Cur then
      TreeView.Selected := Node.GetPrevSibling else
      TreeView.Selected := Node.GetNextSibling;
    end;
    Remove(Node.Data);
    Node.Delete;
    SetSaved(SelectedRoot.Data, False);
    TreeViewChange(Sender, TreeView.Selected);
    GroupsTabControlChange(Sender);
    ListBoxClick(Sender);
   end;
  end;
  tsPtrTable:
  begin
   if N = NIL then Exit;
   if N.Data = NIL then Exit;
   if LocMessageDlg(K7S(SMUDeleteListConfirmation),
    mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
   with PPointerTables(N.Data)^ do
   begin
    if CanCheck then CanCheck := False;
    if Count = 1 then TreeView.Selected := N else
    begin
     if SelectedData = Cur.Data then
      TreeView.Selected := Node.GetPrevSibling else
      TreeView.Selected := Node.GetNextSibling;
    end;
    Remove(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Node.Delete;
    RefreshPtrTables(N);
    with TreeView.Selected do
     if StateIndex = Integer(tsPtrTable) then
      PGroup(N.Parent.Data).grCurPtrTable := Data else
     begin
      PGroup(N.Parent.Data).grCurPtrTable := NIL;
      CurString := NIL;
      CurPointer := NIL;
     end;
    GroupsTabControlChange(Sender);
    TreeViewChange(Sender, TreeView.Selected);
    TreeView.Selected.Expand(False);
   end;
  end;
  tsGroup:
  begin
   if N = NIL then Exit;
   if N.Data = NIL then Exit;
   if LocMessageDlg(K7S(SMUDeleteGroupConfirmation),
                 mtConfirmation, [mbYes, mbNo], 0) <> mrYes then Exit;
   with PGroups(N.Data)^ do
   begin
    if CanCheck then CanCheck := False;
    if Count = 1 then TreeView.Selected := N else
    begin
     if SelectedData = Cur.Data then
      TreeView.Selected := Node.GetPrevSibling else
      TreeView.Selected := Node.GetNextSibling;
    end;
    if CurGroup = Node.Data then CurGroup := NIL;
    Remove(Node.Data);
    SetSaved(SelectedRoot.Data, False);
    Node.Delete;
    GroupsTabControl.Tabs.Clear;
    ChangeGroups(N);
    GroupsTabControlChange(Sender);
    TreeView.Selected.Expand(False);
   end;
  end;
  tsBlock:
  begin
   if N = NIL then Exit;
   if N.Data = NIL then Exit;   
   with PBlocks(N.Data)^ do
   begin
    if Count = 1 then TreeView.Selected := N else
    begin
     if SelectedData = Cur then
      TreeView.Selected := Node.GetPrevSibling else
      TreeView.Selected := Node.GetNextSibling;
    end;
    Remove(Node.Data);
    Node.Delete;
    SetSaved(SelectedRoot.Data, False);
    TreeViewChange(Sender, TreeView.Selected);
   end;
  end;
  tsTableItem:
  begin
   if N = NIL then Exit;
   if N.Parent = NIL then Exit;
   if N.Parent.Data = NIL then Exit;
   with PTable(N.Parent.Data)^ do
   begin
    Case TTreeStateType(N.StateIndex) of
     tsEnds:
     begin
      if EndCount = 1 then TreeView.Selected := N else
      begin
       if SelectedData = EndCur then
        TreeView.Selected := Node.GetPrevSibling else
        TreeView.Selected := Node.GetNextSibling;
      end;
      RemoveByItem(EndRoot, Node.Data);
      Node.Delete;
      TreeViewChange(Sender, TreeView.Selected);
     end;
     tsEls:
     begin
      if ElCount = 1 then TreeView.Selected := N else
      begin
       if SelectedData = ElCur then
        TreeView.Selected := Node.GetPrevSibling else
        TreeView.Selected := Node.GetNextSibling;
      end;
      RemoveByItem(ElRoot, Node.Data);
      Node.Delete;
      TreeViewChange(Sender, TreeView.Selected);
     end;
     tsCodes:
     begin
      if Count = 1 then TreeView.Selected := N else
      begin
       if SelectedData = Cur then
        TreeView.Selected := Node.GetPrevSibling else
        TreeView.Selected := Node.GetNextSibling;
      end;
      RemoveByItem(Root, Node.Data);
      Node.Delete;
      TreeViewChange(Sender, TreeView.Selected);
     end;
    end;
   end;
   SetSaved(SelectedRoot.Data, False);
   ListBoxClick(Sender);
  end;
 end;
end;

procedure TMainForm.TreeViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

 procedure AddMenuItem(Action: TAction);
 var
  MenuItem: TKruptarMenuItem;
 begin
  MenuItem := TKruptarMenuItem.Create(Self);
  if Action = nil then
   MenuItem.Caption := '-' else
   MenuItem.Action := Action;
  CustomPopup.Items.Add(MenuItem);
 end;

 procedure AddEditItems;
 begin
  AddMenuItem(MenuEditCutAction);
  AddMenuItem(MenuEditCopyAction);
  AddMenuItem(MenuEditPasteAction);
  AddMenuItem(nil);
  AddMenuItem(MenuEditClearAction);
 end;

 procedure AddMoveItems;
 begin
  AddMenuItem(MenuProjectItemUpAction);
  AddMenuItem(MenuProjectItemDownAction);
  AddMenuItem(MenuProjectItemRemoveAction);
 end;

 procedure AddLoadSaveItems;
 begin
  AddMenuItem(MenuEditLoadAction);
  AddMenuItem(MenuEditSaveAction);
 end;

 procedure AddEditAndMoveItems;
 begin
  AddLoadSaveItems;
  AddMenuItem(nil);
  AddMoveItems;
  AddMenuItem(nil);
  AddEditItems;
 end;

var
 Node: TKruptarTreeNodeType;
begin
 if ssDouble in Shift then DblClickPressed := True;
 if Button = mbRight then with TreeView do
 begin
  Node := GetNodeAt(X, Y);
  if Node = NIL then Exit;
  Inc(X, 4);
  Inc(Y, 4);
  SelectedData := Node.Data;
  SelectedRoot := Node;
  SelectedGroup := NIL;
  SelectedList := NIL;
  while SelectedRoot.Level > 0 do
  begin
   with SelectedRoot do
   if StateIndex = Integer(tsGroup) then
    SelectedGroup := Data else
   if StateIndex = Integer(tsPtrTable) then
    SelectedList := Data;
   SelectedRoot := SelectedRoot.Parent;
  end;
  Selected := Node;
  CustomPopup.Items.Clear;
  Case TTreeStateType(Node.StateIndex) of
   tsProject:
   begin
    AddMenuItem(MenuFileSaveAction);
    AddMenuItem(MenuFileSaveAsAction);
    AddMenuItem(nil);
    AddMenuItem(MenuProjectItemUpAction);
    AddMenuItem(MenuProjectItemDownAction);
    AddMenuItem(MenuFileCloseAction);
   end;
   // ProjectPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsTables:
   begin
    AddMenuItem(MenuProjectTableAddAction);
    AddMenuItem(MenuProjectItemsAddFromFileAction);    
    AddMenuItem(nil);
    AddMenuItem(MenuEditClearAction);
   end;
   // TablesPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsGroups:
   begin
    AddMenuItem(MenuProjectGroupAddAction);
    AddMenuItem(nil);
    AddMenuItem(MenuEditClearAction);
   end;
   // GroupsPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsCodes,
   // AddTableItemPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsEnds, tsEls, tsBlocks:
   begin
    AddMenuItem(MenuProjectItemAddAction);
    AddMenuItem(MenuProjectItemsAddFromFileAction);
    if Node.StateIndex = Integer(tsCodes) then
     AddMenuItem(MenuProjectTableAnsiFillAction);
    AddMenuItem(nil);
    AddLoadSaveItems;
    AddMenuItem(nil);
    AddEditItems;
   end;
   // AddPopupMenu.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsPtrTables:
   begin
    AddMenuItem(MenuProjectItemAddAction);
    AddMenuItem(nil);
    AddMenuItem(MenuEditClearAction);
   end;
   // AddClearPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsTable: AddEditAndMoveItems;
   // TablePopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsTableItem, tsBlock, tsPointer: AddMoveItems;
   // MovePopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsGroup:
   begin
    AddMoveItems;
    AddMenuItem(nil);
    AddMenuItem(MenuEditClearAction);    
    AddMenuItem(nil);
    AddMenuItem(MenuProjectGroupTextReloadAction);
    AddMenuItem(MenuProjectGroupTextResetAction);
    AddMenuItem(nil);
    AddMenuItem(MenuProjectGroupDictOptionsAction);
    AddMenuItem(MenuProjectGroupDictGenerateAction);
    AddMenuItem(MenuProjectGroupDictToTableAction);
    AddMenuItem(nil);        
    AddMenuItem(MenuProjectGroupTextInsertAction);
   end;
   // GroupPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
   tsPtrTable:
   begin
    AddMenuItem(MenuProjectPointersAddAction);
    AddMenuItem(MenuProjectItemsAddFromFileAction);
    AddMenuItem(nil);
    AddEditAndMoveItems;
    AddMenuItem(nil);
    AddMenuItem(PointersSortAscAction);
    AddMenuItem(PointersSortDescAction);
    AddMenuItem(nil);    
    AddMenuItem(MenuProjectGroupTextReloadAction);
    AddMenuItem(MenuProjectGroupTextResetAction);
    AddMenuItem(nil);
    AddMenuItem(MenuProjectPointersToggleInsertFieldsAction);
    AddMenuItem(MenuProjectPointerFixPointersPositionsAction);
    AddMenuItem(MenuProjectPointersTableLinksResetAction);
    AddMenuItem(nil);
    AddMenuItem(MenuProjectPointersVarOptionsAction);
   end;
   // PtrPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
  end;
  CustomPopup.Popup(ClientOrigin.X + X, ClientOrigin.Y + Y);
 end;
end;

function WideStringToString(const Value: WideString): String;
begin
{$IFDEF FORCED_UNICODE}
  Result := Value;
{$ELSE}
  Result := UTF16toUTF8(Value);
{$ENDIF}
end;

procedure TValues.FillValueListBox(Node: TKruptarTreeNodeType);
var
 PD: PDynamic;
 P: PDynamic;
 DLL: PDLL;
 I: Integer;
 CCC, Bef: Boolean;

 procedure InsertPointerSize(const FieldName: String;
           var Value: LongInt; Editable: Boolean);
 begin
  with MainForm.ValueListBox do
  begin
   if Value < 1 then
    InsertRow(FieldName, '0', True) else
   if Value = 4 then
    InsertRow(FieldName, PS_SIGNEDINT, True) else
   if Value = 5 then
    InsertRow(FieldName, PS_SIGNEDBYTE, True) else
   if Value = 6 then
    InsertRow(FieldName, PS_SIGNEDWORD, True) else
    InsertRow(FieldName, IntToStr(Value), True);
   with ItemProps[FieldName] do
   begin
    EditStyle := esPickList;
    PickList.Add('0');
    PickList.Add('1');
    PickList.Add('2');
    PickList.Add('3');
    PickList.Add(PS_SIGNEDINT);
    PickList.Add(PS_SIGNEDBYTE);
    PickList.Add(PS_SIGNEDWORD);
   end;
   with Add^ do
   begin
    ValueType := vtInteger;
    Min := 0;
    Max := 6;
    ValInteger := @Value;
    CanEdit := Editable;
   end;
  end;
 end;

begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 if CanCheck then MainForm.CheckValues;
 Proj := MainForm.SelectedRoot.Data;
 with MainForm do Case TTreeStateType(Node.StateIndex) of
  tsPointer: with PPointer(Node.Data)^, ValueListBox do
  begin
   Visible := True;
   CanCheck := False;
   ValueListBox.Col := 0;
   Strings.Clear;
   Self.Clear;
   if pnTable1 <> NIL then with pnTable1^ do
    InsertRow(PN_SRC_TBL, AnsiTbName, True) else
    InsertRow(PN_SRC_TBL, SMUNone, True);
   if pnTable2 <> NIL then with pnTable2^ do
    InsertRow(PN_DST_TBL, AnsiTbName, True) else
    InsertRow(PN_DST_TBL, SMUNone, True);
   P := PKruptarProject(SelectedRoot.Data)^.Tables^.Root;
   with ItemProps[0] do
   begin
    EditStyle := esPickList;
    PickList.Add(SMUNone);
    while P <> NIL do with P^, PTable(Data)^ do
    begin
     PickList.Add(AnsiTbName);
     P := Next;
    end;
   end;
   with ItemProps[1] do
   begin
    EditStyle := esPickList;
    PickList := ItemProps[0].PickList;
   end;
   with Add^ do
   begin
    ValueType := vtDynamic;
    ValPointer := Addr(pnTable1);
    Dynamics := PKruptarProject(SelectedRoot.Data)^.Tables;
   end;
   with Add^ do
   begin
    ValueType := vtDynamic;
    ValPointer := Addr(pnTable2);
    Dynamics := PKruptarProject(SelectedRoot.Data)^.Tables;
   end;
   InsertRow(PN_FIXED, BoolStr(pnFixed), True);
   with ItemProps[String(PN_FIXED)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(pnFixed);
   end;
   InsertRow('pnSrcPtrPos', 'h' + IntToHex(pnPos, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := 0;
    ValInteger := Addr(pnPos);
    CanEdit := False
   end;
   InsertRow('pnSrcReference', 'h' + IntToHex(pnReference, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := Low(LongInt);
    ValInteger := Addr(pnReference);
    CanEdit := False;
   end;
   InsertRow('pnDestPtrPos', 'h' + IntToHex(pnInsertPos, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := 0;
    ValInteger := Addr(pnInsertPos);
    CanEdit := True;
   end;
   InsertRow('pnDestReference', 'h' + IntToHex(pnInsertRef, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := Low(LongInt);
    ValInteger := Addr(pnInsertRef);
    CanEdit := True;
   end;
   PD := PPointerTable(Node.Parent.Data).ptParameters.Root;
   I := 1; Bef := False;
   while PD <> NIL do with PD^, PParameter(Data)^ do
   begin
    if not Bef and paBefore then
    with PPointerTable(Node.Parent.Data)^, ptParameters^ do
    begin
     I := 1 + (ptInterval - Before);
     Bef := True;
    end;
    Case paParamType of
     ptByte:
     begin
      InsertRow(paName, IntToStr(Byte(pnParamData[I])), True);
      with Add^ do
      begin
       ValueType := vtByte;
       Min := 0;
       Max := 255;
       ValByte := Addr(pnParamData[I]);
       Inc(I);
      end;
     end;
     ptWord:
     begin
      InsertRow(paName, IntToStr(Word(Addr(pnParamData[I])^)), True);
      with Add^ do
      begin
       ValueType := vtWord;
       Min := 0;
       Max := 65535;
       ValWord := Addr(pnParamData[I]);
       Inc(I, 2);
      end;
     end;
     ptShortInt:
     begin
      InsertRow(paName, IntToStr(ShortInt(Addr(pnParamData[I])^)), True);
      with Add^ do
      begin
       ValueType := vtShortInt;
       Min := Low(ShortInt);
       Max := High(ShortInt);
       ValShortInt := Addr(pnParamData[I]);
       Inc(I);
      end;
     end;
     ptSmallInt:
     begin
      InsertRow(paName, IntToStr(SmallInt(Addr(pnParamData[I])^)), True);
      with Add^ do
      begin
       ValueType := vtSmallInt;
       Min := Low(SmallInt);
       Max := High(SmallInt);
       ValSmallInt := Addr(pnParamData[I]);
       Inc(I, 2);
      end;
     end;
     ptLongInt:
     begin
      InsertRow(paName, IntToStr(LongInt(Addr(pnParamData[I])^)), True);
      with Add^ do
      begin
       ValueType := vtInteger;
       Min := Low(LongInt);
       Max := High(LongInt);
       ValInteger := Addr(pnParamData[I]);
       Inc(I, 4);
      end;
     end;
    end;
    PD := Next;
   end;
   CanCheck := True;
  end;
  tsPtrTable: with PPointerTable(Node.Data)^, ValueListBox do
  begin
   Visible := True;
   CanCheck := False;
   for I := 0 to Strings.Count - 1 do with ItemProps[I] do
   if EditStyle = esPickList then
   begin
    PickList.Clear;
    Strings[I] := '';
   end;
   ValueListBox.Col := 0;
   Strings.Clear;
   Self.Clear;
   if ptTable1 <> NIL then with ptTable1^ do
    InsertRow(PT_SRC_TBL, AnsiTbName, True) else
    InsertRow(PT_SRC_TBL, SMUNone, True);
   if ptTable2 <> NIL then with ptTable2^ do
    InsertRow(PT_DST_TBL, AnsiTbName, True) else
    InsertRow(PT_DST_TBL, SMUNone, True);
   P := PKruptarProject(SelectedRoot.Data).Tables.Root;
   with ItemProps[0] do
   begin
    EditStyle := esPickList;
    PickList.Add(SMUNone);
    while P <> NIL do with P^, PTable(Data)^ do
    begin
     PickList.Add(AnsiTbName);
     P := Next;
    end;
   end;
   with ItemProps[1] do
   begin
    EditStyle := esPickList;
    PickList := ItemProps[0].PickList;
   end; 
   with Add^ do
   begin
    ValueType := vtDynamic;
    ValPointer := Addr(ptTable1);
    Dynamics := PKruptarProject(SelectedRoot.Data).Tables;
   end;
   with Add^ do
   begin
    ValueType := vtDynamic;
    ValPointer := Addr(ptTable2);
    Dynamics := PKruptarProject(SelectedRoot.Data).Tables;
   end;
   if ptDictionary <> NIL then with PGroup(ptDictionary)^ do
    InsertRow(PT_DICT, AnsiGrName, True) else
    InsertRow(PT_DICT, SMUNone, True);
   with ItemProps[String(PT_DICT)] do
   begin
    EditStyle := esPickList;
    PickList.Add(SMUNone);
    P := PKruptarProject(SelectedRoot.Data).Groups.Root;
    while P <> NIL do with P^, PGroup(Data)^ do
    begin
     if grIsDictionary and (Data <> Node.Parent.Parent.Data) then
      PickList.Add(AnsiGrName);
     P := Next;
    end;
   end;
   with Add^ do
   begin
    ValueType := vtGroup;
    ValGroup := Addr(ptDictionary);
    Grp := Node.Parent.Parent.Data;
    Groups := PKruptarProject(SelectedRoot.Data).Groups;
   end;
   if ptInserter <> nil then with ptInserter^ do
   begin
    InsertPointerSize('ptDestPtrSize', piPointerSize, True);
    InsertRow('ptDestReference', 'h' + IntToHex(piReference, 2), True);
    with Add^ do
    begin
     ValueType := vtHex;
     Min := Low(LongInt);
     Max := High(LongInt);
     ValInteger := Addr(piReference);
     CanEdit := True;
    end;
    InsertRow('ptDestStrLen', IntToStr(piStringLength), True);
    with Add^ do
    begin
     ValueType := vtInteger;
     Min := 0;
     Max := 1024;
     ValInteger := Addr(piStringLength);
     CanEdit := True;
    end;
    InsertRow('ptDestAlignment', IntToStr(piAlignment), True);
    with Add^ do
    begin
     ValueType := vtInteger;
     Min := 1;
     Max := 8;
     ValInteger := Addr(piAlignment);
     CanEdit := True;
    end;
    InsertRow('ptDestCharSize', IntToStr(piCharSize), True);
    with Add^ do
    begin
     ValueType := vtInteger;
     Min := 1;
     Max := 2;
     ValInteger := Addr(piCharSize);
     CanEdit := True;
    end;
    InsertRow('ptDestShiftLeft', IntToStr(piShiftLeft), True);
    with Add^ do
    begin
     ValueType := vtInteger;
     Min := 0;
     Max := 31;
     ValInteger := Addr(piShiftLeft);
     CanEdit := True;
    end;
    InsertRow('ptDestAutoPtrStart', BoolStr(piAutoPtrStart), True);
    with ItemProps['ptDestAutoPtrStart'] do
    begin
     EditStyle := esPickList;
     PickList.Add(BoolStr(False));
     PickList.Add(BoolStr(True));
    end;
    with Add^ do
    begin
     ValueType := vtByte;
     Min := 0;
     Max := 1;
     ValByte := Addr(piAutoPtrStart);
     CanEdit := True;
    end;
    InsertRow('ptDestSplittedPtrs', BoolStr(piPunType), True);
    with ItemProps['ptDestSplittedPtrs'] do
    begin
     EditStyle := esPickList;
     PickList.Add(BoolStr(False));
     PickList.Add(BoolStr(True));
    end;
    with Add^ do
    begin
     ValueType := vtByte;
     Min := 0;
     Max := 1;
     ValByte := Addr(piPunType);
     CanEdit := True;
    end;
    InsertRow('ptDestSNESlorom', BoolStr(piSnesLoRom), True);
    with ItemProps['ptDestSNESlorom'] do
    begin
     EditStyle := esPickList;
     PickList.Add(BoolStr(False));
     PickList.Add(BoolStr(True));
    end;
    with Add^ do
    begin
     ValueType := vtByte;
     Min := 0;
     Max := 1;
     ValByte := Addr(piSnesLoRom);
     CanEdit := True;
    end;
   end;
   InsertPointerSize(PT_PTRSIZE, FptPointerSize, ptPointers^.Count = 0);
   InsertRow('ptReference', 'h' + IntToHex(ptReference, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := Low(LongInt);
    Max := High(LongInt);
    ValInteger := Addr(ptReference);
    CanEdit := True;
   end;
   InsertRow('ptInterval', IntToStr(ptInterval), True);
   with Add^ do
   begin
    ValueType := vtInteger;
    with ptParameters^ do
    begin
     Min := AfterFix + BeforeFix;
     if Min = 0 then
      Min := Low(LongInt);
    end;
    Max := High(LongInt);
    ValInteger := Addr(ptInterval);
    CanEdit := True;
   end;
   InsertRow('ptAlignment', IntToStr(ptAlignment), True);
   with Add^ do
   begin
    ValueType := vtInteger;
    Min := 1;
    Max := 8;
    ValInteger := Addr(FptMultiple);
    CanEdit := True;
   end;
   InsertRow('ptShiftLeft', IntToStr(ptShiftLeft), True);
   with Add^ do
   begin
    ValueType := vtInteger;
    Min := 0;
    Max := 31;
    ValInteger := Addr(FptShiftLeft);
    CanEdit := ptPointers^.Count = 0;
   end;
   InsertRow('ptCharSize', IntToStr(ptCharSize), True);
   with Add^ do
   begin
    ValueType := vtInteger;
    Min := 1;
    Max := 8;
    ValInteger := Addr(FptAlign);
    CanEdit := ptPointers^.Count = 0;
   end;
   InsertRow('ptStringLength', IntToStr(ptStringLength), True);
   with Add^ do
   begin
    ValueType := vtInteger;
    Min := 0;
    Max := 32768;
    ValInteger := Addr(FptStringLength);
    CanEdit := True;
   end;
   InsertRow(PT_MOTOROLA, BoolStr(ptMotorola), True);
   with ItemProps[String(PT_MOTOROLA)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptMotorola);
    CanEdit := ptPointers^.Count = 0
   end;
   InsertRow(PT_PUNTYPE, BoolStr(ptPunType), True);
   with ItemProps[String(PT_PUNTYPE)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptPunType);
    CanEdit := ptPointers.Count = 0;
   end;
   InsertRow(PT_LOROM, BoolStr(ptSnesLoRom), True);
   with ItemProps[String(PT_LOROM)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptSnesLoRom);
    CanEdit := ptPointers.Count = 0;
   end;
   InsertRow(PT_SIGNED, BoolStr(ptSigned), True);
   with ItemProps[String(PT_SIGNED)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptSigned);
    CanEdit := ptPointers^.Count = 0
   end;
   InsertRow(PT_AUTOSTART, BoolStr(ptAutoStart), True);
   with ItemProps[String(PT_AUTOSTART)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptAutoStart);
    CanEdit := ptPointers^.Count = 0
   end;
   InsertRow(PT_PTR2PTR, BoolStr(ptPtrToPtr), True);
   with ItemProps[String(PT_PTR2PTR)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptPtrToPtr);
    CanEdit := ptPointers^.Count = 0
   end;
   InsertRow(PT_SEEKSAME, BoolStr(ptSeekSame), True);
   with ItemProps[String(PT_SEEKSAME)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptSeekSame);
    CanEdit := True;
   end;
   InsertRow(PT_NOTINROM, BoolStr(ptNotInSource), True);
   with ItemProps[String(PT_NOTINROM)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(ptNotInSource);
    CanEdit := True;
   end;
   CanCheck := True;
  end;
  tsProject: with PKruptarProject(Node.Data)^, ValueListBox do
  begin
   Visible := True;
   CanCheck := False;
   for I := 0 to Strings.Count - 1 do with ItemProps[I] do
   if EditStyle = esPickList then
   begin
    PickList.Clear;
    Strings[I] := '';
   end;
   ValueListBox.Col := 0;
   Strings.Clear;
   Self.Clear;
   InsertRow(KP_SRC_ROM, InputROM, True);
   ItemProps[0].EditStyle := esEllipsis;
   with Add^ do
   begin
    Name := K7S(SMURomsFilter);
    ValueType := vtFileName;
    ValString := Addr(InputROM);
   end;
   InsertRow(KP_DST_ROM, OutPutRom, True);
   ItemProps[1].EditStyle := esEllipsis;
   with Add^ do
   begin
    Name := K7S(SMURomsFilter);
    ValueType := vtFileName;
    ValString := Addr(OutputROM);
   end;
   InsertRow('kpCodePage', '', True);
   with ItemProps[2] do
   begin
    EditStyle := esPickList;
    for I := 0 to Length(CodePageList) - 1 do with CodePageList[I] do
    begin
     if I = CodePageIndex then Cells[1, 2] := CodePageName;
     PickList.Add(CodePageName);
    end;
   end;
   with Add^ do
   begin
    Name := '';
    ValueType := vtByte;
    Min := 0;
    Max := Length(CodePageList) - 1;
    ValInteger := Addr(CodePageIndex);
   end;
   InsertRow('kpFlags', 'h' + IntToHex(Flags, 2), True);
   with Add^ do
   begin
    ValueType := vtHex;
    Min := Low(LongInt);
    Max := High(LongInt);
    ValInteger := Addr(Flags);
    CanEdit := True;
   end;
   CanCheck := True;
  end;
  tsGroup: with PGroup(Node.Data)^, ValueListBox do
  begin
   Visible := True;
   CCC := False;
   if grPointerTables^.Count > 0 then
   begin
    PD := grPointerTables^.Root;
    while PD <> NIL do with PD^, PPointerTable(Data)^ do
    begin
     if ptPointers^.Count > 0 then
     begin
      CCC := True;
      Break;
     end;
     PD := Next;
    end;
   end;
   CanCheck := False;
   for I := 0 to Strings.Count - 1 do with ItemProps[I] do
   if EditStyle = esPickList then
   begin
    PickList.Clear;
    Strings[I] := '';
   end;
   ValueListBox.Col := 0;
   Strings.Clear;
   Self.Clear;
   InsertRow('grPlugin', '', True);
   with ItemProps[0] do
   begin
    EditStyle := esPickList;
    DLLs^.GetByMethod(PickList);
    if PickList.Count > 0 then
    begin
     if grDLLname = '' then
      Cells[1, 0] := PickList.Strings[0] else
      Cells[1, 0] := grDLLName;
    end;
   end;
   with Add^ do
   begin
    ValueType := vtString;
    ValString := Addr(grDLLName);
    CanEdit := True; //not CCC;
   end;
   InsertRow(GR_ISDICT, BoolStr(grIsDictionary), True);
   with ItemProps[String(GR_ISDICT)] do
   begin
    EditStyle := esPickList;
    PickList.Add(BoolStr(False));
    PickList.Add(BoolStr(True));
   end;
   with Add^ do
   begin
    ValueType := vtByte;
    Min := 0;
    Max := 1;
    ValByte := Addr(grIsDictionary);
   end;
   CanCheck := True;
  end;
  else
  begin
   ValueListBox.EditorMode := False;
   ValueListBox.Visible := False;
   CanCheck := False;
  end;
 end;
end;

procedure TMainForm.ValueListBoxEditButtonClick(Sender: TObject);
var
 V: PValue;
begin
 if Values = NIL then Exit;
 V := Values.Get(ValueListBox.Row);
 if V = NIL then Exit;
 with V^ do Case ValueType of
  vtFileName: with ValueListBox do
  begin
   OpenDialog.Filter := V.Name;
   OpenDialog.FileName := ValString^;
   if OpenDialog.Execute then
   begin
    ValString^ := GetShortFileName(OpenDialog.FileName);
    Cells[Col, Row] := ValString^;
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end;
end;

procedure TMainForm.CheckValues;
var
 V: PValue;
 IP: TItemProp;
 I, J: Integer;
 BRow, BCol: Integer;
 Value: String;
 D: PDynamic;
 BB: Boolean;
begin
 if Values = NIL then Exit;
 BRow := ValueListBox.Row;
 if BRow < 0 then Exit;
 BCol := ValueListBox.Col;
 if BCol < 0 then Exit;
 Value := ValueListBox.Cells[BCol, BRow];
 V := Values.Get(BRow);
 if V = NIL then Exit;
 IP := ValueListBox.ItemProps[BRow];
 with V^, IP do
 begin
  if not CanEdit then
  begin
   ValueListBox.Options := [goVertLine, goHorzLine, goThumbTracking];
   ValueListBox.EditorMode := False;
   BB := CanCheck;
   CanCheck := False;
   Case ValueType of
    vtByte:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValByte^ - Min] else
      ValueListBox.Cells[BCol, BRow] := IntToStr(ValByte^);
    end;
    vtWord:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValWord^ - Min] else
      ValueListBox.Cells[BCol, BRow] := IntToStr(ValWord^);
    end;
    vtShortInt:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValShortInt^- Min] else
      ValueListBox.Cells[BCol, BRow] := IntToStr(ValShortInt^);
    end;
    vtSmallInt:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValSmallInt^- Min] else
      ValueListBox.Cells[BCol, BRow] := IntToStr(ValSmallInt^);
    end;
    vtInteger:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValInteger^ - Min] else
      ValueListBox.Cells[BCol, BRow] := IntToStr(ValInteger^);
    end;
    vtHex:
    begin
     if HasPickList then
      ValueListBox.Cells[BCol, BRow] := PickList.Strings[ValInteger^ - Min] else
      ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValInteger^, 2);
    end;
    vtString, vtFileName: ValueListBox.Cells[BCol, BRow] := ValString^;
   end;
   CanCheck := BB;
   ListBoxClick(NIL);   
   Exit;
  end else
   ValueListBox.Options := [goVertLine, goHorzLine, goEditing,
                            goAlwaysShowEditor, goThumbTracking];
 end;
 with IP do if HasPickList then with PickList do
 begin
  for I := 0 to Count - 1 do
  begin
   if (not V^.MatchCase and (UpperCase(Value) = UpperCase(Strings[I]))) or
      (V^.MatchCase and (Value = Strings[I])) then
   begin
    with V^ do Case ValueType of
     vtByte: if ValByte^ <> Min + I then
     begin
      ValByte^ := Min + I;
      SetSaved(MainUnit.Values.Proj, False);
     end;
     vtSmallInt: if ValSmallInt^ <> Min + I then
     begin
      ValSmallInt^ := Min + I;
      SetSaved(MainUnit.Values.Proj, False);
     end;
     vtShortInt: if ValShortInt^ <> Min + I then
     begin
      ValShortInt^ := Min + I;
      SetSaved(MainUnit.Values.Proj, False);
     end;
     vtWord: if ValWord^ <> Min + I then
     begin
      ValWord^ := Min + I;
      SetSaved(MainUnit.Values.Proj, False);
     end;
     vtInteger: if ValInteger^ <> Min + I then
     begin
      ValInteger^ := Min + I;
      SetSaved(MainUnit.Values.Proj, False); 
     end;
     vtString, vtFileName: if ValString^ <> Value then
     begin
      ValString^ := Value;
      SetSaved(MainUnit.Values.Proj, False);
     end;
     vtGroup: if I = 0 then
     begin
      if ValGroup^ <> NIL then
      begin
       ValGroup^ := NIL;
       SetSaved(MainUnit.Values.Proj, False);
      end;
     end else if Groups <> NIL then
     begin
      D := Groups.Root; J := 0;
      while D <> NIL do with D^, PGroup(Data)^ do
      begin
       if grIsDictionary and (Data <> Grp) then
       begin
        if J = I - 1 then
        begin
         if ValGroup^ <> Data then
         begin
          ValGroup^ := Data;
          SetSaved(MainUnit.Values.Proj, False);
         end;
         ListBoxClick(NIL);
         Exit;
        end;
        Inc(J);
       end;
       D := Next;
      end;
     end;
     vtDynamic: if I = 0 then
     begin
      if ValPointer^ <> NIL then
      begin
       ValPointer^ := NIL;
       SetSaved(MainUnit.Values.Proj, False);
      end;
     end else if Dynamics <> NIL then
     begin
      D := Dynamics^.Root; J := 0;
      while D <> NIL do with D^ do
      begin
       if J = I - 1 then
       begin
        if ValPointer^ <> Data then
        begin
         ValPointer^ := Data;
         SetSaved(MainUnit.Values.Proj, False);
        end;
        ListBoxClick(NIL);
        Exit;
       end;
       Inc(J);
       D := Next;
      end;
     end;
    end;
    ListBoxClick(NIL);
    Exit;
   end;
  end;
  with V^ do
  begin
   AddStateString(etError, '', 0, WideFormat(K7S(SMUStringNotFound), [Value]));
   Case ValueType of
    vtByte:     ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[ValByte^ - Min];
    vtSmallInt: ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[ValSmallInt^ - Min];
    vtShortInt: ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[ValShortInt^ - Min];
    vtWord:     ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[ValWord^ - Min];
    vtInteger,
    vtHex:      ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[ValInteger^ - Min];
    vtString,
    vtFileName: ValueListBox.Cells[BCol, BRow] :=
                ValString^;
    vtDynamic,
    vtGroup:    ValueListBox.Cells[BCol, BRow] :=
                PickList.Strings[0];
   end;
  end;
  ListBoxClick(NIL);
 end else
 begin
  with V^ do Case ValueType of
   vtByte:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     Value := ValueListBox.Cells[BCol, BRow];
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValByte^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValByte^);
    end else
    if (I >= Min) and (I <= Max) then
    begin
     if ValByte^ <> I then
     begin
      ValByte^ := I;
      ValueListBox.Cells[BCol, BRow] := IntToStr(I);
      SetSaved(MainUnit.Values.Proj, False);
     end;
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValByte^);
    end;
   end;
   vtShortInt:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     Value := ValueListBox.Cells[BCol, BRow];
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValShortInt^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValShortInt^);
    end else
    if (I >= Min) and (I <= Max) then
    begin
     if ValShortInt^ <> I then
     begin
      ValShortInt^ := I;
      ValueListBox.Cells[BCol, BRow] := IntToStr(I);
      SetSaved(MainUnit.Values.Proj, False);
     end;
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValShortInt^);
    end;
   end;
   vtSmallInt:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     Value := ValueListBox.Cells[BCol, BRow];
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValSmallInt^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValSmallInt^);
    end else
    if (I >= Min) and (I <= Max) then
    begin
     if ValSmallInt^ <> I then
     begin
      ValSmallInt^ := I;
      ValueListBox.Cells[BCol, BRow] := IntToStr(I);
      SetSaved(MainUnit.Values.Proj, False);
     end;
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValSmallInt^);
    end;
   end;
   vtWord:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     Value := ValueListBox.Cells[BCol, BRow];
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValWord^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValWord^);
    end else
    if (I >= Min) and (I <= Max) then
    begin
     if ValWord^ <> I then
     begin
      ValWord^ := I;
      ValueListBox.Cells[BCol, BRow] := IntToStr(I);
      SetSaved(MainUnit.Values.Proj, False);
     end; 
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValWord^);
    end;
   end;
   vtInteger:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValInteger^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValInteger^);
    end else
    if (I >= Min) and (I <= Max)then
    begin
     if ValInteger^ <> I then
     begin
      ValInteger^ := I;
      ValueListBox.Cells[BCol, BRow] := IntToStr(I);
      SetSaved(MainUnit.Values.Proj, False);
     end;
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := IntToStr(ValInteger^);
    end;
   end;
   vtHex:
   begin
    I := GetLDW(Value);
    if HexError then
    begin
     AddStateString(etError, '', 0, K7S(SMUHexError));
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValInteger^, 2);
    end else if IntError then
    begin
     AddStateString(etError, '', 0, K7S(SMUIntegerError));
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValInteger^, 2);
    end else
    if (I >= Min) and (I <= Max) then
    begin
     if ValInteger^ <> I then
     begin
      ValInteger^ := I;
      ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(I, 2);
      SetSaved(MainUnit.Values.Proj, False);
     end;
    end else
    begin
     AddStateString(etError, '', 0,
      WideFormat(K7S(SMUIndexOutOfBounds), [Min, Max]));
     ValueListBox.Cells[BCol, BRow] := 'h' + IntToHex(ValInteger^, 2);
    end;
   end;
   vtString: if ValString^ <> Value then
   begin
    ValString^ := Value;
    SetSaved(MainUnit.Values.Proj, False);
   end;
   vtFileName: if (Value = '') or FileExists(Value) then
   begin
    if ValString^ <> Value then
    begin
     ValString^ := Value;
     SetSaved(MainUnit.Values.Proj, False);
    end;
   end else
   begin
    if Value <> '' then
     AddStateString(etError, '', 0, WideFormat(K7S(SMUFileNotFound), [Value]));
    ValueListBox.Cells[BCol, BRow] := ValString^;
   end;
   vtDynamics: ValueListBox.Cells[BCol, BRow] := Name;
  end;
  ListBoxClick(NIL);
 end;
end;

procedure TMainForm.ValueListBoxKeyPress(Sender: TObject; var Key: Char);
var
 V: PValue;
begin
 if not CanCheck then Exit;
 if Key = #13 then CheckValues else
 begin
  if Values = NIL then Exit;
  V := Values.Get(ValueListBox.Row);
  if V = NIL then Exit;
  if not V.CanEdit then Key := #0;
 end;
end;

procedure TMainForm.ValueListBoxSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
 if CanCheck then CheckValues;
end;

procedure TMainForm.MenuProjectGroupAddActionExecute(Sender: TObject);
var
 P: PDynamic;
 I: Integer;
 Founded: Boolean;
 S: WideString;
 Node: TKruptarTreeNodeType;
begin
 if SelectedRoot = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False; 
 with PKruptarProject(SelectedRoot.Data)^ do
 begin
  I := 1;
  repeat
   Founded := False;
   S := WideFormat('Group%d', [I]);
   P := Groups^.Root;
   while P <> NIL do with P^, PGroup(Data)^ do
   begin
    if S = grName then
    begin
     Inc(I);
     Founded := True;
     Break;
    end;
    P := Next;
   end;
  until not Founded;
 end;
 with PKruptarProject(SelectedRoot.Data)^ do
 begin
  SelectedData := Groups.Add;
  with PGroup(SelectedData)^ do
  begin
   grName := S;
   grDLLName := '';
  end;
  SetSaved(SelectedRoot.Data, False);
//  GroupsTabControl.Tabs.Clear;
//  RefreshGroups(SelectedRoot.GetLastChild, True);
  Node := TreeView.Items.AddChild(SelectedRoot.GetLastChild, S);
  Node.ImageIndex := Icons[tsGroup];
  Node.SelectedIndex := Icons[tsGroup];
  Node.StateIndex := Integer(tsGroup);
  Node.Data := SelectedData;
  TreeView.Selected := Node;  
  RefreshGroup(Node, True);
  Node.Expand(False);  
  TreeViewChange(Sender, Node);
//  TreeView.Selected.Expand(False);
 end;
end;

procedure TTable.RemoveByIndex(R: PTableItem; Index: Integer);
var
 I: Integer;
 P, PR: PTableItem;
begin
 if Index < 0 then Exit;
 I := 0; P := R; PR := NIL;
 while P <> NIL do
 begin
  if I = Index then Break;
  PR := P;
  P := P^.Next;
 end;
 if I < Index then Exit;
 if R = Root then with P^ do
 begin
  if Index = 0 then
  begin
   tiCodes := '';
   tiString := '';
   if Root = Cur then Cur := Next;
   Root := Next;
   Dispose(P);
   Dec(Count);
  end else if Index = Count - 1 then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   Cur := PR;
   Dispose(P);
   Dec(Count);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(Count);
  end;
 end else if R = EndRoot then with P^ do
 begin
  if Index = 0 then
  begin
   tiCodes := '';
   tiString := '';
   if EndRoot = EndCur then EndCur := Next;
   EndRoot := Next;
   Dispose(P);
   Dec(EndCount);
  end else if Index = Count - 1 then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   EndCur := PR;
   Dispose(P);
   Dec(EndCount);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(EndCount);
  end;
 end else if R = ElRoot then with P^ do
 begin
  if Index = 0 then
  begin
   tiCodes := '';
   tiString := '';
   if ElRoot = ElCur then ElCur := Next;
   ElRoot := Next;
   Dispose(P);
   Dec(ElCount);
  end else if Index = Count - 1 then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   ElCur := PR;
   Dispose(P);
   Dec(ElCount);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(ElCount);
  end;
 end;
 RefreshMax;
end;

procedure TTable.RemoveByItem(R: PTableItem; Item: PTableItem);
var
 P, PR: PTableItem;
begin
 if Item = NIL then Exit;
 P := R; PR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 if R = Root then with P^ do
 begin
  if P = Root then
  begin
   tiCodes := '';
   tiString := '';
   if Root = Cur then Cur := Next;
   Root := Next;
   Dispose(P);
   Dec(Count);
  end else if Next = NIL then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   Cur := PR;
   Dispose(P);
   Dec(Count);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(Count);
  end;
 end else if R = EndRoot then with P^ do
 begin
  if P = EndRoot then
  begin
   tiCodes := '';
   tiString := '';
   if EndRoot = EndCur then EndCur := Next;
   EndRoot := Next;
   Dispose(P);
   Dec(EndCount);
  end else if Next = NIL then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   EndCur := PR;
   Dispose(P);
   Dec(EndCount);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(EndCount);
  end;
 end else if R = ElRoot then with P^ do
 begin
  if P = ElRoot then
  begin
   tiCodes := '';
   tiString := '';
   if ElRoot = ElCur then ElCur := Next;
   ElRoot := Next;
   Dispose(P);
   Dec(ElCount);
  end else if Next = NIL then
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := NIL;
   ElCur := PR;
   Dispose(P);
   Dec(ElCount);
  end else
  begin
   tiCodes := '';
   tiString := '';
   PR^.Next := P^.Next;
   Dispose(P);
   Dec(ElCount);
  end;
 end;
 RefreshMax;
end;

procedure TBlocks.Remove(Item: PBlock);
var
 P, PR: PBlock;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TGroups.Remove(Item: PGroup);
var
 P, PR: PDynamic;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P^.Data = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if Data <> NIL then Dispose(PGroup(Data), Done);
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TParameters.Remove(Item: PParameter);
var
 P, PR: PDynamic;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P^.Data = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if Data <> NIL then Dispose(PParameter(Data), Done);
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TPointerTables.Remove(Item: PPointerTable);
var
 P, PR: PDynamic;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P^.Data = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if Data <> NIL then Dispose(PPointerTable(Data), Done);
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TPointers.Remove(Item: PPointer);
var
 P, PR: PPointer;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if pnStrings <> NIL then Dispose(pnStrings, Done);
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TProjects.Remove(Item: PKruptarProject);
var
 P, PR: PDynamic;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P^.Data = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if Data <> NIL then Dispose(PKruptarProject(Data), Done);
  Dispose(P);
  Dec(Count);
 end;
end;

function TGroups.Find(Name: WideString): PGroup;
var
 P: PDynamic;
begin
 Result := NIL;
 Name := WideUpperCase(Name);
 P := Root;
 while P <> NIL do with P^, PGroup(Data)^ do
 begin
  if Name = WideUpperCase(grName) then
  begin
   Result := Data;
   Exit;
  end;
  P := P^.Next;
 end;
end;

function TProjects.Find(Name: WideString): PKruptarProject;
var
 P: PDynamic;
begin
 Result := NIL;
 Name := WideUpperCase(Name);
 P := Root;
 while P <> NIL do with P^, PKruptarProject(Data)^ do
 begin
  if Name = WideUpperCase(Dir + ProjectFile) then
  begin
   Result := Data;
   Exit;
  end;
  P := Next;
 end;
end;

function TProjects.GetIndex(KP: PKruptarProject): Integer;
var
 P: PDynamic;
begin
 Result := 0;
 P := Root;
 while P <> NIL do with P^ do
 begin
  if KP = Data then Exit;
  Inc(Result);
  P := Next;
 end;
 Result := -1;
end;

function TPointerTables.Find(Name: WideString): PPointerTable;
var
 P: PDynamic;
begin
 Result := NIL;
 Name := WideUpperCase(Name);
 P := Root;
 while P <> NIL do with P^, PPointerTable(Data)^ do
 begin
  if Name = WideUpperCase(ptName) then
  begin
   Result := Data;
   Exit;
  end;
  P := P^.Next;
 end;
end;

function TTables.GetIndex(Item: PTable): Integer;
var
 P: PDynamic; I: Integer;
begin
 Result := -1;
 if Item = NIL then Exit;
 P := Root; I := 0;
 while P <> NIL do with P^ do
 begin
  if Item = Data then
  begin
   Result := I;
   Exit;
  end;
  Inc(I);
  P := P^.Next;
 end;
end;

function TGroups.GetIndex(Item: PGroup): Integer;
var
 P: PDynamic;
 I: Integer;
begin
 Result := -1;
 if Item = NIL then Exit;
 P := Root; I := 0;
 while P <> NIL do with P^ do
 begin
  if Item = Data then
  begin
   Result := I;
   Exit;
  end;
  Inc(I);
  P := P^.Next;
 end;
end;

function TTables.Find(Name: WideString): PTable;
var
 P: PDynamic;
begin
 Result := NIL;
 Name := WideUpperCase(Name);
 P := Root;
 while P <> NIL do with P^, PTable(Data)^ do
 begin
  if Name = WideUpperCase(tbName) then
  begin
   Result := Data;
   Exit;
  end;
  P := P^.Next;
 end;
end;

procedure TTables.Remove(Item: PTable);
var
 P, PR: PDynamic;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P^.Data = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  if Data <> NIL then Dispose(PTable(Data), Done);
  Dispose(P);
  Dec(Count);
 end;
end;

procedure TMainForm.TreeViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
 Node, N: TKruptarTreeNodeType;
begin
 if TreeView.IsEditing then Exit;
 Case Key of
  VK_Delete: MenuProjectItemRemoveActionExecute(Sender);
  VK_F2:
  begin
   Node := TreeView.Selected;
   if Node <> NIL then Node.EditText;
  end;
  VK_BACK:
  begin
   Key := 0;
   Node := TreeView.Selected;
   if Node = NIL then Exit;
   N := Node.Parent;
   if N <> NIL then
   begin
    N.Collapse(False);
    NotChange := True;
    TreeView.Selected := N;
    NotChange := False;
    TreeViewChange(Sender, N);
   end else Node.Collapse(False);
  end;
  VK_SPACE:
  begin
   Key := 0;
   N := TreeView.Selected;
   if N = NIL then Exit;
   N.Expand(False);
  end;
 end;
end;

procedure TMainForm.MenuProjectTableAnsiFillActionExecute(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 I: Integer;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 if TTreeStateType(Node.StateIndex) = tsTableItem then
 begin
  Node := Node.Parent;
  if Node = NIL then Exit;
 end;
 if Node.Parent = NIL then Exit;
 with PTable(Node.Parent.Data)^ do
 begin
  ClearNodes(Root);
  for I := $20 to $FF do
  begin
   SelectedData := Add;
   with PTableItem(SelectedData)^ do
   begin
    tiCodes := AnsiChar(I);
    tiString := Char(AnsiChar(I));
   end;
  end;
  RefreshMax;
  RefreshTable(Node.Parent);
  SetSaved(SelectedRoot.Data, False);
 end;
end;

procedure TMainForm.ListBoxClick(Sender: TObject);
var
// I: Integer;
// P: PPointer;
 S: PString;
 KP: PKruptarProject;
begin
 CurString := NIL;
 CurPointer := NIL;
 if (SelectedRoot <> NIL) and (CurGroup <> NIL) and
    (CurGroup.grCurPtrTable <> NIL) then
 with CurGroup^, grCurPtrTable^ do
 begin
  KP := SelectedRoot.Data;
  ptItemIndex := ListBox.ItemIndex;
  if ListBox.Count <= 0 then
  begin
   ptItemIndex := 0;
   TranslateMemo.ReadOnly := True;
   MemoCanChange := False;
   OriginalMemo.Text := '';
   TranslateMemo.Text := '';
   OriginalStatusBar.Panels[2].Text := '';
   TranslateStatusBar.Panels[3].Text := '';
   Exit;
  end else TranslateMemo.ReadOnly := False;
  S := GetStringByIndex(ptItemIndex);
  if S <> nil then with S^ do
  begin
   CurPointer := FLastPointer;
   CurString := S;
   OriginalMemo.Text := ConvertString(CurPointer.pnTable1, stOriginal,
                         KP.Flags and KP_CRLF_CONVERT <> 0);
   OriginalStatusBar.Panels[2].Text :=
    WideFormat(SMUSizeV, [Length(stOriginal)]);
   MemoCanChange := False;
   TranslateMemo.Lines.Text := stNew;
   MemoCanChange := True;
   with CurPointer^ do if Assigned(pnTable2) then
   begin
    TranslateStatusBar.Panels[3].Text :=
    WideFormat(SMUSizeV, [Length(pnTable2.StringToData(stNew,
                         ptCharSize,
                         KP.Flags and KP_CRLF_CONVERT <> 0))]);
   end else TranslateStatusBar.Panels[3].Text := '';
  end;
 end else
 begin
  OriginalMemo.Text := '';
  TranslateMemo.Text := '';
  TranslateMemo.ReadOnly := True;
  OriginalStatusBar.Panels[2].Text := '';
  TranslateStatusBar.Panels[3].Text := '';
 end;
end;

function ConvertToListString(Index: Integer; const S: WideString): WideString;
begin
 Result := Copy(S, 1, 256);
 if Length(S) > Length(Result) then Result := Result + '...';
 Result := WideFormat('%6.6d %s', [Index, Result]);
end;

procedure TMainForm.RefreshList;
var
 PP: PPointer;
 PS: PString;
 I: Integer;
 SS: WideString;
begin
 if SelectedRoot = NIL then Exit;
 I := 0;
 SendMessage(ListBox.Handle, WM_SETREDRAW, 0, 0);
 if CurGroup <> NIL then with CurGroup^ do
 begin
  with grCurPtrTable^ do
  begin
   PP := ptPointers^.Root;
   while PP <> NIL do with PP^ do
   begin
    PS := pnStrings^.Root;
    while PS <> NIL do with PS^, ListBox do
    begin
     SS := ConvertToListString(I + 1, stNew);
     if I < Count then
      Items.Strings[I] := SS else
      Items.Add(SS);
     Inc(I);
     PS := Next;
    end;
    PP := Next;
   end;
  end;
  with ListBox do
  begin
   if (I = 0) or (grCurPtrTable = NIL) then
   begin
    Clear;
    ListBoxClick(MainForm);
    SendMessage(Handle, WM_SETREDRAW, 1, 0);
    Repaint;
    Exit;
   end;
   ListBox.ClearSelection;
   while Count > I do Items.Delete(Count - 1);
   with grCurPtrTable^ do if ptItemIndex < Count then
   begin
    ItemIndex := ptItemIndex;
    TopIndex := ptItemIndex;
   end;
   if ItemIndex = -1 then ItemIndex := 0;
   ListBox.Selected[ItemIndex] := True;
   ListBoxClick(MainForm);
   SendMessage(Handle, WM_SETREDRAW, 1, 0);
   Repaint;
  end;
 end;
end;

procedure TPointers.MoveUp(Item: PPointer);
var
 P, PR, PRR, N: PPointer;
begin
 if Item = NIL then Exit;
 P := Root;
 PR := NIL;
 PRR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PRR := PR;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 if PR = NIL then Exit;
 with P^ do
 begin
  N := P^.Next;
  P^.Next := PR;
  PR^.Next := N;
  if PR = Root then Root := P else PRR^.Next := P;
  if Cur = P then Cur := PR;
 end;
end;

procedure TBlocks.MoveUp(Item: PBlock);
var
 P, PR, PRR, N: PBlock;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL; PRR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PRR := PR;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 if PR = NIL then Exit;
 with P^ do
 begin
  N := P^.Next;
  P^.Next := PR;
  PR^.Next := N;
  if PR = Root then Root := P else PRR^.Next := P;
  if Cur = P then Cur := PR;
 end;
end;

procedure TTable.MoveUpByItem(R, Item: PTableItem);
var
 P, PR, PRR, N: PTableItem;
begin
 if Item = NIL then Exit;
 P := R; PR := NIL; PRR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PRR := PR;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 if PR = NIL then Exit;
 with P^ do
 begin
  N := P^.Next;
  P^.Next := PR;
  PR^.Next := N;
  if PR = R then
  begin
   if R = Root then Root := P else
   if R = EndRoot then EndRoot := P else
   if R = ElRoot then ElRoot := P;
  end else PRR^.Next := P;
  if R = Root then
  begin
   if Cur = P then
      Cur := PR;
  end else
  if R = EndRoot then
  begin
   if EndCur = P then
      EndCur := PR;
  end else
  if R = ElRoot then
  begin
   if ElCur = P then
      ElCur := PR;
  end;
 end;
end;

constructor TParameters.Init;
begin
 inherited Init(SizeOf(TParameter));
 Before := 0;
 AfterFix := 0;
 BeforeFix := 0;
end;

function TParameters.Add: PParameter;
begin
 inherited Add;
 New(Result, Init);
 Cur.Data := Result;
 Result.Owner := Cur;
end;

procedure TParameters.Clear;
var
 N: PDynamic;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  with Root^ do if Data <> NIL then Dispose(PParameter(Data), Done);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

destructor TParameters.Done;
begin
 Clear;
 DataSize := 0;
end;

destructor TParameter.Done;
begin
 paName := '';
 paBefore := False;
 paParamType := ptByte;
 paFlags := 0;
end;

constructor TParameter.Init;
begin
 paName := '';
 paBefore := False;
 paParamType := ptByte;
 paFlags := 0;
end;

type
 TAbcd = packed record
  A: Byte;
  B: Byte;
  C: Byte;
  D: Byte;
 end;

function WordMotorola(X: DWord): LongInt;
asm
 xchg   al,ah
 movzx  eax,ax
end;

function TripleMotorola(X: DWord): LongInt;
begin
 with TAbcd(Result) do
 begin
  A := TAbcd(X).C;
  B := TAbcd(X).B;
  C := TAbcd(X).A;
  D := 0;
 end;
end;

function DWordMotorola(X: DWord): LongInt;
asm
 bswap eax
end;

function TPointerTable.GetPointerPun(ROM: PBytes; pnPos, C, BlockStart: LongInt;
                                     var pnReference: LongInt): LongInt;
var
 I, SZ: LongInt;
begin
 Result := 0;
 if ptPointerSize > 4 then
  SZ := ptPointerSize - 4 else
  SZ := ptPointerSize;
 if ptPointerSize <= 2 then
  for I := 0 to SZ - 1 do
   Result := Result + (ROM^[pnPos + I * C] shl (I * 8)) else
 begin
  if ptMotorola then
  begin
   case ptPointerSize of
    3: Result := ROM[pnPos] or
             (Word(Addr(ROM[pnPos + C])^) shl 8);
    4: Result := Word(Addr(ROM[pnPos])^) or
             (Word(Addr(ROM[pnPos + C])^) shl 16);
   end;
  end else
  begin
   Result := Word(Addr(ROM[pnPos])^);
   case ptPointerSize of
    3: Result := Result or (ROM[pnPos + C] shl 16);
    4: Result := Result or (Word(Addr(ROM[pnPos + C])^) shl 16);
   end;
  end;
 end;
 if ptMotorola then Case ptPointerSize of
  2, 6: Result := WordMotorola(Result);
  3: Result := TripleMotorola(Result);
  4: Result := DWordMotorola(Result);
 end;
 if ptSnesLoROM then
  Result := ((Result shr 16) shl 15) or (Result and $7FFF);
 if (ptShiftLeft < 0) or (ptShiftLeft > 31) then ptShiftLeft := 0;
 Result := Result shl ptShiftLeft;
 if ptAutoStart then
 begin
  Result := Result + BlockStart;
  pnReference := BlockStart;
 end else
 begin
  Result := Result + ptReference;
  pnReference := ptReference;
 end;
end;

function TPointerTable.GetPointer(var Data; pnPos, BlockStart: LongInt;
                                  var pnReference: LongInt): LongInt;
var
 SZ: LongInt;
begin
 Result := 0;
 if ptPointerSize > 4 then
  SZ := ptPointerSize - 4 else
  SZ := ptPointerSize;
 Move(Data, Result, SZ);
 if ptMotorola then Case ptPointerSize of
  2, 6: Result := WordMotorola(Result);
  3: Result := TripleMotorola(Result);
  4: Result := DWordMotorola(Result);
 end;
 if (ptShiftLeft < 0) or (ptShiftLeft > 31) then ptShiftLeft := 0;
 Case ptPointerSize of
  5: Result := ShortInt(Result);
  6: Result := SmallInt(Result);
 end;
 if ptSnesLoROM then
  Result := ((Result shr 16) shl 15) or (Result and $7FFF);
 Result := Result shl ptShiftLeft;
 if ptSigned then Inc(Result, pnPos);
 if ptAutoStart then
 begin
  Result := Result + BlockStart;
  pnReference := BlockStart;
 end else
 begin
  Result := Result + ptReference;
  pnReference := ptReference;
 end;
end;

procedure TMainForm.MenuProjectPointersAddActionExecute(Sender: TObject);
var
 DLL: PDLL;
 Node: TKruptarTreeNodeType;
 SD: PPointer;
begin
 if SelectedList = nil then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with PKruptarProject(SelectedRoot.Data)^, SelectedList^ do
 begin
  if not FieldFileExists(KP_SRC_ROM, InputROM, False) then Exit;
  if ptTable1 = NIL then
  begin
   LocMessageDlg(K7S(SMUTableNotSelected), mtError, [mbOk], 0);
   Exit;
  end;
  with OwnedBy^ do
  begin
   DLL := DLLs.Get(grDLLName);
   if DLL = NIL then
   begin
    LocMessageDlg(WideFormat(K7S(SMULibraryNotFound), [grDLLName]),
                  mtError, [mbOk], 0);
    Exit;
   end;
   if (ptStringLength <= 0) and (DLL.GetMethod <> tmNone) and DLL.NeedEnd then
   begin
    if ptTable1.EndCount <= 0 then
    begin
     LocMessageDlg(WideFormat(K7S(SMUStringTerminateCodeNotExist),
                   [ptTable1.tbName]), mtError, [mbOk], 0);
     Exit;
    end
   end;
   with AddPointersDialog do
   begin
    if ((ptPointerSize > 0) and Execute(K7S(SMUAddPointersBlock))) or
       ((ptPointerSize <=0) and Execute(K7S(SMUAddTextBlock))) then
    begin
     SD := nil;
     if LoadROM then
     try
      SD := SelectedList.ptPointers.Cur;
//      ptItemIndex := ListBox.Count;
      AddPointersList(BlockStart, BlockEnd, SelectedRoot.Data, DLL, SelectedList);
     finally
      FreeROM;
      if SD = nil then SelectedData :=  SelectedList.ptPointers.Root else
      with SD^ do if Next <> nil then SelectedData := Next;
      Node := TreeView.Selected;
      if Node.StateIndex = Integer(tsPointer) then
       Node := Node.Parent;
      RefreshPtrTable(Node);
      TreeViewChange(nil, TreeView.Selected);
      if CurGroup.grCurPtrTable = Node.Data then
       RefreshList;
      TreeViewDblClick(nil);
      SetSaved(SelectedRoot.Data, False);
{      FreeROM;
      Node := TreeView.Selected;
      if Node.StateIndex = Integer(tsPointer) then
       Node := Node.Parent;
      RefreshPtrTable(Node);
      TreeViewChange(Sender, TreeView.Selected);
      PTablesTabControlChange(Sender);
 }
     end;
    end;
   end;
  end;
 end;
end;

constructor TDLLS.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

destructor TDLLS.Done;
var N: PDLL;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Root.Name := '';
  FreeLibrary(Root.Handle);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

function TDLLS.Add(const FName: String): PDLL;
var
 H: THandle;
 S: WideString;
begin
 Result := NIL;
 S := ExeDir + 'Lib' + PathDelim + FName;
 if not WideFileExists(S) then Exit;
 H := LoadLibraryW(PWideChar(S));
 if H <> 0 then
 begin
  New(Result);
  FillChar(Result^, SizeOf(TDLL), 0);
  with Result^ do
  begin
   Handle := H;
   @GetMethod := GetProcAddress(H, 'GetMethod');
   @SetVariables := GetProcAddress(H, 'SetVariables');
   @GetData := GetProcAddress(H, 'GetData');
   @GetStrings := GetProcAddress(H, 'GetStrings');
   @DisposeStrings := GetProcAddress(H, 'DisposeStrings');
   @NeedEnd := GetProcAddress(H, 'NeedEnd');
   @Description := GetProcAddress(H, 'Description');
   if (Addr(GetMethod) = NIL) or
      (Addr(SetVariables) = NIL) or
      (Addr(GetData) = NIL) or
      (Addr(GetStrings) = NIL) or
      (Addr(DisposeStrings) = NIL) or
      (Addr(NeedEnd) = NIL) then
   begin
    FreeLibrary(Handle);
    Dispose(Result);
    Result := NIL;
    Exit;
   end;
   Name := FName;
   Next := NIL;
  end;
  if Root = NIL then Root := Result else Cur^.Next := Result;
  Cur := Result;
  Inc(Count);
 end;
end;

function TDLLS.Get(const FName: String): PDLL;
var
 N: PDLL;
begin
 N := Root;
 while N <> NIL do with N^ do
 begin
  if FName = Name then
  begin
   Result := N;
   Exit;
  end;
  N := Next;
 end;
 Result := NIL;
end;

procedure TDLLS.Remove(Item: PDLL);
var
 P, PR: PDLL;
begin
 if Item = NIL then Exit;
 P := Root; PR := NIL;
 while P <> NIL do
 begin
  if P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 if P = NIL then Exit;
 with P^ do
 begin
  if Root = P then
  begin
   if Root = Cur then Cur := Next;
   Root := Next;
  end else if Next = NIL then
  begin
   PR^.Next := NIL;
   Cur := PR;
  end else PR^.Next := P^.Next;
  P^.Name := '';
  FreeLibrary(P^.Handle);
  Dispose(P);
  Dec(Count);
 end;
end;

function HexDataToString(const S: RawByteString): WideString;
var I: Integer;
begin
 Result := '';
 for I := 1 to Length(S) do
  Result := Result + '/' + IntToHex(Byte(S[I]), 2);
end;

function TTable.DataToString(var Data; Len, Align: LongInt;
  CRLF: Boolean): WideString;
var
  B: PByte;
  T: PTableItem;
  C, J: Integer;
begin
  B := Addr(Data); Result := '';
  while Len > 0 do
  begin
    T := GetString(B^, Len);
    if T <> NIL then with T^ do
    begin
      J := Length(tiCodes);
      Case tiState of
       isCode: Result := Result + tiString;
       isBreak:
       begin
        if not CRLF or (T <> ElRoot) then
         for C := 1 to J do
          Result := Result + '/' + IntToHex(Byte(tiCodes[C]), 2);
        Result := Result + #13 + #10;
       end;
       isTerminate:
       begin
        for C := 1 to J do
         Result := Result + '/' + IntToHex(Byte(tiCodes[C]), 2);
       end;
      end;

      Inc(B, J);
      Dec(Len, J);

      while J mod Align <> 0 do
      begin
       Result := Result + '/' + IntToHex(B^, 2);

       Inc(J);
       Inc(B);
       Dec(Len);
      end;

    end else
    for J := 1 to Align do
    begin
      Result := Result + '/' + IntToHex(B^, 2);
      Inc(B);
      Dec(Len);
    end;
  end;
end;

procedure TMainForm.GroupsTabControlChange(Sender: TObject);
var
 I: Integer;
 P, D: PDynamic;
begin
 with GroupsTabControl do
 begin
  FSaveTab1Index := TabIndex;
  if (TabIndex = -1) or (SelectedRoot = NIL) then
  begin
   Tabs.Clear;
   CurString := NIL;
   CurPointer := NIL;
   ListBox.Clear;
   ListBoxClick(Sender);
   PTablesTabControl.Tabs.Clear;
  end else
  with PKruptarProject(SelectedRoot.Data)^ do
  begin
   P := Groups^.Root; I := 0;
   while P <> NIL do with P^ do
   begin
    if I = TabIndex then
    begin
     CurGroup := Data;
     Break;
    end;
    Inc(I);
    P := Next;
   end;
   with CurGroup^, PTablesTabControl do
   begin
    Tabs.Clear;
    D := grPointerTables^.Root;
    if D = NIL then
    begin
     ListBox.Clear;
     ListBoxClick(Sender);
    end;
    while D <> NIL do with D^, PPointerTable(Data)^ do
    begin
     Tabs.Add(ptName);
     if grCurPtrTable = NIL then
     begin
      grCurPtrTable := Data;
      TabIndex := 0;
      PTablesTabControlChange(MainForm);
     end else if grCurPtrTable = Data then
     begin
      TabIndex := Tabs.Count - 1;
      PTablesTabControlChange(MainForm);
     end;
     D := Next;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.PTablesTabControlChange(Sender: TObject);
var
 P: PDynamic;
 I: Integer;
begin
 FSaveTab2Index := PTablesTabControl.TabIndex;
 if CurGroup = NIL then Exit;
 with CurGroup^, PTablesTabControl do if TabIndex = -1 then
 begin
  ListBox.Clear;
  ListBoxClick(Sender);
 end else
 begin
  P := grPointerTables^.Root; I := 0;
  while P <> NIL do with P^ do
  begin
   if I = TabIndex then
   begin
    grCurPtrTable := Data;
    RefreshList;
   end;
   Inc(I);
   P := Next;
  end;
 end;
end;

procedure TMainForm.TreeViewDblClick(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 I, J: Integer;
 P: PDynamic;
 PP: PPointer;
 SS: PString;
begin
 with TreeView do
 begin
  Node := TreeView.Selected;
  if SelectedRoot = NIL then Exit;
  if Node = NIL then Exit;
  Case TTreeStateType(Node.StateIndex) of
   tsGroup: with PKruptarProject(SelectedRoot.Data)^, Groups^ do
   begin
    P := Root; I := 0;
    while P <> NIL do with P^ do
    begin
     if Data = Node.Data then
     begin
      GroupsTabControl.TabIndex := I;
      GroupsTabControlChange(Sender);
      Break;
     end;
     Inc(I);
     P := Next;
    end;
   end;
   tsPointer: with PKruptarProject(SelectedRoot.Data)^, Groups^ do
   begin
    P := Root; I := 0;
    while P <> NIL do with P^ do
    begin
     if Data = Node.Parent.Parent.Parent.Data then
     begin
      if GroupsTabControl.TabIndex <> I then
      begin
       GroupsTabControl.TabIndex := I;
       CurGroup := Data;
       if CurGroup <> NIL then with CurGroup^ do
       begin
        grCurPtrTable := Node.Parent.Data;
        PP := grCurPtrTable.ptPointers.Root;
        J := 0;
        while PP <> NIL do with PP^ do
        begin
         if PP = Node.Data then
         begin
          grCurPtrTable.ptItemIndex := J;
          Break;
         end;
         SS := PP.pnStrings.Root;
         while SS <> NIL do
         begin
          Inc(J);
          SS := SS.Next;
         end;
         PP := Next;
        end;
        GroupsTabControlChange(Sender);
       end;
       Break;
      end else
      begin
       if CurGroup <> NIL then with CurGroup^ do
       begin
        if grCurPtrTable <> Node.Parent.Data then
        begin
         grCurPtrTable := Node.Parent.Data;
         PP := grCurPtrTable.ptPointers.Root;
         J := 0;
         while PP <> NIL do with PP^ do
         begin
          if PP = Node.Data then
          begin
           grCurPtrTable.ptItemIndex := J;
           Break;
          end;
          SS := PP.pnStrings.Root;
          while SS <> NIL do
          begin
           Inc(J);
           SS := SS.Next;
          end;
          PP := Next;
         end;
         GroupsTabControlChange(Sender);
        end else
        begin
         PP := grCurPtrTable.ptPointers.Root;
         J := 0;
         while PP <> NIL do with PP^ do
         begin
          if PP = Node.Data then
          begin
           grCurPtrTable.ptItemIndex := J;
           ListBox.ClearSelection;
           ListBox.ItemIndex := J;
           ListBox.Selected[J] := True;
           ListBoxClick(Sender);
           Break;
          end;
          SS := PP.pnStrings.Root;
          while SS <> NIL do
          begin
           Inc(J);
           SS := SS.Next;
          end;
          PP := Next;
         end;
        end;
       end;
       Break;
      end;
     end;
     Inc(I);
     P := Next;
    end;
   end;
   tsPtrTable: with PKruptarProject(SelectedRoot.Data)^, Groups^ do
   begin
    P := Root; I := 0;
    while P <> NIL do with P^ do
    begin
     if Data = Node.Parent.Parent.Data then
     begin
      GroupsTabControl.TabIndex := I;
      P := Root; J := 0;
      while P <> NIL do with P^ do
      begin
       if J = I then
       begin
        CurGroup := Data;
        Break;
       end;
       Inc(J);
       P := Next;
      end;
      if CurGroup <> NIL then with CurGroup^ do
       grCurPtrTable := Node.Data;
      GroupsTabControlChange(Sender);
      Break;
     end;
     Inc(I);
     P := Next;
    end;
   end;
  end;
 end;
end;

constructor TTable.Init;
begin
 tbName := '';
 Root := NIL;
 Cur := NIL;
 EndRoot := NIL;
 EndCur := NIL;
 ElRoot := NIL;
 ElCur := NIL;
 Count := 0;
 EndCount := 0;
 ElCount := 0;
 RefreshMax;
end;

procedure TMainForm.MenuEditLoadAddActionExecute(Sender: TObject);
var
 OpenDlg: TLocOpenDialog;
 Node: TKruptarTreeNodeType;
 CP: LongWord;
 Stream: TWideFileStream;
begin
 OpenDlg := OpenTextDialog;
 if TreeView.Focused then
 begin
  Node := TreeView.Selected;
  if Node = nil then Exit;
  if TTreeStateType(Node.StateIndex) in [tsTables, tsTable, tsEnds,
      tsEls, tsCodes] then
   OpenDlg := OpenTableDialog;
 end;
 if OpenDlg.Execute then
 try
  Stream := TWideFileStream.Create(OpenDlg.FileName,
            fmOpenRead or fmShareDenyWrite);
  try
   PasteStringData(ReadCodePagedString(Stream,
                   PKruptarProject(SelectedRoot.Data).CodePage or
                   CP_WideBOM or CP_MbcsBOM, CP),
                   OpenDlg.FileName, TAction(Sender).Tag = 1);
  finally
   Stream.Free;
  end;
 except
  LocMessageDlg(WideFormat(K7S(SMUUnableToLoad), [OpenDlg.FileName]),
                 mtError, [mbOk], 0);
 end;
end;

procedure TDLLS.GetByMethod(List: TStrings);
var
 M: TMethod;
 D: PDLL;
 Listed: PBytes;
 I: Integer;
begin
 List.Clear;
 if Count <= 0 then Exit;
 GetMem(Listed, Count);
 try
  FillChar(Listed^, Count, 0);
  for M := tmNormal to tmP2P do
  begin
   D := Root;
   I := 0;
   while D <> NIL do with D^ do
   begin
    if GetMethod = M then
    begin
     List.Add(Name);
     Listed[I] := 1;
     Break;
    end;
    Inc(I);
    D := Next;
   end;
  end;
  I := 0;
  D := Root;
  while D <> NIL do with D^ do
  begin
   if Listed[I] = 0 then List.Add(Name);
   Inc(I);
   D := Next;
  end;
 finally
  FreeMem(Listed);
 end;
end;

procedure TMainForm.RefreshProject(Node: TKruptarTreeNodeType; Current: Boolean);
var
 S: WideString;
 TSN, GSN: TKruptarTreeNodeType;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 with TreeView, PKruptarProject(Node.Data)^ do
 begin
  if not fNamed then
   S := ProjectFile else
   S := Dir + ProjectFile;
  Node.Text := S;
  Node.DeleteChildren;
  TSN := Items.AddChild(Node, K7S(STVTables));
  TSN.ImageIndex := Icons[tsDir];
  TSN.SelectedIndex := Icons[tsDir];
  TSN.Data := Tables;
  TSN.StateIndex := Integer(tsTables);
  NotChange := True;
  if SelectedData = TSN.Data then Selected := TSN;
  RefreshTables(TSN);
  GSN := Items.AddChild(Node, K7S(STVGroups));
  GSN.ImageIndex := Icons[tsDir];
  GSN.SelectedIndex := Icons[tsDir];
  GSN.Data := Groups;
  GSN.StateIndex := Integer(tsGroups);
  NotChange := True;
  if SelectedData = GSN.Data then Selected := GSN;
  RefreshGroups(GSN, Current);
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshTables(Node: TKruptarTreeNodeType);
var
 TN: TKruptarTreeNodeType;
 T: PDynamic;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 Node.DeleteChildren;
 with TreeView, PTables(Node.Data)^ do
 begin
  T := Root;
  while T <> NIL do with T^, PTable(Data)^ do
  begin
   TN := Items.AddChild(Node, ' ');
   TN.ImageIndex := Icons[tsTable];
   TN.SelectedIndex := Icons[tsTable];
   TN.Data := Data;
   TN.StateIndex := Integer(tsTable);
   NotChange := True;
   if SelectedData = TN.Data then Selected := TN;
   RefreshTable(TN);
   T := Next;
  end;
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshTable(Node: TKruptarTreeNodeType);
var
 S: WideString;
 CD: Integer;
 TCN, TZN: TKruptarTreeNodeType;
 C: PTableItem;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 Node.DeleteChildren;
 with TreeView, PTable(Node.Data)^ do
 begin
  Node.Text := tbName;
  TZN := Items.AddChild(Node, K7S(STVTerminateCodes));
  TZN.ImageIndex := Icons[tsDir];
  TZN.SelectedIndex := Icons[tsDir];
  TZN.Data := Addr(EndRoot);
  TZN.StateIndex := Integer(tsEnds);
  if SelectedData = TZN.Data then Selected := TZN;
  C := EndRoot;
  while C <> NIL do with C^ do
  begin
   S := '';
   for CD := 1 to Length(tiCodes) do
    S := S + IntToHex(Byte(tiCodes[CD]), 2);
   TCN := Items.AddChild(TZN, S);
   TCN.ImageIndex := Icons[tsTableItem];
   TCN.SelectedIndex := Icons[tsTableItem];
   TCN.Data := C;
   TCN.StateIndex := Integer(tsTableItem);
   if SelectedData = TCN.Data then Selected := TCN;
   C := Next;
  end;
  TZN := Items.AddChild(Node, K7S(STVCRLFCodes));
  TZN.ImageIndex := Icons[tsDir];
  TZN.SelectedIndex := Icons[tsDir];
  TZN.Data := Addr(ElRoot);
  TZN.StateIndex := Integer(tsEls);
  NotChange := True;
  if SelectedData = TZN.Data then Selected := TZN;
  C := ElRoot;
  while C <> NIL do with C^ do
  begin
   S := '';
   for CD := 1 to Length(tiCodes) do
    S := S + IntToHex(Byte(tiCodes[CD]), 2);
   TCN := Items.AddChild(TZN, S);
   TCN.ImageIndex := Icons[tsTableItem];
   TCN.SelectedIndex := Icons[tsTableItem];
   TCN.Data := C;
   TCN.StateIndex := Integer(tsTableItem);
   if SelectedData = TCN.Data then Selected := TCN;
   C := Next;
  end;
  TZN := Items.AddChild(Node, K7S(STVCharacters));
  TZN.ImageIndex := Icons[tsDir];
  TZN.SelectedIndex := Icons[tsDir];
  TZN.Data := Addr(Root);
  TZN.StateIndex := Integer(tsCodes);
  NotChange := True;
  if SelectedData = TZN.Data then Selected := TZN;
  C := Root;
  while C <> NIL do with C^ do
  begin
   S := '';
   for CD := 1 to Length(tiCodes) do
    S := S + IntToHex(Byte(tiCodes[CD]), 2);
   S := S + '=' + EscapeStringEncode(tiString);
   TCN := Items.AddChild(TZN, S);
   TCN.ImageIndex := Icons[tsTableItem];
   TCN.SelectedIndex := Icons[tsTableItem];
   TCN.Data := C;
   TCN.StateIndex := Integer(tsTableItem);
   NotChange := True;
   if SelectedData = TCN.Data then Selected := TCN;
   C := Next;
  end;
 end;
 NotChange := False;
end;

procedure TMainForm.ChangeGroups(Node: TKruptarTreeNodeType);
var
 G: PDynamic;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 with PGroups(Node.Data)^ do
 begin
  G := Root;
  while G <> NIL do with G^, PGroup(Data)^ do
  begin
   with GroupsTabControl do
   begin
    Tabs.Add(grName);
    if CurGroup = Node.Data then TabIndex := Tabs.Count - 1;
   end;
   G := Next;
  end;
 end;
end;

procedure TMainForm.RefreshGroups(Node: TKruptarTreeNodeType; Current: Boolean);
var
 G: PDynamic;
 GN: TKruptarTreeNodeType;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 Node.DeleteChildren;
 with TreeView, PGroups(Node.Data)^ do
 begin
  G := Root;
  while G <> NIL do with G^, PGroup(Data)^ do
  begin
   NotChange := True;
   GN := Items.AddChild(Node, grName);
   GN.ImageIndex := Icons[tsGroup];
   GN.SelectedIndex := Icons[tsGroup];
   GN.Data := Data;
   GN.StateIndex := Integer(tsGroup);
   if SelectedData = GN.Data then
   begin
    NotChange := False;
    Selected := GN;
    TreeViewChange(MainForm, GN);
   end;
   RefreshGroup(GN, Current);
   G := Next;
  end;
 end;
end;

procedure TMainForm.RefreshGroup(Node: TKruptarTreeNodeType; Current: Boolean);
var
 TZN: TKruptarTreeNodeType;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 Node.DeleteChildren;
 with TreeView, PGroup(Node.Data)^ do
 begin
  if Current then with GroupsTabControl do
  begin
   Tabs.Add(grName);
   if CurGroup = Node.Data then TabIndex := Tabs.Count - 1;
  end;
  TZN := Items.AddChild(Node, K7S(STVSpaceForText));
  TZN.ImageIndex := Icons[tsDir];
  TZN.SelectedIndex := Icons[tsDir];
  TZN.Data := grBlocks;
  TZN.StateIndex := Integer(tsBlocks);
  NotChange := True;
  if SelectedData = TZN.Data then Selected := TZN;
  RefreshBlocks(TZN);
  TZN := Items.AddChild(Node, K7S(STVPointers));
  TZN.ImageIndex := Icons[tsDir];
  TZN.SelectedIndex := Icons[tsDir];
  TZN.Data := grPointerTables;
  TZN.StateIndex := Integer(tsPtrTables);
  NotChange := True;
  if SelectedData = TZN.Data then Selected := TZN;
  RefreshPtrTables(TZN);
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshBlocks(Node: TKruptarTreeNodeType);
var
 TCN: TKruptarTreeNodeType;
 B: PBlock;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 Node.DeleteChildren;
 with TreeView, PBlocks(Node.Data)^ do
 begin
  B := Root;
  while B <> NIL do with B^ do
  begin
   TCN := Items.AddChild(Node, GetBlockString(blStart, blCount));
   TCN.ImageIndex := Icons[tsBlock];
   TCN.SelectedIndex := Icons[tsBlock];
   TCN.Data := B;
   TCN.StateIndex := Integer(tsBlock);
   NotChange := True;
   if SelectedData = TCN.Data then Selected := TCN;
   B := Next;
  end;
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshPtrTables(Node: TKruptarTreeNodeType);
var
 PT: PDynamic;
 TCN: TKruptarTreeNodeType;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 Node.DeleteChildren;
 with TreeView, PPointerTables(Node.Data)^ do
 begin
  PT := Root;
  while PT <> NIL do with PT^, PPointerTable(Data)^ do
  begin
   TCN := Items.AddChild(Node, ptName);
   TCN.ImageIndex := Icons[tsPtrTable];
   TCN.SelectedIndex := Icons[tsPtrTable];
   TCN.Data := Data;
   TCN.StateIndex := Integer(tsPtrTable);
   NotChange := True;
   if SelectedData = TCN.Data then Selected := TCN;
   RefreshPtrTable(TCN);
   PT := Next;
  end;
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshPtrTable(Node: TKruptarTreeNodeType);
var
 PP: PPointer;
 Item: TKruptarTreeNodeType;
begin
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 NotChange := True;
 with TreeView, PPointerTable(Node.Data)^ do
 begin
  Node.DeleteChildren;
  PP := ptPointers.Root;
  while PP <> NIL do with PP^ do
  begin
   Item := Items.AddChild(Node, IntToHex(pnPtr, 7));
   Item.ImageIndex := Icons[tsFile];
   Item.SelectedIndex := Icons[tsFile];
   Item.Data := PP;
   Item.StateIndex := Integer(tsPointer);
   NotChange := True;
   if SelectedData = Item.Data then
    Selected := Item;
   PP := Next;
  end;
 end;
 NotChange := False;
end;

procedure TMainForm.RefreshTree(ProjectIndex: Integer);
var
 P: PDynamic;
 PN: TKruptarTreeNodeType;
 I: Integer;
begin
 if SelectedRoot <> NIL then
  SelRoot := SelectedRoot.Data else
  SelRoot := NIL;
 NotChange := True;
 with TreeView do
 begin
  Items.Clear;
  GroupsTabControl.Tabs.Clear;
  P := Projects.Root;
  I := 0;
  if ProjectIndex >= 0 then
  begin
   while P <> NIL do with P^, PKruptarProject(Data)^ do
   begin
    if I = ProjectIndex then
    begin
     PN := Items.AddNode(NIL, NIL, ' ', Data, naAdd);
     PN.ImageIndex := Icons[tsProject];
     PN.SelectedIndex := Icons[tsProject];
     PN.StateIndex := Integer(tsProject);
     PN.Data := Data;
     NotChange := True;
     if SelectedData = PN.Data then Selected := PN;
     RefreshProject(PN, SelRoot = Data);
     Break;
    end;
    Inc(I);
    P := Next;
   end;
  end;
  PN := Selected;
  if PN <> NIL then
  begin
   PN.Expand(False);
   NotChange := False;
   TreeViewChange(MainForm, PN);
  end else NotChange := False;
  with GroupsTabControl do if Tabs.Count <= 0 then
  begin
   PTablesTabControl.Tabs.Clear;
   ListBox.Clear;
   ListBoxClick(MainForm);
  end else GroupsTabControlChange(MainForm);
 end;
end;

procedure TMainForm.MenuProjectPointersTableLinksResetActionExecute(
  Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 Fill: Boolean;
 P: PPointer;
begin
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if TTreeStateType(Node.StateIndex) = tsPointer then
 begin
  Node := Node.Parent;
  Fill := True;
 end else Fill := False;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 if TTreeStateType(Node.StateIndex) <> tsPtrTable then Exit;
 if SelectedRoot = NIL then Exit;
 with PPointerTable(Node.Data)^ do
 begin
  P := ptPointers^.Root;
  while P <> NIL do with P^ do
  begin
   pnTable1 := ptTable1;
   pnTable2 := ptTable2;
   P := Next;
  end;
  if (ptPointers^.Root <> NIL) and Fill then
  begin
   CanCheck := False;
   Values.FillValueListBox(Node);
   ListBoxClick(Sender);
  end else ListBoxClick(Sender);
 end;
end;

procedure TMainForm.TranslateMemoKeyPress(Sender: TObject; var Key: Char);
begin
 if not InsertMode and (Key >= #32) then
 begin
  TranslateMemo.SelLength := 1;
  TranslateMemo.SelText := '';
 end;
end;

procedure TMainForm.TranslateMemoChange(Sender: TObject);
var
 L, I: Integer;
 PT: PPointerTable;
 PD: PDynamic;
 Bef, ValChanged: Boolean;
 Node: TKruptarTreeNodeType;
 PB: PByte;
 KP: PKruptarProject;
 PW: PWord absolute PB;
 PS: PShortInt absolute PB;
 PI: PSmallInt absolute PB;
 PL: PLongInt absolute PB;
begin
 if MemoCanChange and (CurString <> NIL) and (CurPointer <> NIL) then
 with ListBox, CurString^, CurPointer^ do
 begin
  KP := SelectedRoot.Data;
  stNew := TranslateMemo.Text;
  Items.Strings[ItemIndex] := ConvertToListString(ItemIndex + 1, stNew);
  Selected[ItemIndex] := True;
  if Assigned(pnTable2) then
  begin
   PT := CurGroup.grCurPtrTable;
   L := Length(pnTable2.StringToData(stNew, PT^.ptCharSize,
                                     KP.Flags and KP_CRLF_CONVERT <> 0));
   TranslateStatusBar.Panels[3].Text := WideFormat(SMUSizeV, [L]);
   PD := PT.ptParameters.Root;
   I := 1; Bef := False; ValChanged := False;
   while PD <> NIL do with PD^, PParameter(Data)^ do
   begin
    if not Bef and paBefore then with PT^, ptParameters^ do
    begin
     I := 1 + (ptInterval - Before);
     Bef := True;
    end;
    if paFlags and PAR_AUTOSIZE = PAR_AUTOSIZE then
    begin
     PB := Addr(pnParamData[I]);
     Case paParamType of
      ptByte:
      begin
       ValChanged := PB^ <> L;
       PB^ := L;
      end;
      ptWord:
      begin
       ValChanged := PW^ <> L;
       PW^ := L;
      end;
      ptShortInt:
      begin
       ValChanged := PS^ <> L;
       PS^ := L;
      end;
      ptSmallInt:
      begin
       ValChanged := PI^ <> L;
       PI^ := L;
      end;
      ptLongInt:
      begin
       ValChanged := PL^ <> L;
       PL^ := L;
      end;
     end;
    end;
    Inc(I, CParamSize[paParamType]);
    PD := Next;
   end;
   Node := TreeView.Selected;
   if ValChanged and Assigned(Node) and (CurPointer = Node.Data) then
    Values.FillValueListBox(Node);
  end else TranslateStatusBar.Panels[3].Text := '';
  SetSaved(SelectedRoot.Data, False);
  MemoCanChange := True;
 end;
end;

function TParameters.Find(const Name: String): PParameter;
var
 P: PDynamic;
begin
 Result := NIL;
 P := Root;
 while P <> NIL do with P^, PParameter(Data)^ do
 begin
  if Name = paName then
  begin
   Result := Data;
   Exit;
  end;
  P := P^.Next;
 end;
end;

procedure TMainForm.MenuProjectPointersVarOptionsActionExecute(
  Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 P: PPointer;
 S: RawByteString;
begin
 if SelectedRoot = NIL then Exit;
 Node := TreeView.Selected;
 if Node = NIL then Exit;
 if TTreeStateType(Node.StateIndex)= tsPointer then Node := Node.Parent;
 if Node = NIL then Exit;
 if Node.Data = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with PKruptarProject(SelectedRoot.Data)^ do
 begin
  with PPointerTable(Node.Data)^ do
  begin
   if ParamDialog.Execute(ptParameters, ptInterval) and
      (ptPointers^.Count > 0) then
   begin
    if LoadROM then
    try
     P := ptPointers^.Root;
     while P <> NIL do with P^ do
     begin
      S := FillParamData(ROM, pnPos);
      if Length(S) > Length(pnParamData) then
       Move(Pointer(pnParamData)^, Pointer(S)^, Length(pnParamData)) else
       Move(Pointer(pnParamData)^, Pointer(S)^, Length(S));
      P := Next;
     end;
     SetSaved(SelectedRoot.Data, False);
    finally
     FreeROM;
     CanCheck := False;
     Values.FillValueListBox(TreeView.Selected);
    end;
   end;
  end;
 end;
end;

procedure TMainForm.SetSaved(PRJ: Pointer; S: Boolean);
var
 I: Integer;
 P: PDynamic;
 KP: PKruptarProject absolute PRJ;
begin
 if PRJ = NIL then Exit;
 with KP^ do
 begin
  if not S then AllSaved := False;
  fSaved := S;
  I := 0;
  P := Projects.Root;
  while P <> nil do
  begin
   if P.Data = KP then
   begin
    with ProjectTabs do
    if not S then
     Tabs.Strings[I] := '[*]' + ProjectFile else
     Tabs.Strings[I] := ProjectFile;
    Break;
   end;
   P := P.Next;
   Inc(I);
  end;
  if (SelectedRoot <> nil) and (KP = SelectedRoot.Data) then
  begin
   if fNamed then
    SelectedRoot.Text := Dir + ProjectFile else
    SelectedRoot.Text := ProjectFile;
  end;
 end;
end;

const
 S_CRLF: array[0..1] of WideChar = (#13, #10);

function TTable.StringToData(S: WideString; Align: LongInt;
  CRLF: Boolean): RawByteString;
var
 B: PWord;
 T: PTableItem;
 J, X, Len: LongInt;
 SlashMode: Boolean;
 Chain: RawByteString;
 Item: PTableItem;
label CharEncode;
begin
 Result := '';
 B := Pointer(S);
 Len := Length(S) shl 1;
 Chain := '';
 while Len > 0 do
 begin
  if (B^ = 13) or (B^ = 10) then
  begin
   if CRLF then if ElRoot <> NIL then
   begin
    SlashMode := False;
    if Chain <> '' then
    begin
     Item := ElRoot;
     while Item <> NIL do with Item^ do
     begin
      J := Length(Chain) - Length(tiCodes);
      if (J >= 0) and
         CompareMem(Pointer(tiCodes), Addr(Chain[J + 1]), Length(tiCodes)) then
      begin
       SlashMode := True;
       Break;
      end;
      Item := Next;
     end;
    end;
    if not SlashMode then
     Result := Result + ElRoot.tiCodes;
    if B^ = 13 then
    begin
     Inc(B);
     Dec(Len, 2);
    end;
    if B^ = 10 then
    begin
     Inc(B);
     Dec(Len, 2);
    end;
    X := Length(Chain);
    for J := 1 to Aligned(X, Align) - X do
     Result := Result + #0;
    Chain := '';
    Continue;
   end;
   goto CharEncode;
  end else
  begin
   if (WideChar(B^) = '/') and (Len > 2) then
   begin
    J := HexVal(Char(PWords(B)[1]) + Char(PWords(B)[2]));
    if not HexError then
    begin
     Chain := Chain + AnsiChar(J);
     Result := Result + AnsiChar(J);
     Inc(B, 3);
     Dec(Len, 6);
    end else goto CharEncode;
   end else
   begin
    CharEncode:
    X := Length(Chain);
    for J := 1 to Aligned(X, Align) - X do
     Result := Result + #0;
    Chain := '';
    T := GetCodes(B^, Len shr 1);
    if T <> NIL then with T^ do
    begin
     if tiState = isCode then
     begin
      Result := Result + tiCodes;
      X := Length(tiCodes);
      for J := 1 to Aligned(X, Align) - X do
       Result := Result + #0;
     end;
     J := Length(tiString) shl 1;
     Inc(Integer(B), J);
     Dec(Len, J);
    end else
    begin
     Inc(B);
     Dec(Len, 2);
    end;
   end;
  end;
 end;
 X := Length(Result);
 for J := 1 to Aligned(X, Align) - X do
  Result := Result + #0;
end;

procedure TMainForm.MenuProjectGroupTextResetActionExecute(Sender: TObject);

 var
  KP: PKruptarProject;

 procedure PListReset(List: PPointerTable);
 var
  ST: PString;
  P: PPointer;
 begin
  with List^ do
  begin
   P := ptPointers.Root;
   while P <> NIL do with P^ do
   begin
    ST := pnStrings.Root;
    while ST <> NIL do with ST^ do
    begin
     stNew := ConvertString(pnTable1, stOriginal,
              KP.Flags and KP_CRLF_CONVERT <> 0);
     ST := Next;
    end;
    P := Next;
   end;
  end;
 end;

 procedure GroupReset(GRP: PGroup);
 var
  PT: PDynamic;
 begin
  StatesMemo.Clear;
  StatesMemo.Visible := False;
  PT := GRP.grPointerTables.Root;
  while PT <> NIL do with PT^ do
  begin
   PListReset(Data);
   PT := Next;
  end;
  if CurGroup = GRP then RefreshList;
  SetSaved(SelectedRoot.Data, False);
 end;

 procedure JustListReset(List: PPointerTable);
 begin
  StatesMemo.Clear;
  StatesMemo.Visible := False;
  PListReset(List);
  if CurGroup.grCurPtrTable = List then
   RefreshList;
  SetSaved(SelectedRoot.Data, False);
 end;

 var
  Node: TKruptarTreeNodeType;
  ST: PString;
  I: Integer;
(* TMainForm.MenuProjectGroupTextResetActionExecute *)
begin
 if SelectedRoot = nil then Exit;
 KP := SelectedRoot.Data;
 if KP = nil then Exit;
 if TreeView.Focused then
 begin
  Node := TreeView.Selected;
  if Node = nil then Exit;
  if Node.Data = nil then Exit;
  Case TTreeStateType(Node.StateIndex) of
   tsGroup: GroupReset(Node.Data);
   tsPtrTable: JustListReset(Node.Data);
  end;
 end else if ListBox.Focused then
 begin
  StatesMemo.Clear;
  StatesMemo.Visible := False;
  with CurGroup^, ListBox do
  begin
   for I := 0 to Count - 1 do
   begin
    if Selected[I] then with grCurPtrTable^ do
    begin
     ST := GetStringByIndex(I);
     if ST <> nil then with ST^ do
     begin
      stNew := ConvertString(FLastPointer.pnTable1, stOriginal,
               KP.Flags and KP_CRLF_CONVERT <> 0);
      Items[I] := ConvertToListString(I + 1, stNew);
      Selected[I] := True;
     end;
    end;
   end;
  end;
  TranslateMemo.Text := OriginalMemo.Text;
 end else
 if GroupsTabControl.Focused then
  GroupReset(CurGroup) else
 if PTablesTabControl.Focused then
  JustListReset(CurGroup.grCurPtrTable) else
 if TranslateMemo.Focused then
  TranslateMemo.Text := OriginalMemo.Text;;
end;

procedure TMainForm.MenuProjectGroupTextReloadActionExecute(Sender: TObject);

 var
  KP: PKruptarProject;

 procedure ListReload(List: PPointerTable);
 var
  DLL: PDLL;
 begin
  with KP^, List.OwnedBy^ do
  begin
   DLL := DLLs.Get(grDLLName);
   if DLL = NIL then
   begin
    LocMessageDlg(WideFormat(K7S(SMULibraryNotFound), [grDLLName]),
     mtError, [mbOk], 0);
    Exit;
   end;
   if LoadROM then
   try
    ReloadStrs(List, DLL);
   finally
    FreeROM;
    if CurGroup.grCurPtrTable = List then
    begin
     RefreshList;
     ListBoxClick(Sender);
    end;
   end;
  end;
 end;

 procedure GroupReload(GRP: PGroup);
 var
  PT: PDynamic;
  DLL: PDLL;
 begin
  with KP^, GRP^ do
  begin
   DLL := DLLs.Get(grDLLName);
   if DLL = NIL then
   begin
    LocMessageDlg(WideFormat(K7S(SMULibraryNotFound), [grDLLName]),
     mtError, [mbOk], 0);
    Exit;
   end;
   if LoadROM then
   try
    PT := grPointerTables^.Root;
    while PT <> NIL do with PT^ do
    begin
     ReloadStrs(Data, DLL);
     PT := Next;
    end;
   finally
    FreeROM;
    if CurGroup = GRP then
    begin
     RefreshList;
     ListBoxClick(Sender);
    end;
   end;
  end;
 end;

var
 Node: TKruptarTreeNodeType;
begin
 if SelectedRoot = NIL then Exit;
 KP := SelectedRoot.Data;
 if KP = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if FieldFileExists(KP_SRC_ROM, KP.InputROM, False) then
 begin
  if GroupsTabControl.Focused then
  begin
   if CurGroup <> nil then
    GroupReload(CurGroup);
  end else
  if ListBox.Focused or PTablesTabControl.Focused then
  begin
   if CurGroup <> nil then
    ListReload(CurGroup.grCurPtrTable);
  end else
  begin
   Node := TreeView.Selected;
   if Node = NIL then Exit;
   if Node.Data = NIL then Exit;
   Case TTreeStateType(Node.StateIndex) of
    tsGroup: GroupReload(Node.Data);
    tsPtrTable: ListReload(Node.Data);
   end;
  end;
 end;
end;

procedure TMainForm.MenuEditClearActionExecute(
  Sender: TObject);
//var
// Node: TKruptarTreeNodeType;
//begin
var
 H: HWND;
begin
 H := GetEditHandle;
 (* Copy from active TCustomEdit *)
 if H <> 0 then
  SendMessage(H, WM_CLEAR, 0, 0) else
 (* Clears selected data *)
  ClearData;
end;

procedure CopyToClipboard(const S: WideString);
begin
{$IFNDEF FORCED_UNICODE}
  TntClipboard.AsWideText := S;
{$ELSE}
  Clipboard.AsText := S;
{$ENDIF}
end;

function ClipboardHasFormat(Format: Word): Boolean;
begin
{$IFNDEF FORCED_UNICODE}
  Result := TntClipboard.HasFormat(Format);
{$ELSE}
  Result := Clipboard.HasFormat(Format);
{$ENDIF}
end;

function GetTextFromClipboard: WideString;
begin
{$IFNDEF FORCED_UNICODE}
  Result := TntClipboard.AsWideText;
{$ELSE}
  Result := Clipboard.AsText;
{$ENDIF}
end;

procedure TMainForm.MenuEditCutActionExecute(Sender: TObject);
var
 H: HWND;
begin
 H := GetEditHandle;
 (* Copy from active TCustomEdit *)
 if H <> 0 then
  SendMessage(H, WM_CUT, 0, 0) else
 (* Copy string data from active program element *)

 CopyToClipboard(CopyStringData(True));
end;

procedure TMainForm.MenuEditCopyActionExecute(Sender: TObject);
var
 H: HWND;
begin
 H := GetEditHandle;
 (* Copy from active TCustomEdit *)
 if H <> 0 then
  SendMessage(H, WM_COPY, 0, 0) else
 (* Copy string data from active program element *)
  CopyToClipboard(CopyStringData(False));
end;

procedure TMainForm.MenuEditPasteActionExecute(Sender: TObject);
var
 H: HWND;
begin
 if ClipboardHasFormat(CF_TEXT) then
 begin
  H := GetEditHandle;
  (* Copy from active TCustomEdit *)
  if H <> 0 then SendMessage(H, WM_PASTE, 0, 0) else
   PasteStringData(GetTextFromClipboard, '', False);
 end;
end;

procedure TMainForm.MenuEditSelectAllActionExecute(Sender: TObject);
var
 H: HWND;
begin
 if ListBox.Focused then ListBox.SelectAll else
 begin
  H := GetEditHandle;
  if H <> 0 then SendMessage(H, EM_SETSEL, 0, -1);
 end;
end;

procedure TMainForm.TranslateMemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

 procedure AddCode(Code: Byte);
 begin
  with TranslateMemo do SelText := '/' + IntToHex(Code, 2);
 end;

var
 I: Integer;
 TI: PTableItem;
 KP: PKruptarProject;
begin
 if ssCtrl in Shift then
 begin
  Case Key of
   VK_Home: if ssAlt in Shift then with ListBox do if Count > 1 then
   begin
    ClearSelection;
    ItemIndex := 0;
    Selected[0] := True;
    ListBoxClick(Sender);
   end;
   VK_End: if ssAlt in Shift then with ListBox do if Count > 1 then
   begin
    ClearSelection;
    I := Count - 1;
    ItemIndex := I;
    Selected[I] := True;
    ListBoxClick(Sender);
   end;
   VK_Return: Key := 0;
   VK_UP: with ListBox do if Count > 1 then
   begin
    Key := 0;
    I := ItemIndex;
    if I > 0 then
    begin
     ClearSelection;
     ItemIndex := I - 1;
     Selected[I - 1] := True;
     ListBoxClick(Sender);
    end;
   end;
   VK_DOWN: with ListBox do if Count > 1 then
   begin
    I := ItemIndex;
    Key := 0;
    if I < Count - 1 then
    begin
     ClearSelection;
     ItemIndex := I + 1;
     Selected[I + 1] := True;
     ListBoxClick(Sender);
    end;
   end;
  end;
 end else if Key = VK_Return then
 begin
  Key := 0;
  if SelectedRoot <> NIL then
  begin
   KP := SelectedRoot.Data;
   if KP.Flags and KP_CRLF_CONVERT = 0 then
   begin
    if CurPointer <> NIL then with CurPointer^ do if pnTable2 <> NIL then
    with pnTable2^ do if ElRoot <> NIL then
    begin
     if (ssShift in Shift) and (Count > 1) then
      TI := ElRoot^.Next else
      TI := ElRoot;
     if Assigned(TI) then with TI^ do for I := 1 to Length(tiCodes) do
      AddCode(Byte(tiCodes[I]));
    end;
   end;
  end;
 end else if Key = VK_INSERT then
 begin
  Key := 0;
  InsertMode := not InsertMode;
 end;
end;

procedure TMainForm.ValueListBoxExit(Sender: TObject);
begin
 if CanCheck then CheckValues;
end;

const
 prKruptarProject  = 0;
 prInputROM        = 1;
 prOutputROM       = 2;
 prEmulator        = 3;
 prDefineGroup     = 4;
 prPointerUse      = 5;
 prPointerSize     = 6;
 prInterval        = 7;
 prDivision        = 8;
 prType            = 9;
 prMotorola        = 10;
 prSigned          = 11;
 prShl1            = 12;
 prMethod          = 13;
 prDifference      = 14;
 prCharSet         = 15;
 prStringLength    = 16;
 prTablesStart     = 17;
 prTablesEnd       = 18;
 prBlocksStart     = 19;
 prBlocksEnd       = 20;
 prDefineTextBlock = 21;
 prUsedTableSet    = 22;
 prPtrTableStart   = 23;
 prBlockStart      = 24;
 prBlockLength     = 25;
 prStringPointer   = 26;
 prStringsCount    = 27;
 prStringStart     = 28;
 prStringEnd       = 29;
 prTextBlockEnd    = 30;
 prGroupEnd        = 31;
 prProjectEnd      = 32;
 K6_ID_CNT         = 33;
 K6_ID_List: array[0..K6_ID_CNT - 1] of AnsiString =
 (
  '[KRUPTARPROJECT]',
  '[INPUTROM]',
  '[OUTPUTROM]',
  '[EMULATOR]',
  '[DEFINEGROUP]',
  '[POINTERUSE]',
  '[POINTERSIZE]',
  '[INTERVAL]',
  '[DIVISION]',
  '[TYPE]',
  '[MOTOROLA]',
  '[SIGNED]',
  '[SHL1]',
  '[METHOD]',
  '[DIFFERENCE]',
  '[CHARSET]',
  '[STRINGLENGTH]',
  '[TABLESSTART]',
  '[TABLESEND]',
  '[BLOCKSSTART]',
  '[BLOCKSEND]',
  '[DEFINETEXTBLOCK]',
  '[USEDTABLESET]',
  '[PTRTABLESTART]',
  '[BLOCKSTART]',
  '[BLOCKLENGTH]',
  '[STRINGPOINTER]',
  '[STRINGSCOUNT]',
  '[STRINGSTART]',
  '[STRINGEND]',
  '[TEXTBLOCKEND]',
  '[GROUPEND]',
  '[PROJECTEND]'
 );
 prTrue            = 'TRUE';
 prFalse           = 'FALSE';
 prANSI            = 'ANSI';
 prSJIS            = 'SHIFT-JIS';
 prUNIC            = 'UNICODE';

function BoolStr(B: Boolean): String;
begin
 if B then Result := prTrue else Result := prFalse;
end;

function TrueStr(const S: String): Boolean;
begin
 Result := (S = '1') or (UpperCase(S) = BoolStr(True));
end;

type
 PTableSetItem = ^TTableSetItem;
 TTableSetItem = record
  T1: PTable;
  T2: PTable;
  Next: PTableSetItem;
 end;
 PTableSet = ^TTableSet;
 TTableSet = object
  Root, Cur: PTableSetItem;
  Count: Integer;
  constructor Init;
  destructor Done;
  function Add: PTableSetItem;
  function Get(Index: Integer): PTableSetItem;
 end;
 TKruptar6_StringList = class(TStringList)
 private
  FCodePage: LongWord;
  function GetString(Index: Integer): RawByteString;
 public
  property CodePage: LongWord read FCodePage write FCodePage;

  procedure LoadFromStream(Stream: TStream); override;

  property ByteStrings[Index: Integer]: RawByteString read GetString;
 end;

function TKruptarProject.Open6(const FileName: WideString;
  Console: Boolean = False): Boolean;
(* Local variables *)
var
 MainList: TKruptar6_StringList;
 MLIndex, LastID: Integer;

(* Local functions *)
 function ReadIdent(var Value: RawByteString): Integer;
 var
  P, L, I, J: Integer;
  Ident: AnsiString;
 begin
  if LastID >= 0 then
  begin
   Result := LastID;
   LastID := -1;
   Exit;
  end;
  Ident := '';
  while MLIndex < MainList.Count do
  begin
   Value := MainList.ByteStrings[MLIndex];
   Inc(MLIndex);
   P := Pos(RawByteString('['), Value);
   if P > 0 then
   begin
    L := Length(Value);
    for I := P + 1 to L do if Value[I] = ']' then
    begin
     Ident := AnsiString(UpperCase(String(AnsiString(Copy(Value, P, I - P + 1)))));
     Delete(Value, 1, I);
     Break;
    end;
   end;
   if Ident <> '' then
   begin
    L := Length(Value);
    J := 1;
    while (J <= L) and (Value[J] = ' ') do Inc(J);
    Delete(Value, 1, J);
    Dec(L, J);
    J := 0;
    while (J < L) and (PByteArray(Value)[J] = $20) do Inc(J);
    Delete(Value, 1, J);
    for I := 0 to K6_ID_CNT - 1 do
      if Ident = K6_ID_List[I] then
      begin
       Result := I;
       Exit;
      end;

    Break;
   end;
  end;
  Result := -1;
 end; (* function ReadIdent *)

 procedure InvalidFileFormat;
 begin
  if Console then ConsoleWriteln(SInvalidFileFormat);
  raise Exception.Create(SInvalidFileFormat);
 end;
(* End of local functions *)

(* Local variables 2 *)
var
 Value, Value2: RawByteString;
 SW, SW2: WideString;
 PW: PWideChar;
 ID, SCnt, I, J, PtrPos, PtrBlockStart: LongInt;
 grPointerSize: LongInt;
 grInterval: LongInt;
 grMultiple: LongInt;
 grShiftLeft: LongInt;
 grReference: LongInt;
 grStringLength: LongInt;
 grPunType: Boolean;
 grAutoStart: Boolean;
 grPtrToPtr: Boolean;
 grMotorola: Boolean;
 grSigned: Boolean;
 grCodePage: LongWord;
 TableSet: PTableSet;
 TableItem: PTableItem;
 SetItem: PTableSetItem;
 DLL: PDLL;
 ExtractMethod: TMethod;
 GPtrTbl, PtrTbl: PPointerTable;
 PStr: PString;
 PT: PDynamic;
 _InputROM, _OutputROM: RawByteString;
 Table1, Table2: WideString;

(* TKruptarProject.Open6 *)
begin
 Result := False;
 try
  MainList := TKruptar6_StringList.Create;
  try
   MainList.LoadFromFile(FileName);
   Dir := WideExtractFilePath(FileName);
   SetCurrentDirectoryW(Pointer(Dir));
   MLIndex := 0; // Index of current string in main list
   (* Start loading project *)
   LastID := -1;
   if ReadIdent(Value) = prKruptarProject then
   begin
    (* Initialize project *)
    Flags := KP_CRLF_CONVERT;
    CodePage := CP_UTF8;
    ProjectFile := WideExtractFileName(FileName);

    (* Read initial values *)
    repeat
     ID := ReadIdent(Value);
     case ID of
      prInputROM: _InputROM := Value;
      prOutputROM: _OutputROM := Value;
      prEmulator: EmulSelected := CodePageStringDecode(MainList.CodePage, Value);
      else
      begin
       (* Save last ID for ReadIdent  *)
       LastID := ID;
       (* Finish read initial values *)
       Break;
      end;
     end;
    until False;
    (* Check initial values *)
    MainForm.OpenDialog.Filter := K7S(SMURomsFilter);
    InputROM := CheckFile(Dir, _InputROM, Console, True, MainList.CodePage);
    OutputROM := CheckFile(Dir, _OutputROM, Console, False, MainList.CodePage);

    MainForm.OpenDialog.Filter := MainForm.OpenTableDialog.Filter;
    (* Load project *)
    LoadROM(True);
    try
     (* Read groups loop *)
     repeat
      case ReadIdent(Value) of
       prDefineGroup: with Groups.Add^ do
       begin
        grPointerSize := 0;
        grInterval := 0;
        grMultiple := 1;
        grShiftLeft := 0;
        grReference := 0;
        grStringLength := 0;
        grPunType := False;
        grAutoStart := False;
        grMotorola := False;
        grSigned := False;
        grPtrToPtr := False;
        ExtractMethod := tmNormal;
        grCodePage := MainList.CodePage;
        grName := CodePageStringDecode(grCodePage, Value);
        FixFileName(grName);
        GPtrTbl := nil;
        PtrTbl := nil;
        New(TableSet, Init);
        try
         (* Read group loop *)
         repeat
          case ReadIdent(Value) of
           prPointerUse:
           begin
            if (PtrTbl = nil) and (grPointerSize = 0) then
            begin
             if not TrueStr(String(AnsiString(Value))) then
             begin
              DLL := DLLs.MethodGet(tmNormal);
              if DLL <> NIL then
              begin
               grDLLName := DLL.Name;
               Continue;
              end;
             end else Continue;
            end;
            InvalidFileFormat;
           end;
           prPointerSize:
           begin
            if PtrTbl = nil then
            begin
             grPointerSize := GetLDW(string(AnsiString(Value)));
             if not IntError and not HexError and
               (grPointerSize in [2, 3, 4]) then Continue;
            end;
            InvalidFileFormat;
           end;
           prInterval:
           begin
            if PtrTbl = nil then
            begin
             grInterval := GetLDW(string(AnsiString(Value)));
             if not IntError and not HexError then Continue;
            end;
            InvalidFileFormat;
           end;
           prDivision:
           begin
            if PtrTbl = nil then
            begin
             grMultiple := GetLDW(AnsiString(Value));
             if not IntError and not HexError then Continue;
            end;
            InvalidFileFormat;
           end;
           prType:
           begin
            if (PtrTbl = nil) and (Length(Value) = 1) then
            case Value[1] of
             '0':
             begin
              grPunType := False;
              grAutoStart := False;
              Continue;
             end;
             '1':
             begin
              grPunType := True;
              grAutoStart := False;
              Continue;
             end;
             '2':
             begin
              grPunType := False;
              grAutoStart := True;
              Continue;
             end;
            end;
            InvalidFileFormat;
           end;
           prMotorola:
           begin
            if PtrTbl <> NIL then
             InvalidFileFormat;
            grMotorola := TrueStr(AnsiString(Value));
           end;
           prSigned:
           begin
            if PtrTbl <> NIL then
             InvalidFileFormat;
            grSigned := TrueStr(AnsiString(Value));
           end;
           prShl1:
           begin
            if PtrTbl <> NIL then
             InvalidFileFormat;
            grShiftLeft := LongInt(TrueStr(AnsiString(Value))) and 1;
           end;
           prMethod:
           begin
            if PtrTbl = nil then
            begin
             ExtractMethod := TMethod(GetLDW(AnsiString(Value)));
             if not IntError and not HexError and
               (Byte(ExtractMethod) < Byte(tmNone)) then
             begin
              if ExtractMethod = tmP2P then
              begin
               DLL := DLLs.MethodGet(tmNormal);
               grPtrToPtr := True;
              end else DLL := DLLs.MethodGet(ExtractMethod);
              if DLL <> NIL then
              begin
               grDLLName := DLL.Name;
               Continue;
              end;
             end;
            end;
            InvalidFileFormat;
           end;
           prDifference:
           begin
            if PtrTbl = nil then
            begin
             grReference := GetLDW(AnsiString(Value));
             if not IntError and not HexError then Continue;
            end;
            InvalidFileFormat;
           end;
           prCharSet:
           begin
            if PtrTbl = nil then
            begin
             Value := AnsiString(UpperCase(AnsiString(Value)));
             if AnsiString(Value) = prUNIC then
             begin
              grCodePage := CP_UTF8;
              Continue;
             end else
             if AnsiString(Value) = prSJIS then
             begin
              grCodePage := CP_SHIFT_JIS;
              Continue;
             end else
             if AnsiString(Value) = prANSI then
             begin
              grCodePage := MainList.CodePage;
              Continue;
             end;
            end;
            InvalidFileFormat;
           end;
           prStringLength:
           begin
            if PtrTbl = nil then
            begin
             grStringLength := GetLDW(AnsiString(Value));
             if not IntError and not HexError and (grStringLength >= 0) then
              Continue;
            end;
            InvalidFileFormat;
           end;
           prTablesStart:
           begin
            repeat
             Value := MainList.ByteStrings[MLIndex];
             Inc(MLIndex);
             if UpperCase(Trim(AnsiString(Value))) = K6_ID_LIST[prTablesEnd] then Break;
             Table1 := CheckFile(Dir, Value, Console, True, MainList.CodePage);
             Table2 := CheckFile(Dir, MainList.ByteStrings[MLIndex], Console, True, MainList.CodePage);
             Inc(MLIndex);

             with TableSet.Add^ do
             begin
              T1 := Tables.Find(Table1);
              if T1 = NIL then
              begin
               T1 := Tables.Add;
               with T1^ do
               begin
                tbName := Table1;
                LoadFromFile(tbName, grCodePage);
               end;
              end;
              T2 := Tables.Find(Table2);
              if T2 = NIL then
              begin
               T2 := Tables.Add;
               with T2^ do
               begin
                tbName := Table2;
                LoadFromFile(tbName, grCodePage);
               end;
              end;
             end;
            until false;
           end;
           prBlocksStart:
           begin
            repeat
             Value := MainList.ByteStrings[MLIndex];
             Inc(MLIndex);
             if UpperCase(Trim(AnsiString(Value))) = K6_ID_LIST[prBlocksEnd] then Break;
             Value2 := MainList.ByteStrings[MLIndex];
             Inc(MLIndex);
             with grBlocks.Add^ do
             begin
              blStart := GetLDW(AnsiString(Value));
              if IntError or HexError then
               InvalidFileFormat;
              blCount := GetLDW(AnsiString(Value2));
              if IntError or HexError then
               InvalidFileFormat;
              Dec(blCount, blStart - 1);
             end;
            until false;
           end;
           prDefineTextBlock:
           begin
            if GPtrTbl = nil then
            begin
             PtrTbl := grPointerTables.Add(Owner.Data);
             if (grStringLength > 0) or (grPointerSize > 0) or grPunType then
              GPtrTbl := PtrTbl;
            end else PtrTbl := GPtrTbl;
            (* Initialize pointer table *)
            with PtrTbl^ do
            begin
             if ptName = '' then
              PtrTbl.ptName := CodePageStringDecode(MainList.CodePage, Value);
             ptItemIndex := 0;
             ptStringLength := grStringLength;
             ptReference := grReference;
             ptPointerSize := grPointerSize;
             ptAlignment := grMultiple;
             ptInterval := grInterval;
             ptShiftLeft := grShiftLeft;
             ptMotorola := grMotorola;
             ptSigned := grSigned;
             ptPunType := grPunType;
             ptAutoStart := grAutoStart;
             ptPtrToPtr := grPtrToPtr;
             ptSnesLoRom := false;
             if ptSigned and (ptPointerSize = 2) then
              ptPointerSize := 6;
             SetItem := NIL;
             (* Read text block *)
             repeat
              case ReadIdent(Value) of
               prUsedTableSet:
               begin
                J := GetLDW(AnsiString(Value));
                if IntError or HexError then
                 InvalidFileFormat;
                SetItem := TableSet.Get(J);
               end;
               prPtrTableStart, prBlockStart:
               begin
                PtrBlockStart := GetLDW(AnsiString(Value));
                if (SetItem = NIL) or IntError or HexError then
                 InvalidFileFormat;
                PtrPos := PtrBlockStart;
                ptTable1 := SetItem.T1;
                ptTable2 := SetItem.T2;
                LastID := ReadIdent(Value);
                if LastID = prBlockLength then
                 LastID := -1;
                (* Read strings list *)
                repeat
                 ID := ReadIdent(Value);
                 case ID of
                  prStringPointer: with ptPointers.Add^ do
                  begin
                   pnPos := PtrPos;
                   pnInsertPos := PtrPos;
                   if ptPointerSize > 0 then
                   begin
                    if ptPunType then Inc(PtrPos) else
                    begin
                     Inc(PtrPos, ptPointerSize + ptInterval);
                     if ptPointerSize > 4 then Dec(PtrPos, 4);
                    end;
                   end else if ptStringLength > 0 then
                    Inc(PtrPos, ptStringLength + ptInterval);
                   pnPtr := GetLDW(AnsiString(Value));
                   if IntError or HexError then
                    InvalidFileFormat;
                   pnTable1 := ptTable1;
                   pnTable2 := ptTable2;
                   if ptPointerSize > 0 then
                   begin
                    if ptAutoStart then
                     pnReference := PtrBlockStart else
                     pnReference := ptReference;
                   end else pnReference := 0;
                   pnInsertRef := pnReference;
                   pnFixed := (ptPointerSize = 0) and (ptStringLength > 0);
                   New(pnStrings, Init);
                   SCnt := 1 + LongInt(ExtractMethod = tmL1TL1T);
                   PStr := nil;
                   (* Read string *)
                   repeat
                    ID := ReadIdent(Value);
                    case ID of
                     prStringsCount:
                     begin
                      SCnt := GetLDW(AnsiString(Value));
                      if IntError or HexError then
                       InvalidFileFormat;
                     end;
                     prStringStart:
                     begin
                      (* Initialize new strings *)
                      for I := 0 to SCnt - 1 do pnStrings.Add;
                      if PStr = nil then PStr := pnStrings.Root;
                      (* Read string from file *)
                      Value := '';
                      repeat
                       Value2 := MainList.ByteStrings[MLIndex];
                       Inc(MLIndex);
                       if UpperCase(Trim(AnsiString(Value2))) = K6_ID_LIST[prStringEnd] then
                        Break;
                       Value := Value + Value2;
                      until False;
                      (* Convert string *)
                      if grCodePage <> MainList.CodePage then
                      begin
                        SW := CodePageStringDecode(MainList.CodePage, Value);
                        Value := CodePageStringEncode(GetACP, SW);
                      end;
                      SW := CodePageStringDecode(grCodePage, Value);
                      PW := Pointer(SW);
                      (* Convert string loop *)
                      while (PW^ <> #0) and (PStr <> nil) do
                      begin
                       with PStr^ do
                       begin
                        if (PW^ = '[') and (PW[1] <> #0) and (PW[1] = '#') and
                           (PW[4] = ']') and (Word(PW[2]) < 128) and
                           (Word(PW[3]) < 128) and (AnsiChar(PW[2]) in HexSet) and
                           (AnsiChar(PW[3]) in HexSet) then
                        begin
                         stNew := stNew + sHexStart + PW[2] + PW[3];
                         Inc(PW, 5);
                        end else
                        begin
                         if ExtractMethod = tmL1TL1T then
                         begin
                          if PW^ = '^' then
                           PStr := PStr.Next else
                           stNew := stNew + PW^;
                         end else
                         begin
                          TableItem := pnTable2.FindEnd(PW^);
                          if TableItem = nil then
                           TableItem := pnTable2.FindEl(PW^);
                          if TableItem <> nil then
                          begin
                           if TableItem <> pnTable2.ElRoot then
                           with TableItem^ do
                           begin
                            for I := 1 to Length(tiCodes) do
                             stNew := stNew + sHexStart +
                              IntToHex(Byte(tiCodes[I]), 2);
                            if tiState = isTerminate then
                             PStr := PStr.Next else
                             stNew := stNew + sLineBreak;
                           end else stNew := stNew + sLineBreak;
                          end else stNew := stNew + PW^;
                         end;
                         Inc(PW);
                        end;
                       end;
                      end; (* Convert string loop end *)
                     end; (* prStringStart block end *)
                     else
                     begin
                      (* Save last ID for ReadIdent *)
                      LastID := ID;
                      (* Finish read string *)
                      Break;
                     end;
                    end;
                   until False; (* Read string end *)
                  end;
                  else
                  begin
                   (* Save last ID for ReadIdent  *)
                   LastID := ID;
                   (* Finish read strings list *)
                   Break;
                  end;
                 end;
                until False; (* Read strings list end *)
               end; (* "prPtrTableStart" and "prBlockStart" blocks end *)
               prTextBlockEnd:
               begin
                if ptPunType then
                 ptInterval := ptPointers.Count;
                Break;
               end;
               else InvalidFileFormat;
              end;
             until False; (* Read text block end *)
            end; (* Initialize pointer table end *)
           end; (* prDefineTextBlock block end *)
           prGroupEnd:
           begin
            (* Loading text from InputROM *)
            PT := grPointerTables.Root;
            while PT <> NIL do with PT^ do
            begin
             ReloadStrs(Data, DLL);
             PT := Next;
            end;
            (* Finish group read *)
            Break;
           end;
           else InvalidFileFormat;
          end;
         until False; (* Read group loop end *)
        finally
         Dispose(TableSet, Done);
        end;
       end; (* prDefineGroup block end *)
       prProjectEnd:
       begin
        (* Fix tables names *)
        with Tables^ do
        begin
         PT := Root;
         while PT <> NIL do with PT^, PTable(Data)^ do
         begin
          SW := WideChangeFileExt(WideExtractFileName(tbName), '');
          if SW = '' then SW := 'TBL';
          tbName := '';
          I := 0;
          repeat
           if I = 0 then
            SW2 := SW else
            SW2 := SW + IntToStr(I);
           if Find(SW2) = nil then Break;
           Inc(I);
          until False;
          tbName := SW2;
          PT := Next;
         end;
        end;
        (* Finalize loading project *)
        Result := True;
        fSaved := False;
        fNamed := False;
        Break;
       end;
       else InvalidFileFormat;
      end;
     until False; (* End of read groups loop *)
    finally
     FreeROM;
    end;
   end; (* Finish loading project *)
  finally
   MainList.Free;
  end;
 except
  if Console then
   ConsoleWriteln(Format('Unable to load file ''%s''.', [FileName]));
 end;
end;

function TTableSet.Add: PTableSetItem;
begin
 New(Result);
 if Root = NIL then Root := Result else Cur^.Next := Result;
 Cur := Result;
 Inc(Count);
 FillChar(Result^, SizeOf(TTableSetItem), 0);
end;

destructor TTableSet.Done;
var
 N: PTableSetItem;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Dispose(Root);
  Root := N;
 end;
 Cur := NIL;
 Count := 0;
end;

function TTableSet.Get(Index: Integer): PTableSetItem;
var
 N: PTableSetItem;
 I: Integer;
begin
 N := Root; Result := NIL; I := 0;
 while N <> NIL do
 begin
  if I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N.Next;
 end;
end;

constructor TTableSet.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TDLLs.MethodGet(M: TMethod): PDLL;
var
 D: PDLL;
begin
 Result := NIL;
 if Count <= 0 then Exit;
 D := Root;
 while D <> NIL do with D^ do
 begin
  if GetMethod = M then
  begin
   Result := D;
   Break;
  end;
  D := Next;
 end;
end;

procedure TMainForm.MenuFileOpenActionExecute(Sender: TObject);
begin
 if Projects = NIL then Exit;
 with OpenProjectDialog do if Execute then OpenFiles(OpenProjectDialog.Files);
end;

function TTable.FindEnd(C: WideChar): PTableItem;
var
 R: PTableItem;
begin
 R := EndRoot; Result := NIL;
 while R <> NIL do with R^ do
 begin
  if C = tiString then
  begin
   Result := R;
   Exit;
  end;
  R := Next;
 end;
end;

function TTable.FindEl(C: WideChar): PTableItem;
var
 R: PTableItem;
begin
 R := ElRoot; Result := NIL;
 while R <> NIL do with R^ do
 begin
  if C = tiString then
  begin
   Result := R;
   Exit;
  end;
  R := Next;
 end;
end;

procedure TMainForm.WMDropFiles(var Msg: TMessage);
var
 FileName: array[0..1023] of WideChar;
begin
 if Projects = NIL then Exit;
 DragQueryFileW(THandle(Msg.WParam), 0, FileName, Sizeof(FileName));
 FlashWindow(MainForm.Handle, True);
 DragFinish(THandle(Msg.WParam));
 OpenFile(PWideChar(@FileName));
end;

procedure TMainForm.OpenFiles(Files: TWideStrings);
var
 S: WideString;
 I: Integer;
 KP: PKruptarProject;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with Projects^ do for I := 0 to Files.Count - 1 do
 begin
  S := Files.Strings[I];
  KP := Find(S);
  if KP = NIL then
  begin
   KP := Add;
   if KP.Open(S) then
   begin
    AddRecent(S);
    if not KP.fSaved then
     AllSaved := False;
    if I = Files.Count - 1 then
    begin
     SelectedData := KP;
     CanCheck := False;
     RefreshProjectTabs(Count - 1);
     RefreshTree(Count - 1);
     GroupsTabControl.Tabs.Clear;
     ChangeGroups(SelectedRoot.GetLastChild);
//     TreeViewChange(TreeView, TreeView.Selected);
//     GroupsTabControl.Tabs.Clear;
//     RefreshGroups(SelectedRoot.GetLastChild, True);
     GroupsTabControlChange(TreeView);
     NotChange := False;
    end;
   end else
   begin
    AddStateString(etError, '', -1,
    WideFormat(K7S(SMUUnableToLoad), [S]), Files.Count > 1);
    Remove(KP);
   end;
  end else
  begin
   if LocMessageDlg(WideFormat(k7S(SMUReloadConfirmation), [S]),
                    mtConfirmation, [mbYes, mbNo], 0) = mrYes then
   begin
    KP.Done;
    KP.Init;
    if KP.Open(S) then
    begin
     AddRecent(S);
     if not KP.fSaved then
      AllSaved := False;
     if I = Files.Count - 1 then
     begin
      SelectedData := KP;
      CanCheck := False;
      RefreshProjectTabs(GetIndex(KP));
      RefreshTree(GetIndex(KP));
      GroupsTabControl.Tabs.Clear;
      ChangeGroups(SelectedRoot.GetLastChild);
//      TreeViewChange(TreeView, TreeView.Selected);
//      GroupsTabControl.Tabs.Clear;
//      RefreshGroups(SelectedRoot.GetLastChild, True);
      GroupsTabControlChange(TreeView);
      NotChange := False;
     end;
    end else
    begin
     AddStateString(etError, '', -1,
     WideFormat(K7S(SMUUnableToLoad), [S]), Files.Count > 1);
     Remove(KP);
{     if Root <> NIL then SelectedData := Root.Data else SelectedData := NIL;
     CanCheck := False;
     RefreshTree;
     TreeViewClick(TreeView);
     GroupsTabControl.Tabs.Clear;
     RefreshGroups(SelectedRoot.GetLastChild, True);
     GroupsTabControlChange(TreeView);
     TreeViewChange(TreeView, TreeView.Selected);}
     NotChange := False;
    end;
   end;
  end;
 end;
end;

procedure TMainForm.MenuFileCloseActionExecute(Sender: TObject);
var
 Project: PKruptarProject;
 S: WideString;
label
 XXX;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 with Project^ do
 begin
  if not fSaved then
  begin
   if not fNamed then
    S := ProjectFile else
    S := Dir + ProjectFile;
   Case LocMessageDlg(WideFormat(K7S(SMUSaveBeforeClosingConfirmation), [S]),
        mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
    mrYes:
    begin
     if CanCheck then CheckValues;
     CanCheck := False;
     if SaveProject(Project) then
     begin
    XXX:
{      N := SelectedRoot;
      with Projects^ do
      if (Root <> NIL) and (Project = Root^.Data) then
       TreeView.Selected := N.GetNextSibling else
       TreeView.Selected := N.GetPrevSibling;}
      Projects.Remove(Project);
      SelectedRoot := nil;
      RefreshProjectTabs(ProjectTabs.TabIndex);
      RefreshTree(ProjectTabs.TabIndex);
      TreeView.Selected := TreeView.Items.getfirstnode;
      TreeViewChange(nil, TreeView.Selected);
{      N.Delete;
      TreeViewChange(Sender, TreeView.Selected);}
//      if SelectedRoot <> NIL then
//      begin
    //   with SelectedRoot do if not Expanded then Expand(False);
//      end else
      if SelectedRoot = nil then
      begin
//       SelectedRoot := NIL;
       SelectedData := NIL;
       SelectedGroup := NIL;
       SelectedList := NIL;
       ValueListBox.Visible := False;
       ValueListBox.EditorMode := False;
       ValueListBox.Visible := False;
       CurGroup := NIL;
       GroupsTabControlChange(Sender);
      end;
     end;
    end;
    mrNo:
    begin
     if CanCheck then CheckValues;
     CanCheck := False;
     Goto XXX;
    end;
   end;
  end else
  begin
   if CanCheck then CheckValues;
   CanCheck := False;
   Goto XXX;
  end;
 end;
 AllSavedCheck;
end;

procedure TMainForm.MenuFileSaveActionExecute(Sender: TObject);
begin
 if SelectedRoot = NIL then Exit;
 SaveProject(SelectedRoot.Data);
 AllSavedCheck; 
end;

procedure TMainForm.MenuFileSaveAsActionExecute(Sender: TObject);
var
 Project: PKruptarProject;
 SaveFNamed: Boolean;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 with Project^ do
 begin
  SaveFNamed := fNamed;
  fNamed := False;
  if not SaveProject(Project) then fNamed := SaveFNamed;
 end;
 AllSavedCheck; 
end;

procedure TMainForm.MenuFileSaveAllActionExecute(Sender: TObject);
var
 N: PDynamic;
begin
 N := Projects.Root;
 while N <> NIL do
 begin
  SaveProject(N.Data);
  N := N.Next;
 end;
 AllSaved := True;
end;

function TMainForm.SaveProject(PRJ: Pointer): Boolean;
var
 P: PDynamic;
 S: WideString;
 AddToRecent: Boolean;
 Project: PKruptarProject absolute PRJ;
begin
 Result := False;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 AddToRecent := False;
 with Project^ do
 begin
  if not fNamed then
  begin
   if GetExt(Dir + ProjectFile, S) <> '.KPX' then
    SaveProjectDialog.FileName := S + '.kpx' else
    SaveProjectDialog.FileName := Dir + ProjectFile;
   if SaveProjectDialog.Execute then
   begin
    ProjectFile := FSplit(SaveProjectDialog.FileName, Dir);
    fNamed := True;
    AddToRecent := True;
   end else Exit;
  end;
  Result := Save;
  FSaved := not Result;
  SetSaved(Project, Result);
  if Result and AddToRecent then
   AddRecent(SaveProjectDialog.FileName);
 end;
 if AllSaved then
 begin
  P := Projects.Root;
  while P <> nil do with P^ do
  begin
   if not PKruptarProject(Data).fSaved then
   begin
    AllSaved := False;
    Exit;
   end;
   P := Next;
  end;
 end;
end;

var
 CloseResult: Boolean = False;

procedure TMainForm.MenuFileCloseAllActionExecute(Sender: TObject);
var
 //N: TKruptarTreeNodeType;
 Project: PKruptarProject;
 N: PDynamic;
 S: WideString;
begin
// N := TreeView.Items.GetFirstNode;
 if CanCheck then CheckValues;
 CanCheck := False;
 N := Projects.Root;
 while N <> NIL do
 begin
  Project := N.Data;
  with Project^ do
  begin
   if not fSaved then
   begin
    if not fNamed then
     S := ProjectFile else
     S := Dir + ProjectFile;
    Case LocMessageDlg(WideFormat(K7S(SMUSaveBeforeClosingConfirmation), [S]),
                    mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
     mrYes: if not SaveProject(Project) then
     begin
      CloseResult := False;
      Exit;
     end;
     mrCancel, mrNone:
     begin
      CloseResult := False;
      Exit;
     end;
    end;
   end;
  end;
  N := N.Next;
 end;
 AllSaved := True;
 Dispose(Projects, Done);
 New(Projects, Init);
{ N := TreeView.Items.GetFirstNode;
 while N <> NIL do
 begin
  Project := N.Data;
  if Project <> NIL then with Project^ do
  begin
   NN := N.GetNextSibling;
   Projects.Remove(Project);
   N.Delete;
   N := NN;
  end else N := N.GetNextSibling;
 end;  }
 SelectedRoot := NIL;
 SelectedData := NIL;
 SelectedGroup := NIL;
 SelectedList := NIL;
 ValueListBox.Visible := False;
 ValueListBox.EditorMode := False;
 ValueListBox.Visible := False;
 TreeView.Items.Clear;
 CurGroup := NIL;
 RefreshProjectTabs(-1);
 GroupsTabControlChange(Sender);
 CloseResult := True;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 MenuFileCloseAllActionExecute(Sender);
 CanClose := CloseResult;
end;

procedure TMainForm.ValueListBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
 V: PValue;
begin
 if Key = VK_Delete then
 begin
  if Values = NIL then Exit;
  V := Values.Get(ValueListBox.Row);
  if V = NIL then Exit;
  if not V.CanEdit then Key := 0;
 end; 
end;

procedure TMainForm.MenuEditUndoActionExecute(Sender: TObject);
var
 Hndl: HWND;
begin
 Hndl := GetEditHandle;
 if Hndl <> 0 then SendMessage(Hndl, WM_UNDO, 0, 0)
end;

procedure TMainForm.ValueListBoxClick(Sender: TObject);
var
 V: PValue;
 BRow: Integer;
begin
 BRow := ValueListBox.Row;
 if BRow < 0 then Exit;
 if Values = NIL then Exit;
 V := Values.Get(BRow);
 if V = NIL then Exit;
 with ValueListBox do
 if V.CanEdit then
  Options := [goVertLine, goHorzLine, goEditing,
              goAlwaysShowEditor,goThumbTracking] else
 begin
  Options := [goVertLine, goHorzLine, goThumbTracking];
  EditorMode := False;
 end;
end;

function TTables.Get(Index: Integer): PTable;
var
 I: Integer;
 P: PDynamic;
begin
 Result := NIL;
 if (Index < 0) or (Index >= Count) then Exit;
 I := 0;
 P := Root;
 while P <> NIL do with P^ do
 begin
  if I = Index then
  begin
   Result := Data;
   Exit;
  end;
  Inc(I);
  P := Next;
 end;
end;

function TGroups.Get(Index: Integer): PGroup;
var
 I: Integer;
 P: PDynamic;
begin
 Result := NIL;
 if (Index < 0) or (Index >= Count) then Exit;
 I := 0; P := Root;
 while P <> NIL do with P^ do
 begin
  if I = Index then
  begin
   Result := Data;
   Exit;
  end;
  Inc(I);
  P := Next;
 end;
end;

procedure TMainForm.MenuEditSaveActionExecute(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 KP: PKruptarProject;
 SaveDlg: TLocSaveDialog;
 Stream: TWideFileStream;
 Ext: WideString;
begin
 if SelectedRoot = NIL then Exit;
 KP := SelectedRoot.Data;
 if KP = nil then Exit;
 SaveDlg := nil;
 if TreeView.Focused then
 begin
  Node := TreeView.Selected;
  if Node = nil then Exit;
  if Node.Data = nil then Exit;
  with Node do
  case TTreeStateType(StateIndex) of
   tsTable, tsEnds, tsEls, tsCodes:
   begin
    case TTreeStateType(StateIndex) of
     tsTable: Ext := '.tbl';
     tsEnds: Ext := '_TRMN.tbl';
     tsEls: Ext := '_CLRF.tbl';
     tsCodes: Ext := '_CHR.tbl';
    end;
    if StateIndex <> Integer(tsTable) then
     Node := Parent;
    SaveDlg := SaveTableDialog;
    SaveDlg.FileName := PTable(Node.Data).tbName + Ext;
   end;
   tsBlocks:
   begin
    SaveDlg := SaveTextDialog;
    SaveDlg.FileName := PGroup(Parent.Data).grName + '_Ranges.txt';
   end;
   tsPtrTable:
   begin
    SaveDlg := SaveTextDialog;
    SaveDlg.FileName := FixedFileName(PPointerTable(Data).ptName) + '.txt';
   end;
  end;
 end;
 if SaveDlg = nil then
 begin
  SaveDlg := SaveTextDialog;
  SaveDlg.FileName := '';
  if CurGroup <> nil then with CurGroup^ do
  begin
   if ListBox.Focused then
   begin
    if grCurPtrTable <> nil then
     SaveDlg.FileName := FixedFileName(grCurPtrTable.ptName) + '_Selection.txt';
   end else
   if GroupsTabControl.Focused then
    SaveDlg.FileName := CurGroup.grName + '.txt' else
   if PTablesTabControl.Focused then
   begin
    if grCurPtrTable <> nil then
     SaveDlg.FileName := FixedFileName(grCurPtrTable.ptName) + '.txt';
   end;
  end;
 end;
 if SaveDlg.Execute then
 try
  Stream := TWideFileStream.Create(SaveDlg.FileName, fmCreate);
  try
   WriteCodePagedString(CopyStringData(False), Stream,
    KP.CodePage or CP_WideBOM or CP_MbcsBOM);
  finally
   Stream.Free;
  end;
 except
  LocMessageDlg(WideFormat(K7S(SMUUnableToSave), [SaveDlg.FileName]),
                 mtError, [mbOk], 0);
 end;
end;

function StringsToTextStrings(Table: Pointer; Strings: Pointer;
  Align: Integer; KP: PKruptarProject): PTextStrings;
var
 SS: PString;
 TBL: PTable;
 STR: PKStrings;
begin
 TBL := Table;
 Str := Strings;
 Result := NIL;
 if Strings = NIL then Exit;
 New(Result, Init);
 with Result^ do
 begin
  SS := STR.Root;
  while SS <> NIL do with SS^ do
  begin
   with Add^ do
   if Table = NIL then
    Str := stOriginal else
    Str := TBL.StringToData(stNew, Align,
           KP.Flags and KP_CRLF_CONVERT <> 0);
   SS := Next;
  end;
 end;
end;

function FindTextString(Node: PTextString; const S: RawByteString): LongInt;
var
 ResultData: LongRec absolute Result;
begin
 Result := 0;
 while Node <> NIL do with Node^, ResultData do
 begin
  Hi := Pos(S, Str);
  if Hi > 0 then Exit;
  Inc(Lo);
  Node := Next;
 end;
 Result := -1;
end;

procedure TMainForm.MenuProjectTextInsertActionExecute(Sender: TObject);
var
 Project: PKruptarProject;
 G: PDynamic;
 B: PBlock;
 NewSize: Integer;
 ResultI: Boolean;
 F: File;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 try
  ResultI := True;
  MForm := Self;
  InsertResultDialog.Memo.Clear;
  with MyProgressDialog, Project^ do if Groups <> NIL then with Groups^ do
  begin
   if not FieldFileExists(KP_DST_ROM, OutputROM, False) then Exit;
   ProjectGauge.Visible := True;
   ProjectLabel.Visible := True;
   ProjectGauge.Progress := 0;
   GroupGauge.Progress := 0;
   PtrTableGauge.Progress := 0;
   if Count <= 0 then Exit;
   ProjectLabel.Caption := ProjectFile;
   ProjectProgress := 0;
   ProjectGauge.MaxValue := Count;
   NewSize := 0;
   G := Root;
   while G <> NIL do with G^, PGroup(Data)^ do
   begin
    B := grBlocks.Root;
    while B <> NIL do with B^ do
    begin
     if blStart + blCount > NewSize then NewSize := blStart + blCount;
     B := Next;
    end;
    G := Next;
   end;
   if LoadRom(False, OutputRom, NewSize) then
   try
    Self.Enabled := False;
    G := Root;
    while G <> NIL do with G^, PGroup(Data)^ do
    begin
     B := grBlocks.Root;
     while B <> NIL do with B^ do
     begin
      FillChar(ROM[blStart], blCount, 0);
      B := Next;
     end;
     G := Next;
    end;
    G := Root;
    while G <> NIL do with G^ do
    begin
     if not GroupAddToRom(Data, Project) then ResultI := False; //BUG FIXED
     if not Visible then Show;
     ProjectProgress := ProjectProgress + 1;
     G := Next;
    end;
    SetSaved(SelectedRoot.Data, False);   
    try
     AssignFile(F, OutputRom);
     Rewrite(F, 1);
     try
      BlockWrite(F, Rom^, RomSize);
     finally
      CloseFile(F);
     end;
    except
     LocMessageDlg(WideFormat(K7S(SMUUnableToSave), [OutputRom]),
      mtError, [mbOk], 0);
    end;
   finally
    ProgressCanClose := True;
    FreeRom;
    if not ResultI or ProgressAutoClose then Close;
    if not ResultI then InsertResultDialog.ShowModal;
    NotChange := True;
    RefreshGroups(SelectedRoot.GetLastChild, False);
    NotChange := False;
    TreeViewChange(Sender, TreeView.Selected);
   end;
  end;
 except
  Self.Enabled := True;
  raise;
 end;
end;

procedure TMainForm.MenuProjectGroupTextInsertActionExecute(Sender: TObject);
var
 Group: PGroup;
 B: PBlock;
 Project: PKruptarProject;
 NewSize: Integer;
 ResultI: Boolean;
 F: File;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 if CurGroup = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 try
  if TreeView.Focused and (TreeView.Selected.StateIndex = Integer(tsGroup)) then
   Group := SelectedGroup else
   Group := CurGroup;
  MForm := Self;
  ResultI := False;
  InsertResultDialog.Memo.Clear;
  with MyProgressDialog, Project^ do
  begin
   if not FieldFileExists(KP_DST_ROM, OutputROM, False) then Exit;
   ProjectGauge.Visible := False;
   ProjectLabel.Visible := False;
   GroupGauge.Progress := 0;
   PtrTableGauge.Progress := 0;
   NewSize := 0;
   with Group^ do
   begin
    B := grBlocks^.Root;
    while B <> NIL do with B^ do
    begin
     if blStart + blCount > NewSize then NewSize := blStart + blCount;
     B := Next;
    end;
   end;
   if LoadRom(False, OutputRom, NewSize) then
   try
    Self.Enabled := False;
    with Group^ do
    begin
     B := grBlocks^.Root;
     while B <> NIL do with B^ do
     begin
      FillChar(ROM[blStart], blCount, 0);
      B := Next;
     end;
    end;
    ResultI := GroupAddToRom(Group, Project);
    SetSaved(SelectedRoot.Data, False);
    try
     AssignFile(F, OutputRom);
     Rewrite(F, 1);
     try
      BlockWrite(F, Rom^, RomSize);
     finally
      CloseFile(F);
     end;
    except
     LocMessageDlg(WideFormat(K7S(SMUUnableToSave), [OutputRom]),
                   mtError, [mbOk], 0);
    end;
   finally
    ProgressCanClose := True;
    FreeRom;
    if not ResultI or ProgressAutoClose then Close;
    if not ResultI then InsertResultDialog.ShowModal;
    NotChange := True;
    RefreshGroups(SelectedRoot.GetLastChild, False);
    NotChange := False;
    TreeView.Selected.Expand(False);
    TreeViewChange(Sender, TreeView.Selected);
   end;
  end;
 except
  Self.Enabled := True;
  raise;
 end;      
end;

procedure TMainForm.MenuProjectGroupDictOptionsActionExecute(Sender: TObject);
var
 Node, N: TKruptarTreeNodeType;
 Project: PKruptarProject;
 PickList: TWideStringList;
 P: PDynamic;
 I, Index: Integer;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False; 
 Node := TreeView.Selected;
 if (Node <> NIL) and (Node.Data <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsGroup, tsPtrTables, tsBlocks, tsPtrTable, tsPointer]) then with Project^ do
 begin
  N := Node;
  while (N <> NIL) and (N.StateIndex <> Integer(tsGroup)) do N := N.Parent;
  if (N <> NIL) and (N.Data <> NIL) then
  with PGroup(N.Data)^ do if grIsDictionary then
  begin
   PickList := TWideStringList.Create;
   try
    PickList.Add(SMUNone);
    P := Tables.Root; I := 1;
    Index := 0;
    while P <> NIL do with P^, PTable(Data)^ do
    begin
     if grDictTable = Data then Index := I;
     PickList.Add(tbName);
     Inc(I);
     P := Next;
    end;
    Case grDictItemSize of
     2: if grDictMotorola then I := 2 else I := 1;
     3: if grDictMotorola then I := 4 else I := 3;
     4: if grDictMotorola then I := 6 else I := 5;
     else I := 0;
    end;
    with DictionaryDialog do
    if Execute(PickList, grDictItemFirstBytes, Index,
               I, grDictItemStart, grMaxWordSize) then
    begin
     grDictTable := Tables.Get(TablesBox.ItemIndex - 1);
     grDictItemFirstBytes := FirstBytes;
     grDictItemStart := GetLDW(ItemStartEdit.Text);
     grMaxWordSize := MaxWordSizeEdit.Value;
     Case SizeBox.ItemIndex of
      1:
      begin
       grDictItemSize := 2;
       grDictMotorola := False;
      end;
      2:
      begin
       grDictItemSize := 2;
       grDictMotorola := True;
      end;
      3:
      begin
       grDictItemSize := 3;
       grDictMotorola := False;
      end;
      4:
      begin
       grDictItemSize := 3;
       grDictMotorola := True;
      end;
      5:
      begin
       grDictItemSize := 4;
       grDictMotorola := False;
      end;
      6:
      begin
       grDictItemSize := 4;
       grDictMotorola := True;
      end;
      else
      begin
       grDictItemSize := 1;
       grDictMotorola := False;
      end;
     end;
    end;
    SetSaved(SelectedRoot.Data, False);
   finally
    PickList.Free;
   end;
  end;
 end;
end;

procedure TMainForm.MenuProjectGroupDictToTableActionExecute(Sender: TObject);
var
 Node, N: TKruptarTreeNodeType;
 Project: PKruptarProject;
 Group: PGroup;
 ST: PString;
 PT: PDynamic;
 P: PPointer;
 CD: RawByteString;
 Index, Len1, Len2: Integer;
 TI, EndItem: PTableItem;
 Hex: WideString;
 Tbl: PTable;
begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 Node := TreeView.Selected;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if (Node <> NIL) and (Node.Data <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsGroup, tsPtrTables, tsBlocks, tsPtrTable, tsPointer]) then with Project^ do
 begin
  N := Node;
  while (N <> NIL) and (N.StateIndex <> Integer(tsGroup)) do N := N.Parent;
  if (N <> NIL) and (N.Data <> NIL) then
  begin
   Group := N.Data;
   with MyProgressDialog, Group^ do
   if grIsDictionary and (grDictTable <> NIL) then
   begin
    PT := grPointerTables.Root;
    Index := grDictItemStart;
    while PT <> NIL do with PT^, PPointerTable(Data)^ do
    begin
     P := ptPointers.Root;
     while P <> NIL do with P^ do
     begin
      ST := pnStrings^.Root;
      while ST <> NIL do with ST^ do
      begin
       CD := grDictItemFirstBytes;
       Case grDictItemSize of
        1: CD := CD + AnsiChar(Index);
        2: with TABCD(Index) do
        if grDictMotorola then
         CD := CD + AnsiChar(B) + AnsiChar(A) else
         CD := CD + AnsiChar(A) + AnsiChar(B);
        3: with TABCD(Index) do
       if grDictMotorola then
        CD := CD + AnsiChar(C) + AnsiChar(B) + AnsiChar(A) else
        CD := CD + AnsiChar(A) + AnsiChar(B) + AnsiChar(C);
        4: with TABCD(Index) do
        if grDictMotorola then
         CD := CD + AnsiChar(D) + AnsiChar(C) + AnsiChar(B) + AnsiChar(A) else
         CD := CD + AnsiChar(A) + AnsiChar(B) + AnsiChar(C) + AnsiChar(D);
       end;
       with grDictTable^ do
       begin
        TI := GetByCodes(CD);
        if TI = NIL then TI := Add;
        with TI^ do
        begin
         tiCodes := CD;
         TBL := pnTable2;
         if TBL = nil then
          TBL := pnTable1;
         if TBL <> nil then
         begin
          tiString := stNew;
          if Flags and KP_NOEND_DIC_TBL = 0 then
          begin
           EndItem := TBL.EndRoot;
           while (EndItem <> nil) do
           begin
            Len1 := Length(tiString);
            if Length(EndItem.tiCodes) * 3 <= Len1 then
            begin
             Hex := HexDataToString(EndItem.tiCodes);
             Len2 := Length(Hex);
             Dec(Len1, Len2);
             if CompareMem(Addr(tiString[Len1]), Pointer(Hex), Len2 shl 1) then
             begin
              SetLength(tiString, Len1);
              Break;
             end;
            end;
            EndItem := EndItem.Next;
           end;
          end;
         end else tiString := '';
         if tiString = '' then RemoveByItem(Root, TI);
        end;
       end;
       Inc(Index);
       ST := Next;
      end;
      P := Next;
     end;
     PT := Next;
    end;
    grDictTable.RefreshMax;
    SelectedData := grDictTable;
    RefreshTables(SelectedRoot.GetFirstChild);
    SetSaved(SelectedRoot.Data, False);    
   end;
  end;
 end;
end;

type
 PWideString = ^TWideString;
 TWideString = record
  Str: WideString;
  Count: Integer;
  Next: PWideString;
 end;
 PKWideStrings = ^TKWideStrings;
 TKWideStrings = object
  Root, Cur: PWideString;
  Count: Integer;
  constructor Init;
  function Add: PWideString;
  function Get(I: Integer): PWideString;
  function GetSize(Item: PWideString): Integer;
  destructor Done;
  procedure Sort;
 end;

function TKWideStrings.GetSize(Item: PWideString): Integer;
begin
 if Item <> NIL then with Item^ do
  Result := Count * Length(Str) else
  Result := 0;
end;

procedure TKWideStrings.Sort;
type
 PWSArray = ^TWSArray;
 TWSArray = array[Word] of PWideString;
var
 WSArray: PWSArray;
 WS: PWideString;
 procedure QuickSort(L, R: Integer);
 var
  I, J, X: Integer; Y: Pointer;
 begin
  I := L; J := R;
  X := GetSize(WSArray[(L + R) shr 1]);
  repeat
   while GetSize(WSArray[I]) < X do
   begin
    Inc(I);
    if I >= Count then Break;
   end;
   while X < GetSize(WSArray[J]) do
   begin
    Dec(J);
    if J < 0 then Break;
   end;
   if I <= J then
   begin
    Y := WSArray[I];
    WSArray[I] := Addr(WSArray[J]^);
    WSArray[J] := Y;
    Inc(I); Dec(J);
   end;
  until I > J;
  if L < J then QuickSort(L, J);
  if J < R then QuickSort(I, R);
 end;
var I: Integer;
begin
 if Count <= 1 then Exit;
 GetMem(WSArray, Count shl 2);
 try
   WS := Root; I := 0;
   while WS <> NIL do
   begin
    WSArray[I] := Addr(WS^);
    Inc(I);
    WS := WS.Next;
   end;
   QuickSort(0, Count - 1);
   Root := WSArray[Count - 1];
   Cur := WSArray[0];
   for I := Count - 1 downto 0 do
   begin
    WS := WSArray[I];
    if I = 0 then
     WS.Next := NIL else
     WS.Next := WSArray[I - 1];
   end;
 finally
   FreeMem(WSArray);
 end;
end;

function TKWideStrings.Get(I: Integer): PWideString;
var
 N: PWideString;
 J: Integer;
begin
 N := Root;
 J := 0;
 while N <> NIL do
 begin
  if J = I then
  begin
   Result := N;
   Exit;
  end;
  Inc(J);
  N := N^.Next;
 end;
 Result := NIL;
end;

constructor TKWideStrings.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

function TKWideStrings.Add: PWideString;
begin
 New(Result);
 if Root = NIL then Root := Result else Cur.Next := Result;
 Cur := Result;
 Inc(Count);
 Result.Str := '';
 Result.Count := 0;
 Result.Next := NIL;
end;

destructor TKWideStrings.Done;
var
 N: PWideString;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Root.Str := '';
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

procedure TMainForm.MenuProjectGroupDictGenerateActionExecute(Sender: TObject);

var
 WideStrings: UnicodeUtils.TWideStrings;
 BigString, BS: WideString;
 Group: PGroup;
 Node, N: TKruptarTreeNodeType;
 Project: PKruptarProject;
 Words: PKWideStrings;
 G, PT: PDynamic;
 P: PPointer;
 ST: PString;
 Len, MaxWords, MinLen: Integer;

type
 TWord = record
  Str: WideString;
  Count: Integer;
 end;

 function GetWord: TWord;
 var
  Index, Index2, Ind, L, P, K, SL: Integer;
  WC: WideChar;
 begin
  with MyProgressDialog, Result do
  begin
   Index := 1;
   SL := Length(BigString);
   GroupLabel.Caption := K7S(SMUWordSearching);
   GroupGauge.MinValue := 1;
   GroupProgress := 1;
   GroupGauge.MaxValue := SL;
   repeat
    L := 0;
    while Index <= SL do
    begin
     WC := BigString[Index];
     if (WC = #0) or (WC = #9) or (WC = #13) or (WC = #10) then
      Inc(Index) else Break;
    end;
    if Index > SL then Exit;
    Str := '';
    Count := 0;
    P := Index;
    while L < Len do
    begin
     WC := BigString[Index];
     if (WC = #0) or (WC = #9) or (WC = #13) or (WC = #10) then Break;
     Str := Str + WC;
     Inc(L);
     Inc(Index);
    end;
    L := Length(Str);
    if not Visible then Show;
    GroupProgress := Index;
    if L >= MinLen then
    begin
     Count := 1;
     Ind := Index;
     PtrTableLabel.Caption := K7S(SMUWordIterationSearching);
     PtrTableGauge.MinValue := 1;
     PtrTableProgress := 1;
     PtrTableGauge.MaxValue := SL;
     Index2 := Index;
     repeat
      while Index2 <= SL do
      begin
       WC := BigString[Index2];
       if (WC = #0) or (WC = #9) or (WC = #13) or (WC = #10) then
        Inc(Index2) else Break;
      end;
      if (Index2 > SL) or (Index2 + L - 1 > SL) then Break else
      if CompareMem(Addr(Str[1]), Addr(BigString[Index2]), L shl 1) then
      begin
       for K := Index2 to Index2 + L - 1 do BigString[K] := #9;
       Inc(Index2, L);
       Inc(Ind, L);
       Inc(Count);
      end else
      begin
       Inc(Ind);
       Index2 := Ind;
      end;
      PtrTableProgress := Index2;
     until False;
     Delete(BigString, P, Length(Str));
     Exit;
    end;
   until False;
  end;
 end;

var
 Word: TWord;
 C1, C2: WideChar;
 WS: PWideString;
 LI: TKruptarListItemType;
 ZZ, XX: Integer;
 STRX: WideString;
 PS: PString;
 vfound: WideString;

begin
 if SelectedRoot = NIL then Exit;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False; 
 Node := TreeView.Selected;
 if (Node <> NIL) and (Node.Data <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsGroup, tsPtrTables, tsBlocks, tsPtrTable, tsPointer]) then with Project^ do
 begin
  N := Node;
  while (N <> NIL) and (N.StateIndex <> Integer(tsGroup)) do N := N.Parent;
  if (N <> NIL) and (N.Data <> NIL) then
  begin
   Group := N.Data;
   with MyProgressDialog, Group^ do if grIsDictionary then
   begin
    MaxWords := 0;
    PT := grPointerTables.Root;
    while PT <> NIL do with PT^, PPointerTable(Data)^ do
    begin
     P := ptPointers.Root;
     while P <> NIL do with P^ do
     begin
      PS := pnStrings.Root;
      while PS <> NIL do with PS^ do
      begin
       Inc(MaxWords);
       PS := Next;
      end;
      P := Next;
     end;
     PT := Next;
    end;
    if MaxWords < 1 then Exit;
    WideStrings := TWideStringList.Create;
    try
     G := Groups.Root;
     while G <> NIL do with G^, PGroup(Data)^ do
     begin
      PT := grPointerTables.Root;
      while PT <> NIL do with PT^, PPointerTable(Data)^ do
      begin
       if ptDictionary = Group then with ptPointers^ do
       begin
        P := Root;
        while P <> NIL do with P^ do
        begin
         ST := pnStrings.Root;
         while ST <> NIL do with ST^ do
         begin
          BigString := stNew;
          repeat
           Len := Pos('/', BigString);
           if (Len >= 1) and (Len <= Length(BigString) - 2) then
           begin
            C1 := BigString[Len + 1];
            C2 := BigString[Len + 2];
            if (((C1 >= '0') and (C1 <= '9')) or
                ((C1 >= 'A') and (C1 <= 'F')) or
                ((C1 >= 'a') and (C1 <= 'f'))) and
               (((C2 >= '0') and (C2 <= '9')) or
                ((C2 >= 'A') and (C2 <= 'F')) or
                ((C2 >= 'a') and (C2 <= 'f'))) then
            begin
             BigString[Len] := #9;
             BigString[Len + 1] := #9;
             BigString[Len + 2] := #9;
            end;
           end else Break;
          until False;
          WideStrings.Add(BigString);
          ST := Next;
         end;
         P := Next;
        end;
       end;
       PT := Next;
      end;
      G := Next;
     end;
     BigString := WideStrings.Text;
     BS := BigString;
    finally
     WideStrings.Free;
    end;
    if BigString = '' then Exit;
    New(Words, Init);
    try
     Len := grMaxWordSize;
     MForm := Self;
     ProgressCanClose := False;
     ProjectGauge.Visible := False;
     ProjectLabel.Visible := True;
     ProjectGauge.Progress := 0;
     GroupGauge.Progress := 0;
     PtrTableGauge.Progress := 0;
     Self.Enabled := False;
     vfound := K7S(SMUWordsFounded);
     ProjectLabel.Caption := WideFormat(vfound, [0]);
     ProjectProgress := 0;
     ProjectGauge.MaxValue := MaxWords;
     MinLen := (Length(grDictItemFirstBytes) + grDictItemSize) + 1;
     repeat
      Word := GetWord;
      if Word.Count <= 0 then Break;
      if Word.Count > 0 then
      begin
       with Words^.Add^ do
       begin
        Str := Word.Str;
        Count := Word.Count;
       end;
       if not Visible then Show;
       ProjectLabel.Caption := WideFormat(vfound, [Words^.Count]);
       if Words.Count = MaxWords then
       begin
        if LocMessageDlg(K7S(SMUMaxWordsFound), mtConfirmation,
           [mbYes, mbNo], 0) = mrYes then Break;
       end;
      end;
     until False;
     ProgressCanClose := True;
     Close;
     with WordsDialog do
     begin
      STRX := IntToStr(Words^.Count);
      with Words^ do
      begin
       if Count = 1 then Caption := K7S(SMUOneWord) else
       Case STRX[Length(STRX)] of
        '1':
        if (Length(STRX) > 1) and (STRX[Length(STRX) - 1] = '1') then
         Caption := WideFormat(K7S(SMUWordsCount2), [Count]) else
         Caption := WideFormat(K7S(SMUWordsCount1), [Count]);
        '2', '3', '4':
        if (Length(STRX) > 1) and (STRX[Length(STRX) - 1] = '1') then
         Caption := WideFormat(K7S(SMUWordsCount2), [Count]) else
         Caption := WideFormat(K7S(SMUWordsCount3), [Count]);
        '5', '6', '7', '8', '9', '0':
         Caption := WideFormat(K7S(SMUWordsCount2), [Count]);
       end;
      end;
      ListView.Items.Clear;
      Words.Sort;
      WS := Words.Root;
      while WS <> NIL do with WS^ do
      begin
       LI := ListView.Items.Add;
       LI.Caption := Str;
       LI.SubItems.Add(IntToStr(Count));
       WS := Next;
      end;
      if Execute then
      begin
       WS := Words.Root;
       PT := grPointerTables.Root;
       while PT <> NIL do with PT^, PPointerTable(Data)^ do
       begin
        P := ptPointers.Root;
        while P <> NIL do with P^ do
        begin
         PS := pnStrings.Root;
         while PS <> NIL do with PS^ do
         begin
          STRX := '';
          if Flags and KP_NOEND_DIC_TBL = 0 then
          begin
           if (pnTable1 <> NIL) and (pnTable1.EndRoot <> NIL) then
           with pnTable1.EndRoot^ do for Len := 1 to Length(tiCodes) do
            STRX := '/' + IntToHex(Byte(tiCodes[Len]), 2);
          end;
          if WS = NIL then
           stNew := STRX else
           stNew := WS.Str + STRX;
          if WS <> NIL then WS := WS^.Next;
          PS := Next;
         end;
         P := Next;
        end;
        PT := Next;
       end;
      end;
     end;
     SetSaved(SelectedRoot.Data, False);     
    finally
     Dispose(Words, Done);
     if CurGroup = Group then RefreshList;
     Self.Enabled := True;
    end;
   end;
  end;
 end;
end;

function TTable.GetByCodes(const Codes: RawByteString): PTableItem;
var
 T: PTableItem;
begin
 T := Root; Result := NIL;
 while T <> NIL do with T^ do
 begin
  if tiCodes = Codes then
  begin
   Result := T;
   Break;
  end;
  T := Next;
 end;
end;

procedure TMainForm.TimerTimer(Sender: TObject);
var
 L: Integer;
begin
 with OriginalStatusBar, OriginalMemo do
 begin
  if Lines.Count > 0 then
   L := Length(Lines.Strings[CaretPos.Y]) else
   L := 0;
  Panels[0].Text := WideFormat('x: %d/%d', [CaretPos.X, L]);
  Panels[1].Text := WideFormat('y: %d/%d', [CaretPos.Y, Lines.Count]);
 end;
 with TranslateStatusBar, TranslateMemo do
 begin
  if Lines.Count > 0 then
   L := Length(Lines.Strings[CaretPos.Y]) else
   L := 0;
  Panels[0].Text := WideFormat('x: %d/%d', [CaretPos.X, L]);
  Panels[1].Text := WideFormat('y: %d/%d', [CaretPos.Y, Lines.Count]);
 end;
 if InsertMode then
  TranslateStatusBar.Panels[2].Text := SSBInsertV else
  TranslateStatusBar.Panels[2].Text := SSBOverwriteV;
 with ListBox do
 begin
  if Count > 0 then
   ListStatusBar.Panels[0].Text := WideFormat(SLBLineNumberV,
                                [ItemIndex + 1, Count]) else
   ListStatusBar.Panels[0].Text := WideFormat(SLBLineNumberV, [0, 0])
 end;
end;

procedure TMainForm.MenuSearchFindActionExecute(Sender: TObject);
begin
 if not TranslateMemo.ReadOnly then with SearchTextRec do
 begin
  if SearchTextDialog.Execute then
  begin
   CanSearch := True;
   Replace := False;
   SearchString := SearchTextDialog.SearchString;
   ReplaceString := '';
   Options := [];
   if SearchTextDialog.CaseCheckBox.Checked then
    Include(Options, soMatchCase);
   if SearchTextDialog.WholeCheckBox.Checked then
    Include(Options, soOriginal);
  end else Exit;
  MenuSearchFindNextActionExecute(Sender);
 end;
end;

function FindIn(const FStr, Str: WideString; Position: Integer): Integer;
begin
 with SearchTextRec do
 begin
  Result := -1;
  if Position < 0 then Exit;
  if Position >= Length(Str) then Exit;
  if soMatchCase in Options then
   Result := Pos(FStr, Copy(Str, Position + 1, Length(Str) - Position)) else
   Result := Pos(WideUpperCase(FStr),
         WideUpperCase(Copy(Str, Position + 1, Length(Str) - Position)));
  Dec(Result);
  if Result >= 0 then Result := Result + Position;
 end;
end;

procedure TMainForm.MenuSearchFindNextActionExecute(Sender: TObject);
var
 Project: PKruptarProject;
 G, P: PDynamic;
 PP: PPointer;
 Founded: Boolean;
 PS: PString;
 Start: Boolean;
 Index, GroupIndex: Integer;
 PtrTableIndex, StringIndex: Integer;
begin
 if SelectedRoot = NIL then Exit;
 Founded := False;
 Project := SelectedRoot.Data;
 if Project = NIL then Exit;
 if not TranslateMemo.ReadOnly then with SearchTextRec, Project^ do
 begin
  if not CanSearch then
  begin
   MenuSearchFindActionExecute(Sender);
   Exit;
  end;
  if not Replace then
  begin
   G := Groups.Root; Start := False;
   GroupIndex := 0;
   while G <> NIL do with G^, PGroup(Data)^ do
   begin
    P := grPointerTables.Root;
    PtrTableIndex := 0;
    while P <> NIL do with P^, PPointerTable(Data)^ do
    begin
     PP := ptPointers.Root;
     StringIndex := 0;
     while PP <> NIL do with PP^ do
     begin
      PS := pnStrings.Root;
      while PS <> NIL do with PS^ do
      begin
       if not Start then
       begin
        if CurString = PS then
        begin
         if soOriginal in Options then
         begin
          Index := FindIn(SearchString, OriginalMemo.Text,
                          OriginalMemo.SelStart + OriginalMemo.SelLength);
          if Index >= 0 then
          begin
           OriginalMemo.SetFocus;
           OriginalMemo.SelStart := Index;
           OriginalMemo.SelLength := Length(SearchString);
           Exit;
          end;
         end else
         begin
          Index := FindIn(SearchString, stNew,
                          TranslateMemo.SelStart + TranslateMemo.SelLength);
          if Index >= 0 then
          begin
           TranslateMemo.SetFocus;
           TranslateMemo.SelStart := Index;
           TranslateMemo.SelLength := Length(SearchString);
           Exit;
          end;
         end;
         Start := True;
        end;
       end else
       begin
        if soOriginal in Options then
         Index := FindIn(SearchString,
                         ConvertString(pnTable1, stOriginal,
                         Flags and KP_CRLF_CONVERT <> 0),
                         0) else
         Index := FindIn(SearchString, stNew, 0);
        if Index >= 0 then
        begin
         with GroupsTabControl do if TabIndex <> GroupIndex then
         begin
          TabIndex := GroupIndex;
          GroupsTabControlChange(Sender);
         end;
         with PTablesTabControl do if TabIndex <> PtrTableIndex then
         begin
          TabIndex := PtrTableIndex;
          PTablesTabControlChange(Sender);
         end;
         ListBox.ClearSelection;
         ListBox.ItemIndex := StringIndex;
         ListBox.Selected[StringIndex] := True;
         ListBoxClick(Sender);
         if soOriginal in Options then
         begin
          OriginalMemo.SetFocus;
          OriginalMemo.SelStart := Index;
          OriginalMemo.SelLength := Length(SearchString);
         end else
         begin
          TranslateMemo.SetFocus;
          TranslateMemo.SelStart := Index;
          TranslateMemo.SelLength := Length(SearchString);
         end;
         Exit;
        end;
       end;
       Inc(StringIndex);
       PS := Next;
      end;
      PP := Next;
     end;
     Inc(PtrTableIndex);
     P := Next;
    end;
    Inc(GroupIndex);
    G := Next;
   end;
   LocMessageDlg(K7S(SMUTextStringNotFound), mtInformation, [mbOk], 0);
  end else
  begin
   G := Groups.Root; Start := False;
   GroupIndex := 0;
   while G <> NIL do with G^, PGroup(Data)^ do
   begin
    P := grPointerTables.Root;
    PtrTableIndex := 0;
    while P <> NIL do with P^, PPointerTable(Data)^ do
    begin
     PP := ptPointers.Root;
     StringIndex := 0;
     while PP <> NIL do with PP^ do
     begin
      PS := pnStrings.Root;
      while PS <> NIL do with PS^ do
      begin
       if not Start then
       begin
        if CurString = PS then
        begin
         Index := FindIn(SearchString, stNew,
                  TranslateMemo.SelStart + TranslateMemo.SelLength);
         if Index >= 0 then
         begin
          Founded := True;
          TranslateMemo.SetFocus;
          TranslateMemo.SelStart := Index;
          TranslateMemo.SelLength := Length(SearchString);
          if soPromt in Options then
          with LocCreateMessageDialog(WideFormat(K7S(SMUReplaceConfirmation),
                 [SearchString]), mtConfirmation,
                 [mbYes, mbNo, mbCancel, mbYesToAll]) do
          try
           HelpContext := 0;
           HelpFile := '';
           Left := ListBox.ClientOrigin.X;
           Top := (ListBox.ClientHeight + ListBox.ClientOrigin.Y) - Height;
           Case ShowModal of
            mrYes: TranslateMemo.SelText := ReplaceString;
            mrCancel: Exit;
            mrYesToAll:
            begin
             ReplaceAll := True;
             Exclude(Options, soPromt);
             TranslateMemo.SelText := ReplaceString;
            end;
           end;
          finally
           Free;
          end else TranslateMemo.SelText := ReplaceString;
          if not ReplaceAll then Exit;
         end else
         begin
          Start := True;
          Inc(StringIndex);
          PS := Next;
         end;
        end else
        begin
         Inc(StringIndex);
         PS := Next;
        end;
       end else
       begin
        Index := FindIn(SearchString, stNew, 0);
        if Index >= 0 then
        begin
         Founded := True;
         with GroupsTabControl do if TabIndex <> GroupIndex then
         begin
          TabIndex := GroupIndex;
          GroupsTabControlChange(Sender);
         end;
         with PTablesTabControl do if TabIndex <> PtrTableIndex then
         begin
          TabIndex := PtrTableIndex;
          PTablesTabControlChange(Sender);
         end;
         ListBox.ClearSelection;
         ListBox.ItemIndex := StringIndex;
         ListBox.Selected[StringIndex] := True;
         ListBoxClick(Sender);
         TranslateMemo.SetFocus;
         TranslateMemo.SelStart := Index;
         TranslateMemo.SelLength := Length(SearchString);
         if soPromt in Options then
         with LocCreateMessageDialog(WideFormat(K7S(SMUReplaceConfirmation),
                [SearchString]), mtConfirmation,
                [mbYes, mbNo, mbCancel, mbYesToAll]) do
         try
          HelpContext := 0;
          HelpFile := '';
          Left := ListBox.ClientOrigin.X;
          Top := (ListBox.ClientHeight + ListBox.ClientOrigin.Y) - Height;
          Case ShowModal of
           mrYes: TranslateMemo.SelText := ReplaceString;
           mrCancel: Exit;
           mrYesToAll:
           begin
            ReplaceAll := True;
            Exclude(Options, soPromt);
            TranslateMemo.SelText := ReplaceString;
           end;
          end;
         finally
          Free;
         end else TranslateMemo.SelText := ReplaceString;
         if not ReplaceAll then Exit;
         Start := False;
        end else
        begin
         Start := True;
         Inc(StringIndex);
         PS := Next;
        end;
       end;
      end;
      PP := Next;
     end;
     Inc(PtrTableIndex);
     P := Next;
    end;
    Inc(GroupIndex);
    G := Next;
   end;
   if not ReplaceAll or not Founded then
    LocMessageDlg(K7S(SMUTextStringNotFound), mtInformation, [mbOk], 0);
  end;
 end;
end;

procedure TMainForm.MenuSearchFindReplaceActionExecute(Sender: TObject);
begin
 with SearchTextRec do
 begin
  if ReplaceTextDialog.Execute then
  begin
   CanSearch := True;
   Replace := True;
   ReplaceAll := ReplaceTextDialog.ModalResult = mrYesToAll;
   SearchString := ReplaceTextDialog.SearchString;
   ReplaceString := ReplaceTextDialog.ReplaceString;
   Options := [];
   if ReplaceTextDialog.CaseCheckBox.Checked then
    Include(Options, soMatchCase);
   if ReplaceTextDialog.PromtCheckBox.Checked then
   Include(Options, soPromt);
  end else Exit;
  MenuSearchFindNextActionExecute(Sender);
 end;
end;

procedure TMainForm.MenuHelpPluginsItemClick(Sender: TObject);
begin
 if DLLs = NIL then Exit;
 with LibraryDialog do
 begin
  ListBox.Clear;
  DLLs.GetByMethod(ListBox.Items);
  ShowModal;
 end;
end;

procedure TMainForm.ListBoxDblClick(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 if Assigned(SelectedRoot) and Assigned(CurGroup) and Assigned(CurPointer) then
 with TreeView do
 begin
  Node := SelectedRoot.GetLastChild;
  while Node <> NIL do
  begin
   if Node.Data = CurGroup then
   begin
    Node := Node.GetLastChild;
    with CurGroup^ do if Assigned(grCurPtrTable) then while Node <> NIL do
    begin
     if Node.Data = grCurPtrTable then
     begin
      Node := Node.GetFirstChild;
      while Node <> NIL do
      begin
       if Node.Data = CurPointer then
       begin
        TreeView.Selected := Node;
        TreeViewChange(Sender, Node);
        Exit;
       end;
       Node := Node.GetNext;
      end;
      Break;
     end;
     Node := Node.GetNext;
    end;
    Break;
   end;
   Node := Node.GetNext;
  end;
 end;
end;

function GroupAddToRom(GRP: Pointer; Project: PKruptarProject): Boolean;

type
 PTextStringInf = ^TTextStringInf;
 TTextStringInf = record
  Node: PTextString;
  Copy: PTextStringInf;
  PtrTbl: PPointerTable;
  Ptr: PPointer;
  SLen: LongInt;
  Index: LongInt;
  CopyOfs: LongInt;
  MinSkip: Boolean;
 end;
 PTempBlock = ^TTempBlock;
 TTempBlock = packed record
  tbStart: LongInt;
  tbCount: LongInt;
 end;

var
 NList: array of TTextStringInf;
 BlocksLst: array of TTempBlock;
 BlockIndex, MinSize, MinSizeIndex: LongInt;

 procedure BlockSort(L, R: LongInt);
 var
  I, J: LongInt;
  X: Int64;
  Y: TTempBlock;
 begin
  if R <= L then Exit;
  I := L;
  J := R;
  X := Int64(BlocksLst[(L + R) shr 1]);
  repeat
   while Int64(BlocksLst[I]) < X do Inc(I);
   while X < Int64(BlocksLst[J]) do Dec(J);
   if I <= J then
   begin
    if Int64(BlocksLst[I]) <> Int64(BlocksLst[J]) then
    begin
     Y := BlocksLst[I];
     BlocksLst[I] := BlocksLst[J];
     BlocksLst[J] := Y;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then BlockSort(L, J);
  if I < R then BlockSort(I, R);
 end;

 procedure NListSort(L, R: LongInt);
 var
  I, J: LongInt;
  X: LongInt;
  Y: TTextStringInf;
 begin
  if R <= L then Exit;
  I := L;
  J := R;
  X := NList[(L + R) shr 1].SLen;
  repeat
   while NList[I].SLen > X do Inc(I);
   while X > NList[J].Slen do Dec(J);
   if I <= J then
   begin
    if NList[I].SLen <> NList[J].SLen then
    begin
     Y := NList[I];
     NList[I] := NList[J];
     NList[J] := Y;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then NListSort(L, J);
  if I < R then NListSort(I, R);
 end; (* NListSort *)

 var
  Ptrs, BCounts: PDWords;
  TempBlocks: PBlocks;
  PCnt, NS: LongInt;
  Block, B2: PBlock;

 procedure ExcludeBlock(Pos, Len: LongInt);
 var
  N, N2, N3: PBlock;
  X: LongInt;
 begin
  N := TempBlocks.Root;
  while N <> nil do with N^ do
  begin
   if (Pos >= blStart) and (Pos < blStart + blCount) then
   begin
    X := blCount;
    blCount := Pos - blStart;
    N2 := N;
    N := Next;
    if Pos + Len <= blStart + X then
    with TempBlocks^ do
    begin
     New(N3);
     N2.Next := N3;
     N3.Next := N;
     if Cur = N2 then Cur := N3;
     Inc(Count);
     N3.blStart := Pos + Len;
     N3.blCount := (blStart + X) - N3.blStart;
    end;
    if blCount = 0 then
    begin
     if B2 = N2 then B2 := N;
     TempBlocks.Remove(N2);
    end;
   end else N := Next;
  end;
 end;

 function FindBlank(Idx: LongInt): LongInt;
 var
  I, Z, Mul, Maximum, MaxIdx: LongInt;
 begin
  with NList[Idx] do
  begin
   Mul := PtrTbl.ptDestAlignment;
   Maximum := 0;
   MaxIdx := -1;
   for I := BlockIndex to Length(BlocksLst) - 1 do
   with BlocksLst[I] do
   begin
    Result := Aligned(tbStart, Mul);
    if Result + SLen <= tbStart + tbCount then
    begin
     Z := tbCount - (SLen + (Result - tbStart));
     if (Z > 0) and (MinSizeIndex > Idx) then
     begin
      if Z < NList[MinSizeIndex].SLen then
      begin
       if Maximum < Z then
       begin
        Maximum := Z;
        MaxIdx := I;
       end;
       Continue;
      end else
      repeat
       Dec(MinSizeIndex);
       if MinSizeIndex <= Idx then Break;
       with NList[MinSizeIndex] do
        if not MinSkip then
         with PtrTbl^ do
          if (ptDestPointerSize > 0) and ptSeekSame then Break;
      until False;
     end;
     tbCount := Z;
     tbStart := Result + SLen;
     Z := Length(BlocksLst) - 1;
     if BlockIndex < Z then
     begin
      BlockSort(BlockIndex, Z);
      while BlocksLst[BlockIndex].tbCount < MinSize do Inc(BlockIndex);
     end;
     Exit;
    end;
   end;
   if MaxIdx >= 0 then
   begin
    with BlocksLst[MaxIdx] do
    begin
     Result := Aligned(tbStart, Mul);
     tbCount := Maximum;
     tbStart := Result + SLen;
    end;
    BlockSort(BlockIndex, Length(BlocksLst) - 1);
    while BlocksLst[BlockIndex].tbCount < MinSize do Inc(BlockIndex);
   end else Result := -1;
  end;
 end;

 var
  Group: PGroup;
  G, P: PDynamic;
  PP: PPointer;
  TextStrings, TxtStrings, TempStrings: PTextStrings;
  DLL: PDLL;
  PT_SZ, XX, K, J, I, Len: Integer;
  S: RawByteString;
  TS: PTextString;
  LeftSize, WPLeftSize: Integer;
  WPBreak: Boolean;

 function GetStrLen(const Str: RawByteString; Tbl: PPointerTable): LongInt;
 var
  PT_SZ, XX: LongInt;
 begin
  with Tbl^ do
  begin
   XX := ptDestAlignment;
   Result := Aligned(Length(Str), XX);
   if ptPtrToPtr and not ptDestPunType then
   begin
    PT_SZ := ptDestPointerSize;
    if PT_SZ > 4 then
     Dec(PT_SZ, 4);
    Inc(Result, Aligned(PT_SZ, XX));
   end;
  end;
 end;

 procedure StrFinal(Offset, Len: LongInt; const Str: RawByteString; Tbl: PPointerTable; PP: PPointer);
 var
  XX: LongInt;
 begin
  with Tbl^ do
  begin
   with PP^ do
   begin
    pnPtr := Offset;
    if Offset >= 0 then
    begin
     if ptDestPunType then
     SetPointerPun(Project.ROM,ptInterval,pnInsertPos,pnPtr,pnInsertRef) else
      SetPointer(Project.ROM, pnInsertPos, pnInsertPos, pnPtr, pnInsertRef);
    end;
   end;
   if Offset < 0 then
   begin
    Result := False;
    if ptSeekSame then
    begin
     XX := FindTextString(TempStrings.Root, Str);
     if XX = -1 then
     begin
      TempStrings.Add.Str := Str;
      Inc(LeftSize, Len);
     end;
    end else
    begin
     TempStrings.Add.Str := Str;
     Inc(LeftSize, Len);
    end;
   end else
   begin
    if ptPtrToPtr and not ptDestPunType then
    begin
     PT_SZ := ptDestPointerSize;
     if PT_SZ > 4 then
      Dec(PT_SZ, 4);
     PT_SZ := Aligned(PT_SZ, ptDestAlignment);
     SetPointer(Project.ROM, Offset, Offset, Offset + PT_SZ, PP.pnInsertRef);
     Inc(Offset, PT_SZ);
    end;
    Move(Pointer(Str)^, Project.ROM[Offset], Length(Str));
   end;
  end;
 end;

 procedure StringAdd(Idx: LongInt);
 var
  K: LongInt;
 begin
  with NList[Idx], PtrTbl^ do
  begin
   if LongInt(Ptrs^[Index]) < 0 then
   begin
    K := FindBlank(Idx);
    LongInt(Ptrs^[Index]) := K;
    LongInt(BCounts^[Index]) := SLen;
   end else
    K := LongInt(Ptrs^[Index]);
   StrFinal(K, SLen, Node.Str, PtrTbl, Ptr);
  end;
 end; (* end of StringAdd *)

 procedure StringAdd2(const Str: RawByteString; I: LongInt; Tbl: PPointerTable; PP: PPointer);
 var
  Len, K: LongInt;
  Block: PBlock;
 begin
  with Tbl^ do
  begin
   Len := GetStrLen(Str, Tbl);
   if LongInt(Ptrs^[I]) < 0 then
   begin
    Block := TempBlocks.Root;
    K := -1;
    while Block <> nil do with Block^ do
    begin
     if Len <= blCount then
     begin
      K := Aligned(blStart, ptDestAlignment);
      ExcludeBlock(K, Len);
      Break;
     end;
     Block := Next;
    end;
    LongInt(Ptrs^[I]) := K;
    LongInt(BCounts^[I]) := Len;
   end else
    K := LongInt(Ptrs^[I]);
   StrFinal(K, Len, Str, Tbl, PP);
  end;
 end; (* end of StringAdd2 *)


(* function GroupAddToRom *)
begin
 Group := GRP;
 LeftSize := 0;
 WPLeftSize := 0;
 WPBreak := False;
 Result := True;
 with Project^, Group^, grPointerTables^ do
 begin
  New(TempBlocks, Init);
  try
   New(TempStrings, Init);
   try
    Block := grBlocks.Root;
    B2 := Block;
    with TempBlocks^ do while Block <> NIL do with Add^ do
    begin
     blStart := Block.blStart;
     blCount := Block.blCount;
     Block := Block.Next;
    end;
    P := Root; PCnt := 0;
    while P <> NIL do with P^, PPointerTable(Data)^ do
    begin
     Inc(PCnt, ptPointers.Count);
     P := Next;
    end;
    if PCnt > 0 then
    begin
     GetMem(Ptrs, PCnt * 4);
     try
      FillChar(Ptrs^, Pcnt * 4, $FF);
      GetMem(BCounts, PCnt * 4);
      try
       FillChar(BCounts^, Pcnt * 4, 0);
       New(TxtStrings, Init);
       try
        DLL := DLLs.Get(grDLLName);
        if DLL <> NIL then
        begin
         J := 0;
         P := Root;
         while P <> NIL do with P^, PPointerTable(Data)^, ptPointers^ do
         begin
          DLL.SetVariables(ROM, ROMSize, nil, ptDestStringLength, ptDestCharSize);
          PP := Root;
          while PP <> NIL do with PP^ do
          begin
           if pnFixed then
           begin
            TextStrings := StringsToTextStrings(pnTable2, pnStrings,
                                                ptDestCharSize, Project);
            if TextStrings <> NIL then
            begin
             S := DLL.GetData(TextStrings);
             Dispose(TextStrings, Done);
            end else with pnTable2^ do if EndRoot <> NIL then
             S := EndRoot.tiCodes else
             S := #0;
            Ptrs[J] := pnPtr;
            BCounts[J] := Length(S);
            ExcludeBlock(pnPtr, Length(S)) /// Write ME!!!!!
           end;
           PT_SZ := ptDestPointerSize;
           if not ptDestPunType and
              (PT_SZ > 0) and
              (ptParameters <> NIL) and
              (pnParamData <> '') then with ptParameters^ do if Count > 0 then
           begin
            if PT_SZ > 4 then Dec(PT_SZ, 4);
            Move(Pointer(pnParamData)^, ROM[pnInsertPos + PT_SZ], ptInterval - Before);
            Move(pnParamData[1 + (ptInterval - Before)],
                 ROM[pnInsertPos - Before], Before);
           end;
           Inc(J);
           PP := Next;
          end;
          P := Next;
         end;
         GroupProgressInitProc(grName, Count * 2 + PCnt * 2);
         P := Root;
         while P <> NIL do with P^, PPointerTable(Data)^, ptPointers^ do
         begin
          DLL.SetVariables(ROM, ROMSize, nil, ptDestStringLength, ptDestCharSize);
          PtrTableProgressInitProc(ptName, Count);
          PP := Root;
          while PP <> NIL do with PP^, TxtStrings^.Add^ do
          begin
           TextStrings := StringsToTextStrings(pnTable2, pnStrings,
                          ptDestCharSize, Project);
           if TextStrings <> NIL then
           begin
            Str := DLL.GetData(TextStrings);
            Dispose(TextStrings, Done);
           end else with pnTable2^ do if EndRoot <> NIL then
            Str := EndRoot.tiCodes else
            Str := #0;

           IncPtrTableProgressProc(1);
           PP := PP.Next;
          end;
          IncGroupProgressProc(1);
          P := Next;
         end;
         P := Root;
         J := 0;
         TS := TxtStrings.Root;
         while P <> NIL do with P^, PPointerTable(Data)^, ptPointers^ do
         begin
          PtrTableProgressInitProc(ptName, Count);
          PP := ptPointers.Root;
          K := 0;
          if PP <> NIL then
          begin
           if B2 <> NIL then
            K := Aligned(B2.blStart, ptDestAlignment); { else
            K := PP.pnInsertPos;
           XX := ptDestAlignment;
           while K mod XX <> 0 do Inc(K); }
          end;
          while PP <> NIL do
          begin
           if TS <> NIL then with TS^ do
           begin
            if ptDestPointerSize > 0 then
            begin
             if not ptSeekSame then
              StringAdd2(Str, J, PPointerTable(Data), PP);
            end else if ptDestStringLength > 0 then with PP^ do
            begin
             Ptrs[J] := pnPtr;
             BCounts[J] := Length(Str);
             Move(Pointer(Str)^, ROM[pnPtr], Length(Str));
            end else with PP^ do
            begin
             if (B2 <> nil) and not pnFixed then
              pnPtr := K;
             XX := Aligned(Length(Str), ptDestAlignment);
             if B2 <> NIL then
             begin
              with B2^ do if (K + XX > blStart + blCount) and not pnFixed then
              begin
               pnPtr := -1;
               Ptrs[J] := $FFFFFFFF;
               Inc(K, XX);
               WPBreak := True;
               Inc(WPLeftSize, XX);
              end else
              begin
               Ptrs[J] := pnPtr;
               ExcludeBlock(pnPtr, XX);
               B2 := TempBlocks.Root;
               BCounts[J] := XX;
               Inc(K, XX);
               Move(Pointer(Str)^, ROM[pnPtr], Length(Str));
              end;
             end else
             begin
              Ptrs[J] := pnPtr;
              BCounts[J] := XX;
              Move(Pointer(Str)^, ROM[pnPtr], Length(Str));
             end;
            end;
           end;
           Inc(J);
           IncPtrTableProgressProc(1);
           PP := PP.Next;
           TS := TS.Next;
          end;
          if (ptDestPointerSize = 0) and (B2 <> NIL) then B2 := B2.Next;
          IncGroupProgressProc(1);
          P := Next;
         end;
         Setlength(BlocksLst, TempBlocks.Count);
         Block := TempBlocks.Root;
         for I := 0 to TempBlocks.Count - 1 do with BlocksLst[I], Block^ do
         begin
          tbStart := blStart;
          tbCount := blCount;
          Block := Next;
         end;
         BlockIndex := 0;
         BlockSort(0, TempBlocks.Count - 1);
         SetLength(NList, TxtStrings.Count);
         TS := TxtStrings.Root;
         J := 0;
         P := Root;
         while P <> NIL do with P^, PPointerTable(Data)^, ptPointers^ do
         begin
          PP := Root;
          while PP <> NIL do
          begin
           with NList[J] do
           begin
            PtrTbl := PPointerTable(Data);
            Ptr := PP;
            Node := TS;
            SLen := GetStrLen(TS.Str, Data);
            Index := J;
           end;
           TS := TS.Next;
           Inc(J);
           PP := PP.Next;
          end;
          P := Next;
         end;
         NListSort(0, TxtStrings.Count - 1);
         PtrTableProgressInitProc('', TxtStrings.Count);
         for I := 0 to TxtStrings.Count - 1 do with NList[I] do
         begin
          for J := 0 to I - 1 do
          begin
           K := Pos(Node.Str, NList[J].Node.Str);
           if K > 0 then
           begin
            Copy := Addr(NList[J]);
            CopyOfs := K - 1;
            Break;
           end;
          end;
          IncPtrTableProgressProc(1);
          IncGroupProgressProc(1);
         end;
       {  J := 0;
         K := -1;
         for I := 0 to Length(BlocksLst) - 1 do with BlocksLst[I] do
         if tbCount = K then Inc(J) else
         begin
          BlockPosSort(I - J, I - 1);
          K := tbCount;
          J := 1;
         end;
         K := Length(BlocksLst);
         if J > 1 then BlockPosSort(K - J, K - 1);}
         J := Length(NList) - 1;
         for I := 0 to Length(BlocksLst) - 1 do with BlocksLst[I] do
         begin
          repeat
           with NList[J] do
           begin
            Dec(J);
            with PtrTbl^ do if (ptDestPointerSize > 0) and ptSeekSame and
            (tbCount - (Aligned(tbStart, ptDestAlignment) - tbStart) = SLen) then
            begin
             MinSkip := True;
             Break;
            end;
           end;
          until J < 0;
          if J < 0 then Break;
         end;
         MinSizeIndex := 0;
         for I := Length(NList) - 1 downto 0 do
          with NList[I] do
           if not MinSkip then
            with PtrTbl^ do
             if (ptDestPointerSize > 0) and ptSeekSame then
             begin
              MinSizeIndex := I;
              Break;
             end;
         MinSize := NList[MinSizeIndex].SLen;
         PtrTableProgressInitProc('', TxtStrings.Count);
         for J := 0 to TxtStrings.Count - 1 do with NList[J], PtrTbl^ do
         begin
          if Assigned(MyProgressDialog) then
           MyProgressDialog.PtrTableLabel.Caption := ptName;
          if (ptDestPointerSize > 0) and ptSeekSame then
          begin
           if Copy <> nil then
            K := Integer(Ptrs[Copy.Index]) + CopyOfs else
            K := 0;
           if (Integer(Ptrs[Index]) < 0) and ((Copy <> nil) and
              ((not (ptPtrToPtr and not ptDestPunType)) or (CopyOfs = 0))) and
              (K mod ptDestAlignment = 0) then
           begin
            Integer(Ptrs[Index]) := K;
            BCounts[Index] := Length(Node.Str);
            with Ptr^ do
            begin
             pnPtr := Integer(Ptrs[Index]);
             if ptDestPunType then
              SetPointerPun(ROM, ptInterval, pnInsertPos, pnPtr, pnInsertRef) else
              SetPointer(ROM, pnInsertPos, pnInsertPos, pnPtr, pnInsertRef);
            end;
           end else StringAdd(J);
          end;
          IncPtrTableProgressProc(1);
          IncGroupProgressProc(1);
         end;
        end;
       finally
        Dispose(TxtStrings, Done);
       end;
      finally
       FreeMem(BCounts);
      end;
     finally
      FreeMem(Ptrs);
     end;
    end;
    if not Result then
    begin
{     Block := TempBlocks.Root;
     XX := 0;
     while Block <> NIL do with Block^ do
     begin
      if blCount > XX then XX := blCount;
      Block := Next;
     end;
     Dec(LeftSize, XX);}
     ProgressInsertErrorProc(grName, LeftSize);
    end;
    if WPBreak then
    begin
     ProgressInsertErrorProc(grName, WPLeftSize);
     Result := False;
    end;
   finally
    Dispose(TempStrings, Done);
   end;
  finally
   Dispose(TempBlocks, Done);
  end;
 end;
end;

procedure TMainForm.MenuHelpAboutActionExecute(Sender: TObject);
begin
 AboutDialog;
end;

procedure TMainForm.MenuProjectEmulatorSelectActionExecute(Sender: TObject);
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with SelectedRoot, PKruptarProject(Data)^ do
 begin
  OpenDialog.Filter := K7S(SMUExeFilter);
  OpenDialog.FileName := EmulSelected;
  if OpenDialog.Execute then
  begin
   EmulSelected := OpenDialog.FileName;
   SetSaved(SelectedRoot.Data, False);
  end;
 end;
end;

var
 P_OPEN: array[0..4] of WideChar = ('O', 'p', 'e', 'n', #0);

procedure TMainForm.MenuProjectRunRom1ActionExecute(Sender: TObject);
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with SelectedRoot, PKruptarProject(Data)^ do
 begin
  if (EmulSelected = '') or not WideFileExists(EmulSelected) then
   MenuProjectEmulatorSelectActionExecute(Sender);
  ShellExecuteW(Application.Handle,
  @P_OPEN, PWideChar(EmulSelected),
  PWideChar(WideFormat('"%s"', [InputRom])),
  PWideChar(WideExtractFilePath(EmulSelected)),
  SW_SHOWNORMAL);
 end;
end;

procedure TMainForm.MenuProjectRunRom2ActionExecute(Sender: TObject);
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with SelectedRoot, PKruptarProject(Data)^ do
 begin
  if (EmulSelected = '') or not WideFileExists(EmulSelected) then
   MenuProjectEmulatorSelectActionExecute(Sender);
  ShellExecuteW(Application.Handle,
                                   @P_OPEN, PWideChar(EmulSelected),
                                   PWideChar(WideFormat('"%s"', [OutputRom])),
                                   PWideChar(WideExtractFilePath(EmulSelected)),
                                   SW_SHOWNORMAL);
 end;
end;

procedure TMainForm.LoadLanguages(const CurrentLanguage: WideString);
var
 SR: TSearchRecW;
 Action, CheckAction: TKruptarActionType;
 MenuItem: TKruptarMenuItem;
 I: Integer;
begin
 with Localizer, LanguageList do
 begin
  if WideFindFirst(ExeDir + 'Lang' + PathDelim + '*.lng', $21, SR) = 0 then
  try
   repeat
    AddLanguageFile(ExeDir + 'Lang' + PathDelim + SR.Name);
   until WideFindNext(SR) <> 0;
  finally
   WideFindClose(SR);
  end;
  if Count = 0 then MenuLanguageAction.Enabled := False;
  CheckAction := NIL;
  for I := 0 to Count - 1 do with Languages[I] do
  begin
   Action := TKruptarActionType.Create(Self);
   Action.GroupIndex := 1;
   Action.Caption := LanguageName;
   Action.ActionList := ActionList;
   Action.Tag := I;
   Action.OnExecute := LangActionClick;
   MenuItem := TKruptarMenuItem.Create(Self);
   MenuItem.Action := Action;
   LanguageMenu.Add(MenuItem);
   if (I = 0) or (LanguageName = CurrentLanguage) then
    CheckAction := Action;
  end;
  if CheckAction <> NIL then
   LangActionClick(CheckAction);
 end;
end;

procedure InitDynamicStrings;
begin
 ErrorStr[etHint] := K7S(SESHint);
 ErrorStr[etWarning] := K7S(SESWarning);
 ErrorStr[etError] := K7S(SESError);
 ErrorStr[etFatalError] := K7S(SESFatalError);
 SSBInsertV := K7S(SSBInsert);
 SSBOverwriteV := K7S(SSBOverwrite);
 SMUSizeV := K7S(SMUSize);
 SLBLineNumberV := K7S(SLBLineNumber);
end;

procedure TMainForm.LangActionClick(Sender: TObject);
var
 N, N1, N2, N3: TKruptarTreeNodeType;
begin
 with Sender as TKruptarActionType do if not Checked then
 begin
  Checked := True;
  Localizer.LanguageIndex := Tag;
  FLang := Caption;
  InitDynamicStrings;
  N := TreeView.Items.GetFirstNode;
  while N <> NIL do
  begin
   N1 := N.getFirstChild;
   if N1.StateIndex = Integer(tsTables) then
   begin
    N1.Text := K7S(STVTables);
    N2 := N1.getFirstChild;
    while N2 <> NIL do
    begin
     N3 := N2.getFirstChild;
     if N3.StateIndex = Integer(tsEnds) then
      N3.Text := K7S(STVTerminateCodes);
     N3 := N3.getNextSibling;
     if N3.StateIndex = Integer(tsEls) then
      N3.Text := K7S(STVCRLFCodes);
     N3 := N3.getNextSibling;
     if N3.StateIndex = Integer(tsCodes) then
      N3.Text := K7S(STVCharacters);
     N2 := N2.getNextSibling;
    end;
   end;
   N1 := N1.getNextSibling;
   if N1.StateIndex = Integer(tsGroups) then
   begin
    N1.Text := K7S(STVGroups);
    N2 := N1.getFirstChild;
    while N2 <> NIL do
    begin
     N3 := N2.getFirstChild;
     if N3.StateIndex = Integer(tsBlocks) then
      N3.Text := K7S(STVSpaceForText);
     N3 := N3.getNextSibling;
     if N3.StateIndex = Integer(tsPtrTables) then
      N3.Text := K7S(STVPointers);
     N2 := N2.getNextSibling;
    end;
   end;
   N := N.getNextSibling;
  end;
 end;
end;

procedure TMainForm.MenuItemClick(Sender: TObject);
begin
 //do nothing
end;

procedure TMainForm.FormShow(Sender: TObject);
var
 Strings: UnicodeUtils.TWideStrings;
 I: Integer;
begin
 LoadLanguages(FLang);
 if ParamCount > 0 then
 begin
  Strings := TWideStringList.Create;
  try
   for I := 1 to ParamCount do if FileExists(ParamStr(I)) then
    Strings.Add(ParamStr(I));
   if Strings.Count > 0 then OpenFiles(Strings);
  finally
   Strings.Free;
  end;
 end;
end;

procedure TMainForm.MenuEditUndoActionUpdate(Sender: TObject);
var
 H: HWND;
begin
 H := GetEditHandle(True);
 TAction(Sender).Enabled := (H <> 0) and
 (SendMessage(H, EM_CANUNDO, 0, 0) <> 0);
end;

procedure TMainForm.MenuEditSelectAllActionUpdate(Sender: TObject);
var
 H: HWND;
begin
 H := GetEditHandle;
 TAction(Sender).Enabled := ((H <> 0) and
 (SendMessage(H, WM_GETTEXTLENGTH, 0, 0) > 0)) or
 (ListBox.Focused and (ListBox.Count > 0));
end;

procedure TMainForm.MenuActionsUpdate1(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL);
end;

procedure TMainForm.MenuFileCloseAllActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := Projects.Count > 0;
end;

procedure TMainForm.MenuDictActionsUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedGroup <> NIL) and SelectedGroup.grIsDictionary;
end;

procedure TMainForm.MenuProjectGroupDictToTableActionUpdate(
  Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedGroup <> NIL) and SelectedGroup.grIsDictionary and
  (SelectedGroup.grDictTable <> NIL);
end;

procedure TMainForm.MenuProjectItemAddActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled := TreeView.Focused and (Node <> NIL) and
  (TTreeStateType(Node.StateIndex) in [tsEnds, tsEls, tsCodes,
   tsTableItem, tsBlocks, tsBlock, tsPtrTables, tsPtrTable]);
end;

procedure TMainForm.MenuProjectItemUpActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled := TreeView.Focused and (Node <> NIL) and
  (Node.GetPrevSibling <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsTableItem, tsBlock, tsGroup, tsPtrTable, tsPointer, tsTable, tsProject]);
end;

procedure TMainForm.MenuProjectItemDownActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled := TreeView.Focused and (Node <> NIL) and
  (Node.GetNextSibling <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsTableItem, tsBlock, tsGroup, tsPtrTable, tsPointer, tsTable, tsProject]);
end;

procedure TMainForm.MenuProjectItemRemoveActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled := TreeView.Focused and (Node <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsTableItem, tsBlock, tsGroup, tsPtrTable, tsPointer, tsTable]);
end;

procedure TMainForm.MenuEditLoadActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
 Result: Boolean;
begin
 Node := TreeView.Selected;
 Result := MenuProjectItemsAddFromFileAction.Enabled or
  (* Enable if TranslateMemo is active and not Read Only *)
 (TranslateMemo.Focused and not TranslateMemo.ReadOnly) or
 (* Enable if ListBox is active and not empty *)
 (ListBox.Focused and (ListBox.SelCount > 0)) or
 (* Enable if GroupsTabControl is active and not empty *)
 (GroupsTabControl.Focused and (GroupsTabControl.Tabs.Count > 0)) or
 (* Enable if PTablesTabControl is active and not empty *)
 (PTablesTabControl.Focused and (PTablesTabControl.Tabs.Count > 0));
 if TreeView.Focused then
 begin
  if (Node <> nil) and (Node.StateIndex = Integer(tsTables)) then
   Result := False else
   Result := Result or ((Node <> nil) and (Node.StateIndex = Integer(tsTable)));
 end;
 TAction(Sender).Enabled := Result;
end;

procedure TMainForm.MenuProjectTableAnsiFillActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled := TreeView.Focused and (Node <> NIL) and
  (TTreeStateType(Node.StateIndex) in
  [tsCodes, tsTableItem]);
end;

function CheckGroupContents(GRP: PGroup): Boolean;
var
 P: PDynamic;
begin
 if GRP <> NIL then
 begin
  P := GRP.grPointerTables.Root;
  while P <> NIL do with P^, PPointerTable(Data)^ do
  begin
   if ptPointers.Count > 0 then
   begin
    Result := True;
    Exit;
   end;
   P := Next;
  end;
 end;
 Result := False;
end;

procedure TMainForm.MenuProjectGroupTextReloadActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
 (ListBox.Focused and (ListBox.Count > 0)) or
 // Enable if TreeView is active and pointer list or
 // group is selected and not empty
 (TreeView.Focused and (((SelectedList <> nil) and
 (SelectedList.ptPointers.Count > 0)) or CheckGroupContents(SelectedGroup))) or
 (* Enable is GroupsTabControl is active and not empty *)
 (GroupsTabControl.Focused and CheckGroupContents(CurGroup)) or
 (* Enable is PTablesTabControl is active and not empty *)
 (PTablesTabControl.Focused and (CurGroup <> nil) and
  (CurGroup.grCurPtrTable.ptPointers.Count > 0));
end;

procedure TMainForm.MenuProjectGroupTextInsertActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and (CurGroup <> NIL) and
  (PKruptarProject(SelectedRoot.Data).OutputROM <> '');
end;

procedure TMainForm.MenuProjectRunRom1ActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and
  (PKruptarProject(SelectedRoot.Data).InputROM <> '');
end;

procedure TMainForm.MenuProjectRunRom2ActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and
  (PKruptarProject(SelectedRoot.Data).OutputROM <> '');
end;

procedure TMainForm.MenuProjectPointersAddActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (SelectedList <> NIL);{ and not
 ((SelectedList.ptPointers.Count > 0) and
  (SelectedList.ptPointerSize < 1) and
  (SelectedList.ptStringLength < 1));              }
end;

procedure TMainForm.MenuEditClearActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled :=
  (* Disable if original memo focused *)
  not OriginalMemo.Focused and
  (* Enable if saving enabled *)
  (MenuEditSaveAction.Enabled or
  (* Enable if Groups, or Tables, or PtrTables folder in Treeview is selected *)
  (TreeView.Focused and (Node <> NIL) and (TTreeStateType(Node.StateIndex) in
  [tsGroup, tsGroups, tsTables, tsPtrTables]) and
  (Node.getFirstChild <> nil)) or
  (* Enable if other edits are active *)
  (GetEditHandle(True) <> 0));
end;

procedure TMainForm.MenuProjectPointersTableLinksResetActionUpdate(
  Sender: TObject);
begin
 TAction(Sender).Enabled :=
 (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and
 (PKruptarProject(SelectedRoot.Data).Tables.Count > 0) and
 (SelectedList <> nil) and (SelectedList.ptPointers.Count > 0);
end;

procedure TMainForm.MenuProjectPointersVarOptionsActionUpdate(Sender: TObject);
//var
// Node: TKruptarTreeNodeType;
begin
// Node := TreeView.Selected;
 TAction(Sender).Enabled := SelectedList <> nil;
// (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and
// (Node <> NIL) and (TTreeStateType(Node.StateIndex) in [tsPtrTable, tsPointer]);
end;

procedure TMainForm.MenuProjectTextInsertActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL) and
  (PKruptarProject(SelectedRoot.Data).Groups.Count > 0) and
  (PKruptarProject(SelectedRoot.Data).OutputROM <> '');
end;

procedure TMainForm.MenuSearchActionsUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (CurGroup <> NIL) and
  (SelectedRoot <> NIL) and (SelectedRoot.Data <> NIL);
end;

procedure TMainForm.MenuOtherClearMessageListActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := StatesMemo.Visible;
end;

procedure TMainForm.GroupsTabControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 I: Integer;
begin
 with GroupsTabControl do
 begin
  SetFocus;
  if Button = mbRight then
  begin
   I := IndexOfTabAt(X, Y);
   if I >= 0 then
   begin
    if FSaveTab1Index <> I then
    begin
     TabIndex := I;
     GroupsTabControlChange(Sender);
    end;
    TabsPopup.Popup(ClientOrigin.X + X + 4, ClientOrigin.Y + Y + 4);
   end;
  end;
 end;
end;

procedure TMainForm.PTablesTabControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
 I: Integer;
begin
 with PTablesTabControl do
 begin
  SetFocus;
  if Button = mbRight then
  begin
   I := IndexOfTabAt(X, Y);
   if I >= 0 then
   begin
    if FSaveTab2Index <> I then
    begin
     TabIndex := I;
     PTablesTabControlChange(Sender);
    end;
    TabsPopup.Popup(ClientOrigin.X + X + 8, ClientOrigin.Y + Y + 8);
   end;
  end;
 end;
end;

procedure TMainForm.TabsSourceSaveToFileActionExecute(Sender: TObject);
var
 KP: PKruptarProject;
 List: TCodePagedStringList;
begin
 if CurGroup = NIL then Exit;
 if SelectedRoot = NIL then Exit;
 KP := SelectedRoot.Data;
 if KP = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 List := TCodePagedStringList.Create(KP.CodePage);
 try
  if GroupsTabControl.Focused then with GroupsTabControl do
  begin
   SaveTextDialog.FileName := CurGroup.grName + '.txt';
   if SaveTextDialog.Execute then
   begin
    CurGroup.SaveToStringList(List, KP.Flags, True);
    List.SaveToFile(SaveTextDialog.FileName);
   end;
  end else with PTablesTabControl do
  begin
   SaveTextDialog.FileName := FixedFileName(Tabs[TabIndex]) + '.txt';
   if SaveTextDialog.Execute then with CurGroup^ do
   begin
    if grCurPtrTable <> NIL then
     grCurPtrTable.SaveToStringList(List, KP.Flags, True);
    List.SaveToFile(SaveTextDialog.FileName);
   end;
  end;
 finally
  List.Free;
 end;
end;

procedure TMainForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
 Ini: TStreamIniFileW;
 FS: Integer; FntStyle: TFontStyles absolute FS;
 Section, FName: WideString;
begin
 try
  FName := WideChangeFileExt(ParamStr(0), '.cfg');
  Ini := TStreamIniFileW.Create(FName);
  try
   Section := 'Main';
   Ini.WriteString(Section, 'Language', FLang);
   with OriginalMemo.Font do
   begin
    Ini.WriteString(Section, 'FontName', Name);
    Ini.WriteInteger(Section, 'FontHeight', Height);
    Ini.WriteInteger(Section, 'FontSize', Size);
    Ini.WriteInteger(Section, 'FontColor', Color);
    Ini.WriteInteger(Section, 'FontPitch', Integer(Pitch));
    FS := 0;
    FntStyle := Style;
    Ini.WriteInteger(Section, 'FontStyle', FS);
   end;
   Ini.WriteBool(Section, 'InsertProgressAutoClose', ProgressAutoClose);
   if FileRecent1Item.Enabled then
    Ini.WriteString(Section, 'RecentFile1', GetRecentFileName(FileRecent1Item));
   if FileRecent2Item.Enabled then
    Ini.WriteString(Section, 'RecentFile2', GetRecentFileName(FileRecent2Item));
   if FileRecent3Item.Enabled then
    Ini.WriteString(Section, 'RecentFile3', GetRecentFileName(FileRecent3Item));
   if FileRecent4Item.Enabled then
    Ini.WriteString(Section, 'RecentFile4', GetRecentFileName(FileRecent4Item));
   Ini.SaveToFile(FName);
  finally
   Ini.Free;
  end;
 except
 end;
end;

function TPointerTables.GetIndex(Item: PPointerTable): Integer;
var
 P: PDynamic;
 I: Integer;
begin
 Result := -1;
 if Item = NIL then Exit;
 P := Root; I := 0;
 while P <> NIL do with P^ do
 begin
  if Item = Data then
  begin
   Result := I;
   Exit;
  end;
  Inc(I);
  P := P^.Next;
 end;
end;

function TPointerTables.Get(Index: Integer): PPointerTable;
var
 I: Integer;
 P: PDynamic;
begin
 Result := NIL;
 if (Index < 0) or (Index >= Count) then Exit;
 I := 0; P := Root;
 while P <> NIL do with P^ do
 begin
  if I = Index then
  begin
   Result := Data;
   Exit;
  end;
  Inc(I);
  P := Next;
 end;
end;

function TPointerTable.AnsiPtName: String;
begin
 Result := ptName;
{$IFNDEF UNICODE}
 if Result <> ptName then
  Result := UTF16toUTF8(ptName);
{$ENDIF}
end;

function TTable.AnsiTbName: String;
begin
 Result := tbName;
{$IFNDEF UNICODE}
 if Result <> tbName then
  Result := UTF16toUTF8(tbName);
{$ENDIF}
end;

function TGroup.AnsiGrName: String;
begin
 Result := grName;
{$IFNDEF UNICODE}
 if Result <> grName then
  Result := UTF16toUTF8(grName);
{$ENDIF}
end;

function FieldFileExists(const FieldName, FileName: String;
 ConsoleMode: Boolean): Boolean;
begin
 if FileName = '' then
 begin
  AddStateString(etError, '', 0,
   WideFormat(K7S(SMUFileNameFieldIsEmpty), [FieldName]), ConsoleMode);
  Result := False;
 end else
 if not FileExists(FileName) then
 begin
  AddStateString(etError, '', 0,
   WideFormat(K7S(SMUFileNotFound), [FileName]), ConsoleMode);
  Result := False;
 end else Result := True;
end;

function MsgHexToInt(const FileName: WideString; Index: Integer;
                     const Value: String): LongInt;
begin
 Result := HexVal(Value);
 if HexError then
  AddStateString(etError, FileName, Index,
   WideFormat(K7S(SMUInvalidHexadecimalValue), [Value]), True);
end;

procedure TPointerTable.ReloadFromStringList(List: UnicodeUtils.TWideStrings;
  var Index: Integer);
 var
  PP: PPointer;
  PS: PString;
(* TPointerTable.ReloadFromStringList *)
begin
 PP := ptPointers.Root;
 while PP <> NIL do with PP^ do
 begin
  PS := pnStrings.Root;
  while PS <> NIL do with PS^ do
  begin
   stNew := ParseStr(List, Index);
   PS := Next;
  end;
  PP := Next;
 end;
end;

procedure TPointerTable.SaveToStringList(List: UnicodeUtils.TWideStrings; Flags: Integer;
  FromSource: Boolean);
var
 PP: PPointer;
 PS: PString;
begin
 if List = NIL then Exit;
 PP := ptPointers.Root;
 while PP <> NIL do with PP^ do
 begin
  PS := pnStrings.Root;
  while PS <> NIL do with PS^ do
  begin
   if FromSource then
    List.Add(ConvertString(pnTable1, stOriginal,
             Flags and KP_CRLF_CONVERT <> 0)) else
    List.Add(stNew);
   List.Add(sStringEnd);
   List.Add('');
   PS := Next;
  end;
  PP := Next;
 end;
end;

procedure TGroup.ReloadFromStringList(List: UnicodeUtils.TWideStrings; var Index: Integer);
var
 PT: PDynamic;
begin
 if List = NIL then Exit;
 PT := grPointerTables.Root;
 while PT <> NIL do with PT^ do
 begin
  PPointerTable(Data).ReloadFromStringList(List, Index);
  PT := Next;
 end;
end;

procedure TGroup.SaveToStringList(List: UnicodeUtils.TWideStrings; Flags: Integer;
  FromSource: Boolean);
var
 PT: PDynamic;
begin
 if List = NIL then Exit;
 PT := grPointerTables.Root;
 while PT <> NIL do with PT^ do
 begin
  PPointerTable(Data).SaveToStringList(List, Flags, FromSource);
  PT := Next;
 end;
end;

function TKruptarProject.GetCodePage: LongWord;
begin
 Result := CodePageList[CodePageIndex].CodePage;
end;

procedure TKruptarProject.SetCodePage(Value: LongWord);
var
 I: Integer;
begin
 for I := 0 to Length(CodePageList) - 1 do
  if Value = CodePageList[I].CodePage then
   CodePageIndex := I;
end;

{ TKruptar6_StringList }

function TKruptar6_StringList.GetString(Index: Integer): RawByteString;
begin
{$IFDEF FORCED_UNICODE}
  Result := CodePageStringEncode(FCodePage, inherited Strings[Index]);
{$ELSE}
  Result := inherited Strings[Index];
{$ENDIF}
end;

procedure TKruptar6_StringList.LoadFromStream(Stream: TStream);
var
 SW: WideString;
begin
 BeginUpdate;
 try
   SW := ReadCodePagedString(Stream, CP_WideBOM or CP_MbcsBOM, FCodePage);
   SetTextStr({$IFNDEF FORCED_UNICODE}CodePageStringEncode(CP_UTF8, SW){$ELSE}SW{$ENDIF});
   FCodePage := CP_UTF8;
 finally
  EndUpdate;
 end;
end;

procedure TMainForm.AddPointersList(BlockStart, BlockEnd: Integer;
  Project, DLL_, PtrsList: Pointer;
  const ListFileName: WideString; ListIndex: Integer);
var
 L: Integer;
 B: PTextStrings;
 TS: PTextString;
 I: Integer absolute BlockStart;
 KP: PKruptarProject absolute Project;
 DLL: PDLL absolute DLL_;
 List: PPointerTable absolute PtrsList;
begin
 with KP^, List^, ptPointers^ do
 begin
  with ptTable1^ do
   DLL.SetVariables(ROM, ROMSize, EndRoot, EndMaxCodes, ptCharSize);
  while I <= BlockEnd do
  begin
   L := 1;  
   if GetByPos(I) = NIL then
   begin
    SelectedData := Add;
    with PPointer(SelectedData)^ do
    begin
     pnPos := BlockStart;
     pnInsertPos := BlockStart;
     if ptPointerSize > 0 then
     begin
      if not ptPunType then
      begin
       pnParamData := FillParamData(ROM, I);
       pnPtr := GetPointer(ROM^[I], I, BlockStart, pnReference);
      end else
       pnPtr := GetPointerPun(ROM, I, ptInterval, BlockStart, pnReference);
     end else pnPtr := pnPos;
     pnInsertRef := pnReference;
     pnTable1 := ptTable1;
     pnTable2 := ptTable2;
     New(pnStrings, Init);
     try
      with ptTable1^ do
      begin
       if ptNotInSource then
       begin
        New(B, Init);
        B.Add;
       end else
       begin
        if (ptPointerSize > 0) and ptPtrToPtr then
         B := DLL.GetStrings(GetPointer(ROM^[pnPtr], pnPtr, BlockStart,
                             pnReference), ptStringLength) else
         B := DLL.GetStrings(pnPtr, ptStringLength);
       end;
       if B <> NIL then
       try
        TS := B.Root;
        if (TS = NIL) and (ptStringLength <= 0) and (ptPointerSize <= 0) then
         Inc(I);
        while TS <> NIL do with pnStrings.Add^, TS^ do
        begin
         stOriginal := Str;
         if (ptPointerSize <= 0) and (ptStringLength <= 0) then
         begin
          if DLL.GetMethod = tmNone then
          begin
           L := Integer(DLL.NeedEnd);
           if L <= 0 then L := Length(Str);
          end else L := Length(Str);
         end;
         stNew := DataToString(Pointer(Str)^, Length(Str), ptCharSize,
                               Flags and KP_CRLF_CONVERT <> 0);
         TS := Next;
        end;
       finally
        if ptNotInSource then
         Dispose(B, Done) else
         DLL.DisposeStrings(B);
       end else pnStrings.Add;
      end;
     except
      Dispose(pnStrings, Done);
      New(pnStrings, Init);
      pnStrings.Add;
      raise;
     end;
    end;
   end else
   begin
    if ptPointerSize > 0 then
     AddStateString(etHint, ListFileName, ListIndex,
          WideFormat(K7S(SMUPointerIsExist), [I]), True) else
     AddStateString(etHint, ListFileName, ListIndex,
          WideFormat(K7S(SMUStringIsExist), [I]), True);
   end;
   if ptPointerSize > 0 then
   begin
    if ptPunType then Inc(I) else
    begin
     Inc(I, ptPointerSize + ptInterval);
     if ptPointerSize > 4 then Dec(I, 4);
    end;
   end else
   if ptStringLength > 0 then Inc(I, ptStringLength + ptInterval) else
   begin
    Inc(I, L);
    I := Aligned(I, ptAlignment);
   end;
  end;
 end;
end;

procedure TKruptarProject.ReloadStrs(PointerTable: PPointerTable; DLL: PDLL);
var
 X, Offset, J, L: Integer;
 P: PPointer;
 PS: PKStrings;
 BS: PTextString;
 ST: PString;
 B: PTextStrings;
begin
 with PointerTable^ do
 if ptPointers.Count > 0 then
 begin
  Offset := -1;
  with ptPointers^ do
  begin
   P := Root;
   while P <> NIL do with P^ do
   begin
    L := 0;
    New(PS, Init);
    try
     if pnTable1 <> NIL then with pnTable1^ do
      DLL.SetVariables(ROM, ROMSize, EndRoot, EndMaxCodes, ptCharSize);
     if ptPointerSize > 0 then
     begin
      if not ptPunType then
       X := GetPointer(ROM^[pnPos], pnPos, pnReference, J) else
       X := GetPointerPun(ROM, pnPos, ptInterval, pnReference, J);
     end else
     begin
      if (ptStringLength > 0) or (Offset < 0) then
       Offset := pnPos;
      if ptStringLength <= 0 then
       pnPos := Offset;
      X := Offset;
     end;
     if ptNotInSource or (pnTable1 = NIL) then
     begin
      New(B, Init);
      with pnStrings^ do for J := 0 to Count - 1 do B.Add;
     end else with pnTable1^ do
     begin
      if (ptPointerSize > 0) and ptPtrToPtr then
       B := DLL.GetStrings(GetPointer(ROM[X], X, pnReference, J),
                           ptStringLength) else
       B := DLL.GetStrings(X, ptStringLength);
     end;
     if B <> NIL then
     try
      BS := B.Root;
      if (BS = NIL) and (ptStringLength <= 0) and (ptPointerSize <= 0) then
       Inc(Offset);
      ST := pnStrings.Root;
      while BS <> NIL do with PS.Add^, BS^ do
      begin
       stOriginal := Str;
       if (ptPointerSize <= 0) and (ptStringLength <= 0) then
       begin
        if DLL.GetMethod = tmNone then
        begin
         L := Integer(DLL.NeedEnd);
         if L <= 0 then L := Length(Str);
        end else L := Length(Str);
       end;
       BS := Next;
       if ST = NIL then
        stNew := pnTable1.DataToString(Pointer(Str)^, Length(Str),
                          ptCharSize, Flags and KP_CRLF_CONVERT <> 0) else
        stNew := ST.stNew;
       if ST <> NIL then ST := ST.Next;
      end;
     finally
      if ptNotInSource then
       Dispose(B, Done) else
      DLL.DisposeStrings(B);
     end else PS.Add;
     Dispose(pnStrings, Done);
     if (ptPointerSize > 0) and not ptPunType then
      pnParamData := FillParamData(ROM, pnPos) else
      pnParamData := '';
     pnStrings := PS;
    except
     Dispose(PS, Done);
    end;
    if (ptPointerSize <= 0) and (ptStringLength <= 0) then
    begin
     Inc(Offset, L);
     Offset := Aligned(Offset, ptAlignment);
    end;
    P := Next;
   end;
  end;
 end;
end;

procedure TMainForm.MenuProjectPointersToggleInsertFieldsActionExecute(
  Sender: TObject);
begin
 if SelectedList <> nil then with SelectedList^ do
 begin
  if ptInserter <> nil then
  begin
   Dispose(ptInserter);
   ptInserter := nil;
  end else
  begin
   New(ptInserter);
   with ptInserter^ do
   begin
    piReference := ptReference;
    piPointerSize := ptPointerSize;
    piStringLength := ptStringLength;
    piAlignment := ptAlignment;
    piCharSize := ptCharSize;
    piShiftLeft := ptShiftLeft;
    piAutoPtrStart := ptSigned;
   end;
  end;
  Values.FillValueListBox(TreeView.Selected);
  SetSaved(SelectedRoot.Data, False);
 end;
end;

procedure TMainForm.MenuProjectPointersToggleInsertFieldsActionUpdate(
  Sender: TObject);
begin
 TAction(Sender).Enabled := SelectedList <> NIL;
end;

procedure TPointerTable.SetCharSize(Value: Integer);
begin
 if Value in [1, 2] then
  FptAlign := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetAlignment(Value: Integer);
begin
 if Value in [1..8] then
  FptMultiple := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetPointerSize(Value: Integer);
begin
 if Value in [0..6] then
  FptPointerSize := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetShl(Value: Integer);
begin
 if Value in [0..31] then
  FptShiftLeft := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetStringLength(Value: Integer);
begin
 if (Value >= 0) and (Value <= 1024) then
  FptStringLength := Value else
  raise Exception.Create(SBadField);
end;

function TPointerTable.GetDestCharSz: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piCharSize else
  Result := ptCharSize;
end;

function TPointerTable.GetDestAPS: Boolean;
begin
 if ptInserter <> nil then
  Result := ptInserter.piAutoPtrStart else
  Result := ptSigned;
end;

function TPointerTable.GetDestMult: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piAlignment else
  Result := ptAlignment;
end;

function TPointerTable.GetDestPtrSz: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piPointerSize else
  Result := ptPointerSize;
end;

function TPointerTable.GetDestRef: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piReference else
  Result := ptReference;
end;

function TPointerTable.GetDestShl: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piShiftLeft else
  Result := ptShiftLeft;
end;

function TPointerTable.GetDestStrLen: Integer;
begin
 if ptInserter <> nil then
  Result := ptInserter.piStringLength else
  Result := ptStringLength;
end;

procedure TPointerTable.SetDestCharSz(Value: Integer);
begin
 if (ptInserter <> nil) and (Value in [1, 2]) then
  ptInserter.piCharSize := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestAPS(Value: Boolean);
begin
 if ptInserter <> nil then
  ptInserter.piAutoPtrStart := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestMult(Value: Integer);
begin
 if (ptInserter <> nil) and (Value in [1..8]) then
  ptInserter.piAlignment := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestPtrSz(Value: Integer);
begin
 if (ptInserter <> nil) and (Value in [0..6]) then
  ptInserter.piPointerSize := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestRef(Value: Integer);
begin
 if ptInserter <> nil then
  ptInserter.piReference := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestShl(Value: Integer);
begin
 if (ptInserter <> nil) and (Value in [0..31]) then
  ptInserter.piShiftLeft := Value else
  raise Exception.Create(SBadField);
end;

procedure TPointerTable.SetDestStrLen(Value: Integer);
begin
 if (ptInserter <> nil) and (Value >= 0) and (Value <= 1024) then
  ptInserter.piStringLength := Value else
  raise Exception.Create(SBadField);
end;

function TPointerTable.GetDestPun: Boolean;
begin
 if ptInserter <> nil then
  Result := ptInserter.piPunType else
  Result := ptPunType;
end;

procedure TPointerTable.SetDestPun(Value: Boolean);
begin
 if ptInserter <> nil then
  ptInserter.piPunType := Value else
  raise Exception.Create(SBadField);
end;

function TPointerTable.GetDestLoRom: Boolean;
begin
 if ptInserter <> nil then
  Result := ptInserter.piSnesLoRom else
  Result := ptSnesLoRom;
end;

procedure TPointerTable.SetDestLoRom(Value: Boolean);
begin
 if ptInserter <> nil then
  ptInserter.piSnesLoRom := Value else
  raise Exception.Create(SBadField);
end;

procedure TMainForm.MenuProjectPointerFixPointersPositionsActionUpdate(
  Sender: TObject);
begin
 TAction(Sender).Enabled := (SelectedList <> nil) and
  (SelectedList.ptPointers.Count > 1) and
  ((SelectedList.ptDestPointerSize > 0) or
   (SelectedList.ptDestStringLength > 0));
end;

procedure TMainForm.MenuProjectPointerFixPointersPositionsActionExecute(
  Sender: TObject);
var
 PTR: PPointer;
 PtrPos: Integer;
begin
 if SelectedList <> nil then with SelectedList^ do
 if ((ptDestPointerSize > 0) or (ptDestStringLength > 0)) then
 begin
  PTR := ptPointers.Root;
  PtrPos := -1;
  while PTR <> nil do with PTR^ do
  begin
   if PtrPos < 0 then
    PtrPos := pnInsertPos else
    pnInsertPos := PtrPos;
   pnInsertRef := ptDestReference;
   if ptDestPointerSize > 0 then
   begin
    if ptDestPunType then Inc(PtrPos) else
    begin
     Inc(PtrPos, ptDestPointerSize + ptInterval);
     if ptDestPointerSize > 4 then Dec(PtrPos, 4);
    end;
   end else if ptDestStringLength > 0 then
    Inc(PtrPos, ptDestStringLength + ptInterval);
   PTR := Next;
  end;
  Values.FillValueListBox(TreeView.Selected);
  SetSaved(SelectedRoot.Data, False);
 end;
end;

procedure TMainForm.MenuEditSaveActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled :=
 (* Enable if OriginalMemo is active and not empty *)
 (OriginalMemo.Focused and (OriginalMemo.Lines.Count > 0)) or
 (* Enable if TranslateMemo is active and not empty *)
 (TranslateMemo.Focused and (TranslateMemo.Lines.Count > 0)) or
 (* Enable if ListBox is active and not empty *)
 (ListBox.Focused and (ListBox.SelCount > 0)) or
 (* Enable if GroupsTabControl is active and not empty *)
 (GroupsTabControl.Focused and (GroupsTabControl.Tabs.Count > 0)) or
 (* Enable if PTablesTabControl is active and not empty *)
 (PTablesTabControl.Focused and (PTablesTabControl.Tabs.Count > 0)) or
 (* Enable if one of the folder in TreeView is selected and not empty *)
 (TreeView.Focused and (Node <> NIL) and (TTreeStateType(Node.StateIndex) in
  [tsTable, tsEnds, tsEls, tsCodes, tsPtrTable, tsBlocks]) and
  (Node.getFirstChild <> nil));
end;

type
 TValueListEditorHack = class(TValueListEditor) end;

function TMainForm.GetEditHandle(CheckReadOnly: Boolean): HWND;
begin
 Result := 0;
 if ActiveControl is TCustomEdit then
 begin
  if not CheckReadOnly or not TEdit(ActiveControl).ReadOnly then
   Result := ActiveControl.Handle;
 end else
 if ActiveControl is TKruptarTreeView then
  Result := TreeView_GetEditControl(ActiveControl.Handle) else
 if ActiveControl is TValueListEditor then
 with TValueListEditorHack(ActiveControl) do
 begin
  if (EditList <> nil) and (not CheckReadOnly or
      not TEdit(EditList).ReadOnly) then
   Result := EditList.Handle;
 end;
end;

procedure TMainForm.MenuEditPasteActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := ClipboardHasFormat(CF_TEXT) and
 (MenuEditLoadAction.Enabled or (GetEditHandle(True) <> 0));
end;

procedure TMainForm.MenuEditCopyCutActionUpdate(Sender: TObject);
var
 H: HWND;
 L, E: Integer;
begin
 with TAction(Sender) do
 begin
  (* Enable if other edits are active *)
  H := GetEditHandle(Tag = 1);
  if H <> 0 then
  begin
   SendMessage(H, EM_GETSEL, Integer(@L), Integer(@E));
   L := E - L;
  end else L := 0;
  Enabled := (L > 0) or
  (* Enable if saving enabled *)
  ((GetEditHandle = 0) and MenuEditSaveAction.Enabled);
 end;
end;

procedure TMainForm.TabsCopySourceTextActionExecute(Sender: TObject);
var
 KP: PKruptarProject;
 List: TWideStringList;
begin
 if SelectedRoot = NIL then Exit;
 if CurGroup = NIL then Exit;
 KP := SelectedRoot.Data;
 if KP = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 List := TWideStringList.Create;
 with CurGroup^ do
 try
  if GroupsTabControl.Focused then
   SaveToStringList(List, KP.Flags, True) else
  if grCurPtrTable <> NIL then
   grCurPtrTable.SaveToStringList(List, KP.Flags, True);
   CopyToClipboard(List.Text);
 finally
  List.Free;
 end;
end;

function TPointerTable.GetStringByIndex(Index: Integer): PString;
var
 P: PPointer;
 S: PString;
 I: Integer;
begin
 FLastPointer := nil;
 Result := nil; 
 I := 0;
 P := ptPointers.Root;
 while P <> nil do
 begin
  S := P.pnStrings.Root;
  while S <> nil do
  begin
   if Index = I then
   begin
    FLastPointer := P;
    Result := S;
    Exit;
   end;
   S := S.Next;
   Inc(I);
  end;
  P := P.Next;
 end;
end;

procedure TMainForm.ListBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 ListBox.SetFocus;
end;

function ClearConfirm: Boolean;
begin
 Result := LocMessageDlg(K7S(smuClearConfirm), mtConfirmation,
                 [mbYes, mbNo], 0) = mrYes;
end;

function TMainForm.CopyStringData(Clr: Boolean): WideString;
var
 I, Cnt, SZ: Integer;
 S: PString;
 List: TWideStringList;
 Node: TKruptarTreeNodeType;
 Stream: TStream;
 PP: PPointer;
 PB: PBlock;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 Result := '';
 if TranslateMemo.Focused then
  Result := TranslateMemo.Text else
 if OriginalMemo.Focused then
  Result := OriginalMemo.Text else
 (* Copy selected ListBox items *)
 if ListBox.Focused then
 begin
  if (CurGroup <> nil) and (CurGroup.grCurPtrTable <> nil) then
  with CurGroup^, ListBox do
  begin
   for I := 0 to Count - 1 do
   begin
    if Selected[I] then with grCurPtrTable^ do
    begin
     S := GetStringByIndex(I);
     if S <> nil then with S^ do
     begin
      Result := Result +
                stNew + sLineBreak +
                sStringEnd + sLineBreak +
                sLineBreak;
      if Clr then with FLastPointer^ do
      begin
       stNew := '';
       if (pnTable2 <> nil) and (pnTable2.EndRoot <> nil) then
        stNew := HexDataToString(pnTable2.EndRoot.tiCodes);
      end;
     end;
    end;
   end;
   if Clr then
   begin
    RefreshList;
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end else
 (* Copy from active group *)
 if GroupsTabControl.Focused then
 begin
  if CurGroup <> nil then
  begin
   List := TWideStringList.Create;
   try
    CurGroup.SaveToStringList(List);
    Result := List.Text;
   finally
    List.Free;
   end;
   if Clr then
   begin
    CurGroup.ClearStrings;
    RefreshList;
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end else
 (* Copy from active list box *)
 if PTablesTabControl.Focused then
 begin
  if CurGroup <> nil then with CurGroup^ do
  if grCurPtrTable <> nil then
  begin
   List := TWideStringList.Create;
   try
    grCurPtrTable.SaveToStringList(List);
    Result := List.Text;
   finally
    List.Free;
   end;
   if Clr then
   begin
    grCurPtrTable.ClearStrings;
    RefreshList;
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end else
 if TreeView.Selected <> nil then
 begin
  Node := TreeView.Selected;
  if Node.Data <> nil then
  case TTreeStateType(Node.StateIndex) of
   tsTable:
   if not Clr or ClearConfirm then
   with PTable(Node.Data)^ do
   begin
    Stream := TVarWideStringStream.Create(Addr(Result));
    try
     SaveToStream(Stream, CP_UTF16, False);
    finally
     Stream.Free;
    end;
    if Clr then
    begin
     ClearAll;
     RefreshTable(Node);
     ListBoxClick(nil);
     SetSaved(SelectedRoot.Data, False);
    end;
   end;
   tsPtrTable:
   if not Clr or ClearConfirm then
   with PPointerTable(Node.Data)^, ptPointers^ do
   begin
    SZ := ptPointerSize;
    if SZ = 0 then
    begin
     SZ := ptStringLength;
     if SZ <= 0 then
      SZ := 1;
    end else
    begin
     if SZ > 4 then
      Dec(SZ, 4);
    end;
    I := -1;
    Cnt := 0;
    PP := Root;
    while PP <> nil do with PP^ do
    begin
     if I < 0 then
     begin
      I := pnPos;
      Result := Result + IntToHex(I, 8);
     end;
     if (Next = nil) or (I + SZ <> Next.pnPos) then
     begin
      if Cnt > 1 then
       Result := Result + '-' + IntToHex(I, 8);
      Result := Result + sLineBreak;
      I := -1;
      Cnt := 0;
     end else
     begin
      Inc(I, SZ);
      Inc(Cnt);
     end;
     PP := Next;
    end;
    if Clr then
    begin
     Clear;
     RefreshPtrTable(Node);
     PTablesTabControlChange(NIL);
     SetSaved(SelectedRoot.Data, False);
    end;
   end;
   tsBlocks:
   if not Clr or ClearConfirm then
   with PBlocks(Node.Data)^ do
   begin
    PB := Root;
    while PB <> nil do with PB^ do
    begin
     Result := Result + GetBlockString(blStart, blCount) + sLineBreak;
     PB := Next;
    end;
    if Clr then
    begin
     Clear;
     RefreshBlocks(Node);
     SetSaved(SelectedRoot.Data, False);
    end;
   end;
   tsEnds, tsEls, tsCodes:
   if not Clr or ClearConfirm then
   with PTable(Node.Parent.Data)^ do
   begin
    List := TWideStringList.Create;
    try
     SaveToList(List, System.PPointer(Node.Data)^);
     Result := List.Text;
    finally
     List.Free;
    end;
    if Clr then
    begin
     ClearNodes(System.PPointer(Node.Data)^);
     RefreshTable(Node.Parent);
     ListBoxClick(nil);
     SetSaved(SelectedRoot.Data, False);
    end;
   end;
  end;
 end;
end;

procedure TPointerTables.Clear;
var
 N: PDynamic;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  with Root^ do if Data <> NIL then Dispose(PPointerTable(Data), Done);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
 DataSize := 0;
end;

procedure TGroup.ClearStrings;
var
 PT: PDynamic;
begin
 PT := grPointerTables.Root;
 while PT <> NIL do with PT^ do
 begin
  PPointerTable(Data).ClearStrings;
  PT := Next;
 end;
end;

procedure TPointerTable.ClearStrings;
var
 PP: PPointer;
 PS: PString;
begin
 PP := ptPointers.Root;
 while PP <> NIL do with PP^ do
 begin
  PS := pnStrings.Root;
  while PS <> NIL do with PS^ do
  begin
   stNew := '';
   if (pnTable2 <> nil) and (pnTable2.EndRoot <> nil) then
    stNew := HexDataToString(pnTable2.EndRoot.tiCodes);
   PS := Next;
  end;
  PP := Next;
 end;
end;

procedure TBlocks.Clear;
var
 N: PBlock;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
end;

procedure TTable.SaveToList(List: TWideStringList; RootNode: PTableItem);
var
 I: Integer;
 T: PTableItem;
 SS: WideString;
begin
 T := RootNode;
 while T <> NIL do with T^ do
 begin
  SS := '';
  for I := 1 to Length(tiCodes) do
   SS := SS + IntToHex(Byte(tiCodes[I]), 2);
  if RootNode = Root then
   List.Add(SS + '=' + EscapeStringEncode(tiString)) else
   List.Add(SS);
  T := Next;
 end;
end;

procedure TMainForm.MenuProjectGroupTextResetActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled :=
 (TranslateMemo.Focused and not TranslateMemo.ReadOnly) or
 (ListBox.Focused and (ListBox.SelCount > 0)) or
 MenuProjectGroupTextReloadAction.Enabled;
end;

function TPointerTable.ConvertString(TBL: PTable; const S: RawByteString;
  CRLF: Boolean): WideString;
begin
 if TBL <> NIL then
  Result := TBL.DataToString(Pointer(S)^, Length(S), ptCharSize, CRLF) else
  Result := HexDataToString(S);
end;

procedure TMainForm.PasteStringData(const S, FileName: WideString; Clr:Boolean);
var
 I, ListIndex: Integer;
 PS: PString;
 PT: PTable;
 List: TWideStringList;
 Node: TKruptarTreeNodeType;
 SW: WideString;
 KP: PKruptarProject;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if TranslateMemo.Focused then
 begin
  if not TranslateMemo.ReadOnly then
   TranslateMemo.Text := S;
 end else
 begin
  if SelectedRoot = nil then Exit;
  KP := SelectedRoot.Data;
  if KP = nil then Exit;
  List := TWideStringList.Create;
  try
   List.Text := S;
   ListIndex := 0;
   (* Paste to ListBox *)
   if ListBox.Focused then
   begin
    if (CurGroup <> nil) and (CurGroup.grCurPtrTable <> nil) then
    with CurGroup^, ListBox do
    if (SelCount > 0) and (List.Count > 0) then
    begin
     I := 0;
     while I < Count do
     begin
      if Selected[I] then with grCurPtrTable^ do
      begin
       while (I < Count) and (ListIndex < List.Count) do
       begin
        PS := GetStringByIndex(I);
        if PS <> nil then PS.stNew := ParseStr(List, ListIndex);
        Inc(I);
       end;
       Break;
      end;
      Inc(I);
     end;
     RefreshList;
     SetSaved(SelectedRoot.Data, False);
    end;
   end else
   (* Copy from active group *)
   if GroupsTabControl.Focused then
   begin
    if CurGroup <> nil then
    begin
     CurGroup.ReloadFromStringList(List, ListIndex);
     RefreshList;
     SetSaved(SelectedRoot.Data, False);
    end;
   end else
   (* Copy from active list box *)
   if PTablesTabControl.Focused then
   begin
    if CurGroup <> nil then with CurGroup^ do
    if grCurPtrTable <> nil then
    begin
     grCurPtrTable.ReloadFromStringList(List, ListIndex);
     RefreshList;
     SetSaved(SelectedRoot.Data, False);
    end;
   end else
   if TreeView.Selected <> nil then
   begin
    Node := TreeView.Selected;
    if Node.Data <> nil then
    begin
     I := Node.StateIndex;
     if I = Integer(tsPointer) then
     begin
      Node := Node.Parent;
      I := Node.StateIndex;
     end else
     if (I = Integer(tsTables)) and (FileName <> '') then
     begin
      with KP^ do
      begin
       SW := WideChangeFileExt(WideExtractFileName(FileName), '');
       I := 0;
       repeat
        if I = 0 then
         PT := Tables.Find(SW) else
         PT := Tables.Find(SW + IntToStr(I));
        if PT = nil then Break; 
        Inc(I);
       until false;
       if I > 0 then SW := SW + IntToStr(I);
      end;
      Node := TreeView.Items.AddChild(Node, SW);
      PT := KP.Tables.Add;
      PT.tbName := SW;
      Node.Data := PT;
      SelectedData := PT;
      Node.ImageIndex := Icons[tsTable];
      Node.SelectedIndex := Icons[tsTable];
      Node.StateIndex := Integer(tsTable);
      TreeView.Selected := Node;
      I := Integer(tsTable);
     end;
     case TTreeStateType(I) of
      tsTable, tsEnds, tsEls, tsCodes:
      begin
       if I <> Integer(tsTable) then
        Node := Node.Parent;
       with PTable(Node.Data)^ do
       begin
        FLastFileName := FileName;
        case TTreeStateType(I) of
         tsTable:
         begin
          if Clr then
           LoadFromList(List) else
           AddFromStringList(List);
         end;
         tsEnds:
         begin
          if Clr then ClearNodes(EndRoot);
          AddFromStringList(List, taEnds);
         end;
         tsEls:
         begin
          if Clr then ClearNodes(ElRoot);
          AddFromStringList(List, taEls);
         end;
         tsCodes:
         begin
          if Clr then ClearNodes(Root);
          AddFromStringList(List, taCodes);
         end;
        end;
       end;
       RefreshTable(Node);
       Node.Expand(False);       
       ListBoxClick(nil);
       SetSaved(SelectedRoot.Data, False);
      end;
      tsPtrTable:
      begin
       if Clr then PPointerTable(Node.Data).ptPointers.Clear;
       PointersListAdd(List, FileName);
       if KP.fSaved then
       begin
        SetSaved(SelectedRoot.Data, False);
        RefreshPtrTable(Node);
        TreeViewChange(nil, Node);
        if Node.Data = CurGroup.grCurPtrTable then
         RefreshList;
       end;
      end;
      tsBlocks:
      begin
       if Clr then PBlocks(Node.Data).Clear;
       BlocksListAdd(List, FileName);
       if KP.fSaved then
       begin
        SetSaved(SelectedRoot.Data, False);
        RefreshBlocks(Node);
        TreeViewChange(nil, Node);
       end;
      end;
     end;
    end;
   end;
  finally
   List.Free;
  end;
 end;
end;

procedure TTable.AddFromStringList(List: TWideStringList; Mode: TTableAddMode);
var
 C, I: Integer;
 S: WideString;
 CD: RawByteString;
 CC: WideChar;
 Normal: Boolean;
 SaveRoot, SaveCur, DefaultCRLF: PTableItem;
begin
 Normal := Mode <> taEnds;
 for I := 0 to List.Count - 1 do
 begin
  S := List[I];
  if (Mode in [taTBL, taCodes]) and (Pos('=', S) > 0) then
  begin
   CD := '';
   repeat
    C := GetByte(S);
    if C >= 0 then CD := CD + AnsiChar(C) else Break;
   until False;
   TrimLeft(S);
   if (Length(CD) > 0) and (Length(S) > 1) and (S[1] = '=') then
   begin
    Delete(S, 1, 1);
    with Add^ do
    begin
     tiCodes := CD;
     tiString := EscapeStringDecode(S);
    end;
   end else
   begin
    CD := '';
    AddStateString(etHint, FLastFileName, I,
                   K7S(SMUInvalidTableItem), True);
   end; (* if Pos('=', S) > 0 then *)
  end else
  if WideUpperCase(S) = 'ENDS' then
   Normal := False else
  if Mode <> taCodes then
  begin
   S := Trim(S);
   if S <> '' then
   begin
    CC := S[1];
    if not (((CC >= '0') and (CC <= '9')) or
            ((CC >= 'A') and (CC <= 'F')) or
            ((CC >= 'a') and (CC <= 'f'))) then
    begin
     if (CC = '/') and (Length(S) >= 2) and (S[2] = '/') then Continue;
     Delete(S, 1, 1);
     CD := '';
     repeat
      C := GetByte(S);
      if C >= 0 then CD := CD + AnsiChar(C) else Break;
     until False;
     if Length(CD) > 0 then
     begin
      if (Mode = taEls) or (Normal and (CC <> '/')) then
      with AddEl^ do
      begin
       tiCodes := CD;
       tiString := CC;
      end else
      with AddEnd^ do
      begin
       tiCodes := CD;
       tiString := CC;
      end;
     end else
     begin
      CD := '';
      AddStateString(etHint, FLastFileName, I,
                     K7S(SMUInvalidTableItem), True);
     end;
    end else (* if start from Hex *)
    begin
     CD := '';
     repeat
      C := GetByte(S);
      if C >= 0 then CD := CD + AnsiChar(C) else Break;
     until False;
     if Length(CD) > 0 then
     begin
      if (Mode = taEls) or (Normal) then
      with AddEl^ do
      begin
       tiCodes := CD;
       tiString := '';
      end else
      with AddEnd^ do
      begin
       tiCodes := CD;
       tiString := '';
      end;
     end else
     begin
      CD := '';
      AddStateString(etHint, FLastFileName, I,
                     K7S(SMUInvalidTableItem), True);
     end;
    end; (* if start from Hex *)
   end; (* if S <> '' then *)
  end;
 end; (* for I := 0 to List.Count - 1 do *)
 DefaultCRLF := FindEl('^');
 if (DefaultCRLF <> nil) and (DefaultCRLF <> ElRoot) then
 begin
  SaveRoot := ElRoot;
  SaveCur := ElCur;
  ElRoot := nil;
  with AddEl^ do
  begin
   tiCodes := DefaultCRLF.tiCodes;
   tiString := DefaultCRLF.tiString;
   Next := SaveRoot;
  end;
  ElRoot := ElCur;
  ElCur := SaveCur;
  RemoveByItem(ElRoot, DefaultCRLF);
 end else RefreshMax;
end;

procedure TMainForm.PointersListAdd(List: TWideStringList;
  const FileName: WideString);
var
 I, J: Integer;
 BlockStart, BlockEnd: LongInt;
 S: WideString;
 DLL: PDLL;
 Node: TKruptarTreeNodeType;
 SD: PPointer;
begin
 if SelectedList = nil then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 with PKruptarProject(SelectedRoot.Data)^, SelectedList^ do
 begin
  if not FieldFileExists(KP_SRC_ROM, InputROM, False) then Exit;
  if ptTable1 = NIL then
  begin
   LocMessageDlg(K7S(SMUTableNotSelected), mtError, [mbOk], 0);
   Exit;
  end;
  with OwnedBy^ do
  begin
   DLL := DLLs.Get(grDLLName);
   if DLL = NIL then
   begin
    LocMessageDlg(WideFormat(K7S(SMULibraryNotFound), [grDLLName]),
                  mtError, [mbOk], 0);
    Exit;
   end;
   if (ptStringLength <= 0) and (DLL.GetMethod <> tmNone) and DLL.NeedEnd then
   begin
    if ptTable1.EndCount <= 0 then
    begin
     LocMessageDlg(WideFormat(K7S(SMUStringTerminateCodeNotExist),
                   [ptTable1.tbName]), mtError, [mbOk], 0);
     Exit;
    end
   end;
   SD := nil;
   if LoadROM then with ptPointers^ do
   try
    ptItemIndex := 0;
    BlockEnd := 0;
    SD := Cur;
    for J := 0 to List.Count - 1 do
    begin
     S := List[J];
     I := Pos('-', S);
     if I <= 0 then
     begin
      BlockStart := MsgHexToInt(FileName, J, Trim(S));
      if HexError then Continue;
      BlockEnd := BlockStart;
     end else
     begin
      BlockStart := MsgHexToInt(FileName, J, Trim(Copy(S, 1, I - 1)));
      if HexError then Continue;
      BlockEnd := MsgHexToInt(FileName, J, Trim(Copy(S, I + 1, Length(S) - I)));
      if HexError then Continue;
     end;
     AddPointersList(BlockStart, BlockEnd, SelectedRoot.Data,
                     DLL, SelectedList, FileName, J);
    end;
   finally
    FreeROM;
    if SD = nil then SelectedData := Root else
    with SD^ do if Next <> nil then SelectedData := Next;
    Node := TreeView.Selected;
    if Node.StateIndex = Integer(tsPointer) then
     Node := Node.Parent;
    RefreshPtrTable(Node);
    TreeViewChange(nil, TreeView.Selected);
    if CurGroup.grCurPtrTable = Node.Data then
     RefreshList;
    TreeViewDblClick(nil);
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end;
end;

procedure TMainForm.BlocksListAdd(List: TWideStringList;
  const FileName: WideString);
var
 Node, Blocks: TKruptarTreeNodeType;
 B: PBlock;
 I, E, S: Integer;
 S1, S2: WideString;
begin
 Node := TreeView.Selected;
 Blocks := Node;
 if Blocks = NIL then Exit;
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 if TTreeStateType(Blocks.StateIndex) in [tsBlock, tsPointer] then
 begin
  Blocks := Blocks.Parent;
  if Blocks = NIL then Exit;
 end;
 if TTreeStateType(Blocks.StateIndex) = tsPtrTable then
 begin
  Blocks := Blocks.Parent;
  if Blocks = NIL then Exit;
 end;
 if TTreeStateType(Blocks.StateIndex) in [tsBlocks, tsPtrTables] then
 begin
  Blocks := Blocks.Parent;
  if Blocks = NIL then Exit;
 end;
 if TTreeStateType(Blocks.StateIndex) <> tsGroup then Exit;
 with PGroup(Blocks.Data)^ do
 begin
  Blocks := Blocks.GetFirstChild;
  for I := 0 to List.Count - 1 do
  begin
   S1 := Trim(List[I]);
   E := Pos('-', S1);
   S2 := Trim(Copy(S1, E + 1, Length(S1) - E));
   SetLength(S1, E - 1);
   S1 := Trim(S1);
   S := MsgHexToInt(FileName, I, S1);
   if not HexError then
   begin
    E := MsgHexToInt(FileName, I, S2);
    if not HexError then
    begin
     B := grBlocks.Add;
     SelectedData := B;
     with TreeView, B^ do
     begin
      blStart := S;
      blCount := (E + 1) - S;
//      blUsed := False;
      Node := Items.AddChild(Blocks, IntToHex(S, 7) + '-' + IntToHex(E, 7));
      with Node do
      begin
       Data := B;
       StateIndex := Integer(tsBlock);
       ImageIndex := Icons[tsBlock];
       SelectedIndex := Icons[tsBlock];
      end;
     end;
    end;
   end;
  end;
  TreeView.Selected := Node;
  SetSaved(SelectedRoot.Data, False);
  TreeViewChange(nil, Node);
 end;
end;

procedure TTable.LoadFromList(List: TWideStringList);
var
 Temp: TTable;
begin
 Temp.Init;
 try
  Temp.AddFromStringList(List);
  ClearAll;
  Root := Temp.Root;
  Cur := Temp.Cur;
  EndRoot := Temp.EndRoot;
  EndCur := Temp.EndCur;
  ElRoot := Temp.ElRoot;
  ElCur := Temp.ElCur;
  CMaxCodes := Temp.CMaxCodes;
  EndMaxCodes := Temp.EndMaxCodes;
  ElMaxCodes := Temp.ElMaxCodes;
  CMaxChars := Temp.CMaxChars;
  Count := Temp.Count;
  EndCount := Temp.EndCount;
  ElCount := Temp.ElCount;
  FillChar(Temp, SizeOf(TTable), 0);
 finally
  Temp.Done;
 end;
end;

procedure TMainForm.MenuProjectItemsAddFromFileActionUpdate(Sender: TObject);
var
 Node: TKruptarTreeNodeType;
begin
 Node := TreeView.Selected;
 TAction(Sender).Enabled :=
 (* Enable if one of the folder in TreeView is selected and not empty *) 
 (TreeView.Focused and (Node <> NIL) and (TTreeStateType(Node.StateIndex) in
  [tsTables, tsEnds, tsEls, tsCodes, tsPtrTable, tsBlocks, tsPointer]));
end;

procedure TMainForm.MenuFileSaveAllActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (Projects.Count > 0) and not AllSaved;
end;

procedure TMainForm.ClearData;
var
 I: Integer;
 S: PString;
 Node: TKruptarTreeNodeType;
 PTBL, PGRP, PLST: PDynamic;
 TBL: PTable;
 PTR: PPointer;
 Exist: Boolean;
begin
 StatesMemo.Clear;
 StatesMemo.Visible := False;
 (* Clear selected ListBox items *)
 if ListBox.Focused then
 begin
  if (CurGroup <> nil) and (CurGroup.grCurPtrTable <> nil) then
  with CurGroup^, ListBox do
  begin
   for I := 0 to Count - 1 do
   begin
    if Selected[I] then with grCurPtrTable^ do
    begin
     S := GetStringByIndex(I);
     if S <> nil then with S^, FLastPointer^ do
     begin
      stNew := '';
      if (pnTable2 <> nil) and (pnTable2.EndRoot <> nil) then
       stNew := HexDataToString(pnTable2.EndRoot.tiCodes);
     end;
    end;
   end;
   RefreshList;
   SetSaved(SelectedRoot.Data, False);
  end;
 end else
 (* Clear active group *)
 if GroupsTabControl.Focused then
 begin
  if CurGroup <> nil then
  begin
   CurGroup.ClearStrings;
   RefreshList;
   SetSaved(SelectedRoot.Data, False);
  end;
 end else
 (* Clear active list box *)
 if PTablesTabControl.Focused then
 begin
  if CurGroup <> nil then with CurGroup^ do
  if grCurPtrTable <> nil then
  begin
   grCurPtrTable.ClearStrings;
   RefreshList;
   SetSaved(SelectedRoot.Data, False);
  end;
 end else
 if TreeView.Selected <> nil then
 begin
  Node := TreeView.Selected;
  if Node.Data <> nil then
  case TTreeStateType(Node.StateIndex) of
   tsTables: if ClearConfirm then
   with PKruptarProject(SelectedRoot.Data)^ do
   begin
    PTBL := Tables.Root;
    while PTBL <> nil do with PTBL^ do
    begin
     Exist := False;
     TBL := Data;
     PGRP := Groups.Root;
     while not Exist and (PGRP <> nil) do with PGRP^, PGroup(Data)^ do
     begin
      PLST := grPointerTables.Root;
      while not Exist and (PLST <> nil) do with PLST^, PPointerTable(Data)^ do
      begin
       if (TBL = ptTable1) or (TBL = ptTable2) then
       begin
        Exist := True;
        AddStateString(etError, TBL.tbName, -1,
        WideFormat(K7S(SMUTableExistIn), [grName + '->' + ptName]), True);
       end;
       PTR := ptPointers.Root;
       while not Exist and (PTR <> nil) do with PTR^ do
       begin
        if (TBL = pnTable1) or (TBL = pnTable2) then
        begin
         Exist := True;
         AddStateString(etError, TBL.tbName, -1,
         WideFormat(K7S(SMUTableExistIn),
         [grName + '->' + ptName + '->' + IntToHex(pnPtr, 8)]), True)
        end;
        PTR := Next;
       end;
       PLST := Next;
      end;
      PGRP := Next;
     end;
     PTBL := Next;
     if not Exist then Tables.Remove(TBL);
    end;
    RefreshTables(Node);
    Node.Expand(False);
    SetSaved(SelectedRoot.Data, False);
   end;
   tsTable: if ClearConfirm then
   with PTable(Node.Data)^ do
   begin
    ClearAll;
    RefreshTable(Node);
    Node.Expand(False);
    ListBoxClick(nil);
    SetSaved(SelectedRoot.Data, False);
   end;
   tsGroups: if ClearConfirm then
   begin
    PKruptarProject(SelectedRoot.Data).Groups.Clear;
    ListBox.Clear;
    ListBoxClick(nil);
    CurGroup := nil;
    PTablesTabControl.Tabs.Clear;
    GroupsTabControl.Tabs.Clear;
    RefreshGroups(Node, False);
    SetSaved(SelectedRoot.Data, False);
   end;
   tsGroup: if ClearConfirm then
   begin
    SelectedGroup.grPointerTables.Clear;
    SelectedGroup.grBlocks.Clear;
    RefreshGroup(Node, False);
    Node.Expand(False);
    if SelectedGroup = CurGroup then
    begin
     PTablesTabControl.Tabs.Clear;
     PTablesTabControlChange(nil);
    end;
    SetSaved(SelectedRoot.Data, False);
   end;
   tsPtrTables: if ClearConfirm then
   begin
    SelectedGroup.grPointerTables.Clear;
    RefreshPtrTables(Node);
    SelectedGroup.grCurPtrTable := nil;
    if SelectedGroup = CurGroup then
    begin
     PTablesTabControl.Tabs.Clear;
     PTablesTabControlChange(nil);
    end;
    SetSaved(SelectedRoot.Data, False);
   end;
   tsPtrTable: if ClearConfirm then
   with PPointerTable(Node.Data)^, ptPointers^ do
   begin
    Clear;
    RefreshPtrTable(Node);
    PTablesTabControlChange(NIL);
    SetSaved(SelectedRoot.Data, False);
   end;
   tsBlocks: if ClearConfirm then
   with PBlocks(Node.Data)^ do
   begin
    Clear;
    RefreshBlocks(Node);
    SetSaved(SelectedRoot.Data, False);
   end;
   tsEnds, tsEls, tsCodes: if ClearConfirm then
   with PTable(Node.Parent.Data)^ do
   begin
    ClearNodes(System.PPointer(Node.Data)^);
    RefreshTable(Node.Parent);
    ListBoxClick(nil);
    SetSaved(SelectedRoot.Data, False);
   end;
  end;
 end;
end;

procedure TTables.Clear;
var
 N: PDynamic;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  with Root^ do
   if Data <> NIL then
    Dispose(PTable(Data), Done);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
 DataSize := 0;
end;

procedure TGroups.Clear;
var
 N: PDynamic;
begin
 while Root <> NIL do
 begin
  N := Root.Next;
  with Root^ do
   if Data <> NIL then
    Dispose(PGroup(Data), Done);
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
 Cur := NIL;
 DataSize := 0;
end;

procedure TMainForm.AddRecent(const FileName: WideString);

 procedure Push(Index: Integer);
 begin
  if Index >= 4 then
   SetRecentFileName(FileRecent4Item, GetRecentFileName(FileRecent3Item));
  if Index >= 3 then
   SetRecentFileName(FileRecent3Item, GetRecentFileName(FileRecent2Item));
  if Index >= 2 then
   SetRecentFileName(FileRecent2Item, GetRecentFileName(FileRecent1Item));
  SetRecentFileName(FileRecent1Item, FileName);
 end;

 var
  UpperName: WideString;

 function CompareWith(Item: TKruptarMenuItem): Boolean;
 begin
  if UpperName = '' then
   UpperName := WideUpperCase(FileName);
  if Item.Enabled and (UpperName = WideUpperCase(GetRecentFileName(Item))) then
  begin
   Push(Item.Tag);
   Result := True;
  end else Result := False;
 end;

var
 LastEmpty: TKruptarMenuItem;
 Index: Integer;
begin
 if WideUpperCase(WideExtractFileExt(FileName)) = '.KPX' then
 begin
  if not CompareWith(FileRecent1Item) and
     not CompareWith(FileRecent2Item) and
     not CompareWith(FileRecent3Item) and
     not CompareWith(FileRecent4Item) then
  begin
   if not FileRecent1Item.Enabled then
    LastEmpty := FileRecent1Item else
   if not FileRecent2Item.Enabled then
    LastEmpty := FileRecent2Item else
   if not FileRecent3Item.Enabled then
    LastEmpty := FileRecent3Item else
   if not FileRecent4Item.Enabled then
    LastEmpty := FileRecent4Item else
    LastEmpty := nil;
   if LastEmpty <> nil then
   begin
    FileSeparator2.Visible := True;
    LastEmpty.Enabled := True;
    LastEmpty.Visible := True;
    Index := LastEmpty.Tag;   
   end else Index := 4;
   Push(Index);
  end;
 end;
end;

procedure TMainForm.FileRecentItemClick(Sender: TObject);
begin
 OpenFile(GetRecentFileName(TKruptarMenuItem(Sender)));
end;

procedure TMainForm.OpenFile(const FileName: WideString);
var
 List: TWideStringList;
begin
 OpenProjectDialog.FileName := FileName;
 List := TWideStringList.Create;
 try
  List.Add(FileName);
  OpenFiles(List);
 finally
  List.Free;
 end;
end;

procedure TMainForm.RefreshProjectTabs(ProjectIndex: Integer);
var
 P: PDynamic;
 I: Integer;
 S: WideString;
begin
 SendMessage(ProjectTabs.Handle, WM_SETREDRAW, 0, 0);
 ProjectTabs.Tabs.Clear;
 I := 0;
 P := Projects.Root;
 while P <> nil do with P^, PKruptarProject(P.Data)^ do
 begin
  S := ProjectFile;
  if not fSaved then S := '[*]' + S;
  ProjectTabs.Tabs.Add(S);
  if ProjectIndex = I then
   ProjectTabs.TabIndex := I;
  Inc(I);
  P := Next;
 end;
 SendMessage(ProjectTabs.Handle, WM_SETREDRAW, 1, 0);
 ProjectTabs.Repaint;
end;

procedure TMainForm.ProjectTabsChange(Sender: TObject);
begin
 RefreshTree(ProjectTabs.TabIndex);
 TreeView.Selected := TreeView.Items.GetFirstNode;
 TreeView.Selected.Expand(False);
 TreeViewChange(Nil, TreeView.Selected);
end;

procedure TMainForm.AllSavedCheck;
var
 P: PDynamic;
begin
 AllSaved := True;
 P := Projects.Root;
 while P <> nil do with P^ do
 begin
  if not PKruptarProject(Data).fSaved then
  begin
   AllSaved := False;
   Exit;
  end;
  P := Next;
 end;
end;

procedure TMainForm.CustomPopupPopup(Sender: TObject);
begin
 MenuProjectItemsAddFromFileAction.Update;
 MenuProjectItemAddAction.Update;
 MenuProjectItemUpAction.Update;
 MenuProjectItemDownAction.Update;
 MenuProjectItemRemoveAction.Update;
 MenuEditUndoAction.Update;
end;

procedure TMainForm.PointersSortActionExecute(Sender: TObject);
var
 List: array of PPointer;
 procedure AscSortList(L, R: Integer);
 var
  I, J: Integer;
  X, Y: PPointer;
 begin
  I := L;
  J := R;
  X := List[(L + R) shr 1];
  repeat
   while List[I].pnPos < X.pnPos do Inc(I);
   while X.pnPos < List[J].pnPos do Dec(J);
   if I <= J then
   begin
    if List[I].pnPos <> List[J].pnPos then
    begin
     Y := List[I];
     List[I] := List[J];
     List[J] := Y;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then AscSortList(L, J);
  if I < R then AscSortList(I, R);
 end; (* AscSortList *)
 procedure DescSortList(L, R: Integer);
 var
  I, J: Integer;
  X, Y: PPointer;
 begin
  I := L;
  J := R;
  X := List[(L + R) shr 1];
  repeat
   while List[I].pnPos > X.pnPos do Inc(I);
   while X.pnPos > List[J].pnPos do Dec(J);
   if I <= J then
   begin
    if List[I].pnPos <> List[J].pnPos then
    begin
     Y := List[I];
     List[I] := List[J];
     List[J] := Y;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then DescSortList(L, J);
  if I < R then DescSortList(I, R);
 end; (* DescSortList *)

var
 N: PPointer;
 I, X: Integer;
 SelectedItem: PPointer;
 TreeNode: TKruptarTreeNodeType;
begin
 SelectedItem := nil;
 with SelectedList^, ptPointers^ do
 begin
  X := ptItemIndex;
  SetLength(List, Count);
  N := Root;
  for I := 0 to Count - 1 do
  begin
   if I = X then
    SelectedItem := N;
   List[I] := N;
   N := N.Next;
  end;
  if TAction(Sender).Tag = 0 then
   AscSortList(0, Count - 1) else
   DescSortList(0, Count - 1);
  Root := List[0];
  ptItemIndex := 0;
  N := Root;
  for I := 1 to Count - 1 do
  begin
   Cur := List[I];
   if Cur = SelectedItem then
    ptItemIndex := I;
   N.Next := Cur;
   N := Cur;
  end;
  N.Next := nil;
  X := ptItemIndex;
 end;
 TreeNode := TreeView.Selected;
 if TreeNode.StateIndex = Integer(tsPointer) then
  TreeNode := TreeNode.Parent;
 RefreshPtrTable(TreeNode);
 TreeViewChange(nil, TreeView.Selected);
 if CurGroup.grCurPtrTable = TreeNode.Data then
 begin
  SelectedList.ptItemIndex := X;
  RefreshList;
 end;
 SetSaved(SelectedRoot.Data, False);
end;

procedure TMainForm.PointersSortActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (SelectedList <> nil) and
  (SelectedList.ptPointers.Count > 1);
end;

initialization
 ErrorStr[etNone] := '';
 InitDynamicStrings;
 AddStateString := AddStateStringWin;
 New(Projects, Init);
 New(Values, Init);
 New(DLLs, Init);
finalization
 Dispose(DLLs, Done);
 Dispose(Values, Done);
 Dispose(Projects, Done);
end.



