object DictionaryDialog: TDictionaryDialog
  Left = 202
  Top = 280
  BorderStyle = bsDialog
  Caption = 'DTE/MTE Dictionary'
  ClientHeight = 257
  ClientWidth = 174
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  LocCaption = 'DictionaryDialogTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TKruptarPanel
    Left = 0
    Top = 0
    Width = 174
    Height = 217
    Align = alTop
    BorderStyle = bsSingle
    TabOrder = 0
    object TableLabel: TLocLabel
      Left = 8
      Top = 8
      Width = 69
      Height = 13
      Caption = 'TBL to modify:'
      LocCaption = 'DictionaryDialogTable'
      Localizer = MainForm.Localizer
    end
    object MaxWordSizeLabel: TLocLabel
      Left = 8
      Top = 168
      Width = 102
      Height = 13
      Caption = 'Maximal word length:'
      LocCaption = 'DictionaryDialogMaxWordLen'
      Localizer = MainForm.Localizer
    end
    object ItemSizeLabel: TLocLabel
      Left = 8
      Top = 88
      Width = 50
      Height = 13
      Caption = 'Code size:'
      LocCaption = 'DictionaryDialogCodeSize'
      Localizer = MainForm.Localizer
    end
    object ItemStartLabel: TLocLabel
      Left = 8
      Top = 48
      Width = 103
      Height = 13
      Caption = 'First bytes of a code:'
      LocCaption = 'DictionaryDialogFirstBytes'
      Localizer = MainForm.Localizer
    end
    object LabelStartValue: TLocLabel
      Left = 8
      Top = 128
      Width = 95
      Height = 13
      Caption = 'Code'#39's count origin:'
      LocCaption = 'DictionaryDialogCountOrigin'
      Localizer = MainForm.Localizer
    end
    object TablesBox: TKruptarComboBox
      Left = 8
      Top = 24
      Width = 153
      Height = 22
      Style = csOwnerDrawFixed
      TabOrder = 0
    end
    object MaxWordSizeEdit: TSpinEdit
      Left = 8
      Top = 184
      Width = 153
      Height = 22
      MaxValue = 64
      MinValue = 2
      TabOrder = 4
      Value = 2
      OnChange = MaxWordSizeEditChange
    end
    object FirstBytesEdit: TEdit
      Left = 8
      Top = 64
      Width = 153
      Height = 21
      TabOrder = 1
      OnChange = FirstBytesEditChange
      OnKeyPress = FirstBytesEditKeyPress
    end
    object SizeBox: TComboBox
      Left = 8
      Top = 104
      Width = 153
      Height = 22
      Style = csOwnerDrawFixed
      ItemIndex = 0
      TabOrder = 2
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '2 (BIG_ENDIAN)'
        '3'
        '3 (BIG_ENDIAN)'
        '4'
        '4 (BIG_ENDIAN)')
    end
    object ItemStartEdit: TEdit
      Left = 7
      Top = 144
      Width = 154
      Height = 21
      TabOrder = 3
      Text = '0'
    end
  end
  object OkButton: TLocButton
    Left = 4
    Top = 224
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkButtonClick
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelButton: TLocButton
    Left = 92
    Top = 224
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
end
