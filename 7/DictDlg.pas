unit DictDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, LocClasses, KruptarComponents, UnicodeUtils, FormatW
  {$IFNDEF FORCED_UNICODE}
  , TntStdCtrls
  , TntExtCtrls
  {$ENDIF};

type
  TDictionaryDialog = class(TLocForm)
    Panel: TKruptarPanel;
    TablesBox: TKruptarComboBox;
    MaxWordSizeEdit: TSpinEdit;
    FirstBytesEdit: TEdit;
    SizeBox: TComboBox;
    TableLabel: TLocLabel;
    MaxWordSizeLabel: TLocLabel;
    ItemSizeLabel: TLocLabel;
    ItemStartLabel: TLocLabel;
    OkButton: TLocButton;
    CancelButton: TLocButton;
    ItemStartEdit: TEdit;
    LabelStartValue: TLocLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OkButtonClick(Sender: TObject);
    procedure FirstBytesEditKeyPress(Sender: TObject; var Key: Char);
    procedure FirstBytesEditChange(Sender: TObject);
    procedure MaxWordSizeEditChange(Sender: TObject);
  private
    { Private declarations }
  public
   FirstBytes: RawByteString;
   Function Execute(List: TWideStrings; const FS: RawByteString; Index, EL, ES, WS: Integer): Boolean;
  end;

var
  DictionaryDialog: TDictionaryDialog;

implementation

Uses MainUnit, k7HexUnit, StringConsts, LocMsgBox, MyUtils;

{$R *.dfm}

Var
 OkPressed: Boolean;

Function TDictionaryDialog.Execute(List: TWideStrings; const FS: RawByteString;
  Index, EL, ES, WS: Integer): Boolean;
Var
  I: Integer; S: String;
begin
  TablesBox.Items := List;
  TablesBox.ItemIndex := Index;
  SizeBox.ItemIndex := EL;
  S := '';
  For I := 1 to Length(FS) do
   S := S + IntToHex(Byte(FS[I]), 2);
  FirstBytesEdit.Text := S;
  ItemStartEdit.Text := 'h' + IntToHex(ES, 2);
  MaxWordSizeEdit.Value := WS;
  ShowModal;
  Result := ModalResult = mrOk;
end;

procedure TDictionaryDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 TablesBox.SetFocus;
end;

procedure TDictionaryDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := False;
 If OkPressed then
 begin
  OkPressed := False;
  If not UnsCheckOut(ItemStartEdit.Text) then
  begin
   LocMessageDlg(WideFormat(K7S(SDD_BadStartValue), [LabelStartValue.Caption]),
         mtError, [mbOk], 0);
   CanClose := False;
  end Else CanClose := True;
 end Else CanClose := True;
end;

procedure TDictionaryDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

Const
 HexSet: Set of AnsiChar = ['0'..'9', 'A'..'F'];

procedure TDictionaryDialog.FirstBytesEditKeyPress(Sender: TObject;
  var Key: Char);
begin
 If Key = #8 then Exit;
 Key := UpCase(Key);
 If not (AnsiChar(Key) in HexSet) then Key := #0;
end;

procedure TDictionaryDialog.FirstBytesEditChange(Sender: TObject);
Var S: String; I, J: Integer; C: Boolean;
begin
 S := FirstBytesEdit.Text;
 C := False;
 For I := 1 to Length(S) do If not (AnsiChar(S[I]) in HexSet) then
 begin
  S[I] := Upcase(S[I]);
  If not (AnsiChar(S[I]) in HexSet) then S[I] := '0';
  C := True;
 end;
 If C then FirstBytesEdit.Text := S;
 J := Length(S);
 If J mod 2 > 0 then Dec(J);
 FirstBytes := '';
 If J <= 1 then Exit;
 I := 1;
 Repeat
  FirstBytes := FirstBytes + AnsiChar(HexVal(S[I] + S[I + 1]));
  Inc(I, 2);
 Until I > J;
end;

procedure TDictionaryDialog.MaxWordSizeEditChange(Sender: TObject);
Var S: String;
begin
 With MaxWordSizeEdit do
 begin
  S := Text;
  If not UnsCheckOut(S) then
   Value := 0 Else If UpCase(S[1]) = 'H' then
   Value := GetLDW(S);
 end;
end;

end.
