object SearchTextDialog: TSearchTextDialog
  Left = 221
  Top = 459
  BorderStyle = bsDialog
  Caption = 'Find Text'
  ClientHeight = 151
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  LocCaption = 'FindDialogTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object SearchLabel: TLocLabel
    Left = 0
    Top = 9
    Width = 84
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Text to find:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
    LocCaption = 'FindDialogFind'
    Localizer = MainForm.Localizer
  end
  object SearchText: TKruptarComboBox
    Left = 88
    Top = 9
    Width = 243
    Height = 26
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object OptionsBox: TLocGroupBox
    Left = 6
    Top = 37
    Width = 326
    Height = 75
    Caption = 'Options'
    TabOrder = 1
    LocCaption = 'FindDialogOptions'
    Localizer = MainForm.Localizer
    object CaseCheckBox: TLocCheckBox
      Left = 9
      Top = 18
      Width = 312
      Height = 20
      Caption = 'Case sensitive'
      TabOrder = 0
      LocCaption = 'FindDialogCaseSensitive'
      Localizer = MainForm.Localizer
    end
    object WholeCheckBox: TLocCheckBox
      Left = 9
      Top = 46
      Width = 312
      Height = 20
      Caption = 'Search in source text'
      TabOrder = 1
      LocCaption = 'FindDialogSourceSearch'
      Localizer = MainForm.Localizer
    end
  end
  object OkButton: TLocButton
    Left = 174
    Top = 120
    Width = 75
    Height = 25
    Caption = #1054#1050
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OkButtonClick
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelButton: TLocButton
    Left = 257
    Top = 120
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
end
