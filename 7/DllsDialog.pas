unit DllsDialog;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Buttons, Dialogs,
  LocClasses, KruptarComponents, UnicodeUtils;

type
  TLibraryDialog = class(TLocForm)
    OkButton: TLocButton;
    Bevel: TBevel;
    ListBox: TListBox;
    Memo: TKruptarMemo;
    RefreshButton: TLocButton;
    DescriptionLabel: TLocLabel;
    procedure FormShow(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
    procedure RefreshButtonClick(Sender: TObject);
  end;

var
 LibraryDialog: TLibraryDialog;

implementation

Uses MainUnit, StringConsts, MyUtils;

{$R *.DFM}

procedure TLibraryDialog.FormShow(Sender: TObject);
begin
 If ListBox.Count > 0 then ListBox.ItemIndex := 0;
 ListBox.SetFocus;
 ListBoxClick(Sender);
end;

procedure TLibraryDialog.ListBoxClick(Sender: TObject);
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
 S: RawByteString; CP: LongWord;
begin
 Memo.Clear;
 with ListBox do
 begin
  if ItemIndex < 0 then Exit;
  with DLLs^.Get(Items[ItemIndex])^ do
  begin
   Lang := MainForm.Localizer.Language;
   if Lang <> NIL then
   begin
    Item := Lang.ValueItems[Name];
    if Item <> NIL then
    begin
     Memo.Text := Item.Value;
     Exit;
    end;
   end;
   if Addr(Description) <> NIL then
   begin
    S := Description;
    if (Length(S) >= 3) and
       (S[1] = #$EF) and (S[2] = #$BB) and (S[3] = #$BF) then
    begin
     Delete(S, 1, 3);
     CP := CP_UTF8;
    end else
     CP := GetACP;
    if S <> '' then
    begin
     Memo.Text := CodePageStringDecode(CP, S);
     Exit;
    end;
   end;
   Memo.Text := K7S(SDDNoDescription);
  end;
 end;
end;

procedure TLibraryDialog.RefreshButtonClick(Sender: TObject);
Var II: Integer;
begin
 If DLLs <> NIL then Dispose(DLLs, Done);
 II := ListBox.ItemIndex;
 ListBox.Clear;
 New(DLLs, Init);
 LoadDlls;
 DLLS^.GetByMethod(ListBox.Items);
 If (II >= 0) and (II < ListBox.Count) then ListBox.ItemIndex := II;
 ListBoxClick(Sender);
end;

end.
