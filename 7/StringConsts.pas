unit StringConsts;

interface

uses LocClasses;

const
 KP_SRC_ROM: PChar = 'kpSourceROM';
 KP_DST_ROM: PChar = 'kpDestinationROM';
 GR_ISDICT: PChar = 'grIsDictionary';
 PT_PUNTYPE: PChar = 'ptSplittedPtrs';
 PT_LOROM: PChar = 'ptSNESlorom'; 
 PT_SIGNED: PChar = 'ptAutoPtrStart';
 PT_MOTOROLA: PChar = 'ptBIG_ENDIAN';
 PT_AUTOSTART: PChar = 'ptAutoReference';
 PT_PTRSIZE: PChar = 'ptPointerSize';
 PT_DICT: PChar = 'ptDictionary';
 PT_SEEKSAME: PChar = 'ptSeekSame';
 PT_NOTINROM: PChar = 'ptNotInSource';
 PT_PTR2PTR: PChar = 'ptPtrToPtr';
 PT_SRC_TBL: PChar = 'ptSrcTable';
 PT_DST_TBL: PChar = 'ptDestTable';
 PN_SRC_TBL: PChar = 'pnSrcTable';
 PN_DST_TBL: PChar = 'pnDestTable';
 PN_INSERTER: PChar = 'pnInserter';
 PN_FIXED: PChar = 'pnFixed';
 PS_SIGNEDBYTE: PChar = '1 (signed)';
 PS_SIGNEDWORD: PChar = '2 (signed)';
 PS_SIGNEDINT: PChar = '4';
 SMUNone: PChar = '[None]';
 SMUSelf: PChar = '[Self]';
 SNoInputROM: PChar = 'No input ROM.';
 SInvalidFileFormat: PChar = 'Invalid file format.';
 SBadField: PChar = 'Bad field value.';
 sStringEnd = '{END}';
 sHexStart = '/'; 

const
 SWDCol1                          = 0;
 SWDCol2                          = 1;
 SMUIndexOutOfBounds              = 2;
 SMUIntegerError                  = 3;
 SMUHexError                      = 4;
 SMUInvalidTableItem              = 5;
 SMUTerminateStringAbsence        = 6;
 SMUFileNameFieldIsEmpty          = 7;
 SMUFileNotFound                  = 8;
 SMUFileNotFoundLoadOther         = 9;
 SMUFileNotFoundLoadOtherX        = 10;
 SMUFileNotFoundInProject         = 11;
 SMULibraryNotFound               = 12;
 SMUSuchTableIsExist              = 13;
 SMUSuchListIsExist               = 14;
 SMUSuchGroupIsExist              = 15;
 SMUInvalidString                 = 16;
 SMUInvalidHexadecimalValue       = 17;
 SMUInvalidIntegerValue           = 18;
 SMUTableExistIn                  = 19;
 SMUStringNotFound                = 20;
 SMUTableNotSelected              = 21;
 SMUStringTerminateCodeNotExist   = 22;
 SMUPointerIsExist                = 23;
 SMUStringIsExist                 = 24;
 SMUUnableToLoad                  = 25;
 SMUUnableToSave                  = 26;
 SMUTextStringNotFound            = 27;
 SMUSpaceLeft                     = 28;
 SMUBytesCount1                   = 29;
 SMUBytesCount2                   = 30;
 SMUBytesCount3                   = 31;
 SMUVersion                       = 32;
 SMUSize                          = 33;
 SMUDeleteTableConfirmation       = 34;
 SMUDeleteListConfirmation        = 35;
 SMUDeleteGroupConfirmation       = 36;
 SMUReloadConfirmation            = 37;
 SMUSaveBeforeClosingConfirmation = 38;
 SMUReplaceConfirmation           = 39;
 SAPFirstAddressLessThenZero      = 40;
 SAPSecondAddressLessThenZero     = 41;
 SAPSecondAddressLessThenFirst    = 42;
 SPDCloseConfirmation             = 43;
 SRTInputSearchText               = 44;
 SPDTotalInterval                 = 45;
 SPDValueIsExist                  = 46;
 SPDBytesOccupied                 = 47;
 SDDNoDescription                 = 48;
 SDD_BadStartValue                = 49;
 SLBLineNumber                    = 50;
 SSBInsert                        = 51;
 SSBOverwrite                     = 52;
 SMUOneWord                       = 53;
 SMUWordsCount1                   = 54;
 SMUWordsCount2                   = 55;
 SMUWordsCount3                   = 56;
 SMUWordsFounded                  = 57;
 SMUMaxWordsFound                 = 58;
 SMUWordSearching                 = 59;
 SMUWordIterationSearching        = 60;
 SMUAddTextBlock                  = 61;
 SMUAddPointersBlock              = 62;
 SMURomsFilter                    = 63;
 SMUExeFilter                     = 64;
 SESHint                          = 65;
 SESWarning                       = 66;
 SESError                         = 67;
 SESFatalError                    = 68;
 STVTables                        = 69;
 STVGroups                        = 70;
 STVTerminateCodes                = 71;
 STVCRLFCodes                     = 72;
 STVCharacters                    = 73;
 STVSpaceForText                  = 74;
 STVPointers                      = 75;
 smuClearConfirm                  = 76;
 smuLineIndex                     = 77;

const
 DefaultLang: array[0..77] of record N, V: String end =
 ((N: 'GeneratedDictDialogCol1'; V: 'Word'),
  (N: 'GeneratedDictDialogCol2'; V: 'Reference count'),
  (N: 'ValueIsOutOfBounds'; V: 'Value must be between %d and %d.'),
  (N: 'InvalidDecimalValue1'; V: 'Invalid decimal value.'),
  (N: 'InvalidHexadecimalValue1'; V: 'Invalid hexadecimal value.'),
  (N: 'InvalidLineInTable'; V: 'Invalid line in TBL.'),
  (N: 'NoStringTerminator1'; V: 'No string termination code.'),
  (N: 'FileNameFieldIsEmpty'; V: 'File name field ''%s'' is empty.'),
  (N: 'FileNotFound'; V: 'File ''%s'' not found.'),
  (N: 'FileNotFoundLoadConfirm'; V: 'File ''%s'' not found.'#13#10'Do you want to load another one?'),
  (N: 'FileNotFoundMustBeLoaded'; V: 'File ''%s'' not found.'#13#10'Please load another one.'),
  (N: 'FileNotFoundInProject'; V: 'File ''%s'' not found in the project file.'),
  (N: 'PluginNotFound'; V: 'Plugin ''%s'' not found.'),
  (N: 'TableIsAlreadyExist'; V: 'TBL with that name is already exist.'),
  (N: 'ListIsAlreadyExist'; V: 'Pointers list with that name is already exist.'),
  (N: 'GroupIsAlreadyExist'; V: 'Group with that name is already exist.'),
  (N: 'InvalidRangeItemInput'; V: 'Invalid text insert range input.'#13#10'Example: 002A3C00-002AFFFF.'),
  (N: 'InvalidHexadecimalValue2'; V: '''%s'' is invalid hexadecimal value.'),
  (N: 'InvalidDecimalValue2'; V: '''%s'' is invalid decimal value.'),
  (N: 'TableRemoveUnable'; V: 'Unable to remove TBL, because it is used in [%s].'),
  (N: 'StringNotFoundInList'; V: 'String ''%s'' is not in list.'),
  (N: 'TableNotSelected'; V: 'TBL is not selected.'),
  (N: 'NoStringTerminator2'; V: 'No string termination code in ''%s'' TBL.'),
  (N: 'PointerIsAlreadyExist'; V: 'Pointer at offset %x is already exist.'),
  (N: 'StringIsAlreadyExist'; V: 'String at offset %x is already exist.'),
  (N: 'LoadFileError'; V: 'Unable to load file ''%s''.'),
  (N: 'SaveFileError'; V: 'Unable to save file ''%s''.'),
  (N: 'SearchTextNotFound'; V: 'Search text is not found.'),
  (N: 'NotEnoughSpaceFor'; V: '[%s]: Not enough space for %d ($%x) %s.'),
  (N: 'BytesCount1'; V: 'byte'),
  (N: 'BytesCount2'; V: 'bytes'),
  (N: 'BytesCount3'; V: 'bytes'),
  (N: 'ProgramVersion'; V: 'Version: %s'),
  (N: 'ConvertedTextSize'; V: 'Size: %d'),
  (N: 'TableRemoveConfirm'; V: 'Are you really want to remove the TBL?'),
  (N: 'ListRemoveConfirm'; V: 'Are you really want to remove the pointers list?'),
  (N: 'GroupRemoveConfirm'; V: 'Are you really want to remove the group?'),
  (N: 'ProjectReloadConfirm'; V: 'Project ''%s'' is already loaded. Reload it?' + #13#10 +
                                'All modified data will be lost!'),
  (N: 'ProjectCloseConfirm'; V: 'Save ''%s'' before closing?'),
  (N: 'TextReplaceConfirm'; V: 'Replace this occurrence of ''%s''?'),
  (N: 'StartOffsetError'; V: 'Start offset is lower than zero.'),
  (N: 'FinishOffsetError'; V: 'Finish offset is lower than zero.'),
  (N: 'FinishOffsetError2'; V: 'Finish offset is lower than start offset.'),
  (N: 'CloseConfirm'; V: 'Are you sure you want to close?'#13#10'All modified data will be lost.'),
  (N: 'NoSearchText'; V: 'Type the text to find.'),
  (N: 'TotalInterval'; V: 'Total interval: %d'),
  (N: 'VarIsAlreadyExist'; V: 'Variable with a such name is already exist.'),
  (N: 'BytesUsed'; V: 'Bytes used: %d'),
  (N: 'PluginNoDescription'; V: 'No description.'),
  (N: 'InvalidDecimalValue2'; V: '''%s'' is invalid decimal value.'),
  (N: 'ListStringIndex'; V: 'Index %d of %d'),
  (N: 'InsertMode'; V: 'Insert'),
  (N: 'OverwriteMode'; V: 'Overwrite'),
  (N: 'OneWord'; V: '1 word'),
  (N: 'WordsCount1'; V: '%d word'),
  (N: 'WordsCount2'; V: '%d words'),
  (N: 'WordsCount3'; V: '%d words'),
  (N: 'WordsFound'; V: 'Words found: %d'),
  (N: 'MaxWordsStopConfirm'; V: 'Maximum words for the dictionary found. Stop the process?'),
  (N: 'WordSearching'; V: 'Searching word'),
  (N: 'SameWordsSearching'; V: 'Searching same words'),
  (N: 'AddPointersDialogTitle2'; V: 'Add Text Directly'),
  (N: 'AddPointersDialogTitle1'; V: 'Add Pointers'),
  (N: 'AnyFilesFilter'; V: 'Any files (*.*)|*.*'),
  (N: 'ExeFilesFilter'; V: 'Executable files (*.exe)|*.exe'),
  (N: 'MsgDlgInformation'; V: 'Information'),
  (N: 'MsgDlgWarning'; V: 'Warning'),
  (N: 'MsgDlgError'; V: 'Error'),
  (N: 'FatalError'; V: 'Fatal Error'),
  (N: 'MenuProjectTables'; V: 'Tables'),
  (N: 'MenuProjectGroups'; V: 'Groups'),
  (N: 'TableStringTerminators'; V: 'String Terminators'),
  (N: 'TableCRLFCodes'; V: 'CRLF Codes'),
  (N: 'TableCharacters'; V: 'Characters'),
  (N: 'TextInsertRange'; V: 'Text Insert Range'),
  (N: 'MenuProjectPointers'; V: 'Lists of Pointers'),
  (N: 'ClearConfirm'; V: 'Are you sure?'),
  (N: 'LineIndex'; V: 'Line number ')
 );

var
 ConstsLocalizer: TCustomLocalizer = NIL;
 SSBInsertV, SSBOverwriteV, SMUSizeV, SLBLineNumberV: WideString;

function K7S(Index: Integer): WideString;

implementation

function K7S(Index: Integer): WideString;
var
 Lang: TLanguageListItem;
 Item: TLanguageString;
begin
 with DefaultLang[Index] do
 begin
  if ConstsLocalizer <> NIL then
  begin
   Lang := ConstsLocalizer.Language;
   if Lang <> NIL then
   begin
    Item := Lang.ValueItems[N];
    if Item <> NIL then
    begin
     Result := Item.Value;
     Exit;
    end;
   end;
  end;
  Result := V;
 end;
end;

end.
