unit KruptarComponentsReg;

interface

procedure Register;

implementation

uses
  KruptarComponents, Classes, DesignIntf, DesignEditors;

procedure Register;
begin
  RegisterClass(TKruptarTabSheet);
  RegisterClass(TKruptarToolButton);
  RegisterClass(TKruptarTreeNode);
  RegisterClass(TKruptarMenuItem);
  RegisterCustomModule(TKruptarForm, TCustomModule);
  RegisterComponents('Kruptar Components',
  [TKruptarToolBar, TKruptarPanel, TKruptarMemo, TKruptarListBox, TKruptarEdit,
   TKruptarOpenDialog,
   TKruptarStatusBar, TKruptarTreeView, TKruptarMainMenu, TKruptarPopupMenu,
   TKruptarLabel, TKruptarPageControl, TKruptarSpeedButton, TKruptarComboBox,
   TKruptarListView, TKruptarTabControl, TKruptarButton, TKruptarImage]);
end;

end.
