program Kruptar7;

uses
  ShareMem,
  SysUtils,
  Windows,
  Forms,
  KruptarComponents in 'KruptarComponents.pas',  
  MainUnit in 'MainUnit.pas' {MainForm},
  k7HexUnit in 'k7HexUnit.pas',
  DllsDialog in 'DllsDialog.pas' {LibraryDialog},
  Needs in 'Needs.pas',
  AddPtrsDialog in 'AddPtrsDialog.pas' {AddPointersDialog},
  ParamDlg in 'ParamDlg.pas' {ParamDialog},
  Dynamics in 'Dynamics.pas',
  ProgressDialogUnit in 'ProgressDialogUnit.pas' {MyProgressDialog},
  DictDlg in 'DictDlg.pas' {DictionaryDialog},
  WordsDlg in 'WordsDlg.pas' {WordsDialog},
  SearchText in 'SearchText.pas' {SearchTextDialog},
  ReplaceText in 'ReplaceText.pas' {ReplaceTextDialog},
  StringConsts in 'StringConsts.pas',
  insertresultdlg in 'insertresultdlg.pas' {InsertResultDialog},
  about in 'about.pas' {AboutBox},
  LocMsgBox in 'LocMsgBox.pas',
  SEF in 'SEF.pas' {SEFORM: TLocForm};

{$R *.res}

function GetVersion: String;
var
 S: String; N, Len: LongWord; Buf: PChar; Value: PChar;
begin
 S := ParamStr(0);
 N := GetFileVersionInfoSize(Pointer(S), N);
 If N > 0 then
 begin
  Buf := AllocMem(N);
  GetFileVersionInfo(Pointer(S), 0, N, Buf);
  If VerQueryValue(Buf, PChar('StringFileInfo\040904E4\FileVersion'),
                        Pointer(Value), Len) then
   Result := Value Else
   Result := '7.0.0.0';
  FreeMem(Buf, N);
 end;
end;

procedure ConsWrite(Str: String);
{$IFNDEF FORCED_UNICODE}
Var P: PChar;
{$ENDIF}
begin
{$IFNDEF FORCED_UNICODE}
 P := Pointer(Str);
 While P^ <> #0 do
 begin
  Case P^ of
   #192..#239: Dec(Byte(P^), $40);
   #240..#255: Dec(Byte(P^), $10);
         #168: P^ := #240;
         #184: P^ := #241;
  end;
  Inc(P);
 end;
{$ENDIF}
 Write(Str);
end;

procedure ConsWriteln(Str: String);
{$IFNDEF FORCED_UNICODE}
Var P: PChar;
{$ENDIF}
begin
{$IFNDEF FORCED_UNICODE}
 P := Pointer(Str);
 While P^ <> #0 do
 begin
  Case P^ of
   #192..#239: Dec(Byte(P^), $40);
   #240..#255: Dec(Byte(P^), $10);
         #168: P^ := #240;
         #184: P^ := #241;
  end;
  Inc(P);
 end;
{$ENDIF}
 Writeln(Str);
end;

Procedure AddStateStringConsole
(ErrorType: TErrorType; const FileName: WideString;
 Line: Integer; const Txt: WideString; Mess: Boolean = False);
begin
//
end;
Var
 Project: PKruptarProject;

procedure ConsoleIncGroupProgress(IncValue: Integer);
begin
//
end;

procedure ConsoleProgressInsertError(const LName: WideString; Size: Integer);
begin
 ConsoleWriteln(Format('"%s" - not enough space for %u ($%x) %s.',
 [String(LName), Size, Size, DeclinateNumeralString(Size, 'byte',
                                                 'bytes',
                                                 'bytes')]));
end;

procedure ConsoleGroupProgressInit(const LName: WideString; MaxVal: Integer);
begin
 ConsoleWriteln(Format('Working with "%s" group...', [String(LName)]));
end;

procedure ConsoleIncPtrTableProgress(IncValue: Integer);
begin
//
end;

procedure ConsolePtrTableProgressInit(const LName: WideString; MaxVal: Integer);
begin
 ConsoleWriteln(Format('Inserting "%s" string list...', [String(LName)]));
end;

Procedure Insert;
Var
 G: PDynamic; B: PBlock; NewSize: Integer; F: File;
begin
 With Project^ do If Groups <> NIL then With Groups^ do
 begin
  If Count <= 0 then Exit;
  try
   NewSize := 0;
   G := Root;
   While G <> NIL do With G^, PGroup(Data)^ do
   begin
    B := grBlocks^.Root;
    While B <> NIL do With B^ do
    begin
     If blStart + blCount > NewSize then NewSize := blStart + blCount;
     B := Next;
    end;
    G := Next;
   end;
   if LoadRom(True, OutputRom, NewSize) then
   try
    G := Root;
    While G <> NIL do With G^, PGroup(Data)^ do
    begin
     B := grBlocks^.Root;
     While B <> NIL do With B^ do
     begin
      FillChar(ROM^[blStart], blCount, 0);
      B := Next;
     end;
     G := Next;
    end;
    G := Root;
    While G <> NIL do With G^ do
    begin
     GroupAddToRom(Data, Project);
     G := Next;
    end;
    AssignFile(F, OutputRom);
    Rewrite(F, 1);
    try
     BlockWrite(F, Rom^, RomSize);
    finally
     CloseFile(F);
    end;
   finally
    FreeRom;
   end;
  except
   ConsoleWriteln(Format('Unable to insert text to "%s".', [OutputRom]));
  end;
 end;
end;

Procedure InsertText(WriteProc: TWriteProc; WritelnProc: TWriteProc;
                     const ExePath, FileName: String);
Label Failed;
begin
 ConsoleWrite := WriteProc;
 ConsoleWriteln := WritelnProc;
 AddStateString := AddStateStringConsole;
 ConsoleWrite('Loading libraries... ');
 New(DLLs, Init);
 ExeDir := ExePath;
 LoadDLLs;
 try
  If DLLs.Count > 0 then
  begin
   ConsoleWriteln('Done.');
   ConsoleWrite(Format('Loading "%s"... ', [FileName]));
   New(Project, Init);
   try
    If Project.Open(FileName, True) then
    begin
     ConsoleWriteln('Done.');
     ConsoleWriteln('Inserting text...');
     GroupProgressInitProc := ConsoleGroupProgressInit;
     PtrTableProgressInitProc := ConsolePtrTableProgressInit;
     IncPtrTableProgressProc := ConsoleIncPtrTableProgress;
     IncGroupProgressProc := ConsoleIncGroupProgress;
     ProgressInsertErrorProc := ConsoleProgressInsertError;
     Insert;
    end else ConsoleWriteln('Failed.');
   finally
    Dispose(Project, Done);
   end;
  end else ConsoleWriteln('Failed.');
 finally
  Dispose(DLLs, Done);
 end;
end;

Procedure Usage;
begin
 If AllocConsole then
 begin
  SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
  Writeln(Format('Kruptar %s', [GetVersion]));
  Writeln('');
  Writeln('To open project from console, type:');
  Writeln(#9'kruptar7 ProjectFileName');
  Writeln('To insert text from project to output rom, type:');
  Writeln(#9'kruptar7 -i ProjectFileName');
  Readln;
  FreeConsole;
 end;
end;

Label StartProgram;
begin
 If ParamCount > 0 then
 begin
  If ParamStr(1) = '-i' then
  begin
   If AllocConsole then
   begin
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
    InsertText(ConsWrite, ConsWriteln,  ExtractFilePath(ParamStr(0)), ParamStr(2));
    FreeConsole;
   end;
  end else If ParamStr(1) = '-?' then Usage Else Goto StartProgram;
 end Else
 begin
  StartProgram:
  Application.Initialize;
  Application.Title := 'Kruptar 7';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TLibraryDialog, LibraryDialog);
  Application.CreateForm(TAddPointersDialog, AddPointersDialog);
  Application.CreateForm(TParamDialog, ParamDialog);
  Application.CreateForm(TMyProgressDialog, MyProgressDialog);
  Application.CreateForm(TDictionaryDialog, DictionaryDialog);
  Application.CreateForm(TWordsDialog, WordsDialog);
  Application.CreateForm(TSearchTextDialog, SearchTextDialog);
  Application.CreateForm(TReplaceTextDialog, ReplaceTextDialog);
  Application.CreateForm(TInsertResultDialog, InsertResultDialog);
  Application.CreateForm(TSEFORM, SEFORM);
  Application.Run;
 end;
end.
