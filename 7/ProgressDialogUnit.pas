unit ProgressDialogUnit;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Gauges, LocClasses,
  ActnList, LocActnList, KruptarComponents
  {$IFDEF FORCED_UNICODE}
  , System.Actions
  {$ENDIF};

type
  TMyProgressDialog = class(TForm)
    ProjectGauge: TGauge;
    GroupGauge: TGauge;
    PtrTableGauge: TGauge;
    PtrTableLabel: TKruptarLabel;
    ProjectLabel: TKruptarLabel;
    Panel: TKruptarPanel;
    GroupLabel: TKruptarLabel;
    CloseButton: TLocButton;
    AutoCloseCheck: TLocCheckBox;
    ProgressActions: TLocActionList;
    ProgressCloseAction: TLocAction;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ProgressCloseActionExecute(Sender: TObject);
    procedure ProgressCloseActionUpdate(Sender: TObject);
    procedure AutoCloseCheckClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
   protected
    fProjectProgress: Integer;
    fGroupProgress: Integer;
    fPtrTableProgress: Integer;
    procedure SetProjectProgress(X: Integer);
    procedure SetGroupProgress(X: Integer);
    procedure SetPtrTableProgress(X: Integer);
   public
    property ProjectProgress: Integer read fProjectProgress write SetProjectProgress;
    property GroupProgress: Integer read fGroupProgress write SetGroupProgress;
    property PtrTableProgress: Integer read fPtrTableProgress write SetPtrTableProgress;
  end;

var
 MyProgressDialog: TMyProgressDialog;
 ProgressCanClose: Boolean = True;
 ProgressAutoClose: Boolean = False;
 MForm: TForm;

implementation

uses MainUnit;

{$R *.DFM}

Procedure TMyProgressDialog.SetProjectProgress(X: Integer);
begin
 fProjectProgress := X;
 ProjectGauge.Progress := X;
 Application.ProcessMessages;
end;

Procedure TMyProgressDialog.SetGroupProgress(X: Integer);
begin
 fGroupProgress := X;
 GroupGauge.Progress := X;
 Application.ProcessMessages;
end;

Procedure TMyProgressDialog.SetPtrTableProgress(X: Integer);
begin
 fPtrTableProgress := X;
 PtrTableGauge.Progress := X;
 Application.ProcessMessages;
end;

procedure TMyProgressDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := ProgressCanClose;
end;

procedure TMyProgressDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 MForm.Enabled := True;
end;

procedure TMyProgressDialog.FormShow(Sender: TObject);
var
 H: Integer;
begin
 If not ProjectGauge.Visible then
  H := ProjectGauge.BoundsRect.Bottom - GroupGauge.BoundsRect.Bottom else
  H := 0;
 ClientHeight := 212 - H;
 AutoCloseCheck.Top := 184 - H;
 CloseButton.Top := 152 - H;
 AutoCloseCheck.SetFocus;
end;

procedure TMyProgressDialog.ProgressCloseActionExecute(Sender: TObject);
begin
 Close;
end;

procedure TMyProgressDialog.ProgressCloseActionUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := ProgressCanClose;
end;

procedure TMyProgressDialog.AutoCloseCheckClick(Sender: TObject);
begin
 ProgressAutoClose := AutoCloseCheck.Checked;
end;

procedure TMyProgressDialog.FormCreate(Sender: TObject);
begin
 AutoCloseCheck.Checked := ProgressAutoClose;
end;

end.
