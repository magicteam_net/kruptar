object MainForm: TMainForm
  Left = 220
  Top = 77
  Caption = 'Kruptar 7'
  ClientHeight = 578
  ClientWidth = 821
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TKruptarStatusBar
    Left = 0
    Top = 559
    Width = 821
    Height = 19
    AutoHint = True
    Panels = <>
  end
  object ToolBar: TKruptarToolBar
    Left = 0
    Top = 0
    Width = 821
    Height = 29
    ButtonWidth = 25
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Images = ImageList
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    object MenuFileNewButton: TKruptarToolButton
      Left = 0
      Top = 0
      Action = MenuFileNewAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuFileOpenButton: TKruptarToolButton
      Left = 25
      Top = 0
      Action = MenuFileOpenAction
      ParentShowHint = False
      ShowHint = True
    end
    object TntToolButton8: TKruptarToolButton
      Left = 50
      Top = 0
      Width = 8
      Caption = 'TntToolButton8'
      Style = tbsSeparator
    end
    object MenuFileSaveButton: TKruptarToolButton
      Left = 58
      Top = 0
      Action = MenuFileSaveAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuFileSaveAsButton: TKruptarToolButton
      Left = 83
      Top = 0
      Action = MenuFileSaveAsAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuFileSaveAllButton: TKruptarToolButton
      Left = 108
      Top = 0
      Action = MenuFileSaveAllAction
      ParentShowHint = False
      ShowHint = True
    end
    object Separator1: TKruptarToolButton
      Left = 133
      Top = 0
      Width = 8
      Caption = 'Separator1'
      ImageIndex = 19
      Style = tbsSeparator
    end
    object MenuProjectTableAddButton: TKruptarToolButton
      Left = 141
      Top = 0
      Action = MenuProjectTableAddAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuProjectGroupAddButton: TKruptarToolButton
      Left = 166
      Top = 0
      Action = MenuProjectGroupAddAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuProjectItemAddButton: TKruptarToolButton
      Left = 191
      Top = 0
      Action = MenuProjectItemAddAction
      ParentShowHint = False
      ShowHint = True
    end
    object TntToolButton7: TKruptarToolButton
      Left = 216
      Top = 0
      Action = MenuProjectPointersAddAction
    end
    object TntToolButton15: TKruptarToolButton
      Left = 241
      Top = 0
      Action = MenuProjectItemsAddFromFileAction
    end
    object TntToolButton14: TKruptarToolButton
      Left = 266
      Top = 0
      Width = 8
      Caption = 'TntToolButton14'
      Style = tbsSeparator
    end
    object MenuProjectItemUpButton: TKruptarToolButton
      Left = 274
      Top = 0
      Action = MenuProjectItemUpAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuProjectItemDownButton: TKruptarToolButton
      Left = 299
      Top = 0
      Action = MenuProjectItemDownAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuProjectItemRemoveButton: TKruptarToolButton
      Left = 324
      Top = 0
      Action = MenuProjectItemRemoveAction
      ParentShowHint = False
      ShowHint = True
    end
    object Separator4: TKruptarToolButton
      Left = 349
      Top = 0
      Width = 8
      Caption = 'Separator4'
      ImageIndex = 19
      Style = tbsSeparator
    end
    object MenuEditLoadButton: TKruptarToolButton
      Left = 357
      Top = 0
      Action = MenuEditLoadAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuEditSaveButton: TKruptarToolButton
      Left = 382
      Top = 0
      Action = MenuEditSaveAction
      ParentShowHint = False
      ShowHint = True
    end
    object Separator3: TKruptarToolButton
      Left = 407
      Top = 0
      Width = 8
      Caption = 'Separator3'
      ImageIndex = 19
      Style = tbsSeparator
    end
    object TntToolButton11: TKruptarToolButton
      Left = 415
      Top = 0
      Action = MenuProjectGroupTextReloadAction
    end
    object TntToolButton12: TKruptarToolButton
      Left = 440
      Top = 0
      Action = MenuProjectGroupTextResetAction
    end
    object TntToolButton9: TKruptarToolButton
      Left = 465
      Top = 0
      Width = 8
      Caption = 'TntToolButton9'
      Style = tbsSeparator
    end
    object TntToolButton1: TKruptarToolButton
      Left = 473
      Top = 0
      Action = MenuEditUndoAction
    end
    object TntToolButton10: TKruptarToolButton
      Left = 498
      Top = 0
      Width = 8
      Caption = 'TntToolButton10'
      Style = tbsSeparator
    end
    object TntToolButton3: TKruptarToolButton
      Left = 506
      Top = 0
      Action = MenuEditCutAction
    end
    object TntToolButton4: TKruptarToolButton
      Left = 531
      Top = 0
      Action = MenuEditCopyAction
    end
    object TntToolButton5: TKruptarToolButton
      Left = 556
      Top = 0
      Action = MenuEditPasteAction
    end
    object TntToolButton6: TKruptarToolButton
      Left = 581
      Top = 0
      Width = 8
      Caption = 'TntToolButton6'
      Style = tbsSeparator
    end
    object MenuEditClearButton: TKruptarToolButton
      Left = 589
      Top = 0
      Action = MenuEditClearAction
    end
    object TntToolButton2: TKruptarToolButton
      Left = 614
      Top = 0
      Width = 8
      Caption = 'TntToolButton2'
      Style = tbsSeparator
    end
    object MenuProjectGroupTextInsertButton: TKruptarToolButton
      Left = 622
      Top = 0
      Action = MenuProjectGroupTextInsertAction
      ParentShowHint = False
      ShowHint = True
    end
    object MenuProjectTextInsertButton: TKruptarToolButton
      Left = 647
      Top = 0
      Action = MenuProjectTextInsertAction
      ParentShowHint = False
      ShowHint = True
    end
    object Separator5: TKruptarToolButton
      Left = 672
      Top = 0
      Width = 8
      Caption = 'Separator5'
      ImageIndex = 16
      Style = tbsSeparator
    end
    object MenuProjectRunRom1Button: TKruptarToolButton
      Left = 680
      Top = 0
      Action = MenuProjectRunRom1Action
    end
    object MenuProjectRunRom2Button: TKruptarToolButton
      Left = 705
      Top = 0
      Action = MenuProjectRunRom2Action
    end
  end
  object ProjectTabs: TKruptarTabControl
    Left = 0
    Top = 29
    Width = 821
    Height = 530
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    TabStop = False
    OnChange = ProjectTabsChange
    object Panel: TKruptarPanel
      Left = 237
      Top = 6
      Width = 580
      Height = 520
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object GroupsTabControl: TKruptarTabControl
        Left = 0
        Top = 0
        Width = 580
        Height = 212
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = GroupsTabControlChange
        OnMouseDown = GroupsTabControlMouseDown
        object PTablesTabControl: TKruptarTabControl
          Left = 4
          Top = 6
          Width = 572
          Height = 202
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          TabPosition = tpBottom
          OnChange = PTablesTabControlChange
          OnMouseDown = PTablesTabControlMouseDown
          object ListBox: TKruptarListBox
            Left = 4
            Top = 4
            Width = 564
            Height = 175
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial Unicode MS'
            Font.Style = []
            ItemHeight = 18
            MultiSelect = True
            ParentFont = False
            PopupMenu = TextEditPopup
            TabOrder = 0
            OnClick = ListBoxClick
            OnDblClick = ListBoxDblClick
            OnMouseDown = ListBoxMouseDown
          end
          object ListStatusBar: TKruptarStatusBar
            Left = 4
            Top = 179
            Width = 564
            Height = 19
            Panels = <
              item
                Width = 100
              end>
          end
        end
      end
      object BottomPanel: TKruptarPanel
        Left = 0
        Top = 212
        Width = 580
        Height = 308
        Align = alBottom
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object StatesMemo: TKruptarMemo
          Left = 0
          Top = 232
          Width = 580
          Height = 76
          Align = alBottom
          BevelInner = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          PopupMenu = StatesPopup
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 2
          Visible = False
        end
        object OriginalPanel: TKruptarPanel
          Left = 0
          Top = 0
          Width = 294
          Height = 232
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object OriginalMemo: TKruptarMemo
            Left = 0
            Top = 0
            Width = 294
            Height = 213
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            HideSelection = False
            ParentFont = False
            PopupMenu = TextEditPopup
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
          end
          object OriginalStatusBar: TKruptarStatusBar
            Left = 0
            Top = 213
            Width = 294
            Height = 19
            Panels = <
              item
                Width = 70
              end
              item
                Width = 70
              end
              item
                Width = 70
              end>
          end
        end
        object TranslatePanel: TKruptarPanel
          Left = 294
          Top = 0
          Width = 286
          Height = 232
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object TranslateMemo: TKruptarMemo
            Left = 0
            Top = 0
            Width = 286
            Height = 213
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Courier New'
            Font.Style = []
            HideSelection = False
            ParentFont = False
            PopupMenu = TextEditPopup
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 0
            OnChange = TranslateMemoChange
            OnKeyDown = TranslateMemoKeyDown
            OnKeyPress = TranslateMemoKeyPress
          end
          object TranslateStatusBar: TKruptarStatusBar
            Left = 0
            Top = 213
            Width = 286
            Height = 19
            Panels = <
              item
                Width = 70
              end
              item
                Width = 70
              end
              item
                Width = 70
              end
              item
                Width = 70
              end>
          end
        end
      end
    end
    object LeftPanel: TKruptarPanel
      Left = 4
      Top = 6
      Width = 233
      Height = 520
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object TreeView: TKruptarTreeView
        Left = 0
        Top = 0
        Width = 233
        Height = 185
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        HideSelection = False
        Images = ImageList
        Indent = 19
        MultiSelectStyle = [msControlSelect, msShiftSelect, msSiblingOnly]
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 0
        OnChange = TreeViewChange
        OnClick = TreeViewClick
        OnCollapsing = TreeViewCollapsing
        OnDblClick = TreeViewDblClick
        OnEdited = TreeViewEdited
        OnKeyDown = TreeViewKeyDown
        OnMouseDown = TreeViewMouseDown
      end
      object ValuePalette: TKruptarPanel
        Left = 0
        Top = 185
        Width = 233
        Height = 335
        Align = alBottom
        TabOrder = 1
        object ValueListBox: TValueListEditor
          Left = 1
          Top = 1
          Width = 231
          Height = 333
          Align = alClient
          Color = clBtnFace
          DefaultColWidth = 98
          DisplayOptions = [doAutoColResize]
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [keyUnique]
          Options = [goVertLine, goHorzLine, goThumbTracking]
          ParentFont = False
          ParentShowHint = False
          ScrollBars = ssVertical
          ShowHint = False
          TabOrder = 0
          Visible = False
          OnClick = ValueListBoxClick
          OnEditButtonClick = ValueListBoxEditButtonClick
          OnExit = ValueListBoxExit
          OnKeyDown = ValueListBoxKeyDown
          OnKeyPress = ValueListBoxKeyPress
          OnSelectCell = ValueListBoxSelectCell
          ColWidths = (
            98
            127)
        end
      end
    end
  end
  object MainMenu: TKruptarMainMenu
    Images = ImageList
    Left = 449
    Top = 44
    object FileMenu: TKruptarMenuItem
      Action = MenuFileAction
      object MenuFileNewItem: TKruptarMenuItem
        Action = MenuFileNewAction
      end
      object MenuFileOpenItem: TKruptarMenuItem
        Action = MenuFileOpenAction
      end
      object FileSeparator1: TKruptarMenuItem
        Caption = '-'
      end
      object MenuFileSaveItem: TKruptarMenuItem
        Action = MenuFileSaveAction
      end
      object MenuFileSaveAsItem: TKruptarMenuItem
        Action = MenuFileSaveAsAction
      end
      object MenuFileSaveAllItem: TKruptarMenuItem
        Action = MenuFileSaveAllAction
      end
      object MenuFileCloseItem: TKruptarMenuItem
        Action = MenuFileCloseAction
      end
      object MenuFileCloseAllItem: TKruptarMenuItem
        Action = MenuFileCloseAllAction
      end
      object FileSeparator2: TKruptarMenuItem
        Caption = '-'
        Visible = False
      end
      object FileRecent1Item: TKruptarMenuItem
        Tag = 1
        Caption = '?'
        Enabled = False
        Visible = False
        OnClick = FileRecentItemClick
      end
      object FileRecent2Item: TKruptarMenuItem
        Tag = 2
        Caption = '?'
        Enabled = False
        Visible = False
        OnClick = FileRecentItemClick
      end
      object FileRecent3Item: TKruptarMenuItem
        Tag = 3
        Caption = '?'
        Enabled = False
        Visible = False
        OnClick = FileRecentItemClick
      end
      object FileRecent4Item: TKruptarMenuItem
        Tag = 4
        Caption = '?'
        Enabled = False
        Visible = False
        OnClick = FileRecentItemClick
      end
      object FileSeparator3: TKruptarMenuItem
        Caption = '-'
      end
      object FileExitItem: TKruptarMenuItem
        Action = MenuFileExitAction
      end
    end
    object EditMenu: TKruptarMenuItem
      Action = MenuEditAction
      object MenuProjectTableLoadItem: TKruptarMenuItem
        Action = MenuEditLoadAction
      end
      object MenuProjectTableSaveItem: TKruptarMenuItem
        Action = MenuEditSaveAction
      end
      object N8: TKruptarMenuItem
        Caption = '-'
      end
      object MenuEditUndoItem: TKruptarMenuItem
        Action = MenuEditUndoAction
      end
      object EditSeparator1: TKruptarMenuItem
        Caption = '-'
      end
      object MenuEditCutItem: TKruptarMenuItem
        Action = MenuEditCutAction
      end
      object MenuEditCopyItem: TKruptarMenuItem
        Action = MenuEditCopyAction
      end
      object MenuEditPasteItem: TKruptarMenuItem
        Action = MenuEditPasteAction
      end
      object EditSeparator2: TKruptarMenuItem
        Caption = '-'
      end
      object MenuEditSelectAllItem: TKruptarMenuItem
        Action = MenuEditSelectAllAction
      end
      object EditSeparator3: TKruptarMenuItem
        Caption = '-'
      end
      object MenuProjectPointersClearItem: TKruptarMenuItem
        Action = MenuEditClearAction
      end
    end
    object SearchMenu: TKruptarMenuItem
      Action = MenuSearchAction
      object MenuSearchFindItem: TKruptarMenuItem
        Action = MenuSearchFindAction
      end
      object MenuSearchFindReplaceItem: TKruptarMenuItem
        Action = MenuSearchFindReplaceAction
      end
      object MenuSearchFindNextItem: TKruptarMenuItem
        Action = MenuSearchFindNextAction
      end
    end
    object ProjectMenu: TKruptarMenuItem
      Action = MenuProjectAction
      object MenuProjectTablesItem: TKruptarMenuItem
        Action = MenuProjectTablesAction
        object MenuProjectTableAddItem: TKruptarMenuItem
          Action = MenuProjectTableAddAction
        end
        object MenuProjectTableAnsiFillItem: TKruptarMenuItem
          Action = MenuProjectTableAnsiFillAction
        end
      end
      object MenuProjectGroupsItem: TKruptarMenuItem
        Action = MenuProjectGroupsAction
        object MenuProjectGroupAddItem: TKruptarMenuItem
          Action = MenuProjectGroupAddAction
        end
        object GroupsSeparator1: TKruptarMenuItem
          Caption = '-'
        end
        object MenuProjectGroupTextReloadItem: TKruptarMenuItem
          Action = MenuProjectGroupTextReloadAction
        end
        object MenuProjectGroupTextResetItem: TKruptarMenuItem
          Action = MenuProjectGroupTextResetAction
        end
        object GroupsSeparator2: TKruptarMenuItem
          Caption = '-'
        end
        object MenuProjectGroupDictOptionsItem: TKruptarMenuItem
          Action = MenuProjectGroupDictOptionsAction
        end
        object MenuProjectGroupDictGenerateItem: TKruptarMenuItem
          Action = MenuProjectGroupDictGenerateAction
        end
        object MenuProjectGroupDictToTableItem: TKruptarMenuItem
          Action = MenuProjectGroupDictToTableAction
        end
        object GroupsSeparator3: TKruptarMenuItem
          Caption = '-'
        end
        object MenuProjectPointersItem: TKruptarMenuItem
          Action = MenuProjectPointersAction
          object MenuProjectPointersAddItem: TKruptarMenuItem
            Action = MenuProjectPointersAddAction
          end
          object PointersSeparator1: TKruptarMenuItem
            Caption = '-'
          end
          object MenuProjectPointersAddInsertInfoMenuItem: TKruptarMenuItem
            Action = MenuProjectPointersToggleInsertFieldsAction
          end
          object FixPointersPositions2: TKruptarMenuItem
            Action = MenuProjectPointerFixPointersPositionsAction
          end
          object MenuProjectPointersTableLinksResetItem: TKruptarMenuItem
            Action = MenuProjectPointersTableLinksResetAction
          end
          object PointersSeparator2: TKruptarMenuItem
            Caption = '-'
          end
          object MenuProjectPointersVarOptionsItem: TKruptarMenuItem
            Action = MenuProjectPointersVarOptionsAction
          end
          object PointersSeparator3: TKruptarMenuItem
            Caption = '-'
          end
          object AscendingSortMenuItem: TKruptarMenuItem
            Action = PointersSortAscAction
          end
          object DescendingSortMenuItem: TKruptarMenuItem
            Action = PointersSortDescAction
          end
        end
        object GroupsSeparator4: TKruptarMenuItem
          Caption = '-'
        end
        object MenuProjectGroupTextInsertItem: TKruptarMenuItem
          Action = MenuProjectGroupTextInsertAction
        end
      end
      object ProjectSeparator1: TKruptarMenuItem
        Caption = '-'
      end
      object MenuProjectItemAddItem: TKruptarMenuItem
        Action = MenuProjectItemAddAction
      end
      object AddFromFile1: TKruptarMenuItem
        Action = MenuProjectItemsAddFromFileAction
      end
      object N3: TKruptarMenuItem
        Caption = '-'
      end
      object MenuProjectItemUpItem: TKruptarMenuItem
        Action = MenuProjectItemUpAction
      end
      object MenuProjectItemDownItem: TKruptarMenuItem
        Action = MenuProjectItemDownAction
      end
      object MenuProjectItemRemoveItem: TKruptarMenuItem
        Action = MenuProjectItemRemoveAction
      end
      object ProjectSeparator2: TKruptarMenuItem
        Caption = '-'
      end
      object MenuProjectTextInsertItem: TKruptarMenuItem
        Action = MenuProjectTextInsertAction
      end
      object ProjectSeparator3: TKruptarMenuItem
        Caption = '-'
      end
      object MenuProjectEmulatorSelectItem: TKruptarMenuItem
        Action = MenuProjectEmulatorSelectAction
      end
      object MenuProjectRunRom1Item: TKruptarMenuItem
        Action = MenuProjectRunRom1Action
      end
      object MenuProjectRunRom2Item: TKruptarMenuItem
        Action = MenuProjectRunRom2Action
      end
    end
    object OptionsMenu: TKruptarMenuItem
      Action = MenuOptionsAction
      object LanguageMenu: TKruptarMenuItem
        Action = MenuLanguageAction
      end
      object LanguageMenuSeparator: TKruptarMenuItem
        Caption = '-'
      end
      object Font1: TKruptarMenuItem
        Action = MenuOptionsFontAction
      end
    end
    object HelpMenu: TKruptarMenuItem
      Action = MenuHelpAction
      object MenuHelpPluginsItem: TKruptarMenuItem
        Action = MenuHelpPluginsAction
      end
      object HelpSeparator1: TKruptarMenuItem
        Caption = '-'
      end
      object MenuHelpAboutItem: TKruptarMenuItem
        Action = MenuHelpAboutAction
      end
    end
  end
  object ImageList: TImageList
    Left = 489
    Top = 40
    Bitmap = {
      494C0101250027003C0010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000A0000000010020000000000000A0
      00000000000000000000000000000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD867B00DECFBD00F7EF
      E700000000000000000000000000000000000000000000000000000000000000
      000000000000FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58E7B00DECFC6000000
      00000086840000868400000000000000000000000000C6C7C600000000000086
      840000000000FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD8E7B00E7D7CE000000
      00000086840000868400000000000000000000000000C6C7C600000000000086
      840000000000FFDFBD0094615A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6968400E7D7CE000000
      0000008684000086840000000000000000000000000000000000000000000086
      840000000000FFDFBD0094616300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6968400EFDFCE000000
      0000008684000086840000868400008684000086840000868400008684000086
      840000000000FFDFBD009C696300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9E8400EFDFD6000000
      0000008684000086840000000000000000000000000000000000008684000086
      840000000000FFDFBD009C696300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9E8400EFE7DE000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600000000000086
      840000000000FFDFBD00A5716B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A68C00EFE7DE000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600000000000000
      000000000000FFDFBD00AD716B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEAE8C00F7E7DE000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C60000000000C6C7
      C60000000000FFDFBD00AD797300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7B68C00F7EFE7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFA6A500AD797300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B5867300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000000000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084868400008684008486
      8400008684008486840084000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000868400848684000086
      8400848684000086840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084868400008684008486
      8400008684008486840084000000FFFFFF00000000000000000000000000FFFF
      FF00840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000868400848684000086
      8400848684000086840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000000000000084000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084868400008684008486
      8400008684008486840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000008400000000000000000000000000000000000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000FFFF
      FF00840000008400000084000000840000000000000000868400848684000086
      8400848684000086840084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000084868400008684008486
      8400008684008486840000868400848684000086840084868400008684008486
      8400008684000000000000000000000000000000000000000000840000008400
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000008400000000000000000000000000000000868400848684000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400848684000000000000000000000000000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000FFFFFF0000000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000084868400848684000000
      0000000000000000000000000000000000000000000000000000000000008486
      8400008684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000868400848684000086
      84000000000000FFFF00000000000000000000FFFF0000000000848684000086
      8400848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300000000000000
      000000000000A5797300A5797300F7AE7B00A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797B000000000000FF
      000000000000FFCFC600FFCFC600FFCFC600EFC7AD00FF9E5A00FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000F7EFE700FFBE7300FFC77B00F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000FFC77B00FFB67300A5865200FFFFFF00FFFF
      FF00F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000FFC77B00ADCFBD0031308C00CED7CE00FFFF
      FF00F7EFE700FFDFBD0094615A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C69684000000000000FF
      000000000000FFBE8400EFBE7300E7AE84006B969C00F79E5200F79E6300F7C7
      AD00F7EFE700FFDFBD0094616300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084000000000000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000000000000000000000000000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6968400000000000000
      000000000000BD8652009C795A0084AEA500EF966300F7A66B00B5865200A58E
      6B00F7EFE700FFDFBD009C696300000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008486840000000000000000000000000084868400000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000000000000000000000000000C6C7
      C6000000000000000000000000000000000000000000CE9E8400EFDFD600F7EF
      E700B586520094694A007B306300FFA66B00FFA6630063306300C68652008461
      4200F7EFE700FFDFBD009C696300000000000000000000000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840000000000000000000000000084868400000000000000
      00000000000000000000000000000000000000000000CE9E8400EFE7DE00F7EF
      E700BD864A00210063007B968400E7966300DE8E5A002961940052616B00A571
      5200F7EFE700FFDFBD00A5716B00000000000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      000000000000840000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000C6C7C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A68C00EFE7DE00FFF7
      F700FFFFFF00B5A6B500BDAE9400C68E5A00E79663009CB6BD00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00AD716B00000000000000000000000000840000008400
      0000000000008400000000000000000000000000000000000000000000000000
      000000000000840000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000C6C7C60000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000C6C7C60000000000000000000000
      00000000000000000000000000000000000000000000DEAE8C00F7E7DE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFAE6B00C68E5200FFFFFF00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00AD797300000000000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C7
      C60000000000000000000000000000000000C6C7C60000000000000000000000
      00000000000000000000000000000000000000000000E7B68C00F7EFE700FFFF
      FF00FFFFFF00FFFFFF00A5967B00F7A66B00FFAE73007BAEB500FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD797300000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848684000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00BD866B00FFBE7300EFAE6B00C6A68C00FFF7EF00B586
      7300B5867300B5867300B5867300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848684000000000000000000000000000000
      00000000000000000000000000000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00D6BEB500A5BECE00DECFC600AD9E8C00FFFFF700B586
      7300FFC77B00DEA67300CEAEA500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00948E840094969400738E8C00BDB6B500F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300DEC7BD00FFCF
      C600AD9E9400AD9E9400B5A69C00AD9E9400FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300DECFBD009C8E
      8400CE8E8C00E7CFCE00EFD7D600DEB6B500B5A68C00B5AEA500AD9E94009C8E
      8C00F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300A58E8400C686
      8400C6797B00E7C7C600EFDFDE00DEBEBD00299E210084A66B00D6A6A500D6A6
      A500D6A6A500B5AEA5009C696300000000000000000000000000000000000000
      0000008684000086840000000000000000000000000000000000C6C7C6000000
      0000008684000000000000000000000000000000000063CFCE0063CFCE0063CF
      CE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CF
      CE0063CFCE0063CFCE00000000000000000000000000FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF000000000000000000000000000000000000000000AD797300C6797B007B49
      84007B498400DEB6B500EFDFDE00DEBEBD00CEA6A500189E100084A66B00D6A6
      A500D6A6A500D6AEAD009C696300000000000000000000000000000000000000
      0000008684000086840000000000000000000000000000000000C6C7C6000000
      00000086840000000000000000000000000000000000CEFFFF0094CFFF0094FF
      FF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094CF
      FF0094CFFF0063CFCE00000000000000000000000000FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF000000000000000000000000000000000000000000AD797300C6797B007B49
      8400C6797B00D69E9C00EFD7D600DEC7C600D6AEAD00CEA6A500109E0800B5A6
      8C00429E3100D6AEAD009C696300000000000000000000000000000000000000
      0000008684000086840000000000000000000000000000000000000000000000
      00000086840000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CF
      FF0094CFFF0063CFCE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300312094004228
      8C00C6797B00CE8E8C00EFD7D600E7C7C600D6AEAD00D6A6A50094A67300009E
      0000109E0800DEB6B5009C696300000000000000000000000000000000000000
      0000008684000086840000868400008684000086840000868400008684000086
      84000086840000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE0000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000AD797300BD797B003920
      9400C6797B00C6868400E7CFCE00E7C7C600D6AEAD00D6A6A5005A9E4A00219E
      1800009E0000E7C7C6009C696300000000000000000000000000000000000000
      0000008684000086840000000000000000000000000000000000000000000086
      84000086840000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CF
      FF0094FFFF0063CFCE0000000000000000000000000000000000FFFFFF000000
      00000000000000000000C6C7C6000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000CE9E8400C68684004228
      8C00C6797B00C6797B00DEB6B500E7CFCE00DEBEBD00DEB6B500D6A6A500D6A6
      A500D6AEAD00EFD7D600A5716B00000000000000000000000000000000000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000086840000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE0000000000000000000000000000000000FFFFFF000000
      00000000000000000000C6C7C6000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000D6A68C00D69E9C008C51
      8400C6868400D6A6A500EFDFDE00E7C7C600DEAEAD00DEAEAD00E7C7C600E7BE
      BD00F7E7E700FFDFBD00AD716B00000000000000000000000000000000000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000086840000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094CFFF0094FFFF0094CF
      FF0094FFFF0063CFCE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEAE8C00F7E7DE00E7BE
      BD00E7BEBD00E7C7C600D68E4A00CE610000CE610000D68E6B00CE969400B58E
      8400F7EFE700FFDFBD00AD797300000000000000000000000000000000000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      00000000000000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FF
      FF0094CFFF0063CFCE000000000000000000000000000000000000000000FFFF
      FF000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000000000000000000000000000E7B68C00F7EFE700FFFF
      FF00FFFFFF00D6BEBD00DECFCE00D6C7C600D6BEBD00B5AEA5009C8E8C00FFE7
      D600FFBEBD00FFA6A500AD797300000000000000000000000000000000000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      0000C6C7C60000000000000000000000000000000000CEFFFF00CEFFFF00CEFF
      FF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFF
      FF0094FFFF0063CFCE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B5867300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFEFEF00CEFF
      FF00CEFFFF0094FFFF0094FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD867B00DECFBD00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000086
      840000000000000000000000000000000000C6C7C60000000000008684000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000636163006361630063616300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58E7B00DECFC600F7EF
      E700FFE7CE00FFE7CE00FFE7C600FFDFBD00FFDFBD00FFDFB500FFDFB500FFD7
      AD00F7EFE700FFDFBD008C595A000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C6000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000000000000000000000008684000086
      840000000000000000000000000000000000C6C7C60000000000008684000000
      000000000000000000000000000000000000000000000000000000000000AD9E
      9400AD9E9400B5A69C00AD9E9400636163006361630063616300636163000000
      00000000000000000000000000000000000000000000BD8E7B00E7D7CE000000
      000000000000000000000000000000000000000000000000000000000000FFCF
      9C00F7EFE700FFDFBD0094615A000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000008684000000
      00000086840000000000000000000000000000000000000000009C8E8400CE8E
      8C00E7CFCE00EFD7D600DEB6B500B5A68C00B5AEA500AD9E94009C8E8C006361
      63006361630063616300636163000000000000000000C6968400E7D7CE000000
      0000000000000086840000868400008684000086840000868400008684000000
      0000F7EFE700FFDFBD00946163000000000000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000086
      8400008684000086840000868400008684000086840000868400008684000000
      00000086840000000000000000000000000000000000A58E8400C6868400C679
      7B00E7C7C600EFDFDE00DEBEBD00299E210084A66B00D6A6A500D6A6A500D6A6
      A500B5AEA500E7C7C600636163000000000000000000C6968400EFDFCE000000
      000000FFFF000000000000868400008684000086840000868400008684000086
      840000000000FFDFBD009C6963000000000000000000FFFFFF00FFFFFF000000
      0000000000000086840000868400008684000086840000868400008684000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000868400008684000000
      000000868400000000000086840000000000B59E9400C6797B007B4984007B49
      8400DEB6B500EFDFDE00DEBEBD00CEA6A500189E100084A66B00D6A6A500D6A6
      A500D6AEAD00E7C7C600636163000000000000000000CE9E8400EFDFD6000000
      0000FFFFFF0000FFFF0000000000008684000086840000868400008684000086
      840000868400000000009C6963000000000000000000FFFFFF00FFFFFF000000
      000000FFFF000000000000868400008684000086840000868400008684000086
      840000000000FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C60000000000008684000000
      000000868400000000000086840000000000C6797B00C6797B007B498400C679
      7B00D69E9C00EFD7D600DEC7C600D6AEAD00CEA6A500109E0800B5A68C00429E
      3100D6AEAD00DEAEAD00636163000000000000000000CE9E8400EFE7DE000000
      000000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C60000000000008684000000
      000000868400000000000086840000000000A57194003120940042288C00C679
      7B00CE8E8C00EFD7D600E7C7C600D6AEAD00D6A6A50094A67300009E0000109E
      0800DEB6B500D69E9C00636163000000000000000000D6A68C00EFE7DE000000
      0000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFDF
      BD00F7EFE700FFDFBD00AD716B000000000000000000FFFFFF00FFFFFF000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C60000000000000000000000
      000000868400000000000086840000000000C6797B00BD797B0039209400C679
      7B00C6868400E7CFCE00E7C7C600D6AEAD00D6A6A5005A9E4A00219E1800009E
      0000E7C7C60063616300000000000000000000000000DEAE8C00F7E7DE000000
      000000FFFF00FFFFFF000000000000000000000000000000000000000000FFCF
      9C00F7EFE700FFDFBD00AD7973000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C60000000000C6C7C6000000
      00000086840000000000008684000000000000000000C686840042288C00C679
      7B00C6797B00DEB6B500E7CFCE00DEBEBD00DEB6B500D6A6A500D6A6A500D6AE
      AD00EFD7D60063616300000000000000000000000000E7B68C00F7EFE700FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD7973000000000000000000FFFFFF00FFFFFF00FFFF
      FF008486840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000008684000000000000000000D69E9C008C518400C686
      8400D6A6A500EFDFDE00E7C7C600DEAEAD00DEAEAD00E7C7C600E7BEBD00F7E7
      E7006361630000000000000000000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B58673000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084868400C6C7C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084868400C6C7C60000000000000000000000000000000000000000000000
      00000086840000000000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C6000000
      0000C6C7C6000000000000868400000000000000000000000000E7BEBD00E7BE
      BD00E7C7C600D68E4A00CE610000CE610000D68E6B00CE969400B58E84006361
      63000000000000000000000000000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA5000000000000000000FFFFFF00FFFFFF00FFFF
      FF008486840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6BEBD00DECFCE00D6C7C600D6BEBD00B5AEA5009C8E8C00000000000000
      00000000000000000000000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000086840000000000C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C60000000000C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008080800080808000808
      0800424100004241000042410000000000000808080000000000424100000000
      00000000000000000000080808000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A000000000000000000AD797300000000000000
      000000000000A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008080800000000000808
      0800424100000000000042410000000000000808080000000000424100004241
      00004241000000000000080808000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A000000000000000000AD797B000000000000FF
      000000000000FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008080800000000000808
      0800424100000000000042410000000000000808080000000000424100000000
      00004241000000000000080808000000000000000000AD867B00DECFBD00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A000000000000000000000000000000000000FF
      0000000000000000000000000000F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008080800080808000808
      0800424100000000000042410000080808000808080008080800424100004241
      00004241000008080800080808000808080000000000B58E7B00DECFC6000000
      000000000000FFE7CE00FFE7C600FFDFBD00FFDFBD00FFDFB500FFDFB500FFD7
      AD00F7EFE700FFDFBD008C595A00000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000FFDFBD00FFDFBD00FFDFB500FFDFB500FFD7
      AD00F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B797B007B797B007B797B007B797B007B797B007B797B000000
      00000000000000000000000000000000000000000000BD8E7B00E7D7CE000000
      00000000000000000000FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C000000
      0000F7EFE700FFDFBD0094615A000000000000000000000000000000000000FF
      0000000000000000000000000000FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD0094615A000000000000000000000000007B797B005251
      52004A0029009C2863009C2863009C2863009C2863009C2863009C2863004A00
      2900525152007B797B00000000000000000000000000000000007B797B005251
      52004A0029009C2863009C2863009C2863009C2863009C2863009C2863004A00
      2900525152007B797B00000000000000000000000000C6968400E7D7CE00F7EF
      E7000000000000000000F7EFE700F7EFE700F7EFE7000000000000000000F7EF
      E700F7EFE700FFDFBD00946163000000000000000000C69684000000000000FF
      000000000000F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD009461630000000000525152006B0042009C2863009C28
      63009C2863006B0042006B0042006B0042006B0042006B0042006B0042009C28
      63009C2863009C2863006B00420052515200525152006B0042009C2863009C28
      63009C2863006B0042006B0042006B0042006B0042006B0042006B0042009C28
      63009C2863009C2863006B0042005251520000000000C6968400EFDFCE00F7EF
      E700FFD7AD000000000000000000FFD7AD000000000000000000FFD7A500FFD7
      A500F7EFE700FFDFBD009C6963000000000000000000C6968400000000000000
      000000000000FFD7AD00FFD7AD00FFD7AD00FFD7AD00FFD7A500FFD7A500FFD7
      A500F7EFE700FFDFBD009C696300000000004A0029009C2863009C2863004A00
      290000000000101810005A615A0052515200525152006B696B00101810000000
      00006B0042009C2863009C2863004A0029004A0029009C2863009C2863004A00
      290008080800101810005A615A0052515200525152006B696B00101810000808
      08006B0042009C2863009C2863004A00290000000000CE9E8400EFDFD600F7EF
      E700FFCF9C00FFCF9C00000000000000000000000000FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD009C6963000000000000000000CE9E8400EFDFD600F7EF
      E700FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD009C696300000000004A0029009C2863008C968C004A00
      290000000000FF8E6300FF8E6300FF8E6300FF8E6300FF8E6300FF8E63000000
      00004A0029009C2863009C2863004A0029004A0029009C2863008C968C004A00
      290008080800FF8E6300FF8E6300FF8E6300FF8E6300FF8E6300FF8E63000808
      08004A0029009C2863009C2863004A00290000000000CE9E8400EFE7DE00F7EF
      E700F7EFE700F7EFE700000000000000000000000000F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD00A5716B000000000000000000CE9E8400EFE7DE00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD00A5716B00000000004A0029008C968C00848684004A00
      290000000000FF8E6300A5AEA500A59E9C009486730094960000FF8E63000000
      00004A0029008C968C009C2863004A0029004A0029008C968C00848684004A00
      290008080800FF8E6300A5AEA500A59E9C009486730094960000FF8E63000808
      08004A0029008C968C009C2863004A00290000000000D6A68C00EFE7DE00FFF7
      F700FFDFBD000000000000000000FFDFBD00FFDFBD0000000000FFDFBD00FFDF
      BD00F7EFE700FFDFBD00AD716B000000000000000000D6A68C00EFE7DE00FFF7
      F700FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDF
      BD00F7EFE700FFDFBD00AD716B00000000006B0042009C2863009C2863004A00
      290000000000FF714200FF714200FF714200FF714200FF714200FF7142000000
      00004A0029009C286300948673004A0029006B0042009C2863009C2863004A00
      290008080800FF714200FF714200FF714200FF714200FF714200FF7142000808
      08004A0029009C286300948673004A00290000000000DEAE8C00F7E7DE00FFF7
      F7000000000000000000FFCF9C00FFCF9C00FFCF9C00FFCF9C0000000000FFCF
      9C00F7EFE700FFDFBD00AD7973000000000000000000DEAE8C00F7E7DE00FFF7
      F700FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD00AD797300000000009C2863009C2863009C2863004A00
      2900000000006B2000006B2000006B2000006B2000006B2000006B2000000000
      00004A002900948673009C2863004A0029009C2863009C2863009C2863004A00
      2900080808006B2000006B2000006B2000006B2000006B2000006B2000000808
      08004A002900948673009C2863004A00290000000000E7B68C00F7EFE7000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00000000000000
      0000FFBEBD00FFA6A500AD7973000000000000000000E7B68C00F7EFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD797300000000005251520094867300FF698C009C28
      63004A0029004A0010004A0010004A0010004A0010004A0010004A0010004A00
      29009C286300FF698C00FF8E8C00293029005251520094867300FF698C009C28
      63004A0029004A0010004A0010004A0010004A0010004A0010004A0010004A00
      29009C286300FF698C00FF8E8C002930290000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B58673000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B58673000000000073797300CECFCE00A5AEA500FF8E
      8C009C2863009C2863009C2863009C2863009C2863009C2863009C2863009C28
      6300FF8E8C00A5AEA500CECFCE006B696B0073797300CECFCE00A5AEA500FF8E
      8C009C2863009C2863009C2863009C2863009C2863009C2863009C2863009C28
      6300FF8E8C00A5AEA500CECFCE006B696B0000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA5000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA50000000000000000006B696B00737973004249
      4200FF69C600FF69C600FF69C600FF69C600FF69C600FF69C600FF69C600FF69
      C60042494200737973007B797B0000000000000000006B696B00737973004249
      4200FF69C600FF69C600FF69C600FF69C600FF69C600FF69C600FF69C600FF69
      C60042494200737973007B797B000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A500000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      00007B797B007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B00000000000000000000000000000000000000000000000000000000000000
      00007B797B007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A50000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      0000000000000000000000000000636163000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF00000000000063CFCE0063CFCE0063CFCE0063CFCE0063CF
      CE0063CFCE0063CFCE0000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000AD9E94006361630063616300636163006361
      6300000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C6000000000000000000000000000000000000FF0000C6C7C600000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000000000000000000000000094FFFF0094CFFF0094FFFF0094CFFF0094CF
      FF0094CFFF0063CFCE00000000000000000000000000000000000000000000FF
      0000000000000000000000000000DEB6B500B5A68C00B5AEA500AD9E94009C8E
      8C00636163006361630063616300636163000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C600000000000000000000000000C6C7C60000FF0000C6C7C6000000
      00000000000000000000000000000000000000000000CEFFFF000000000000FF
      00000000000094CFFF00FF00000094CFFF00FF00000094CFFF0094FFFF0094CF
      FF0094CFFF0063CFCE00000000000000000000000000000000000000000000FF
      000000000000E7C7C600EFDFDE00DEBEBD00299E210084A66B00D6A6A500D6A6
      A500D6A6A500B5AEA500E7C7C600636163000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000000000000000000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C60000FF0000C6C7C600000000000000000000FF0000C6C7C60000FF0000C6C7
      C6000000000000000000000000000000000000000000CEFFFF00000000000000
      00000000000094FFFF0094FFFF007B00000094CFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE00000000000000000000000000B59E9400000000000000
      000000000000DEB6B500EFDFDE00DEBEBD00CEA6A500189E100084A66B00D6A6
      A500D6A6A500D6AEAD00E7C7C600636163000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C60000000000C6C7C60000FF0000C6C7C60000FF
      0000C6C7C60000000000000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF00FF000000FF000000FF000000FF000000FF00000094FFFF0094CF
      FF0094FFFF0063CFCE00000000000000000000000000C6797B00C6797B007B49
      8400C6797B00D69E9C00EFD7D600DEC7C600D6AEAD00CEA6A500109E0800B5A6
      8C00429E3100D6AEAD00DEAEAD00636163000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C6000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C60000FF0000C6C7C600000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF007B00000094FFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE00000000000000000000000000A5719400312094004228
      8C00C6797B00CE8E8C00EFD7D600E7C7C600D6AEAD00D6A6A50094A67300009E
      0000109E0800DEB6B500D69E9C00636163000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C600000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C6000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF00FF00000094FFFF00FF00000094CFFF0094FFFF0094CF
      FF0094FFFF0063CFCE00000000000000000000000000C6797B00BD797B003920
      9400C6797B00C6868400E7CFCE00E7C7C600D6AEAD00D6A6A5005A9E4A00219E
      1800009E0000E7C7C60063616300000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000C6C7C600000000000000000000000000000000000000000000FF0000C6C7
      C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C60000FF0000C6C7C60000FF0000C6C7C60000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FF
      FF0094CFFF0063CFCE0000000000000000000000000000000000C68684004228
      8C00C6797B00C6797B00DEB6B500E7CFCE00DEBEBD00DEB6B500D6A6A500D6A6
      A500D6AEAD00EFD7D60063616300000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C600000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C6000000000000000000CEFFFF00CEFFFF00CEFF
      FF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFF
      FF0094FFFF0063CFCE0000000000000000000000000000000000D69E9C008C51
      8400C6868400D6A6A500EFDFDE00E7C7C600DEAEAD00DEAEAD00E7C7C600E7BE
      BD00F7E7E7006361630000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C6000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7C60000FF0000C6C7
      C60000FF0000C6C7C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7BE
      BD00E7BEBD00E7C7C600D68E4A00CE610000CE610000D68E6B00CE969400B58E
      8400636163000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C60000FF0000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C60000FF0000C6C7C60000000000C6C7C60000FF0000C6C7C60000FF
      0000C6C7C6000000000000000000000000000000000000000000EFEFEF00CEFF
      FF00CEFFFF0094FFFF0094FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6BEBD00DECFCE00D6C7C600D6BEBD00B5AEA5009C8E8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C60000FF0000C6C7C60000000000000000000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C60000FF0000C6C7C600000000000000000000FF0000C6C7C60000FF0000C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C7C60000FF0000C6C7C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C7C60000FF
      0000C6C7C600000000000000000000000000C6C7C60000FF0000C6C7C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C7C600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF0000C6C7
      C6000000000000000000000000000000000000FF0000C6C7C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C6000000000000000000C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF008400
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF008400
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF008400
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF008400
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008486840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084868400C6C7C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0084868400C6C7C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF008486840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00848684000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000840000008400000084000000
      8400000084000000000000000000000000000000000000008400000084000000
      8400000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797300000000000000
      000000000000A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A000000000000000000AD797300000000000000
      000000000000A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300A5797300A5797300A5797300A5797300A579
      7300A5797300A57973008C595A000000000000000000AD797B000000000000FF
      000000000000FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A000000000000000000AD797B000000000000FF
      000000000000FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A000000000000000000000000000000000000FF
      0000000000000000000000000000F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A000000000000000000AD867B00DECFBD00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A000000000000000000000000000000000000FF
      0000000000000000000000000000F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A000000000000000000AD867B00DECFBD00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A00000000000000000000FF000000FF000000FF
      000000FF000000FF00000000000000000000FFFFFF0000000000000000000000
      0000F7EFE700FFDFBD008C595A000000000000000000B58E7B00DECFC600F7EF
      E70000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000F7EFE700FFDFBD008C595A00000000000000000000FF000000FF000000FF
      000000FF000000FF000000000000FFDFBD00FFDFBD00FFDFB500FFDFB500FFD7
      AD00F7EFE700FFDFBD008C595A000000000000000000B58E7B00DECFC600F7EF
      E700FFE7CE00FFE7CE00FFE7C600FFDFBD00FFDFBD00FFDFB500FFDFB500FFD7
      AD00F7EFE700FFDFBD008C595A000000000000000000000000000000000000FF
      0000000000000000000000000000FFFFFF00FFFFFF0000000000FFFFFF000000
      0000F7EFE700FFDFBD0094615A000000000000000000BD8E7B00E7D7CE00F7EF
      E70000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF000000
      0000F7EFE700FFDFBD0094615A000000000000000000000000000000000000FF
      000000000000000000000000000000000000000000000000000000000000FFCF
      9C00F7EFE700FFDFBD0094615A000000000000000000BD8E7B00E7D7CE00F7EF
      E700FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD0094615A000000000000000000C69684000000000000FF
      000000000000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00F7EFE700FFDFBD00946163000000000000000000C6968400E7D7CE00F7EF
      E70000000000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00F7EFE700FFDFBD00946163000000000000000000C69684000000000000FF
      0000000000000086840000868400008684000086840000868400008684000000
      0000F7EFE700FFDFBD00946163000000000000000000C6968400E7D7CE00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD00946163000000000000000000C6968400000000000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000F7EFE700FFDFBD009C6963000000000000000000C6968400EFDFCE000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000F7EFE700FFDFBD009C6963000000000000000000C6968400000000000000
      0000000000000000000000868400008684000086840000868400008684000086
      840000000000FFDFBD009C6963000000000000000000C6968400EFDFCE00F7EF
      E700FFD7AD00FFD7AD00FFD7AD00FFD7AD00FFD7AD00FFD7A500FFD7A500FFD7
      A500F7EFE700FFDFBD009C6963000000000000000000CE9E8400EFDFD600F7EF
      E70000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000F7EFE700FFDFBD009C6963000000000000000000CE9E8400EFDFD600F7EF
      E70000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000F7EFE700FFDFBD009C6963000000000000000000CE9E8400EFDFD6000000
      0000FFFFFF0000FFFF0000000000008684000086840000868400008684000086
      840000868400000000009C6963000000000000000000CE9E8400EFDFD600F7EF
      E700FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD009C6963000000000000000000CE9E8400EFE7DE00F7EF
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00A5716B000000000000000000CE9E8400EFE7DE00F7EF
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00A5716B000000000000000000CE9E8400EFE7DE000000
      000000FFFF00FFFFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9E8400EFE7DE00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EFE700F7EF
      E700F7EFE700FFDFBD00A5716B000000000000000000D6A68C00EFE7DE00FFF7
      F70000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000F7EFE700FFDFBD00AD716B000000000000000000D6A68C00EFE7DE00FFF7
      F70000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000F7EFE700FFDFBD00AD716B000000000000000000D6A68C00EFE7DE000000
      0000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFDF
      BD00F7EFE700FFDFBD00AD716B000000000000000000D6A68C00EFE7DE00FFF7
      F700FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDFBD00FFDF
      BD00F7EFE700FFDFBD00AD716B000000000000000000DEAE8C00F7E7DE00FFF7
      F70000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000F7EFE700FFDFBD00AD7973000000000000000000DEAE8C00F7E7DE00FFF7
      F70000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000F7EFE700FFDFBD00AD7973000000000000000000DEAE8C00F7E7DE000000
      000000FFFF00FFFFFF000000000000000000000000000000000000000000FFCF
      9C00F7EFE700FFDFBD00AD7973000000000000000000DEAE8C00F7E7DE00FFF7
      F700FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00F7EFE700FFDFBD00AD7973000000000000000000E7B68C00F7EFE700FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFBEBD00FFA6A500AD7973000000000000000000E7B68C00F7EFE700FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFBEBD00FFA6A500AD7973000000000000000000E7B68C00F7EFE700FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD7973000000000000000000E7B68C00F7EFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD7973000000000000000000E7B68C00FFEFE7000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFF7EF000000
      0000B5867300B5867300B58673000000000000000000E7B68C00FFEFE7000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFF7EF000000
      0000B5867300B5867300B58673000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B58673000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7EF00B586
      7300B5867300B5867300B58673000000000000000000EFB69400FFF7E700FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000FFF7EF00B586
      7300FFC77B00DEA67300CEAEA5000000000000000000EFB69400FFF7E700FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000FFF7EF00B586
      7300FFC77B00DEA67300CEAEA5000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA5000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700B586
      7300FFC77B00DEA67300CEAEA5000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A500000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A500000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A500000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00F7F7F700F7F7F700F7F7F700EFEFEF00F7F7F700B586
      7300DEB68C00CEB6A500000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A50000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A50000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A50000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A50000000000000000000000000000000000AD797300A5797300A579
      7300A5797300A5797300A5797300F7AE7B00A5797300A5797300A5797300A579
      7300A5797300A57973008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD797B00DEC7BD00FFCF
      C600FFCFC600FFCFC600FFCFC600FFCFC600EFC7AD00FF9E5A00FFCFC600FFCF
      C600FFCFC600FFCFC6008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD867B00DECFBD00F7EF
      E700F7EFE700F7EFE700F7EFE700F7EFE700FFBE7300FFC77B00F7EFE700F7EF
      E700F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000C6C7C600C6C7
      C600000000000086840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58E7B00DECFC600F7EF
      E700FFFFFF00FFFFFF00FFFFFF00FFC77B00FFB67300A5865200FFFFFF00FFFF
      FF00F7EFE700FFDFBD008C595A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000C6C7C600C6C7
      C600000000000086840000000000000000000000000063CFCE0063CFCE0063CF
      CE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CFCE0063CF
      CE0063CFCE0063CFCE00000000000000000000000000BD8E7B00E7D7CE00F7EF
      E700FFFFFF00FFFFFF00E79E6B00FFC77B00ADCFBD0031308C00CED7CE00FFFF
      FF00F7EFE700FFDFBD0094615A00000000000000000000000000008684000086
      8400008684000086840000868400008684000086840000868400008684000000
      0000000000000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000C6C7C600C6C7
      C6000000000000868400000000000000000000000000CEFFFF0094CFFF0094FF
      FF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094FFFF0094CFFF0094CF
      FF0094CFFF0063CFCE00000000000000000000000000C6968400E7D7CE00F7EF
      E700FFFFFF00FFBE8400EFBE7300E7AE84006B969C00F79E5200F79E6300F7C7
      AD00F7EFE700FFDFBD0094616300000000000000000000FFFF00000000000086
      8400008684000086840000868400008684000086840000868400008684000086
      8400000000000000000000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      00000000000000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094CFFF00FF00000094CFFF00FF00000094CFFF0094FFFF0094CF
      FF0094CFFF0063CFCE00000000000000000000000000C6968400EFDFCE00F7EF
      E7009C715200BD8652009C795A0084AEA500EF966300F7A66B00B5865200A58E
      6B00F7EFE700FFDFBD009C6963000000000000000000FFFFFF0000FFFF000000
      0000008684000086840000868400008684000086840000868400008684000086
      8400008684000000000000000000000000000000000000000000008684000086
      8400008684000086840000868400008684000086840000868400008684000086
      84000086840000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF007B00000094CFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE00000000000000000000000000CE9E8400EFDFD600F7EF
      E700B586520094694A007B306300FFA66B00FFA6630063306300C68652008461
      4200F7EFE700FFDFBD009C696300000000000000000000FFFF00FFFFFF0000FF
      FF00000000000086840000868400008684000086840000868400008684000086
      8400008684000086840000000000000000000000000000000000008684000086
      8400000000000000000000000000000000000000000000000000000000000000
      00000086840000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF00FF000000FF000000FF000000FF000000FF00000094FFFF0094CF
      FF0094FFFF0063CFCE00000000000000000000000000CE9E8400EFE7DE00F7EF
      E700BD864A00210063007B968400E7966300DE8E5A002961940052616B00A571
      5200F7EFE700FFDFBD00A5716B000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C6000000000000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF007B00000094FFFF0094FFFF0094CFFF0094FF
      FF0094CFFF0063CFCE00000000000000000000000000D6A68C00EFE7DE00FFF7
      F700FFFFFF00B5A6B500BDAE9400C68E5A00E79663009CB6BD00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00AD716B00000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C6000000000000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF00FF00000094FFFF00FF00000094CFFF0094FFFF0094CF
      FF0094FFFF0063CFCE00000000000000000000000000DEAE8C00F7E7DE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFAE6B00C68E5200FFFFFF00FFFFFF00FFFF
      FF00F7EFE700FFDFBD00AD7973000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C6000000000000868400000000000000000000000000CEFFFF0094FFFF0094FF
      FF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FFFF0094FF
      FF0094CFFF0063CFCE00000000000000000000000000E7B68C00F7EFE700FFFF
      FF00FFFFFF00FFFFFF00A5967B00F7A66B00FFAE73007BAEB500FFF7EF00FFE7
      D600FFBEBD00FFA6A500AD797300000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C6000000000000868400000000000000000000000000CEFFFF00CEFFFF00CEFF
      FF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFF
      FF0094FFFF0063CFCE00000000000000000000000000E7B68C00FFEFE700FFFF
      FF00FFFFFF00FFFFFF00BD866B00FFBE7300EFAE6B00C6A68C00FFF7EF00B586
      7300B5867300B5867300B5867300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFB69400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00D6BEB500A5BECE00DECFC600AD9E8C00FFFFF700B586
      7300FFC77B00DEA67300CEAEA500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008684000000
      0000C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C60000000000C6C7C60000000000000000000000000000000000EFEFEF00CEFF
      FF00CEFFFF0094FFFF0094FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFBE9400FFF7E700FFFF
      FF00FFFFFF00FFFFFF00948E840094969400738E8C00BDB6B500F7F7F700B586
      7300DEB68C00CEB6A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7BE9400DEAE8400DEAE
      8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400DEAE8400B586
      7300D6B6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF0080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      80030000000000008007000000000000FFFFFFFFFFFFFFFFF9FFFFFFFC00FFFF
      F6CFFE008000FFFFF6B7FE000000FFFFF6B7FE000000FFFFF8B780000000FFF7
      FE8F80000001C1F7FE3F80000003C3FBFF7F80000003C7FBFE3F80010003CBFB
      FEBF80030003DCF7FC9F80070FC3FF0FFDDF807F0003FFFFFDDF80FF8007FFFF
      FDDF81FFF87FFFFFFFFFFFFFFFFFFFFFFFFFB6E78001FFFFFE49B76B8001FFFF
      FE4984270001FFFFFFFFB76B0001FFFFFFFFCEE70001FFFFC7C7FFFF8001FFF7
      C7C7C7C78001C1F7C387C7C78001C3FBC007C3878001C7FBC007C0078001CBFB
      C007C0078001DCF7C007C0078001FF0FC007C0078001FFFFF39FC0078001FFFF
      F39FF39F8003FFFFF39FF39F8007FFFF80017F7EFFFFFFFF8001BFFF8001FFFF
      8001F003800007C18001E003000007C18001E003000007C18001E00300000101
      8001E0030000000180012003000000018001E002000000018001E00300008003
      8001E0030000C1078001E0030000C1078001E0030003E38F8001FFFF80FFE38F
      8003BF7DC1FFE38F80077F7EFFFFFFFF8001FFFFFFFFFFFF8001FFFFC00FFFFF
      80010000800FF1FF800100008003E01F800100008003C0018001000080008001
      8001000080000001800100008000000180010000800000018001000080000003
      8001000080008003800100008000800780010101E000C00F80010303E000F03F
      80030707F800FFFF8007FFFFF800FFFFD5C5815D800180014455A54580018001
      4555A555800100015450840080010001F81FF81F80010001C003C00380018001
      0000000080018001000000008001800100000000800180010000000080018001
      0000000080018001000000008001800100000000800180010000000080018001
      8001800180038003F00FF00F80078007C7FFC7FFFFFFFFFF8001C7FFFFFFFFFF
      000000FFF9FFCF3F0000000FF8FFC71F00000000F87FC30F0000C000F83FC107
      00008000F81FC00300008000F80FC00100008000F807C00000008001F807C000
      0000C001F80FC0010000C003F81FC0030003E007F83FC10780FFF81FF87FC30F
      C1FFFFFFF8FFC71FFFFFFFFFF9FFCF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FC7FFFFFDFFB0000FFFFFFFF8FFF0000FC7FFEFF87F70000FC7FFC7FC7EF
      0000FFFFF83FE3CF0000FC7FF01FF19F0000FC7FE00FF83F0000E00FFC7FFC7F
      0000F01FFC7FF83F0000F83FFFFFF19F0101FC7FFC7FC3CF0303FEFFFC7F87E7
      0707FFFFFFFF8FFBFFFFFFFFFC7FFFFF80018001800180018001800180018001
      0001800100018001000180010001800100018001000180018001800180018001
      8001800180018001800180018001800180018001800180018001800180018001
      8001800180018001800180018001800180018001800180018001800180018001
      800380038003800380078007800780078001FFFFFFFFFFFF8001FFFFC0018001
      8001FFFF800180008001001F800100008001000F800100008001000780010000
      8001000380010000800100018001000080010000800100008001001F80010000
      8001001F800100008001001F8001000080018FFF800100038001FFFF800180FF
      8003FFFF8001C1FF8007FFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList: TLocActionList
    Images = ImageList
    Localizer = Localizer
    Left = 533
    Top = 40
    object MenuEditAction: TLocAction
      Category = 'Edit'
      Caption = 'Edit'
      OnExecute = MenuItemClick
      LocCaption = 'MenuEdit'
    end
    object MenuEditLoadAction: TLocAction
      Tag = 1
      Category = 'Edit'
      Caption = 'Load From File...'
      Enabled = False
      Hint = 'Load From File|Loads the contents from an existing file'
      ImageIndex = 20
      ShortCut = 49228
      OnExecute = MenuEditLoadAddActionExecute
      OnUpdate = MenuEditLoadActionUpdate
      LocCaption = 'MenuEditLoad'
      LocHint = 'MenuEditLoadHint'
    end
    object MenuEditSaveAction: TLocAction
      Category = 'Edit'
      Caption = 'Save To File...'
      Enabled = False
      Hint = 'Save To File|Saves the contents to a new file'
      ImageIndex = 36
      ShortCut = 16497
      OnExecute = MenuEditSaveActionExecute
      OnUpdate = MenuEditSaveActionUpdate
      LocCaption = 'MenuEditSave'
      LocHint = 'MenuEditSaveHint'
    end
    object MenuProjectAction: TLocAction
      Category = 'Project'
      Caption = 'Project'
      OnExecute = MenuItemClick
      LocCaption = 'MenuProject'
    end
    object MenuProjectTablesAction: TLocAction
      Category = 'Project'
      Caption = 'Tables'
      ImageIndex = 26
      OnExecute = MenuItemClick
      LocCaption = 'MenuProjectTables'
    end
    object MenuProjectTableAnsiFillAction: TLocAction
      Category = 'Project'
      Caption = 'ANSI Fill'
      Enabled = False
      Hint = 
        'ANSI Fill|Fills the active TBL item with the characters from the' +
        ' ANSI character set'
      OnExecute = MenuProjectTableAnsiFillActionExecute
      OnUpdate = MenuProjectTableAnsiFillActionUpdate
      LocCaption = 'MenuProjectTableAnsiFill'
      LocHint = 'MenuProjectTableAnsiFillHint'
    end
    object MenuProjectGroupsAction: TLocAction
      Category = 'Project'
      Caption = 'Groups'
      ImageIndex = 26
      OnExecute = MenuItemClick
      LocCaption = 'MenuProjectGroups'
    end
    object MenuProjectGroupAddAction: TLocAction
      Category = 'Project'
      Caption = 'Add Group'
      Enabled = False
      Hint = 'Add Group|Adds a new group to the active project'
      ImageIndex = 12
      ShortCut = 16455
      OnExecute = MenuProjectGroupAddActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuProjectGroupAdd'
      LocHint = 'MenuProjectGroupAddHint'
    end
    object MenuFileAction: TLocAction
      Category = 'File'
      Caption = 'File'
      OnExecute = MenuItemClick
      LocCaption = 'MenuFile'
    end
    object MenuFileNewAction: TLocAction
      Category = 'File'
      Caption = 'New Project'
      Hint = 'New Project|Creates a new project'
      ImageIndex = 30
      ShortCut = 16462
      OnExecute = MenuFileNewActionExecute
      LocCaption = 'MenuFileNew'
      LocHint = 'MenuFileNewHint'
    end
    object MenuProjectGroupTextReloadAction: TLocAction
      Category = 'Project'
      Caption = 'Reload Source Text'
      Enabled = False
      Hint = 'Reload Source Text|Reloads all text in the active group or list'
      ImageIndex = 21
      ShortCut = 16502
      OnExecute = MenuProjectGroupTextReloadActionExecute
      OnUpdate = MenuProjectGroupTextReloadActionUpdate
      LocCaption = 'MenuProjectGroupTextReload'
      LocHint = 'MenuProjectGroupTextReloadHint'
    end
    object MenuFileOpenAction: TLocAction
      Category = 'File'
      Caption = 'Open Project...'
      Hint = 'Open Project|Opens an existing project'
      ImageIndex = 1
      ShortCut = 16463
      OnExecute = MenuFileOpenActionExecute
      LocCaption = 'MenuFileOpen'
      LocHint = 'MenuFileOpenHint'
    end
    object MenuFileSaveAction: TLocAction
      Category = 'File'
      Caption = 'Save'
      Enabled = False
      Hint = 'Save|Saves the active project'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = MenuFileSaveActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuFileSave'
      LocHint = 'MenuFileSaveHint'
    end
    object MenuProjectGroupTextResetAction: TLocAction
      Category = 'Project'
      Caption = 'Restore Source Text'
      Enabled = False
      Hint = 
        'Restore Source Text|Restores edited text to the original state i' +
        'n the active program element'
      ImageIndex = 8
      ShortCut = 16503
      OnExecute = MenuProjectGroupTextResetActionExecute
      OnUpdate = MenuProjectGroupTextResetActionUpdate
      LocCaption = 'MenuProjectGroupTextReset'
      LocHint = 'MenuProjectGroupTextResetHint'
    end
    object MenuProjectGroupTextInsertAction: TLocAction
      Category = 'Project'
      Caption = 'Recalculate And Insert'
      Enabled = False
      Hint = 
        'Recalculate And Insert|Recalculates all pointers in the active p' +
        'roject group and inserts the edited text to the ROM'
      ImageIndex = 14
      ShortCut = 120
      OnExecute = MenuProjectGroupTextInsertActionExecute
      OnUpdate = MenuProjectGroupTextInsertActionUpdate
      LocCaption = 'MenuProjectGroupTextInsert'
      LocHint = 'MenuProjectGroupTextInsertHint'
    end
    object MenuProjectTextInsertAction: TLocAction
      Category = 'Project'
      Caption = 'Recalculate And Insert All'
      Enabled = False
      Hint = 
        'Recalculate And Insert All|Recalculates all pointers in all proj' +
        'ect groups and inserts the edited text to the ROM'
      ImageIndex = 15
      ShortCut = 16504
      OnExecute = MenuProjectTextInsertActionExecute
      OnUpdate = MenuProjectTextInsertActionUpdate
      LocCaption = 'MenuProjectTextInsert'
      LocHint = 'MenuProjectTextInsertHint'
    end
    object MenuProjectGroupDictOptionsAction: TLocAction
      Category = 'Project'
      Caption = 'DTE/MTE Dictionary...'
      Enabled = False
      Hint = 'DTE/MTE Dictionary|Shows the DTE/MTE dictionary options dialog'
      ShortCut = 49220
      OnExecute = MenuProjectGroupDictOptionsActionExecute
      OnUpdate = MenuDictActionsUpdate
      LocCaption = 'MenuProjectGroupDictOptions'
      LocHint = 'MenuProjectGroupDictOptionsHint'
    end
    object MenuProjectGroupDictGenerateAction: TLocAction
      Category = 'Project'
      Caption = 'Generate Dictionary'
      Enabled = False
      Hint = 'Generate Dictionary|Generates the DTE/MTE dictionary'
      ShortCut = 49223
      OnExecute = MenuProjectGroupDictGenerateActionExecute
      OnUpdate = MenuDictActionsUpdate
      LocCaption = 'MenuProjectGroupDictGenerate'
      LocHint = 'MenuProjectGroupDictGenerateHint'
    end
    object MenuProjectGroupDictToTableAction: TLocAction
      Category = 'Project'
      Caption = 'Dictionary To TBL'
      Enabled = False
      Hint = 
        'Dictionary To TBL|Puts all generated dictionary words into selec' +
        'ted TBL'
      ShortCut = 24693
      OnExecute = MenuProjectGroupDictToTableActionExecute
      OnUpdate = MenuProjectGroupDictToTableActionUpdate
      LocCaption = 'MenuProjectGroupDictToTable'
      LocHint = 'MenuProjectGroupDictToTableHint'
    end
    object MenuProjectPointersAction: TLocAction
      Category = 'Project'
      Caption = 'Pointers'
      ImageIndex = 26
      OnExecute = MenuItemClick
      LocCaption = 'MenuProjectPointers'
    end
    object MenuProjectPointersToggleInsertFieldsAction: TLocAction
      Category = 'Project'
      Caption = 'Toggle Insertion Fields'
      Enabled = False
      Hint = 
        'Toggle Insertion Fields|Adds or removes additional text insertin' +
        'g fields to/from the pointers list properties'
      ShortCut = 49221
      OnExecute = MenuProjectPointersToggleInsertFieldsActionExecute
      OnUpdate = MenuProjectPointersToggleInsertFieldsActionUpdate
      LocCaption = 'MenuProjectPointersToggleInsertFields'
      LocHint = 'MenuProjectPointersToggleInsertFieldsHint'
    end
    object MenuProjectPointersAddAction: TLocAction
      Category = 'Project'
      Caption = 'Add Pointers...'
      Enabled = False
      Hint = 'Add Pointers|Adds a new pointers to the active pointers list'
      ImageIndex = 4
      ShortCut = 49232
      OnExecute = MenuProjectPointersAddActionExecute
      OnUpdate = MenuProjectPointersAddActionUpdate
      LocCaption = 'MenuProjectPointersAdd'
      LocHint = 'MenuProjectPointersAddHint'
    end
    object MenuEditCopyAction: TLocAction
      Category = 'Edit'
      Caption = 'Copy'
      Enabled = False
      Hint = 'Copy|Copies the selected contents and puts them on the Clipboard'
      ImageIndex = 33
      ShortCut = 16451
      OnExecute = MenuEditCopyActionExecute
      OnUpdate = MenuEditCopyCutActionUpdate
      LocCaption = 'MenuEditCopy'
      LocHint = 'MenuEditCopyHint'
    end
    object MenuEditClearAction: TLocAction
      Category = 'Edit'
      Caption = 'Clear'
      Enabled = False
      Hint = 'Clear|Clears the contents of the active program element'
      ImageIndex = 18
      ShortCut = 16430
      OnExecute = MenuEditClearActionExecute
      OnUpdate = MenuEditClearActionUpdate
      LocCaption = 'MenuEditClear'
      LocHint = 'MenuEditClearHint'
    end
    object MenuFileSaveAsAction: TLocAction
      Category = 'File'
      Caption = 'Save As...'
      Enabled = False
      Hint = 'Save As|Saves the active project with a new name'
      ImageIndex = 25
      ShortCut = 49235
      OnExecute = MenuFileSaveAsActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuFileSaveAs'
      LocHint = 'MenuFileSaveAsHint'
    end
    object MenuFileSaveAllAction: TLocAction
      Category = 'File'
      Caption = 'Save All'
      Enabled = False
      Hint = 'Save All|Saves all opened projects'
      ImageIndex = 22
      OnExecute = MenuFileSaveAllActionExecute
      OnUpdate = MenuFileSaveAllActionUpdate
      LocCaption = 'MenuFileSaveAll'
      LocHint = 'MenuFileSaveAllHint'
    end
    object MenuFileCloseAction: TLocAction
      Category = 'File'
      Caption = 'Close'
      Enabled = False
      Hint = 'Close|Closes the active project'
      ImageIndex = 11
      ShortCut = 49240
      OnExecute = MenuFileCloseActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuFileClose'
      LocHint = 'MenuFileCloseHint'
    end
    object MenuFileCloseAllAction: TLocAction
      Category = 'File'
      Caption = 'Close All'
      Enabled = False
      Hint = 'Close All|Closes all opened projects'
      OnExecute = MenuFileCloseAllActionExecute
      OnUpdate = MenuFileCloseAllActionUpdate
      LocCaption = 'MenuFileCloseAll'
      LocHint = 'MenuFileCloseAllHint'
    end
    object MenuFileExitAction: TLocAction
      Category = 'File'
      Caption = 'Exit'
      Hint = 'Exit|Quits the program'
      OnExecute = MenuFileExitActionExecute
      LocCaption = 'MenuFileExit'
      LocHint = 'MenuFileExitHint'
    end
    object MenuProjectPointersVarOptionsAction: TLocAction
      Category = 'Project'
      Caption = 'Near-Pointer Variables...'
      Enabled = False
      Hint = 
        'Near-Pointer Variables|Shows the near-pointer variables options ' +
        'dialog'
      ShortCut = 49238
      OnExecute = MenuProjectPointersVarOptionsActionExecute
      OnUpdate = MenuProjectPointersVarOptionsActionUpdate
      LocCaption = 'MenuProjectPointersVarOptions'
      LocHint = 'MenuProjectPointersVarOptionsHint'
    end
    object MenuProjectPointersTableLinksResetAction: TLocAction
      Category = 'Project'
      Caption = 'Reset TBL Links'
      Enabled = False
      Hint = 
        'Reset TBL Links|Sets the TBL links to all pointers in the active' +
        ' pointers list'
      ShortCut = 49236
      OnExecute = MenuProjectPointersTableLinksResetActionExecute
      OnUpdate = MenuProjectPointersTableLinksResetActionUpdate
      LocCaption = 'MenuProjectPointersTableLinksReset'
      LocHint = 'MenuProjectPointersTableLinksResetHint'
    end
    object MenuProjectItemAddAction: TLocAction
      Category = 'Project'
      Caption = 'Add Item'
      Enabled = False
      Hint = 'Add Item|Adds a new item to the active project'
      ImageIndex = 19
      ShortCut = 49217
      OnExecute = MenuProjectItemAddActionExecute
      OnUpdate = MenuProjectItemAddActionUpdate
      LocCaption = 'MenuProjectItemAdd'
      LocHint = 'MenuProjectItemAddHint'
    end
    object MenuProjectItemsAddFromFileAction: TLocAction
      Category = 'Project'
      Caption = 'Add From File...'
      Enabled = False
      Hint = 'Add From File|Adds the contents from an existing file'
      ImageIndex = 6
      ShortCut = 49231
      OnExecute = MenuEditLoadAddActionExecute
      OnUpdate = MenuProjectItemsAddFromFileActionUpdate
      LocCaption = 'MenuProjectItemsAddFromFile'
      LocHint = 'MenuProjectItemsAddFromFileHint'
    end
    object MenuProjectItemUpAction: TLocAction
      Category = 'Project'
      Caption = 'Move Item Up'
      Enabled = False
      Hint = 'Move Item Up|Moves the active project item up'
      ImageIndex = 9
      ShortCut = 16469
      OnExecute = MenuProjectItemUpActionExecute
      OnUpdate = MenuProjectItemUpActionUpdate
      LocCaption = 'MenuProjectItemUp'
      LocHint = 'MenuProjectItemUpHint'
    end
    object MenuProjectItemDownAction: TLocAction
      Category = 'Project'
      Caption = 'Move Item Down'
      Enabled = False
      Hint = 'Move Item Down|Moves the active project item down'
      ImageIndex = 10
      ShortCut = 16456
      OnExecute = MenuProjectItemDownActionExecute
      OnUpdate = MenuProjectItemDownActionUpdate
      LocCaption = 'MenuProjectItemDown'
      LocHint = 'MenuProjectItemDownHint'
    end
    object MenuProjectItemRemoveAction: TLocAction
      Category = 'Project'
      Caption = 'Remove Item'
      Enabled = False
      Hint = 'Remove Item|Removes the active project item'
      ImageIndex = 11
      ShortCut = 16452
      OnExecute = MenuProjectItemRemoveActionExecute
      OnUpdate = MenuProjectItemRemoveActionUpdate
      LocCaption = 'MenuProjectItemRemove'
      LocHint = 'MenuProjectItemRemoveHint'
    end
    object MenuProjectEmulatorSelectAction: TLocAction
      Category = 'Project'
      Caption = 'Select Emulator...'
      Enabled = False
      Hint = 
        'Select Emulator|Selects an emulator executable file to run one o' +
        'f the project ROM'#39's'
      OnExecute = MenuProjectEmulatorSelectActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuProjectEmulatorSelect'
      LocHint = 'MenuProjectEmulatorSelectHint'
    end
    object MenuProjectRunRom1Action: TLocAction
      Category = 'Project'
      Caption = 'Run Source ROM...'
      Enabled = False
      Hint = 'Run Source ROM|Runs the source ROM in the selected emulator'
      ImageIndex = 16
      ShortCut = 122
      OnExecute = MenuProjectRunRom1ActionExecute
      OnUpdate = MenuProjectRunRom1ActionUpdate
      LocCaption = 'MenuProjectRunRom1'
      LocHint = 'MenuProjectRunRom1Hint'
    end
    object MenuProjectRunRom2Action: TLocAction
      Category = 'Project'
      Caption = 'Run Destination ROM...'
      Enabled = False
      Hint = 
        'Run Destination ROM|Runs the destination ROM in the selected emu' +
        'lator'
      ImageIndex = 17
      ShortCut = 123
      OnExecute = MenuProjectRunRom2ActionExecute
      OnUpdate = MenuProjectRunRom2ActionUpdate
      LocCaption = 'MenuProjectRunRom2'
      LocHint = 'MenuProjectRunRom2Hint'
    end
    object MenuOtherClearMessageListAction: TLocAction
      Category = 'Other'
      Caption = 'Clear'
      Hint = 'Clear|Clears the messages list'
      OnExecute = MenuOtherClearMessageListActionExecute
      OnUpdate = MenuOtherClearMessageListActionUpdate
      LocCaption = 'MenuOtherClearMessageList'
      LocHint = 'MenuOtherClearMessageListHint'
    end
    object MenuEditUndoAction: TLocAction
      Category = 'Edit'
      Caption = 'Undo'
      Enabled = False
      Hint = 'Undo|Reverts the last text input'
      ImageIndex = 31
      ShortCut = 16474
      OnExecute = MenuEditUndoActionExecute
      OnUpdate = MenuEditUndoActionUpdate
      LocCaption = 'MenuEditUndo'
      LocHint = 'MenuEditUndoHint'
    end
    object MenuEditCutAction: TLocAction
      Tag = 1
      Category = 'Edit'
      Caption = 'Cut'
      Enabled = False
      Hint = 'Cut|Cuts the selected contents and puts them on the Clipboard'
      ImageIndex = 32
      ShortCut = 16472
      OnExecute = MenuEditCutActionExecute
      OnUpdate = MenuEditCopyCutActionUpdate
      LocCaption = 'MenuEditCut'
      LocHint = 'MenuEditCutHint'
    end
    object MenuEditPasteAction: TLocAction
      Category = 'Edit'
      Caption = 'Paste'
      Enabled = False
      Hint = 'Paste|Inserts the contents from the Clipboard'
      ImageIndex = 34
      ShortCut = 16470
      OnExecute = MenuEditPasteActionExecute
      OnUpdate = MenuEditPasteActionUpdate
      LocCaption = 'MenuEditPaste'
      LocHint = 'MenuEditPasteHint'
    end
    object MenuEditSelectAllAction: TLocAction
      Category = 'Edit'
      Caption = 'Select All'
      Enabled = False
      Hint = 'Select All|Selects the entire text'
      ShortCut = 16449
      OnExecute = MenuEditSelectAllActionExecute
      OnUpdate = MenuEditSelectAllActionUpdate
      LocCaption = 'MenuEditSelectAll'
      LocHint = 'MenuEditSelectAllHint'
    end
    object MenuSearchAction: TLocAction
      Category = 'Search'
      Caption = 'Search'
      OnExecute = MenuItemClick
      LocCaption = 'MenuSearch'
    end
    object MenuSearchFindAction: TLocAction
      Category = 'Search'
      Caption = 'Find...'
      Enabled = False
      Hint = 'Find|Tries to find the typed sentence in the projects'
      ImageIndex = 27
      ShortCut = 16454
      OnExecute = MenuSearchFindActionExecute
      OnUpdate = MenuSearchActionsUpdate
      LocCaption = 'MenuSearchFind'
      LocHint = 'MenuSearchFindHint'
    end
    object MenuSearchFindReplaceAction: TLocAction
      Category = 'Search'
      Caption = 'Find And Replace...'
      Enabled = False
      Hint = 
        'Find And Replace|Tries to find and replace the typed sentence in' +
        ' the projects'
      ImageIndex = 29
      ShortCut = 16466
      OnExecute = MenuSearchFindReplaceActionExecute
      OnUpdate = MenuSearchActionsUpdate
      LocCaption = 'MenuSearchFindReplace'
      LocHint = 'MenuSearchFindReplaceHint'
    end
    object MenuSearchFindNextAction: TLocAction
      Category = 'Search'
      Caption = 'Find Next'
      Enabled = False
      Hint = 
        'Find Next|Tries to find or replace the last typed sentence in th' +
        'e projects again'
      ImageIndex = 28
      ShortCut = 114
      OnExecute = MenuSearchFindNextActionExecute
      OnUpdate = MenuSearchActionsUpdate
      LocCaption = 'MenuSearchFindNext'
      LocHint = 'MenuSearchFindNextHint'
    end
    object MenuLanguageAction: TLocAction
      Category = 'Language'
      Caption = 'Language'
      OnExecute = MenuItemClick
      LocCaption = 'MenuLanguage'
    end
    object MenuHelpAction: TLocAction
      Category = 'Help'
      Caption = 'Help'
      OnExecute = MenuItemClick
      LocCaption = 'MenuHelp'
    end
    object MenuHelpPluginsAction: TLocAction
      Category = 'Help'
      Caption = 'Plugins...'
      Hint = 'Plugins|Shows information about all loaded plugins'
      OnExecute = MenuHelpPluginsItemClick
      LocCaption = 'MenuHelpPlugins'
      LocHint = 'MenuHelpPluginsHint'
    end
    object MenuHelpAboutAction: TLocAction
      Category = 'Help'
      Caption = 'About...'
      Hint = 'About|Shows information about the program'
      ShortCut = 16496
      OnExecute = MenuHelpAboutActionExecute
      LocCaption = 'MenuHelpAbout'
      LocHint = 'MenuHelpAboutHint'
    end
    object TabsSourceSaveToFileAction: TLocAction
      Tag = 1
      Category = 'Tabs'
      Caption = 'Save Source Text...'
      Hint = 
        'Save Source Text|Saves the editing group or pointers list source' +
        ' text to a new file'
      ImageIndex = 36
      OnExecute = TabsSourceSaveToFileActionExecute
      LocCaption = 'TabsSourceSaveToFile'
      LocHint = 'TabsSourceSaveToFileHint'
    end
    object MenuOptionsAction: TLocAction
      Category = 'Options'
      Caption = 'Options'
      OnExecute = MenuItemClick
      LocCaption = 'MenuOptions'
    end
    object MenuOptionsFontAction: TLocAction
      Category = 'Options'
      Caption = 'Font...'
      Hint = 'Font|Shows font selection dialog'
      OnExecute = ConfigFontItemClick
      LocCaption = 'MenuOptionsFont'
      LocHint = 'MenuOptionsFontHint'
    end
    object MenuProjectPointerFixPointersPositionsAction: TLocAction
      Category = 'Project'
      Caption = 'Fix Pointers Positions'
      Enabled = False
      Hint = 
        'Fix Pointers Positions|Fixes all pointers positions using ptDest' +
        'XXX fields starting from first pointer in list'
      ShortCut = 49222
      OnExecute = MenuProjectPointerFixPointersPositionsActionExecute
      OnUpdate = MenuProjectPointerFixPointersPositionsActionUpdate
      LocCaption = 'MenuProjectPointerFixPointersPositions'
      LocHint = 'MenuProjectPointerFixPointersPositionsHint'
    end
    object TabsCopySourceTextAction: TLocAction
      Category = 'Tabs'
      Caption = 'Copy Source Text'
      Hint = 
        'Copy Source Text|Copies the selected group or list source text a' +
        'nd puts it on the Clipboard'
      ImageIndex = 33
      OnExecute = TabsCopySourceTextActionExecute
      LocCaption = 'TabsCopySourceText'
      LocHint = 'TabsCopySourceTextHint'
    end
    object MenuProjectTableAddAction: TLocAction
      Category = 'Project'
      Caption = 'Add TBL'
      Enabled = False
      Hint = 'Add TBL|Adds a new TBL item to the active project'
      ImageIndex = 13
      ShortCut = 16468
      OnExecute = MenuProjectTableAddActionExecute
      OnUpdate = MenuActionsUpdate1
      LocCaption = 'MenuProjectTableAdd'
      LocHint = 'MenuProjectTableAddHint'
    end
    object PointersSortAscAction: TLocAction
      Category = 'Project'
      Caption = 'Ascending Sort'
      Enabled = False
      Hint = 'Ascending Sort|Ascending sorts pointers in list'
      OnExecute = PointersSortActionExecute
      OnUpdate = PointersSortActionUpdate
      LocCaption = 'MenuProjectPointersAscSort'
      LocHint = 'MenuProjectPointersAscSortHint'
    end
    object PointersSortDescAction: TLocAction
      Tag = 1
      Category = 'Project'
      Caption = 'Descending Sort'
      Enabled = False
      Hint = 'Descending Sort|Descending sorts pointers in list'
      OnExecute = PointersSortActionExecute
      OnUpdate = PointersSortActionUpdate
      LocCaption = 'MenuProjectPointersDescSort'
      LocHint = 'MenuProjectPointersDescSortHint'
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 401
    Top = 40
  end
  object OpenProjectDialog: TLocOpenDialog
    DefaultExt = '.kpx'
    Filter = 
      'All formats (*.kpx;*.kpj)|*.kpx;*.kpj|Kruptar 7 Project (*.kpx)|' +
      '*.kpx|Kruptar 6 Project (*.kpj)|*.kpj'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist, ofEnableSizing]
    Title = 'Open'
    LocTitle = 'OpenDialogTitle'
    LocFilter = 'ProjectOpenFilter'
    Localizer = Localizer
    Left = 753
    Top = 40
  end
  object StatesPopup: TKruptarPopupMenu
    Images = ImageList
    MenuAnimation = [maLeftToRight]
    Left = 769
    Top = 84
    object ClearStateMemoPopItem: TKruptarMenuItem
      Action = MenuOtherClearMessageListAction
    end
  end
  object OpenDialog: TLocOpenDialog
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Open'
    LocTitle = 'OpenDialogTitle'
    Localizer = Localizer
    Left = 621
    Top = 40
  end
  object OpenTableDialog: TLocOpenDialog
    DefaultExt = '.tbl'
    Filter = 'TBL files (*.tbl)|*.tbl|Any files (*.*)|*.*'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Open'
    LocTitle = 'OpenDialogTitle'
    LocFilter = 'TablesFilter'
    Localizer = Localizer
    Left = 665
    Top = 40
  end
  object TextEditPopup: TKruptarPopupMenu
    Images = ImageList
    Left = 641
    Top = 84
    object LoadFromFile2: TKruptarMenuItem
      Action = MenuEditLoadAction
    end
    object SaveToFile3: TKruptarMenuItem
      Action = MenuEditSaveAction
    end
    object N26: TKruptarMenuItem
      Caption = '-'
    end
    object N5: TKruptarMenuItem
      Action = MenuEditUndoAction
    end
    object N4: TKruptarMenuItem
      Caption = '-'
    end
    object CutItem: TKruptarMenuItem
      Action = MenuEditCutAction
    end
    object CopyItem: TKruptarMenuItem
      Action = MenuEditCopyAction
    end
    object PasteItem: TKruptarMenuItem
      Action = MenuEditPasteAction
    end
    object N7: TKruptarMenuItem
      Caption = '-'
    end
    object SelectAllItem: TKruptarMenuItem
      Action = MenuEditSelectAllAction
    end
    object N1: TKruptarMenuItem
      Caption = '-'
    end
    object Clear5: TKruptarMenuItem
      Action = MenuEditClearAction
    end
    object N25: TKruptarMenuItem
      Caption = '-'
    end
    object ReloadSourceText1: TKruptarMenuItem
      Action = MenuProjectGroupTextReloadAction
    end
    object MenuEditTextResetAction2: TKruptarMenuItem
      Action = MenuProjectGroupTextResetAction
    end
  end
  object SaveProjectDialog: TLocSaveDialog
    DefaultExt = '.kpx'
    Filter = 'Kruptar 7 Project (*.kpx)|*.kpx'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save As'
    LocTitle = 'SaveDialogTitle'
    LocFilter = 'ProjectSaveFilter'
    Localizer = Localizer
    Left = 489
    Top = 84
  end
  object SaveTableDialog: TLocSaveDialog
    DefaultExt = '.tbl'
    Filter = 'TBL files (*.tbl)|*.tbl|Any files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Save As'
    LocTitle = 'SaveDialogTitle'
    LocFilter = 'TablesFilter'
    Localizer = Localizer
    Left = 401
    Top = 84
  end
  object Timer: TTimer
    Interval = 10
    OnTimer = TimerTimer
    Left = 533
    Top = 84
  end
  object OpenTextDialog: TLocOpenDialog
    DefaultExt = '.txt'
    Filter = 'Text files (*.txt)|*.txt|Any files (*.*)|*.*'
    Title = 'Open'
    LocTitle = 'OpenDialogTitle'
    LocFilter = 'TextFilesFilter'
    Localizer = Localizer
    Left = 709
    Top = 40
  end
  object Localizer: TLocalizer
    Left = 577
    Top = 40
  end
  object TabsPopup: TKruptarPopupMenu
    Images = ImageList
    Left = 709
    Top = 84
    object absLoadFromFileAction1: TKruptarMenuItem
      Action = MenuEditLoadAction
    end
    object absSaveToFileAction1: TKruptarMenuItem
      Action = MenuEditSaveAction
    end
    object N28: TKruptarMenuItem
      Caption = '-'
    end
    object absSourceSaveToFileAction1: TKruptarMenuItem
      Action = TabsSourceSaveToFileAction
    end
    object CopySourceText1: TKruptarMenuItem
      Action = TabsCopySourceTextAction
    end
    object N30: TKruptarMenuItem
      Caption = '-'
    end
    object extReload1: TKruptarMenuItem
      Action = MenuProjectGroupTextReloadAction
    end
    object extReset1: TKruptarMenuItem
      Action = MenuProjectGroupTextResetAction
    end
    object N2: TKruptarMenuItem
      Caption = '-'
    end
    object Cut4: TKruptarMenuItem
      Action = MenuEditCutAction
    end
    object Copy4: TKruptarMenuItem
      Action = MenuEditCopyAction
    end
    object Paste4: TKruptarMenuItem
      Action = MenuEditPasteAction
    end
    object N27: TKruptarMenuItem
      Caption = '-'
    end
    object Clear6: TKruptarMenuItem
      Action = MenuEditClearAction
    end
  end
  object SaveTextDialog: TLocSaveDialog
    DefaultExt = '.txt'
    Filter = 'Text files (*.txt)|*.txt|Any files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    LocTitle = 'SaveDialogTitle'
    LocFilter = 'TextFilesFilter'
    Localizer = Localizer
    Left = 445
    Top = 84
  end
  object CustomPopup: TKruptarPopupMenu
    Images = ImageList
    OnPopup = CustomPopupPopup
    Left = 581
    Top = 83
  end
end
