unit KruptarComponents;

interface

uses
{$IFDEF FORCED_UNICODE}
    Vcl.Forms
  , Vcl.ComCtrls
  , Vcl.StdCtrls
  , Vcl.ExtCtrls
  , Vcl.Menus
  , Vcl.Buttons
  , Vcl.ActnList
  , Vcl.Dialogs
{$ELSE}
    TntForms
  , TntStdCtrls
  , TntComCtrls
  , TntExtCtrls
  , TntMenus
  , TntButtons
  , TntActnList
  , TntDialogs
{$ENDIF};

type
{$IFDEF FORCED_UNICODE}
  TKruptarForm = class(TForm);
  TKruptarToolBar = class(TToolBar);
  TKruptarPanel = class(TPanel);
  TKruptarMemo = class(TMemo);
  TKruptarListBox = class(TListBox);
  TKruptarStatusBar = class(TStatusBar);
  TKruptarTreeViewType = TTreeView;
  TKruptarMainMenu = class(TMainMenu);
  TKruptarPopupMenu = class(TPopupMenu);
  TKruptarLabel = class(TLabel);
  TKruptarEdit = class(TEdit);
  TKruptarPageControl = class(TPageControl);
  TKruptarSpeedButton = class(TSpeedButton);
  TKruptarComboBox = class(TComboBox);
  TKruptarListView = class(TListView);
  TKruptarTabControl = class(TTabControl);
  TKruptarButton = class(TButton);
  TKruptarImage = class(TImage);
  TKruptarTabSheetType = TTabSheet;
  TKruptarToolButtonType = TToolButton;
  TKruptarTreeNodeType = TTreeNode;
  TKruptarMenuItemType = TMenuItem;
  TKruptarListItemType = TListItem;
  TKruptarActionType = TAction;
  TKruptarOpenDialog = class(TOpenDialog);
{$ELSE}
  TKruptarForm = class(TTntForm);
  TKruptarOpenDialog = class(TTntOpenDialog);
  TKruptarToolBar = class(TTntToolBar);
  TKruptarPanel = class(TTntPanel);
  TKruptarMemo = class(TTntMemo);
  TKruptarListBox = class(TTntListBox);
  TKruptarStatusBar = class(TTntStatusBar);
  TKruptarTreeViewType = TTntTreeView;
  TKruptarMainMenu = class(TTntMainMenu);
  TKruptarPopupMenu = class(TTntPopupMenu);
  TKruptarLabel = class(TTntLabel);
  TKruptarEdit = class(TTntEdit);
  TKruptarPageControl = class(TTntPageControl);
  TKruptarSpeedButton = class(TTntSpeedButton);
  TKruptarComboBox = class(TTntComboBox);
  TKruptarListView = class(TTntListView);
  TKruptarTabControl = class(TTntTabControl);
  TKruptarButton = class(TTntButton);
  TKruptarImage = class(TTntImage);
  TKruptarTabSheetType = TTntTabSheet;
  TKruptarToolButtonType = TTntToolButton;
  TKruptarTreeNodeType = TTntTreeNode;
  TKruptarMenuItemType = TTntMenuItem;
  TKruptarListItemType = TTntListItem;
  TKruptarActionType = TTntAction;
{$ENDIF}
  TKruptarTabSheet = class(TKruptarTabSheetType);
  TKruptarToolButton = class(TKruptarToolButtonType);
  TKruptarTreeNode = class(TKruptarTreeNodeType);
  TKruptarMenuItem = class(TKruptarMenuItemType);
  TKruptarTreeView = class(TKruptarTreeViewType);

implementation

end.
