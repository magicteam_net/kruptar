object SEFORM: TSEFORM
  Left = 205
  Top = 426
  BorderStyle = bsDialog
  Caption = 'Input'
  ClientHeight = 97
  ClientWidth = 165
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  LocCaption = 'InputBoxTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 7
    Top = 6
    Width = 153
    Height = 51
    Shape = bsFrame
  end
  object TypeLabel: TLocLabel
    Left = 16
    Top = 14
    Width = 73
    Height = 13
    Caption = 'Pointers count:'
    LocCaption = 'PointersCount'
    Localizer = MainForm.Localizer
  end
  object OKBtn: TLocButton
    Left = 6
    Top = 66
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OKBtnClick
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelBtn: TLocButton
    Left = 86
    Top = 66
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
  object StartEdit: TEdit
    Left = 15
    Top = 30
    Width = 137
    Height = 21
    TabOrder = 2
  end
end
