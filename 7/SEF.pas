unit SEF;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, HexUnit, Dialogs, LocClasses, KruptarComponents;

type
  TSEFORM = class(TLocForm)
    OKBtn: TLocButton;
    CancelBtn: TLocButton;
    Bevel: TBevel;
    StartEdit: TEdit;
    TypeLabel: TLocLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    Function Execute: Boolean;
  end;

var
  SEFORM: TSEFORM;
  OkPressed: Boolean;

implementation

uses MainUnit, StringConsts, LocMsgBox;

{$R *.dfm}

Function TSEFORM.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TSEFORM.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 CanClose := False;
 If OkPressed then
 begin
  OkPressed := False;
  If not UnsCheckOut(StartEdit.Text) then
   LocMessageDlg(K7S(SMUIntegerError), mtError, [mbOk], 0) else
   CanClose := True;
 end Else CanClose := True;
end;

procedure TSEFORM.OKBtnClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TSEFORM.FormShow(Sender: TObject);
begin
 OkPressed := False;
 StartEdit.SetFocus;
end;

end.
