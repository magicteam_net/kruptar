library Zelda3;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'The Legend of Zelda'#13#10'A Link to the Past';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; PP: PWord; Ptrs: String; J: Word;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  SetLength(Ptrs, (Count + 1) shl 1);
  PP := Pointer(Ptrs);
  J := $C7C7;
  R := Root;
  While R <> NIL do With R^ do
  begin
   PP^ := J;
   Inc(PP);
   Inc(J, Length(Str));
   Result := Result + Str;
   R := Next;
  end;
  PP^ := J;
  Result := Ptrs + Result;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var P: PChar; I, J: Integer; PCur, PNext: PWord;
begin
 Result := NIL;
 If (X <> $74703) then Exit;
 PNext := Addr(ROM^[X]);
 PCur := PNext;
 Inc(PNext);
 New(Result, Init);
 For I := 0 to 96 do With Result^.Add^ do
 begin
  J := Integer(PCur^) + $68000;
  P := Addr(ROM^[J]);
  Str := '';
  For J := 1 to PNext^ - PCur^ do
  begin
   Str := Str + P^;
   Inc(P);
  end;
  Inc(PCur);
  Inc(PNext);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

