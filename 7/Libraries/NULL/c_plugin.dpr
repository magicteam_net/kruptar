library c_plugin;

uses
  ShareMem, SysUtils,
  Needs in '..\..\Needs.pas',
  KruptarComponents in '..\..\KruptarComponents.pas',
  c_plugin_dlg in 'c_plugin_dlg.pas' {SelectPluginDialog};

{$E .kpl}

const
 SKPLDescription = 'C-Plugin Loader';

var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = Pointer(-1);
 MaxCodes: LongInt = 0; // Gets string length and flags for GetData
 Align: LongInt = 1;
 OriginalSize: LongInt = 0;

function Description: RawByteString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: LongInt; stdcall;
begin
 Result := OriginalSize; // Return original string size if it differs from result
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 MaxCodes := MC;
 Align := AL;
 EndsRoot := ER;
end;

var
 strings: PTextStrings;

function add_string(str: Pointer; size: LongInt): LongInt; stdcall;
var
 Item: PTextString;
begin
 Result := strings.Count;
 Item := strings.Add;
 SetLength(Item.Str, size);
 Move(str^, Pointer(Item.Str)^, size);
end;

procedure set_string(index: LongInt; str: Pointer; size: LongInt); stdcall;
var
 Item: PTextString;
begin
 Item := strings.Get(index);
 SetLength(Item.Str, size);
 Move(str^, Pointer(Item.Str)^, size);
end;

function get_string(index: LongInt; var size: LongInt): Pointer stdcall;
var
 Item: PTextString;
begin
 Item := strings.Get(index);
 if Item <> nil then
 begin
  Result := Pointer(Item.Str);
  size := Length(Item.Str);
 end else
 begin
  Result := nil;
  size := -1;
 end;
end;

function strings_count: LongInt; stdcall;
begin
 Result := strings.Count;
end;

function GetData(TextStrings: PTextStrings): RawByteString; stdcall;
var
 data: TPluginData;
begin
 Result := '';
 if TextStrings = nil then Exit;
 if PluginHandle <> 0 then
 begin
  strings := TextStrings;
  data.rom := ROM;
  data.rom_size := RomSize;
  data.parameter := MaxCodes;
  data.char_size := Align;
  Addr(data.add_string) := nil;
  Addr(data.set_string) := nil;
  Addr(data.get_string) := @get_string;
  Addr(data.get_count) := @strings_count;

  SetLength(Result, GetEncodeSize(@data));
  Encode(@data, Pointer(Result));
 end;
end;

function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
var
 data: TPluginData;
begin
 Result := nil;
 if PluginHandle <> 0 then
 begin
  New(Result, Init);
  strings := Result;

  data.rom := ROM;
  data.rom_size := RomSize;
  data.parameter := Sz;
  data.char_size := Align;
  Addr(data.add_string) := @add_string;
  Addr(data.set_string) := @set_string;
  Addr(data.get_string) := @get_string;
  Addr(data.get_count) := @strings_count;
  data.original_string_size := 0;
  Decode(@data, X);

  OriginalSize := data.original_string_size;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.


