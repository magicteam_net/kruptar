library pirates_smd;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'SMD Pirates! Gold';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0; // Gets string length and flags for GetData
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Integer; stdcall;
begin
 Result := 0; // Return original string size if it differs from result 
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmC2S1eSne;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
 I: Integer;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  Result := Char(Count) + Char(Count shr 8);
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   for I := 0 to Align - 1 do
    Result := Result + #0;
   R := R^.Next;
  end;
 end;
end;

function SwapWord(X: Word): Word;
asm
 xchg al,ah
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
const
 Scan: array[0..7] of Byte = (0,0,0,0,0,0,0,0);
var
 WS: WideString;
 I, Y, Max: Integer;
 P: PByte;
 PW: PWord;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 PW := Addr(ROM[X]);
 Inc(X, 2);
 Max := RomSize - Align; 
 P := Addr(ROM[X]);
 for I := 0 to SwapWord(PW^) - 1 do
  with Result.Add^ do
  begin
   case Align of
    0: ;
    1:
    begin
     Str := PChar(P);
     Inc(P, Length(Str) + 1);
    end;
    2:
    begin
     WS := PWideChar(P);
     X := Length(WS) * 2;
     SetLength(Str, X);
     Move(Pointer(WS)^, Pointer(Str)^, X);
     Inc(P, Length(Str) + 2);
    end;
    else
    begin
     Y := X;
     while Y <= Max do
     begin
      if CompareMem(P, @Scan, Align) then
       Break;
      Inc(Y, Align);
      Inc(P, Align);
     end;
     Y := (Y - X) + Align;
     SetLength(Str, Y);
     Move(ROM[X], Pointer(Str)^, Y);
     Inc(Y, Align);
     Inc(P, Y);
     Inc(X, Y);
    end;
   end;
  end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.


