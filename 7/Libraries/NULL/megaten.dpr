library megaten;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Megami Tensei (NES)';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 P: PByte;
 B: Byte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  P := Addr(ROM[X]);
  while X < RomSize do
  begin
   B := P^;
   Str := Str + Char(B);
   case B of
    $F0:
    begin
     Inc(P);
     Str := Str + Char(P^);
     Break;    
    end;
    $F3, $F4, $F8, $FC:
    begin
     Inc(P);
     Str := Str + Char(P^);
     Inc(P);
     Str := Str + Char(P^);
     if B = $F8 then
     begin
      Inc(P);
      Str := Str + Char(P^);
     end;
     Break;
    end;
    $FB:
    begin
     Inc(P);
     if P^ in [0..3] then
      Str := Str + Char(P^);
     Break;
    end;
   end;
   Inc(P);
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

