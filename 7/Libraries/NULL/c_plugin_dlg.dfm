object SelectPluginDialog: TSelectPluginDialog
  Left = 336
  Top = 246
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Select C-Plugin'
  ClientHeight = 94
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 8
    Width = 313
    Height = 50
    Shape = bsFrame
  end
  object SelectButton: TSpeedButton
    Left = 288
    Top = 24
    Width = 23
    Height = 21
    Caption = '...'
    OnClick = SelectButtonClick
  end
  object OkButton: TButton
    Left = 86
    Top = 64
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object PluginEdit: TKruptarEdit
    Left = 16
    Top = 24
    Width = 265
    Height = 21
    TabOrder = 1
  end
  object CancelButton: TButton
    Left = 167
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object OpenDialog: TKruptarOpenDialog
    DefaultExt = 'dll'
    Filter = '*.dll'
    Options = [ofHideReadOnly, ofOldStyleDialog, ofEnableSizing]
    Left = 280
    Top = 64
  end
end
