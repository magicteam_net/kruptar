library shadowgate;

uses
  ShareMem, SysUtils, Compression,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Shadowgate (NES) text recompressor.';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

type
 TFreqList = array[Byte] of Cardinal;

var
 StrCnt: Integer = 259;
 BlkIdx: Integer = -1;
 InsStrCnt: Integer = 0;
 InsBlkIdx: Integer = 0;
 Compressed: Boolean = False;
 StringsList: array of PTextStrings;
 Freqs: array of TFreqList;
 TextNode: PTextString;

// Huffman compression
function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 LeftBuffer, RightBuffer, CodesBuffer: array of Byte;
 Index, NodesCount: Integer;

 function FillTree(Node: PHuffTreeNode): Boolean;
 begin
  if Node <> NIL then with Node^ do
  begin
   if (Left <> NIL) and (Right <> NIL) then
   begin
    if Position >= 0 then
    case Direction of
     diLeft:   LeftBuffer[Position] := Left.Position;
     diRight: RightBuffer[Position] := Right.Position;
    end;
    Result := FillTree(Left) and FillTree(Right);
   end else
   begin
    if Position >= 0 then
    begin
     case Direction of
      diLeft:   LeftBuffer[Position] := Index + 128;
      diRight: RightBuffer[Position] := Index + 128;
     end;
    end;
    CodesBuffer[Index] := Value;
    Inc(Index);
    Result := True;
   end;
  end else Result := False;
 end; // FillTree

var
 I, J, X, BitPos: Integer;
 S: String;
 N: PTextString;
 FreqsPtr: ^TFreqList;
 Tree: THuffmanTree;
 Data: PByte;
 DataMask: Byte;

 function WriteBitStream(Value: Byte): Integer;
 var
  Mask: Cardinal;
 begin
  with Tree.BitStreams[Value] do
  begin
   Result := Size;
   Mask := 1 shl (Size - 1);
   while Mask > 0 do
   begin
    if Bits and Mask <> 0 then
     Data^ := Data^ or DataMask;
    Mask := Mask shr 1;
    DataMask := DataMask shl 1;
    if DataMask = 0 then
    begin
     Inc(Data);
     Data^ := 0;
     DataMask := 1;
    end;
   end;
  end;
 end; // WriteBitStream
// GetData
begin
 if (BlkIdx = 2) and (StrCnt = 259) then
 begin
  if not Compressed then
  begin
   for I := 0 to 2 do
   begin
    Tree := THuffmanTree.Create(Freqs[I]);
    with Tree do
    try
     NodesCount := Length(Branches);
     SetLength(LeftBuffer, NodesCount);
     SetLength(RightBuffer, NodesCount);
     SetLength(CodesBuffer, LeavesCount);
     Index := 0;
     if FillTree(Trunk) then with StringsList[I]^ do
     begin
      N := Root;
      for X := 0 to 255 do with N^ do
      begin
       S := Str;
       SetLength(Str, Length(Str) * 2);
       Data := Pointer(Str);
       Data^ := 0;
       DataMask := 1;
       BitPos := 0;
       for J := 1 to Length(S) do
        Inc(BitPos, WriteBitStream(Byte(S[J])));
       SetLength(Str, (BitPos + 7) shr 3);
       N := Next;
      end;
      with Add^ do
      begin
       SetLength(Str, NodesCount);
       Move(Pointer(LeftBuffer)^, Pointer(Str)^, NodesCount);
      end;
      with Add^ do
      begin
       SetLength(Str, NodesCount);
       Move(Pointer(RightBuffer)^, Pointer(Str)^, NodesCount);
      end;
      with Add^ do
      begin
       SetLength(Str, LeavesCount);
       Move(Pointer(CodesBuffer)^, Pointer(Str)^, LeavesCount);
      end;
     end;
    finally
     Tree.Free;
    end;
   end;
   TextNode := nil;
   InsBlkIdx := 0;
   InsStrCnt := 259;
   Compressed := True;
  end;
  if InsStrCnt = 259 then
  begin
   TextNode := StringsList[InsBlkIdx].Root;
   Inc(InsBlkIdx);   
   InsStrCnt := 0;
  end;
  Inc(InsStrCnt);
  Result := TextNode.Str;
  TextNode := TextNode.Next;
  if (InsBlkIdx = 3) and (InsStrCnt = 259) then
  begin
   for I := 0 to BlkIdx do
    Dispose(StringsList[I], Done);
   Finalize(StringsList);
   Finalize(Freqs);
   BlkIdx := -1;
   StrCnt := 259;
   Compressed := False;
  end;
 end else
 begin
  if StrCnt = 259 then
  begin
   Inc(BlkIdx);
   SetLength(StringsList, BlkIdx + 1);
   SetLength(Freqs, BlkIdx + 1);
   New(StringsList[BlkIdx], Init);
   StrCnt := 0;
  end;
  with StringsList[BlkIdx]^ do
  if StrCnt < 256 then
  begin
   S := TextStrings.Root.Str + #0;
   I := 0;
   N := Root;
   while N <> nil do
   begin
    if N.Str = S then Break;
    Inc(I);
    N := N.Next;
   end;
   if I = Count then
   begin
    FreqsPtr := Addr(Freqs[BlkIdx]);
    for I := 1 to Length(S) do
     Inc(FreqsPtr[Byte(S[I])]);
   end;
   Add.Str := S;
  end;
  Inc(StrCnt);
  Result := '';
 end;
end;

(*  // Original 5 bit compression
function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 P, Data: PByte; DataMask: Byte;

 procedure WriteBits(Value, Count: Integer);
 var
  Mask: Byte; L: Integer;
 begin
  Mask := 1 shl (Count - 1);
  while Mask > 0 do
  begin
   if DataMask = 0 then
   begin
    L := Length(Result) + 1;
    SetLength(Result, L);
    Data := Addr(Result[L]);
    Data^ := 0;
    DataMask := $80;
   end;
   if Value and Mask <> 0 then
    Data^ := Data^ or DataMask;
   Mask := Mask shr 1;
   DataMask := DataMask shr 1;
  end;
 end;

var
 R: PTextString;
 L: Integer;
 C: Byte;
begin
 Result := '';
 if TextStrings = NIL then Exit;
 with TextStrings^ do
 begin
  R := Root;
  while R <> NIL do with R^ do
  begin
   P := Pointer(Str);
   L := Length(Str);
   DataMask := 0;
   while L > 0 do
   begin
    C := P^;
    WriteBits(C, 5);
    Inc(P);
    Dec(L);
    if C = $1E then
    begin
     C := P^;
     WriteBits(C, 5);
     Inc(P);
     Dec(L);
     case C of
      $1C, $1D:
      begin
       WriteBits(P^, 8);
       Inc(P);
       Dec(L);
      end;
      $1E, $1F:
      begin
       Result := Result + Char(P^);
       Inc(P);
       Result := Result + Char(P^);
       Inc(P);
       Dec(L, 2);
       DataMask := 0;
      end;
     end;
    end;
   end;
   WriteBits($1F, 5);
   R := Next;
  end;
 end;
end;
   *)
function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 B, C: Byte; P: PByte; BitsRead: Integer;

 function ReadBits(Count: Integer): Integer;
 var
  I: Integer;
 begin
  Result := 0;
  for I := 0 to Count - 1 do
  begin
   if BitsRead >= 8 then
   begin
    BitsRead := 0;
    B := P^;
    Inc(P);
   end;
   Result := Result shl 1;
   if B and $80 <> 0 then
    Result := Result or 1;
   B := B shl 1;
   Inc(BitsRead);
  end;
 end;

begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  BitsRead := 8;
  P := Addr(ROM[X]);
  repeat
   C := ReadBits(5);
   case C of
    $1E:
    begin
     C := ReadBits(5);
     Str := Str + #$1E + Char(C);
     case C of
      $1C, $1D: Str := Str + Char(ReadBits(8));
      $1E, $1F:
      begin
       Str := Str + Char(P^);
       Inc(P);
       Str := Str + Char(P^);
       Inc(P);
       BitsRead := 8;
      end;
     end;
    end;
    $1F: Break;
    else Str := Str + Char(C);
   end;
  until False;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

