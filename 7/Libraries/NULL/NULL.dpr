library NULL;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'NULL';

var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0; // Gets string length and flags for GetData
 Align: LongInt = 1;

function Description: RawByteString; stdcall;
begin
 Result := AnsiString(SKPLDescription);
end;

function NeedEnd: LongInt; stdcall;
begin
 Result := 0; // Return original string size if it differs from result
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): RawByteString; stdcall;
var
 R: PTextString;
 I: LongInt;
begin
 Result := '';
 if (TextStrings = nil) or (Align <= 0) then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   for I := 0 to Align - 1 do
    Result := Result + #0;
   R := R^.Next;
  end;

  if MaxCodes > 0 then
  begin
   I := MaxCodes div Align;
   if (Length(Result) > I) then
    SetLength(Result, I);
  end;
 end;
end;

function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
//const
// Scan: array[0..7] of Byte = (0,0,0,0,0,0,0,0);
var
 WS: WideString;
 Y, Max: LongInt;
 P: PByte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  case Align of
   0: ;
   1: Str := AnsiString(PAnsiChar(Addr(ROM[X])));
   2:
   begin
    WS := PWideChar(Addr(ROM[X]));
    X := Length(WS) * 2;
    SetLength(Str, X);
    Move(Pointer(WS)^, Pointer(Str)^, X);
   end;
   else
   begin
    P := Addr(ROM[X]);
    Y := X;
    Max := RomSize - Align;
    while Y <= Max do
    begin
     if Int64(Pointer(P)^) = 0 then
//     if CompareMem(P, @Scan, Align) then
      Break;
     Inc(Y, Align);
     Inc(P, Align);
    end;
    Y := (Y - X) + Align;
    SetLength(Str, Y);
    Move(ROM[X], Pointer(Str)^, Y);
   end;
  end;

  if (Sz > 0) and (Align > 0) then
  begin
   Sz := Sz div Align;
   if Length(Str) > Sz then
    SetLength(Str, Sz);
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.


