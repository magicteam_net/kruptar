library zelda_gbc;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Zelda GBC';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 P: PChar;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  P := Addr(ROM[X]);
  while P^ <> #0 do
  begin;
   Str := Str + P^;
   if (P^ > #1) and (P^ <= #10) then
   begin
    Inc(P);
    Str := Str + P^;
   end;
   Inc(P);
  end;
  end;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 R := TextStrings.Root;
 if R <> NIL then Result := R.Str + #0;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

