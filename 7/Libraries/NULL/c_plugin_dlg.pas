unit c_plugin_dlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, KruptarComponents,
  {$IFNDEF FORCED_UNICODE}
   TntSysUtils,
  {$ENDIF}
  MyUtils,
  MyClassesEx;

type
  TSelectPluginDialog = class(TForm)
    SelectButton: TSpeedButton;
    OkButton: TButton;
    OpenDialog: TKruptarOpenDialog;
    PluginEdit: TKruptarEdit;
    Bevel: TBevel;
    CancelButton: TButton;
    procedure SelectButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type
 TStringAdd = function(str: Pointer; size: LongInt): Integer; stdcall;
 TStringSet = procedure(index: Integer; str: Pointer; size: LongInt); stdcall;
 TStringGet = function(index: Integer; var size: LongInt): Pointer; stdcall;
 TStringsCount = function: LongInt; stdcall;

 PPluginData = ^TPluginData;
 TPluginData = packed record
  rom: Pointer;
  rom_size: LongInt;
  
  parameter: LongInt;
  char_size: LongInt;

  add_string: TStringAdd;
  set_string: TStringSet;
  get_string: TStringGet;

  get_count: TStringsCount;

  original_string_size: LongInt;
 end;

 TDecodeProc = procedure(Data: PPluginData; Offset: LongInt); stdcall;
 TGetEncodeSize = function(Data: PPluginData): LongInt; stdcall;
 TEncodeProc = procedure(Data: PPluginData; Buffer: Pointer); stdcall;  

var
 Decode: TDecodeProc;
 Encode: TEncodeProc;
 GetEncodeSize: TGetEncodeSize;

 PluginHandle: THandle = 0;

function SelectPlugin(const Default: WideString): WideString;

implementation

{$IFDEF FORCED_UNICODE}
uses
  UITypes;
{$ENDIF}

{$R *.dfm}

function SelectPlugin(const Default: WideString): WideString;
var
 dlg: TSelectPluginDialog;
begin
 dlg := TSelectPluginDialog.Create(Application);
 try
  dlg.PluginEdit.Text := Default;
  if dlg.ShowModal = mrOk then
   Result := dlg.PluginEdit.Text else
   Result := '';
 finally
  dlg.Free;
 end;
end;

procedure TSelectPluginDialog.SelectButtonClick(Sender: TObject);
begin
 if OpenDialog.Execute then
  PluginEdit.Text := OpenDialog.FileName;
end;

{$IFDEF FORCED_UNICODE}
function WideMessageDlg(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint; DefaultButton: TMsgDlgBtn): Integer; overload;
begin
  Result := MessageDlg(Msg, DlgType, Buttons, HelpCtx, DefaultButton);
end;
{$ENDIF}

procedure TSelectPluginDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := (ModalResult <> mrOk) or WideFileExists(PluginEdit.Text);
 if not CanClose then
  WideMessageDlg('Plugin file name is invalid', mtError, [mbOK], 0, mbOk);
end;

procedure LoadPlugin;
var
 str: WideString;
 IniName: WideString;
 Buffer: array[0..MAX_PATH] of WideChar;
begin
 GetModuleFileNameW(hInstance, Buffer, Length(Buffer));
 IniName := WideExtractFilePath(PWideChar(@Buffer)) + 'c_plugin.ini';
 try
  with TStreamIniFileW.Create do
  try
   try
    LoadFromFile(IniName);
   except
    // do nothing
   end;
   str := SelectPlugin(ReadString('C_Plugin', 'PluginName', ''));
   WriteString('C_Plugin', 'PluginName', str);
   try
    SaveToFile(IniName);
   except
    // do nothing
   end;
   PluginHandle := LoadLibraryW(Pointer(str));
   if PluginHandle <> 0 then
   begin
    @Decode := GetProcAddress(PluginHandle, 'Decode');
    @Encode := GetProcAddress(PluginHandle, 'Encode');
    @GetEncodeSize := GetProcAddress(PluginHandle, 'GetEncodeSize');;

    if (Addr(Decode) = nil) or
       (Addr(Encode) = nil) or
       (Addr(GetEncodeSize) = nil) then
    begin
     FreeLibrary(PluginHandle);
     PluginHandle := 0;
     raise Exception.Create('');
    end;
   end else
    raise Exception.Create('');
  finally
   Free;
  end;
 except
  WideMessageDlg('Error loading plugin', mtError, [mbOk], 0, mbOk);
 end;
end;


initialization
 LoadPlugin;
finalization
 if PluginHandle <> 0 then
  FreeLibrary(PluginHandle);
end.
