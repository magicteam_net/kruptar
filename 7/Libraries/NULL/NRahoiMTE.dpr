library NRahoiMTE;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'NULL';

Var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0;
 Align: LongInt = 1;

Function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: LongInt; stdcall;
begin
 Result := 0;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
Var R: PTextString;
    MTELengthOffset: Integer;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  MTELengthOffset := $4833E;
  While R <> NIL do
  begin
   ROM[MTELengthOffset] := Length(R.Str);
   Inc(MTELengthOffset);
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 MY: Byte;
 I, LEN, MTELengthOffset: Integer;
 Wrd: PTextString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 MTELengthOffset := $4833E;
 Wrd := Result.Add;
 I := 0;
 LEN := ROM[MTELengthOffset]; //��������� ����� ������� �����
 while I < Sz do // ���� I ������ ���������� ���� � ����� ������ ����
 begin
  MY := ROM[X]; //��������� ������ �� ����
  Inc(X); //����������� ������� � ���� �� ����
  Dec(LEN); //��������� ����� �� 1 ����, ��� ��� ���� ���� �� �������
  Wrd.Str := Wrd.Str + Char(MY); // ���� ������ �� ������ �� ���, �� ������ ���������� ���
  if LEN = 0 then
  begin
   if I < Sz - 1 then
    Wrd := Result.Add; //��������� ����� ����� � ������ ����
   Inc(I); //���� ����� ��������� �������, �� ����������� ������ �����
   Inc(MTELengthOffset); //����������� ������ ����
   LEN := ROM[MTELengthOffset]; //��������� ����� �����
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

