library SaintSeiyaAttacks;

uses
  ShareMem, HexUnit,
  SysUtils, Math,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Saint Seiya - Ougon Densetsu Kanketsu Hen (NES)';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
type
 TPtrRecord = packed record
  prPPU_Ofs: Word;
  prLinePtr: Word;
 end;
var
 R: PTextString;
 P: PByte;
 S: AnsiString;
 PtrList: AnsiString;
 PtrRunner: ^TPtrRecord;
 InsOfs, L, LL: Integer;
begin
 Result := '';
 if TextStrings = NIL then Exit;
 with TextStrings^ do
 begin
  SetLength(PtrList, Count * SizeOf(TPtrRecord));
  InsOfs := $85E9 + Length(PtrList);
  PtrRunner := Pointer(PtrList);
  R := Root;
  while R <> NIL do with R^ do
  begin
   P := Pointer(Str);
   L := Length(Str);
   S := '';
   if P^ = $7F then
   begin
    Inc(P);
    while P^ <> $7F do
    begin
     if P^ in [$80..$89] then
      S := S + Char(P^ - $50) else
      S := S + Char(P^ - $49);
     Inc(P);
    end;
    Inc(P);
    Dec(L, Length(S) + 2);
   end;
   with PtrRunner^ do
   begin
    prPPU_Ofs := SwapWord(HexToInt(S));
    prLinePtr := InsOfs;
   end;
   Inc(PtrRunner);
   Inc(InsOfs, L + 1);   
   Result := Result + Char(L);
   LL := Length(Result);
   SetLength(Result, LL + L);
   Move(P^, Result[LL + 1], L);
   R := Next;
  end;
  Result := PtrList + Result;
 end;
end;

function SSIntToHex(Value: Word): AnsiString;
var
 I: Integer;
 P: PByte;
begin
 Result := IntToHex(Value, 4);
 P := Pointer(Result);
 for I := 0 to Length(Result) - 1 do
 begin
  if P^ in [$30..$39] then
   Inc(P^, $50) else
   Inc(P^, $49);
  Inc(P);
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
type
 TPtrRecord = packed record
  prPPU_Ofs1: Word;
  prLinePtr1: Word;
  prPPU_Ofs2: Word;
  prLinePtr2: Word;
 end;
var
 PtrRunner: ^TPtrRecord;
 Top, Bottom: PByte;
 L1, L2, I, J: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X <> $185F9) then Exit;
 PtrRunner := Addr(ROM[X]);
 New(Result, Init);
 for I := 0 to 27 do
 begin
  with Result.Add^, PtrRunner^ do
  begin
   Str := #$7F + SSIntToHex(SwapWord(prPPU_Ofs1)) + #$7F;
   Top := Addr(ROM[prLinePtr1 + $10010]);
   L1 := Top^;
   Inc(Top);
   Bottom := Addr(ROM[prLinePtr2 + $10010]);
   L2 := Bottom^;
   Inc(Bottom);
   for J := 0 to Max(L1, L2) - 1 do
   begin
    if J < L2 then
    begin
     Str := Str + Char(Bottom^);
     Inc(Bottom);
    end;
    if J < L1 then
    begin
     if Top^ <> 1 then
      Str := Str + Char(Top^);
     Inc(Top);
    end;
   end;
  end;
  Inc(PtrRunner);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

