library NRahoi;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'NULL';

Var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0;
 Align: LongInt = 1;

Function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: LongInt; stdcall;
begin
 Result := 0;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
Var R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
   with R^ do
   begin
    if (Str <> '') and (Byte(Str[Length(Str)]) >= $F6) then
     Result := Result + Str else
     Result := Result + Str + #$C4;
    R := Next;
   end;
 end;
end;

Function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
Var
 MY: byte;
 I: Integer;
 Wrd: PTextString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 Wrd := Result.Add;
 I := 0; //��������� �������� ������� �����
 while I < Sz do // ���� I ������ ���������� ���� � ����� ������ ����
 begin
  MY := ROM[X]; //��������� ������ �� ����
  Inc(X); //����������� ������� � ���� �� ����
  if MY <> $C4 then
   Wrd.Str := Wrd.Str + Char(MY);
  if MY in [$C4, $F6..$FB] then
  begin
   Inc(I); //���� ������ ����� ���� ��������� �����, �� ����������� ������ �����
   if I < Sz then
    Wrd := Result.Add;
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

