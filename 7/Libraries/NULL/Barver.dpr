library Barver;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Barver Battle Saga';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
const
 WidthTable: array[#$00..#$EF] of Byte =
 (
  $04,$02,$04,$07,$05,$0B,$07,$02,$03,$03,$05,$07,$02,$04,$02,$04,
  $06,$06,$06,$06,$06,$06,$06,$06,$06,$06,$02,$02,$06,$06,$06,$05,
  $0A,$07,$06,$06,$07,$06,$05,$07,$07,$03,$04,$06,$05,$08,$07,$08,
  $06,$08,$06,$06,$07,$07,$07,$0B,$06,$07,$06,$03,$04,$03,$07,$07,
  $03,$05,$06,$05,$06,$06,$04,$06,$06,$02,$03,$05,$02,$09,$06,$06,
  $06,$06,$04,$04,$04,$06,$05,$09,$05,$05,$04,$05,$02,$05,$07,$0A,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00);
 WidthTableSmall: array[#$00..#$EF] of Byte =
 (
  $03,$01,$03,$06,$04,$06,$05,$01,$02,$02,$05,$05,$02,$04,$01,$06,
  $04,$04,$04,$04,$04,$04,$04,$04,$04,$04,$01,$02,$03,$04,$03,$04,
  $05,$04,$04,$04,$04,$04,$04,$04,$04,$03,$03,$04,$04,$05,$05,$04,
  $04,$04,$04,$04,$05,$04,$05,$05,$05,$05,$04,$02,$06,$02,$04,$05,
  $03,$04,$04,$04,$04,$04,$04,$04,$04,$01,$02,$04,$01,$05,$04,$04,
  $04,$04,$04,$04,$04,$04,$04,$05,$04,$04,$03,$03,$01,$03,$04,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,
  $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00);

var
 R: PTextString; Wtbl: PBytes;
 OWC, WidthCounter, Return: Integer;
 Block, CuttedBlock: Char; P: PChar;
 S, CuttedStr, LineBreaker: AnsiString;
 MaxWidth: Integer;
function CutLastWord: AnsiString;
var
 PP, LastSpace: PChar;
 XBlock: Char;
 I, L: Integer;
begin
 Result := '';
 OWC := WidthCounter;
 WidthCounter := 0;
 if not ((Block = #$F8) and (P^ = #0) and (OWC <= MaxWidth)) then
 begin
  L := Length(CuttedStr);
  XBlock := CuttedBlock;
  LastSpace := NIL;
  PP := Pointer(CuttedStr);
  for I := 1 to L do
  begin
   if PP^ in [#$F0..#$F8] then XBlock := PP^ else
   begin
    if (XBlock = #$F8) and (PP^ in [#0, #$0D]) then
    begin
     LastSpace := PP;
     WidthCounter := 0;
     CuttedBlock := XBlock;
    end else
    begin
     if XBlock = #$F8 then
      Inc(WidthCounter, WTbl[Byte(PP^)] + 1) else
      Inc(WidthCounter, 16);
    end;
   end;
   Inc(PP);
  end;
  if LastSpace = NIL then
  begin
   LastSpace := PP;
   Dec(LastSpace);
   while LastSpace^ >= #$F0 do
    Dec(LastSpace);
  end;
  L := Integer(LastSpace) - Integer(Pointer(CuttedStr));
  If LastSpace^ <> #0 then Inc(L);
  Inc(LastSpace);
  if Integer(LastSpace) < Integer(PP) then
  begin
   if LastSpace^ in [#$F0..#$F8] then CuttedBlock := LastSpace^;
   I := Integer(PP) - Integer(LastSpace);
   SetLength(Result, I);
   Move(LastSpace^, Pointer(Result)^, I);
  end;
  SetLength(CuttedStr, L);
  CuttedStr := CuttedStr + LineBreaker;
 end else CuttedStr := CuttedStr + LineBreaker;
end;
var
 I, L: Integer; WordWrap: Boolean;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 with TextStrings^ do
 begin
  R := Root;
  if R <> NIL then
  begin
   P := Pointer(R.Str);
   WidthCounter := 0;
   CuttedStr := '';
   Block := #$F8;
   CuttedBlock := #$F8;
   Return := 0;
   L := Length(R.Str);
   WordWrap := True;
   Wtbl := @WidthTable;
   LineBreaker := #$FE;
   MaxWidth := 192; //193;
   if P^ = #$F9 then
   begin
    Dec(L);
    Inc(P);
    if P^ = #$F9 then
    begin
     Dec(L);
     Inc(P);
     Wtbl := @WidthTableSmall;
     LineBreaker := LineBreaker + #$00;
     MaxWidth := 64; //65;
    end else WordWrap := False;
   end;
   for I := 1 to L do
   begin
    if Return > 0 then
    begin
     Result := Result + P^;
     if P^ = #$FF then
     begin
      Inc(Return);
      if Return = 3 then Return := 0;
     end else Return := 0;
    end else
    if WordWrap then
    begin
     if P^ >= #$FD then
     begin
      if WidthCounter > MaxWidth then
      begin
       S := CutLastWord;
       Result := Result + CuttedStr;
       CuttedStr := S;
      end;
      Result := Result + CuttedStr + P^;
      WidthCounter := 0;
      CuttedStr := '';
      CuttedBlock := Block;
      if P^ = #$FF then
       Inc(Return) else
       Return := 0;
     end else
     if P^ >= #$FA then CuttedStr := CuttedStr + P^ else
     if P^ >= #$F0 then
     begin
      if Block <> P^ then
      begin
       Block := P^;
       CuttedStr := CuttedStr + Block;
      end
     end else
     begin
      if WidthCounter >= MaxWidth then
      begin
       S := CutLastWord;
       Result := Result + CuttedStr;
       CuttedStr := S;
       if (OWC <= MaxWidth) and (Block = #$F8) and (P^ = #0) then
       begin
        Inc(P);
        Continue;
       end;
      end;
      CuttedStr := CuttedStr + P^;
      if Block = #$F8 then
       Inc(WidthCounter, WTbl[Byte(P^)] + 1) else
       Inc(WidthCounter, 16);
     end;
    end else //if not WordWrap
    begin
     if P^ >= #$FD then
     begin
      Result := Result + P^;
      if P^ = #$FF then
       Inc(Return) else
       Return := 0;
     end else
     if P^ >= #$FA then
      Result := Result + P^ else
     if P^ >= #$F0 then
     begin
      if Block <> P^ then
      begin
       Block := P^;
       Result := Result + Block;
      end
     end else
      Result := Result + P^;
    end;
    Inc(P);
   end;
  end else Result := #$FF#$00;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 P: PChar; Block: Char; Return: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  P := Addr(ROM^[X]);
  Str := '';
  Block := #$F8;
  Return := 0;
  Repeat
   if Return > 0 then
   begin
    Str := Str + P^;
    if P^ = #$FF then
    begin
     Inc(Return);
     if Return = 3 then Return := 0;
    end else
    if Return > 1 then
     Return := 0 else
     Break;
   end else
   if P^ = #$FF then
   begin
    Return := 1;
    Str := Str + #$FF;
    if Sz = 1 then Break;
   end else
   if P^ >= #$FA then
   begin
    Str := Str + P^;
   end else
   if P^ >= #$F0 then Block := P^ else
   begin
    Str := Str + Block + P^;
   end;
   Inc(P);
  Until False;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

