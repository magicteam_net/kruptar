library gateway;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Shadowgate (NES) text recompressor.';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str + #0;
   R := R^.Next;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 B, C: Byte; P: PByte; BitsRead: Integer;

 function ReadBits(Count: Integer): Integer;
 var
  I: Integer;
 begin
  Result := 0;
  for I := 0 to Count - 1 do
  begin
   if BitsRead >= 8 then
   begin
    BitsRead := 0;
    B := P^;
    Inc(P);
   end;
   Result := Result shl 1;
   if B and $80 <> 0 then
    Result := Result or 1;
   B := B shl 1;
   Inc(BitsRead);
  end;
 end;

begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  BitsRead := 8;
  P := Addr(ROM[X]);
  repeat
   C := ReadBits(5);
   case C of
    $1E:
    begin
     C := ReadBits(5);
     Str := Str + #$1E + Char(C);
     case C of
      $1C, $1D: Str := Str + Char(ReadBits(8));
      $1E, $1F:
      begin
       Str := Str + Char(P^);
       Inc(P);
       Str := Str + Char(P^);
       Inc(P);
       BitsRead := 8;       
      end;
     end;
    end;
    $1F: Break;
    else Str := Str + Char(C);
   end;
  until False;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

