library pirtates_smd;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Pirates! Gold SMD';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0; // Gets string length and flags for GetData
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Integer; stdcall;
begin
 Result := 0; // Return original string size if it differs from result 
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmC2S1eSne;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
 I: Integer;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   for I := 0 to Align - 1 do
    Result := Result + #0;
   R := R^.Next;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
const
 Scan: array[0..7] of Byte = (0,0,0,0,0,0,0,0);
var
 WS: WideString;
 Y, Max: Integer;
 P: PByte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  case Align of
   0: ;
   1: Str := PChar(Addr(ROM[X]));
   2:
   begin
    WS := PWideChar(Addr(ROM[X]));
    X := Length(WS) * 2;
    SetLength(Str, X);
    Move(Pointer(WS)^, Pointer(Str)^, X);
   end;
   else
   begin
    P := Addr(ROM[X]);
    Y := X;
    Max := RomSize - Align;
    while Y <= Max do
    begin
     if CompareMem(P, @Scan, Align) then
      Break;
     Inc(Y, Align);
     Inc(P, Align);
    end;
    Y := (Y - X) + Align;
    SetLength(Str, Y);
    Move(ROM[X], Pointer(Str)^, Y);
   end;
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.


