library Arcana;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Arcana';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;
 LastLen: Integer;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Integer; stdcall;
begin
 Result := LastLen;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str + #0;
   R := R^.Next;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 P: PChar;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 with Result.Add^ do
 begin
  P := Addr(ROM[X]);
  Str := '';
  repeat
   case P^ of
    #$00: Break;
    #2, #$03, #$06, #8, #$0A:
    begin
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
    end;
    #$01, #$05, #$1B:
    begin
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
    end;
    #$10, #$14:
    begin
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
    end;
    #$11:
    begin
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;
     Inc(P);
     Str := Str + P^;     
    end;
    else Str := Str + P^;
   end;
   Inc(P);
  until False;
  LastLen := Length(Str) + 1;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

