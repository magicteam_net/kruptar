library SaintSeiya;

uses
  ShareMem, Windows,
  SysUtils, MyClasses,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Saint Seiya - Ougon Densetsu Kanketsu Hen (NES)';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Procedure CommandLine(Line: AnsiString);
Const
 ReadBuffer = 2400;
Var
 Security: TSecurityAttributes;
 ReadPipe, WritePipe: THandle;
 Start: TStartUpInfo;
 ProcessInfo: TProcessInformation;
// Buffer: PChar;
// BytesRead: DWord;
 //S: AnsiString;
 AppRunning: DWord;
begin
 With Security do
 begin
  nlength := SizeOf(TSecurityAttributes);
  binherithandle := true;
  lpsecuritydescriptor := nil;
 end;
 If CreatePipe(ReadPipe, WritePipe, @Security, 0) then
 begin
//  Buffer := AllocMem(ReadBuffer + 1);
  FillChar(Start, Sizeof(Start), #0);
  start.cb := SizeOf(start);
  start.hStdOutput := WritePipe;
  start.hStdInput := ReadPipe;
  start.dwFlags := STARTF_USESTDHANDLES + STARTF_USESHOWWINDOW;
  start.wShowWindow := SW_HIDE;
  If CreateProcess(nil, PChar(Line), @Security, @Security, true,
                   NORMAL_PRIORITY_CLASS, nil, nil, start, ProcessInfo) then
  begin
   Repeat
    Apprunning := WaitForSingleObject(ProcessInfo.hProcess, 100);
 //   ReadFile(ReadPipe, Buffer[0], ReadBuffer, BytesRead, nil);
//   Buffer[BytesRead] := #0;
//    OemToAnsi(Buffer, Buffer);
//    AMemo.Text := AMemo.text + string(Buffer);
//    Application.ProcessMessages;
   until (Apprunning <> WAIT_TIMEOUT);
  end;
//  FreeMem(Buffer);
  CloseHandle(ProcessInfo.hProcess);
  CloseHandle(ProcessInfo.hThread);
  CloseHandle(ReadPipe);
  CloseHandle(WritePipe);
 end;
end;

const
 Code18F9B: AnsiString =
'$=$8F8B' + #13#10 +
'@@Loop:' + #13#10 +
'	jsr	$B7F0' + #13#10 +
'	cmp	#$01' + #13#10 +
'	beq	@@Space' + #13#10 +
'	cmp	#$A4' + #13#10 +
'	beq	@@NewLine' + #13#10 +
'	cmp	#$FF' + #13#10 +
'	bne	@@Char' + #13#10 +
'	lda	#$00' + #13#10 +
'	sta	$641' + #13#10 +
'	sta	$EC' + #13#10 +
'	jmp	$9067' + #13#10 +
'@@Space:' + #13#10 +
'	inc	$644' + #13#10 +
'	bne	@@Loop' + #13#10 +
'	inc	$645' + #13#10 +
'	bne	@@Loop' + #13#10 +
'@@Char:' + #13#10 +
'	ldx	$645' + #13#10 +
'	stx	$2006' + #13#10 + 
'	ldx	$644' + #13#10 + 
'	stx	$2006' + #13#10 + 
'	sta	$2007' + #13#10 + 
'	inc	$644' + #13#10 + 
'	bne	@@NoHighInc' + #13#10 + 
'	inc	$655' + #13#10 +
'@@NoHighInc:' + #13#10 + 
'	jmp	$9055' + #13#10 + 
'@@NewLine:' + #13#10 + 
'	lda	$646' + #13#10 + 
'	clc' + #13#10 +
'	adc	#$20' + #13#10 + 
'	sta	$644' + #13#10 + 
'	sta	$646' + #13#10 +
'	lda	$647' + #13#10 +
'	adc	#$00' + #13#10 +
'	sta	$645' + #13#10 +
'	sta	$647' + #13#10 +
'	jmp	$9067' + #13#10 +
'end';
 DecompressInitCode: AnsiString =
'$=$%x' + #13#10 +
'	tay' + #13#10 +
'	and	#$07' + #13#10 +
'	sta	$D3' + #13#10 +
'	tya' + #13#10 +
'	lsr' + #13#10 +
'	lsr' + #13#10 +
'	lsr' + #13#10 +
'	sta	$D2' + #13#10 +
'	ldx	#$FF' + #13#10 +
'	stx	$D4' + #13#10 +
'	rts' + #13#10 +
'end';
 DecompressPrepareCode: AnsiString =
'$=$B7F0' + #13#10 +
'lda #$15' + #13#10 +
'jsr $E55E' + #13#10 +
'lda #$17' + #13#10 +
'jsr $E575' + #13#10 +
'lda $641' + #13#10 +
'sec' + #13#10 +
'sbc #$02' + #13#10 +
'clc' + #13#10 +
'adc $D1' + #13#10 +
'tay' + #13#10 +
'lda $D2' + #13#10 +
'adc #$00' + #13#10 +
'sta $2006' + #13#10 +
'sty $2006' + #13#10 +
'lda $2007' + #13#10 +
'	lda	$D4' + #13#10 +
'	cmp	#$FF' + #13#10 +
'	bne	@@MainLoopStart' + #13#10 +
'	lda	$2007' + #13#10 +
'	inc	$641' + #13#10 +
'	ldx	#$08' + #13#10 +
'	ldy	$D3' + #13#10 +
'	beq	@@NoShift' + #13#10 +
'@@ShiftLoop:' + #13#10 +
'	lsr' + #13#10 +
'	dex' + #13#10 +
'	dey' + #13#10 +
'	bne	@@ShiftLoop' + #13#10 +
'@@NoShift:' + #13#10 +
'	sta	$D3' + #13#10 +
'	stx	$D4' + #13#10;
 DecompressCode: AnsiString =
'@@MainLoopStart:' + #13#10 +
'	ldx	#$00' + #13#10 +
'@@MainLoop:' + #13#10 +
'	lda	$D3' + #13#10 +
'	ror' + #13#10 +
'	sta	$D3' + #13#10 +
'	bcs	@@Right' + #13#10 +
'	lda	@@LeftNodes,x	' + #13#10 +
'	bcc	@@Continue' + #13#10 +
'@@Right:' + #13#10 +
'	lda	@@RightNodes,x' + #13#10 +
'@@Continue:' + #13#10 +
'	tax' + #13#10 +
'	dec	$D4' + #13#10 +
'	bne	@@Continue2' + #13#10 +
'	ldy	$2007' + #13#10 +
'	sty	$D3' + #13#10 +
'	ldy	#$08' + #13#10 +
'	sty	$D4' + #13#10 +
'	inc	$641' + #13#10 +
'@@Continue2:	' + #13#10 +
'	cpx	#$80' + #13#10 +
'	bcc	@@MainLoop' + #13#10 +
'	lda	@@Chars-128,x' + #13#10 +
'	rts' + #13#10 +
'@@LeftNodes:' + #13#10 +
'%s' + #13#10 +
'@@RightNodes:' + #13#10 +
'%s' + #13#10 +
'@@Chars:' + #13#10 +
'%s' + #13#10 +
'end';
 DecompressCodeAlt: AnsiString =
'@@MainLoopStart:' + #13#10 +
'	ldy	#$00' + #13#10 +
'@@MainLoop:' + #13#10 +
'	lda	$D3' + #13#10 +
'	ror' + #13#10 +
'	sta	$D3' + #13#10 +
'	bcs	@@Right' + #13#10 +
'	lda	@@LeftNodes,y' + #13#10 +
'	ldx	@@LeftAlt,y' + #13#10 +
'	bcc	@@Continue' + #13#10 +
'@@Right:' + #13#10 +
'	lda	@@RightNodes,y' + #13#10 +
'	ldx	@@RightAlt,y' + #13#10 +
'@@Continue:' + #13#10 +
'	dec	$D4' + #13#10 +
'	bne	@@Continue2' + #13#10 +
'	ldy	$2007' + #13#10 +
'	sty	$D3' + #13#10 +
'	ldy	#$08' + #13#10 +
'	sty	$D4' + #13#10 +
'	inc	$641' + #13#10 +
'@@Continue2:	' + #13#10 +
'	tay' + #13#10 +
'	cpx	#$00' + #13#10 +
'	beq	@@MainLoop' + #13#10 +
'@@Found:' + #13#10 +
'	lda	@@Chars,y' + #13#10 +
'	rts' + #13#10 +
'@@LeftNodes:' + #13#10 +
'%s' + #13#10 +
'@@LeftAlt:' + #13#10 +
'%s' + #13#10 +
'@@RightNodes:' + #13#10 +
'%s' + #13#10 +
'@@RightAlt:' + #13#10 +
'%s' + #13#10 +
'@@Chars:' + #13#10 +
'%s' + #13#10 +
'end';
 InitInplaceCode: AnsiString =
'$=$ED54' + #13#10 +
'	jmp	$%x' + #13#10 +
'end';

function BufToHex(const Buf: array of Byte): AnsiString;
var
 I: Integer;
begin
 Result := #9 + '.BYTE' + #9;
 for I := 0 to Length(Buf) - 2 do
 begin
  Result := Result + '$' + IntToHex(Buf[I], 2);
  if I and 15 = 15 then
   Result := Result + #13#10 + #9 + '.BYTE' + #9 else
   Result := Result + ',';
 end;
 Result := Result + '$' + IntToHex(Buf[Length(Buf) - 1], 2);
end;

const
 TempAsm: AnsiString = '$SSkplgFile$.asm';
 TempBin: AnsiString = '$SSkplgFile$.bin';

function Compile(const Code: AnsiString): AnsiString;
var
 Txt: TextFile;
 Bin: File;
 SZ: Integer;
begin
 AssignFile(Txt, TempAsm);
 Rewrite(Txt);
 try
  Write(Txt, Code);
 finally
  CloseFile(Txt);
 end;
 CommandLine('asm6 ' + TempAsm);
 DeleteFile(TempAsm);
 AssignFile(Bin, TempBin);
 Reset(Bin, 1);
 try
  SZ := FileSize(Bin);
  SetLength(Result, SZ);
  BlockRead(Bin, Pointer(Result)^, SZ);
 finally
  CloseFile(Bin);
 end;
 DeleteFile(TempBin);
end;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;

 var
  LeftBuffer, RightBuffer,
  LeftAlt, RightAlt,
  CodesBuffer: array of Byte;
  Index, NodesCount: Integer;
  Alt: Boolean;

 function FillTree(Node: PHuffTreeNode): Boolean;
 begin
  if Node <> NIL then with Node^ do
  begin
   if (Left <> NIL) and (Right <> NIL) then
   begin
    if Position >= 0 then
    case Direction of
     diLeft:   LeftBuffer[Position] := Left.Position;
     diRight: RightBuffer[Position] := Right.Position;
    end;
    Result := FillTree(Left) and FillTree(Right);
   end else
   begin
    if Position >= 0 then
    begin
     if Alt then
     case Direction of
      diLeft:
      begin
       LeftBuffer[Position] := Index;
       LeftAlt[Position] := 1;
      end;
      diRight:
      begin
       RightBuffer[Position] := Index;
       RightAlt[Position] := 1;
      end;
     end else
     case Direction of
      diLeft:   LeftBuffer[Position] := Index + 128;
      diRight: RightBuffer[Position] := Index + 128;
     end;
    end;
    CodesBuffer[Index] := Value;
    Inc(Index);
    Result := True;
   end;
  end else Result := False;
 end;

 var
  R: PTextString;
  I, X: Integer;
  S: AnsiString;
  BitPos: Word;
  WP: PWord; //^WordRec;
  Tree: THuffmanTree;
  FreqList: array[Byte] of Cardinal;
  Data, Jump: PByte;
  DataMask: Byte;
function WriteBitStream(Value: Byte): Integer;
var
 Mask: Cardinal;
begin
 with Tree.BitStreams[Value] do
 begin
  Result := Size;
  Mask := 1 shl (Size - 1);
  while Mask > 0 do
  begin
   if Bits and Mask <> 0 then
    Data^ := Data^ or DataMask;
   Mask := Mask shr 1;
   DataMask := DataMask shl 1;
   if DataMask = 0 then
   begin
    Inc(Data);
    if Data = Jump then
    begin
     Inc(Data, $1000);
     FillChar(Data^, $1000, 255);
    end;
    Data^ := 0;
    DataMask := 1;
   end;
  end;
 end;
end;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  FillChar(FreqList, SizeOf(FreqList), 0);
  R := Root;
  while R <> NIL do with R^ do
  begin
   for I := 1 to Length(Str) do
    Inc(FreqList[Byte(Str[I])]);
   Inc(FreqList[255]);
   R := Next;
  end;
  Tree := THuffmanTree.Create(FreqList);
  with Tree do
  try
   if TotalBytesCount <= $2000 then
   begin
    Alt := LeavesCount > 128;
    NodesCount := Length(Branches);
    SetLength(LeftBuffer, NodesCount);
    SetLength(RightBuffer, NodesCount);
    SetLength(CodesBuffer, LeavesCount);
    if Alt then
    begin
     SetLength(LeftAlt, NodesCount);
     SetLength(RightAlt, NodesCount);
    end;
    Index := 0;
    if FillTree(Trunk) then
    begin
     S := Compile(Code18F9B);
     Move(Pointer(S)^, Pointer(Integer(ROM) + $18F9B)^, Length(S));
     if Alt then
      S := Compile(DecompressPrepareCode +
                   Format(DecompressCodeAlt,
                         [BufToHex(LeftBuffer),
                          BufToHex(LeftAlt),
                          BufToHex(RightBuffer),
                          BufToHex(RightAlt),
                          BufToHex(CodesBuffer)])) else
      S := Compile(DecompressPrepareCode +
                   Format(DecompressCode,
                         [BufToHex(LeftBuffer),
                          BufToHex(RightBuffer),
                          BufToHex(CodesBuffer)]));
     X := Length(S);
     Move(Pointer(S)^, Pointer(Integer(ROM) + $1B800)^, X);
     Inc(X, $B7F0);
     S := Compile(Format(InitInplaceCode, [X]));
     Move(Pointer(S)^, Pointer(Integer(ROM) + $1ED64)^, Length(S));
     S := Compile(Format(DecompressInitCode, [X])) + 'HEND';
     Move(Pointer(S)^, Pointer(Integer(ROM) + X + $10010)^, Length(S));

     Data := Pointer(Integer(ROM) + $35010);
     Jump := Pointer(Integer(ROM) + $36010);
     FillChar(Data^, $1000, 255);
     Data^ := 0;
     DataMask := 1;
     BitPos := 0;
     X := 0;
     SetLength(Result, (Count + 1) shl 1);
     WP := Pointer(Result);
     R := Root;
     while R <> NIL do with R^ do
     begin
      WP^ := Byte(BitPos shr 3) or (BitPos and $F800) or ((BitPos and 7) shl 8);
      Inc(WP);
      for I := 1 to Length(Str) do
       Inc(BitPos, WriteBitStream(Byte(Str[I])));
      Inc(BitPos, WriteBitStream(255));
      Inc(X);
      if X = 251 then
      begin
       WP^ := $A67B;
       Inc(WP);
      end;
      R := Next;
     end;
     if Alt then
      PCardinal(Integer(ROM) + $1B7FC)^ := $36353248 else
      PCardinal(Integer(ROM) + $1B7FC)^ := $38323148;
    end;
   end else SetLength(Result, (Count + 1) shl 1 + 1);
  finally
   Tree.Free;
  end;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 Ptr: PWord;
 TextBlock: array[0..$1FFF] of Byte;
 P: PChar;
 I: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 Move(Pointer(Integer(ROM) + $35010)^, TextBlock[$0000], $1000);
 Move(Pointer(Integer(ROM) + $37010)^, TextBlock[$1000], $1000);
 Ptr := Addr(ROM[X]);
 for I := 0 to 255 do
 begin
  if I <> 251 then
  with Result.Add^ do
  begin
   P := Addr(TextBlock[Ptr^]);
   while P^ <> #$FF do
   begin
    Str := Str + P^;
    Inc(P);
   end;
  end;
  Inc(Ptr);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

