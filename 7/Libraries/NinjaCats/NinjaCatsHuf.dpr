library NinjaCatsHuf;

uses
  ShareMem,
  Windows,
  SysUtils,
  Compression,
  Needs in '..\..\Needs.pas';

{$E .kpl}

const
 SKPLDescription = 'Cattou Ninden Teyandee (NES)';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: AnsiString; stdcall;
begin
 Result := SKPLDescription;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

const
 Y_Tbl: array[0..6] of Byte =
 ($23, $43, $63, $83, $A3, $C3, $E3);
// ($23, $63, $A3, $E3);
 ClrTbl: array[0..6] of Byte =
 ($3C, $4B, $5A, $69, $78, $87, $96);
 {$I OMsgInit.pas}
 {$I OutputMsg.pas}
 {$I OutputMsgAlt.pas}
 STR_CNT = 37;
 ProgInit = $E11C;
 ProgramStart = $ECA2;
 ProgramEnd = $EE69;
 CodePtrPos = $E14B;
 Y_TblPtrPos = $EE6D;
 ClrTblPos = $F012;
 LCPos =  $140+7;
 RCPos =  $145+7;
 CCPos =  $150+7;
 LAPos =  $142+7;
 LAAPos = $145+7;
 RAPos =  $14A+7;
 RAAPos = $14D+7;
 BankSize = $2000; //$DE01 - $C010;
 CopyBlock1 = $DE01;
 CopyBlock2 = $DEDB;
 Block1Size = CopyBlock2 - CopyBlock1;
 Block2Size = $E010 - CopyBlock2;
 Block1Bank = $F090; // Ptr = $FC25;
 Block2Bank = $F091; // Ptr = $FC27;
 Block1NPos = $11E01;
 Block2NPos = $11EDB;
 CBCnt = 2;
 CopyBlocks: array[0..CBCnt-1] of record O, S, B, N: Integer end =
 ((O: CopyBlock1; S: Block1Size; B: Block1Bank; N: Block1NPos),
  (O: CopyBlock2; S: Block2Size; B: Block2Bank; N: Block2NPos));
 ProgramSize = ProgramEnd - ProgramStart;
 ProgSz = SizeOf(OutputMsgCode) + SizeOf(Y_Tbl);
 ProgSzAlt = SizeOf(OutputMsgAltCode) + SizeOf(Y_Tbl);
 TblMax = ProgramSize - ProgSz;
 TblMaxAlt = ProgramSize - ProgSzAlt;

function GetData(TextStrings: PTextStrings): AnsiString; stdcall;
var
 LeftBuffer, LeftAlt, RightBuffer, RightAlt, CodesBuffer: array of Byte;
 Index, NodesCount: Integer;
 Alt: Boolean;

 function FillTree(Node: PHuffTreeNode): Boolean;
 begin
  if Node <> NIL then with Node^ do
  begin
   if (Left <> NIL) and (Right <> NIL) then
   begin
    if Position >= 0 then
    case Direction of
     diLeft:   LeftBuffer[Position] := Left.Position;
     diRight: RightBuffer[Position] := Right.Position;
    end;
    Result := FillTree(Left) and FillTree(Right);
   end else
   begin
    if Position >= 0 then
    begin
     if Alt then
     case Direction of
      diLeft:
      begin
       LeftBuffer[Position] := Value;
       LeftAlt[Position] := 1;
      end;
      diRight:
      begin
       RightBuffer[Position] := Value;
       RightAlt[Position] := 1;
      end;
     end else
     case Direction of
      diLeft:   LeftBuffer[Position] := Index + 128;
      diRight: RightBuffer[Position] := Index + 128;
     end;
    end;
    CodesBuffer[Index] := Value;
    Inc(Index);
    Result := True;
   end;
  end else Result := False;
 end; // FillTree

var
 I, J, K, BitPos: Integer;
 N: PTextString;
 Tree: THuffmanTree;
 Data: PByte;
 DataMask: Byte;

 function WriteBitStream(Value: Byte): Integer;
 var
  Mask: Cardinal;
 begin
  with Tree.BitStreams[Value] do
  begin
   Result := Size;
   Mask := 1 shl (Size - 1);
   while Mask > 0 do
   begin
    if DataMask = 1 then Data^ := 0;
    if Bits and Mask <> 0 then
     Data^ := Data^ or DataMask;
    Mask := Mask shr 1;
    DataMask := DataMask shl 1;
    if DataMask = 0 then
    begin
     Inc(Data);
     DataMask := 1;
    end;
   end;
  end;
 end; // WriteBitStream

type
 PTextStringInf = ^TTextStringInf;
 TTextStringInf = record
  Node: PTextString;
  Copy: PTextStringInf;
  Index: Integer;
  CopyOfs: Integer;
  Offset: Integer;
  CopyBank: Byte;
 end;

var
 NList: array of TTextStringInf;

 procedure NListSort(L, R: Integer);
 var
  I, J: Integer;
  X, Y: TTextStringInf;
 begin
  I := L;
  J := R;
  X := NList[(L + R) shr 1];
  repeat
   while Length(NList[I].Node.Str) > Length(X.Node.Str) do Inc(I);
   while Length(X.Node.Str) > Length(NList[J].Node.Str) do Dec(J);
   if I <= J then
   begin
    if Length(NList[I].Node.Str) <> Length(NList[J].Node.Str) then
    begin
     Y := NList[I];
     NList[I] := NList[J];
     NList[J] := Y;
    end;
    Inc(I);
    Dec(J);
   end;
  until I > J;
  if L < J then NListSort(L, J);
  if I < R then NListSort(I, R);
 end; (* NListSort *)

function GetBitsCount(P: PByte; Len: Integer): Integer;
var
 I: Integer;
begin
 Result := 0;
 with Tree do
  for I := 0 to Len - 1 do
  begin
   Inc(Result, BitStreams[P^].Size);
   Inc(P);
  end;
end;

var
 PtrLst: array of Word;
 TextData: array of record PtrIdx: Integer; S: String end;
 Offset, LPtr, LAPtr, RPtr, RAPtr, CharPtr: Integer;
 FreqList: array[Byte] of Cardinal;
 P: PByte; BanksTbl: PBytes;
 InsBlocks: array of packed
 record
  Bank, Mask: Byte;
  Filled, Max: Integer
 end;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 with TextStrings^ do
 begin
  SetLength(NList, Count);
  N := Root;
  for I := 0 to Count - 1 do
  begin
   with NList[I] do
   begin
    Node := N;
    Index := I;
   end;
   N := N.Next;
  end;
  NListSort(0, Count - 1);
  SetLength(PtrLst, Count);
{  N := Root;
  I := 0;
  while N <> NIL do with N^ do
  begin
   Offset := Pos(Str, TextData) - 1;
   if Offset < 0 then
   begin
    Offset := Length(TextData);
    TextData := TextData + Str;
   end;
   PtrLst[I] := Offset;
   Inc(I);
   N := Next;
  end;         }
  FillChar(FreqList, SizeOf(FreqList), 0);
  for I := 0 to Count - 1 do with NList[I] do
  begin
   K := 0;
   for J := 0 to I - 1 do
   begin
    K := Pos(Node.Str, NList[J].Node.Str);
    if K > 0 then
    begin
     Copy := Addr(NList[J]);
     CopyOfs := K - 1;
     Break;
    end;
   end;
   if K <= 0 then
    with Node^ do
     for K := 1 to Length(Str) do
      Inc(FreqList[Byte(Str[K])]);
  end;
{  for I := 1 to Length(TextData) do
   Inc(FreqList[Byte(TextData[I])]);}
  Tree := THuffmanTree.Create(FreqList);
  with Tree do
  try
   NodesCount := Length(Branches);
   Alt := LeavesCount > 128;
 {  J := TotalBytesCount;
   if (Alt and (J <= BankSize - NodesCount * 4)) or (not Alt and (
   ((LeavesCount <= TblMax) and (J <= BankSize - NodesCount * 2)) or
   ((NodesCount <= TblMax) and (J <= BankSize - (NodesCount + LeavesCount))) or
    (J <= BankSize - (NodesCount * 2 + LeavesCount)))) then}
   begin
    SetLength(LeftBuffer, NodesCount);
    SetLength(RightBuffer, NodesCount);
    SetLength(CodesBuffer, LeavesCount);
    if Alt then
    begin
     SetLength(LeftAlt, NodesCount);
     SetLength(RightAlt, NodesCount);
    end;
    Index := 0;
    if FillTree(Trunk) then
    begin
     for I := 0 to CBCnt - 1 do
      with CopyBlocks[I] do
       if ROM[N] = $FF then
       begin
        Move(ROM[O], ROM[N], S);
        ROM[B] := 8;
       end;
     FillChar(Pointer(Integer(ROM) + $C010)^, BankSize, $FF);
     FillChar(Pointer(Integer(ROM) + ProgramStart)^, ProgramSize, $FF);
     SetLength(InsBlocks, 2);
     if not Alt then
     begin
      BanksTbl := Pointer(Integer(ROM) + ProgramStart +
                         (SizeOf(OutputMsgCode) - Count));
      Word(Pointer(Integer(ROM) + Y_TblPtrPos)^) := (ProgramStart - $6010) +
       SizeOf(OutputMsgCode);
      Move(OutputMsgCode, Pointer(Integer(ROM) + ProgramStart)^,
       SizeOf(OutputMsgCode));
      Move(Y_Tbl, Pointer(Integer(ROM) + ProgramStart +
       SizeOf(OutputMsgCode))^, SizeOf(Y_Tbl));
    {  with InsBlocks[2] do
      begin
       Bank := (ProgramStart - 16) div $2000;
       Mask := 1;
       Filled := ((ProgSz + ProgramStart) - $E010) shl 3;
       Max := Filled + TblMax shl 3;
      end;     }
    {  if LeavesCount <= TblMax then
      begin
       CharPtr := (ProgSz + ProgramStart) - $6010;
       LPtr := $A000;
       RPtr := $A000 + NodesCount;
       Move(Pointer(LeftBuffer)^, ROM[LPtr + $2010], NodesCount);
       Move(Pointer(CodesBuffer)^, ROM[CharPtr + $6010], LeavesCount);
      end else
      if NodesCount <= TblMax then
      begin
       LPtr := (ProgSz + ProgramStart) - $6010;
       CharPtr := $A000;
       RPtr := $A000 + LeavesCount;
       Move(Pointer(LeftBuffer)^, ROM[LPtr + $6010], NodesCount);
       Move(Pointer(CodesBuffer)^, ROM[CharPtr + $2010], LeavesCount);
      end else
      begin   }
       LPtr := $A000;
       CharPtr := $A000 + NodesCount;
       RPtr := CharPtr + LeavesCount;
       Move(Pointer(LeftBuffer)^, ROM[LPtr + $2010], NodesCount);
       Move(Pointer(CodesBuffer)^, ROM[CharPtr + $2010], LeavesCount);
      {end;      }
      Word(Pointer(Integer(ROM) + ProgramStart + LCPos)^) := LPtr;
      Word(Pointer(Integer(ROM) + ProgramStart + RCPos)^) := RPtr;
      Word(Pointer(Integer(ROM) + ProgramStart + CCPos)^) := CharPtr - 128;
      Offset := RPtr + $2010;
      Move(Pointer(RightBuffer)^, ROM[Offset], NodesCount);
      Inc(Offset, NodesCount);
     end else
     begin
      BanksTbl := Pointer(Integer(ROM) + ProgramStart +
                         (SizeOf(OutputMsgAltCode) - Count));
      Word(Pointer(Integer(ROM) + Y_TblPtrPos)^) := (ProgramStart - $6010) +
       SizeOf(OutputMsgAltCode);
      Move(OutputMsgAltCode, Pointer(Integer(ROM) + ProgramStart)^,
       SizeOf(OutputMsgAltCode));
      Move(Y_Tbl, Pointer(Integer(ROM) + ProgramStart +
       SizeOf(OutputMsgAltCode))^, SizeOf(Y_Tbl));
  {    with InsBlocks[2] do
      begin
       Bank := (ProgramStart - 16) div $2000;
       Mask := 1;
       Filled := ((ProgSzAlt + ProgramStart) - $E010) shl 3;
       Max := Filled + TblMaxAlt shl 3;
      end;   }
      LPtr := $A000;
      LAPtr := $A000 + NodesCount;
      RPtr := LAPtr + NodesCount;
      RAPtr := RPtr + NodesCount;
      Word(Pointer(Integer(ROM) + ProgramStart + LAPos)^) := LPtr;
      Word(Pointer(Integer(ROM) + ProgramStart + LAAPos)^) := LAPtr;
      Word(Pointer(Integer(ROM) + ProgramStart + RAPos)^) := RPtr;
      Word(Pointer(Integer(ROM) + ProgramStart + RAAPos)^) := RAPtr;
      Move(Pointer(LeftBuffer)^, ROM[LPtr + $2010], NodesCount);
      Move(Pointer(LeftAlt)^, ROM[LAPtr + $2010], NodesCount);
      Move(Pointer(RightBuffer)^, ROM[RPtr + $2010], NodesCount);
      Offset := RAPtr + $2010;
      Move(Pointer(RightAlt)^, ROM[Offset], NodesCount);
      Inc(Offset, NodesCount);
     end;
     with InsBlocks[0] do
     begin
      Bank := 6;
      Mask := 1;
      Filled := (Offset - $C010) shl 3;
      Max := 65536;
     end;
     with InsBlocks[1] do
     begin
      Bank := 1;
      Mask := 1;
      Filled := ($3C20 - $2010) shl 3;
      Max := ($3FF0 - $2010) shl 3;
     end;
{     Data := Addr(ROM[Offset]);
     DataMask := 1;}
     for I := 0 to Count - 1 do with NList[I] do
     begin
      if Copy = nil then
      begin
       with Node^ do
        K := GetBitsCount(Pointer(Str), Length(Str));
       for J := 0 to Length(InsBlocks) - 1 do
        with InsBlocks[J] do
         if K <= Max - Filled then
         begin
          Offset := Filled;
          PtrLst[Index] := Byte(Filled shr 3) or (Filled and $F800) or
                           ((Filled and 7) shl 8);
          BanksTbl[Index] := Bank;
          CopyBank := Bank;
          Data := Addr(ROM[(Bank * $2000) + 16 + Filled shr 3]);
          Inc(Filled, K);
          DataMask := Mask;
          with Node^ do
          begin
           P := Pointer(Str);
           for K := 0 to Length(Str) - 1 do
           begin
            WriteBitStream(P^);
            Inc(P);
           end;
          end;
          Mask := DataMask;
          Break;
         end;
      end else
      begin
       K := Copy.Offset + GetBitsCount(Pointer(Copy.Node.Str), CopyOfs);
       Offset := K;
       PtrLst[Index] := Byte(K shr 3) or (K and $F800) or
                           ((K and 7) shl 8);
       CopyBank := Copy.CopyBank;
       BanksTbl[Index] := CopyBank;
      end;
     end;
{     P := Pointer(TextData);
     for J := 0 to Length(TextData) - 1 do
     begin
      WriteBitStream(P^);
      Inc(P);
     end;
     for J := 0 to Count - 1 do
     begin
      BitPos := (Offset - $C010) shl 3;
      P := Pointer(TextData);
      for I := 0 to PtrLst[J] - 1 do
      begin
       Inc(BitPos, BitStreams[P^].Size);
       Inc(P);
      end;
      PtrLst[J] := Byte(BitPos shr 3) or (BitPos and $F800) or ((BitPos and 7) shl 8);
     end;                           }
     SetLength(Result, Count * 2);
 {    Offset := PtrLst[0];
     PtrLst[0] := PtrLst[1];
     PtrLst[1] := Offset;}
     Move(Pointer(PtrLst)^, Pointer(Result)^, Count * 2);
     Word(Pointer(Integer(ROM) + CodePtrPos)^) := (ProgramStart - $6010) + 9;
     Move(OMsgInitCode, Pointer(Integer(ROM) + ProgInit)^, SizeOf(OMsgInitCode));
     Move(ClrTbl, Pointer(Integer(ROM) + ClrTblPos)^, SizeOf(ClrTbl));
    end;
   end;
  finally
   Tree.Free;
  end;
 end;
end;


Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var
 B, ROMEnd: PByte; J: Integer; S: PTextString;
 PtrTbl: PWordArray;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 If (EndsRoot = NIL) or (MaxCodes <= 0) then Exit;
 New(Result, Init);
 PtrTbl := Addr(ROM[X]);
 ROMEnd := Addr(ROM[RomSize]);
 With Result^ do
 begin
  for J := 0 to STR_CNT - 1 do
  begin
{   if J = 0 then
    B := Addr(ROM[PtrTbl[1] + $2010]) else
   if J = 1 then
    B := Addr(ROM[PtrTbl[0] + $2010]) else}
    B := Addr(ROM[PtrTbl[J] + $2010]);
   S := Add;
   with S^ do
   begin
    while B^ <> $FF do
    begin
    { if B^ = $F8 then
     begin
      Str := Str + Char($F8) + Char($F8);
      Inc(B);
     end else   }
     If B^ = $F9 then
     begin
      Str := Str + Char($F9);
      Inc(B);
      If B^ = $FF then
      begin
       Str := Str + Char($FF);
       Inc(B);
      end;
     end Else
     begin
      Str := Str + Char(B^);
      Inc(B);
     end;
     If (Integer(B) >= Integer(ROMEnd)) then Exit;
    end;
    Str := Str + #$FF;
   end;
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

