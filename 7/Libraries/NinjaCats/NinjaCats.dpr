library NinjaCats;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Cattou Ninden Teyandee';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Var SL: Integer = 0;

Function NeedEnd: Boolean; stdcall;
Asm
 mov eax,SL
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var B: ^Byte; I: Integer; S: PTextString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
  If (EndsRoot = NIL) or (MaxCodes <= 0) then Exit;
  New(Result, Init);
  B := Addr(ROM^[X]);
  I := 0;
  SL := 0;
  With Result^ do
  begin
   S := Add;
   While B^ <> $FF do With S^ do
   begin
    If B^ = $F9 then
    begin
     Str := Str + Char(B^);
     Inc(Integer(B));
     Inc(I);
     If B^ = $FF then
     begin
      Str := Str + Char($FF);
      Inc(Integer(B));
      Inc(I);
     end;
    end Else
    begin
     Str := Str + Char(B^);
     Inc(Integer(B));
     Inc(I);
    end;
    If (I = Sz) or (X + I > RomSize) then Exit;
   end;
   SL := I + 1;
  end;
end;

exports
 GetMethod, SetVariables, GetData, GetStrings,
 DisposeStrings, NeedEnd, Description;

end.
