library LittleMermaid;

uses
  ShareMem,
  Classes,
  SysUtils,
  Needs in '..\..\Needs.pas';

{$E .kpl}

{$R *.res}

resourcestring
 SKPLDescription = 'GameBoy Advance Little Mermaid Text Library';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Type
 TLine = Array[0..24] of Char;
Type
 THeader = Packed Record
  LinesCount: Byte;
  ParagraphCount: Byte;
  Zero1: Byte;
  Zero2: Byte;
  Zero3: Byte;
 end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; K, J, JJ: Integer; S, Num: String;
 Header: THeader; Lines: Array of TLine; StringList: TStringList;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  If R <> NIL then With R^ do
  begin
   StringList := TStringList.Create;
   If Str = '' then
    StringList.Text := ' ' Else
    StringList.Text := Str;
   Header.LinesCount := 2;
   K := StringList.Count;
   While K mod 2 > 0 do Inc(K);
   Header.ParagraphCount := K div 2;
   Header.Zero1 := 0;
   Header.Zero2 := 0;
   Header.Zero3 := 0;
   SetLength(Result, SizeOf(THeader));
   Move(Header, Result[1], SizeOf(THeader));
   SetLength(Lines, 2);
   For K := 0 to Header.ParagraphCount - 1 do
   begin
    FillChar(Lines[0], 25 * 2, 32);
    J := K shl 1;
    JJ := 0;
    If J < StringList.Count then
    begin
     S := StringList.Strings[J];
     If S[1] = '[' then
     begin
      JJ := 2;
      Num := '';
      While (JJ <= Length(S)) and (S[JJ] in ['0'..'9']) do
      begin
       Num := Num + S[JJ];
       Inc(JJ);
      end;
      If S[JJ] = ']' then
      begin
       JJ := StrToInt(Num);
       Delete(S, 1, Length(Num) + 2);
      end Else JJ := 0;
     end;
     If Length(S) >= 25 then
      Move(S[1], Lines[0], 25) Else
      Move(S[1], Lines[0], Length(S));
    end;
    J := K shl 1 + 1;
    If J < StringList.Count then
    begin
     S := StringList.Strings[J];
     If Length(S) >= 25 then
      Move(S[1], Lines[1], 25) Else
      Move(S[1], Lines[1], Length(S));
    end;
    SetLength(S, 58);
    Move(JJ, S[1], 4);
    J := 50;
    Move(J, S[5], 4);
    Move(Lines[0], S[9], 50);
    Result := Result + S;
   end;
   Result := Result + #0;
   StringList.Free;
  end;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var
 C, I: Integer; TS: PTextString; Header: THeader;
 BB: ^Integer; Lines: Array of TLine;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 Header := THeader(Addr(ROM^[X])^);
 Inc(X, SizeOf(THeader));
 BB := Addr(ROM^[X]);
 TS := Result^.Add;
 TS^.Str := '';
 With Header do For I := 0 to ParagraphCount - 1 do
 begin
  With TS^ do Str := Str + '[' + IntToStr(BB^) + ']';
  Inc(Integer(BB), 4);
  SetLength(Lines, LinesCount);
  FillChar(Lines[0], LinesCount * SizeOf(TLine), 0);
  C := BB^;
  Inc(Integer(BB), 4);
  Move(BB^, Lines[0], C);
  Inc(Integer(BB), C);
  With TS^ do For C := 0 to LinesCount - 1 do
  begin
   If C < LinesCount - 1 then Str := Str + Lines[C] + #$0D + #$0A Else
   begin
    If I = ParagraphCount - 1 then
     Str := Str + Lines[C] Else
     Str := Str + Lines[C] + #$0D + #$0A;
   end;
  end;
 end;
// With Result^ do For I := 0 to C - 1 do Add^.Str := Decompress(X);
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.
