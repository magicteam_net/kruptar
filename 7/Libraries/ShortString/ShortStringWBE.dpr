library ShortStringWBE;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Short string wide';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function SwapWord(Value: Word): Word;
asm
 xchg al,ah
end;


Function GetData(TextStrings: PTextStrings): String; stdcall;
Var
 R: PTextString;
 WW: Word;
 WC: array[0..1] of Char absolute WW;
begin
 Result := Char(0);
 If TextStrings = NIL then Exit;
 Result := '';
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
 If Length(Result) > 255 * 2 then SetLength(Result, 255 * 2);
 WW := SwapWord((Length(Result) + 1) div 2);
 Result := WC[0] + WC[1] + Result;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var I: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  I := SwapWord(Word(Addr(ROM^[X])^)) * 2;
  While (I > 0) and (X + I >= RomSize) do Dec(I);
  If I = 0 then Exit;
  SetLength(Str, I);
  Move(ROM^[X + 2], Str[1], I);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

