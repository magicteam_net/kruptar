library ShortString;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription =
 '���������� ��� ������ �� �������� �������:' + #13#10 +
 '����� ������ $Len - ���� ����;' + #13#10 +
 '������ ������ $Len ����.' + #13#10 +
 '������ ���� $len (����� 1) ����������� � ���� ������� ����� ptStringLength.' + #13#10 +
 'Endianess ����������� � ������� ���� ptStringLength';

Var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0; // Gets string length and flags for GetData
 Align: LongInt = 1;

Function Description: RawByteString; stdcall;
begin
 Result := AnsiString(SKPLDescription);
end;

Function NeedEnd: LongBool; stdcall;
begin
 Result := False;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmL1T;
end;

Procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

function SwapInt(Value: LongInt): LongInt;
asm
 bswap  eax
end;

type
  LongRec = packed record
    case LongInt of
      0: (Lo, Hi: Word);
      1: (Words: array [0..1] of Word);
      2: (Bytes: array [0..3] of Byte);
  end;

function Min(A, B: LongInt): LongInt;
begin
 if A < B then
  Result := A else
  Result := B;
end;


Function GetData(TextStrings: PTextStrings): RawByteString; stdcall;
Var
 R: PTextString;
 Value, I: LongInt;
begin
 Result := '';
 If TextStrings <> NIL then
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
 Value := Length(Result) div Align;
 case MaxCodes and 3 of
  0:   Value := Min(Value, $FF);
  1:   Value := Min(Value, $FFFF);
  else Value := Min(Value, 256 * 1024);
 end;
 SetLength(Result, Value * Align);
 with LongRec(Value) do
 if MaxCodes >= 4 then // if big endian
 begin
  for I := MaxCodes and 3 downto 0 do
   Result := AnsiChar(Bytes[I]) + Result;
 end else
 begin
  for I := 0 to MaxCodes and 3 do
   Result := AnsiChar(Bytes[I]) + Result;
 end;
end;

Function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
var
 I: LongInt;
 Value: Int64;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  Value := 0;
  if Sz >= 4 then // if big endian
  begin
   for I := Sz and 3 downto 0 do
   begin
    Value := Value or (ROM[X] shl (I shl 3));
    Inc(X);
   end;
   Value := SwapInt(Value);
  end else
  for I := 0 to Sz and 3 do
  begin
   Value := Value or (ROM[X] shl (I shl 3));
   Inc(X);
  end;
  Value := Value * Align;
  if Value > 0 then
  begin
   SetLength(Str, Value);
   Move(ROM[X], Pointer(Str)^, Value);
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

