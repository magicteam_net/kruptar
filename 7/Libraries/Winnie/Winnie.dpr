library Winnie;

uses
  ShareMem, Classes,
  Needs in '..\..\Needs.pas',
  gba, MyClasses;

{$E .kpl}

resourcestring
 SKPLDescription = 'Winnie the Pooh GBA';

var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

function Description: String; stdcall;
begin
 Result := SKPLDescription;;
end;

function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

procedure SetVariables(X: PBytes; Sz: Integer;
  ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

const
 PACK_SIGN = $5041434B;
 LZ77_SIGN = $37375A4C;

type
 TWinniePackHeader = packed record
  wpPackSign: Cardinal;
  wpFileSize: Integer;
  wpLz77Sign: Cardinal;
 end;

function GetData(TextStrings: PTextStrings): String; stdcall;
var
 Dest: TVarStringStream;
 Ptrs: array of Integer;
 I: Integer;
 CStream: TLZ77_PackStream;
 Header: TWinniePackHeader;
 N: PTextString;
begin
 Result := '';
 if Assigned(TextStrings) then with TextStrings^ do
 begin
  Dest := TVarStringStream.Create(Addr(Result));
  try
   SetLength(Ptrs, Count);
   with Header do
   begin
    wpPackSign := PACK_SIGN;
    wpFileSize := 4 + Count * 4;
    wpLz77Sign := LZ77_SIGN;
    I := 0;
    N := TextStrings.Root;
    while N <> NIL do with N^ do
    begin
     if Str <> '' then
     begin
      Ptrs[I] := wpFileSize;
      Inc(wpFileSize, Length(Str) + 1);
     end else Ptrs[I] := 0;
     Inc(I);
     N := Next;
    end;
   end;
   Dest.WriteBuffer(Header, SizeOf(Header));
   CStream := TLZ77_PackStream.Create(Dest, lzNormal);
   try
    CStream.WriteBuffer(Count, 4);
    CStream.WriteBuffer(Ptrs[0], Count * 4);
    N := TextStrings.Root;
    while N <> NIL do with N^ do
    begin
     if Str <> '' then CStream.WriteBuffer(Pointer(Str)^, Length(Str) + 1);
     N := Next;
    end;
   finally
    CStream.Free;
   end;
  except
   Result := '';
  end;
  Dest.Free;
 end;
end;

function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 Source: TBufferStream;
 DStream: TLZ77_UnpackStream;
 Header: TWinniePackHeader;
 PtrsCount, I, J, Ofs, Start: Integer;
 Ptrs: array of Integer;
 TextBuf: Pointer;
 P: PChar;
begin
 Result := NIL;
 If (X >= 0) and (X < RomSize) then
 begin
  Source := TBufferStream.Create(Addr(ROM[X]), RomSize - X);
  try
   Source.ReadBuffer(Header, SizeOf(Header));
   with Header do if (wpPackSign = PACK_SIGN) and (wpLz77Sign = LZ77_SIGN) then
   begin
    DStream := TLZ77_UnpackStream.Create(Source);
    try
     if DStream.Size = wpFileSize then
     begin
      DStream.ReadBuffer(PtrsCount, 4);
      SetLength(Ptrs, PtrsCount);
      DStream.ReadBuffer(Ptrs[0], PtrsCount * 4);
      Start := DStream.Seek(0, soFromCurrent);
      SZ := DStream.Size - Start;
      if SZ >= 0 then
      begin
       GetMem(TextBuf, SZ);
       try
        DStream.ReadBuffer(TextBuf^, SZ);
        New(Result, Init);
        with Result^ do for I := 0 to PtrsCount - 1 do
        begin
         Ofs := Ptrs[I] - Start;
         with Add^ do if Ofs < 0 then Str := '' else
         begin
          Str := '';
          P := TextBuf;
          Inc(P, Ofs);
          if P^ = #1 then for J := 1 to 3 do
          begin
           Str := Str + P^;
           Inc(P);
          end;
          Str := Str + P;
         end;
        end;
       finally
        if SZ > 0 then FreeMem(TextBuf);
       end;
      end;
     end;
    finally
     DStream.Free;
    end;
   end;
  except
   if Result <> NIL then
    Dispose(Result, Done);
   Result := NIL;
  end;
  Source.Free;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

