library WayForward;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'WayForward';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Type
 TWayString = Array[0..31] of Char;
 PWayItem = ^TWayItem;
 TWayItem = Record
  Str: TWayString;
  Next: PWayItem;
 end;
 TWayStrings = Class
 private
  FRoot, FCur: PWayItem;
  FCount: Integer;
  Function Get(Index: Integer): PWayItem;
  Function GetString(Index: Integer): String;
 public
  Constructor Create;
  Destructor Destroy; override;
  Procedure Add(Item: TWayString);
  Procedure Clear;
  property Items[Index: Integer]: PWayItem read Get;
  property Strings[Index: Integer]: String read GetString;
  property Count: Integer read FCount;
 end;

Function TWayStrings.Get(Index: Integer): PWayItem;
Var N: PWayItem; I: Integer;
begin
 Result := NIL;
 If (Index < 0) or (Index >= FCount) then Exit;
 N := FRoot; I := 0;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Function TWayStrings.GetString(Index: Integer): String;
Var WS: TWayString;
begin
 Result := '';
 If (Index < 0) or (Index >= FCount) then Exit;
 WS := Get(Index)^.Str;
 SetLength(Result, SizeOf(TWayString));
 Move(WS[0], Result[1], SizeOf(TWayString));
end;

Constructor TWayStrings.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TWayStrings.Destroy;
begin
 Clear;
 Inherited Destroy;
end;

Procedure TWayStrings.Add(Item: TWayString);
Var N: PWayItem;
begin
 New(N);
 If FRoot = NIL then FRoot := N Else FCur^.Next := N;
 FCur := N; Inc(FCount);
 N^.Str := Item;
 N^.Next := NIL;
end;

Procedure TWayStrings.Clear;
Var N: PWayItem;
begin
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var
 R: PTextString; I, J: Integer; WS: TWayString; Strs: TWayStrings;
Label Sett;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  Strs := TWayStrings.Create;
  R := Root;
  While R <> NIL do With R^ do
  begin
   I := 1; J := 0;
   FillChar(WS, SizeOf(TWayString), 0);
   While (I <= Length(Str)) do
   begin
    If Str[I] <> #10 then
    begin
     If J <= 31 then WS[J] := Str[I];
     If I = Length(Str) then Goto Sett;
    end Else
    begin Sett:
     Strs.Add(WS);
     FillChar(WS, SizeOf(TWayString), 0);
     J := -1;
    end;
    Inc(J);
    Inc(I);
   end;
   WS[0] := '#';
   Strs.Add(WS);
   FillChar(WS, SizeOf(TWayString), 0);
   R := Next;
  end;
  WS[0] := '~';
  Strs.Add(WS);
  With Strs do For J := 0 to Count - 1 do Result := Result + Strings[J];
  Strs.Free;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var
 I, J: Integer; S: PTextString;
 WayString: ^TWayString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 If (EndsRoot = NIL) or (MaxCodes <= 0) then Exit;
 New(Result, Init);
 WayString := Addr(ROM^[X]);
 I := 0;
 S := Result^.Add;
 With Result^ do While WayString^[0] <> '~' do
 begin
  If (WayString^[0] = '#') and (WayString^[1] = #0) then
  begin
   With S^ do If Str <> '' then SetLength(Str, Length(Str) - 1);
   S := Add;
  end Else With S^ do
  begin
   J := 0;
   While (J <= 31) and (WayString^[J] <> #0) do
   begin
    Str := Str + WayString^[J];
    Inc(J);
   end;
   Str := Str + #10;
  end;
  Inc(Integer(WayString), SizeOf(TWayString));
  Inc(I, SizeOf(TWayString));
  If X + I > RomSize then Exit;  
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

