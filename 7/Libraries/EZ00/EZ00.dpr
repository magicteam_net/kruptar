library MagicPockets;

uses
  ShareMem, Gba, Classes, MyClasses,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'EZ00 Packed Strings';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Type
 TEZ00 = Packed Record
  ezSignature: Cardinal;
  ezUnpackedSize: Integer;
  ezInfoSize: Integer;
  ezPassCount: Integer;
 end;

Const
 EZ00_SIGN = $30305A45;

function EZ00_Decompress(var Source; var DestBuf: Pointer): Integer;
var SrcInited: Boolean; SRC: PByte;
function SourceRead(var Buffer; Count: Integer): Integer;
begin
 If not SrcInited then
 begin
  SRC := @Source;
  SrcInited := True;
 end;
 Move(SRC^, Buffer, Count);
 Inc(SRC, Count);
 Result := Count;
end;
var
 Header: TEZ00; Info: Pointer; PB, r8: PByte; PW: PWord absolute PB;
 r0, r2, r3, r4, r9, r1, r6, r10: Integer;
 w8: PWord absolute r8; Buf1, Buf2, Buf3: Array[0..63] of Word;
 d8: PInteger absolute r8;
function Get: Integer;
begin
 Result := r10 and not (r6 shl r1);
 r10 := r10 shr r1;
 Dec(r9, r1);
 If r9 >= 0 then Exit;
 Inc(r9, 16);
 Inc(PW);
 r10 := r10 or (PW^ shl r9);
end;
procedure FillBuf;
var
 Buf: Array[0..31] of Byte; P: PByte; r6, r0, X, Z0, Z1, Z2: Integer;
 W: PWord; BB: Boolean;
begin
 r1 := 5;
 r2 := Get;
 If r2 <> 0 then
 begin
  P := Addr(Buf[0]);
  X := r2;
  Repeat
   r1 := 4;
   P^ := Get;
   Inc(P);
   Dec(r2);
  Until r2 = 0;
  r1 := 0; r3 := $8000;
  For r0 := 1 to 16 do
  begin
   r2 := X;
   P := Addr(Buf[0]);
   Repeat
    If P^ = r0 then
    begin
     z2 := -1;
     z2 := z2 and not (z2 shl r0);
     w8^ := z2; Inc(w8);
     r6 := 16 - P^;
     z1 := r1 shr r6;
     z2 := r0;
     z0 := 0;
     Repeat
      BB := z1 and 1 = 1;
      z1 := z1 shr 1;
      z0 := z0 shl 1;
      If BB then z0 := z0 or 1;
      Dec(z2);
     Until z2 = 0;
     w8^ := z0; Inc(w8);
     z0 := Integer(P) - Integer(Addr(Buf[0]));
     z0 := z0 or (P^ shl 8);
     W := Pointer(r8);
     Inc(W, $1E);
     W^ := z0;
     Inc(r1, r3);
    end;
    Inc(P);
    Dec(r2);
   Until r2 = 0;
   r3 := r3 shr 1;
  end;
 end;
end;
function Depack: Integer;
var P: PByte;
begin
 r2 := r10 and (r6 shr 16);
 Repeat
  Result := d8^; Inc(d8);
  r1 := Result and r2;
 Until r1 = Result shr 16;
 P := r8; Inc(P, $3D);
 r1 := P^;
 r10 := r10 shr r1;
 Dec(r9, r1);
 If r9 < 0 then
 begin
  Inc(PW); r1 := PW^;
  Inc(r9, 16);
  r10 := r10 or (r1 shl r9);
 end;
 P := r8; Inc(P, $3C);
 r1 := P^;
 If r1 >= 2 then
 begin
  r2 := r1 - 1;
  Result := r10 and not (r6 shl r2);
  r10 := r10 shr r2;
  Dec(r9, r2);
  If r9 < 0 then
  begin
   Inc(PW); r1 := PW^;
   Inc(r9, 16);
   r10 := r10 or (r1 shl r9);
  end;
  r1 := 1;
  Result := Result or (r1 shl r2);
 end Else Result := r1;
end;
var DB, P: PByte; J: Integer;
label __Loc1, __Loc2;
begin
 Result := 0;
 SrcInited := False;
 DestBuf := NIL;
 With Header do
 If (SourceRead(Header, SizeOf(TEZ00)) = SizeOf(TEZ00)) and
    (ezSignature = EZ00_SIGN) and (ezInfoSize > 0) and (ezUnpackedSize > 0) then
 begin
  GetMem(Info, ezInfoSize);
  If SourceRead(Info^, ezInfoSize) = ezInfoSize then
  begin
   GetMem(DestBuf, ezUnpackedSize);
   DB := DestBuf;
   PB := Info;
   r10 := PW^;
   r9 := 0;
   r1 := 2;
   r6 := -1;
   Get;
   For J := 1 to ezPassCount do
   begin
    r8 := Addr(Buf1[0]);
    FillBuf;
    r8 := Addr(Buf2[0]);
    FillBuf;
    r8 := Addr(Buf3[0]);
    FillBuf;
    r1 := 16;
    r4 := Get;
    Goto __Loc2;
  __Loc1:
    r8 := Addr(Buf2[0]);
    r3 := Depack;
    r8 := Addr(Buf3[0]);
    r0 := Depack + 2;
    Inc(r3);
    Repeat
     P := DB; Dec(P, r3);
     DB^ := P^; Inc(DB);
     Dec(r0);
    Until r0 = 0;
  __Loc2:
    r8 := Addr(Buf1[0]);
    r0 := Depack;
    if r0 > 0 then
    begin
     SourceRead(DB^, r0);
     Inc(DB, r0);
     r1 := PW^;
     r10 := r10 or (r1 shl r9);
    end;
    Dec(r4);
    If r4 > 0 then Goto __Loc1;
   end;
   Result := ezUnpackedSize;
  end;
  FreeMem(Info);
 end;
end;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
var
 R: PTextString;
 B, Z: Integer;
 Buf: TMemoryStream;
 W: PSmallInt;
 ResultStream: TVarStringStream;
 Huffman: THuffmanPackStream;
begin
 Result := '';
 if TextStrings = NIL then Exit;
 with TextStrings^ do
 begin
  ResultStream := TVarStringStream.Create(Addr(Result));
  try
   Huffman := THuffmanPackStream.Create(ResultStream);
   try
    Buf := TMemoryStream.Create;
    try
     Buf.Size := Count * 2;
     Buf.Position := Buf.Size;
     W := Buf.Memory;
     R := Root; B := 4; Z := -1;
     while R <> NIL do With R^ do
     begin
      if (Z < 0) or (Str <> '') then
      begin
       If Z < 0 then Z := 0;
       W^ := Buf.Position + 1;
       Buf.Write(B, 1);
       Buf.Write(Pointer(Str)^, Length(Str));
       Buf.Write(Z, 1);
      end Else W^ := -1;
      Inc(W);
      R := Next;
     end;
     Huffman.Write(Buf.Memory^, Buf.Size);
    finally
     Buf.Free;
    end;
   finally
    Huffman.Free;
   end;
  except
   Result := '';
  end;
  ResultStream.Free;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
var
 P: PChar; Buf: Pointer;
 W: PSmallInt; Cnt, I: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 SZ := EZ00_Decompress(ROM^[X], Buf);
 if Buf <> NIL then
 begin
  W := Buf;
  New(Result, Init);
  with Result^ do
  begin
   I := 0;
   while W^ < 0 do
   begin
    Add.Str := '';
    Inc(W);
    Inc(I);
   end;
   Cnt := ((W^ - 1) shr 1) - I;
   for I := 1 to Cnt do with Add^ do
   begin
    if W^ >= SZ then Break;
    if W^ < 0 then Str := '' else
    begin
     P := Buf;
     Inc(P, W^);
     Str := P;
    end;
    Inc(W);
   end;
  end;
  FreeMem(Buf);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

