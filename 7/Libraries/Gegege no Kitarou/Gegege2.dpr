library Gegege2;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Gegege no Kitarou 2 (NES)';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str + #255;
   R := R^.Next;
  end;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var //B: PByte;
    P, CP: PChar; N,{ Cnt, I,} J: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  If (X = $2AE73) or
     (X = $2AEB3) or
     (X = $2AEF3) or
     (X = $2AF33) then
  begin
   CP := Addr(ROM^[X]);
   P := CP; Dec(P, 32);
   For J := 1 to 5 do
   begin
    If P^ <> #0 then
     Str := Str + P^ + CP^ Else
     Str := Str + CP^;
    Inc(P); Inc(CP);
   end;
  end Else If X = $822C then Str := '' Else
  If ((X >= $3D978) and (X <= $3DA63)) then
  begin
   P := Addr(ROM^[X]);
   CP := P; Inc(CP, 7);
   For N := 0 to 2 do
   begin
    For J := 1 to 7 do
    begin
     If P^ <> #0 then
      Str := Str + P^ + CP^ Else
      Str := Str + CP^;
     Inc(P); Inc(CP);
    end;
    Inc(P, 7);
    Inc(CP, 7);
    If N = 2 then Break;
    Str := Str + #10;
   end;
  end Else
  If ((X >= $187F3) and (X <= $188C0)) or
     ((X >= $397D0) and (X <= $39F2F)) then
  begin
   P := Addr(ROM^[X]);
   CP := P; Inc(CP, 7);
   For J := 1 to 7 do
   begin
    If P^ <> #0 then
     Str := Str + P^ + CP^ Else
     Str := Str + CP^;
    Inc(P); Inc(CP);
   end;
  end Else
  begin
   P := Addr(ROM^[X]);
   CP := P; Inc(CP, 22);
   For N := 0 to 1 do
   begin
    For J := 1 to 22 do
    begin
     If P^ <> #0 then
      Str := Str + P^ + CP^ Else
      Str := Str + CP^;
     Inc(P); Inc(CP);
    end;
    Inc(P, 22);
    Inc(CP, 22);
    If N = 1 then Break;
    Str := Str + #10;
   end;
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

