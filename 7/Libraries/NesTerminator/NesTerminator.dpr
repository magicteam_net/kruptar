library NesTerminator;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = '���������� ��� ������ �� �������� �������:' + #13#10 +
           '����� ������ $Len - ���� ����;' + #13#10 +
           '������ ������ $Len ����;' + #13#10 +
           '����� ������ $Len - ���� ����;' + #13#10 +
           '������ ������ $Len ����.';

Var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0;
 Align: LongInt = 1;

Function Description: RawByteString; stdcall;
begin
 Result := AnsiString(SKPLDescription);
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmL1TL1T;
end;

Procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): RawByteString; stdcall;
Var R: PTextString; I: LongInt; S: RawByteString;
begin
 Result := #0#0;
 If TextStrings = NIL then Exit;
 Result := '';
 With TextStrings^ do
 begin
  R := Root; I := 0;
  While R <> NIL do With R^ do
  begin
   S := Str;
   If Length(S) > 255 then SetLength(S, 255);
   Result := Result + AnsiChar(Length(S)) + S;
   R := Next;
   If (I = 0) and (R = NIL) then
   begin
    Result := Result + #0;
    Exit;
   end;
   Inc(I);
   If I >= 2 then Exit;
  end;
 end;
end;

Function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
Var I, XX: LongInt;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  I := ROM^[X];
  XX := X + I + 1;
  While (I > 0) and (X + I >= RomSize) do Dec(I);
  If I = 0 then Exit;
  SetLength(Str, I);
  Move(ROM^[X + 1], Str[1], I);
 end;
 With Result^.Add^ do
 begin
  I := ROM^[XX];
  While (I > 0) and (XX + I >= RomSize) do Dec(I);
  If I = 0 then Exit;
  SetLength(Str, I);
  Move(ROM^[XX + 1], Str[1], I);
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

