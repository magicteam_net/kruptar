library ActionMan;

uses
 ShareMem,
 Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Action Man - Robot Atak';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNormal;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function Decompress(Var Src): String;
Var S, D: ^Integer; Len, Pos, r0, r10, r9, r5, r1, r6: Integer;
Procedure h30031E8;
begin
 r0 := r10 and not (r6 shl r1);
 r10 := r10 shr r1;
 Dec(r9, r1);
 If r9 >= 0 then Exit;
 Inc(Integer(S), 2);
 r1 := Word(S^);
 Inc(r9, 16);
 r10 := r10 or (r1 shl r9);
end;
begin
 S := Addr(Src);
 Result := '';
 If S^ = $30305A45 then
 begin
  Inc(Integer(S), 4);
  Len := S^;
  Inc(Integer(S), 4);
  Pos := S^;
  Inc(Integer(S), 4);
  r0 := S^;
  r5 := r0;
  Inc(Integer(S), 4);
  r10 := Word(S^);
  Integer(D) := Integer(S) + Pos;
  r9 := 0;
  r1 := 2;
  r6 := -1;
  h30031E8;
 end;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
begin
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
begin
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;
 
end.
 