library AmazeHuffman;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = '���������� ��� ������ � �������, ������ ������� ��������, ' +
           '�������������� � ����� �� Amaze Entertainment.';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Type
 PFreq = ^TFreq;
 TFreq = Record
  Count: Integer;
  Code: Integer;
  HCode: DWord;
  LRbit: DWord;
  Pos: Integer;
  Left: PFreq;
  Right: PFreq;
  Next: PFreq;
 end;
 PFreqs = ^TFreqs;
 TFreqs = Object
  Root: PFreq;
  Count: Integer;
  Constructor Init;
  Function Add: PFreq;
  Function MakeParent: Boolean;
  Destructor Done;
 end;

Constructor TFreqs.Init;
begin
 Root := NIL;
 Count := 0;
end;

Function TFreqs.Add: PFreq;
begin
 New(Result);
 FillChar(Result^, SizeOf(TFreq), 0);
 Result^.Code := -1;
 Result^.Next := Root;
 Root := Result;
 Inc(Count);
end;

Destructor TFreqs.Done;
Procedure FreeNodes(Node: PFreq);
begin
 If Node <> NIL then
 begin
  FreeNodes(Node^.Left);
  FreeNodes(Node^.Right);
  Dispose(Node^.Left);
  Dispose(Node^.Right);
 end;
end;
Var N: PFreq;
begin
 While Root <> NIL do
 begin
  With Root^ do
  begin
   N := Next;
   FreeNodes(Left);
   Dispose(Left);
   FreeNodes(Right);
   Dispose(Right);
  end;
  Dispose(Root);
  Root := N;
 end;
 Count := 0;
end;

Function TFreqs.MakeParent: Boolean;
Function FindMin: PFreq;
Var I: Integer; P, N: PFreq;
begin
 I := $7FFFFFFF;
 N := Root;
 P := NIL;
 While N <> NIL do With N^ do
 begin
  If I > Count then
  begin
   I := Count;
   P := N;
  end;
  N := Next;
 end;
 N := Root;
 If N = P then
 begin
  Root := N^.Next;
  P^.Next := NIL;
  Dec(Count);
  Result := P;
  Exit;
 end;
 While N <> NIL do
 begin
  If P = N^.Next then
  begin
   N^.Next := P^.Next;
   P^.Next := NIL;
   Dec(Count);
   Result := P;
   Exit;
  end;
  N := N^.Next;
 end;
 Result := NIL;
end;
Var A, B, N: PFreq;
begin
 Result := False;
 A := FindMin;
 B := FindMin;
 If B = NIL then
 begin
  Root := A;
  Exit;
 end;
 If Root <> NIL then With Add^ do
 begin
  Left := B;
  Right := A;
  Code := -1;
  Count := A^.Count + B^.Count;
  Result := True;
 end Else
 begin
  New(N);
  FillChar(N^, SizeOf(TFreq), 0);
  With N^ do
  begin
   Left := B;
   Right := A;
   Code := -1;
   Count := A^.Count + B^.Count;
  end;
  Root := N;
  Inc(Count);
  Result := False;
 end;
end;  

Function CreateTree(Var Dest; Root: PFreq): DWord;
Function NodePos(L, R: PFreq; Pos: DWord): DWord;
begin
 If L <> NIL then
 begin
  L^.Pos := Pos;
  R^.Pos := Pos + 1;
  Result := NodePos(R^.Left, R^.Right, NodePos(L^.Left, L^.Right, Pos + 2));
 end Else Result := Pos;
end;
Var B: PWords;
Procedure PutNode(N: PFreq);
begin
 If N <> NIL then With N^ do
 begin
  If Pos >= 0 then
  begin
   If Code = -1 then
    B^[Pos] := (Left^.Pos shl 1 + 1024) shr 2 Else
    B^[Pos] := Code;
  end;
  PutNode(Left);
  PutNode(Right);
 end;
end;
begin
 Result := 0;
 If Root = NIL then Exit;
 With Root^ do
 begin
  Pos := -1;
  Result := NodePos(Left, Right, 0);
 end;
 B := Addr(Dest);
 PutNode(Root);
end;

Function Compress(Strings: PTextStrings; Var Dest): Integer;
Var Codes: Array[Byte] of Packed Record Code: DWord; Size: DWord end;
Procedure AnalizeCodes(Node: PFreq; A, B: Integer);
Var I: Integer;
begin
 I := Node^.Code;
 If I <> -1 then
 begin
  Codes[I].Size := A;
  Codes[I].Code := B;
 end;
 With Node^ do
 begin
  HCode := B;
  If Left = NIL then Exit;
  Left^.LRbit := 1;
  Right^.LRbit := 0;
  AnalizeCodes(Left, A + 1, B + B);
  AnalizeCodes(Right, A + 1, (B + B) or 1);
 end;
end;
Var MasX, Dst, DataSize: DWord; Bit: DWord;
Procedure AddCode(Code: Byte);
Var Mask: DWord;
begin
 With Codes[Code] do
 begin
  Mask := 1 shl (Size - 1);
  While Mask > 0 do
  begin
   If Code and Mask = 0 then
    PByte(DST)^ := PByte(DST)^ and not MasX Else
    PByte(DST)^ := PByte(DST)^ or MasX;
   Mask := Mask shr 1;
   MasX := MasX shl 1;
   If MasX = $100 then
   begin
    Inc(Dst);
    Inc(Result);
    Inc(DataSize);
    MasX := 1;
    PByte(Dst)^ := 0;
   end;
  end;
  Inc(Bit, Size);
 end;
end;
Var
 Frs: PFreqs; Freqs: Array[Byte] of DWord; B: DWord;
 N: PTextString; I, J, NCount: Integer; Tsz: PDWord; Ptrs: PDWords;
begin
 FillChar(Freqs, SizeOf(Freqs), 0);
 Dst := DWord(Addr(Dest));
 Result := 0;
 New(Frs, Init);
 N := Strings^.Root;
 While N <> NIL do With N^ do
 begin
  I := 1;
  If Length(Str) > 0 then
  Repeat
   If Str[I] <> #13 then Inc(Freqs[Byte(Str[I])]);
   Inc(I);
  Until I > Length(Str);
  Inc(Freqs[0]);
  N := Next;
 end;
 With Frs^ do
 begin
  For I := 0 to 255 do If Freqs[I] <> 0 then With Add^ do
  begin
   Code := I;
   Count := Freqs[I];
  end;
  If Count > 0 then
  begin
   While MakeParent do;
   FillChar(Codes, SizeOf(Codes), 0);
   AnalizeCodes(Root, 0, 0);
   Tsz := PDWord(Dst);
   NCount := CreateTree(Pointer(Dst + 4)^, Root) shl 1 + 4;
   While NCount mod 4 > 0 do Inc(NCount);
   Inc(Dst, NCount);
   Inc(Result, NCount);
   Tsz^ := NCount;
   Ptrs := Pointer(Dst);
   Inc(Dst, Strings^.Count shl 2);
   Inc(Result, Strings^.Count shl 2);
   B := NCount + Strings^.Count shl 2;
   N := Strings^.Root; I := 0;
   While N <> NIL do With N^ do
   begin
    Ptrs^[I] := B;
    DataSize := 1;
    Bit := 0;
    MasX := 1;
    J := 1;
    PByte(Dst)^ := 0;
    If Length(Str) > 0 then
    Repeat
     If Str[J] <> #13 then AddCode(Byte(Str[J]));
     Inc(J);
    Until J > Length(Str);
    AddCode(0);
    Inc(Dst);
    Inc(Result);
    Inc(I);
    Inc(B, DataSize);
    N := Next;
   end;
  end;
 end;
 Dispose(Frs, Done);
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; Sz: Integer; Dest: Pointer;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  Sz := 0;
  R := Root;
  While R <> NIL do
  begin
   Inc(Sz, Length(R^.Str));
   R := R^.Next;
  end;
  If Sz > 0 then
  begin
   Sz := Sz * 2;
   GetMem(Dest, Sz);
   Sz := Compress(TextStrings, Dest^);
   SetLength(Result, Sz);
   Move(Dest^, Result[1], Sz);
   FreeMem(Dest);
  end;
 end;
end;

Function Decompress(r0: DWord; X: Integer): String;
Var
 r2, r1, r12, r8, r7, r6, r5, r4, r3: DWord;
 h30039AC, h30039B0, h30039B4: DWord;
Label XuX;
begin
 Result := '';
 If (X < 0) or (X >= RomSize) then Exit;
 h30039AC := DWord(Addr(ROM^[X]));
 h30039B0 := h30039AC + PDWord(h30039AC)^;
 h30039B4 := h30039AC + 4;
 r4 := r0;
 r12 := 0;
 r8 := 0;
 r3 := h30039AC;
 If r3 = 0 then Exit;
 r0 := PDWord((r4 shl 2) + h30039B0)^;
 r2 := r3 + r0;
 r6 := 0;
 r4 := 0;
 r7 := PByte(r2)^;
 Inc(r2);
 Repeat
  r3 := $100;
  r5 := r6 + 1;
  Repeat
   r0 := ((r3 shl 2) + h30039B4) - $400;
   If (r7 shr r4) and 1 <> 0 then
   begin
    r3 := PWord(r0 + 2)^
   end Else
   begin
    r3 := PWord(r0)^;
   end;
   Inc(r4);
   If r4 = 8 then
   begin
    r4 := 0;
    r7 := PByte(r2)^;
    Inc(r2);
   end;
  Until r3 <= $FF;
  If r3 > 0 then Result := Result + Char(r3) Else Exit;
  r6 := r5;
  r0 := r3 shl $18;
  r1 := r0 shr $18;
  If r1 > $EF then
  begin
   r3 := r8;
   If r3 = 0 then
   begin
    r1 := r1 shl 8;
    r12 := r1;
    r0 := 1;
    r8 := r0;
   end Else Goto XuX;
  end Else
  begin
   r3 := r8;
   If r3 > 0 then
   begin
   XuX:
    r0 := r0 shr $18;
    r1 := r12;
    r1 := r1 or r0;
    r12 := r1;
    r3 := 0;
    r8 := r3;
   end Else r12 := r1;
  end;
  r0 := r12;
 Until r0 = 0;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var B, C, I: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 DWord(B) := PDWord(Addr(ROM^[X]))^;
 C := (Integer(PDWord(Addr(ROM^[X + B]))^) - B) shr 2;
 With Result^ do For I := 0 to C - 1 do Add^.Str := Decompress(I, X);
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

