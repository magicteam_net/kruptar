library Musashi;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Musashi no Bouken';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Var SL: Integer = 0;

Function NeedEnd: Boolean; stdcall;
Asm
 mov eax,SL
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; C: ^Char; I, J: Integer;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do With R^ do
  begin
   If Str <> '' then
   begin
    C := Pointer(Str);
    J := 0;
    For I := 1 to Length(Str) do
    begin
     If (C^ = #$0D) and (J >= 16) then J := 0 Else
     begin
      Result := Result + C^;
      If C^ <> #$EF then Inc(J);
     end;
     If C^ in [#$0A, #$0B, #$0D] then J := 0;
     Inc(Integer(C));
    end;
   end;
   Result := Result + Char(0);
   Exit;
   R := Next;
  end;
 end;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var  B: ^Byte; I, L: Integer; S: PTextString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
  If (EndsRoot = NIL) or (MaxCodes <= 0) then Exit;
  New(Result, Init);
  B := Addr(ROM^[X]);
  I := 0;
  SL := 0;
  With Result^ do
  begin
   L := 0;
   S := Add;
   While B^ <> 0 do With S^ do
   begin
    Str := Str + Char(B^);
    If B^ in [$A, $B, $D] then L := 0 Else Inc(L);
    Inc(Integer(B));
    If L = 16 then
    begin
     Str := Str + Char($D);
     L := 0;
    end;
    Inc(I);
    If (I = Sz) or (X + I > RomSize) then Exit;
   end;
   SL := I + 1;
  end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

