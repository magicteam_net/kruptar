library Taxi3;

uses
  ShareMem, SysUtils,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Taxi3';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; S: String; Error: Boolean; B, W, L: Integer; P: PChar;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 S := '';
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   S := S + R^.Str + #0;
   R := R^.Next;
  end;
 end;
 B := 0;
 W := 0;
 If (Length(S) >= 6) and (S[1] = '<') and (S[2] = 'p') then
 begin
  L := 2;
  P := Addr(S[3]);
  Error := False;
  While P^ <> '=' do
  begin
   If P^ in ['0'..'9'] then
   begin
    B := B * 10 + (Byte(P^) - $30);
    Inc(L);
   end Else
   begin
    Error := True;
    Break;
   end;
   Inc(P);
  end;
  If not Error then
  begin
   Inc(P);
   Inc(L);
   If (B > 255) or (B < 0)  then B := 0;
   Error := False;
   While P^ <> '>' do
   begin
    If P^ in ['0'..'9'] then
    begin
     W := W * 10 + (Byte(P^) - $30);
     Inc(L);
    end Else
    begin
     Error := True;
     Break;
    end;
    Inc(P);
   end;
   If not Error then
   begin
    Inc(L);
    If (W > 65535) or (W < 0) then W := 0;
    Delete(S, 1, L);
   end;
  end;
 end;
 Result := Char(B) + Char(W) + Char(W shr 8) + S;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var P: PChar; W: ^Word absolute P; B: Byte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  P := Addr(ROM^[X]);
  B := Byte(P^);
  Inc(P);
  Str := '<p' + IntToStr(B) + '=' + IntToStr(W^) + '>';
  Inc(W);
  While P^ <> #0 do
  begin
   Str := Str + P^;
   Inc(P);
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

