library Barnyard;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

{$R *.res}

resourcestring
 SKPLDescription = 'GameBoy Advance Barnyard Text Library';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Type
 TInteger = Packed Record
  a: Char;
  b: Char;
  c: Char;
  d: Char;
 end;
Var R: PTextString; Sz, K: Integer; Dest: Pointer; S: String;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  With TInteger(Count) do
   Result := Result + a + b + c + d;
  Sz := 0; 
  With TInteger(Sz) do
   Result := Result + a + b + c + d;
  R := Root;
  While R <> NIL do With R^ do
  begin
   S := '';
   K := Length(Str) + 1;
   Sz := K + $8000;
   K := (K and not 3) + 8;
   With TInteger(Sz) do
    S := S + a + b + c + d;
   S := S + Str + #0;
   While Length(S) <> K do S := S + #0; 
   Result := Result + S;
   R := Next;
  end;
 end;
end; 

Function Decompress(Var Pos: Integer): String;
Var B: ^Word; CH: ^Char; C, Z: Integer;
Var r0, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12: Integer;
Label h808A81C, h808A960, h808A968, h808A96C;
begin
 Result := '';
 B := Addr(ROM^[Pos]);
 Integer(CH) := Integer(B) + 4;
 C := B^;
 If C >= $8000 then
 begin
  C := C and $7FFF;
  While CH^ <> #0 do
  begin
   Result := Result + CH^;
   Inc(CH);
  end;
 end Else
 begin
  r8 := Integer(CH);
  r5 := 0;
  r10 := 7;
  r9 := 8;
  Goto h808A96C;
 h808A81C:
  Case r4 of
   $3F:
   begin
    r0 := r5 shr 3;
    r1 := r5;
    r4 := r10;
    r1 := r1 and r4;
    r4 := r8;
    r2 := r4 + r0;
    r4 := Byte(Pointer(r2)^);
    r4 := r4 shr r1;
    r0 := 1;
    r4 := r4 and r0;
    If r1 > 7 then
    begin
     r0 := Byte(Pointer(r2 + 1)^);
     r2 := r9;
     r1 := r2 - r1;
     r0 := r0 shl r1;
     r4 := r4 or r0;
     r0 := r4 shl $18;
     r4 := r0 shr $18;
    end;
    r0 := 1;
    r4 := r4 and r0;
    Inc(r5);
    Case r4 of
     0:
     begin
      r0 := r5 shr 3;
      r1 := r5;
      r4 := r10;
      r1 := r1 and r4;
      Inc(r0, r8);
      r4 := Byte(Pointer(r0)^);
      r4 := r4 shr r1;
      r2 := $3F;
      r4 := r4 and r2;
      If r1 > 2 then
      begin
       r0 := Byte(Pointer(r0 + 1)^);
       r2 := r9;
       r1 := r2 - r1;
       r0 := r0 shl r1;
       r4 := r4 or r0;
       r0 := r4 shl $18;
       r4 := r0 shr $18;
      end;
      r4 := r4 and r3;
      Inc(r5, 6);
      Z := $CAFD8;
      r1 := Integer(Addr(ROM^[Z]));
      r0 := r4 - 1;
      Inc(r0, r1);
      r1 := Byte(Pointer(r0)^);
      r0 := $DF;
      r0 := r0 and r1;
      Goto h808A968;
     end;
     1:
     begin
      r0 := r5 shr 3;
      r1 := r5;
      r3 := r10;
      r1 := r1 and r3;
      r4 := r8;
      r2 := r4 + r0;
      r0 := Byte(Pointer(r2)^);
      r0 := r0 shr r1;
      r0 := r0 shl $18;
      r4 := r0 shr $18;
      If r1 > 0 then
      begin
       r0 := Byte(Pointer(r2 + 1)^);
       r2 := r9;
       r1 := r2 - r1;
       r0 := r0 shl r1;
       r4 := r4 or r0;
       r0 := r4 shl $18;
       r4 := r0 shr $18;
      end;
      Inc(r5, 8);
      Result := Result + Char(r4);
      Goto h808A96C;
     end;     
    end;
   end;{
   $3E:
   begin
    r0 := r5 shr 3;
    r1 := r5;
    r3 := r10;
    r1 := r1 and r3;
    Inc(r0, r8);
    r2 := Byte(Pointer(r0)^);
    r2 := r2 shr 1;
    r4 := 255;
    r2 := r2 and r4;
    If r1 > 0 then
    begin
     r0 := Byte(Pointer(r0 + 1)^);
     r3 := r9;
     r1 := r3 - r1;
     r0 := r0 shl r1;
     r2 := r2 or r0;
     r0 := r2 shl $10;
     r2 := r0 shr $10;
    end;
    r7 := 255;
    r3 := r7;
    r3 := r3 and r2;
    Inc(r5, 8);
    r0 := r5 shr 3;
    r1 := r5;
    r2 := r10;
    r1 := r1 and r2;
    Inc(r0, r8);
    r2 := Byte(Pointer(r0)^);
    r2 := r2 shr r1;
    r2 := r2 and r4;
    If r1 > 0 then
    begin
     r0 := Byte(Pointer(r0 + 1)^);
     r4 := r9;
     r1 := r4 - r1;
     r0 := r0 shl r1;
     r2 := r2 or r0;
     r0 := r2 shl $10;
     r2 := r0 shr $10;
    end;
    r2 := r2 and r7;
    Inc(r5, 8);
    r0 := r2 shl 8;
    r3 := r3 or r0;
    r0 := r3 shl $10;
    r4 := r0 shr $10; 
   end;   }
   Else Goto h808A960;
  end;
 h808A960:
  Z := $CAFD8;
  r0 := Integer(Addr(ROM^[Z]));
  r1 := r4 - 1;
  Inc(r1, r0);
  r0 := Byte(Pointer(r1)^);
 h808A968:
  Result := Result + Char(r0);
 h808A96C:
  r0 := r5 shr 3;
  r1 := r5;
  r2 := r10;
  r1 := r1 and r2;
  Inc(r0, r8);
  r4 := Byte(Pointer(r0)^);
  r4 := r4 shr r1;
  r3 := $3F;
  r4 := r4 and r3;
  If r1 > 2 then
  begin
   r0 := Byte(Pointer(r0 + 1)^);
   r2 := r9;
   r1 := r2 - r1;
   r0 := r0 shl r1;
   r4 := r4 or r0;
   r0 := r4 shl $18;
   r4 := r0 shr $18;
  end;
  r0 := $3F;
  r4 := r4 and r0;
  r3 := r4;
  Inc(r5, 6);
  If r4 <> 0 then Goto h808A81C;
 end;
 Z := (C and not 3) + 8;
 Inc(Pos, Z);
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var B, C, I: Integer;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 C := Integer(Addr(ROM^[X])^);
 Inc(X, 8);
 With Result^ do For I := 0 to C - 1 do Add^.Str := Decompress(X);
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.
