library Kyo;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Samurai Deeper Kyo';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := False;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function HexCharToByte(C: Char; var Error: Boolean): Byte;
begin
 Error := False;
 Result := 0;
 If C in ['0'..'9'] then
  Result := Byte(C) - $30 Else
 If C in ['A'..'F'] then
  Result := Byte(C) - $37 Else
 If C in ['a'..'f'] then
  Result := Byte(C) - $57 Else
  Error := True;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString; P: PChar; B1, B2: Byte; HexError, NoNewLine, First: Boolean;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  NoNewLine := False;
  R := Root;
  While R <> NIL do With R^ do
  begin
   P := Pointer(Str);
   While P^ <> #0 do
   begin
    If P^ = '[' then
    begin
     Inc(P);
     First := True;
     Repeat
      B1 := HexCharToByte(P^, HexError);
      If HexError then Break;
      Inc(P);
      B2 := HexCharToByte(P^, HexError);
      If HexError then Break;
      B1 := B2 or (B1 shl 4);
      Result := Result + Char(B1);
      If First and (B1 = $FD) then NoNewLine := True;
      Inc(P);
      First := False;
     Until (P^ = ']') or (P^ = #0);
     If (P^ = #0) or HexError then Break;
    end Else
    Case P^ of
     '''':     Result := Result + #0;
     '-':      Result := Result + #1;
     '�':      Result := Result + #2;
     '"':      Result := Result + #3;
     '�':      Result := Result + #4;
     '�':      Result := Result + #5;
     '�'..'�': Result := Result + Char(Byte(P^) - $60); //�-�, �-�
     'A'..'Z': Result := Result + Char(Byte(P^) + $5F); //A-Z
     'a'..'z': Result := Result + Char(Byte(P^) + $59); //a-z
     '.':      Result := Result + #$D4;
     ',':      Result := Result + #$D5;
     ';':      Result := Result + #$D6;
     ':':      Result := Result + #$D7;
     '!':      Result := Result + #$D8;
     '?':      Result := Result + #$D9;
     '0'..'9': Result := Result + Char(Byte(P^) + $AA); //0-9
     ' ':      Result := Result + #$FE;
     #10:      If not NoNewLine then
                Result := Result + #$F1 Else
                NoNewLine := False;
     #13:      {** Do nothing **};
     Else      Result := Result + #$D9;
    end;
    Inc(P);
   end;
   R := Next;
  end;
 end;
end;

Function ByteToHex(Value: Byte): String;
Var A: Byte;
begin
 A := Value and 15;
 If A <= 9 then
  Result := Char(A + $30) Else
  Result := Char(A + $37);
 A := Value shr 4;
 If A <= 9 then
  Result := Char(A + $30) + Result Else
  Result := Char(A + $37) + Result;
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var P: PByte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
 begin
  P := Addr(ROM^[X]);
  Str := '';
  While True do
  begin
   Case P^ of
    $F0:
    begin
     Str := Str + '[F0';
     Inc(P);
     Str := Str + ByteToHex(P^);
     If P^ > 0 then
     begin
      Inc(P);
      Str := Str + ByteToHex(P^);
     end;
     Str := Str + ']';
     Break;
    end;
    $F1:      Str := Str + #10;
    $FE:      Str := Str + ' ';
    $FD:
    begin
     Str := Str + '[' + ByteToHex(P^);
     Inc(P);
     Str := Str + ByteToHex(P^) + ']'#10;
    end;
    $EE..$EF, $F2..$FC, $FF:
    begin
     Str := Str + '[' + ByteToHex(P^);
     Inc(P);
     Str := Str + ByteToHex(P^) + ']';
    end;
    $00:      Str := Str + '''';
    $01:      Str := Str + '-';
    $02:      Str := Str + '�';
    $03:      Str := Str + '"';
    $04:      Str := Str + '�';
    $05:      Str := Str + '�';
    $60..$9F: Str := Str + Char(P^ + $60); //�-�, �-�
    $A0..$B9: Str := Str + Char(P^ - $5F); //A-Z
    $BA..$D3: Str := Str + Char(P^ - $59); //a-z
    $D4:      Str := Str + '.';
    $D5:      Str := Str + ',';
    $D6:      Str := Str + ';';
    $D7:      Str := Str + ':';
    $D8:      Str := Str + '!';
    $D9:      Str := Str + '?';
    $DA..$E3: Str := Str + Char(P^ - $AA); //0-9
    Else      Str := Str + '[' + ByteToHex(P^) + ']';
   end;
   Inc(P);
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

