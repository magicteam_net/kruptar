library NULL;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = 'Bee 52';

Var
 ROM: PBytes = NIL;
 RomSize: Integer = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: Integer = 0;
 Align: Integer = 1;

Function Description: String; stdcall;
begin
 Result := SKPLDescription;
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNone;
end;

Procedure SetVariables(X: PBytes; Sz: Integer; ER: PTableItem; MC, AL: Integer); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): String; stdcall;
Var R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
 If Result = '' then Result[Length(Result)] := Char((Byte(Result[Length(Result)]) and $7F) + 128);
end;

Function GetStrings(X, Sz: Integer): PTextStrings; stdcall;
Var B: byte;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 New(Result, Init);
 With Result^.Add^ do
  begin
  Str:='';
  Repeat
  B := ROM^[X];
  If B >= 128 then
   begin
    Str := Str + Char(B and $7F);
    Break;
   end Else Str := Str + Char(B);
   Inc(X);
  Until False;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

