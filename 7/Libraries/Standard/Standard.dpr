library Standard;

uses
  ShareMem,
  Needs in '..\..\Needs.pas';

{$E .kpl}

resourcestring
 SKPLDescription = '���������� ��� ������ �� ��������, ���������������� �������� ����� ������.';

Var
 ROM: PBytes = NIL;
 RomSize: LongInt = 0;
 EndsRoot: PTableItem = NIL;
 MaxCodes: LongInt = 0;
 Align: LongInt = 1;

Function Description: RawByteString; stdcall;
begin
 Result := AnsiString(SKPLDescription);
end;

Function NeedEnd: Boolean; stdcall;
begin
 Result := True;
end;

Function GetMethod: TMethod; stdcall;
begin
 Result := tmNormal;
end;

Procedure SetVariables(X: PBytes; Sz: LongInt; ER: PTableItem; MC, AL: LongInt); stdcall;
begin
 ROM := X;
 RomSize := Sz;
 EndsRoot := ER;
 MaxCodes := MC;
 Align := AL;
end;

Function GetData(TextStrings: PTextStrings): RawByteString; stdcall;
Var R: PTextString;
begin
 Result := '';
 If TextStrings = NIL then Exit;
 With TextStrings^ do
 begin
  R := Root;
  While R <> NIL do
  begin
   Result := Result + R^.Str;
   R := R^.Next;
  end;
 end;
end;

Function GetStrings(X, Sz: LongInt): PTextStrings; stdcall;
Var R: PTableItem; B: ^Byte; I, J: LongInt; S: PTextString;
begin
 Result := NIL;
 If (X >= RomSize) or (X < 0) then Exit;
 If Sz > 0 then
 begin
  New(Result, Init);
  With Result^.Add^ do
  begin
   If X + Sz > RomSize then J := RomSize - X Else J := Sz;
   SetLength(Str, J);
   Move(ROM^[X], Str[1], J);
  end;
 end Else
 begin
  If (EndsRoot = NIL) or (MaxCodes <= 0) then Exit;
  New(Result, Init);
  B := Addr(ROM^[X]);
  I := 0;
  With Result^ do
  begin
   S := Add;
   Repeat
    if I mod Align = 0 then
     R := GetEnd(B, EndsRoot, MaxCodes) else
     R := NIL;
    If R <> NIL then With S^, R^ do
    begin
     Str := Str + tiCodes;
     J := Length(tiCodes);
    end Else With S^ do
    begin
     Str := Str + Char(B^);
     J := 1;
    end;
    Inc(B, J);
    Inc(I, J);

    If (I = Sz) or (X + I > RomSize) then Exit;
    If R <> NIL then
    begin
     If Sz <= 0 then
      Exit Else
      S := Add;
    end;
   Until False;
  end;
 end;
end;

exports
 GetMethod,
 SetVariables,
 GetData,
 GetStrings,
 DisposeStrings,
 NeedEnd,
 Description;

end.

