object MyProgressDialog: TMyProgressDialog
  Left = 268
  Top = 206
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  ClientHeight = 212
  ClientWidth = 307
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TKruptarPanel
    Left = 0
    Top = 0
    Width = 307
    Height = 212
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 0
    object ProjectLabel: TKruptarLabel
      Left = 12
      Top = 102
      Width = 3
      Height = 13
    end
    object PtrTableLabel: TKruptarLabel
      Left = 12
      Top = 9
      Width = 3
      Height = 13
    end
    object PtrTableGauge: TGauge
      Left = 12
      Top = 28
      Width = 278
      Height = 23
      ForeColor = clTeal
      Progress = 0
    end
    object GroupGauge: TGauge
      Left = 12
      Top = 74
      Width = 278
      Height = 23
      ForeColor = clTeal
      Progress = 0
    end
    object ProjectGauge: TGauge
      Left = 12
      Top = 120
      Width = 278
      Height = 23
      ForeColor = clTeal
      Progress = 0
    end
    object GroupLabel: TKruptarLabel
      Left = 12
      Top = 55
      Width = 3
      Height = 13
    end
    object CloseButton: TLocButton
      Left = 114
      Top = 152
      Width = 75
      Height = 25
      Action = ProgressCloseAction
      Cancel = True
      Default = True
      TabOrder = 0
      LocCaption = 'ButtonClose'
      Localizer = MainForm.Localizer
    end
    object AutoCloseCheck: TLocCheckBox
      Left = 12
      Top = 184
      Width = 277
      Height = 17
      Caption = 'Automatically close after finish'
      TabOrder = 1
      OnClick = AutoCloseCheckClick
      LocCaption = 'InsertProgressAutoClose'
      Localizer = MainForm.Localizer
    end
  end
  object ProgressActions: TLocActionList
    Localizer = MainForm.Localizer
    Left = 240
    Top = 152
    object ProgressCloseAction: TLocAction
      Caption = 'Close'
      Enabled = False
      OnExecute = ProgressCloseActionExecute
      OnUpdate = ProgressCloseActionUpdate
      LocCaption = 'ButtonClose'
    end
  end
end
