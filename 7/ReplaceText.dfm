object ReplaceTextDialog: TReplaceTextDialog
  Left = 233
  Top = 214
  BorderStyle = bsDialog
  Caption = 'Find And Replace'
  ClientHeight = 177
  ClientWidth = 353
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  LocCaption = 'FindReplaceDialogTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object SearchLabel: TLocLabel
    Left = 0
    Top = 9
    Width = 85
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Text to find:'
    WordWrap = True
    LocCaption = 'FindReplaceDialogFind'
    Localizer = MainForm.Localizer
  end
  object ReplaceLabel: TLocLabel
    Left = 0
    Top = 37
    Width = 85
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Replace with:'
    WordWrap = True
    LocCaption = 'FindReplaceDialogReplace'
    Localizer = MainForm.Localizer
  end
  object SearchText: TKruptarComboBox
    Left = 88
    Top = 9
    Width = 262
    Height = 26
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object OptionsBox: TLocGroupBox
    Left = 3
    Top = 65
    Width = 347
    Height = 75
    Caption = 'Options'
    TabOrder = 2
    LocCaption = 'FindReplaceDialogOptions'
    Localizer = MainForm.Localizer
    object CaseCheckBox: TLocCheckBox
      Left = 9
      Top = 18
      Width = 328
      Height = 20
      Caption = 'Case sensitive'
      TabOrder = 0
      LocCaption = 'FindReplaceDialogCaseSensitive'
      Localizer = MainForm.Localizer
    end
    object PromtCheckBox: TLocCheckBox
      Left = 9
      Top = 46
      Width = 328
      Height = 20
      Caption = 'Prompt on replace'
      Checked = True
      State = cbChecked
      TabOrder = 1
      LocCaption = 'FindReplaceDialogPrompt'
      Localizer = MainForm.Localizer
    end
  end
  object OkButton: TLocButton
    Left = 5
    Top = 148
    Width = 75
    Height = 25
    Caption = #1054#1050
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = OkButtonClick
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelButton: TLocButton
    Left = 271
    Top = 148
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
  object ReplaceText: TKruptarComboBox
    Left = 88
    Top = 37
    Width = 262
    Height = 26
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object ReplaceAllButton: TLocButton
    Left = 85
    Top = 148
    Width = 92
    Height = 25
    Caption = 'Replace All'
    ModalResult = 10
    TabOrder = 4
    OnClick = ReplaceAllButtonClick
    LocCaption = 'ButtonReplaceAll'
    Localizer = MainForm.Localizer
  end
end
