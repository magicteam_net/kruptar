unit ReplaceText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, LocClasses, KruptarComponents;

type
  TReplaceTextDialog = class(TLocForm)
    SearchText: TKruptarComboBox;
    SearchLabel: TLocLabel;
    OptionsBox: TLocGroupBox;
    CaseCheckBox: TLocCheckBox;
    OkButton: TLocButton;
    CancelButton: TLocButton;
    ReplaceText: TKruptarComboBox;
    ReplaceLabel: TLocLabel;
    PromtCheckBox: TLocCheckBox;
    ReplaceAllButton: TLocButton;
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ReplaceAllButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
   SearchString: WideString;
   ReplaceString: WideString;
   Function Execute: Boolean;
  end;

var
  ReplaceTextDialog: TReplaceTextDialog;
  OkPressed: Boolean;

implementation

Uses MainUnit, SearchText, StringConsts, LocMsgBox;

{$R *.dfm}

Function TReplaceTextDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult in [mrOk, mrYesToAll];
 If Result then
 begin
  SearchString := SearchText.Text;
  ReplaceString := ReplaceText.Text;
  SearchText.Items.Add(SearchString);
  ReplaceText.Items.Add(ReplaceString);
  SearchTextDialog.SearchText.Items := SearchText.Items;
 end Else
 begin
  SearchString := '';
  ReplaceString := '';
 end;
end;

procedure TReplaceTextDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 SearchText.SetFocus;
end;

procedure TReplaceTextDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TReplaceTextDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If OkPressed then
 begin
  If SearchText.Text = '' then
  begin
   LocMessageDlg(K7S(SRTInputSearchText), mtError, [mbOk], 0);
   CanClose := False;
   OkPressed := False;
  end;
 end;
end;

procedure TReplaceTextDialog.ReplaceAllButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

end.
