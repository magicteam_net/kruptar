object AddPointersDialog: TAddPointersDialog
  Left = 215
  Top = 447
  BorderStyle = bsDialog
  ClientHeight = 136
  ClientWidth = 262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 0
    Top = 0
    Width = 262
    Height = 97
    Align = alTop
    Shape = bsFrame
  end
  object BlockStartLabel: TLocLabel
    Left = 8
    Top = 8
    Width = 60
    Height = 13
    Caption = 'Start offset:'
    LocCaption = 'AddPointersDialogStartOffset'
    Localizer = MainForm.Localizer
  end
  object BlockEndLabel: TLocLabel
    Left = 8
    Top = 48
    Width = 63
    Height = 13
    Caption = 'Finish offset:'
    LocCaption = 'AddPointersDialogFinishOffset'
    Localizer = MainForm.Localizer
  end
  object OkButton: TLocButton
    Left = 101
    Top = 105
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
    OnClick = OkButtonClick
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelButton: TLocButton
    Left = 181
    Top = 105
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
  object BlockStartEdit: TEdit
    Left = 10
    Top = 24
    Width = 241
    Height = 21
    TabOrder = 2
  end
  object BlockEndEdit: TEdit
    Left = 10
    Top = 64
    Width = 241
    Height = 21
    TabOrder = 3
  end
end
