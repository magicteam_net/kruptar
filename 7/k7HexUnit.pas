unit k7HexUnit;

interface

Uses Windows, SysUtils;

Function HexVal(s: string): DWord;
Function GetLDW(s: string): LongInt;
Function HCheckOut(s: string): Boolean;
Function UnsCheckOut(s: string): Boolean;
Function DeclinateNumeralString(Number: LongInt; S1, S2, S3: AnsiString): AnsiString; overload;
Function DeclinateNumeralString(Number: Int64; S1, S2, S3: AnsiString): AnsiString; overload;
Function DeclinateNumeralString(Number: LongInt; S1, S2, S3: WideString): WideString; overload;
Function DeclinateNumeralString(Number: Int64; S1, S2, S3: WideString): WideString; overload;
function Aligned(Value, Alignment: LongInt): LongInt;

const
 HexSet: set of AnsiChar = ['0'..'9', 'A'..'F', 'a'..'f'];

var
 HexError: Boolean = False;
 IntError: Boolean = False;

implementation


function Aligned(Value, Alignment: LongInt): LongInt;
asm
 cmp  edx,1
 jle  @@Exit
 mov  ecx,edx
 dec  ecx
 add  eax,ecx
 push eax
 mov  ecx,edx
 cdq
 idiv ecx
 pop eax
 sub eax,edx
@@Exit:
end;
{begin
 if Alignment > 1 then
 begin
  Result := Value + (Alignment - 1);
  Dec(Result, Result mod Alignment);
 end else Result := Value;
end;}

function StrToInt(const S: string): Integer;
Var E: Integer;
begin
 Val(S, Result, E);
 IntError := E <> 0;
end;

Function GetLDW(s: string): LongInt;
begin
 IntError := True;
 If S = '' then
 begin
  Result := 0;
  Exit;
 end;
 case s[1] of
   'h', 'H':
   begin
    Delete(s, 1, 1);
    If S = '' then
    begin
     Result := 0;
     HexError := True;
     IntError := False;
    end Else
    begin
     Result := LongInt(Hexval(s));
     IntError := False;
    end;
   end;
   Else Result := StrToInt(s);
 end;
end;

Function HexVal(s: string): DWord;
Function sign(h: Char): Byte;
begin
 case h of
   '0'..'9': Result := Ord(h) - 48;
   'A'..'F': Result := Ord(h) - 55;
   'a'..'f': Result := Ord(h) - 87;
   else      Result := 0;
 end;
end;
Const hs: Array[1..8] of LongWord = ($1,
                                  $10,
                                  $100,
                                  $1000,
                                  $10000,
                                  $100000,
                                  $1000000,
                                  $10000000);
Var i: Byte;
begin
 hexerror := true;
 Result := 0;
 If Length(s) <= 8 then
 begin
  For i := 1 to Length(s) do
    case s[i] of
      '0'..'9', 'A'..'F', 'a'..'f':;
      else Exit;
    end;
  Result := 0;
  for i := 1 to Length(s) do
    Inc(Result, sign(s[i]) * hs[length(s) + 1 - i]);
  hexerror := false;
 end;
end;

Function HCheckOut(s: string): Boolean;
Var i: integer;
begin
 Result := False;
 if s = '' then exit;
 Result := True;
 case s[1] of
   'h', 'H':
   begin
    Delete(s, 1, 1);
    if (length(s) > 8) or (S = '') then
    begin
     Result := false;
     Exit;
    end;
    For I := 1 to Length(s) do
     case s[i] of
      '0'..'9', 'A'..'F', 'a'..'f': ;
      else
      begin
        Result := False;
        Exit;
      end;
     end;
   end;
   Else
   begin
    If s[1] = '-' then
     Delete(s, 1, 1);
    If (Length(s) > 10) or (S = '') then
    begin
     Result := false;
     Exit;
    end;
    For i := 1 to Length(s) do
     case s[i] of
      '0'..'9': ;
      else
      begin
        Result := False;
        Exit;
      end;
     end;
   end;
 end;
end;

Function UnsCheckOut(s: string): Boolean;
Var i: integer;
begin
 Result := False;
 if s = '' then exit;
 Result := True;
 case s[1] of
   'h', 'H':
   begin
    Delete(s, 1, 1);
    if (length(s) > 8) or (S = '') then
    begin
     Result := false;
     Exit;
    end;
    For i := 1 to Length(s) do
     case s[i] of
       '0'..'9', 'A'..'F', 'a'..'f': ;
       else
       begin
        Result := False;
        Exit;
       end;
     end;
   end;
   Else
   begin
    if (length(s) > 10) or (S = '') then
    begin
     Result := false;
     Exit;
    end;
    For i := 1 to Length(s) do
     case s[i] of
       '0'..'9': ;
       else
       begin
        Result := False;
        Exit;
       end;
     end;
   end;
 end;
end;

Function DeclinateNumeralString(Number: LongInt; S1, S2, S3: AnsiString): AnsiString;
begin
 If Number = 1 then Result := S1 Else
 Case Number mod 10 of
  1: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S1;
  2, 3, 4: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S3;
  Else Result := S2;
 end;
end;

Function DeclinateNumeralString(Number: Int64; S1, S2, S3: AnsiString): AnsiString;
begin
 If Number = 1 then Result := S1 Else
 Case Number mod 10 of
  1: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S1;
  2, 3, 4: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S3;
  Else Result := S2;
 end;
end;

Function DeclinateNumeralString(Number: LongInt; S1, S2, S3: WideString): WideString;
begin
 If Number = 1 then Result := S1 Else
 Case Number mod 10 of
  1: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S1;
  2, 3, 4: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S3;
  Else Result := S2;
 end;
end;

Function DeclinateNumeralString(Number: Int64; S1, S2, S3: WideString): WideString;
begin
 If Number = 1 then Result := S1 Else
 Case Number mod 10 of
  1: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S1;
  2, 3, 4: If (Number div 10) mod 10 = 1 then
   Result := S2 Else
   Result := S3;
  Else Result := S2;
 end;
end;

end.
