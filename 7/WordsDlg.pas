unit WordsDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, LocClasses, KruptarComponents;

type
  TWordsDialog = class(TLocForm)
    ListView: TKruptarListView;
    OkButton: TLocButton;
    CancelButton: TLocButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
     Function Execute: Boolean;
  end;

var
  WordsDialog: TWordsDialog;

implementation

uses MainUnit, StringConsts;

{$R *.dfm}

Function TWordsDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TWordsDialog.FormShow(Sender: TObject);
begin
 ListView.Columns[0].Caption := K7S(SWDCol1);
 ListView.Columns[1].Caption := K7S(SWDCol2);
end;

end.
