object WordsDialog: TWordsDialog
  Left = 489
  Top = 375
  BorderStyle = bsDialog
  Caption = 'Generated Dictionary'
  ClientHeight = 272
  ClientWidth = 402
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  LocCaption = 'GeneratedDictDialogTitle'
  Localizer = MainForm.Localizer
  PixelsPerInch = 96
  TextHeight = 13
  object ListView: TKruptarListView
    Left = 0
    Top = 0
    Width = 402
    Height = 232
    Align = alTop
    Columns = <
      item
        AutoSize = True
      end
      item
        AutoSize = True
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial Unicode MS'
    Font.Style = []
    GridLines = True
    ReadOnly = True
    ParentFont = False
    TabOrder = 0
    ViewStyle = vsReport
  end
  object OkButton: TLocButton
    Left = 239
    Top = 240
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    LocCaption = 'ButtonOK'
    Localizer = MainForm.Localizer
  end
  object CancelButton: TLocButton
    Left = 323
    Top = 240
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
    LocCaption = 'ButtonCancel'
    Localizer = MainForm.Localizer
  end
end
