unit UnicodeFiles deprecated;

interface

Uses SysUtils;

Type
 PUnicodeTextFile = ^TUnicodeTextFile;
 TUnicodeTextFile = Object
  FName: String;
  Data: WideString;
  NPos: Integer;
  WriteMode: Boolean;   
  Constructor AssignFile(FileName: String);
  Procedure Rewrite;
  Procedure Readln(Var S: WideString);
  Procedure Writeln(S: WideString);
  Function EndOfFile: Boolean;
  Destructor CloseFile;
 end;

implementation

Constructor TUnicodeTextFile.AssignFile(FileName: String);
Var F: File; W, R: Integer;
begin
 FName := FileName;
 If not FileExists(FileName) then
 begin
  Data := '';
  NPos := 0;
  WriteMode := False;
 end Else
 begin
  System.AssignFile(F, FileName);
  System.Reset(F, 1);
  WriteMode := False;
  W := 0;
  BlockRead(F, W, 2, R);
  If W = $FEFF then
  begin
   W := FileSize(F) - 2;
   If W > 0 then
   begin
    SetLength(Data, W div 2);
    BlockRead(F, Data[1], W, R);
   end Else Data := '';
  end Else Data := '';
  NPos := 0;
  WriteMode := False;
  System.CloseFile(F);
 end;
end;

Function TUnicodeTextFile.EndOfFile: Boolean;
begin
 Result := NPos >= Length(Data);
end;

Procedure TUnicodeTextFile.Rewrite;
begin
 WriteMode := True;
 NPos := 0;
 Data := '';
end;

Procedure TUnicodeTextFile.Readln(Var S: WideString);
begin
 S := ''; 
 While not EndOfFile do
 begin
  If Data[NPos + 1] = #13 then
  begin
   Inc(NPos);
   If Data[NPos + 1] <> #10 then Exit;
  end;
  If EndOfFile then Exit;
  If Data[NPos + 1] = #10 then begin Inc(NPos); Exit end;
  S := S + Data[NPos + 1];
  Inc(NPos);
 end;
end;

Procedure TUnicodeTextFile.Writeln(S: WideString);
begin
 If not WriteMode then Exit;
 Data := Data + S + #13 + #10;
end;

Destructor TUnicodeTextFile.CloseFile;
Var F: File; W: Integer;
begin
 If WriteMode then
 begin
  System.AssignFile(F, FName);
  System.Rewrite(F, 1);
  W := $FEFF;
  BlockWrite(F, W, 2);
  BlockWrite(F, Data[1], Length(Data) * 2);
  System.CloseFile(F);
 end;
 Data := '';
 FName := '';
 NPos := 0;
 WriteMode := False;
end;

end.
