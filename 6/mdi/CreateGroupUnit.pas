unit CreateGroupUnit;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Dialogs, Spin, CheckLst;

type
  TCreateGroupForm = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    Bevel: TBevel;
    Bevel1: TBevel;
    PointerSizeRadio: TRadioGroup;
    DivisionRadio: TRadioGroup;
    TypeRadio: TRadioGroup;
    MethodRadio: TRadioGroup;
    FirstPtrEdit: TLabeledEdit;
    EncodeList: TComboBox;
    EncodeLabel: TLabel;
    BlockStartEdit: TLabeledEdit;
    BlockEndEdit: TLabeledEdit;
    BlocksList: TListBox;
    AddBlockButton: TSpeedButton;
    Bevel2: TBevel;
    BlockLabel: TLabel;
    InputTableEdit: TLabeledEdit;
    OutputTableEdit: TLabeledEdit;
    OpenInputButton: TSpeedButton;
    OpenOutputButton: TSpeedButton;
    OpenDialog: TOpenDialog;
    MoveUpButton: TSpeedButton;
    MoveDownButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    IntervalEdit: TSpinEdit;
    IntervalLabel: TLabel;
    OpenDllDialog: TOpenDialog;
    CheckListBox: TCheckListBox;
    Label1: TLabel;
    AlignEdit: TSpinEdit;
    procedure OpenInputButtonClick(Sender: TObject);
    procedure OpenOutputButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelButtonClick(Sender: TObject);
    procedure AddBlockButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreateGroupForm: TCreateGroupForm;

implementation

{$R *.dfm}

Uses HexUnit;

Var bCancelButton, bokbutton: Boolean;

procedure TCreateGroupForm.OpenInputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  InputTableEdit.Text := OpenDialog.FileName;
end;

procedure TCreateGroupForm.OpenOutputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  OutputTableEdit.Text := OpenDialog.FileName;
end;

procedure TCreateGroupForm.FormShow(Sender: TObject);
Var I: Integer;
begin
 InputTableEdit.SetFocus;
 InputTableEdit.Text := '';
 OutputTableEdit.Text := '';
 FirstPtrEdit.Text := 'h00000000';
 BlockStartEdit.Text := 'h00000000';
 BlockEndEdit.Text := 'h00000000';
 BlocksList.Clear;
 EncodeList.ItemIndex := 0;
 PointerSizeRadio.ItemIndex := 0;
 DivisionRadio.ItemIndex := 0;
 TypeRadio.ItemIndex := 0;
 AlignEdit.Value := 1;
 IntervalEdit.Value := 0;
 For I := 0 to 2 do CheckListBox.Checked[I] := False;
 bCancelButton := false;
 bokbutton := false;
end;

procedure TCreateGroupForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If not FileExists(InputTableEdit.Text) then
 begin
  ShowMessage('���� "' + InputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not FileExists(OutputTableEdit.Text) then
 begin
  ShowMessage('���� "' + OutputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not HCheckOut(FirstPtrEdit.Text) then
 begin
  ShowMessage('"' + FirstPtrEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end Else
 If not UnsCheckOut(IntervalEdit.Text) then
 begin
  ShowMessage('"' + IntervalEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end;
end;

procedure TCreateGroupForm.CancelButtonClick(Sender: TObject);
begin
 bCancelButton := True;
end;

procedure TCreateGroupForm.AddBlockButtonClick(Sender: TObject);
begin
 If HCheckOut(BlockStartEdit.Text) and HCheckOut(BlockEndEdit.Text) and
    (GetLDW(BlockStartEdit.Text) >= 0) and (GetLDW(BlockEndEdit.Text) >= 0) then
 begin
  BlocksList.Items.Add(BlockStartEdit.Text);
  BlocksList.Items.Add(BlockEndEdit.Text);
 end Else ShowMessage('���� ��� ��� �������� �������� ������� ��� ������ ����!');
end;

procedure TCreateGroupForm.OKButtonClick(Sender: TObject);
begin
 bokbutton := true;
end;

procedure TCreateGroupForm.MoveUpButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex > 0 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex - 1;
 end;
end;

procedure TCreateGroupForm.MoveDownButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex < BlocksList.Items.Count - 1 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex + 1;
 end;
end;

procedure TCreateGroupForm.DeleteButtonClick(Sender: TObject);
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex mod 2 = 1 then
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex - 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end Else
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex + 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end;
end;

end.
