object SearchTextDialog: TSearchTextDialog
  Left = 309
  Top = 337
  BorderStyle = bsDialog
  Caption = #1055#1086#1080#1089#1082' '#1090#1077#1082#1089#1090#1072
  ClientHeight = 134
  ClientWidth = 292
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SearchLabel: TLabel
    Left = 6
    Top = 12
    Width = 34
    Height = 13
    Caption = #1053#1072#1081#1090#1080':'
  end
  object SearchText: TTntComboBox
    Left = 46
    Top = 8
    Width = 241
    Height = 23
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial Unicode MS'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 0
  end
  object OptionsBox: TGroupBox
    Left = 5
    Top = 32
    Width = 283
    Height = 65
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
    TabOrder = 1
    object CaseCheckBox: TCheckBox
      Left = 8
      Top = 16
      Width = 121
      Height = 17
      Caption = #1059#1095#1080#1090#1099#1074#1072#1090#1100' '#1088#1077#1075#1080#1089#1090#1088
      TabOrder = 0
    end
    object WholeCheckBox: TCheckBox
      Left = 8
      Top = 40
      Width = 121
      Height = 17
      Caption = #1048#1089#1082#1072#1090#1100' '#1074' '#1086#1088#1080#1075#1080#1085#1072#1083#1077
      TabOrder = 1
    end
  end
  object OkButton: TButton
    Left = 68
    Top = 104
    Width = 75
    Height = 25
    Caption = #1054#1050
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 148
    Top = 104
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 3
  end
end
