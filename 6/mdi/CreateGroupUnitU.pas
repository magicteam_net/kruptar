unit CreateGroupUnitU;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Dialogs, Spin;

type
  TCreateGroupFormU = class(TForm)
    OKButton: TButton;
    CancelButton: TButton;
    Bevel1: TBevel;
    InputTableEdit: TLabeledEdit;
    OpenInputButton: TSpeedButton;
    OpenOutputButton: TSpeedButton;
    OutputTableEdit: TLabeledEdit;
    Bevel2: TBevel;
    BlocksList: TListBox;
    BlockLabel: TLabel;
    AddBlockButton: TSpeedButton;
    BlockEndEdit: TLabeledEdit;
    BlockStartEdit: TLabeledEdit;
    EncodeList: TComboBox;
    EncodeLabel: TLabel;
    OpenDialog: TOpenDialog;
    StringLengthEdit: TLabeledEdit;
    MoveUpButton: TSpeedButton;
    MoveDownButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    IntervalLabel: TLabel;
    IntervalEdit: TSpinEdit;
    procedure OpenInputButtonClick(Sender: TObject);
    procedure OpenOutputButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure AddBlockButtonClick(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure StringLengthEditChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreateGroupFormU: TCreateGroupFormU;

implementation

{$R *.dfm}

Uses HexUnit;

Var bCancelButton, bokbutton: Boolean;

procedure TCreateGroupFormU.OpenInputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  InputTableEdit.Text := OpenDialog.FileName;
end;

procedure TCreateGroupFormU.OpenOutputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  OutputTableEdit.Text := OpenDialog.FileName;
end;

procedure TCreateGroupFormU.FormShow(Sender: TObject);
begin
 InputTableEdit.SetFocus;
 InputTableEdit.Text := '';
 OutputTableEdit.Text := '';
 BlockStartEdit.Text := 'h00000000';
 BlockEndEdit.Text := 'h00000000';
 StringLengthEdit.Text := '';
 IntervalEdit.Enabled := False;
 BlocksList.Clear;
 EncodeList.ItemIndex := 0;
 bCancelButton := false;
 bokbutton := false;
end;

procedure TCreateGroupFormU.CancelButtonClick(Sender: TObject);
begin
 bCancelButton := true;
end;

procedure TCreateGroupFormU.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If not FileExists(InputTableEdit.Text) then
 begin
  ShowMessage('���� "' + InputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not FileExists(OutputTableEdit.Text) then
 begin
  ShowMessage('���� "' + OutputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If (StringLengthEdit.Text <> '') and not UnsCheckOut(StringLengthEdit.Text) then
 begin
  If GetLDW(StringLengthEdit.Text) = 0 then
  ShowMessage('����� ������ �� ����� ���� ����� ����!') Else
  ShowMessage('"' + StringLengthEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end Else
 If IntervalEdit.Enabled and not UnsCheckOut(IntervalEdit.Text) then
 begin
  ShowMessage('"' + IntervalEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end;
end;

procedure TCreateGroupFormU.AddBlockButtonClick(Sender: TObject);
begin
 If HCheckOut(BlockStartEdit.Text) and HCheckOut(BlockEndEdit.Text) and
    (GetLDW(BlockStartEdit.Text) >= 0) and (GetLDW(BlockEndEdit.Text) >= 0) then
 begin
  BlocksList.Items.Add(BlockStartEdit.Text);
  BlocksList.Items.Add(BlockEndEdit.Text);
 end Else ShowMessage('���� ��� ��� �������� �������� ������� ��� ������ ����!');
end;

procedure TCreateGroupFormU.OKButtonClick(Sender: TObject);
begin
 bokbutton := true;
end;

procedure TCreateGroupFormU.MoveUpButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex > 0 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex - 1;
 end;                                            
end;

procedure TCreateGroupFormU.MoveDownButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex < BlocksList.Items.Count - 1 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex + 1;
 end;
end;

procedure TCreateGroupFormU.DeleteButtonClick(Sender: TObject);
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex mod 2 = 1 then
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex - 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end Else
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex + 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end;
end;

procedure TCreateGroupFormU.StringLengthEditChange(Sender: TObject);
begin
 IntervalEdit.Enabled := StringLengthEdit.Text <> '';
 IntervalLabel.Enabled := IntervalEdit.Enabled;
end;

end.
