�
 TCREATEGROUPFORM 0�(  TPF0TCreateGroupFormCreateGroupFormLeft�Top� AutoSize	BorderStylebsDialogCaption   Новая группаClientHeight�ClientWidthaColor	clBtnFace
ParentFont	OldCreateOrder	PositionpoDesktopCenterOnCloseQueryFormCloseQueryOnShowFormShowPixelsPerInch`
TextHeight TBevelBevelLeft Top WidthaHeight�ShapebsFrame  TBevelBevel2Left TopXWidth!HeightQShapebsFrame  TBevelBevel1LeftTopXWidthIHeightYShapebsFrame  TLabelEncodeLabelLeft� Top0Width7HeightCaption	   >48@>2:0  TSpeedButtonAddBlockButtonLeft� TopxWidthHeightHint   >1028BLCaption>>OnClickAddBlockButtonClick  TLabel
BlockLabelLeft� TopZWidthLHeightCaption   Блоки для текстаFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 
ParentFont  TSpeedButtonOpenInputButtonLeftATopWidthHeight
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������������������������������������������������������                                 ���������������       �� �� �� �� �� �� �� �� ��   ������������    ��    �� �� �� �� �� �� �� �� ��   ���������   ��� ��    �� �� �� �� �� �� �� �� ��   ������    ����� ��    �� �� �� �� �� �� �� �� ��   ���   ��� ����� ��                                     ����� ����� ����� ����� ��   ���������������   ��� ����� ����� ����� �����   ���������������    ����� ��                     ������������������         ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������OnClickOpenInputButtonClick  TSpeedButtonOpenOutputButtonLeftATop@WidthHeight
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������������������������������������������������������                                 ���������������       �� �� �� �� �� �� �� �� ��   ������������    ��    �� �� �� �� �� �� �� �� ��   ���������   ��� ��    �� �� �� �� �� �� �� �� ��   ������    ����� ��    �� �� �� �� �� �� �� �� ��   ���   ��� ����� ��                                     ����� ����� ����� ����� ��   ���������������   ��� ����� ����� ����� �����   ���������������    ����� ��                     ������������������         ������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������OnClickOpenOutputButtonClick  TSpeedButtonMoveUpButtonLeft!Top`WidthHeightHint!   Передвинуть вверх
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������������������������         ���������������������������������������������������������������������������������������         ���������������������������������������         ���������������������������������������������������������������������������������������         ���������������������������������������         ������������������������������                           ������������������������                     ������������������������������               ������������������������������������         ������������������������������������������   ������������������������������������������������������������������������������������������������������������������������OnClickMoveUpButtonClick  TSpeedButtonMoveDownButtonLeft!TopxWidthHeightHint   Передвинуть вниз
Glyph.Data
:  6  BM6      6   (                  �  �          ���������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������   ������������������������������������������         ������������������������������������               ������������������������������                     ������������������������                           ������������������������������         ���������������������������������������         ���������������������������������������������������������������������������������������         ���������������������������������������         ���������������������������������������������������������������������������������������         ���������������������OnClickMoveDownButtonClick  TSpeedButtonDeleteButtonLeft!Top�WidthHeightHint   #40;8BL
Glyph.Data
:  6  BM6      6   (                  �  �          ������������������������������������������������������������������������������������������������������   ������������������������������   ���������         ���������������������������������������            ���������������������   ���������������         ������������������   ���������������������         ������������      ������������������������         ������      ������������������������������               ������������������������������������         ������������������������������������               ������������������������������         ������      ���������������������            ������������      ���������������            ������������������      ������������         ���������������������������   ������������������������������������������������������OnClickDeleteButtonClick  TLabelIntervalLabelLeftHTop� Width+HeightCaption   Интервал:Font.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 
ParentFont  TLabelLabel1Left� Top� WidthAHeightCaption   Выравнивание:Font.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 
ParentFont  TButtonOKButtonLeft[Top�WidthKHeightCaptionOKDefault	ModalResultTabOrderOnClickOKButtonClick  TButtonCancelButtonLeft� Top�WidthKHeightCancel	Caption   B<5=0ModalResultTabOrderOnClickCancelButtonClick  TRadioGroupPointerSizeRadioLeftTop`WidthYHeight9Caption   Размер пойнтераFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 	ItemIndex Items.Strings   Два байта   Три байта   Четыре байта 
ParentFontTabOrder  TRadioGroupDivisionRadioLeftxTop`Width9Height9Caption	   @0B=>ABLFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 	ItemIndex Items.Strings1248 
ParentFontTabOrder  TRadioGroup	TypeRadioLeft� Top`WidthIHeight9Caption   "8?Font.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 	ItemIndex Items.StringsNormalPunType	AutoStart 
ParentFontTabOrder  TRadioGroupMethodRadioLeftTop� WidthIHeightuCaption(   Метод хранения текстаFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 	ItemIndex Items.Strings   Текст + /$   Длина (1 байт) + Текст(   Длина (1 байт) + Текст + /j   Длина (1 байт) + Первая строка + Длина (1 байт) +  Вторая строкаc   Кол-во строк (2 байта) + 1-ая строка + / + ... + / + N-ая строка + /D   Длина (1 байт) + X (1 байт) + Данные + ТекстSpecialPtr2Ptr 
ParentFontTabOrder  TLabeledEditFirstPtrEditLeft6Top@WidthyHeightEditLabel.WidthcEditLabel.HeightEditLabel.Caption   Разница смещенийLabelPositionlpAboveLabelSpacingTabOrder  	TComboBox
EncodeListLeft� Top@WidthyHeightStylecsDropDownList
ItemHeightTabOrderItems.StringsANSI	SHIFT-JISUNICODE   TLabeledEditBlockStartEditLeft(TophWidthyHeightEditLabel.Width_EditLabel.HeightEditLabel.Caption#   Начальное смещениеEditLabel.Font.CharsetRUSSIAN_CHARSETEditLabel.Font.ColorclWindowTextEditLabel.Font.Height�EditLabel.Font.NameSmall FontsEditLabel.Font.Style EditLabel.ParentFontLabelPositionlpAboveLabelSpacingTabOrder  TLabeledEditBlockEndEditLeft(Top�WidthyHeightEditLabel.WidthZEditLabel.HeightEditLabel.Caption!   Конечное смещениеEditLabel.Font.CharsetRUSSIAN_CHARSETEditLabel.Font.ColorclWindowTextEditLabel.Font.Height�EditLabel.Font.NameSmall FontsEditLabel.Font.Style EditLabel.ParentFontLabelPositionlpAboveLabelSpacingTabOrder	  TListBox
BlocksListLeft� TophWidthaHeight9Font.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style 
ItemHeight
ParentFontTabOrder
  TLabeledEditInputTableEditLeft	TopWidth8HeightEditLabel.Width� EditLabel.HeightEditLabel.Caption6   Таблица для оригинального ROM'aLabelPositionlpAboveLabelSpacingTabOrder   TLabeledEditOutputTableEditLeft	Top@Width8HeightEditLabel.Width� EditLabel.HeightEditLabel.Caption3   Таблица для изменяемого ROM'a LabelPositionlpAboveLabelSpacingTabOrder  	TSpinEditIntervalEditLeftxTop� Width)HeightFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style MaxValue� MinValue 
ParentFontTabOrderValue   TCheckListBoxCheckListBoxLeftTophWidthIHeight1
ItemHeightItems.StringsMotorolaSignedShl 1 TabOrder  	TSpinEdit	AlignEditLeft� Top� Width)HeightFont.CharsetRUSSIAN_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameSmall Fonts
Font.Style MaxValueMinValue
ParentFontTabOrderValue   TOpenDialog
OpenDialog
DefaultExt.tblFilter   Таблица (*.tbl)|*.tblOptionsofHideReadOnlyofFileMustExistofEnableSizingofDontAddToRecent Left0Top   TOpenDialogOpenDllDialog
DefaultExt.dllFilterDynamic Link Library|*.dllOptionsofHideReadOnlyofFileMustExistofEnableSizing Left0Top@   