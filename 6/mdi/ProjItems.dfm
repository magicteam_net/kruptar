object ProjectItemsForm: TProjectItemsForm
  Left = 181
  Top = 214
  Width = 217
  Height = 292
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsSizeToolWin
  Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1087#1088#1086#1077#1082#1090#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormShow
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TreeView: TTreeView
    Left = 0
    Top = 0
    Width = 209
    Height = 262
    Align = alClient
    HideSelection = False
    Images = KruptarForm.ImageList
    Indent = 19
    TabOrder = 0
    OnChange = TreeViewChange
    OnEdited = TreeViewEdited
    OnKeyDown = TreeViewKeyDown
    OnKeyPress = TreeViewKeyPress
  end
end
