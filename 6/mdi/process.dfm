object procform: Tprocform
  Left = 351
  Top = 320
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 57
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Gauge1: TGauge
    Left = 0
    Top = 0
    Width = 441
    Height = 25
    Progress = 0
  end
  object Label1: TLabel
    Left = 7
    Top = 40
    Width = 3
    Height = 13
  end
  object Button1: TButton
    Left = 367
    Top = 32
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object imptimer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = imptimerTimer
    Left = 8
    Top = 24
  end
  object downl: TNMHTTP
    Host = 'http://djinn.avs.ru'
    Port = 80
    ReportLevel = 0
    OnPacketRecvd = downlPacketRecvd
    Body = 'info.txt'
    Header = 'Head.txt'
    InputFileMode = True
    OutputFileMode = False
    OnSuccess = downlSuccess
    ProxyPort = 0
    Left = 40
    Top = 24
  end
end
