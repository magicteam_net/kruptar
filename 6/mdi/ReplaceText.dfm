object ReplaceTextDialog: TReplaceTextDialog
  Left = 320
  Top = 591
  BorderStyle = bsDialog
  Caption = #1047#1072#1084#1077#1085#1072' '#1090#1077#1082#1089#1090#1072
  ClientHeight = 158
  ClientWidth = 306
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SearchLabel: TLabel
    Left = 4
    Top = 12
    Width = 34
    Height = 13
    Caption = #1053#1072#1081#1090#1080':'
  end
  object ReplaceLabel: TLabel
    Left = 4
    Top = 36
    Width = 68
    Height = 13
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1085#1072':'
  end
  object SearchText: TTntComboBox
    Left = 78
    Top = 8
    Width = 225
    Height = 23
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial Unicode MS'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 0
  end
  object OptionsBox: TGroupBox
    Left = 3
    Top = 56
    Width = 300
    Height = 65
    Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
    TabOrder = 2
    object CaseCheckBox: TCheckBox
      Left = 8
      Top = 16
      Width = 121
      Height = 17
      Caption = #1059#1095#1080#1090#1099#1074#1072#1090#1100' '#1088#1077#1075#1080#1089#1090#1088
      TabOrder = 0
    end
    object PromtCheckBox: TCheckBox
      Left = 8
      Top = 40
      Width = 137
      Height = 17
      Caption = #1055#1086#1076#1090#1074#1077#1088#1078#1076#1072#1090#1100' '#1079#1072#1084#1077#1085#1091
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object OkButton: TButton
    Left = 32
    Top = 128
    Width = 75
    Height = 25
    Caption = #1054#1050
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 200
    Top = 128
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 5
  end
  object ReplaceText: TTntComboBox
    Left = 78
    Top = 32
    Width = 225
    Height = 23
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial Unicode MS'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 1
  end
  object ReplaceAllButton: TButton
    Left = 108
    Top = 128
    Width = 89
    Height = 25
    Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1074#1089#1105
    ModalResult = 10
    TabOrder = 4
    OnClick = ReplaceAllButtonClick
  end
end
