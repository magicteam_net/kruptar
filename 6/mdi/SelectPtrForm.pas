unit SelectPtrForm;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls;

type
  TSelectPtrDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Radio: TRadioGroup;
    procedure OKBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    Function Execute: Boolean;
  end;

var
  SelectPtrDlg: TSelectPtrDlg;
  OkPressed: Boolean;

implementation

{$R *.dfm}

Uses HexUnit, Dialogs;

Function TSelectPtrDlg.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

procedure TSelectPtrDlg.OKBtnClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TSelectPtrDlg.FormShow(Sender: TObject);
begin
 OkPressed := False;
end;

end.
