unit Hexedit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ComCtrls, ExtCtrls, Menus;

type
  THexEditForm = class(TForm)
    Pages: TPageControl;
    Original: TTabSheet;
    EditMode: TTabSheet;
    HexGridO: TStringGrid;
    HexGridE: TStringGrid;
    StatusBar: TStatusBar;
    Timer: TTimer;
    PopupMenu: TPopupMenu;
    GotoN: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure HexGridEKeyPress(Sender: TObject; var Key: Char);
    procedure TimerTimer(Sender: TObject);
    procedure HexGridEKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HexGridOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PagesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GotoNClick(Sender: TObject);
  private
    fColCount, fRowCount: Integer;
    fPosition: Dword;
    foPosition: Dword;
    Function GetPos: Dword;
    Procedure SetPos(X: DWord);
    Function GetoPos: Dword;
    Procedure SetoPos(X: DWord);
    Function GetCc: Integer;
    Function GetRc: Integer;
    Procedure SetCc(X: Integer);
    Procedure SetRc(X: Integer);
  public
    Procedure RefreshHex;
    Procedure DrawText;
    Property ColCount: Integer read GetCc write SetCc;
    Property RowCount: Integer read GetRc write SetRc;
    Property Position: DWord read GetPos write SetPos;
    Property oPosition: DWord read GetoPos write SetoPos;
  end;

var
  HexEditForm: THexEditForm;
  Need: Boolean;
  CCHANGED: Boolean;

implementation

{$R *.dfm}

Uses KruptarMain, KruptarProjectUnit, HexUnit, HexTexU, ProjItems, UniConv,
     GotoForm;

Procedure THexEditForm.DrawText;
Var P: Pointer; ROM: ^TROMData; TD, TW: DWord; Br: Boolean;
Function GetSymbol(dpj: dword): string;
var uui: dword;
begin
 tw := ROM^[Dpj] * $1000000;
 tw := tw + ROM^[Dpj + 1] * $10000;
 tw := tw + ROM^[Dpj + 2] * $100;
 tw := tw + ROM^[Dpj + 3];
 for Uui := 3 downto 0 do
 begin
  If Br then
  Result := PGroup(P)^.Table1[0, Tw] Else
  Result := PGroup(P)^.Table2[0, Tw]; 
  if PGroup(P)^.tadlen <> uui + 1 then result := '';
  if Result = '' then Tw := Tw div $100 else Break;
  If uui = 0 then break;
 end;
 td := uui;
end;
Var
 S, SS: String; K, J, D, RS: DWord; WS: WideString; undoo: boolean; U: TTreeNode;
 HexTextFormMemoReadOnly: Boolean;
Label aExit;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then
 begin
  aExit:
  HexTextForm.Caption := '00000000';
  HexTextForm.Memo.Clear;
  HexTextForm.Memo.ReadOnly := True;
  Exit;
 end;
 If U.Level = 1 then U := U.Parent;
 P := U.Data;
 If Pages.ActivePageIndex = 0 then With HexGridO do
 begin
  D := foPosition;
  RS := KruptarForm.KruptarProject.ROMSize;
  ROM := Addr(KruptarForm.KruptarProject.ROM^[0]);
  HexTextFormMemoReadOnly := True;
  Br := True;
 end Else With HexGridE do
 begin
  D :=  fPosition;
  RS := KruptarForm.KruptarProject.ROMESize;
  ROM := Addr(KruptarForm.KruptarProject.ROME^[0]);
  HexTextFormMemoReadOnly := False;
  Br := False;
 end;
 If D >= RS then Goto aExit;
 S := '';
 For K := 0 to fRowCount - 1 do
 begin
  J := 0;
  UndoO := False;
  Repeat
   Ss := GetSymbol(K * DWord(fColCount) + d + j);
   If PGroup(P)^.UndSym then UndoO := True;
   If (Ss = '') then
   begin
    Ss := '[#' + IntTohex(ROM^[K * DWord(fColCount) + d + j], 2) + ']';
    PGroup(P)^.TadLen := 1;
    If Br then
    PGroup(P)^.Table1[0, ROM^[K * DWord(fColCount) + d + j]] := Ss Else
    PGroup(P)^.Table2[0, ROM^[K * DWord(fColCount) + d + j]] := Ss;
   end;
   If PGroup(P)^.EndSym and Undoo and (PGroup(P)^.EundoStr <> '') then
   begin
    S := S + PGroup(P)^.EundoStr;
    PGroup(P)^.EndSym := False;
   end else S := S + Ss;
   Inc(j, Td + 1);
  until j >= DWord(fColCount);
  S := S + #13#10;
 end;
 If PGroup(P)^.xKodir = kdANSI then WS := S Else WS := UniText(S);
 HexTextForm.Memo.Lines.Text := WS;
 HexTextForm.Caption := IntToHex(D, 8);
 HexTextForm.Memo.ReadOnly := HexTextFormMemoReadOnly;
end;

Procedure THexEditForm.RefreshHex;
Var X, Y: Integer; W, B: DWord;
begin
 If not KruptarForm.Created then
 begin
  For y := 0 to fRowCount - 1 do
   For x := 0 to fColCount - 1 do
   begin
    HexGridO.Cells[x, y] := '';
    HexGridE.Cells[x, y] := '';
   end;
 end Else WIth KruptarForm.KruptarProject do
 For y := 0 to fRowCount - 1 do
  For x := 0 to fColCount - 1 do
  begin
   b := DWord(y * fColCount + x);
   W := foPosition + b;
   If W < ROMSize then
    HexGridO.Cells[x, y] := IntToHex(ROM^[W], 2) Else
    HexGridO.Cells[x, y] := '';
   W := fPosition + b;
   If W < ROMESize then
    HexGridE.Cells[x, y] := IntToHex(ROME^[W], 2) Else
    HexGridE.Cells[x, y] := '';
  end;
 HexTextForm.Memo.ReadOnly := False;
 If not Need then DrawText;
end;

Function THexEditForm.GetCc: Integer;
begin
 Result := fColCount;
end;

Function THexEditForm.GetRc: Integer;
begin
 Result := fRowCount;
end;

Procedure THexEditForm.SetCc(X: Integer);
begin
 fColCount := X;
 HexGridO.ColCount := X;
 HexGridE.ColCount := X;
end;

Procedure THexEditForm.SetRc(X: Integer);
begin
 fRowCount := X;
 HexGridO.RowCount := X;
 HexGridE.RowCount := X;
end;

procedure THexEditForm.FormShow(Sender: TObject);
begin
 ColCount := 16;
 RowCount := 16;
 Position := 0;
 oPosition := 0;
end;

procedure THexEditForm.FormResize(Sender: TObject);
Var W, H: Integer;
begin
 W := ((ClientWidth - 17) div 16) * 16;
 H := ((ClientHeight - 53) div 16) * 16;
 fColCount := W div 16;
 fRowCount := H div 16;
 If fColCount < 8 then fColCount := 8;
 If fRowCount < 1 then fRowCount := 1;
 W := fColCount * 16;
 H := fRowCount * 16;
 HexGridE.Width := W;
 HexGridO.Width := W;
 HexGridE.Height := H;
 HexGridO.Height := H;
 ColCount := fColCount;
 RowCount := fRowCount;
 ClientWidth := W + 17;
 ClientHeight := H + 53;
 Pages.Width := ClientWidth;
 Pages.Height := ClientHeight - 20;
 RefreshHex;
end;

Function THexEditForm.GetoPos: Dword;
begin
 Result := foPosition;
end;

Procedure THexEditForm.SetoPos(X: DWord);
begin
 Original.Caption := 'InROM - ' + IntToHex(X, 8);
 If foPosition = X then Exit;
 foPosition := X;
 RefreshHex;
end;

Function THexEditForm.GetPos: Dword;
begin
 Result := fPosition;
end;

Procedure THexEditForm.SetPos(X: DWord);
begin
 EditMode.Caption := 'OutROM - ' + IntToHex(X, 8);
 If fPosition = X then Exit;
 fPosition := X;
 RefreshHex;
end;

procedure THexEditForm.HexGridEKeyPress(Sender: TObject; var Key: Char);
Var B: DWord;
begin
 If not KruptarForm.Created then
 begin
  Key := #0;
  Exit;
 end;
 Key := UpCase(Key);
 If Key in ['0'..'9', 'A'..'F'] then With HexGridE do
 begin
  Hint := Hint + Key;
  If Length(Hint) = 2 then
  begin
   B := fPosition + DWord(Row * fColCount + Col);
   If B < KruptarForm.KruptarProject.ROMEsize then
   begin
    Cells[Col, Row] := Hint;
    KruptarForm.KruptarProject.ROME^[B] := HexVal(Hint);
   end;
   Hint := '';
   If Col < fColCount - 1 then Col := Col + 1 Else
   begin
    Col := 0;
    If Row < fRowCount - 1 then Row := Row + 1 Else
    begin
     B := fPosition + DWord(fColCount) * DWord(fRowCount);
     If B < KruptarForm.KruptarProject.ROMESize then
     begin
      Row := 0;
      Position := B;
     end;
    end;
   end;
  end;
 end;
end;

procedure THexEditForm.TimerTimer(Sender: TObject);
Var R, C, P, S: DWord;
begin
 If Pages.ActivePageIndex = 0 then
 begin
  C := HexGridO.Col;
  R := HexGridO.Row;
  P := foPosition;
  S := KruptarForm.KruptarProject.ROMSize;
 end Else
 begin
  C := HexGridE.Col;
  R := HexGridE.Row;
  P := fPosition;
  S := KruptarForm.KruptarProject.ROMESize;
 end;
 StatusBar.Panels[0].Text := IntToHex(P + R * DWord(fColCount) + C, 8);
 StatusBar.Panels[1].Text := IntToHex(S, 8);
 StatusBar.Panels[2].Text := IntToStr(fColCount) + 'x' + IntToStr(fRowCount);
 If CCHANGED then
 StatusBar.Panels[3].Text := '*' Else
 StatusBar.Panels[3].Text := '';
end;

procedure THexEditForm.HexGridEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var B: LongInt;
begin
 If not KruptarForm.Created then
 begin
  Key := 0;
  Exit;
 end;
 With HexGridE do
 Case Key of
  VK_LEFT: If ssShift in Shift then
  begin
   Key := 0;
   B := fPosition - 1;
   If B < 0 then B := 0;
   Position := B;
   Exit;
  end Else If Col = 0 then
  begin
   Key := 0;
   If Row > 0 then
   begin
    Col := fColCount - 1;
    Row := Row - 1;
   end Else
   begin
    B := LongInt(fPosition) - fColCount;
    If B < 0 then
    begin
     Col := fColCount + B;
     B := 0;
    end Else Col := fColCount - 1;
    Position := B;
   end;
  end;
  VK_UP: If Row = 0 then
  begin
   Key := 0;
   B := LongInt(fPosition) - fColCount;
   If B < 0 then
    B := 0;
   Position := B;
  end;
  VK_PRIOR:
  begin
   Key := 0;
   B := LongInt(fPosition) - fColCount * fRowCount;
   If B < 0 then B := 0;
   Position := B;
  end;
  VK_RIGHT: If ssShift in Shift then
  begin
   Key := 0;
   B := fPosition + 1;
   If DWord(B) < KruptarForm.KruptarProject.ROMESize then Position := B;
   Exit;
  end Else If Col = fColCount - 1 then
  begin
   Key := 0;
   If Row < fRowCount - 1 then
   begin
    Col := 0;
    Row := Row + 1;
   end Else
   begin
    B := LongInt(fPosition) + fColCount;
    If DWord(B + fColCount * fRowCount) >= KruptarForm.KruptarProject.ROMESize then
    begin
     B := LongInt(KruptarForm.KruptarProject.ROMESize) - fColCount * fRowCount;
     If B < 0 then B := 0;
    end Else Col := 0;
    Position := B;
   end;
  end;
  VK_DOWN: If Row = fRowCount - 1 then
  begin
   Key := 0;
   B := LongInt(fPosition) + fColCount;
   If DWord(B + fColCount * fRowCount) >= KruptarForm.KruptarProject.ROMESize then
   begin
    B := LongInt(KruptarForm.KruptarProject.ROMESize) - fColCount * fRowCount;
    If B < 0 then B := 0;
   end;
   Position := B;
  end;
  VK_NEXT:
  begin
   Key := 0;
   B := LongInt(fPosition) + fColCount * fRowCount;
   If DWord(B) >= KruptarForm.KruptarProject.ROMESize then
   begin
    B := LongInt(KruptarForm.KruptarProject.ROMESize) - (fColCount * fRowCount);
    If B < 0 then B := 0;
   end;
   Position := B;
  end;
 end;
end;

procedure THexEditForm.HexGridOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var B: LongInt;
begin
 If not KruptarForm.Created then
 begin
  Key := 0;
  Exit;
 end;
 With HexGridO do
 Case Key of
  VK_LEFT: If ssShift in Shift then
  begin
   Key := 0;
   B := foPosition - 1;
   If B < 0 then B := 0;
   oPosition := B;
   Exit;
  end Else If Col = 0 then
  begin
   Key := 0;
   If Row > 0 then
   begin
    Col := fColCount - 1;
    Row := Row - 1;
   end Else
   begin
    B := LongInt(foPosition) - fColCount;
    If B < 0 then
    begin
     Col := fColCount + B;
     B := 0;
    end Else Col := fColCount - 1;
    oPosition := B;
   end;
  end;
  VK_UP: If Row = 0 then
  begin
   Key := 0;
   B := LongInt(foPosition) - fColCount;
   If B < 0 then
    B := 0;
   oPosition := B;
  end;
  VK_PRIOR:
  begin
   Key := 0;
   B := LongInt(foPosition) - fColCount * fRowCount;
   If B < 0 then B := 0;
   oPosition := B;
  end;
  VK_RIGHT: If ssShift in Shift then
  begin
   Key := 0;
   B := foPosition + 1;
   If DWord(B) < KruptarForm.KruptarProject.ROMSize then oPosition := B;
   Exit;
  end Else If Col = fColCount - 1 then
  begin
   Key := 0;  
   If Row < fRowCount - 1 then
   begin
    Col := 0;
    Row := Row + 1;
   end Else
   begin
    B := LongInt(foPosition) + fColCount;
    If DWord(B + fColCount * fRowCount) >= KruptarForm.KruptarProject.ROMSize then
    begin
     B := LongInt(KruptarForm.KruptarProject.ROMSize) - fColCount * fRowCount;
     If B < 0 then B := 0;
    end Else Col := 0;
    oPosition := B;
   end;
  end;
  VK_DOWN: If Row = fRowCount - 1 then
  begin
   Key := 0;
   B := LongInt(foPosition) + fColCount;
   If DWord(B + fColCount * fRowCount) >= KruptarForm.KruptarProject.ROMSize then
   begin
    B := LongInt(KruptarForm.KruptarProject.ROMSize) - fColCount * fRowCount;
    If B < 0 then B := 0;
   end;
   oPosition := B;
  end;
  VK_NEXT:
  begin
   Key := 0;
   B := LongInt(foPosition) + fColCount * fRowCount;
   If DWord(B) >= KruptarForm.KruptarProject.ROMSize then
   begin
    B := LongInt(KruptarForm.KruptarProject.ROMSize) - (fColCount * fRowCount);
    If B < 0 then B := 0;
   end;
   oPosition := B;
  end;
 end;
end;

procedure THexEditForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 KruptarForm.HexEditem.Checked := False;
 HexTextForm.Visible := False;
 Visible := False;
 If not KruptarForm.SavePos.Checked then KruptarForm.InitWindows;
end;

procedure THexEditForm.PagesChange(Sender: TObject);
begin
 RefreshHex;
end;

procedure THexEditForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
end;

procedure THexEditForm.GotoNClick(Sender: TObject);
begin
 Case Pages.ActivePageIndex of
  0: OkBottomDlg.Edit.Text := 'h' + IntToHex(foPosition, 8);
  1: OkBottomDlg.Edit.Text := 'h' + IntToHex(fPosition, 8);
 end;
 OKBottomDlg.ShowModal;
 If OKBottomDlg.ModalResult = mrOk then Case Pages.ActivePageIndex of
  0: oPosition := GetLDW(OkBottomDlg.Edit.Text);
  1:  Position := GetLDW(OkBottomDlg.Edit.Text);
 end;
end;

end.
