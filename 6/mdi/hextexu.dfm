object HexTextForm: THexTextForm
  Left = 430
  Top = 531
  Width = 281
  Height = 140
  BorderStyle = bsSizeToolWin
  Caption = '00000000'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Memo: TUnicodeMemo
    Left = 0
    Top = 0
    Width = 273
    Height = 113
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 0
    OnChange = MemoChange
    OnKeyDown = MemoKeyDown
    OnKeyPress = MemoKeyPress
  end
end
