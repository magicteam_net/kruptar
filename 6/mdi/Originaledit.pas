unit Originaledit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, UnicodeEdit, UnicodeMemo, ExtCtrls, BufForm,
  Menus, StdActns, ActnList;

type
  TOriginalTextEditForm = class(TForm)
    Memo: TUnicodeMemo;
    StatusBar: TStatusBar;
    Timer: TTimer;
    ActionList: TActionList;
    EditCopy: TEditCopy;
    EditSelectAll: TEditSelectAll;
    PopupMenu: TPopupMenu;
    CopyI: TMenuItem;
    N1: TMenuItem;
    SelectAll: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure MemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TimerTimer(Sender: TObject);
    procedure StatusBarClick(Sender: TObject);
  private
    { Private declarations }
  public
    Procedure SetText;
  end;

var
  OriginalTextEditForm: TOriginalTextEditForm;
  OposText: String;

implementation

uses KruptarMain, ProjItems, StringListUnit, KruptarProjectUnit, UniConv;

{$R *.dfm}

procedure TOriginalTextEditForm.FormShow(Sender: TObject);
begin
 KruptarForm.OrigTextItem.Checked := True;
end;

Procedure TOriginalTextEditForm.SetText;
Var Pp: PPoss; U: TTreeNode; I, J: Integer; S, Ss: WideString; Ind: Integer;
    N: PtTable;
Label Kuka;
begin
 StatusBar.Panels[0].Text := '';
 U := ProjectItemsForm.TreeView.Selected;
 Ind := StringsListForm.StringsListBox.ItemIndex;
 if (Ind < 0) or (U = NIL) or (U.Data = NIL) then
 begin
  Memo.Lines.Clear;
  Exit;
 end;
 If U.Level = 1 then U := U.Parent;
 pp := PGroup(U.Data)^.Posso.Root;
 j := 0;
 ss := '';
 With PGroup(U.Data)^ do While pp <> nil do
 begin
  n := Pp^.PtrTable^.Root;
  while n <> nil do
  begin
   if j = Ind then
   begin
    OposText := IntToHex(N^.TPtr, 8);
    StatusBar.Panels[0].Text := OposText;    
    Case xKodir of
     kdANSI, kdUNIC: S := N^.text;
     kdSJIS: S := UniText(N^.Text);
    end;
    i := 0; j := 1;
    if s <> '' then
    repeat
     repeat
      inc(i);
     until (i = Length(s)) or (
     (s[i] = '~') or
     (s[i] = '^') or
     (s[i] = '*') or
     (s[i] = '/') or
     (s[i] = '\'));
     Ss := Ss + Copy(S, j, i - j + 1) + #13#10;
     j := i + 1;
    until i = Length(s);
    Goto Kuka;
   end;
   Inc(j);
   N := N^.next;
  end;
  Pp := Pp^.next;
 end;
Kuka:
 Memo.Lines.SetText(PWideChar(Ss));
end;

procedure TOriginalTextEditForm.MemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
end;

procedure TOriginalTextEditForm.TimerTimer(Sender: TObject);
begin
 StatusBar.Panels[1].Text := 'x: ' +
 IntToStr(Memo.CaretPos.X) + '/' +
 IntToStr(Length(Memo.Lines.Strings[Memo.CaretPos.Y]));
 StatusBar.Panels[2].Text := 'y: ' +
 IntToStr(Memo.CaretPos.Y) + '/' +
 IntToStr(Memo.Lines.Count - 1 * Byte(Memo.Lines.Count > 0));
end;

procedure TOriginalTextEditForm.StatusBarClick(Sender: TObject);
begin
 Form1.Edit1.Text := StatusBar.Panels[0].Text;
 Form1.Edit1.SelectAll;
 Form1.Edit1.CopyToClipboard;
end;

end.
