unit KruptarProjectUnit;

interface

Uses Windows, HexUnit, SysUtils, Dialogs, Classes, UnicodeLists;

type
 TPMetT = (tmNormal, tmL1T, tmL1Te, tmL1TL1T, tmC2S1eSne, tmHat, tmBkp, tmP2P{, tmDLL}, tmNone);

Const
 MTCount = Byte(tmNone);
 kdANSI = 0;
 kdSJIS = 1;
 kdUNIC = 2;
 CEq = '=';
 CHex = 'h';
 prKruptarProject  = '[KruptarProject]';
 prInputROM        = '[InputROM]';
 prOutputROM       = '[OutputROM]';
 prEmulator        = '[Emulator]';
 prDefineGroup     = '[DefineGroup]';
 prAlign           = '[Align]';
 prPointerUse      = '[PointerUse]';
 prPointerSize     = '[PointerSize]';
 prInterval        = '[Interval]';
 prDivision        = '[Division]';
 prType            = '[Type]';
 prMotorola        = '[Motorola]';
 prSigned          = '[Signed]';
 prShl1            = '[Shl1]';
 prMethod          = '[Method]';
 prDifference      = '[Difference]';
 prCharSet         = '[CharSet]';
 prStringLength    = '[StringLength]';
 prTablesStart     = '[TablesStart]';
 prTablesEnd       = '[TablesEnd]';
 prBlocksStart     = '[BlocksStart]';
 prBlocksEnd       = '[BlocksEnd]';
 prDefineTextBlock = '[DefineTextBlock]';
 prUsedTableSet    = '[UsedTableSet]';
 prPtrTableStart   = '[PtrTableStart]';
 prBlockStart      = '[BlockStart]';
 prBlockLength     = '[BlockLength]';
 prStringPointer   = '[StringPointer]';
 prStringsCount    = '[StringsCount]';
 prStringStart     = '[StringStart]';
 prStringEnd       = '[StringEnd]';
 prTextBlockEnd    = '[TextBlockEnd]';
 prGroupEnd        = '[GroupEnd]';
 prProjectEnd      = '[ProjectEnd]';
 prTrue            = 'TRUE';
 prFalse           = 'FALSE';
 prANSI            = 'ANSI';
 prSJIS            = 'SHIFT-JIS';
 prUNIC            = 'UNICODE';

type
 TROMData = Array[0..2000000] of byte;
 TLongs = Record
  a, b: DWord;
 end;
 TLData = array[0..8191] of TLongs;

type
 pctable = ^tctable;
 tctable = record
  s: WideString;
  tl: byte;
  endstr: boolean;
  undo: boolean;
  eundo: WideString;
  next: pctable;
  Case Byte of
   0: (p: dword);
   1: (a, b, c, d: Byte);
 end;
 pctables = ^tctables;
 tctables = record
  tab, cur: pctable;
  count: integer;
  cl: byte;
  fname: string;
  next: pctables;
 end;
 TcTableso = object
  rootO, curO: pctables;
  rootE, curE: pctables;
  Owner: Pointer;
  Count: Integer;
  Constructor Init;
  Function GetNameO(Tn: Integer): String;
  Function GetNameE(Tn: Integer): String;
  Procedure Add(f1, f2: WideString);
  Procedure ChangeTable(TabN: Integer; f1, f2: String);
  Procedure CheckTables;
  Function FindTables(f1, f2: string): Integer;
  Destructor Done;
 end;
 pttable = ^tttable;
 tttable = record
  tptr: dword;
  same: dword;
  text: WideString;
  sinl: word;
  next: pttable;
 end;
 pptrtable = ^tptrtable;
 Tptrtable = object
  root, cur: pttable;
  count, bcnt: integer;
  Name: String;
  pos: dword;
  constructor init;
  procedure Add(tp: dword; s: WideString);
  procedure LoadAdd(Tp: DWord; s: WideString);
  destructor clear;
 end;
 pposs = ^tposs;
 tposs = record
  ptrtable: pptrtable;
  GutGut: PPoss;
  tabn: integer;
  next: pposs;
 end;
 tposso = object
  root, cur: pposs;
  Owner: Pointer;
  count: integer;
  constructor init;
  Procedure Reload;
  procedure add(tn: integer; p, cnt: dword);
  procedure adde(tn: integer; p: dword; pp: pptrtable);
  destructor clear;
 end;
 tpossor = object
  root, cur: pposs;
  Owner: Pointer;
  count: integer;
  constructor init;
  procedure addposso;
  destructor clear;
 end;
{
Type
 PData = ^TData;
 TData = Array[Word] of Byte;
 TVarType = (vtNone, vtByte, vtWord, vtInt, vtDword);
 PVariable = ^TVariable;
 TVariable = Record
  Data: PData;
  Size: DWord;
  VarType: TVarType;
  Next: PVariable;
 end;
 PVariables = ^TVariables;
 TVariables = Object
  Root, Cur: PVariable;
  Count: Integer;
  Constructor Init;
  Function Add(Sz: DWord; VT: TVarType): PVariable;
  Destructor Done;
 end;
 TReadString = Object
  Code: PData;
  CodeSize: DWord;
  Variables: PVariables;
  Constructor Init;
  Procedure Read(P: PCTable);
  Destructor Done;
 end;
 TStringLength = Object
  Code: PData;
  CodeSize: DWord;
  Variables: PVariables;
  Constructor Init;
  Function Length(P: PCTable): Integer;
  Destructor Done;
 end;
 TWriteString = Object
  Code: PData;
  CodeSize: DWord;
  Variables: PVariables;
  Constructor Init;
  Procedure Write(P: PCTable);
  Destructor Done;
 end;
 TCodeData = Object
  Owner: Pointer;
  ReadString: ^TReadString;
  StringLength: ^TStringLength;
  WriteString: ^TWriteString;
  Constructor Init(Own: Pointer);
  Procedure Compile(FileName: String);
  Destructor Done;
 end;
         }
Type
PGroup = ^TGroup;
TGroup = Object
 Owner: Pointer;
 GrName: String;
 FromFile: Boolean;
 CurP: PtTable;
 CurP2: PtTable;
 OTables: TCTablesO;
 EmptyS: ^TLData;
 EmptyC: Integer;
 Oofs: DWord;
 Posso: TPosso;
 PossoR: TPossoR;
 Tadlen: Byte;
 Tt: Boolean;
 EndSym: Boolean;
 UndSym: Boolean;
 eUndoStr: WideString;
 Next: PGroup;
 xKodir: Integer;
 xPtrSize: Byte;
 xKrat: Byte;
 xMethod: TPMetT;
 xSigned: Boolean;
 xMotorola: Boolean;
 xFirstPtr: LongInt;
 xInterval: Byte;
 xNoPtr: Boolean;
 xShl1: Boolean;
 xStrLen: Word;
 xPntp: boolean;
 xAutoStart: Boolean;
 xAlign: Integer;
 {xCodeData: TCodeData; }
 Constructor Create(Own: Pointer);
 Constructor Add(Own: Pointer);
 Destructor  Clear;
//procs
 Procedure   LoadTab(Tn: Integer; Ff: String);
 Function    GetTitem(TabN, x: DWord): WideString;
 Procedure   SetTitem(TabN, x: DWord; Value: WideString);
 Function    GetT2item(TabN, x: DWord): WideString;
 Procedure   SetT2item(TabN, x: DWord; Value: WideString);
 Function    GetATitem(x: WideString; tabn: integer): dword;
 Function    GetUTitem(x: WideString; tabn: integer): dword;
 Function    GetA1Titem(x: WideString; tabn: integer): dword;
 Function    GetMLen(tabn: integer): integer;
 Procedure   GetTableNames(tabn: Integer; Var n1, n2: string);
//properties
 Property    Table1[TabN, X: DWord]: WideString Read GetTitem Write SetTitem;
 Property    Table2[TabN, X: DWord]: WideString Read GetT2item Write SetT2item;
 Property    AntiTable[X: WideString; tabn: Integer]: DWord read GetATitem;
 Property    AntiUTable[X: WideString; tabn: Integer]: DWord read GetUTitem;
 Property    AntiTable1[X: WideString; tabn: Integer]: DWord read GetA1Titem;
 Property    ChMaxLen[TabN: Integer]: Integer read GetMLen;
end;

PGroups = ^TGroups;
TGroups = Object
 Root, Cur: PGroup;
 Count: Integer;
 Constructor Init;
 Destructor Done;
end;

Type
 TKruptarProject = Object
  fNamed: Boolean;
  fSaved: Boolean;
  InputROM: String;
  OutputROM: String;
  ProjectFile: String;
  Emulator: String;
  Groups: TGroups;
  ROM: ^TRomData;
  ROME: ^TRomData;
  ROMSize: DWord;
  ROMESize: DWord;
  Constructor Init(i, o: string);
  Procedure Save;
  Procedure SaveUnic;
  Procedure Open(FileName: String);
  Procedure Create;
  Procedure LoadROM;
  Procedure FreeROM;
  Destructor Done;
  Procedure Redir(Var S: WideString);
 end;

implementation

Uses KruptarMain, StringListUnit, ProjItems, CreateGroupUnit, NewProject,
     Recalc, WStrings;

Constructor TcTableso.Init;
begin
 rootO := nil;
 curO := nil;
 rootE := nil;
 curE := nil;
 Owner := nil;
 Count := 0;
end;

Procedure TcTableso.ChangeTable(TabN: Integer; f1, f2: String);
var n: pctables; I: Integer; ij: pctable;
begin
 If (TabN >= Count) or (TabN < 0) then Exit;
 n := RootO;
 i := 0;
 While i <> TabN do
 begin
  n := n^.next;
  Inc(i);
 end;
 While n^.tab <> nil do
 begin
  n^.tab^.s := '';
  n^.tab^.eundo := '';
  ij := n^.tab^.next;
  If n^.cur = n^.tab then n^.cur := NIL;
  Dispose(n^.tab);
  n^.cur := NIL;
  n^.tab := ij;
 end;
 n^.fname := f1;
 PGroup(Owner)^.tt := false; PGroup(Owner)^.LoadTab(TabN, f1);
 n := RootE;
 i := 0;
 While i <> TabN do
 begin
  n := n^.next;
  Inc(i);
 end;
 While n^.tab <> nil do
 begin
  n^.tab^.s := '';
  n^.tab^.eundo := '';
  ij := n^.tab^.next;
  Dispose(n^.tab);
  n^.tab := ij;
 end;
 n^.fname := f2;
 PGroup(Owner)^.tt := true; PGroup(Owner)^.LoadTab(TabN, f2);
 For i := 0 to 255 do
 begin
  PGroup(Owner)^.endsym := false;
  PGroup(Owner)^.undsym := false;
  PGroup(Owner)^.eundostr := '';
  PGroup(Owner)^.Tadlen := 1;
  PGroup(Owner)^.Table2[count - 1, i] := '[#' + IntToHex(i, 2) + ']';
 end;
end;

Procedure TcTableso.Add(f1, f2: WideString);
var n, rr: pctables; i: byte;
begin
 inc(count);
 new(n);
 n^.tab := nil;
 n^.cur := nil;
 n^.fname := f1;
 n^.count := 0;
 n^.cl := 0;
 n^.next := nil;
 rr := curO;
 curO := n;
 if rr <> nil then rr^.next := curO;
 if rootO = nil then rootO := n;
 PGroup(Owner)^.tt := false; PGroup(Owner)^.LoadTab(count - 1, f1);
 new(n);
 n^.tab := nil;
 n^.cur := nil;
 n^.fname := f2;
 n^.count := 0;
 n^.cl := 0;
 n^.next := nil;
 rr := curE;
 curE := n;
 if rr <> nil then rr^.next := curE;
 if rootE = nil then rootE := n;
 PGroup(Owner)^.tt := true;
 PGroup(Owner)^.LoadTab(count - 1, f2);
 For i := 0 to 255 do
 begin
  PGroup(Owner)^.endsym := false;
  PGroup(Owner)^.undsym := false;
  PGroup(Owner)^.eundostr := '';
  PGroup(Owner)^.Tadlen := 1;  
  PGroup(Owner)^.Table2[count - 1, i] := '[#' + IntToHex(i, 2) + ']';
 end;
end;

Procedure TcTableso.CheckTables;
var n1: pctable; nos: boolean;
begin
 n1 := CurO^.tab;
 nos := true;
 while n1 <> nil do
 begin
  if n1^.endstr then
  begin
   nos := false;
   break;
  end;
  n1 := n1^.next;
 end;
 If nos then
 begin
  PGroup(Owner)^.tadlen := 1;
  PGroup(Owner)^.table1[Count - 1, 0] := '/';
 end;
 n1 := CurE^.tab;
 nos := true;
 while n1 <> nil do
 begin
  if n1^.endstr then
  begin
   nos := false;
   break;
  end;
  n1 := n1^.next;
 end;
 If nos then
 begin
  PGroup(Owner)^.tadlen := 1;
  PGroup(Owner)^.table2[Count - 1, 0] := '/';
 end;
end;

Function TcTableso.GetNameO(Tn: Integer): String;
var n: PCTables; I: Integer;
begin
 I := 0;
 N := RootO;
 While I <> Tn do
 begin
  N := N^.Next;
  Inc(I);
 end;
 Result := N^.FName;
end;

Function TcTableso.GetNameE(Tn: Integer): String;
var n: PCTables; I: Integer;
begin
 I := 0;
 N := RootE;
 While I <> Tn do
 begin
  N := N^.Next;
  Inc(I);
 end;
 Result := N^.FName;
end;

Function TcTableso.FindTables(f1, f2: string): Integer;
var n1, n2: pctables; i: Integer;
begin
 n1 := RootO;
 n2 := RootE;
 Result := -1;
 i := 0;
 While (n1 <> nil) or (n2 <> nil) do
 begin
  If (n1^.fname = f1) and (n2^.fname = f2) then
  begin
   Result := i;
   Exit;
  end;
  n1 := n1^.next;
  n2 := n2^.next;
  Inc(i);
 end;
end;

Destructor TcTableso.Done;
var n: pctables; t, tn: pctable;
begin
 while rootO <> nil do
 begin
  n := rootO;
  rootO := n^.next;
  t := n^.tab;
  while t <> nil do
  begin
   tn := t;
   t := tn^.next;
   dispose(tn);
  end;
  dispose(n);
 end;
 while rootE <> nil do
 begin
  n := rootE;
  rootE := n^.next;
  t := n^.tab;
  while t <> nil do
  begin
   tn := t;
   t := tn^.next;
   dispose(tn);
  end;
  dispose(n);
 end;
 Count := 0;
end;

Constructor TPtrTable.Init;
begin
 root := nil;
 cur := nil;
 bcnt := 0;
 count := 0;
 pos := 0;
 Name := '';
end;

Procedure TPtrTable.Add(tp: DWord; s: WideString);
var n, rr: pttable;
begin
 new(n);
 n^.tptr := tp;
 n^.text := s;
 n^.same := 0;
 n^.sinl := 0;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
end;

Procedure TPtrTable.LoadAdd(tp: DWord; s: WideString);
var n, rr: pttable;
begin
 new(n);
 n^.tptr := tp;
 n^.text := s;
 n^.same := 0;
 n^.sinl := 0;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
end;

Destructor TPtrTable.Clear;
var n: pttable;
begin
 while root <> nil do
 begin
  n := root;
  n^.tptr := 0;
  n^.same := 0;
  n^.text := '';
  n^.sinl := 0;
  root := n^.next;
  dispose(n);
 end;
 root := nil;
 cur := nil;
 bcnt := 0;
 count := 0;
 pos := 0;
 Name := '';
end;

Constructor TPosso.Init;
begin
 root := nil;
 cur := nil;
 Owner := nil;
 count := 0;
end;

Var NANA: PPoss;

Procedure TPosso.Reload;
Var Prev, N, CCur: PPoss; Name: String; Pos, xCnt: Dword;
begin
 N := Root;
 Prev := NIL;
 While N <> NIL do
 begin
  Name := N^.ptrtable^.Name;
  Pos := N^.ptrtable^.pos;
  xCnt := N^.ptrtable^.bcnt;
  Dispose(N^.ptrtable, clear);
  CCur := Cur;
  Add(n^.tabn, Pos, xCnt);
  Dec(Count);
  NANA^.next := N^.next;
  NANA^.GutGut := N^.GutGut;
  NANA^.tabn := N^.tabn;
  NANA^.ptrtable^.Name := Name;
  If Root = N then Root := NANA;
  If CCur = N then CCur := NANA;
  If Prev <> NIL then Prev^.Next := NANA;
  Dispose(N);
  Cur := CCur;
  Cur^.next := NIL;
  Prev := NANA;
  N := NANA^.next;
 end;
end;

Procedure TPosso.Add(tn: Integer; p, cnt: DWord);
type dwd = record a, b, c, d: byte end;
type wd = record a, b: byte end;
var
 rr: pposs; n: PPoss absolute NANA; jb, i, j, d, DU: dword; zrom, prom, grom: ^tromdata;
 s, ss: WideString; undoo: boolean;
 psz: dword; g: dwd; sinll: word; fst, ii, tw, td, pntt: dword; bki: word;
label eloop;
Function GetSymbol(Pos: DWord): WideString;
Var C: ^DWord; X: DWord; I: Integer;
begin
 C := Addr(KruptarForm.KruptarProject.ROM^[Pos]);
 With DWD(C^) do
 begin
  DWD(X).A := D;
  DWD(X).B := C;
  DWD(X).C := B;
  DWD(X).D := A;
 end;
 With PGroup(Owner)^ do For I := 4 downto 1 do
 begin
  Result := Table1[TN, X];
  If TadLen <> I then Result := '';
  If Result = '' then X := X shr 8 Else
  begin
   TD := I;
   While Integer(TD) mod xAlign > 0 do Inc(TD);
   Dec(TD);
   Break;
  end;
 end;
{ tw :=      KruptarForm.KruptarProject.ROM^[dpj] shl 24;
 tw := tw + KruptarForm.KruptarProject.ROM^[dpj + 1] shl 16;
 tw := tw + KruptarForm.KruptarProject.ROM^[dpj + 2] shl 8;
 tw := tw + KruptarForm.KruptarProject.ROM^[dpj + 3];
 TD := 0;
 for uui := 3 downto 0 do
 begin
  result := PGroup(Owner)^.table1[tn,tw];
  if PGroup(Owner)^.tadlen <> uui + 1 then result := '';
  if result = '' then tw := tw shr 8 else break;
  Inc(TD);
  if uui = 0 then break;
 end;
 While Integer(TD) mod PGroup(Owner)^.xAlign > 0 do Inc(TD);
 Dec(TD);}
end;
Label endst;
begin
 sinll := 0;
 new(n);
 new(n^.ptrtable, init);
 n^.ptrtable^.pos := p;
 n^.ptrtable^.bcnt := cnt;
 n^.tabn := tn;
 n^.next := nil;
 RecalcCount := n^.ptrtable^.bcnt;
 RCount := 0;
 RecalcForm.Show;
 CC := False;
 RecalcForm.Caption := '�������� ������ �� ROM''�';
 RecalcForm.Gauge.MaxValue := RecalcCount;
 prom := Addr(KruptarForm.KruptarProject.ROM^[p]);
 d := p;
 if PGroup(Owner)^.xPntp then pntt := cnt else pntt := 1;
 If not PGroup(Owner)^.xNoPtr then for i := 0 to cnt - 1 do
 begin
  s := ''; td := 0;
  With PGroup(Owner)^ do
  If not xPntp then fst := I * (xPtrSize + xInterval) Else fst := i;
  Case PGroup(Owner)^.xPtrSize of
   2: if PGroup(Owner)^.xMotorola then
   begin
    dwd(d).d := 0;
    dwd(d).c := 0;
    dwd(d).b := prom^[fst];
    dwd(d).a := prom^[fst + 1 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := 0;
    dwd(d).d := 0;
   end;
   3: if PGroup(Owner)^.xMotorola then
   begin
    dwd(d).d := 0;
    dwd(d).c := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).a := prom^[fst + 2 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := prom^[fst + 2 * pntt];
    dwd(d).d := 0;
   end;
   4: if PGroup(Owner)^.xMotorola then
   begin
    dwd(d).d := prom^[fst];
    dwd(d).c := prom^[fst + 1 * pntt];
    dwd(d).b := prom^[fst + 2 * pntt];
    dwd(d).a := prom^[fst + 3 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := prom^[fst + 2 * pntt];
    dwd(d).d := prom^[fst + 3 * pntt];
   end;
  end;
  If PGroup(Owner)^.xShl1 then D := D shl 1;
  if PGroup(Owner)^.xSigned and (PGroup(Owner)^.xPtrSize in [2, 4]) then
  begin
   With PGroup(Owner)^ do
   Case xPtrSize of
    2:
    begin
     LongInt(D) := SmallInt(D);
     Inc(D, P + I * (2 + xInterval));
    end;
    4: Inc(D, P + I * (4 + xInterval));
   end;
  end Else
  begin
   If PGroup(Owner)^.xAutoStart then PGroup(Owner)^.xFirstPtr := n^.ptrtable^.pos;
   if PGroup(Owner)^.xFirstPtr < 0 then Dec(d, dword(abs(PGroup(Owner)^.xFirstPtr)))
   Else                  inc(d, dword(abs(PGroup(Owner)^.xFirstPtr)));
  end;
  Case PGroup(Owner)^.xMethod of
{   tmDLL:
    S := Cod2Str(MLenF, CConv, Addr(KruptarForm.KruptarProject.ROM^[D]));}
   tmP2P:
   begin
    zrom := Addr(KruptarForm.KruptarProject.ROM^[D]);
  Case PGroup(Owner)^.xPtrSize of
   2: if PGroup(Owner)^.xMotorola then
   begin
    dwd(dU).d := 0;
    dwd(dU).c := 0;
    dwd(dU).b := Zrom^[0];
    dwd(dU).a := Zrom^[1 * pntt];
   end Else
   begin
    dwd(du).a := Zrom^[0];
    dwd(du).b := Zrom^[1 * pntt];
    dwd(du).c := 0;
    dwd(du).d := 0;
   end;
   3: if PGroup(Owner)^.xMotorola then
   begin
    dwd(du).d := 0;
    dwd(du).c := Zrom^[0];
    dwd(du).b := Zrom^[1 * pntt];
    dwd(du).a := Zrom^[2 * pntt];
   end Else
   begin
    dwd(du).a := Zrom^[0];
    dwd(du).b := Zrom^[1 * pntt];
    dwd(du).c := Zrom^[2 * pntt];
    dwd(du).d := 0;
   end;
   4: if PGroup(Owner)^.xMotorola then
   begin
    dwd(du).d := Zrom^[0];
    dwd(du).c := Zrom^[1 * pntt];
    dwd(du).b := Zrom^[2 * pntt];
    dwd(du).a := Zrom^[3 * pntt];
   end Else
   begin
    dwd(du).a := Zrom^[0];
    dwd(du).b := Zrom^[1 * pntt];
    dwd(du).c := Zrom^[2 * pntt];
    dwd(du).d := Zrom^[3 * pntt];
   end;
  end;
  If PGroup(Owner)^.xShl1 then Du := Du shl 1;
  if PGroup(Owner)^.xSigned and (PGroup(Owner)^.xPtrSize in [2, 4]) then
  begin
   With PGroup(Owner)^ do
   Case xPtrSize of
    2:
    begin
     LongInt(Du) := SmallInt(Du);
     Inc(Du, P + I * (2 + xInterval));
    end;
    4: Inc(Du, P + I * (4 + xInterval));
   end;
  end Else
  begin
   If PGroup(Owner)^.xAutoStart then PGroup(Owner)^.xFirstPtr := n^.ptrtable^.pos;
   if PGroup(Owner)^.xFirstPtr < 0 then Dec(du, dword(abs(PGroup(Owner)^.xFirstPtr)))
   Else                  inc(du, dword(abs(PGroup(Owner)^.xFirstPtr)));
  end;
    j := 0;
    undoo := false;
    repeat
     if du + j < KruptarForm.KruptarProject.ROMSize then
     begin
      ss := GetSymbol(du + j);
      if PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
       if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
       begin
        s := s + PGroup(Owner)^.eundostr;
        PGroup(Owner)^.endsym := false;
       end else s := s + ss;
      end;
     end Else
     begin
      s := s + '/';
      break;
     end;
     inc(j, td);
    until PGroup(Owner)^.endsym;
   end;
   tmNormal:
   begin
    j := 0;
    undoo := false;
    repeat
     if d + j < KruptarForm.KruptarProject.ROMSize then
     begin
      ss := GetSymbol(d + j);
      If PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
       if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
       begin
        s := s + PGroup(Owner)^.eundostr;
        PGroup(Owner)^.endsym := false;
       end else s := s + ss;
      end;
     end Else
     begin
      s := s + '/';
      break;
     end;
     inc(j, td);
    until PGroup(Owner)^.endsym;
   end; //tmNormal
   tmBkp:
   begin
    jb := 0;
    wd(bki).a := KruptarForm.KruptarProject.ROM^[d];
    wd(bki).b := KruptarForm.KruptarProject.ROM^[d + 1];
    Repeat
     j := bki;
     undoo := false;
     repeat
      if d + j < KruptarForm.KruptarProject.ROMsize then
      begin
       ss := GetSymbol(d + j);
       if PGroup(Owner)^.undsym then undoo := true;
       if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
       begin
        ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
        PGroup(Owner)^.Tadlen := 1;
        PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
        PGroup(Owner)^.endsym := False;
        S := S + ss;
        Inc(J);
       end Else
       begin
        Inc(J);
        if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
        begin
         s := s + PGroup(Owner)^.eundostr;
         PGroup(Owner)^.endsym := false;
        end else s := s + ss;
       end;
      end Else
      begin
       s := s + '/';
       break;
      end;
      inc(j, td);
     until PGroup(Owner)^.endsym;
     Inc(jb, 2);
     wd(bki).a := KruptarForm.KruptarProject.rom^[d + jb];
     wd(bki).b := KruptarForm.KruptarProject.rom^[d + jb + 1];
    until bki = $FFFF;
   end; //tmBkp
   tmL1T, tmL1Te:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
       if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
       begin
        s := s + PGroup(Owner)^.eundostr;
        PGroup(Owner)^.endsym := false;
       end else s := s + ss;
      end;
     inc(j, td);
    until j > KruptarForm.KruptarProject.rom^[d];
   end; //tmL1T(e)
   tmHat:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
       if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
       begin
        s := s + PGroup(Owner)^.eundostr;
        PGroup(Owner)^.endsym := false;
       end else s := s + ss;
      end;
     inc(j, td);
    until j > KruptarForm.KruptarProject.rom^[d] * 2 + 1;
   end; //tmHat
   tmL1TL1T:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
     if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
     begin
      s := s + PGroup(Owner)^.eundostr;
      PGroup(Owner)^.endsym := false;
     end else s := s + ss;
     end;
     inc(j, td);
    until j > KruptarForm.KruptarProject.rom^[d];
    s := s + '^';
    psz := j; j := 1;
    repeat
     ss := GetSymbol(d + psz + j);
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin
       Inc(J);
       s := s + ss;
      end;
     inc(j, td);
    until j > KruptarForm.KruptarProject.rom^[d + psz];
   end;
   tmC2S1eSne:
   begin
    j := 0;
    if (KruptarForm.KruptarProject.rom^[d + j + 1 * Byte(PGroup(Owner)^.xMotorola)] > 0) or
      ((KruptarForm.KruptarProject.rom^[d + j] > 0) and
       (KruptarForm.KruptarProject.rom^[d + j + 1] = 0)) then
    begin
     wd(sinll).b := KruptarForm.KruptarProject.rom^[d + j +
                     1 * Byte(not PGroup(Owner)^.xMotorola)];
     wd(sinll).a := KruptarForm.KruptarProject.rom^[d + j +
                     1 * Byte(PGroup(Owner)^.xMotorola)];
     inc(j, 2);
    end Else
    begin
     while KruptarForm.KruptarProject.rom^[d + j] = 0 do inc(j);
     sinll := j div 2;
    end;
    for ii := 1 to sinll do
    begin
     undoo := false;
     repeat
      if d + j < KruptarForm.KruptarProject.Romsize then
      begin
       ss := GetSymbol(d + j);
       if PGroup(Owner)^.undsym then undoo := true;
       if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
       begin
        ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
        PGroup(Owner)^.Tadlen := 1;
        PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
        PGroup(Owner)^.endsym := False;
        S := S + ss;
        Inc(J);
       end Else
       begin
        Inc(J);
        if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
        begin
         s := s + PGroup(Owner)^.eundostr;
         PGroup(Owner)^.endsym := false;
        end else s := s + ss;
       end;
      end Else break;
      inc(j, td);
     until PGroup(Owner)^.endsym;
    end;
   end;
  end;
 endst:
  n^.ptrtable^.add(d, s);
  if PGroup(Owner)^.xMethod = tmC2S1eSne then n^.ptrtable^.cur^.sinl := sinll;
  RecalcForm.RCounter := RCount + 1;
 end Else
 begin
  If PGroup(Owner)^.xStrLen > 0 then for i := 0 to cnt - 1 do
  begin
   s := ''; j := 0;
   undoo := false;
   repeat
    ss := GetSymbol(d + j);
    if PGroup(Owner)^.undsym then undoo := true;
      if (ss = '') then For TW := 1 to PGroup(Owner)^.xAlign do
      begin
       ss := '[#' + Inttohex(KruptarForm.KruptarProject.ROM^[d + j], 2) + ']';
       PGroup(Owner)^.Tadlen := 1;
       PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.ROM^[d + j]] := ss;
       PGroup(Owner)^.endsym := False;
       S := S + ss;
       Inc(J);
      end Else
      begin Inc(J);
    if PGroup(Owner)^.endsym and undoo and (PGroup(Owner)^.eundostr <> '') then
    begin
     s := s + PGroup(Owner)^.eundostr;
     PGroup(Owner)^.endsym := false;
    end else s := s + ss; end;
    inc(j, td);
   until j = PGroup(Owner)^.xStrLen;
   N^.PtrTable^.Add(D, S);
   With PGroup(Owner)^ do Inc(D, Dword(xStrLen + xInterval));
   RecalcForm.RCounter := RCount + 1;
  end Else
  begin
   i := 0; j := 0;
   repeat
    if j = 0 then
    begin
     tw := prom^[i] * $1000000;
     tw := tw + prom^[i + 1] * $10000;
     tw := tw + prom^[i + 2] * $100;
     tw := tw + prom^[i + 3];
     for td := 3 downto 0 do
     begin
      ss := PGroup(Owner)^.table1[tn,tw];
      if PGroup(Owner)^.tadlen <> td + 1 then ss := '';
      if ss = '' then tw := tw div $100 else break;
      if td = 0 then break;
     end;
     j := 1;
    end;
    ss := GetSymbol(d + j - 1);
    if (ss = '') and (PGroup(Owner)^.emptyc > 0) then
    begin
     ss := '[#' + Inttohex(KruptarForm.KruptarProject.rom^[d + j - 1], 2) + ']';
     PGroup(Owner)^.tadlen := 1;
     PGroup(Owner)^.table1[tn,KruptarForm.KruptarProject.rom^[d + j - 1]] := ss;
    end;
    s := s + ss;
    RecalcForm.RCounter := RCount + 1;
    if PGroup(Owner)^.endsym then
    begin
     n^.ptrtable^.add(d, s);
     inc(d, j + td);
     j := 0;
     s := '';
    end Else inc(j, td + 1);
    inc(i, td + 1);
    eloop:
   until i >= cnt;
  end;
 end;
{ if @Cod2Str <> nil then FreeLibrary(Handle);}
 CC := True;
 RecalcForm.Close;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
end;

Procedure TPosso.AddE(tn: Integer; p: DWord; pp: PPtrTable);
var i, d: dword; prom: ^tromdata; s,ss: WideString;
var n, rr: pposs; tt: pttable; tw, td: dword;
label eloop;
begin
 new(n);
 new(n^.ptrtable, init);
 n^.ptrtable^.pos := p;
 n^.tabn := tn;
 n^.next := nil;
 tt := pp^.root;
 while tt <> nil do
 begin
  d := tt^.tptr;
  prom := Addr(KruptarForm.KruptarProject.rom^[d]);
  i := 0; s := '';
  repeat
   tw := prom^[i] * $1000000;
   tw := tw + prom^[i + 1] * $10000;
   tw := tw + prom^[i + 2] * $100;
   tw := tw + prom^[i + 3];
   for td := 3 downto 0 do
   begin
    ss := PGroup(Owner)^.table1[tn,tw];
    if PGroup(Owner)^.tadlen <> td + 1 then ss := '';
    if ss = '' then tw := tw div $100 else break;
    if td = 0 then break;
   end;
   s := s + ss;
   inc(i, td + 1);
   n^.ptrtable^.bcnt := i;
   pp^.bcnt := i;
  until PGroup(Owner)^.EndSym or (ss = '') or (d + i >= KruptarForm.KruptarProject.ROMSize);
  n^.ptrtable^.add(d, s);
  Break;
 end;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
end;

Destructor TPosso.Clear;
var n: pposs;
begin
 while root <> nil do
 begin
  n := root;
  root := n^.next;
  dispose(n^.ptrtable, clear);
  dispose(n);
 end;
end;

Constructor TPossoR.Init;
begin
 root := nil;
 cur := nil;
 Owner := nil;
 count := 0;
end;

Procedure TPossoR.AddPosso;
var n, nn, rr: pposs; nt: pttable; i: integer;
begin
 n := PGroup(Owner)^.Posso.root;
 if root <> nil then for i := 1 to count do n := n^.next;
 while n <> nil do
 begin
  nt := n^.ptrtable^.root;
  new(nn);
  nn^.next := nil;
  nn^.tabn := n^.tabn;
  n^.GutGut := nn;
  nn^.GutGut := n;
  new(nn^.ptrtable, init);
  nn^.next := nil;
  while nt <> nil do
  begin
   nn^.ptrtable^.pos := n^.ptrtable^.pos;
   nn^.ptrtable^.bcnt := n^.ptrtable^.bcnt;
   nn^.ptrtable^.Name := n^.ptrtable^.Name;
   nn^.ptrtable^.add(nt^.tptr, nt^.text);
   nn^.ptrtable^.cur^.same := nt^.same;
   nn^.ptrtable^.cur^.sinl := nt^.sinl;
   nt := nt^.next;
  end;
  rr := cur;
  cur := nn;
  if rr <> nil then rr^.next := nn;
  if root = nil then root := nn;
  n := n^.next;
  inc(count);
 end;
end;

Destructor TPossoR.Clear;
var n: pposs;
begin
 while root <> nil do
 begin
  n := root;
  root := n^.next;
  dispose(n^.ptrtable, clear);
  dispose(n);
 end;
end;

Constructor TGroup.Create(Own: Pointer);
begin
 Owner := Own;
 PGroups(Own)^.Root := @Self;
 PGroups(Own)^.Cur := @Self;
 PGroups(Own)^.Count := 1;
 GrName := '';
 xPtrSize := 0;
 xInterval := 0;
 xKrat := 0;
 xAlign := 1;
 xMethod := tmNone;
 xSigned := false;
 xMotorola := false;
 xKodir := 0;
 xFirstPtr := 0;
 xNoPtr := false;
 xStrLen := 0;
 xPntp := false;
 xAutoStart := false;
 CurP := NIL;
 CurP2 := NIL;
 New(EmptyS);
 EmptyC := 0;
 Oofs := 0;
 OTables.Init;
 OTables.Owner := @Self;
 Posso.Init;
 Posso.Owner := @Self;
 PossoR.Init;
 PossoR.Owner := @Self;
 Tadlen := 0;
 Tt := false;
 EndSym := false;
 UndSym := false;
 eUndoStr := '';
 Next := NIL;
end;

Constructor TGroup.Add(Own: Pointer);
begin
 Owner := Own;
 PGroups(Own)^.Cur^.Next := @Self;
 PGroups(Own)^.Cur := @Self;
 Inc(PGroups(Own)^.Count);
 GrName := '';
 xAlign := 1;
 xPtrSize := 0;
 xKrat := 0;
 xMethod := tmNone;
 xSigned := False;
 xMotorola := False;
 xKodir := 0;
 xFirstPtr := 0;
 xNoPtr := False;
 xStrLen := 0;
 xPntp := False;
 xAutoStart := False;
 CurP := NIL;
 CurP2 := NIL;
 New(EmptyS);
 EmptyC := 0;
 Oofs := 0;
 OTables.Init;
 OTables.Owner := @Self;
 Posso.Init;
 Posso.Owner := @Self;
 PossoR.Init;
 PossoR.Owner := @Self;
 Tadlen := 0;
 Tt := false;
 EndSym := false;
 UndSym := false;
 eUndoStr := '';
 Next := NIL;
end;

Destructor TGroup.Clear;
begin
 GrName := '';
 With PGroups(Owner)^ do If Cur = @Self then Cur := Next;
 Dec(PGroups(Owner)^.Count);
 Dispose(Emptys);
 Posso.Clear;
 PossoR.Clear;
 OTables.Done;
end;

Procedure TGroup.LoadTab(Tn: Integer; Ff: String);
var
 TFile: TextFile; S: WideString; P, B: Integer; C: WideString;
 Eeb, Unb: Boolean; Ps, Es, Ed, Ee: DWord; WS: TUnicodeListBox;
begin
 Eeb := false; Unb := false;
 eundostr := '';
 WS := KruptarForm.UniTab;
 WS.Items.Clear;
 If xKodir = kdUNIC then
 begin
  WS.Items.LoadFromFile(FF);
  For Ps := 0 to WS.Items.Count - 1 do
  begin
   S := WS.Items.Strings[PS];
   If s = 'undo' then unb := true Else
   if s = 'ends' then eeb := true Else
   If Pos('=', s) > 0 then
   begin
    P := Pos('=', s);
    C := Copy(S, P + 1, Length(s) - P);
    If Eeb and Unb then
    begin
     B := HexVal(Copy(S, 2, P - 2));
     TadLen := Length(Copy(S, 2, p - 2)) div 2;
     EndSym := True;
     UndSym := False;
     EundoStr := c;
     If not Tt then Table1[tn, b] := S[1] Else Table2[Tn, b] := s[1];
    end Else
    begin
     B := HexVal(Copy(S, 1, P - 1));
     TadLen := Length(Copy(S, 1, P - 1)) div 2;
     EndSym := False;
     UndSym := False;
     If Unb then
     begin
      UndSym := True;
      Unb := False;
     end;
     If not Tt then Table1[Tn, b] := C else Table2[Tn, b] := c;
    end;
   end else if (length(s) >= 3) and (pos('=', s) <= 0) then
   case s[1] of
    '~':
    begin
     Es := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := eeb;
     undsym := false;
     if not tt then table1[tn, Es] := s[1] Else table2[tn, Es] := s[1];
    end;
    '^':
    begin
     es := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := eeb;
     undsym := false;
     if not tt then table1[tn, es] := s[1] Else table2[tn, es] := s[1];
    end;
    '*':
    begin
     ed := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := eeb;
     undsym := false;
     if not tt then table1[tn, ed] := s[1] Else table2[tn, ed] := s[1];
    end;
    '/':
    begin
     ee := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := true;
     undsym := false;
     if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
    end;
    '\':
    begin
     ee := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := eeb;
     undsym := false;
     if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
    end;
    Else If eeb then
    begin
     ee := hexval(copy(s, 2, length(s) - 1));
     tadlen := length(copy(s, 2, length(s) - 1)) div 2;
     endsym := true;
     undsym := false;
     if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
    end;
   end;
  end;
  WS.Items.Clear;
 end Else begin
 Assignfile(tfile, ff); Reset(tfile); while not eof(tfile) do
 begin
  Readln(TFile, s);
  If s = 'undo' then unb := true Else
  if s = 'ends' then eeb := true Else
  if pos('=', s) > 0 then
  begin
   p := pos('=', s);
   c := copy(s, p + 1, length(s) - p);
   if eeb and unb then
   begin
    b := Hexval(copy(s, 2, p - 2));
    tadlen := length(copy(s, 2, p - 2)) div 2;
    endsym := true;
    undsym := false;
    eundostr := c;
    if not tt then Table1[tn, b] := s[1] Else Table2[tn, b] := s[1];
   end Else
   begin
    b := Hexval(copy(s, 1, p - 1));
    tadlen := length(copy(s, 1, p - 1)) div 2;
    endsym := false;
    undsym := false;
    if unb then
    begin
     undsym := true;
     unb := false;
    end;
    if not tt then table1[tn, b] := c else table2[tn, b] := c;
   end;
  end else if (length(s) >= 3) and (pos('=', s) <= 0) then
  case s[1] of
   '~':
   begin
    ps := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ps] := s[1] Else table2[tn, ps] := s[1];
   end;
   '^':
   begin
    es := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, es] := s[1] Else table2[tn, es] := s[1];
   end;
   '*':
   begin
    ed := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ed] := s[1] Else table2[tn, ed] := s[1];
   end;
   '/':
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := true;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
   '\':
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
   Else If eeb then
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := true;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
  end;
 end; CloseFile(TFile) end;
end;

Function TGroup.GetTitem(TabN, x: DWord): WideString;
var n: pctable; rc: pctables; i: integer;
begin
 result := '';
 endsym := false;
 rc := Otables.rootO;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.p = x) then
  begin
   result := n^.s;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end;
  n := n^.next;
 end;
end;

Procedure TGroup.SetTitem(TabN, x: DWord; Value: WideString);
var n, rr: pctable; b: boolean; rc: pctables; i: integer;
begin
 rc := Otables.rootO;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 b := true;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.p = x) and (n^.tl = tadlen) then
  begin
   b := false;
   break;
  end;
  n := n^.next;
 end;
 if b then new(n);
 n^.s := value;
 n^.p := x;
 n^.tl := tadlen;
 n^.endstr := endsym;
 n^.undo := undsym;
 n^.eundo := eundostr;
 if n^.tl = 0 then n^.tl := 1;
 if length(value) > rc^.cl then rc^.cl := length(value);
 if b then
 begin
  n^.next := nil;
  rr := rc^.cur;
  rc^.cur := n;
  if rr <> nil then rr^.next := rc^.cur;
  if rc^.tab = nil then rc^.tab := n;
  inc(rc^.count);
 end;
end;

Function TGroup.GetT2item(TabN, x: DWord): WideString;
var n: pctable; rc: pctables; i: integer;
begin
 result := '';
 endsym := false;
 rc := Otables.rootE;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.p = x then
  begin
   result := n^.s;
   tadlen := n^.tl;
   endsym := n^.endstr;
   eundostr := n^.eundo;
   undsym := n^.undo;
   break;
  end;
  n := n^.next;
 end;
end;

Procedure TGroup.SetT2item(TabN, x: DWord; Value: WideString);
var n, rr: pctable; b: boolean; rc: pctables; i: integer;
begin
 rc := Otables.rootE;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 b := true;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.s = value) then
  begin
   b := false;
   break;
  end;
  n := n^.next;
 end;
 if b then new(n);
 n^.s := value;
 n^.p := x;
 n^.tl := tadlen;
 n^.endstr := endsym;
 n^.undo := undsym;
 n^.eundo := eundostr;
 if n^.tl = 0 then n^.tl := 1;
 if length(value) > rc^.cl then rc^.cl := length(value);
 if b then
 begin
  n^.next := nil;
  rr := rc^.cur;
  rc^.cur := n;
  if rr <> nil then rr^.next := rc^.cur;
  if rc^.tab = nil then rc^.tab := n;
  inc(rc^.count);
 end;
end;

Function TGroup.GetATitem(x: WideString; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.s = x) then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end Else
  If (n^.eundo = x) then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := false;
   undsym := false;
   eundostr := '';
   break;
  end;
  n := n^.next;
 end;
end;

Function TGroup.GetUTitem(x: WideString; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.eundo = x then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   break;
  end;
  n := n^.next;
 end;
end;

Function TGroup.GetA1Titem(x: WideString; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootO;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.s = x then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end;
  n := n^.next;
 end;
end;

Procedure TGroup.GetTableNames(tabn: Integer; Var n1, n2: String);
var rc: pctables; i: integer;
begin
 n1 := ''; n2 := '';
 if tabn > otables.Count - 1 then exit;
 rc := Otables.rootO;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n1 := rc^.fname;
 rc := Otables.rootE;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n2 := rc^.fname;
end;

Function TGroup.GetMLen(tabn: integer): integer;
var rc: pctables; i: integer;
begin
 result := 0;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 result := rc^.cl;
end;

Constructor TGroups.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

Destructor TGroups.Done;
begin
 Cur := Root;
 While Cur <> NIL do Dispose(Cur, Clear);
 Root := NIL;
 Count := 0;
end;

Constructor TKruptarProject.Init(i, o: string);
begin
 KruptarForm.Named := False;
 KruptarForm.Saved := False;
 Dir := '';
 Emulator := '';
 InputROM := i;
 OutputROM := o;
 ProjectFile := '';
 Create;
end;

Procedure TKruptarProject.LoadROM;
Var ffile: File;
begin
 if ROMSize > 0 then
  FreeMem(ROM, ROMSize + 4);
 if ROMESize > 0 then
  FreeMem(ROME, ROMESize + 4);
 Assignfile(ffile, InputROM);
 Reset(FFile, 1);
 ROMSize := Filesize(FFile);
 GetMem(ROM, ROMSize + 4);
 BlockRead(FFile, ROM^, ROMSize);
 CloseFile(ffile);
 If not  KruptarForm.HexEditor1.Checked then
 begin
  Assignfile(ffile, OutPutROM);
  Reset(FFile, 1);
  ROMESize := Filesize(FFile);
  GetMem(ROME, ROMESize + 4);
  BlockRead(FFile, ROME^, ROMESize);
  CloseFile(ffile);
 end;
end;

Procedure TKruptarProject.FreeROM;
begin
 If ROMSize > 0 then
 begin
  FreeMem(ROM, ROMSize + 4);
  ROMSize := 0;
 end;
 If ROMESize > 0 then
 begin
  FreeMem(ROME, ROMESize + 4);
  ROMESize := 0;
 end;
end;

Procedure TKruptarProject.Create;
begin
 LoadROM;
 Groups.Init;
end;

Destructor TKruptarProject.Done;
begin
 Groups.Done;
 FreeROM;
 KruptarForm.Named := False;
 KruptarForm.Saved := False;
 Dir := '';
 Emulator := '';
 InputROM := '';
 OutputROM := '';
 ProjectFile := '';
 StringsListForm.Caption := ListTitle;
end;

Procedure TKruptarProject.SaveUnic;
Procedure Writeln(F: TUnicodeListBox; S: WideString);
begin
 F.Items.Add(S);
end;
Var PFile: TUnicodeListBox; P, Q, R: Pointer; I, J: Integer; S: WideString;
begin
 If ProjectFile = '' then Exit;
 I := Length(ProjectFile);
 Dir := ProjectFile;
 While (I > 0) and (Dir[I] <> '\') do Dec(I);
 Inc(I); Delete(Dir, I, Length(Dir) - I + 1);
 PFile := KruptarForm.Unicum;
 PFile.Items.Clear;
 Writeln(PFile, prKruptarProject);
 S := InputROM;
 If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
 Writeln(PFile, prInputROM + CEq + S);
 S := OutputROM;
 If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
 Writeln(PFile, prOutputROM + CEq + S);
 Writeln(PFile, prEmulator + CEq + Emulator);
 P := Groups.Root;
 While P <> NIL do With PGroup(P)^ do
 begin
  Writeln(PFile, prDefineGroup + CEq + GrName);
  If not xNoPtr then
  begin
   Writeln(PFile, prPointerUse + CEq + prTRUE);
   Writeln(PFile, prPointerSize + CEq + IntToStr(xPtrSize));
   Writeln(PFile, prInterval + CEq + IntToStr(xInterval));
   Writeln(PFile, prDivision + CEq + IntToStr(xKrat));
   If xPntp then
    Writeln(PFile, prType + CEq + '1') Else
   If xAutoStart then
    Writeln(PFile, prType + CEq + '2') Else
    Writeln(PFile, prType + CEq + '0');
   If xMotorola then
    Writeln(PFile, prMotorola + CEq + prTRUE) Else
    Writeln(PFile, prMotorola + CEq + prFALSE);
   If xSigned then
    Writeln(PFile, prSigned + CEq + prTRUE) Else
    Writeln(PFile, prSigned + CEq + prFALSE);
   If xShl1 then
    Writeln(PFile, prShl1 + CEq + prTRUE) Else
    Writeln(PFile, prShl1 + CEq + prFALSE);
   Writeln(PFile, prMethod + CEq + IntToStr(Byte(xMethod)));
   Writeln(PFile, prDifference + CEq + CHex + IntToHex(xFirstPtr, 8));
   Case xKodir of
    kdANSI: Writeln(PFile, prCharSet + CEq + prANSI);
    kdSJIS: Writeln(PFile, prCharSet + CEq + prSJIS);
    kdUNIC: Writeln(PFile, prCharSet + CEq + prUNIC);
   end;
  end Else
  begin
   Writeln(PFile, prPointerUse + CEq + prFALSE);
   Case xKodir of
    kdANSI: Writeln(PFile, prCharSet + CEq + prANSI);
    kdSJIS: Writeln(PFile, prCharSet + CEq + prSJIS);
    kdUNIC: Writeln(PFile, prCharSet + CEq + prUNIC);
   end;
   Writeln(PFile, prStringLength + CEq + IntToStr(xStrLen));
  end;
  If OTables.Count > 0 then
  begin
   Writeln(PFile, prTablesStart);
   Q := OTables.rootO;
   R := OTables.rootE;
   If (Q <> NIL) and (R <> NIL) then For I := 0 to OTables.Count - 1 do
   begin
    S := pctables(Q)^.fname;
    If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
    Writeln(PFile, S);
    S := pctables(R)^.fname;
    If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
    Writeln(PFile, S);
    Q := pctables(Q)^.next;
    R := pctables(R)^.next;
    If (Q = NIL) or (R = NIL) then Break;
   end;
   Writeln(PFile, prTablesEnd);
  end;
  If EmptyC > 0 then
  begin
   Writeln(PFile, prBlocksStart);
   For I := 0 to EmptyC - 1 do
   begin
    Writeln(PFile, CHEX + IntToHex(EmptyS^[i].a, 8));
    Writeln(PFile, CHEX + IntToHex(EmptyS^[i].b, 8));
   end;
   Writeln(PFile, prBlocksEnd);
  end;
  Q := PossoR.Root;
  While Q <> NIL do With PPoss(Q)^ do
  begin
   Writeln(PFile, prDefineTextBlock + CEq + PtrTable^.Name);
   Writeln(PFile, prUsedTableSet + CEq + IntToStr(TabN));
   If xNoPtr then
   begin
    Writeln(PFile, prBlockStart + CEq + CHEX + IntToHex(PtrTable^.Pos, 8));
    If xStrLen = 0 then
     Writeln(PFile, prBlockLength + CEq + IntToStr(PtrTable^.BCnt));
   end Else
    Writeln(PFile, prPtrTableStart + CEq + CHEX + IntToHex(PtrTable^.Pos, 8));
   R := PtrTable^.Root;
   While R <> NIL do With PtTable(R)^ do
   begin
    Writeln(PFile, prStringPointer + CEq + CHEX + IntToHex(TPtr, 8));
    if xMethod = tmC2S1eSne then
     Writeln(PFile, prStringsCount + CEq + IntToStr(Sinl));
    Writeln(PFile, prStringStart);
    S := Text;
    i := 0; j := 1;
    If S <> '' then
    repeat
     repeat
      Inc(I);
     until (I = Length(S)) or (
     (s[i] = '~') or
     (s[i] = '^') or
     (s[i] = '*') or
     (s[i] = '/') or
     (s[i] = '\'));
     Writeln(PFile, Copy(S, j, i - j + 1));
     j := i + 1;
    until i = Length(s);
    Writeln(PFile, prStringEnd);
    R := Next;
   end;
   Writeln(PFile, prTextBlockEnd);
   Q := Next;
  end;
  Writeln(PFile, prGroupEnd);
  P := Next;
 end;
 Writeln(PFile, prProjectEnd);
 Pfile.Items.SaveToFile(ProjectFile);
 PFile.Items.Clear;
 KruptarForm.Named := True;
 KruptarForm.Saved := True;
end;

Procedure TKruptarProject.Save;
Var PFile: TextFile; P, Q, R: Pointer; I, J: Integer; S: WideString;
begin
 If ProjectFile = '' then Exit;
 P := Groups.Root;
 While P <> NIL do With PGroup(P)^ do
 begin
  If xKodir = kdUNIC then
  begin
   SaveUnic;
   Exit;
  end;
  P := Next;
 end;
 I := Length(ProjectFile);
 Dir := ProjectFile;
 While (I > 0) and (Dir[I] <> '\') do Dec(I);
 Inc(I); Delete(Dir, I, Length(Dir) - I + 1);
 AssignFile(PFile, ProjectFile);
 Rewrite(PFile);
 Writeln(PFile, prKruptarProject);
 S := InputROM;
 If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
 Writeln(PFile, prInputROM + CEq + S);
 S := OutputROM;
 If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
 Writeln(PFile, prOutputROM + CEq + S);
 Writeln(PFile, prEmulator + CEq + Emulator);
 P := Groups.Root;
 While P <> NIL do With PGroup(P)^ do
 begin
  Writeln(PFile, prDefineGroup + CEq + GrName);
  If not xNoPtr then
  begin
   Writeln(PFile, prPointerUse + CEq + prTRUE);
   Writeln(PFile, prPointerSize + CEq + IntToStr(xPtrSize));
   Writeln(PFile, prInterval + CEq + IntToStr(xInterval));
   Writeln(PFile, prDivision + CEq + IntToStr(xKrat));
   If xPntp then
    Writeln(PFile, prType + CEq + '1') Else
   If xAutoStart then
    Writeln(PFile, prType + CEq + '2') Else
    Writeln(PFile, prType + CEq + '0');
   If xMotorola then
    Writeln(PFile, prMotorola + CEq + prTRUE) Else
    Writeln(PFile, prMotorola + CEq + prFALSE);
   If xSigned then
    Writeln(PFile, prSigned + CEq + prTRUE) Else
    Writeln(PFile, prSigned + CEq + prFALSE);
   If xShl1 then
    Writeln(PFile, prShl1 + CEq + prTRUE) Else
    Writeln(PFile, prShl1 + CEq + prFALSE);
   Writeln(PFile, prMethod + CEq + IntToStr(Byte(xMethod)));
   Writeln(PFile, prDifference + CEq + CHex + IntToHex(xFirstPtr, 8));
   If xKodir = kdSJIS then
    Writeln(PFile, prCharSet + CEq + prSJIS) Else
    Writeln(PFile, prCharSet + CEq + prANSI);
  end Else
  begin
   Writeln(PFile, prPointerUse + CEq + prFALSE);
   If xKodir = kdSJIS then
    Writeln(PFile, prCharSet + CEq + prSJIS) Else
    Writeln(PFile, prCharSet + CEq + prANSI);
   Writeln(PFile, prStringLength + CEq + IntToStr(xStrLen));
  end;
  If OTables.Count > 0 then
  begin
   Writeln(PFile, prTablesStart);
   Q := OTables.rootO;
   R := OTables.rootE;
   If (Q <> NIL) and (R <> NIL) then For I := 0 to OTables.Count - 1 do
   begin
    S := pctables(Q)^.fname;
    If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
    Writeln(PFile, S);
    S := pctables(R)^.fname;
    If Pos(Dir, S) = 1 then Delete(S, 1, Length(Dir));
    Writeln(PFile, S);
    Q := pctables(Q)^.next;
    R := pctables(R)^.next;
    If (Q = NIL) or (R = NIL) then Break;
   end;
   Writeln(PFile, prTablesEnd);
  end;
  If EmptyC > 0 then
  begin
   Writeln(PFile, prBlocksStart);
   For I := 0 to EmptyC - 1 do
   begin
    Writeln(PFile, CHEX + IntToHex(EmptyS^[i].a, 8));
    Writeln(PFile, CHEX + IntToHex(EmptyS^[i].b, 8));
   end;
   Writeln(PFile, prBlocksEnd);
  end;
  Q := PossoR.Root;
  While Q <> NIL do With PPoss(Q)^ do
  begin
   Writeln(PFile, prDefineTextBlock + CEq + PtrTable^.Name);
   Writeln(PFile, prUsedTableSet + CEq + IntToStr(TabN));
   If xNoPtr then
   begin
    Writeln(PFile, prBlockStart + CEq + CHEX + IntToHex(PtrTable^.Pos, 8));
    If xStrLen = 0 then
     Writeln(PFile, prBlockLength + CEq + IntToStr(PtrTable^.BCnt));
   end Else
    Writeln(PFile, prPtrTableStart + CEq + CHEX + IntToHex(PtrTable^.Pos, 8));
   R := PtrTable^.Root;
   While R <> NIL do With PtTable(R)^ do
   begin
    Writeln(PFile, prStringPointer + CEq + CHEX + IntToHex(TPtr, 8));
    if xMethod = tmC2S1eSne then
     Writeln(PFile, prStringsCount + CEq + IntToStr(Sinl));
    Writeln(PFile, prStringStart);
    S := Text;
    i := 0; j := 1;
    If S <> '' then
    repeat
     repeat
      Inc(I);
     until (I = Length(S)) or (
     (s[i] = '~') or
     (s[i] = '^') or
     (s[i] = '*') or
     (s[i] = '/') or
     (s[i] = '\'));
     Writeln(PFile, Copy(S, j, i - j + 1));
     j := i + 1;
    until i = Length(s);
    Writeln(PFile, prStringEnd);
    R := Next;
   end;
   Writeln(PFile, prTextBlockEnd);
   Q := Next;
  end;
  Writeln(PFile, prGroupEnd);
  P := Next;
 end;
 Write(PFile, prProjectEnd);
 CloseFile(PFile);
 KruptarForm.Named := True;
 KruptarForm.Saved := True;
end;

Procedure ErrorMessage;
begin
 ShowMessage('������ � ����� �������!');
end;

Function TrueStr(S: String): Boolean;
begin
 Result := UpperCase(S) = prTrue;
end;

Procedure TKruptarProject.Redir(Var S: WideString);
begin
 If (Pos(':', S) < 2) then S := Dir + S;
end;

Procedure TKruptarProject.Open(FileName: String);
Var PFile: TextFile; WS: TUnicodeListBox; JJ: Integer; UNIC: Boolean;
Procedure ReadLN(Var S: WideString);
begin
 If Unic then
 begin
  If JJ >= WS.Items.Count then Exit;
  S := WS.Items.Strings[JJ];
  Inc(JJ);
 end Else System.ReadLN(PFile, S);
end;
Procedure CloseFile;
begin
 If not Unic then System.CloseFile(PFile) Else WS.Items.Clear;
end;
Function EofX: Boolean;
begin
 If Unic then Result := JJ >= WS.Items.Count Else Result := Eof(PFile);
end;
Function GetCurCom(Var S: WideString): WideString;
Var Ss: WideString; P, i, j: Integer;
begin
 Result := '';
 While (Result = '') and not EofX do
 begin
  S := '';
  ReadLn(Ss);
  P := Pos('[', SS);
  For i := P to Length(SS) do
   If SS[i] = ']' then
   begin
    Result := Copy(SS, p, I - p + 1);
    Delete(ss, 1, I);
    Break;
   end;
  J := 1;
  While (J <= Length(ss)) and (SS[j] = ' ') do Inc(J);
  Delete(SS, 1, J);
  J := 1;
  While (J <= Length(ss)) and (SS[j] = ' ') do Inc(J);
  Delete(SS, 1, J - 1);
  S := SS;
 end;
 Result := UpperCase(Result);
end;
Procedure Flash(Grp: PGroup);
var n, nn: pposs;
begin
 n := Grp^.Posso.Root;
 nn := Grp^.PossoR.Root;
 while (n <> nil) and (nn <> NIL) do
 begin
  n^.PtrTable^.Name := nn^.PtrTable^.Name;
  n^.GutGut := nn;
  nn^.GutGut := n;
  n := n^.next;
  nn := nn^.next;
 end;
end;

Var pCommand, Namae, S, RI, RO: WideString; NewC: PGroup; GroupsP: ^TGroups; I, J: Cardinal;
    bbChanged: Boolean; Karr: PPoss; _xTN: Integer; _xPos, _xCnt: Dword; RR: Pointer;
    SA: String; Test: File; ZI: Word;
Label Axit, Uxit;
begin
 bbChanged := False;
 I := Length(FileName);
 Dir := Filename;
 While (I > 0) and (Dir[I] <> '\') do Dec(I);
 Inc(I); Delete(Dir, I, Length(Dir) - Integer(I) + 1);
 AssignFile(Test, FileName);
 Reset(Test, 1);
 BlockRead(Test, ZI, 2, _xPos);
 Unic := (ZI = $FEFF);
 System.CloseFile(Test);
 JJ := 0;
 If not Unic then
 begin
  AssignFile(PFile, FileName);
  Reset(PFile);
 end Else
 begin
  WS := KruptarForm.Unicum;
  WS.Items.Clear;
  WS.Items.LoadFromFile(FileName);
 end;
 pCommand := GetCurCom(S);
 StringsListForm.StringsListBox.Items.Clear;
 If pCommand <> UpperCase(prKruptarProject) then ErrorMessage Else
 begin
  pCommand := GetCurCom(S);
  If pCommand = UpperCase(prInputROM) then
  begin
   RI := S;
   pCommand := GetCurCom(S);
   If pCommand <> UpperCase(prOutputROM) then Goto Uxit;
   RO := S;
  end ELse If pCommand = UpperCase(prOutputROM) then
  begin
   RO := S;
   pCommand := GetCurCom(S);
   If pCommand <> UpperCase(prInputROM) then Goto Uxit;
   RI := S;
  end Else
  begin Axit:
   Done;Uxit:
   ErrorMessage;
   CloseFile;
   KruptarForm.Created := False;
   KruptarForm.Saved := False;
   KruptarForm.Named := False;
   Exit;
  end;
  Redir(RI); Redir(RO);
  If not FileExists(RI) then
  begin
   ShowMessage('������������ ROM "' + RI + '" �� ������!' + #13#10 + '��������� ������.');
   If CreateNewProjectForm.OpenDialog.Execute then
   begin
    RI := CreateNewProjectForm.OpenDialog.FileName;
    bbChanged := True;
   end Else
   begin
    CloseFile;
    Exit;
   end;
  end;
  If not FileExists(RO) then
  begin
   ShowMessage('���������� ROM "' + RO + '" �� ������!' + #13#10 + '��������� ������.');
   If CreateNewProjectForm.OpenDialog.Execute then
   begin
    RO := CreateNewProjectForm.OpenDialog.FileName;
    bbChanged := True;
   end Else
   begin
    CloseFile;
    Exit;
   end;
  end;
  Init(RI, RO);
  I := Length(FileName);
  Dir := Filename;
  While (I > 0) and (Dir[I] <> '\') do Dec(I);
  Inc(I); Delete(Dir, I, Length(Dir) - Integer(I) + 1);
  ProjectFile := FileName;
  GroupsP := Addr(Groups);
  pCommand := GetCurCom(S);
  If pCommand = UpperCase(prEmulator) then
  begin
   If FileExists(S) then
    Emulator := S Else
    Emulator := '';
   pCommand := GetCurCom(S);
  end;
  While (pCommand <> UpperCase(prProjectEnd)) and not EofX do
  begin
   If pCommand = UpperCase(prDefineGroup) then
   While (pCommand <> UpperCase(prGroupEnd)) and not EofX do
   begin
    If GroupsP^.Root = NIL then New(NewC, Create(GroupsP)) Else New(NewC, Add(GroupsP));
    NewC^.GrName := S;
    pCommand := GetCurCom(S);
    If pCommand = UpperCase(prPointerUse) then
     NewC^.xNoPtr := not TrueStr(S) Else Goto Axit;
    pCommand := GetCurCom(S);
    While (pCommand <> UpperCase(prTablesStart)) and not EofX do
    begin
     If pCommand = UpperCase(prPointerSize) then
      Case s[1] of
       '2': NewC^.xPtrSize := 2;
       '3': NewC^.xPtrSize := 3;
       '4': NewC^.xPtrSize := 4;
       Else Goto Axit;
      end Else
     If pCommand = UpperCase(prInterval) then
     begin
      If UnsCheckOut(S) then NewC^.xInterval := GetLDW(S) Else Goto Axit;
     end Else
     If pCommand = UpperCase(prDivision) then
      Case s[1] of
       '1': NewC^.xKrat := 1;
       '2': NewC^.xKrat := 2;
       '4': NewC^.xKrat := 4;
       '8': NewC^.xKrat := 8;
       Else Goto Axit;
      end Else
     If pCommand = UpperCase(prType) then
      Case s[1] of
       '0': begin NewC^.xPntp := False; NewC^.xAutoStart := False end;
       '1': begin NewC^.xPntp := True;  NewC^.xAutoStart := False end;
       '2': begin NewC^.xPntp := False; NewC^.xAutoStart := True  end;
       Else Goto Axit;
      end Else
     If pCommand = UpperCase(prMotorola) then
      NewC^.xMotorola := TrueStr(S) Else
     If pCommand = UpperCase(prSigned) then
      NewC^.xSigned := TrueStr(S) Else
     If pCommand = UpperCase(prShl1) then
     begin
      NewC^.xShl1 := TrueStr(S);
      If NewC^.xShl1 and (NewC^.xKrat = 1) then NewC^.xKrat := 2;
     end Else
     If pCommand = UpperCase(prMethod) then
      Case s[1] of
       '0'..'9':
       begin
        I := StrToInt(s[1]);
        If I < MtCount then NewC^.xMethod := TPMetT(I) Else Goto Axit;
       end;
       Else Goto Axit;
      end Else
     If (pCommand = UpperCase(prDifference)) and HCheckOut(S) then
      NewC^.xFirstPtr := GetLDW(S) Else
     If pCommand = UpperCase(prCharSet) then
     begin
      If s = prUNIC then NewC^.xKodir := kdUNIC Else
      If s = prSJIS then NewC^.xKodir := kdSJIS Else
      If s = prANSI then NewC^.xKodir := kdANSI Else Goto Axit;
     end Else
     If pCommand = UpperCase(prStringLength) then
     begin
      If UnsCheckOut(S) then NewC^.xStrLen := GetLDW(S) Else Goto Axit;
     end;
     pCommand := GetCurCom(s);
    end;
    Readln(S);
    If (pCommand = UpperCase(prTablesStart)) and
       (UpperCase(S) <> UpperCase(prTablesEnd)) then
    While UpperCase(S) <> UpperCase(prTablesEnd) do
    begin
     Readln(pCommand);
     Redir(S); Redir(pCommand);
     If not FileExists(S) then
     begin
      ShowMessage('������� "' + S + '" ��� ������ "' + NewC^.GrName + '" �� �������!' + #13#10 + '��������� ������.');
      GetDir(0, SA);
      If CreateGroupForm.OpenDialog.Execute then
      begin
       ChDir(SA);
       S := CreateGroupForm.OpenDialog.FileName;
       bbChanged := True;
      end Else
      begin
       CloseFile;
       Done;
       Exit;
      end;
     end;
     If not FileExists(pCommand) then
     begin
      ShowMessage('������� "' + pCommand + '" ��� ������ "' + NewC^.GrName + '" �� �������!' + #13#10 + '��������� ������.');
      GetDir(0, SA);
      If CreateGroupForm.OpenDialog.Execute then
      begin
       ChDir(SA);
       pCommand := CreateGroupForm.OpenDialog.FileName;
       bbChanged := True;
      end Else
      begin
       CloseFile;
       Done;
       Exit;
      end;
     end;
     NewC^.OTables.Add(S, pCommand);
     Readln(S);
    end Else Goto Axit;
    pCommand := GetCurCom(S);
    If pCommand = UpperCase(prBlocksStart) then
    begin
     Readln(S);
     While UpperCase(S) <> UpperCase(prBlocksEnd) do
     begin
      If not HCheckOut(S) then Goto Axit;
      NewC^.EmptyS^[NewC^.EmptyC].a := GetLDW(S);
      Readln(S);
      If not HCheckOut(S) then Goto Axit;
      NewC^.EmptyS^[NewC^.EmptyC].b := GetLDW(S);
      Inc(NewC^.EmptyC);
      Readln(S);
     end;
     pCommand := GetCurCom(S);
    end;
    _xtn := 0;
    _xpos := 0;
    While pCommand = UpperCase(prDefineTextBlock) do
    Case NewC^.xNoPtr of
     False:
     begin
      Namae := S;
      pCommand := GetCurCom(S);
      If (pCommand = UpperCase(prUsedTableSet)) and UnsCheckOut(S) then
       _xTN := StrToInt(S) Else Goto Axit;
      pCommand := GetCurCom(S);
      If (pCommand = UpperCase(prPtrTableStart)) and UnsCheckOut(S) then
       _xPos := GetLDW(S) Else Goto Axit;
      _xCnt := 0;
      New(Karr);
      New(Karr^.PtrTable, Init);
      Karr^.PtrTable^.Pos := _xPos;
      Karr^.TabN := _xTn;
      Karr^.Next := nil;
      pCommand := GetCurCom(S);
      While (pCommand = UpperCase(prStringPointer)) and UnsCheckOut(S) do
      begin
       J := GetLDW(S);
       I := 0;
       if (NewC^.xMethod = tmC2S1eSne) then
       begin
        pCommand := GetCurCom(S);
        If (pCommand = UpperCase(prStringsCount)) and UnsCheckOut(S) then
         I := GetLDW(S);
       end;
       pCommand := GetCurCom(S);
       If pCommand = UpperCase(prStringStart) then
       begin
        RI := '';
        Readln(RO);
        While UpperCase(RO) <> UpperCase(prStringEnd) do
        begin
         RI := RI + RO;
         Readln(RO);
        end;
       end Else
       begin
        Dispose(Karr^.PtrTable, Clear);
        Dispose(Karr);
        Goto Axit;
       end;
       Karr^.PtrTable^.LoadAdd(J, RI);
       Karr^.PtrTable^.Cur^.Same := 0;
       Karr^.PtrTable^.Cur^.SinL := I;
       Inc(_xCnt);
       pCommand := GetCurCom(S);
      end;
      Karr^.PtrTable^.Name := Namae;
      Karr^.PtrTable^.BCnt := _xCnt;
      RR := NewC^.PossoR.Cur;
      NewC^.PossoR.Cur := Karr;
      if RR <> nil then PPoss(RR)^.Next := Karr;
      if NewC^.PossoR.Root = nil then NewC^.PossoR.Root := Karr;
      Inc(NewC^.PossoR.Count);
      NewC^.Posso.Add(_xTn, _xPos, Karr^.PtrTable^.BCnt);
      pCommand := GetCurCom(S);
     end;
     True:
     begin
      Namae := S;
      pCommand := GetCurCom(S);
      If (pCommand = UpperCase(prUsedTableSet)) and UnsCheckOut(S) then
       _xTN := StrToInt(S) Else Goto Axit;
      pCommand := GetCurCom(S);
      If (pCommand = UpperCase(prBlockStart)) and UnsCheckOut(S) then
       _xPos := GetLDW(S) Else Goto Axit;
      _xCnt := 0;
      If NewC^.xStrLen = 0 then
      begin
       pCommand := GetCurCom(S);
       If (pCommand = UpperCase(prBlockLength)) and UnsCheckOut(S) then
       _xCnt := GetLDW(S) Else Goto Axit;
      end;
      New(Karr);
      New(Karr^.PtrTable, Init);
      Karr^.PtrTable^.Pos := _xPos;
      Karr^.TabN := _xTn;
      Karr^.Next := nil;
      pCommand := GetCurCom(S);
      While (pCommand = UpperCase(prStringPointer)) and UnsCheckOut(S) do
      begin
       J := GetLDW(S);
       I := 0;
       if (NewC^.xMethod = tmC2S1eSne) then
       begin
        pCommand := GetCurCom(S);
        If (pCommand = UpperCase(prStringsCount)) and UnsCheckOut(S) then
         I := GetLDW(S);
       end;
       pCommand := GetCurCom(S);
       If pCommand = UpperCase(prStringStart) then
       begin
        RI := '';
        Readln(RO);
        While UpperCase(RO) <> UpperCase(prStringEnd) do
        begin
         RI := RI + RO;
         Readln(RO);
        end;
       end Else
       begin
        Dispose(Karr^.PtrTable, Clear);
        Dispose(Karr);
        Goto Axit;
       end;
       Karr^.PtrTable^.LoadAdd(J, RI);
       Karr^.PtrTable^.Cur^.Same := 0;
       Karr^.PtrTable^.Cur^.SinL := I;
       If NewC^.xStrLen > 0 then Inc(_xCnt);
       pCommand := GetCurCom(S);
      end;
      Karr^.PtrTable^.Name := Namae;
      Karr^.PtrTable^.BCnt := _xCnt;
      RR := NewC^.PossoR.Cur;
      NewC^.PossoR.Cur := Karr;
      if RR <> nil then PPoss(RR)^.Next := Karr;
      if NewC^.PossoR.Root = nil then NewC^.PossoR.Root := Karr;
      Inc(NewC^.PossoR.Count);
      NewC^.Posso.Add(_xTn, _xPos, Karr^.PtrTable^.BCnt);
      pCommand := GetCurCom(S);
     end
    end;
    Flash(NewC);
   end;
   pCommand := GetCurCom(S);
  end;
  KruptarForm.Created := True;
  KruptarForm.Saved := not bbChanged;
  KruptarForm.Named := True;
 end;
 CloseFile;
end;
{
Constructor TReadString.Init;
begin
end;

Procedure TReadString.Read(P: PCTable);
begin
end;

Destructor TReadString.Done;
begin
end;

Constructor TStringLength.Init;
begin
end;

Function TStringLength.Length(P: PCTable): Integer;
begin
end;

Destructor TStringLength.Done;
begin
end;

Constructor TWriteString.Init;
begin
end;

Procedure TWriteString.Write(P: PCTable);
begin
end;

Destructor TWriteString.Done;
begin
end;

Constructor TCodeData.Init(Own: Pointer);
begin
 Owner := Own;
 ReadString := NIL;
 WriteString := NIL;
 StringLength := NIL;
end;

Procedure TCodeData.Compile(FileName: String);
Var F: TextFile;
Function GetCurCom(Var S: String): String;
Var Ss: String; P, i, j: Integer;
begin
 Result := '';
 While (Result = '') and not Eof(F) do
 begin
  S := '';
  ReadLn(Ss);
  P := Pos('[', SS);
  If P < 1 then
  begin
   P := 1;
   While not (SS[P] in ['a'..'z']) do Inc(P);
   For I := P to Length(SS) do
    If not (SS[I] in ['a'..'z']) then
    begin
     Result := Copy(SS, p, I - P);
     Delete(ss, 1, I - 1);
     Break;
    end;
  end Else
  begin
   For I := P to Length(SS) do
    If SS[i] = ']' then
    begin
     Result := Copy(SS, p, I - p + 1);
     Delete(ss, 1, I);
     Break;
    end;
  end;
 end;
 Result := UpperCase(Result);
end;
Function GetDvR(Var S: String): String;
Var Ss: String; P, i, j: Integer;
begin
 Result := '';
 While (Result = '') and not Eof(F) do
 begin
  S := '';
  ReadLn(Ss);
  P := 1;
  While S[I] = ' ' do Inc(P);
  For I := P to Length(SS) do
   If (I + 1 <= Length(SS)) and (SS[I] = ':') and (SS[I + 1] = '=') then
   begin
    Result := Copy(SS, p, 2);
    Delete(ss, 1, 2);
    Break;
   end;
 end;
end;
Function GetOper(Var S: String): String;
Var SS: String; P, i, j: Integer;
begin
 Result := '';
 While (Result = '') and not Eof(F) do
 begin
  S := '';
  ReadLn(Ss);
  P := 1;
  While S[I] = ' ' do Inc(P);
  For I := P to Length(SS) do
   If SS[I] = '(' then
   begin
    Result := SS[I];
    Delete(ss, 1, 1);
    Break;
   end Else If not (SS[I] in ['a'..'z']) then
   begin
    Result := Copy(SS, p, I - P);
    Delete(ss, 1, I - 1);
    Break;
   end;
 end;
end;
begin
 AssignFile(F, FileName);
 Reset(F);
 With PGroup(Owner)^ do
 begin
 end;
 CloseFile(F);
end;

Destructor TCodeData.Done;
begin
 Owner := NIL;
 If ReadString <> NIL then Dispose(ReadString, Done);
 If WriteString <> NIL then Dispose(WriteString, Done);
 If StringLength <> NIL then Dispose(StringLength, Done);
end;

Constructor TVariables.Init;
begin
 Root := NIL;
 Cur := NIL;
 Count := 0;
end;

Const
 DS: Array[TVarType] of DWord = (0, 1, 2, 4, 4);

Function TVariables.Add(Sz: DWord; VT: TVarType): PVariable;
begin
 New(Result);
 If Root = NIL then
 begin
  Root := Result;
  Cur := Result;
 end Else
 begin
  Cur^.Next := Result;
  Cur := Result;
 end;
 Inc(Count);
 With Result^ do
 begin
  Size := Sz;
  If Size > 0 then GetMem(Data, Size * DS[VT]) Else Data := NIL;
  VarType := VT;
  Next := NIL;
 end;
end;

Destructor TVariables.Done;
Var N: PVariable;
begin
 While Root <> NIL do
 begin
  N := Root^.Next;
  With Root^ do If Data <> NIL then FreeMem(Data);
  Dispose(Root);
  Root := N;
 end;
 Cur := NIL;
 Count := 0;
end;  }

end.

