unit hextexu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UnicodeEdit, UnicodeMemo;

type
  THexTextForm = class(TForm)
    Memo: TUnicodeMemo;
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MemoChange(Sender: TObject);
    procedure MemoKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HexTextForm: THexTextForm;

implementation

uses KruptarMain, HexEdit, ProjItems, KruptarProjectUnit, ComCtrls, UniConv,
     Japan;

{$R *.dfm}

procedure THexTextForm.FormResize(Sender: TObject);
begin
 Memo.Width := ClientWidth;
 Memo.Height := ClientHeight;
end;

procedure THexTextForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 KruptarForm.HexEditem.Checked := False;
 HexEditForm.Visible := False;
end;

procedure THexTextForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
end;

procedure THexTextForm.MemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
 If (UpCase(Char(Key)) = 'V') and (ssCtrl in Shift) then Key := 0 Else
 If Key = VK_RETURN then Key := VK_DOWN Else
 If (Key = VK_INSERT) and (ssShift in Shift) then Key := 0 Else
 If ((Memo.CaretPos.X = 0) or
 (Length(Memo.Lines.Strings[Memo.CaretPos.y]) = 1)) and (Key = VK_BACK) then
  Key := VK_LEFT Else
 If (Length(Memo.Lines.Strings[Memo.CaretPos.Y]) = 1) or
   (Memo.CaretPos.X = Length(Memo.Lines.Strings[Memo.CaretPos.Y])) and
    (Key = VK_DELETE) then Key := VK_RIGHT Else
 If ((Key = VK_DELETE) or (Key = VK_BACK)) and (Pos(#13, Memo.SelWideText) > 0) then Key := 0 Else
end;

Var KeyPresss: Boolean;

procedure THexTextForm.MemoChange(Sender: TObject);
Var
 A: ^TROMData; O, I, K: Integer; L: DWord; LCN: Word; QUADRO: Pointer;
Type
 kk = record a, b, c, d: byte end;
Procedure AddString(tn: Integer; ss: String);
Var sd: String; w: Integer; aa: ^TROMData;
begin
 With PGroup(QUADRO)^ do
 begin
  aa := addr(a^[0]);
  i := 1; k := 0; lcn := 0;
  repeat
   sd := '';
   for w := 0 to ChMaxLen[tn] - 1 do
   begin
    If i + w > length(ss) then break;
    sd := sd + ss[i + w];
   end;
   o := 1;
   inc(i, length(sd) + 1);
   repeat
    l := antitable[sd, tn];
    If Endsym then Inc(lcn);
    SetLength(sd, length(sd) - 1);
    Dec(i);
    if (l = $FFFFFFFF) and (sd = '') then
    begin
     l := antitable[' ', tn];
     if l = $FFFFFFFF then l := 0;
     break;
    end;
   until l <> $FFFFFFFF;
   if tadlen = 1 then aa^[k] := kk(l).a Else if tadlen = 2 then
   begin
    o := 2;
    aa^[k    ] := kk(l).b;
    aa^[k + 1] := kk(l).a;
   end Else if tadlen = 3 then
   begin
    aa^[k    ] := kk(l).c;
    aa^[k + 1] := kk(l).b;
    aa^[k + 2] := kk(l).a;
    o := 3;
   end Else
   begin
    aa^[k    ] := kk(l).d;
    aa^[k + 1] := kk(l).c;
    aa^[k + 2] := kk(l).b;
    aa^[k + 3] := kk(l).a;
    o := 4;
   end;
   inc(k, o);
  until i > length(ss);
 end;
end;
Var U: TTreeNode; S: String;
begin
 If not KeyPresss then Exit;
 KeyPresss := False;
 If Memo.ReadOnly then Exit;
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 Need := True;
 If U.Level = 1 then U := U.Parent;
 QUADRO := U.Data;
 If PGroup(QUADRO)^.xKodir = kdANSI then
  S := Memo.Lines.Strings[Memo.CaretPos.Y] Else
  S := SJIStext(Memo.Lines.Strings[Memo.CaretPos.Y]);
 A := Addr(KruptarForm.KruptarProject.ROME^[HexEditForm.Position + DWord(Memo.CaretPos.Y * HexEditForm.ColCount)]);
 AddString(0, S);
 HexEditForm.RefreshHex;
 CCHANGED := True; 
 Need := False;
end;

Var KeyStr: String;

procedure THexTextForm.MemoKeyPress(Sender: TObject; var Key: Char);
Function NotFound(S: String): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Pos(KeyStr, Romaji[I]) < 1) do Inc(I);
 Result := I = KanaC + 1;
end;
Function NotFoundC(C: Char): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Romaji[I][1] <> C) do Inc(I);
 Result := I = KanaC + 1;
end;
Function DownCase(Ch: Char ): Char;
begin
 Result := Ch;
 Case Result of
  'A'..'Z': Inc(Result, Ord('a') - Ord('A'));
 end;
end;
Var I: Integer; S: WideString;
begin
 KeyPresss := True;
 S := Memo.SelWideText;
 If Pos(#13, S) > 0 then Delete(S, Pos(#13, S), Length(S) - Pos(#13, S) + 1);
 Memo.SelLength := Length(S);
 If (Key <> #8) and
  KruptarForm.OptionsKanjiItem.Enabled and
  KruptarForm.OptionsKanjiItem.Checked then
 begin
  If (KeyStr <> '') then
  begin
   If KeyStr[1] in LSoGL then DownCase(Key) Else
   If KeyStr[1] in BSoGL then UpCase(Key) Else
   begin
    KeyStr := '';
    Key := #0;
    Exit;
   end;
  end;
  If (KeyStr = '') and not (Key in LSogL) and not (Key in BSogL) and NotFoundC(Key)then
  begin
   Key := #0;
   Exit;
  end;
  KeyStr := KeyStr + Key;
  Key := #0;
  If (Length(KeyStr) = 2) and not(KeyStr[2] in ['+', '*']) and
     (UpCase(KeyStr[2]) = UpCase(KeyStr[1])) and
     (UpCase(KeyStr[2]) <> 'N') then
  begin
   If (KeyStr[1] in BSogL) and (UpCase(KeyStr[2]) = KeyStr[1]) then
   begin
    Memo.SelWideText := UniText('�b');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   If (KeyStr[1] in LSogL) and (DownCase(KeyStr[2]) = KeyStr[1]) then
   begin
    Memo.SelWideText := UniText('��');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   begin
    KeyStr := '';
    Exit;
   end;
  end;
  If NotFound(KeyStr) then
  begin
   KeyStr := '';
   Exit;
  end;
  I := 0;
  While (I <= KanaC) and (KeyStr <> Romaji[I]) do Inc(I);
  If I <= KanaC then
  begin
   Memo.SelWideText := UniText(Kana[I]);
   KeyStr := '';
  end;
 end;
end;


end.
