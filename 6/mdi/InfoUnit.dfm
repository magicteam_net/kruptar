object InfoForm: TInfoForm
  Left = 448
  Top = 298
  AutoSize = True
  BorderStyle = bsDialog
  Caption = 'InfoForm'
  ClientHeight = 201
  ClientWidth = 249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object InfoMemo: TMemo
    Left = 0
    Top = 0
    Width = 249
    Height = 201
    Color = clInactiveBorder
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    OnKeyDown = InfoMemoKeyDown
  end
end
