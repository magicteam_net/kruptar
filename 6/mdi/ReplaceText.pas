unit ReplaceText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TntStdCtrls;

type
  TReplaceTextDialog = class(TForm)
    SearchText: TTntComboBox;
    SearchLabel: TLabel;
    OptionsBox: TGroupBox;
    CaseCheckBox: TCheckBox;
    OkButton: TButton;
    CancelButton: TButton;
    ReplaceText: TTntComboBox;
    ReplaceLabel: TLabel;
    PromtCheckBox: TCheckBox;
    ReplaceAllButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ReplaceAllButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
   SearchString: WideString;
   ReplaceString: WideString;
   Function Execute: Boolean;
  end;

var
  ReplaceTextDialog: TReplaceTextDialog;
  OkPressed: Boolean;

implementation

Uses SearchText;

{$R *.dfm}

Function TReplaceTextDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult in [mrOk, mrYesToAll];
 If Result then
 begin
  SearchString := SearchText.Text;
  ReplaceString := ReplaceText.Text;
  SearchText.Items.Add(SearchString);
  ReplaceText.Items.Add(ReplaceString);
  SearchTextDialog.SearchText.Items := SearchText.Items;
 end Else
 begin
  SearchString := '';
  ReplaceString := '';
 end;
end;

procedure TReplaceTextDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 SearchText.SetFocus;
end;

procedure TReplaceTextDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TReplaceTextDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If OkPressed then
 begin
  If SearchText.Text = '' then
  begin
   ShowMessage('������� ������ ��� ������.');
   CanClose := False;
   OkPressed := False;
  end;
 end;
end;

procedure TReplaceTextDialog.ReplaceAllButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

end.
