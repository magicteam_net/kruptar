unit FRep;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, ComCtrls, UnicodeEdit;

type
  TFindReplaceForm = class(TForm)
    OkButton: TButton;
    CancelBtn: TButton;
    PageControl: TPageControl;
    FindTab: TTabSheet;
    ReplaceTab: TTabSheet;
    FEdit: TUnicodeEdit;
    FindLabel: TLabel;
    FindLabelR: TLabel;
    RFEdit: TUnicodeEdit;
    ReplaceLabel: TLabel;
    REdit: TUnicodeEdit;
    ConfirmReplaceCheck: TCheckBox;
    JapTypeItem: TCheckBox;
    ReplaceAllCheck: TCheckBox;
    StatusBar: TStatusBar;
    Timer: TTimer;
    procedure FEditKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RFEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure RFEditEnter(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure RFEditMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    Function Execute: Boolean;
  end;

var
  FindReplaceForm: TFindReplaceForm;

implementation

{$R *.DFM}

Uses Uniconv, Japan;

Var Memo: TUnicodeEdit;
    KeyStr: String;
    InsPressed: Boolean;

procedure TFindReplaceForm.FEditKeyPress(Sender: TObject; var Key: Char);
Function NotFound(S: String): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Pos(S, Romaji[I]) < 1) do Inc(I);
 Result := I = KanaC + 1;
end;
Function NotFoundC(C: Char): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Romaji[I][1] <> C) do Inc(I);
 Result := I = KanaC + 1;
end;
Function DownCase(Ch: Char ): Char;
begin
 Result := ch;
 Case Result of
  'A'..'Z': Inc(Result, Ord('a') - Ord('A'));
 end;
end;
Var I: Integer;
begin
 If JapTypeItem.Checked then
  Case TUnicodeEdit(Sender).Tag of
   0: Memo := FEdit;
   1: Memo := RFEdit;
   2: Memo := REdit;
  end;
 If JapTypeItem.Checked and not (Key in [#13, #10, #8]) then
 begin
  If (KeyStr <> '') then
  begin
   If KeyStr[1] in LSoGL then DownCase(Key) Else
   If KeyStr[1] in BSoGL then UpCase(Key) Else
   begin
    KeyStr := '';
    Key := #0;
    Exit;
   end;
  end;
  If (KeyStr = '') and not (Key in LSogL) and not (Key in BSogL) and NotFoundC(Key)then
  begin
   Key := #0;
   Exit;
  end;
  KeyStr := KeyStr + Key;
  Key := #0;
  If (Length(KeyStr) = 2) and not(KeyStr[2] in ['+', '*']) and
     (UpCase(KeyStr[2]) = UpCase(KeyStr[1])) and
     (UpCase(KeyStr[2]) <> 'N') then
  begin
   If (KeyStr[1] in BSogL) and (UpCase(KeyStr[2]) = KeyStr[1]) then
   begin
    If InsPressed then Memo.SelLength := 1;
    Memo.SelWideText := UniText('�b');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   If (KeyStr[1] in LSogL) and (DownCase(KeyStr[2]) = KeyStr[1]) then
   begin
    If InsPressed then Memo.SelLength := 1;
    Memo.SelWideText := UniText('��');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   begin
    KeyStr := '';
    Exit;
   end;
  end;
  If NotFound(KeyStr) then
  begin
   KeyStr := '';
   Exit;
  end;
  I := 0;
  While (I <= KanaC) and (KeyStr <> Romaji[I]) do Inc(I);
  If I <= KanaC then
  begin
   If InsPressed then Memo.SelLength := Length(Kana[I]) div 2;
   Memo.SelWideText := UniText(Kana[I]);
   KeyStr := '';
  end;
 end;
end;

procedure TFindReplaceForm.FormShow(Sender: TObject);
begin
 if PageControl.ActivePage = FindTab then FEdit.SetFocus Else RFEdit.SetFocus;
end;

procedure TFindReplaceForm.RFEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if ssCtrl in Shift then
 begin
  If (upcase(char(key)) = 'J') then
   JapTypeItem.Checked := not JapTypeItem.Checked;
  KeyStr := '';
 end;
 Case Key of
  VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_HOME, VK_END,
  VK_DELETE, VK_ESCAPE, VK_NEXT, VK_PRIOR, VK_BACK, VK_RETURN:
   KeyStr := ''
  Else IF Key = VK_INSERT then InsPressed := not InsPressed;
 end;
end;

procedure TFindReplaceForm.FormActivate(Sender: TObject);
begin
 if FindReplaceForm.PageControl.ActivePageIndex = 0 then
  FindReplaceForm.FEdit.SetFocus Else
 FindReplaceForm.RFEdit.SetFocus;
end;

procedure TFindReplaceForm.RFEditEnter(Sender: TObject);
begin
 TUnicodeEdit(Sender).SelectAll;
end;

procedure TFindReplaceForm.TimerTimer(Sender: TObject);
begin
 StatusBar.Panels[2].Text := KeyStr;
 If InsPressed then
  StatusBar.Panels[0].Text := '������' Else
  StatusBar.Panels[0].Text := '�������';
end;

procedure TFindReplaceForm.RFEditMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 With TUnicodeEdit(Sender) do
  If not Focused then SelectAll;
end;

Function TFindReplaceForm.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

end.
