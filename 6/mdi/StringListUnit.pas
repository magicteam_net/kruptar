unit StringListUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, UnicodeLists, ComCtrls, StdCtrls, Menus, ActnList;

type
  TStringsListForm = class(TForm)
    StringsListBox: TUnicodeListBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StringsListBoxClick(Sender: TObject);
    procedure StringsListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StringsListForm: TStringsListForm;

implementation

{$R *.dfm}

Uses KruptarMain, Originaledit, NewTextEdit, ProjItems, KruptarProjectUnit;

procedure TStringsListForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 KruptarForm.StringListWinItem.Checked := False;
end;

procedure TStringsListForm.StringsListBoxClick(Sender: TObject);
var U: TTreeNode; LstTitle: String; P, Q, R, DORK: Pointer;
    I, J, DORKi: Integer;
Label EndL;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then
 begin
  Caption := ListTitle;
  Exit;
 end;
 If U.Level = 1 then U := U.Parent;
 LstTitle := ListTitle + ' - [' + PGroup(U.Data)^.GrName + ']';
 With StringsListBox do If ItemIndex >= 0 then
  Caption := LstTitle + ' ' + IntToStr(ItemIndex + 1) + '/' + IntToStr(Items.Count)
 Else Caption := LstTitle;
 KruptarForm.RefreshOriginal;
 KruptarForm.RefreshNew;
 I := StringsListBox.ItemIndex;
 P := U.Data;
 J := 0;
 While P <> NIL do With PGroup(P)^ do
 begin
  Q := Posso.Root;
  While Q <> NIL do With PPoss(Q)^ do
  begin
   R := PtrTable^.Root;
   DORK := R; DORKi := 0;
   While R <> NIL do With PtTable(R)^ do
   begin
    If J = I then
    begin
     Q := R;
     While DORKi > 0 do
     begin
      Dec(I);
      Dec(DORKi);
     end;
     While (Q <> NIL) and (PtTable(R)^.TPtr = PtTable(Q)^.TPtr) do
     begin
      Inc(J);
      Q := PtTable(Q)^.Next;
     end;
     Goto EndL;
    end;
    Inc(J);
    R := Next;
    If (R <> NIL) and (PtTable(DORK)^.TPtr = PtTable(R)^.TPtr) then Inc(DORKi) Else
    begin
     DORKi := 0;
     DORK := R;
    end;
   end;
   Q := Next;
  end;
  P := Next;
 end;
EndL:
 If J = 0 then Exit;
 StringsListBox.SelectionModel.ClearSelection;
 StringsListBox.SelectionModel.AddSelectionInterval(J - 1, I);
end;

procedure TStringsListForm.StringsListBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
 If (ssShift in Shift) and ((Key = VK_UP) or (Key = VK_DOWN) or
                            (Key = VK_PRIOR) or (Key = VK_NEXT)) then
  Key := 0;
 If (ssCtrl in Shift) and ((Key = VK_PRIOR) or (Key = VK_NEXT) or
          (Key = VK_HOME) or (Key = VK_END)) then
  StringsListBox.SelectionModel.ClearSelection;
end;

end.
