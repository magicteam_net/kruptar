unit ElParama;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Dialogs, HexUnit;

type
  TElParamsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    BlockEndEdit: TLabeledEdit;
    BlockStartEdit: TLabeledEdit;
    AddBlockButton: TSpeedButton;
    BlocksList: TListBox;
    MoveUpButton: TSpeedButton;
    MoveDownButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    BlockLabel: TLabel;
    Bevel2: TBevel;
    OpenOutputButton: TSpeedButton;
    OpenInputButton: TSpeedButton;
    InputTableEdit: TLabeledEdit;
    OutputTableEdit: TLabeledEdit;
    OpenDialog: TOpenDialog;
    procedure OpenInputButtonClick(Sender: TObject);
    procedure OpenOutputButtonClick(Sender: TObject);
    procedure AddBlockButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElParamsDialog: TElParamsDialog;
  bOkButton, bCancelButton: Boolean;

implementation

{$R *.dfm}

procedure TElParamsDialog.OpenInputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  InputTableEdit.Text := OpenDialog.FileName;
end;

procedure TElParamsDialog.OpenOutputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  OutputTableEdit.Text := OpenDialog.FileName;
end;

procedure TElParamsDialog.AddBlockButtonClick(Sender: TObject);
begin
 If HCheckOut(BlockStartEdit.Text) and HCheckOut(BlockEndEdit.Text) and
    (GetLDW(BlockStartEdit.Text) >= 0) and (GetLDW(BlockEndEdit.Text) >= 0) then
 begin
  BlocksList.Items.Add(BlockStartEdit.Text);
  BlocksList.Items.Add(BlockEndEdit.Text);
 end Else ShowMessage('���� ��� ��� �������� �������� ������� ��� ������ ����!');
end;

procedure TElParamsDialog.MoveUpButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex > 0 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex - 1;
 end;  
end;

procedure TElParamsDialog.MoveDownButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex < BlocksList.Items.Count - 1 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex + 1;
 end;
end;

procedure TElParamsDialog.DeleteButtonClick(Sender: TObject);
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex mod 2 = 1 then
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex - 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end Else
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex + 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end;
end;

procedure TElParamsDialog.OKBtnClick(Sender: TObject);
begin
 bokbutton := true;
end;

procedure TElParamsDialog.CancelBtnClick(Sender: TObject);
begin
 bCancelButton := true;
end;

procedure TElParamsDialog.FormShow(Sender: TObject);
begin
 InputTableEdit.SetFocus;
 BlockStartEdit.Text := 'h00000000';
 BlockEndEdit.Text := 'h00000000';
 bCancelButton := false;
 bokbutton := false;
end;

procedure TElParamsDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If not FileExists(InputTableEdit.Text) then
 begin
  ShowMessage('���� "' + InputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not FileExists(OutputTableEdit.Text) then
 begin
  ShowMessage('���� "' + OutputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end;
end;

end.
