object OriginalTextEditForm: TOriginalTextEditForm
  Left = 188
  Top = 489
  Width = 429
  Height = 300
  VertScrollBar.Visible = False
  BorderStyle = bsSizeToolWin
  Caption = #1054#1088#1080#1075#1080#1085#1072#1083#1100#1085#1099#1081' '#1090#1077#1082#1089#1090
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo: TUnicodeMemo
    Left = 0
    Top = 0
    Width = 421
    Height = 251
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    OnKeyDown = MemoKeyDown
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 251
    Width = 421
    Height = 19
    Hint = #1065#1077#1083#1095#1086#1082' '#1083#1077#1074#1086#1081' '#1082#1085#1086#1087#1082#1086#1081' '#1084#1099#1096#1080' '#1082#1086#1087#1080#1088#1091#1077#1090' '#1072#1076#1088#1077#1089' '#1089#1090#1088#1086#1082#1080' '#1074' '#1073#1091#1092#1092#1077#1088
    Panels = <
      item
        Width = 70
      end
      item
        Width = 60
      end
      item
        Width = 60
      end
      item
        Width = 50
      end>
    SimplePanel = False
    SizeGrip = False
    OnClick = StatusBarClick
  end
  object Timer: TTimer
    Interval = 10
    OnTimer = TimerTimer
    Left = 448
  end
  object ActionList: TActionList
    Left = 360
    object EditCopy: TEditCopy
      Category = 'Edit'
      Caption = '&Copy'
      ImageIndex = 1
      ShortCut = 16451
    end
    object EditSelectAll: TEditSelectAll
      Category = 'Edit'
      Caption = 'Select &All'
      ShortCut = 16449
    end
  end
  object PopupMenu: TPopupMenu
    Left = 384
    object CopyI: TMenuItem
      Action = EditCopy
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SelectAll: TMenuItem
      Action = EditSelectAll
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
    end
  end
end
