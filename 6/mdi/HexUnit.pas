unit Hexunit;

interface

Uses Windows, SysUtils;

Function HexVal(s: string): DWord;
Function GetLDW(s: string): LongInt;
Function HCheckOut(s: string): Boolean;
Function UnsCheckOut(s: string): Boolean;

Var HexError: Boolean;

implementation

Function GetLDW(s: string): LongInt;
begin
 if s[1] in ['h', 'H'] then
 begin
  Delete(s, 1, 1);
  Result := Hexval(s);
 end Else Result := StrToInt(s);
end;

Function HexVal(s: string): DWord;
Function sign(h: Char): Byte;
begin
 if h in ['0'..'9'] then Result := Byte(h) - 48 Else
 if h in ['A'..'F'] then Result := Byte(h) - 55 Else
                         Result := Byte(h) - 87;
end;
Const hs: Array[1..8] of DWord = ($1,
                                  $10,
                                  $100,
                                  $1000,
                                  $10000,
                                  $100000,
                                  $1000000,
                                  $10000000);
Var i: Byte; Label Error;
begin
 hexerror := true;
 Result := 0;
 If Length(s) <= 8 then
 begin
  For i := 1 to Length(s) do If not (s[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then Goto Error;
  Result := 0;
  for i := 1 to Length(s) do Inc(Result, sign(s[i]) * hs[length(s) + 1 - i]);
  hexerror := false;
 end Else
 begin Error:
  hexerror := true;
 end;
end;

Function HCheckOut(s: string): Boolean;
Var i: integer;
begin
 Result := False;
 if s = '' then exit;
 Result := True;
 if s[1] in ['h', 'H'] then
 begin
  Delete(s, 1, 1);
  if length(s) > 8 then
  begin
   Result := false;
   Exit;
  end;
  For i := 1 to Length(s) do
   If not (s[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then
   begin
    Result := False;
    Exit;
   end;
 end Else
 begin
  If s[1] = '-' then
   Delete(s, 1, 1);
  If Length(s) > 10 then
  begin
   Result := false;
   Exit;
  end;
  For i := 1 to Length(s) do
   If not (s[i] in ['0'..'9']) then
   begin
    Result := False;
    Exit;
   end;
 end;
end;

Function UnsCheckOut(s: string): Boolean;
Var i: integer;
begin
 Result := False;
 if s = '' then exit;
 Result := True;
 if s[1] in ['h', 'H'] then
 begin
  Delete(s, 1, 1);
  For i := 1 to Length(s) do
   If not (s[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then
   begin
    Result := False;
    Exit;
   end;
 end Else
 begin
  For i := 1 to Length(s) do
   If not (s[i] in ['0'..'9']) then
   begin
    Result := False;
    Exit;
   end;
 end;
end;

end.
