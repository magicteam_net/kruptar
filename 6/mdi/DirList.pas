unit DirList;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Grids, DirOutln, FileCtrl;

type
  TDirectoryListDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    DriveComboBox: TDriveComboBox;
    DirectoryListBox: TDirectoryListBox;
  private
    { Private declarations }
  public
    Function Execute: Boolean;
    Function Dirname: String;
  end;

var
  DirectoryListDialog: TDirectoryListDialog;

implementation

{$R *.dfm}

Function TDirectoryListDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
end;

Function TDirectoryListDialog.Dirname: String;
begin
 Result := DirectoryListBox.Directory;
end;

end.
