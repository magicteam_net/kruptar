unit Recalc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Gauges, ComCtrls;

type
  TRecalcForm = class(TForm)
    Gauge: TGauge;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    Procedure SetRC(I: Integer);
  public
    Property RCounter: Integer Write SetRC;
  end;

var
  RecalcForm: TRecalcForm;
  RecalcU: TTreeNode;
  RecalcCount: Integer;
  RCount: Integer;
  RecalcI: Integer;
  cc: Boolean;  

implementation

{$R *.dfm}

Procedure TRecalcForm.SetRC(I: Integer);
begin
 RCount := I;
 Gauge.Progress := RCount;
end;

procedure TRecalcForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 CanClose := CC;
end;

end.
