object HexEditForm: THexEditForm
  Left = 430
  Top = 196
  Width = 281
  Height = 336
  BorderStyle = bsSizeToolWin
  Caption = 'HexEditor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Pages: TPageControl
    Left = 0
    Top = 0
    Width = 273
    Height = 289
    ActivePage = Original
    TabIndex = 0
    TabOrder = 0
    OnChange = PagesChange
    object Original: TTabSheet
      Caption = 'Input ROM'
      object HexGridO: TStringGrid
        Left = 4
        Top = 2
        Width = 256
        Height = 256
        BorderStyle = bsNone
        Color = clTeal
        ColCount = 16
        DefaultColWidth = 16
        DefaultRowHeight = 16
        FixedCols = 0
        RowCount = 16
        FixedRows = 0
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clMoneyGreen
        Font.Height = -9
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        GridLineWidth = 0
        Options = [goDrawFocusSelected]
        ParentFont = False
        ScrollBars = ssNone
        TabOrder = 0
        OnKeyDown = HexGridOKeyDown
      end
    end
    object EditMode: TTabSheet
      Caption = 'Output ROM'
      ImageIndex = 1
      object HexGridE: TStringGrid
        Left = 4
        Top = 2
        Width = 256
        Height = 256
        BorderStyle = bsNone
        Color = clTeal
        ColCount = 16
        DefaultColWidth = 16
        DefaultRowHeight = 16
        FixedCols = 0
        RowCount = 16
        FixedRows = 0
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clMoneyGreen
        Font.Height = -9
        Font.Name = 'Small Fonts'
        Font.Style = [fsBold]
        GridLineWidth = 0
        Options = [goDrawFocusSelected]
        ParentFont = False
        ScrollBars = ssNone
        TabOrder = 0
        OnKeyDown = HexGridEKeyDown
        OnKeyPress = HexGridEKeyPress
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 290
    Width = 273
    Height = 19
    Panels = <
      item
        Width = 66
      end
      item
        Width = 66
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    SimplePanel = False
    SizeGrip = False
  end
  object Timer: TTimer
    Interval = 10
    OnTimer = TimerTimer
    Left = 200
  end
  object PopupMenu: TPopupMenu
    Left = 232
    object GotoN: TMenuItem
      Caption = #1055#1077#1088#1077#1093#1086#1076'...'
      OnClick = GotoNClick
    end
  end
end
