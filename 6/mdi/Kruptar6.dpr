program Kruptar6;

uses
  Forms,
  KruptarMain in 'KruptarMain.pas' {KruptarForm},
  about in 'about.pas' {AboutBox},
  NewProject in 'NewProject.pas' {CreateNewProjectForm},
  ProjItems in 'ProjItems.pas' {ProjectItemsForm},
  KruptarProjectUnit in 'KruptarProjectUnit.pas',
  CreateGroupUnit in 'CreateGroupUnit.pas' {CreateGroupForm},
  CreateGroupUnitU in 'CreateGroupUnitU.pas' {CreateGroupFormU},
  NewItemUnit in 'NewItemUnit.pas' {NewItemForm},
  StringListUnit in 'StringListUnit.pas' {StringsListForm},
  ElParama in 'ElParama.pas' {ElParamsDialog},
  Originaledit in 'Originaledit.pas' {OriginalTextEditForm},
  NewTextEdit in 'NewTextEdit.pas' {NewTextForm},
  Recalc in 'Recalc.pas' {RecalcForm},
  InfoUnit in 'InfoUnit.pas' {InfoForm},
  bufform in 'bufform.pas' {Form1},
  FRep in 'FRep.pas' {FindReplaceForm},
  dict in 'dict.pas' {DictForm},
  process in 'process.pas' {procform},
  Hexedit in 'Hexedit.pas' {HexEditForm},
  hextexu in 'hextexu.pas' {HexTextForm},
  SelectPtrForm in 'SelectPtrForm.pas' {SelectPtrDlg},
  GotoForm in 'GotoForm.pas' {OKBottomDlg};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Kruptar 6.0.6.2';
  Application.CreateForm(TKruptarForm, KruptarForm);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TCreateNewProjectForm, CreateNewProjectForm);
  Application.CreateForm(TProjectItemsForm, ProjectItemsForm);
  Application.CreateForm(TCreateGroupForm, CreateGroupForm);
  Application.CreateForm(TCreateGroupFormU, CreateGroupFormU);
  Application.CreateForm(TNewItemForm, NewItemForm);
  Application.CreateForm(TStringsListForm, StringsListForm);
  Application.CreateForm(TElParamsDialog, ElParamsDialog);
  Application.CreateForm(TOriginalTextEditForm, OriginalTextEditForm);
  Application.CreateForm(TNewTextForm, NewTextForm);
  Application.CreateForm(TRecalcForm, RecalcForm);
  Application.CreateForm(TInfoForm, InfoForm);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TFindReplaceForm, FindReplaceForm);
  Application.CreateForm(TDictForm, DictForm);
  Application.CreateForm(Tprocform, procform);
  Application.CreateForm(THexEditForm, HexEditForm);
  Application.CreateForm(THexTextForm, HexTextForm);
  Application.CreateForm(TSelectPtrDlg, SelectPtrDlg);
  Application.CreateForm(TOKBottomDlg, OKBottomDlg);
  Application.Run;
end.
