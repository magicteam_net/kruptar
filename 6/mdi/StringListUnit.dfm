object StringsListForm: TStringsListForm
  Left = 297
  Top = 242
  Width = 505
  Height = 292
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsSizeToolWin
  Caption = #1057#1087#1080#1089#1086#1082' '#1090#1077#1082#1089#1090#1086#1074#1099#1093' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object StringsListBox: TUnicodeListBox
    Left = 0
    Top = 0
    Width = 497
    Height = 262
    ItemHeight = 16
    MultiSelect = True
    Alignment = taLeftJustify
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
    OnClick = StringsListBoxClick
    OnKeyDown = StringsListBoxKeyDown
  end
end
