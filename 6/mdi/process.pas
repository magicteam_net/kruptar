unit process;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, ExtCtrls, StdCtrls, Psock, NMHttp;

type
  Tprocform = class(TForm)
    Gauge1: TGauge;
    imptimer: TTimer;
    Button1: TButton;
    downl: TNMHTTP;
    Label1: TLabel;
    procedure imptimerTimer(Sender: TObject);
    procedure downlPacketRecvd(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure downlSuccess(Cmd: CmdType);
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  procform: Tprocform; var impprogr: integer; faname: string; u: boolean;

implementation

{$R *.dfm}

Uses KruptarMain;

procedure Tprocform.imptimerTimer(Sender: TObject);
begin
 ImpProgr := Gauge1.Progress;
end;

procedure Tprocform.DownlPacketRecvd(Sender: TObject);
begin
 Gauge1.Progress := Round(downl.BytesRecvd*100/downl.BytesTotal) ;
 Label1.Caption := '�������� ' + IntToStr(downl.BytesRecvd) + ' ���� �� ' +
                    IntToStr(downl.BytesTotal) + '.';
end;

Procedure Tprocform.FormActivate(Sender: TObject);
begin
 U := false;
 Gauge1.Progress := 0;
 Label1.Caption := '';
 Button1.Enabled := false;
 Downl.Body := KruptarForm.SaveDialog.Filename;
 Downl.Get('http://djinn.avs.ru/progs/' + faname);
 Downl.Disconnect;
 Close;
end;

procedure Tprocform.DownlSuccess(Cmd: CmdType);
begin
 If downl.BytesRecvd = downl.BytesTotal then
 begin
  u := true;
  Label1.Caption := Label1.Caption + ' ������!';
  button1.Enabled := true;
 end;
end;

procedure Tprocform.Button1Click(Sender: TObject);
begin
 Close;
end;

procedure Tprocform.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 CanClose := button1.enabled;
end;

end.
