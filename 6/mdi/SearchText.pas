unit SearchText;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TntStdCtrls;

type
  TSearchTextDialog = class(TForm)
    SearchText: TTntComboBox;
    SearchLabel: TLabel;
    OptionsBox: TGroupBox;
    CaseCheckBox: TCheckBox;
    WholeCheckBox: TCheckBox;
    OkButton: TButton;
    CancelButton: TButton;
    procedure FormShow(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
   SearchString: WideString;
   Function Execute: Boolean;
  end;

var
  SearchTextDialog: TSearchTextDialog;
  OkPressed: Boolean;

implementation

Uses ReplaceText;

{$R *.dfm}

Function TSearchTextDialog.Execute: Boolean;
begin
 ShowModal;
 Result := ModalResult = mrOk;
 If Result then
 begin
  SearchString := SearchText.Text;
  SearchText.Items.Add(SearchString);
  ReplaceTextDialog.SearchText.Items := SearchText.Items;
 end Else SearchString := ''; 
end;

procedure TSearchTextDialog.FormShow(Sender: TObject);
begin
 OkPressed := False;
 SearchText.SetFocus;
end;

procedure TSearchTextDialog.OkButtonClick(Sender: TObject);
begin
 OkPressed := True;
end;

procedure TSearchTextDialog.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If OkPressed then
 begin
  If SearchText.Text = '' then
  begin
   ShowMessage('������� ������ ��� ������.');
   CanClose := False;
   OkPressed := False;
  end;
 end;
end;

end.
