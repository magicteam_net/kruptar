unit ProjItems;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons;

type
  TProjectItemsForm = class(TForm)
    TreeView: TTreeView;
    procedure AddGroupButtonClick(Sender: TObject);
    procedure TreeViewEdited(Sender: TObject; Node: TTreeNode;
      var S: String);
    procedure AddItemButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure TreeViewChange(Sender: TObject; Node: TTreeNode);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DeleteItemButtonClick(Sender: TObject);
    procedure ElParamsButtonClick(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
    procedure TreeViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TreeViewKeyPress(Sender: TObject; var Key: Char);
    Procedure EnableButtons;

  end;

var
  ProjectItemsForm: TProjectItemsForm;
  xI: Integer;

implementation

uses CreateGroupUnit, CreateGroupUnitU, KruptarMain, KruptarProjectUnit, HexUnit,
  NewItemUnit, ElParama, StringListUnit, SelectPtrForm;

{$R *.dfm}

procedure TProjectItemsForm.AddGroupButtonClick(Sender: TObject);
var Tmp: ^TGroups; NewC: PGroup; i: integer;
begin
 If SelectPtrDlg.Execute then Case SelectPtrDlg.Radio.ItemIndex of
  0:
  begin
   CreateGroupForm.ShowModal;
   If CreateGroupForm.ModalResult = mrOk then
   begin
    Tmp := Addr(KruptarForm.KruptarProject.Groups);
    If Tmp^.Root = NIL then
     New(NewC, Create(Tmp))
    Else
     New(NewC, Add(Tmp));
    NewC^.GrName := 'Group' + IntTostr(Tmp^.Count);
    With CreateGroupForm do
    begin
     NewC^.xPtrSize := PointerSizeRadio.ItemIndex + 2;
     Case DivisionRadio.ItemIndex of
        0: NewC^.xKrat := 1;
        1: NewC^.xKrat := 2;
        2: NewC^.xKrat := 4;
      Else NewC^.xKrat := 8;
     end;
     NewC^.xInterval := GetLDW(IntervalEdit.Text);
     NewC^.xMethod := TPMetT(MethodRadio.ItemIndex);
     NewC^.xSigned := CheckListBox.Checked[1];
     NewC^.xShl1 := CheckListBox.Checked[2];
     If NewC^.xShl1 and (NewC^.xKrat = 1) then NewC^.xKrat := 2;
     NewC^.xMotorola := CheckListBox.Checked[0];
     NewC^.xKodir := EncodeList.ItemIndex;
     NewC^.xFirstPtr := GetLDW(FirstPtrEdit.Text);
     NewC^.xNoPtr := false;
     NewC^.xStrLen := 0;
     NewC^.xPntp := TypeRadio.ItemIndex = 1;
     NewC^.xAutoStart := TypeRadio.ItemIndex = 2;
     NewC^.EmptyC := BlocksList.Items.Count div 2;
     NewC^.xAlign := AlignEdit.Value;
     for i := 0 to NewC^.EmptyC - 1 do
     begin
      NewC^.EmptyS^[i].a := GetLDW(BlocksList.Items.strings[i * 2]);
      NewC^.EmptyS^[i].b := GetLDW(BlocksList.Items.strings[i * 2 + 1]);
     end;
     NewC^.OTables.Add(InputTableEdit.Text, OutputTableEdit.Text);
     KruptarForm.Saved := False;
     StringsListForm.StringsListBox.Items.Clear;
     ccurnode := NIL;
     KruptarForm.RefreshAll;
    end;
   end;
  end;
  1:
  begin
   CreateGroupFormU.ShowModal;
   If CreateGroupFormU.ModalResult = mrOk then
   begin
    Tmp := Addr(KruptarForm.KruptarProject.Groups);
    If Tmp^.Root = NIL then
     New(NewC, Create(Tmp))
    Else
     New(NewC, Add(Tmp));
    With CreateGroupFormU do
    begin
     NewC^.GrName := 'Group' + IntTostr(Tmp^.Count);
     NewC^.xPtrSize := 0;
     NewC^.xKrat := 0;
     NewC^.xMethod := tmNone;
     NewC^.xSigned := false;
     NewC^.xMotorola := false;
     NewC^.xKodir := EncodeList.ItemIndex;
     NewC^.xFirstPtr := 0;
     NewC^.xNoPtr := True;
     If StringLengthEdit.Text <> '' then
      NewC^.xStrLen := GetLDW(StringLengthEdit.Text) Else
      NewC^.xStrLen := 0;
     If NewC^.xStrLen > 0 then
      NewC^.xInterval := GetLDW(IntervalEdit.Text) Else
      NewC^.xInterval := 0;
     NewC^.xPntp := false;
     NewC^.xAutoStart := false;
     NewC^.EmptyC := BlocksList.Items.Count div 2;
     for i := 0 to NewC^.EmptyC - 1 do
     begin
      NewC^.EmptyS^[i].a := GetLDW(BlocksList.Items.strings[i * 2]);
      NewC^.EmptyS^[i].b := GetLDW(BlocksList.Items.strings[i * 2 + 1]);
     end;
     NewC^.OTables.Add(InputTableEdit.Text, OutputTableEdit.Text);
     KruptarForm.Saved := False;
     StringsListForm.StringsListBox.Items.Clear;
     ccurnode := NIL;
     KruptarForm.RefreshAll;
    end;
   end;
  end;
  2:
  begin
  end;
 end
end;

procedure TProjectItemsForm.TreeViewEdited(Sender: TObject;
  Node: TTreeNode; var S: String);
begin
 If Node.Level = 0 then PGroup(Node.Data)^.GrName := s Else
    PPoss(Node.Data)^.ptrtable^.Name := s;
 KruptarForm.Saved := False;
 KruptarForm.RefreshList;
end;

procedure TProjectItemsForm.AddItemButtonClick(Sender: TObject);
Var i, a, b, ft: LongInt; tmp: TTreeNode; p: Pointer;
begin
 Tmp := TreeView.Selected;
 If Tmp = NIL then Exit;
 If Tmp.Level = 1 then
 begin
  Tmp := Tmp.Parent;
  p := Tmp.Data;
 end Else p := Tmp.Data;
 With PGroup(p)^ do
 begin
  NewItemForm.InputTableEdit.Text := OTables.rootO^.fname;
  NewItemForm.OutputTableEdit.Text := OTables.rootE^.fname;
  NewItemForm.BlocksList.Clear;
  For i := 0 to EmptyC - 1 do
  begin
   NewItemForm.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].a, 8));
   NewItemForm.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].b, 8));
  end;
  NoPtrs := xNoPtr;
 end;
 NewItemForm.ShowModal;
 If NewItemForm.ModalResult = mrOk then With PGroup(p)^ do
 begin
  EmptyC := NewItemForm.BlocksList.Items.Count div 2;
  for i := 0 to EmptyC - 1 do
  begin
   EmptyS^[i].a := GetLDW(NewItemForm.BlocksList.Items.strings[i * 2]);
   EmptyS^[i].b := GetLDW(NewItemForm.BlocksList.Items.strings[i * 2 + 1]);
  end;
  ft := OTables.FindTables(NewItemForm.InputTableEdit.Text,
        NewItemForm.OutputTableEdit.Text);
  If ft = -1 then
  begin
   OTables.Add(NewItemForm.InputTableEdit.Text,
               NewItemForm.OutputTableEdit.Text);
   ft := OTables.Count - 1;
  end;
  a := GetLDW(NewItemForm.PtrStartEdit.Text);
  b := GetLDW(NewItemForm.PtrEndEdit.Text);
  If xNoPtr then
  begin
   If xStrLen = 0 then Posso.Add(ft, a, (b + 1) - a) Else
    Posso.Add(ft, a, ((b + 1) - a) div (xStrLen + xInterval));
  end Else Posso.Add(ft, a, ((b + 1) - a) div (xPtrSize + xInterval));
  PossoR.AddPosso;
  KruptarForm.Saved := False;
  StringsListForm.StringsListBox.Items.Clear;
  ccurnode := NIL;
  Gazzo := P;
  KruptarForm.RefreshAll;
 end;
end;

Procedure TProjectItemsForm.EnableButtons;
Var U: TTreeNode;
begin
 U := TreeView.Selected;
 KruptarForm.HexReload.Enabled := KruptarForm.Created and
                                  not KruptarForm.HexEditor1.Checked;
 KruptarForm.FileClose.Enabled := KruptarForm.Created;
 KruptarForm.GroupNew.Enabled := KruptarForm.Created;
 KruptarForm.GroupLoadOld.Enabled := KruptarForm.Created;
 KruptarForm.ProjectDoall.Enabled := KruptarForm.Created;
 KruptarForm.ToolsSetEmul.Enabled := KruptarForm.Created;
 KruptarForm.ToolsEmulatorStart.Enabled := KruptarForm.Created;
 KruptarForm.ToolsNEmulatorStart.Enabled := KruptarForm.Created;
 KruptarForm.HexEditSave.Enabled := KruptarForm.Created and
                                  not KruptarForm.HexEditor1.Checked;
 KruptarForm.SearchFind.Enabled := U <> nil;
 KruptarForm.Search2Find.Enabled := U <> nil;
 KruptarForm.SearchFindNext.Enabled := U <> nil;
 KruptarForm.Search2FindNext.Enabled := U <> nil;
 KruptarForm.Search2Replace.Enabled := U <> nil;
 KruptarForm.GroupNewItem.Enabled := U <> nil;
 KruptarForm.GroupImport.Enabled := U <> nil;
 KruptarForm.GroupInfo.Enabled := U <> nil;
 KruptarForm.GroupDelete.Enabled := U <> nil;
 KruptarForm.GroupEdit.Enabled := U <> nil;
 KruptarForm.GroupRecalc.Enabled := U <> nil;
 KruptarForm.GroupInsert.Enabled := U <> nil;
 KruptarForm.GroupRestoreText.Enabled := U <> nil;
 KruptarForm.ProjectDogroup.Enabled := U <> nil;
 With KruptarForm.KruptarProject do
 KruptarForm.ToolsProgress.Enabled :=
 (Groups.Root <> NIL) and (Groups.Root^.Posso.Count > 0);
 If U = nil then
 begin
  KruptarForm.GroupMoveUp.Enabled := False;
  KruptarForm.GroupMoveDown.Enabled := False;
  KruptarForm.KanjiButton.Enabled := False;
  KruptarForm.OptionsKanjiItem.Enabled := False;
  Exit;
 end;
 If U.Level = 1 then
 begin
  KruptarForm.KanjiButton.Enabled := PGroup(U.Parent.Data)^.xKodir in [kdSJIS, kdUNIC];
  KruptarForm.OptionsKanjiItem.Enabled := KruptarForm.KanjiButton.Enabled;
  KruptarForm.GroupMoveUp.Enabled := (U.Parent <> NIL) and (U.Parent.GetFirstChild <> U);
  KruptarForm.GroupMoveDown.Enabled := (U.Parent <> NIL) and (U.Parent.GetLastChild <> U);
 end Else
 begin
  KruptarForm.KanjiButton.Enabled := PGroup(U.Data)^.xKodir in [kdSJIS, kdUNIC];
  KruptarForm.OptionsKanjiItem.Enabled := KruptarForm.KanjiButton.Enabled;
  KruptarForm.GroupMoveUp.Enabled := TreeView.Items.GetFirstNode <> U;
  KruptarForm.GroupMoveDown.Enabled := U.GetNextSibling <> nil;
 end;
end;

procedure TProjectItemsForm.FormShow(Sender: TObject);
begin
 EnableButtons;
 KruptarForm.RefreshList;
end;

procedure TProjectItemsForm.MoveUpButtonClick(Sender: TObject);
Var P, Q, R: PGroup; U: TTreeNode;
    F, G, H: PPoss;
    J: PPoss absolute P;
    K: PPoss absolute Q;
    L: PPoss absolute R;
begin
 If TreeView.Selected = NIL then Exit;
 Case TreeView.Selected.Level of
  0: begin
   If TreeView.Selected = TreeView.Items.GetFirstNode then Exit;
   U := TreeView.Selected.GetPrevSibling;
   P := U.Data;
   R := TreeView.Selected.Data;
   Q := R^.Next;
   R^.Next := P;
   P^.Next := Q;
   If U.GetPrevSibling <> NIL then
    PGroup(U.GetPrevSibling.Data)^.Next := R Else
    PGroups(R^.Owner)^.Root := R;
   With PGroups(R^.Owner)^ do If Cur = R then
    Cur := P;
   TreeView.Selected.MoveTo(U, naInsert);
   U.Collapse(True);
   TreeView.Selected.Expand(True);   
   EnableButtons;
   KruptarForm.Saved := False;
  end;
  1: begin
   If TreeView.Selected = TreeView.Selected.Parent.GetFirstChild then Exit;
   U := TreeView.Selected.GetPrev;
   J := U.Data;
   F := J^.GutGut;
   L := TreeView.Selected.Data;
   H := L^.GutGut;
   K := L^.next;
   G := H^.next;
   L^.Next := J;
   H^.next := F;
   J^.Next := K;
   F^.next := G;
   If  U = TreeView.Selected.Parent.GetFirstChild then
   begin
    PGroup(TreeView.Selected.Parent.Data)^.Posso.root := L;
    PGroup(TreeView.Selected.Parent.Data)^.PossoR.root := H;
   end Else
   begin
    Pposs(U.GetPrev.Data)^.next := L;
    Pposs(U.GetPrev.Data)^.GutGut^.next := H;
   end;
   With PGroup(TreeView.Selected.Parent.Data)^.Posso do If Cur = L then Cur := J;
   With PGroup(TreeView.Selected.Parent.Data)^.PossoR do If Cur = H then Cur := F;
   TreeView.Selected.MoveTo(U, naInsert);
   EnableButtons;
   IdontWannaDoThis := True;
   KruptarForm.RefreshList;
   KruptarForm.Saved := False;
  end;
 end;
end;

procedure TProjectItemsForm.MoveDownButtonClick(Sender: TObject);
Var P, Q, R: PGroup; U: TTreeNode;
    F, G, H: PPoss;
    J: PPoss absolute P;
    K: PPoss absolute Q;
    L: PPoss absolute R;
begin
 If TreeView.Selected = NIL then Exit;
 Case TreeView.Selected.Level of
  0: begin
   If TreeView.Selected.GetNextSibling = NIL then Exit;
   U := TreeView.Selected.GetNextSibling;
   P := TreeView.Selected.Data;
   R := U.Data;
   Q := R^.Next;
   R^.Next := P;
   P^.Next := Q;
   If TreeView.Selected.GetPrevSibling <> NIL then
    PGroup(TreeView.Selected.GetPrevSibling.Data)^.Next := R Else
    PGroups(R^.Owner)^.Root := R;
   With PGroups(R^.Owner)^ do If Cur = R then Cur := P;
   U.MoveTo(TreeView.Selected, naInsert);
   U.Collapse(True);
   TreeView.Selected.Expand(True);
   EnableButtons;
   KruptarForm.Saved := False;
  end;
  1: begin
   If TreeView.Selected = TreeView.Selected.Parent.GetLastChild then Exit;
   U := TreeView.Selected.GetNext;
   J := TreeView.Selected.Data;
   F := J^.GutGut;
   L := U.Data;
   H := L^.GutGut;
   K := L^.next;
   G := H^.next;
   L^.Next := J;
   H^.next := F;
   J^.Next := K;
   F^.next := G;
   If TreeView.Selected = TreeView.Selected.Parent.GetFirstChild then
   begin
    PGroup(TreeView.Selected.Parent.Data)^.Posso.root := L;
    PGroup(TreeView.Selected.Parent.Data)^.PossoR.root := H;
   end Else
   begin
    Pposs(TreeView.Selected.GetPrev.Data)^.next := L;
    Pposs(TreeView.Selected.GetPrev.Data)^.GutGut^.next := H;
   end;
   With PGroup(TreeView.Selected.Parent.Data)^.Posso do If Cur = L then Cur := J;
   With PGroup(TreeView.Selected.Parent.Data)^.PossoR do If Cur = H then Cur := F;
   U.MoveTo(TreeView.Selected, naInsert);
   EnableButtons;
   IdontWannaDoThis := True;
   KruptarForm.RefreshList;
   KruptarForm.Saved := False;   
  end;
 end;
end;

procedure TProjectItemsForm.TreeViewChange(Sender: TObject; Node: TTreeNode);
Var U: TTreeNode; pp: PPoss; I: Integer;
begin
 EnableButtons;
 U := TreeView.Selected;
 If (U <> NIL) and (U.Level = 1) then With PGroup(U.Parent.Data)^ do
 begin
  I := 0;
  pp := Posso.Root;
  While pp <> U.Data do
  begin
   Inc(I, PP^.PtrTable^.Count);
   Pp := Pp^.Next;
  end;
  With StringsListForm.StringsListBox do If Items.Count > 0 then
   ItemIndex := Items.Count - 1;
  xI := I;
 end;
 KruptarForm.RefreshList;
end;

procedure TProjectItemsForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 KruptarForm.ProjElsMenuItem.Checked := False;
end;

procedure TProjectItemsForm.DeleteItemButtonClick(Sender: TObject);
Var N, U: TTreeNode; P, R: PGroup; ur, up: PPoss; PP: PPoss absolute P; RR: PPoss absolute R;
begin
 N := TreeView.Selected;
 If N = NIL then Exit;
 Case N.Level of
  0: If MessageDlg('�� ������������� ������ ������� ������ "' +
     PGroup(N.Data)^.GrName + '"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
   U := N.GetPrevSibling;
   If U <> NIL then
   begin
    P := U.Data;
    R := N.Data;
    P^.Next := R^.Next;
   end Else
   begin
    P := NIL;
    R := N.Data;
    PGroups(R^.Owner)^.Root := R^.Next;
   end;
   With PGroups(R^.Owner)^ do If Cur = R then Cur := P;
   Dispose(R, Clear);
   TreeView.Items.Delete(N);
   EnableButtons;
   IdontWannaDoThis := True;
   KruptarForm.RefreshList;
   KruptarForm.RefreshOriginal;
   KruptarForm.RefreshNew;
   KruptarForm.Saved := False;
  end;
  1: If MessageDlg('�� ������������� ������ ������� ������� "' +
     PPoss(N.Data)^.ptrtable^.Name + '"?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
   U := N.GetPrev;
   If U <> N.Parent then
   begin
    PP := U.Data;
    UP := PP^.GutGut;
    RR := N.Data;
    UR := RR^.GutGut;
    PP^.Next := RR^.Next;
    UP^.Next := UR^.Next;
   end Else
   begin
    PP := NIL;
    UP := NIL;
    RR := N.Data;
    UR := RR^.GutGut;
    PGroup(N.Parent.Data)^.Posso.Root := RR^.Next;
    PGroup(N.Parent.Data)^.PossoR.Root := UR^.Next;
   end;
   With PGroup(N.Parent.Data)^.Posso do If Cur = RR then Cur := PP;
   With PGroup(N.Parent.Data)^.PossoR do If Cur = UR then Cur := UP;
   Dec(PGroup(N.Parent.Data)^.Posso.Count);
   Dispose(RR^.PtrTable, Clear);
   Dispose(RR);
   Dec(PGroup(N.Parent.Data)^.PossoR.Count);
   Dispose(UR^.PtrTable, Clear);
   Dispose(UR);
   TreeView.Items.Delete(N);
   EnableButtons;
   IdontWannaDoThis := True;
   KruptarForm.RefreshList;
   KruptarForm.RefreshOriginal;
   KruptarForm.RefreshNew;   
   KruptarForm.Saved := False;
  end;
 end;
end;

procedure TProjectItemsForm.ElParamsButtonClick(Sender: TObject);
Var
 i: LongInt; tmp, El: TTreeNode; p: Pointer; D: PPoss;
 s1, s2: string;
begin
 Tmp := TreeView.Selected;
 El := Tmp;
 If Tmp = NIL then Exit;
 D := El.Data;
 If Tmp.Level = 1 then
 begin
  Tmp := Tmp.Parent;
  p := Tmp.Data;
 end Else p := Tmp.Data;
 Case El.Level of
  0: With PGroup(p)^ do
  begin
   ElParamsDialog.InputTableEdit.Text := OTables.rootO^.fname;
   ElParamsDialog.OutputTableEdit.Text := OTables.rootE^.fname;
   ElParamsDialog.BlocksList.Clear;
   For i := 0 to EmptyC - 1 do
   begin
    ElParamsDialog.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].a, 8));
    ElParamsDialog.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].b, 8));
   end;
   ElParamsDialog.Caption := '��������� ������ "' + GrName + '"';
   ElParamsDialog.ShowModal;
   If ElParamsDialog.ModalResult = mrOk then With PGroup(p)^ do
   begin
    EmptyC := ElParamsDialog.BlocksList.Items.Count div 2;
    for i := 0 to EmptyC - 1 do
    begin
     EmptyS^[i].a := GetLDW(ElParamsDialog.BlocksList.Items.strings[i * 2]);
     EmptyS^[i].b := GetLDW(ElParamsDialog.BlocksList.Items.strings[i * 2 + 1]);
    end;
    s1 := ElParamsDialog.InputTableEdit.Text;
    s2 := ElParamsDialog.OutputTableEdit.Text;
    KruptarForm.Saved := False;
    If (s1 <> OTables.rootO^.fname) or (s2 <> OTables.rootE^.fname) then
    begin
     OTables.ChangeTable(0, s1, s2);
     Posso.Reload;
     StringsListForm.StringsListBox.Items.Clear;
     ccurnode := NIL;
     KruptarForm.RefreshAll;
    end;
   end;
  end;
  1: With D^ do
  begin
   PGroup(p)^.GetTableNames(tabn, s1, s2);
   ElParamsDialog.InputTableEdit.Text := s1;
   ElParamsDialog.OutputTableEdit.Text := s2;
   ElParamsDialog.BlocksList.Clear;
   With PGroup(p)^ do For i := 0 to EmptyC - 1 do
   begin
    ElParamsDialog.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].a, 8));
    ElParamsDialog.BlocksList.Items.Add('h' + IntToHex(EmptyS^[I].b, 8));
   end;
   ElParamsDialog.Caption := '��������� �������� "' + PtrTable^.Name + '"';
   ElParamsDialog.ShowModal;
   If ElParamsDialog.ModalResult = mrOk then With PGroup(p)^ do
   begin
    EmptyC := ElParamsDialog.BlocksList.Items.Count div 2;
    for i := 0 to EmptyC - 1 do
    begin
     EmptyS^[i].a := GetLDW(ElParamsDialog.BlocksList.Items.strings[i * 2]);
     EmptyS^[i].b := GetLDW(ElParamsDialog.BlocksList.Items.strings[i * 2 + 1]);
    end;
    s1 := ElParamsDialog.InputTableEdit.Text;
    s2 := ElParamsDialog.OutputTableEdit.Text;
    KruptarForm.Saved := False;
    If D^.tabn = 0 then
    begin
     i := OTables.FindTables(s1, s2);
     If D^.tabn = i then Exit;
     D^.tabn := i;
     If D^.tabn = -1 then
     begin
      OTables.Add(s1, s2);
      D^.tabn := OTables.Count - 1;
     end;
     Posso.Reload;
     StringsListForm.StringsListBox.Items.Clear;
     ccurnode := NIL;
     KruptarForm.RefreshAll;
    end Else
    If (s1 <> OTables.rootO^.fname) or (s2 <> OTables.rootE^.fname) then
    begin
     OTables.ChangeTable(D^.tabn, s1, s2);
     Posso.Reload;
     StringsListForm.StringsListBox.Items.Clear;
     ccurnode := NIL;
     KruptarForm.RefreshAll;
    end;
   end;
  end;
 end;
end;

procedure TProjectItemsForm.TreeViewDblClick(Sender: TObject);
Var U: TTreeNode; P: Pointer; N, NN: PTTable;
begin
 U := TreeView.Selected;
 If U = NIL then Exit;
 Case U.Level of
  1: If (MessageDlg(
  '�� ������������� ������ ������������ ������������ ����� �������� "' +
   PPoss(U.Data)^.PtrTable^.Name + '"?', mtConfirmation, [mbYes, mbNo], 0)
   = mrYes) then With PPoss(U.Data)^ do
  begin
   N := PtrTable^.Root;
   NN := GutGut^.PtrTable^.Root;
   While N <> NIL do
   begin
    NN^.TPtr := N^.TPtr;
    NN^.Text := N^.Text;
    N := N^.Next;
    NN := NN^.Next;
   end;
   KruptarForm.RefreshList;
   KruptarForm.RefreshNew;
   KruptarForm.Saved := False;
  end;
  0: With PGroup(U.Data)^ do If (MessageDlg(
  '�� ������������� ������ ������������ ������������ ����� ������ "' +
   GrName + '"?', mtConfirmation, [mbYes, mbNo], 0)
   = mrYes) then With PossoR do
  begin
   P := Owner;
   Clear;
   Init;
   Owner := P;
   AddPosso;
   KruptarForm.RefreshList;
   KruptarForm.RefreshNew;
   KruptarForm.Saved := False;
  end;
 end;
end;

procedure TProjectItemsForm.TreeViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If ssAlt in Shift then KruptarForm.SetFocus;
 If Key = VK_DELETE then DeleteItemButtonClick(Sender);
end;

procedure TProjectItemsForm.TreeViewKeyPress(Sender: TObject;
  var Key: Char);
Var U: TTreeNode;
begin
 If not TreeView.IsEditing and (Key in [#13, #32]) then
 begin
  U := TreeView.Selected;
  If (U = NIL) or (U.Level = 1) then Exit;
  If not U.Expanded then U.Expand(True) Else U.Collapse(True);
  Key := #0;
 end;
end;

end.
