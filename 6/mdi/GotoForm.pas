unit GotoForm;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls;

type
  TOKBottomDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Edit: TEdit;
    procedure OKBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OKBottomDlg: TOKBottomDlg;
  bOkButton, bCancelButton: Boolean;

implementation

{$R *.dfm}

Uses HexUnit, Dialogs;

procedure TOKBottomDlg.OKBtnClick(Sender: TObject);
begin
 bOkButton := True;
end;

procedure TOKBottomDlg.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If (Edit.Text <> '') and not UnsCheckOut(Edit.Text) then
 begin
  ShowMessage('"' + Edit.Text + '" - неверное числовое значение!');
  CanClose := False;
 end;
end;

procedure TOKBottomDlg.CancelBtnClick(Sender: TObject);
begin
 bCancelButton := True;
end;

procedure TOKBottomDlg.FormShow(Sender: TObject);
begin
 bOkButton := False;
 bCancelButton := False;
end;

end.
