unit NewItemUnit;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Dialogs, HexUnit;

type
  TNewItemForm = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    InputTableEdit: TLabeledEdit;
    OpenInputButton: TSpeedButton;
    OpenOutputButton: TSpeedButton;
    OutputTableEdit: TLabeledEdit;
    OpenDialog: TOpenDialog;
    Bevel2: TBevel;
    BlockEndEdit: TLabeledEdit;
    BlockStartEdit: TLabeledEdit;
    AddBlockButton: TSpeedButton;
    BlocksList: TListBox;
    BlockLabel: TLabel;
    Bevel3: TBevel;
    PtrEndEdit: TLabeledEdit;
    PtrStartEdit: TLabeledEdit;
    MoveUpButton: TSpeedButton;
    MoveDownButton: TSpeedButton;
    DeleteButton: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure AddBlockButtonClick(Sender: TObject);
    procedure MoveUpButtonClick(Sender: TObject);
    procedure MoveDownButtonClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure DeleteButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure OpenInputButtonClick(Sender: TObject);
    procedure OpenOutputButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NewItemForm: TNewItemForm;
  NoPtrs: Boolean;

implementation

{$R *.dfm}

Var bCancelButton, bokbutton: boolean;

procedure TNewItemForm.FormShow(Sender: TObject);
begin
 Case NoPtrs of
  False:
  begin
   PtrStartEdit.EditLabel.Caption := '������ ����� ���������';
   PtrEndEdit.EditLabel.Caption := '����� ����� ���������';
  end;
  Else
  begin
   PtrStartEdit.EditLabel.Caption := '������ ����� ������';
   PtrEndEdit.EditLabel.Caption := '����� ����� ������';
  end
 end;
 PtrStartEdit.Text := 'h00000000';
 PtrEndEdit.Text := 'h00000000';
 BlockStartEdit.Text := 'h00000000';
 BlockEndEdit.Text := 'h00000000';
 PtrStartEdit.SetFocus;
 bcancelbutton := false;
 bokbutton := false;
end;

procedure TNewItemForm.CancelBtnClick(Sender: TObject);
begin
 bCancelButton := true;
end;

procedure TNewItemForm.AddBlockButtonClick(Sender: TObject);
begin
 If HCheckOut(BlockStartEdit.Text) and HCheckOut(BlockEndEdit.Text) and
    (GetLDW(BlockStartEdit.Text) >= 0) and (GetLDW(BlockEndEdit.Text) >= 0) then
 begin
  BlocksList.Items.Add(BlockStartEdit.Text);
  BlocksList.Items.Add(BlockEndEdit.Text);
 end Else ShowMessage('���� ��� ��� �������� �������� ������� ��� ������ ����!');
end;

procedure TNewItemForm.MoveUpButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex > 0 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex - 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex - 1;
 end;
end;

procedure TNewItemForm.MoveDownButtonClick(Sender: TObject);
Var s: string;
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex < BlocksList.Items.Count - 1 then
 begin
  s := BlocksList.Items.Strings[BlocksList.ItemIndex];
  BlocksList.Items.Strings[BlocksList.ItemIndex] :=
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1];
  BlocksList.Items.Strings[BlocksList.ItemIndex + 1] := s;
  BlocksList.ItemIndex := BlocksList.ItemIndex + 1;
 end;
end;

procedure TNewItemForm.OKBtnClick(Sender: TObject);
begin
 bokbutton := true;
end;

procedure TNewItemForm.DeleteButtonClick(Sender: TObject);
begin
 If (BlocksList.ItemIndex < 0) or
    (BlocksList.ItemIndex >= BlocksList.Items.Count) then Exit;
 If BlocksList.ItemIndex mod 2 = 1 then
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex - 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end Else
 begin
  BlocksList.Items.Delete(BlocksList.ItemIndex + 1);
  BlocksList.Items.Delete(BlocksList.ItemIndex);
 end;
end;

procedure TNewItemForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If not FileExists(InputTableEdit.Text) then
 begin
  ShowMessage('���� "' + InputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not FileExists(OutputTableEdit.Text) then
 begin
  ShowMessage('���� "' + OutputTableEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not HCheckOut(PtrStartEdit.Text) then
 begin
  ShowMessage('"' + PtrStartEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end Else
 If not HCheckOut(PtrEndEdit.Text) then
 begin
  ShowMessage('"' + PtrEndEdit.Text + '" - �������� �������� ��������!');
  CanClose := False;
 end;
 If GetLDW(PtrEndEdit.Text) <= GetLDW(PtrStartEdit.Text) then
 begin
  ShowMessage('�������� �������� ������ ���� ������ ��� ���������!');
  CanClose := False;
 end;
end;

procedure TNewItemForm.OpenInputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  InputTableEdit.Text := OpenDialog.FileName;
end;

procedure TNewItemForm.OpenOutputButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  OutputTableEdit.Text := OpenDialog.FileName;
end;

end.
