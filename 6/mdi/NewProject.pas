unit NewProject;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Buttons, Dialogs;

type
  TCreateNewProjectForm = class(TForm)
    OkButton: TButton;
    CancelButton: TButton;
    Bevel: TBevel;
    InputRomEdit: TLabeledEdit;
    OutputRomEdit: TLabeledEdit;
    OpenInputRomButton: TSpeedButton;
    OpenOutputRomButton: TSpeedButton;
    OpenDialog: TOpenDialog;
    procedure OpenInputRomButtonClick(Sender: TObject);
    procedure OpenOutputRomButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelButtonClick(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
  end;

var
  CreateNewProjectForm: TCreateNewProjectForm;

implementation


{$R *.DFM}

Var bCancelButton, bokbutton: Boolean;

procedure TCreateNewProjectForm.OpenInputRomButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  InputRomEdit.Text := OpenDialog.FileName;
end;

procedure TCreateNewProjectForm.OpenOutputRomButtonClick(Sender: TObject);
begin
 If OpenDialog.Execute then
  OutputRomEdit.Text := OpenDialog.FileName;
end;

procedure TCreateNewProjectForm.FormShow(Sender: TObject);
begin
 InputRomEdit.Text := '';
 OutputRomEdit.Text := '';
 bCancelButton := False;
 bokbutton := false;
 InputRomEdit.SetFocus;
end;

procedure TCreateNewProjectForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 If bCancelButton or not bOkButton then Exit;
 If not FileExists(InputRomEdit.Text) then
 begin
  ShowMessage('���� "' + InputRomEdit.Text + '" �� ������!');
  CanClose := False;
 end Else
 If not FileExists(OutputRomEdit.Text) then
 begin
  ShowMessage('���� "' + OutputRomEdit.Text + '" �� ������!');
  CanClose := False;
 end;
end;

procedure TCreateNewProjectForm.CancelButtonClick(Sender: TObject);
begin
 bCancelButton := True;
end;

procedure TCreateNewProjectForm.OkButtonClick(Sender: TObject);
begin
 bokbutton := false;
end;

end.
