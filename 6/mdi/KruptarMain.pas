Unit KruptarMain;

Interface

Uses
 Windows, SysUtils, Classes, Graphics, Forms, Controls, Menus,
 StdCtrls, Dialogs, Buttons, Messages, ExtCtrls, ComCtrls, StdActns,
 ActnList, ToolWin, ImgList, KruptarProjectUnit, ShellApi, Psock, NMHttp,
 UnicodeEdit, UnicodeMemo, Grids, UnicodeLists;

Const CurVer = 6062;

Type TRect2 = Record
      Left, Top, W, H: Integer;
     end;

type
  TKruptarForm = class(TForm)
    MainMenu: TMainMenu;
    FileMenu: TMenuItem;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    WindowMenu: TMenuItem;
    HelpMenu: TMenuItem;
    RecentEndLine: TMenuItem;
    FileExitItem: TMenuItem;
    ProjElsMenuItem: TMenuItem;
    StringListWinItem: TMenuItem;
    HelpAboutItem: TMenuItem;
    OpenDialog: TOpenDialog;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    ChangeTextItem: TMenuItem;
    StatusBar: TStatusBar;
    ActionList: TActionList;
    FileNew: TAction;
    FileSave: TAction;
    FileExit: TAction;
    FileOpen: TAction;
    FileSaveAs: TAction;
    HelpAbout: TAction;
    OrigTextItem: TMenuItem;
    ToolBar: TToolBar;
    OpenButton: TToolButton;
    SaveButton: TToolButton;
    NewButton: TToolButton;
    ImageList: TImageList;
    SaveDialog: TSaveDialog;
    N2: TMenuItem;
    DefaultWindows: TMenuItem;
    DoAllButton: TToolButton;
    Separator1: TToolButton;
    ProjectDoall: TAction;
    DoI: TMenuItem;
    DoAllI: TMenuItem;
    ProjectDogroup: TAction;
    GroupMenu: TMenuItem;
    NewGroupI: TMenuItem;
    NewItemI: TMenuItem;
    EditI: TMenuItem;
    Sepa2: TMenuItem;
    LoadGroupI: TMenuItem;
    Sepa1: TMenuItem;
    MoveUpI: TMenuItem;
    MoveDownI: TMenuItem;
    DeleteI: TMenuItem;
    Sepa3: TMenuItem;
    RecalcIt: TMenuItem;
    InsROMi: TMenuItem;
    GroupNew: TAction;
    GroupLoadOld: TAction;
    GroupNewItem: TAction;
    GroupEdit: TAction;
    GroupMoveUp: TAction;
    GroupMoveDown: TAction;
    GroupDelete: TAction;
    GroupRecalc: TAction;
    GroupInsert: TAction;
    NewGroupButton: TToolButton;
    NewItemButton: TToolButton;
    EditButton: TToolButton;
    LoadGroupButton: TToolButton;
    MoveUpButton: TToolButton;
    MoveDownButton: TToolButton;
    DeleteButton: TToolButton;
    RecalcButton: TToolButton;
    InsertButton: TToolButton;
    Separator4: TToolButton;
    Separator2: TToolButton;
    DoButton: TToolButton;
    Separator5: TToolButton;
    Separator6: TToolButton;
    GroupRestoreText: TAction;
    ReloadI: TMenuItem;
    ReloadButton: TToolButton;
    Config: TMenuItem;
    CopyFindItem: TMenuItem;
    OldOpenDialog: TOpenDialog;
    GroupInfo: TAction;
    GroupInfo1: TMenuItem;
    InfoButton: TToolButton;
    Separator3: TToolButton;
    Separator7: TToolButton;
    KanjiButton: TToolButton;
    OptionsKanji: TAction;
    OptionsKanjiItem: TMenuItem;
    FileClose: TAction;
    FileClose1: TMenuItem;
    FontDialog: TFontDialog;
    OptionsFont: TAction;
    Sepa: TMenuItem;
    FontI: TMenuItem;
    RecentLine: TMenuItem;
    Recent3: TMenuItem;
    Recent2: TMenuItem;
    Recent1: TMenuItem;
    Recent4: TMenuItem;
    Search1: TMenuItem;
    SearchFindI: TMenuItem;
    SearchFindNextI: TMenuItem;
    SearchFind: TAction;
    SearchFindNext: TAction;
    ActionInsert: TAction;
    Search2: TMenuItem;
    Search2Find: TAction;
    Search2FindNext: TAction;
    Search2Replace: TAction;
    FindI: TMenuItem;
    FindNextI: TMenuItem;
    ReplaceI: TMenuItem;
    N1: TMenuItem;
    ToolsHexEditor: TAction;
    ToolsProgress: TAction;
    ToolsEmulatorStart: TAction;
    ToolsNEmulatorStart: TAction;
    HexEditem: TMenuItem;
    oolsProgress1: TMenuItem;
    N3: TMenuItem;
    oolsEmulatorStart1: TMenuItem;
    oolsNEmulatorStart1: TMenuItem;
    ToolsSetEmul: TAction;
    oolsSetEmul1: TMenuItem;
    EmulOpenDialog: TOpenDialog;
    InputEmulButton: TToolButton;
    OutputEmulButton: TToolButton;
    Separator8: TToolButton;
    Upd: TNMHTTP;
    Update: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    SavePos: TMenuItem;
    Separator9: TToolButton;
    UpdateButton: TToolButton;
    N7: TMenuItem;
    HexEditSave: TAction;
    SaveHexButton: TToolButton;
    N8: TMenuItem;
    SaveROM: TMenuItem;
    ReloadROM: TMenuItem;
    HexReload: TAction;
    Separator10: TToolButton;
    GroupImport: TAction;
    N9: TMenuItem;
    ImportItem: TMenuItem;
    PtrDialog: TOpenDialog;
    N10: TMenuItem;
    Enter1: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    HexEditor1: TMenuItem;
    Unicum: TUnicodeListBox;
    UNITAB: TUnicodeListBox;
    TextExtract: TMenuItem;
    SaveTextDialog: TSaveDialog;
    N14: TMenuItem;
    procedure FileNewExecute(Sender: TObject);
    procedure FileOpenExecute(Sender: TObject);
    procedure HelpAboutExecute(Sender: TObject);
    procedure FileExitExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FileSaveExecute(Sender: TObject);
    procedure FileSaveAsExecute(Sender: TObject);
    procedure ProjElsMenuItemClick(Sender: TObject);
    procedure StringListWinItemClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure OrigTextItemClick(Sender: TObject);
    procedure ChangeTextItemClick(Sender: TObject);
    procedure DefaultWindowsClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure GroupNewExecute(Sender: TObject);
    procedure GroupLoadOldExecute(Sender: TObject);
    procedure GroupNewItemExecute(Sender: TObject);
    procedure GroupEditExecute(Sender: TObject);
    procedure GroupMoveUpExecute(Sender: TObject);
    procedure GroupMoveDownExecute(Sender: TObject);
    procedure GroupDeleteExecute(Sender: TObject);
    procedure GroupRecalcExecute(Sender: TObject);
    procedure GroupInsertExecute(Sender: TObject);
    procedure ProjectDoallExecute(Sender: TObject);
    procedure ProjectDogroupExecute(Sender: TObject);
    procedure GroupRestoreTextExecute(Sender: TObject);
    procedure GroupInfoExecute(Sender: TObject);
    procedure CopyFindItemClick(Sender: TObject);
    procedure OptionsKanjiExecute(Sender: TObject);
    procedure FileCloseExecute(Sender: TObject);
    procedure OptionsFontExecute(Sender: TObject);
    procedure Recent1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SearchFindBeforeExecute(Sender: TObject);
    procedure SearchFindNextHint(Sender: TObject);
    procedure ActionInsertExecute(Sender: TObject);
    procedure Search2FindExecute(Sender: TObject);
    procedure Search2FindNextExecute(Sender: TObject);
    procedure Search2ReplaceExecute(Sender: TObject);
    procedure ToolsHexEditorExecute(Sender: TObject);
    procedure ToolsProgressExecute(Sender: TObject);
    procedure ToolsEmulatorStartExecute(Sender: TObject);
    procedure ToolsNEmulatorStartExecute(Sender: TObject);
    procedure ToolsSetEmulExecute(Sender: TObject);
    procedure UpdateExecute(Sender: TObject);
    procedure UpdSuccess(Cmd: CmdType);
    procedure SavePosClick(Sender: TObject);
    procedure HexEditSaveExecute(Sender: TObject);
    procedure HexReloadExecute(Sender: TObject);
    procedure GroupImportExecute(Sender: TObject);
    procedure Enter1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure HexEditor1Click(Sender: TObject);
    procedure TextExtractClick(Sender: TObject);
    procedure N14Click(Sender: TObject);
  private
    DefHeight: Integer;
    ClHeight: Integer;
    CurMN: Char;
    Procedure wNamed(x: boolean);
    Function  rNamed: boolean;
    Procedure wSaved(x: boolean);
    Function  rSaved: boolean;
    Procedure SetRecent(S: String);
    Procedure ppFind(U: TTreeNode; S: WideString);
    Procedure ppReplace(U: TTreeNode; S, Rs: WideString);
  public
    Created: Boolean;
    KruptarProject: TKruptarProject;
    Procedure InitWindows;
    Function GetCurMN: Char;
    Procedure RefreshTree;
    Procedure RefreshList;
    Procedure RefreshOriginal;
    Procedure RefreshNew;
    Procedure RefreshAll;
    Function Recalculate: Integer;
    Function InsertText: Integer;
    Property Named: boolean read rNamed write wNamed;
    Property Saved: boolean read rSaved write wSaved;
  end;

Const
 ListTitle: String = '������ ��������� ���������';

Type
 DWordArray = Array[0..16383] of DWord;

var
  KruptarForm: TKruptarForm;
  LstTitle, Dir: String;
  cCurNode: Pointer;
  cnCount: Integer;
  IDontWannaDoThis: Boolean;
  ReCAP: Pointer;
  Gazzo: PGroup;
  QuietSel: Boolean;
  ListArray: DWordArray;

Procedure Redot(Var S: String);

implementation

{$R *.dfm}
{$R SOUND.RES}

uses About, NewProject, ProjItems, StringListUnit, UniConv, Originaledit,
  NewTextEdit, Recalc, CreateGroupUnit, HexUnit, MMSystem, InfoUnit, FRep,
  dict, process, Hexedit, hextexu;

Procedure TKruptarForm.wNamed(x: boolean);
begin
 KruptarProject.fNamed := x;
 If not Created then
 begin
  Caption := Application.Title;
  Exit;
 end;
 If x then
 begin
  Caption := Application.Title + ' - [' + KruptarProject.ProjectFile + ']';
  If not Saved then Caption := Caption + '*';
 end Else
  Caption := Application.Title + ' - [Noname.kpj]*';
end;

Function  TKruptarForm.rNamed: boolean;
begin
 Result := KruptarProject.fNamed;
end;

Procedure TKruptarForm.wSaved(x: boolean);
begin
 KruptarProject.fSaved := x;
 If not Created then
 begin
  Caption := Application.Title;
  Exit;
 end;
 if not named then exit;
 If x then
  Caption := Application.Title + ' - [' + KruptarProject.ProjectFile + ']'
 Else
  Caption := Application.Title + ' - [' + KruptarProject.ProjectFile + ']*';
end;

Function  TKruptarForm.rSaved: boolean;
begin
 Result := KruptarProject.fSaved;
end;

Procedure TKruptarForm.RefreshOriginal;
Var B: Boolean;
begin
 B := Saved;
 OriginalTextEditForm.Memo.Lines.Clear;
 Saved := B;
 OriginalTextEditForm.SetText;
end;

Procedure TKruptarForm.RefreshNew;
Var B: Boolean;
begin
 B := Saved;
 NewTextForm.Memo.Lines.Clear;
 Saved := B;
 NewTextForm.SetText;
end;

Procedure TKruptarForm.RefreshList;
Var Node: TTreeNode; n: pttable; pp, pe: pposs; var I, J: integer;
function _is: string;
begin
 result := pe^.ptrtable^.Name + '-';
end;
begin
 If QuietSel then
 begin
  QuietSel := False;
  Exit;
 end;
 With ProjectItemsForm do
 begin
  Node := TreeView.Selected;
  StringsListForm.Caption := ListTitle;
  If Node = NIL then
  begin
   StringsListForm.StringsListBox.Items.Clear;
   Exit;
  end;
  If Node.Level = 1 then Node := Node.Parent;
  If (Node = NIL) or (Node.Data = NIL) then
  begin
   StringsListForm.StringsListBox.Items.Clear;
   Exit;
  end;
  StringsListForm.StringsListBox.ItemHeight :=
  Abs(StringsListForm.StringsListBox.Font.Height) + 2;
  If (cCurNode = Node.Data) and (PGroup(cCurNode)^.Posso.count = cnCount) and
     not IDontWannaDoThis then
  begin
   If xI >= 0 then StringsListForm.StringsListBox.ItemIndex := xI;
   xI := -1;
   With PGroup(cCurNode)^ do
   begin
    Case xKodir of
     kdANSI: StringsListForm.StringsListBox.Font.Charset := DEFAULT_CHARSET;
     kdSJIS, kdUNIC: StringsListForm.StringsListBox.Font.Charset := SHIFTJIS_CHARSET;
    end;
    LstTitle := ListTitle + ' - [' + GrName + ']';
    StringsListForm.Caption := LstTitle;
    With StringsListForm.StringsListBox do If ItemIndex >= 0 then
    begin
     StringsListForm.Caption := LstTitle + ' ' +
     IntToStr(ItemIndex + 1) + '/' +
     IntToStr(Items.Count);
    end;
    pe := posso.root;
    pp := possor.root;
    i := 0;
    while (pe <> nil) or (pp <> nil) do
    begin
     n := pp^.ptrtable^.root;
     while n <> nil do
     begin
      Case xKodir of
       kdANSI, kdUNIC: StringsListForm.StringsListBox.Items.Strings[i] :=
               _is + Inttohex(n^.tptr, 8) + ' -> ' + n^.text;
       kdSJIS: StringsListForm.StringsListBox.Items.Strings[i] :=
               _is + InttoHex(n^.tptr, 8) + ' -> ' + unitext(n^.text);
      end;
      If I < 16383 then ListArray[I] := Dword(N);
      n := n^.next;
      Inc(I);
     end;
     pp^.ptrtable^.Name := pe^.ptrtable^.Name;
     pp := pp^.next;
     pe := pe^.next;
    end;
   end;
   Exit;
  end;
  IDontWannaDoThis := False;
  cCurNode := Node.Data;
  cnCount := PGroup(cCurNode)^.Posso.count;
  If xI >= 0 then I := xI Else I := 0;
  xI := -1;
  StringsListForm.StringsListBox.Items.Clear;
  If Node = NIL then Exit;
  With PGroup(Node.Data)^ do
  begin
   Case xKodir of
    kdANSI: StringsListForm.StringsListBox.Font.Charset := DEFAULT_CHARSET;
    kdSJIS, kdUNIC: StringsListForm.StringsListBox.Font.Charset := SHIFTJIS_CHARSET;
   end;
   LstTitle := ListTitle + ' - [' + GrName + ']';
   StringsListForm.Caption := LstTitle;
   pe := posso.root;
   pp := possor.root;
   while (pe <> nil) or (pp <> nil) do
   begin
    n := pp^.ptrtable^.root;
    while n <> nil do
    begin
     Case xKodir of
      kdANSI, kdUNIC: StringsListForm.StringsListBox.Items.Add(
              _is + Inttohex(n^.tptr, 8) + ' -> ' + n^.text);
      kdSJIS: StringsListForm.StringsListBox.Items.Add(
              _is + InttoHex(n^.tptr, 8) + ' -> ' + unitext(n^.text))
     end;
     J := StringsListForm.StringsListBox.Items.Count - 1;
     If J < 16383 then ListArray[J] := DWord(N);
     n := n^.next;
    end;
    pp^.ptrtable^.Name := pe^.ptrtable^.Name;
    pp := pp^.next;
    pe := pe^.next;
   end;
   With StringsListForm.StringsListBox do
   If (i >= 0) and (i < Items.Count) then
   begin
    ItemIndex := Items.Count - 1;
    ItemIndex := i;
    StringsListForm.Caption := LstTitle + ' ' +
    IntToStr(ItemIndex + 1) + '/' +
    IntToStr(Items.Count);
   end;
  end;
 end;
 StringsListForm.StringsListBox.Refresh;
 RefreshOriginal;
 RefreshNew;
end;

Procedure TKruptarForm.RefreshAll;
begin
 RefreshTree;
 RefreshList;
 RefreshOriginal;
 RefreshNew;
 ProjectItemsForm.Show;
 StringsListForm.Show;
 OriginalTextEditForm.show;
 NewTextForm.Show;
 If HexEditForm.Visible then
 begin
  HexEditForm.RefreshHex;
  HexTextForm.SetFocus;
  HexEditForm.SetFocus;
 end;
end;

procedure TKruptarForm.FileNewExecute(Sender: TObject);
begin
 If Created and not Saved then
 Case MessageDlg('������� ������ �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
  mrYes: FileSaveExecute(Sender);
  mrCancel: Exit;
 end;
 CreateNewProjectForm.ShowModal;
 If CreateNewProjectForm.ModalResult = mrOk then
 begin
  StringsListForm.StringsListBox.Items.Clear;
  ProjectItemsForm.TreeView.Items.Clear;
  KruptarProject.Done;
  With CreateNewProjectForm do
   KruptarProject.Init(InputRomEdit.Text, OutputRomEdit.Text);
  Created := True;
  Saved := false;
  Named := false;
  RefreshAll;
  FormActivate(Sender);
 end;
end;

Var RecentCount: Byte;

procedure TKruptarForm.FileOpenExecute(Sender: TObject);
Var S: String;
begin
 If Created and not Saved then
 Case MessageDlg('������� ������ �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
  mrYes: FileSaveExecute(Sender);
  mrCancel: Exit;
 end;
 If OpenDialog.Execute then
 begin
  StringsListForm.StringsListBox.Items.Clear;
  ProjectItemsForm.TreeView.Items.Clear;
  Created := False;
  Saved := false;
  Named := false;
  KruptarProject.Done;
  S := OpenDialog.FileName; Redot(S);
  KruptarProject.Open(S);
  Gazzo := KruptarProject.Groups.Root;
  If Gazzo <> NIL then SetRecent(KruptarProject.ProjectFile);
  RefreshAll;
  FormActivate(Sender);
 end;
end;

procedure TKruptarForm.HelpAboutExecute(Sender: TObject);
var WaveHandle: THandle; WavePointer: pointer;
begin
 WaveHandle := FindResource(hInstance, 'KRUPTAR_WAV', RT_RCDATA);
 if WaveHandle <> 0 then
 begin
  WaveHandle := LoadResource(hInstance,WaveHandle);
  if WaveHandle <> 0 then
  begin
   WavePointer := LockResource(WaveHandle);
   SndPlaySound(WavePointer, SND_MEMORY or SND_ASYNC);
   UnlockResource(WaveHandle);
   FreeResource(WaveHandle);
  end;
 end;
 AboutBox.ShowModal;
end;

procedure TKruptarForm.FileExitExecute(Sender: TObject);
begin
 Close;
end;

Var SW, SH: Integer;

Procedure TKruptarForm.InitWindows;
Var Rect: TRect;
begin
 Top := 0;
 Left := 0;
 ClHeight := ClientHeight;
 SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);
 Width := Screen.Width;
 ProjectItemsForm.Left := 0;
 ProjectItemsForm.Top := DefHeight;
 HexEditForm.Top := DefHeight;
 HexEditForm.Left := Rect.Right - HexEditForm.Width;
 HexTextForm.Top := DefHeight + HexEditForm.Height;
 HexTextForm.Left := 0;
 HexTextForm.Width := Screen.Width;
 HexTextForm.ClientHeight :=
 HexEditForm.RowCount * (HexTextForm.Memo.Font.Size + 7);
 HexTextForm.Top := Rect.Bottom - HexTextForm.Height;
 HexEditForm.RefreshHex;
 If not HexEditForm.Visible then
 begin
  ProjectItemsForm.Width := 217;
  ProjectItemsForm.Height := (Rect.Bottom - DefHeight) div 2 - 60;
  StringsListForm.Top := DefHeight;
  StringsListForm.Left := ProjectItemsForm.Width;
  StringsListForm.Width := Screen.Width - ProjectItemsForm.Width;
  StringsListForm.Height := (Rect.Bottom - DefHeight) div 2 - 60;
  OriginalTextEditForm.Left := 0;
  OriginalTextEditForm.Width := Screen.Width div 2;
  OriginalTextEditForm.Top := DefHeight + ProjectItemsForm.Height;
  OriginalTextEditForm.Height := Rect.Bottom - OriginalTextEditForm.Top;
  NewTextForm.Width := Screen.Width div 2;
  NewTextForm.Height := OriginalTextEditForm.Height;
  NewTextForm.Left := OriginalTextEditForm.Width;
  NewTextForm.Top := OriginalTextEditForm.Top;
 end Else
 begin
  ProjectItemsForm.Width := 117;
  ProjectItemsForm.Height := (HexTextForm.Top - DefHeight) div 2 - 30;
  StringsListForm.Top := DefHeight;
  StringsListForm.Left := ProjectItemsForm.Width;
  StringsListForm.Width := (Screen.Width - HexEditForm.Width) - ProjectItemsForm.Width;
  StringsListForm.Height := (HexTextForm.Top - DefHeight) div 2 - 30;
  OriginalTextEditForm.Left := 0;
  OriginalTextEditForm.Width := (Screen.Width - HexEditForm.Width + 1) div 2;
  OriginalTextEditForm.Top := DefHeight + ProjectItemsForm.Height;
  OriginalTextEditForm.Height := HexTextForm.Top - OriginalTextEditForm.Top;
  NewTextForm.Width := (Screen.Width - HexEditForm.Width) div 2;
  NewTextForm.Height := OriginalTextEditForm.Height;
  NewTextForm.Left := OriginalTextEditForm.Width;
  NewTextForm.Top := OriginalTextEditForm.Top;
 end;
 ProjElsMenuItem.Checked := True;
 StringListWinItem.Checked := True;
 OrigTextItem.Checked := True;
 ChangeTextItem.Checked := True;
 ProjectItemsForm.Show;
 StringsListForm.Show;
 OriginalTextEditForm.show;
 NewTextForm.Show;
 If HexEditForm.Visible then
 begin
  HexTextForm.SetFocus;
  HexEditForm.SetFocus;
 end;
 SetFocus;
end;

Procedure Redot(Var S: String);
Var U, D: String; J, I, K: Integer;
begin
 If Dir = '' then Exit;
 If Pos('\', S) < 1 then
 begin
  D := Dir;
  If D[Length(D)] <> '\' then D := D + '\';
  S := D + S;
  Exit;
 end;
 U := S;
 I := 0;
 While Pos('..\', U) > 0 do
 begin
  Delete(U, Pos('..\', U), 3);
  Inc(I);
 end;
 D := Dir;
 If D[Length(D)] <> '\' then D := D + '\';
 For J := 1 to I do
 begin
  K := Length(D) - 1;
  While (K > 0) and (D[K] <> '\') do Dec(k);
  Delete(D, K + 1, Length(D) - (K + 1) + 1);
 end;
 If I > 0 then S := D + U;
end;

Var ExeDir: String;

procedure TKruptarForm.FormShow(Sender: TObject);
Procedure Redot(Var S: String);
Var U, D: String; J, I, K: Integer;
begin
 GetDir(0, D);
 Dir := D;
 If D[Length(D)] <> '\' then D := D + '\';
 If Pos('\', S) < 1 then
 begin
  S := D + S;
  Exit;
 end;
 U := S;
 I := 0;
 While Pos('..\', U) > 0 do
 begin
  Delete(U, Pos('..\', U), 3);
  Inc(I);
 end;
 For J := 1 to I do
 begin
  K := Length(D) - 1;
  While (K > 0) and (D[K] <> '\') do Dec(k);
  Delete(D, K + 1, Length(D) - (K + 1) + 1);
 end;
 If I > 0 then S := D + U;
end;
Var
 S: String; CfgFile: File;
 Fnt: Record
  Height: Integer;
  Pitch: TFontPitch;
  Size: Integer;
  Style: TFontStyles;
 end;
 I: Word; B: Boolean; R: DWord; W: TRect2;
begin
 HexEditForm.RowCount := 16;
 HexEditForm.ColCount := 16;
 Defheight := Height;
 xI := -1;
 S := ParamStr(0);
 ExeDir := '';
 While Pos('\', S) > 0 do
 begin
  Exedir := Exedir + Copy(S, 1, Pos('\', s));
  Delete(s, 1, pos('\', s));
 end;
 If ExeDir = '' then
 begin
  GetDir(0, ExeDir);
  If ExeDir[Length(ExeDir)] <> '\' then ExeDir := ExeDir + '\';
 end;
 If FileExists(ExeDir + 'Kruptar6.cfg') then
 begin
  AssignFile(CfgFile, ExeDir + 'Kruptar6.cfg');
  Reset(CfgFile, 1);
  BlockRead(CfgFile, Fnt, SizeOf(Fnt));
  NewTextForm.Memo.Font.Height := Fnt.Height;
  NewTextForm.Memo.Font.Pitch := Fnt.Pitch;
  NewTextForm.Memo.Font.Size := Fnt.Size;
  NewTextForm.Memo.Font.Style := Fnt.Style;
  BlockRead(CfgFile, I, 2);
  SetLength(S, I);
  BlockRead(CfgFile, S[1], I);
  NewTextForm.Memo.Font.Name := S;
  OriginalTextEditForm.Memo.Font := NewTextForm.Memo.Font;
  HexTextForm.Memo.Font := NewTextForm.Memo.Font;
  BlockRead(CfgFile, B, 1);
  CopyFindItem.Checked := B;
  BlockRead(CfgFile, B, 1);
  OptionsKanjiItem.Checked := B;
  KanjiButton.Down := B;
  BlockRead(CfgFile, RecentCount, 1);
  If RecentCount > 0 then
  begin
   BlockRead(CfgFile, I, 2);
   SetLength(S, I);
   BlockRead(CfgFile, S[1], I);
   Recent1.Caption := S;
   RecentLine.Visible := True;
   Recent1.Visible := True;
   If RecentCount > 1 then
   begin
    BlockRead(CfgFile, I, 2);
    SetLength(S, I);
    BlockRead(CfgFile, S[1], I);
    Recent2.Caption := S;
    Recent2.Visible := True;
    If RecentCount > 2 then
    begin
     BlockRead(CfgFile, I, 2);
     SetLength(S, I);
     BlockRead(CfgFile, S[1], I);
     Recent3.Caption := S;
     Recent3.Visible := True;
     If RecentCount > 3 then
     begin
      BlockRead(CfgFile, I, 2);
      SetLength(S, I);
      BlockRead(CfgFile, S[1], I);
      Recent4.Caption := S;
      Recent4.Visible := True;
     end;
    end;
   end;
  end;
  BlockRead(CfgFile, Byte(B), 1, R);
  If (R = 1) and (Byte(B) = 1) then
  begin
   SavePos.Checked := True;
   BlockRead(CfgFile, B, 1);
   ProjectItemsForm.Visible := B;
   ProjElsMenuItem.Checked := B;
   BlockRead(CfgFile, W, SizeOf(TRect2));
   With ProjectItemsForm do
   begin
    Left := W.Left;
    Top := W.Top;
    ClientWidth:= W.W;
    ClientHeight := W.H;
   end;
   BlockRead(CfgFile, B, 1);
   StringsListForm.Visible := B;
   StringListWinItem.Checked := B;
   BlockRead(CfgFile, W, SizeOf(TRect2));
   With StringsListForm  do
   begin
    Left := W.Left;
    Top := W.Top;
    ClientWidth:= W.W;
    ClientHeight := W.H;
   end;
   BlockRead(CfgFile, B, 1);
   OriginalTextEditForm.Visible := B;
   OrigTextItem.Checked := B;
   BlockRead(CfgFile, W, SizeOf(TRect2));
   With OriginalTextEditForm  do
   begin
    Left := W.Left;
    Top := W.Top;
    ClientWidth:= W.W;
    ClientHeight := W.H;
   end;
   BlockRead(CfgFile, B, 1);
   NewTextForm.Visible := B;
   ChangeTextItem.Checked := B;
   BlockRead(CfgFile, W, SizeOf(TRect2));
   With NewTextForm  do
   begin
    Left := W.Left;
    Top := W.Top;
    ClientWidth:= W.W;
    ClientHeight := W.H;
   end;
   BlockRead(CfgFile, Byte(B), 1, R);   
  end;
  B := (R = 1) and (Byte(B) = 3);
  If B and not HexEditor1.Checked then HexEditor1Click(Sender);
  CloseFile(CfgFile);
 end;
 Caption := Application.Title;
 CurMN := '^';
 If SavePos.Checked then
 begin
  Top := 0;
  Left := 0;
  Defheight := Height;
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);
  Width := Screen.Width;
 end Else InitWindows;
 SW := Screen.Width;
 SH := Screen.Height;
 S := ParamStr(1);
 Redot(S);
 If FileExists(S) then
 begin
  KruptarProject.Open(S);
  Gazzo := KruptarProject.Groups.Root;
  RefreshAll;
 end;
end;

procedure TKruptarForm.FormResize(Sender: TObject);
begin
 ClientHeight := ClHeight;
end;

procedure TKruptarForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 If Created and not Saved then
  Case MessageDlg('������ �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
   mrYes:
   begin
    FileSaveExecute(Sender);
    StringsListForm.StringsListBox.Items.Clear;
    ProjectItemsForm.TreeView.Items.Clear;
    KruptarProject.Done;
   end;
   mrNo:
   begin
    StringsListForm.StringsListBox.Items.Clear;
    ProjectItemsForm.TreeView.Items.Clear;
    KruptarProject.Done;
   end;
   mrCancel: CanClose := False;
  end;
end;

Procedure TKruptarForm.SetRecent(S: String);
Var I: Byte;
    Recent: Array [1..4] of String;
begin
 If S <> '' then
 begin
  Recent[1] := Recent1.Caption;
  Recent[2] := Recent2.Caption;
  Recent[3] := Recent3.Caption;
  Recent[4] := Recent4.Caption;
  For I := 1 to 4 do
  While Pos('&', Recent[I]) > 0 do Delete(Recent[I], Pos('&', Recent[I]), 1);
  If S = Recent[2] then
  begin
   Recent[2] := Recent[1];
   Recent[1] := S;
  end Else
  If S = Recent[3] then
  begin
   Recent[3] := Recent[2];
   Recent[2] := Recent[1];
   Recent[1] := S;
  end Else
  If S = Recent[4] then
  begin
   Recent[4] := Recent[3];
   Recent[3] := Recent[2];
   Recent[2] := Recent[1];
   Recent[1] := S;
  end Else If S <> Recent[1] then
  begin
   Inc(RecentCount);
   Case RecentCount of
    1:
    begin
     Recent[1] := S;
     RecentLine.Visible := True;
     Recent1.Visible := True;
    end;
    2:
    begin
     Recent[2] := Recent[1];
     Recent[1] := S;
     Recent2.Visible := True;
    end;
    3:
    begin
     Recent[3] := Recent[2];
     Recent[2] := Recent[1];
     Recent[1] := S;
     Recent3.Visible := True;
    end;
    4:
    begin
     Recent[4] := Recent[3];
     Recent[3] := Recent[2];
     Recent[2] := Recent[1];
     Recent[1] := S;
     Recent4.Visible := True;
    end;
    5:
    begin
     Recent[4] := Recent[3];
     Recent[3] := Recent[2];
     Recent[2] := Recent[1];
     Recent[1] := S;
     RecentCount := 4;
    end;
   end;
  end;
  Recent1.Caption := Recent[1];
  Recent2.Caption := Recent[2];
  Recent3.Caption := Recent[3];
  Recent4.Caption := Recent[4];
 end;
end;

procedure TKruptarForm.FileSaveExecute(Sender: TObject);
begin
 If Created then
  Case Named of
   False: FileSaveAsExecute(Sender);
   Else
   begin
    KruptarProject.Save;
    SetRecent(KruptarProject.ProjectFile);
   end;
  end;
end;

procedure TKruptarForm.FileSaveAsExecute(Sender: TObject);
begin
 If SaveDialog.Execute then
 begin
  KruptarProject.ProjectFile := SaveDialog.FileName;
  Named := True;
  KruptarProject.Save;
  SetRecent(KruptarProject.ProjectFile);
 end;
end;

Procedure TKruptarForm.RefreshTree;
Var n: PGroup; pn: pposs; CurNode, CurChild: TTreeNode;
begin
 With ProjectItemsForm do
 begin
  TreeView.Items.Clear;
  cCurNode := NIL;
  n := kruptarproject.groups.root;
  While n <> nil do
  begin
   CurNode := TreeView.Items.AddNode(nil, nil, n^.grname, n, naAdd);
   curnode.ImageIndex := 6;
   Curnode.SelectedIndex := 6;
   pn := n^.Posso.root;
   While pn <> nil do
   begin
    If pn^.ptrtable^.Name = '' then
     pn^.ptrtable^.Name := IntToHex(pn^.ptrtable^.pos, 8);
    CurChild := TreeView.Items.AddChild(CurNode, pn^.ptrtable^.Name);
    CurChild.ImageIndex := 0;
    CurChild.SelectedIndex := 0;
    CurChild.Data := pn;
    pn := pn^.next;
   end;
   If Gazzo = N then
   begin
    CurNode.Expand(True);
    CurNode.Selected := True;
   end;
   n := n^.Next;
  end;
  RefreshOriginal;
  RefreshNew;
 end;
end;

procedure TKruptarForm.ProjElsMenuItemClick(Sender: TObject);
begin
 ProjElsMenuItem.Checked := not ProjElsMenuItem.Checked;
 ProjectItemsForm.Visible := ProjElsMenuItem.Checked;
end;

procedure TKruptarForm.StringListWinItemClick(Sender: TObject);
begin
 StringListWinItem.Checked := not StringListWinItem.Checked;
 StringsListForm.Visible := StringListWinItem.Checked;
end;

procedure TKruptarForm.FormActivate(Sender: TObject);
begin
 SaveButton.Enabled := Created;
 FileSaveItem.Enabled := Created;
 FileSaveAsItem.Enabled := Created;
end;

procedure TKruptarForm.OrigTextItemClick(Sender: TObject);
begin
 OrigTextItem.Checked := not OrigTextItem.Checked;
 OriginalTextEditForm.Visible := OrigTextItem.Checked;
end;

procedure TKruptarForm.ChangeTextItemClick(Sender: TObject);
begin
 ChangeTextItem.Checked := not ChangeTextItem.Checked;
 NewTextForm.Visible := ChangeTextItem.Checked;
end;

procedure TKruptarForm.DefaultWindowsClick(Sender: TObject);
begin
 InitWindows;
end;

Function TKruptarForm.GetCurMN: Char;
begin
 If Enter1.Checked then
  Result := '^' Else
 If N11.Checked then
  Result := '*' Else
 If N12.Checked then
  Result := '~' Else
  Result := #13;
end;

procedure TKruptarForm.FormPaint(Sender: TObject);
begin
 If (Screen.Width <> SW) or
    (Screen.Height <> SH) then
 begin
  If SavePos.Checked then Exit;
  SW := Screen.Width;
  Sh := Screen.Height;
  InitWindows;
 end;
end;

procedure TKruptarForm.GroupNewExecute(Sender: TObject);
begin
 ProjectItemsForm.AddGroupButtonClick(Sender);
end;

procedure TKruptarForm.GroupLoadOldExecute(Sender: TObject);
Procedure Redot(Var S: String);
Var U, D: String; J, I, K: Integer;
begin
 If Pos('\', S) < 1 then
 begin
  GetDir(0, D);
  If D[Length(D)] <> '\' then D := D + '\';
  S := D + S;
  Exit;
 end;
 U := S;
 I := 0;
 While Pos('..\', U) > 0 do
 begin
  Delete(U, Pos('..\', U), 3);
  Inc(I);
 end;
 GetDir(0, D);
 If D[Length(D)] <> '\' then D := D + '\';
 For J := 1 to I do
 begin
  K := Length(D) - 1;
  While (K > 0) and (D[K] <> '\') do Dec(k);
  Delete(D, K + 1, Length(D) - (K + 1) + 1);
 end;
 If I > 0 then S := D + U;
end;
Type
 TPtrType = (ptNo, ptNo2, pt2bNES, pt2bNESlen, pt2bNESlen2,
             pt2bNESlen3, pt2bSMD, pt2bSigned, pt3bSNES,
             pt4bGBA, pt4bSMD, pt4bSMDbt2, pt4bSMDpg, ptNone);

Const PNames: Array[TPtrType] of String =
('NoPtr', 'NoPtr2', '2bNES', '2bNESlen', '2bNESlen2',
'2bNESlen3', '2bSMD', '2bSigned', '3bSNES', '4bGBA', '4bSMD',
 '4bSMDbt2', '4bSMDpg', '');

Const PtCount = Byte(pt4bSMDpg);

Procedure Flash(Grp: PGroup);
var n, nn: pposs;
begin
 n := Grp^.Posso.Root;
 nn := Grp^.PossoR.Root;
 while (n <> nil) and (nn <> NIL) do
 begin
  n^.PtrTable^.Name := nn^.PtrTable^.Name;
  n^.GutGut := nn;
  nn^.GutGut := n;
  n := n^.next;
  nn := nn^.next;
 end;
end;
Function FileSplit(S: String): String;
Var i: Integer;
begin
 I := Length(s);
 While s[i] <> '.' do Dec(I);
 Delete(s, I, Length(s) - I + 1);
 I := Length(s);
 While (i > 0) and (s[i] <> '\') do Dec(I);
 Delete(S, 1, I);
 Result := S;
end;
Type
 TCond = (cnHex, cnStr);
Var
 Tmp: ^TGroups; NewC: PGroup; I, SI: Integer; DD, D, TNum, SD: DWord;
 Cond: TCond; RNM, TF: TextFile; S, SS: String; BMore: Boolean; BMoreC: Integer;
 N, Rr: PPosS; SinLl: Word; OldCur: PGroup; PtrType: TPtrType;
Label LMore, LMoreEnd, LMara, Axit, Uxit, Cont;
begin
 If OldOpenDialog.Execute then
 begin
  AssignFile(RNM, OldOpenDialog.FileName);
  Reset(RNM);
  Tmp := Addr(KruptarProject.Groups);
  If TMP = NIL then Exit;
  OldCur := TMP^.Cur;
  If Tmp^.Root = NIL then New(NewC, Create(Tmp)) Else New(NewC, Add(Tmp));
  NewC^.GrName := FileSplit(OldOpenDialog.FileName);
  Readln(RNM, S);
  If UpperCase(S) = 'JAP' then
  begin
   NewC^.xKodir := kdSJIS;
   Readln(RNM, S);
  end;
  Readln(RNM, S);
  bMore := False;
  bMoreC := 0;
  I := 1;
 LMore:
  Readln(RNM, S);
  Readln(RNM, SS);
  Redot(s); Redot(ss);
  If not FileExists(S) then
  begin
   ShowMessage('������� "' + S + '" �� �������!' + #13#10 + '��������� ������.');
   GetDir(0, S);
   If CreateGroupForm.OpenDialog.Execute then
   begin
    ChDir(S);
    S := CreateGroupForm.OpenDialog.FileName;
   end Else
   begin
    CloseFile(RNM);
    Dispose(NewC, Clear);
    TMP^.Cur := OldCur;
    If OldCur = NIL then TMP^.Root := NIL;
    Exit;
   end;
  end;
  If not FileExists(SS) then
  begin
   ShowMessage('������� "' + SS + '" �� �������!' + #13#10 + '��������� ������.');
   GetDir(0, SS);
   If CreateGroupForm.OpenDialog.Execute then
   begin
    ChDir(SS);
    SS := CreateGroupForm.OpenDialog.FileName;
   end Else
   begin
    Goto Uxit;
   Axit:
    ShowMessage('������ � ����� �������!');
   Uxit:
    CloseFile(RNM);
    Dispose(NewC, Clear);
    TMP^.Cur := OldCur;
    If OldCur = NIL then TMP^.Root := NIL;
    Exit;
   end;
  end;
  NewC^.OTables.Add(S, SS);
  If bMore and (I < bMoreC) then
  begin
   Inc(I);
  end Else If bMore then Goto lMoreEnd;
  Readln(RNM, S);
  If UpperCase(S) = '->MORE<-' then
  begin
   Readln(RNM, S);
   If UnsCheckOut(S) then
   begin
    bMoreC := GetLDW(S);
    bMore := True;
    Goto LMore;
   end Else Goto Axit;
  end Else Goto lMara;
 LMoreEnd:
  Readln(RNM, S);
 LMara:
  If NewC = NIL then Exit;
  PtrType := ptNone;
  If Pos(' ', s) = 0 then
  begin
   I := 0;
   While (I <= PtCount) and (UpperCase(S) <> UpperCase(PNames[TPtrType(I)])) do
    Inc(I);
   If I > PtCount then goto Axit;
   PtrType := TPtrType(I);
   NewC^.xNoPtr := False;
   NewC^.xKrat := 1;
   NewC^.xMethod := tmNone;
   NewC^.xPntp := False;
   NewC^.xAutoStart := False;
   Case PtrType of
    ptNo: NewC^.xNoPtr := True;
    ptNo2: With NewC^ do
    begin
     xNoPtr := True;
     xStrLen := 0;
    end;
    pt2bNES: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1T;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen2: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1Te;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen3: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1TL1T;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bSMD: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt2bSigned: With NewC^ do
    begin
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := True;
     xMotorola := True;
    end;
    pt3bSNES: With NewC^ do
    begin
     xPtrSize := 3;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt4bGBA: With NewC^ do
    begin
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt4bSMD: With NewC^ do
    begin
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt4bSMDbt2: With NewC^ do
    begin
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt4bSMDpg: With NewC^ do
    begin
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmC2S1eSne;
     xSigned := False;
     xMotorola := True;
    end;
   end;
   Goto Cont;
  end;
  If Pos(' ', S) > 0 then
  begin
   Ss := Copy(S, 1, Pos(' ', S) - 1);
   Delete(S, 1, Pos(' ', s));
   If UnsCheckOut(ss) then NewC^.xPtrSize := GetLDW(Ss) else goto Axit;
   If NewC^.xPtrSize = 0 then
   begin
    NewC^.xNoPtr := True;
    If UnsCheckOut(s) then NewC^.xStrLen := GetLDW(s) else goto Axit;
    NewC^.xKrat := 1;
    NewC^.xMethod := tmNormal;
   end Else
   begin
    if NewC^.xPtrSize > 4 then
    begin
     Dec(NewC^.xPtrSize, 4);
     NewC^.xPntp := True;
    end Else NewC^.xPntp := False;
    Ss := Copy(S, 1, Pos(' ', s) - 1);
    Delete(S, 1, Pos(' ', s));
    If UnsCheckOut(ss) then NewC^.xKrat := GetLDW(ss) else goto Axit;
    Ss := Copy(S, 1, Pos(' ', s) - 1);
    Delete(S, 1, Pos(' ', s));
    If UnsCheckOut(ss) then Byte(NewC^.xMethod) := GetLDW(ss) else goto Axit;
    Ss := Copy(S, 1, Pos(' ', s) - 1);
    Delete(S, 1, Pos(' ', s));
    If UnsCheckOut(ss) then D := GetLDW(ss) else goto Axit;
    If D > 1 then
    begin
     Dec(d, 2);
     NewC^.xAutoStart := True;
    end Else NewC^.xAutoStart := False;
    NewC^.xSigned := Boolean(D);
    Ss := S;
    If UnsCheckOut(ss) then NewC^.xMotorola := Boolean(GetLDW(ss)) else goto Axit;
   end;
  Cont:
   Readln(RNM, S);
   if s[1] <> '-' then s := 'h' + s;
   If HCheckOut(S) then
   begin
    NewC^.Oofs := GetLDW(S);
   end else goto Axit;
   If (PtrType <> ptNone) and NewC^.xNoPtr then
    NewC^.xStrLen := NewC^.Oofs Else
   NewC^.xFirstPtr := NewC^.Oofs * Dword(not NewC^.xNoPtr);
   ReadLn(RNM, S);
   If UnsCheckout(S) then NewC^.EmptyC := GetLDW(S) else goto Axit;
   For I := 0 to NewC^.EmptyC - 1 do
   begin
    ReadLn(RNM, S);
    S := 'h' + S;
    If UnsCheckout(S) then NewC^.EmptyS^[I].a := DWord(GetLDW(S)) else goto Axit;
    ReadLn(RNM, S);
    S := 'h' + S;
    If UnsCheckout(S) then NewC^.EmptyS^[I].b := DWord(GetLDW(S)) else goto Axit;
   end;
   I := 0;
   While not Eof(RNM) do
   begin
    ReadLn(RNM, s);
    If Pos('=', S) > 0 then
    begin
     D := HexVal(Copy(S, 1, 8));
     TNum := 0;
     If HexError then
     begin
      If UnsCheckOut(Copy(S, 1, Pos(' ', s) - 1)) then
      Tnum := GetLDW(Copy(S, 1, Pos(' ', s) - 1)) else goto Axit;
      Delete(S, 1, pos(' ', s));
      D := Hexval(Copy(S, 1, 8));
      If HexError then goto Axit;
     end;
     AssignFile(Tf, Copy(S, 10, Length(S) - 9));
     Reset(Tf);
     Cond := cnHex;
     Readln(Tf, S);
     If UnsCheckOut(S) then I := GetLDW(s) else goto Axit;
     N := NIL;
     If not (NewC^.xNoPtr and (NewC^.xStrLen = 0) and (NewC^.EmptyC = 0)) then
     begin
      New(N);
      New(N^.PtrTable, Init);
      N^.PtrTable^.Pos := D;
      N^.PtrTable^.Name := IntToHex(D, 8);
      N^.TabN := TNum;
      N^.Next := nil;
     end;
     Dd := 0; Sd := 0; Si := 0;
     SinLl := 0;
     while not Eof(Tf) do
     begin
      ReadLn(Tf, s);
      Case Cond of
       cnHex:
       begin
        Dd := HexVal(Copy(S, 1, Pos(' ', S) - 1));
        If HexError then goto Axit;
        Delete(S, 1, Pos(' ', S));
        if NewC^.xMethod = tmC2S1eSne then
        begin
         If UnsCheckOut(Copy(S, 1, Pos(' ', S) - 1)) then
         Sd := GetLDW(Copy(S, 1, Pos(' ', S) - 1)) else goto Axit;
         Delete(S, 1, Pos(' ', s));
         SinLl := StrToInt(s);
         Inc(Si, Sd);
         Ss := '';
         Cond := cnStr;
        end Else
        begin
         If UnsCheckOut(S) then Sd := GetLDW(S) Else Exit;
         Inc(Si, Sd);
         Ss := '';
         Cond := cnStr;
        end;
       end;
       cnStr:
       begin
        Ss := Ss + S;
        case NewC^.xPtrSize of
         0:
         begin
          ReadLn(Tf, S);
          If NewC^.xNoPtr and (NewC^.xStrLen = 0) and (NewC^.EmptyC = 0) then
          begin
           New(N);
           New(N^.PtrTable, Init);
           N^.PtrTable^.Pos := Dd;
           N^.PtrTable^.Name := IntToHex(Dd, 8);
           N^.TabN := TNum;
           N^.Next := nil;
          end;
          N^.PtrTable^.LoadAdd(Dd, Ss);
          N^.PtrTable^.Cur^.Same := 0;
          N^.PtrTable^.Cur^.SinL := SinLl;
          While Sd > 0 do
          begin
           N^.PtrTable^.LoadAdd(Dd, Ss);
           N^.PtrTable^.Cur^.Same := 0;
           N^.PtrTable^.Cur^.SinL := SinLl;
           Dec(Sd);
          end;
          N^.PtrTable^.BCnt := I;
          If NewC^.xNoPtr and (NewC^.xStrLen = 0) and (NewC^.EmptyC = 0) then
          begin
           NewC^.Posso.AddE(TNum, Dd, N^.PtrTable);
           Rr := NewC^.PossoR.Cur;
           NewC^.PossoR.Cur := N;
           If Rr <> Nil then Rr^.Next := N;
           If NewC^.PossoR.Root = nil then NewC^.PossoR.Root := N;
           Inc(NewC^.PossoR.Count);
          end;
          Cond := cnHex;
         end;
         Else
         begin
          if (NewC^.xMethod in [tmL1T, tmL1Te, tmL1TL1T, tmHat]) and
             (Ss[length(ss)] = '/') then SetLength(Ss, Length(Ss) - 1);
          Readln(Tf, S);
          N^.PtrTable^.LoadAdd(Dd, Ss);
          N^.PtrTable^.Cur^.Same := 0;
          N^.PtrTable^.Cur^.SinL := SinLl;
          While Sd > 0 do
          begin
           N^.PtrTable^.LoadAdd(Dd, Ss);
           N^.PtrTable^.Cur^.Same := 0;
           N^.PtrTable^.Cur^.SinL := SinLl;
           Dec(Sd);
          end;
          Cond := cnHex;
         end;
        end;
       end;
      end;
     end;
     If not (NewC^.xNoPtr and (NewC^.xStrLen = 0) and (NewC^.EmptyC = 0)) then
     begin
      NewC^.Posso.Add(TNum, D, I + Si);
      Rr := NewC^.PossoR.Cur;
      NewC^.PossoR.Cur := N;
      If Rr <> Nil then Rr^.Next := N;
      If NewC^.PossoR.Root = nil then NewC^.PossoR.Root := N;
      Inc(NewC^.PossoR.Count);
     end;
     CloseFile(Tf);
    end;
   end;
  end Else Goto Axit;
  Flash(NewC);
  CloseFile(RNM);
  Saved := False;
  StringsListForm.StringsListBox.Items.Clear;
  ccurnode := NIL;
  RefreshAll;
 end;
end;

procedure TKruptarForm.GroupNewItemExecute(Sender: TObject);
begin
 ProjectItemsForm.AddItemButtonClick(Sender);
end;

procedure TKruptarForm.GroupEditExecute(Sender: TObject);
begin
 ProjectItemsForm.ElParamsButtonClick(Sender);
end;

procedure TKruptarForm.GroupMoveUpExecute(Sender: TObject);
begin
 ProjectItemsForm.MoveUpButtonClick(Sender);
end;

procedure TKruptarForm.GroupMoveDownExecute(Sender: TObject);
begin
 ProjectItemsForm.MoveDownButtonClick(Sender);
end;

procedure TKruptarForm.GroupDeleteExecute(Sender: TObject);
begin
 ProjectItemsForm.DeleteItemButtonClick(Sender);
end;

Function TKruptarForm.Recalculate: Integer;
Var P: Pointer; U: TTreeNode;
Function GLength(Tn: Integer; Ss: WideString): Integer;
Var Sd: WideString; K, I, W, SlCnt: Integer; L: DWord;
begin
 With PGroup(P)^ do
 begin
  i := 1; k := 0;
  SlCnt := 0;
  repeat
   Sd := '';
   For w := 0 to ChMaxLen[tn] - 1 do
   begin
    if i + w > length(ss) then break;
    sd := sd + ss[i + w];
   end;
   inc(i, length(sd) + 1);
   repeat
    l := antitable[sd,tn];
    if endsym then Inc(slcnt);
    SetLength(sd, length(sd) - 1);
    Dec(i);
    if (l = $FFFFFFFF) and (sd = '') then
    begin
     l := antitable[' ',tn];
     if l = $FFFFFFFF then l := 0;
     break;
    end;
   until l <> $FFFFFFFF;
   if l <> $FFFFFFFF then inc(k, tadlen);
  until i > length(ss);
  if xMethod = tmBkp then Inc(k, slcnt * 2 + 2) Else
  If xMethod = tmP2P then Inc(K, xPtrSize);
  result := k;
 end;
end;

Function CopyFind(s: pttable; xx: pptrtable): boolean;
var n: pposs; nt: pttable;
begin
 With PGroup(P)^ do
 begin
  result := false;
  if xNoPtr then Exit;
  if xAutostart then
  begin
   nt := xx^.root;
   if nt = s then exit;
   while nt <> nil do
   begin
    if nt^.text = s^.text then
    begin
     result := true;
     s^.tptr := nt^.tptr;
     exit;
    end;
    nt := nt^.next;
    if nt = s then Exit;
   end;
  end Else
  begin
   n := possor.root;
   while n <> nil do
   begin
    nt := n^.ptrtable^.root;
    if nt = s then exit;
    while nt <> nil do
    begin
     if nt^.text = s^.text then
     begin
      result := true;
      s^.tptr := nt^.tptr;
      exit;
     end;
     nt := nt^.next;
     if nt = s then Exit;
    end;
    n := n^.next;
   end;
  end;
 end;
end;

var n: pposs; nt: pttable; i, j: dword;
    gl: integer;
Label ELoop;
begin
 Result := 0;
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 If U.Level = 1 then U := U.Parent;
 If U = NIL then Exit;
 RecalcForm.RCounter := 0;
 P := U.Data;
 With PGroup(P)^ do
 begin
  if EmptyC = 0 then Exit;
  if xNoPtr and (xStrLen > 0) then exit;
  If PossoR.Count = 0 then Exit;
  n := possor.root;
  I := EmptyS^[0].a; j := 0;
  while n <> nil do
  begin
   nt := n^.ptrtable^.root;
   While nt <> nil do if not CopyFindItem.Checked or not CopyFind(nt, n^.Ptrtable) then
   begin
    Nt^.tptr := i;
    Gl := GLength(n^.tabn, nt^.text) + byte(xMethod in [tmL1T, tmL1TL1T, tmHat]) + 2 *
          byte(xMethod in [tmC2S1eSne, tmL1Te]);
    inc(I, gl);
    if not xNoPtr then while i mod xkrat > 0 do inc(i);
    if xAutostart and (I > emptys^[j].b + 1) then
    begin
     ReCAP := n^.ptrtable;
     Result := 2;
     goto eloop;
    end Else
    if not xAutostart and not xNoPtr and (i > emptys^[j].b + 1) then
    begin
     inc(j);
     if j = DWord(EmptyC) then
     begin
      i := LongInt(EmptyS^[j - 1].b) - gl;
      nt^.tptr := i;
      Result := 1;
      goto eloop;
     end;
     i := emptys^[j].a;
     nt^.tptr := i;
     Inc(i, GLength(n^.tabn, nt^.text) + byte(xMethod in [tmL1T, tmL1TL1T, tmHat]) + 2 *
          byte(xMethod in [tmC2S1eSne, tmL1Te]));
     while i mod xkrat > 0 do inc(i);
    end Else If xNoPtr and (i > emptys^[j].b + 1) then
    begin
     Result := 1;
     goto eloop;
    end;
    nt := nt^.next;
    RecalcForm.RCounter := RCount + 1;
   end Else
   begin
    nt := nt^.next;
    RecalcForm.RCounter := RCount + 1;
   end;
   If xAutostart then
   begin
    Inc(J);
    I := EmptyS^[j].a;
   end;
   n := n^.next;
  end;
 end;
 Eloop:
 Saved := False;
end;

procedure TKruptarForm.GroupRecalcExecute(Sender: TObject);
begin
 RecalcU := ProjectItemsForm.TreeView.Selected;
 If RecalcU = NIL then Exit;
 If RecalcU.Level = 1 then RecalcU := RecalcU.Parent;
 If RecalcU = NIL then Exit;
 RecalcCount := StringsListForm.StringsListBox.Items.Count;
 ProjectItemsForm.Enabled := False;
 StringsListForm.Enabled := False;
 OriginalTextEditForm.Enabled := False;
 NewTextForm.Enabled := False;
 RecalcForm.Show;
 CC := False;
 RecalcForm.Caption := '�������� ��������� - [' + PGroup(ReCalcU.Data)^.GrName + ']';
 RecalcForm.Gauge.MaxValue := RecalcCount;
 RecalcI := Recalculate;
 CC := True;
 RecalcForm.Close;
 Case RecalcI of
  0: RecalcForm.Gauge.Progress := RecalcCount;
  1: ShowMessage('����� �� ��������� � ��������� ��� ���� ������������!');
  2: ShowMessage('����� �������� "' + PPtrTable(ReCap)^.Name + '" �� ��������� � ��������� ��� ���� ������������!');
 end;
 RefreshList;
 ProjectItemsForm.Enabled := True;
 StringsListForm.Enabled := True;
 OriginalTextEditForm.Enabled := True;
 NewTextForm.Enabled := True;
 With PGroup(ReCalcU.Data)^ do
  If (RecalcI = 0) and (xStrLen = 0) and (EmptyC > 0) then
   ShowMessage('������!');
end;

Function TKruptarForm.InsertText: Integer;
Var QUADRO: Pointer; LLU: TTreeNode; ava: ^TROMData;
var ss: WideString; n: pposs; nn: pttable; x, d, e, ZZZ, s: dword; o, i, k: integer; j, l: dword;
var lcn: word; a, b: ^tromdata; type kk = record a, b, c, d: byte end;
type wd = record a, b: byte end;
Procedure AddString(tn: Integer; ss: WideString);
Var sd: WideString; w: Integer; aa: ^TROMData; DU: DWord;
begin
 With PGroup(QUADRO)^ do
 begin
  If xMethod = tmP2P then
  begin
    Inc(ZZZ, 4);
    if xFirstPtr < 0 then du := ZZZ + dword(abs(xFirstPtr))
    Else                  du := ZZZ - dword(abs(xFirstPtr));
    If xShl1 then du := du shr 1;
    aa := Addr(a^[0]);
    Case xPtrSize of
     2:
     begin
      aa^[byte(xMotorola)] := kk(du).a;
      aa^[byte(not xMotorola)] := kk(du).b;
     end;
     3:
     begin
      aa^[(2 * byte(xMotorola))] := kk(du).a;
      aa^[1] := kk(du).b;
      aa^[(2 - 2 * byte(xMotorola))] := kk(du).c;
     end;
     4:
     begin
      aa^[(3 * byte(xMotorola))] := kk(du).a;
      aa^[(1 + byte(xMotorola))] := kk(du).b;
      aa^[(2 - byte(xMotorola))] := kk(du).c;
      aa^[(3 - 3 * byte(xMotorola))] := kk(du).d;
     end
    end;
   aa := Addr(a^[xPtrSize]);
  end Else If xMethod = tmBkp then
  begin
   aa := Addr(ava^[0]);
   a^[0] := 0;
   a^[1] := 0;
  end Else aa := addr(a^[0]);
  i := 1; k := 0; lcn := 0;
  if xMethod in [tmL1T, tmL1Te, tmL1TL1T, tmHat] then inc(k);
  if xMethod = tmL1Te then ss := ss + '/';
  if xMethod = tmC2S1eSne then inc(k, 2);
  repeat
   if (xMethod = tmL1TL1T) and (ss[i] = '^') then
   begin
    aa^[0] := i - 1;
    aa := addr(a^[k]);
    k := 0;
   end;
   sd := '';
   for w := 0 to chmaxlen[tn] - 1 do
   begin
    if i + w > length(ss) then break;
    sd := sd + ss[i + w];
   end;
   o := 1;
   inc(i, length(sd) + 1);
   repeat
    l := antitable[sd, tn];
    If endsym then inc(lcn);
    SetLength(sd, length(sd) - 1);
    Dec(i);
    if (l = $FFFFFFFF) and (sd = '') then
    begin
     l := antitable[' ', tn];
     if l = $FFFFFFFF then l := 0;
     break;
    end;
   until l <> $FFFFFFFF;
   if tadlen = 1 then aa^[k] := kk(l).a Else if tadlen = 2 then
   begin
    o := 2;
    aa^[k    ] := kk(l).b;
    aa^[k + 1] := kk(l).a;
   end Else if tadlen = 3 then
   begin
    aa^[k    ] := kk(l).c;
    aa^[k + 1] := kk(l).b;
    aa^[k + 2] := kk(l).a;
    o := 3;
   end Else
   begin
    aa^[k    ] := kk(l).d;
    aa^[k + 1] := kk(l).c;
    aa^[k + 2] := kk(l).b;
    aa^[k + 3] := kk(l).a;
    o := 4;
   end;
   inc(k, o);
   If endsym and (xMethod = tmBkp) and (i <= length(ss)) then
    Word(Addr(a^[lcn * 2])^) := k;
  until i > length(ss);
  if xMethod in [tmL1T, tmL1Te] then
   aa^[0] := k - 1 - byte(xMethod = tmL1Te) else
  if xMethod = tmHat then
   aa^[0] := (k - 2) div 2
  else if xMethod = tmL1TL1T then
  begin
   aa^[0] := k - 1;
  end else if xMethod = tmC2S1eSne then
  begin
   aa^[0] := wd(nn^.sinl).b;
   aa^[1] := wd(nn^.sinl).a;
  end Else If xMethod = tmBkp then
  begin
   For w := 0 to lcn - 1 do
    Inc(Word(Addr(a^[w * 2])^), lcn * 2 + 2);
   Word(Addr(a^[lcn * 2])^) := $FFFF;
   Move(aa^, a^[lcn * 2 + 2], k);
  end;
 end;
 RecalcForm.RCounter := RCount + 1;
end;

type kk2 = record a, b: char end;
var fst, pntt, xcntx: longint; ffile: File;
begin
 Result := 0;
 RecalcForm.RCounter := 0;
 LLU := ProjectItemsForm.TreeView.Selected;
 If LLU = NIL then Exit;
 If LLU.Level = 1 then LLU := LLU.Parent;
 If LLU = NIL then Exit;
 Quadro := LLU.Data;
 With PGroup(Quadro)^ do
 begin
  AssignFile(ffile, KruptarProject.OutputROM);
  {$I-}
  Reset(Ffile, 1);
  {$I+}
  if IOResult <> 0 then
  begin
   ShowMessage('���� �� ������!');
   Exit;
  end;
  GetMem(ava, $10000);
  s := filesize(ffile);
  for i := 0 to emptyc - 1 do
   if emptys^[i].b > s then
    s := emptys^[i].b + 1;
  GetMem(b, s);
  BlockRead(FFile, b^, s, d);
  For I := 0 to EmptyC - 1 do
   Fillchar(B^[EmptyS^[i].a], EmptyS^[i].b - EmptyS^[i].a + 1, 0);
  N := PossoR.Root;
  xcntx := 0;
  If not xNoPtr then while n <> nil do
  begin
   nn := n^.ptrtable^.root;
   j := 0;
   if xpntp then
   begin
    xcntx := 0;
    while nn <> nil do
    begin
     inc(xcntx, 1 + nn^.same);
     nn := nn^.next;
    end;
    nn := n^.ptrtable^.root;
   end;
   while nn <> nil do
   begin
    if not xpntp then fst := j * dword(xPtrSize + xInterval) else fst := j;
    e := n^.ptrtable^.pos + dword(fst);
    If xAutoStart then xFirstPtr := n^.ptrtable^.pos;
    if xFirstPtr < 0 then d := nn^.tptr + dword(abs(xFirstPtr))
    Else                  d := nn^.tptr - dword(abs(xFirstPtr));
    If xSigned then
    begin
     if xPtrSize = 2 then
     begin
      Dec(d, e);
      longint(d) := smallint(d);
     end Else Dec(d, e);
    end;
    If xShl1 then d := d shr 1;
    a := addr(b^[e]);
    inc(j);
    if not xpntp then pntt := 1 else pntt := xcntx;
    Case xPtrSize of
     2: for x := 0 to nn^.same do
     begin
      if not xpntp then fst := x * dword(xPtrSize) else fst := x;
      a^[fst + byte(xMotorola) * pntt] := kk(d).a;
      a^[fst + byte(not xMotorola) * pntt] := kk(d).b;
     end;
     3: for x := 0 to nn^.same do
     begin
      if not xpntp then fst := x * dword(xPtrSize) else fst := x;
      a^[fst + (2 * byte(xMotorola)) * pntt] := kk(d).a;
      a^[fst + 1 * pntt] := kk(d).b;
      a^[fst + (2 - 2 * byte(xMotorola)) * pntt] := kk(d).c;
     end;
     4: for x := 0 to nn^.same do
     begin
      if not xpntp then fst := x * dword(xPtrSize) else fst := x;
      a^[fst + (3 * byte(xMotorola)) * pntt] := kk(d).a;
      a^[fst + (1 + byte(xMotorola)) * pntt] := kk(d).b;
      a^[fst + (2 - byte(xMotorola)) * pntt] := kk(d).c;
      a^[fst + (3 - 3 * byte(xMotorola)) * pntt] := kk(d).d;
     end
    end;
    inc(j, nn^.same);
    ss := nn^.text;
    a := addr(b^[nn^.tptr]);
    ZZZ := nn^.tptr;
    AddString(n^.tabn, ss);
    nn := nn^.next;
   end;
   n := n^.next;
  end Else
  begin
   while n <> nil do
   begin
    nn := n^.ptrtable^.root;
    while nn <> nil do
    begin
     ss := nn^.text;
     A := addr(b^[nn^.tptr]);
     AddString(n^.tabn, ss);
     j := 0;
     if xStrLen > 0 then
      for i := k to xStrLen - 1 do
      begin
       l := antitable[' ', n^.tabn];
       if tadlen = 1 then
       begin
        a^[i] := kk(l).a;
        o := 1;
       end Else
       if tadlen = 2 then
       begin
        o := 2;
        a^[k + integer(j)    ] := kk(l).b;
        a^[k + integer(j) + 1] := kk(l).a;
       end Else
       if tadlen = 3 then
       begin
        a^[k + integer(j)    ] := kk(l).c;
        a^[k + integer(j) + 1] := kk(l).b;
        a^[k + integer(j) + 2] := kk(l).a;
        o := 3;
       end Else
       begin
        a^[k + integer(j)    ] := kk(l).d;
        a^[k + integer(j) + 1] := kk(l).c;
        a^[k + integer(j) + 2] := kk(l).b;
        a^[k + integer(j) + 3] := kk(l).a;
        o := 4;
       end;
       inc(j, o);
      end;
     nn := nn^.next;
    end;
    n := n^.next;
   end;
  end;
  CloseFile(ffile);
  AssignFile(ffile, KruptarProject.OutputROM);
  Rewrite(FFile, 1);
  BlockWrite(ffile, b^, s);
  CloseFile(ffile);
  FreeMem(b, s);
  Freemem(ava, $10000);
 end;
end;

procedure TKruptarForm.GroupInsertExecute(Sender: TObject);
begin
 RecalcU := ProjectItemsForm.TreeView.Selected;
 If RecalcU = NIL then Exit;
 If RecalcU.Level = 1 then RecalcU := RecalcU.Parent;
 If RecalcU = NIL then Exit;
 RecalcCount := StringsListForm.StringsListBox.Items.Count;
 ProjectItemsForm.Enabled := False;
 StringsListForm.Enabled := False;
 OriginalTextEditForm.Enabled := False;
 NewTextForm.Enabled := False;
 RecalcForm.Show;
 CC := False;
 RecalcForm.Caption := '������� ������ - [' + PGroup(ReCalcU.Data)^.GrName + ']';
 RecalcForm.Gauge.MaxValue := RecalcCount;
 RecalcI := InsertText;
 CC := True;
 RecalcForm.Gauge.Progress := RecalcCount;
 RecalcForm.Close;
 ProjectItemsForm.Enabled := True;
 StringsListForm.Enabled := True;
 OriginalTextEditForm.Enabled := True;
 NewTextForm.Enabled := True;
 If RecalcI = 0 then ShowMessage('������!');
end;

procedure TKruptarForm.ProjectDoallExecute(Sender: TObject);
Var U: TTreeNode;
begin
 If ProjectItemsForm.TreeView.Items.Count < 1 then Exit;
 ProjectItemsForm.Enabled := False;
 StringsListForm.Enabled := False;
 OriginalTextEditForm.Enabled := False;
 NewTextForm.Enabled := False;
 U := ProjectItemsForm.TreeView.Selected;
 ProjectItemsForm.TreeView.Select(ProjectItemsForm.TreeView.Items.GetFirstNode, []);
 RecalcU := ProjectItemsForm.TreeView.Selected;
 While RecalcU <> NIL do
 begin
  ProjectItemsForm.TreeView.Select(RecalcU, []);
  RefreshList;
  RecalcCount := StringsListForm.StringsListBox.Items.Count;
  RecalcForm.Show;
  CC := False;
  RecalcForm.Caption := '�������� ��������� - [' + PGroup(ReCalcU.Data)^.GrName + ']';
  RecalcForm.Gauge.MaxValue := RecalcCount;
  RecalcI := Recalculate;
  CC := True;
  RecalcForm.Close;
  Case RecalcI of
   0:
   begin
    RecalcForm.Gauge.Progress := RecalcCount;
    RecalcCount := StringsListForm.StringsListBox.Items.Count;
    RecalcForm.Show;
    CC := False;
    RecalcForm.Caption := '������� ������ - [' + PGroup(ReCalcU.Data)^.GrName + ']';
    RecalcForm.Gauge.MaxValue := RecalcCount;
    RecalcI := InsertText;
    CC := True;
    RecalcForm.Gauge.Progress := RecalcCount;
    RecalcForm.Close;
   end;
   1: ShowMessage('����� �� ��������� � ��������� ��� ���� ������������!');
   2: ShowMessage('����� �������� "' + PPtrTable(ReCap)^.Name + '" �� ��������� � ��������� ��� ���� ������������!');
  end;
  If RecalcI in [1, 2] then Break;
  RecalcU := RecalcU.GetNextSibling;
 end;
 ProjectItemsForm.TreeView.Select(U, []);
 RefreshList;
 Saved := False;
 ProjectItemsForm.Enabled := True;
 StringsListForm.Enabled := True;
 OriginalTextEditForm.Enabled := True;
 NewTextForm.Enabled := True;
 If RecalcI = 0 then ShowMessage('������!');
end;

procedure TKruptarForm.ProjectDogroupExecute(Sender: TObject);
begin
 RecalcU := ProjectItemsForm.TreeView.Selected;
 If RecalcU = NIL then Exit;
 If RecalcU.Level = 1 then RecalcU := RecalcU.Parent;
 If RecalcU = NIL then Exit;
 RecalcCount := StringsListForm.StringsListBox.Items.Count;
 ProjectItemsForm.Enabled := False;
 StringsListForm.Enabled := False;
 OriginalTextEditForm.Enabled := False;
 NewTextForm.Enabled := False;
 RecalcForm.Show;
 CC := False;
 RecalcForm.Caption := '�������� ��������� - [' + PGroup(ReCalcU.Data)^.GrName + ']';
 RecalcForm.Gauge.MaxValue := RecalcCount;
 RecalcI := Recalculate;
 CC := True;
 RecalcForm.Close;
 Case RecalcI of
  0:
  begin
   RecalcForm.Gauge.Progress := RecalcCount;
   RecalcCount := StringsListForm.StringsListBox.Items.Count;
   RecalcForm.Show;
   CC := False;
   RecalcForm.Caption := '������� ������ - [' + PGroup(ReCalcU.Data)^.GrName + ']';
   RecalcForm.Gauge.MaxValue := InsertText;
   CC := True;
   RecalcForm.Gauge.Progress := RecalcCount;
   RecalcForm.Close;
  end;
  1: ShowMessage('����� �� ��������� � ��������� ��� ���� ������������!');
  2: ShowMessage('����� �������� "' + PPtrTable(ReCap)^.Name + '" �� ��������� � ��������� ��� ���� ������������!');
 end;
 RefreshList;
 Saved := False;
 ProjectItemsForm.Enabled := True;
 StringsListForm.Enabled := True;
 OriginalTextEditForm.Enabled := True;
 NewTextForm.Enabled := True;
 If RecalcI = 0 then ShowMessage('������!');
end;

procedure TKruptarForm.GroupRestoreTextExecute(Sender: TObject);
begin
 ProjectItemsForm.TreeViewDblClick(Sender);
end;

procedure TKruptarForm.GroupInfoExecute(Sender: TObject);
Function YesNo(N: Boolean): String;
begin
 If N then Result := '��' Else Result := '���';
end;
Function EncodeS(N: Integer): String;
begin
 Case N of
  kdANSI: Result := 'ANSI';
  kdSJIS: Result := 'SHIFT-JIS';
  kdUNIC: Result := 'UNICODE';
  Else Result := 'o.O';
 end;
end;
Function GetMethod(N: TPMetT): String;
begin
 Case N of
  tmNormal: Result := 'tmNormal';
  tmL1T: Result := 'tmL1T';
  tmL1Te: Result := 'tmL1Te';
  tmL1TL1T: Result := 'tmL1TL1T';
  tmC2S1eSne: Result := 'tmC2S1eSne';
  tmHat: Result := 'tmHat';
  tmBkp: Result := 'tmBkp';
  tmP2P: Result := 'tmPtr2Ptr';
  Else Result := 'tmNone';
 end;
end;
Var U: TTreeNode; D: Pointer; I: Integer;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 D := U.Data;
 Case U.Level of
  0: With PGroup(U.Data)^ do
  begin
   InfoForm.Caption := GrName;
   With InfoForm.InfoMemo do
   begin
    Clear;
    Case xNoPtr of
     False:
     begin
      Lines.Add('������ ��������: ' + IntToStr(xPtrSize));
      Lines.Add('���������: ' + IntToStr(xKrat));
      Lines.Add('�����: ' + GetMethod(xMethod));
      Lines.Add('Motorola: ' + YesNo(xMotorola));
      Lines.Add('Signed: ' + YesNo(xSigned));
      Lines.Add('���������: ' + EncodeS(xKodir));
      If not xAutoStart then
       Lines.Add('������� ��������: ' + IntToHex(xFirstPtr, 8));
      If xPntp then
       Lines.Add('���: PunType') Else
      If xAutoStart then
       Lines.Add('���: AutoStart') Else
       Lines.Add('���: Normal');
     end;
     True:
     begin
      Lines.Add('��� ���������');
      If xStrLen > 0 then Lines.Add('����� ������: ' + IntToStr(xStrLen));
     end;
    end;
    Lines.Add('������� 1: ' + OTables.GetNameO(0));
    Lines.Add('������� 2: ' + OTables.GetNameE(0));
    If EmptyC = 0 then Lines.Add('����� ��� ������: ���') Else
    begin
     Lines.Add('����� ��� ������:');
     For I := 0 to EmptyC - 1 do
     begin
      Lines.Add(IntToHex(EmptyS^[I].a, 8));
      Lines.Add(IntToHex(EmptyS^[I].b, 8));
     end;
    end;
    SelStart := 0;
   end;
  end;
  1: With PGroup(U.Parent.Data)^ do
  begin
   InfoForm.Caption := GrName + ' / ' + PPoss(D)^.PtrTable^.Name;
   With InfoForm.InfoMemo do
   begin
    Clear;
    Lines.Add('������� 1: ' + OTables.GetNameO(PPoss(D)^.TabN));
    Lines.Add('������� 2: ' + OTables.GetNameE(PPoss(D)^.TabN));
    Case xNoPtr of
     False: With PPoss(D)^.PtrTable^ do
     begin
      Lines.Add('������ ����� ���������: ' + IntToHex(Pos, 8));
      Lines.Add('����� ����� ���������: ' +
                 IntToHex(Pos + Dword(Count) * Dword(xPtrSize) - 1, 8));
     end;
     True: With PPoss(D)^.PtrTable^ do If xStrLen = 0 then
     begin
      Lines.Add('������ ���������� �����: ' + IntToHex(Pos, 8));
      Lines.Add('����� ���������� �����: ' +
                 IntToHex(Pos + Dword(BCnt) - 1, 8));
     end Else
     begin
      Lines.Add('������ ���������� �����: ' + IntToHex(Pos, 8));
      Lines.Add('����� ���������� �����: ' +
                 IntToHex(Pos + Dword(BCnt) * Dword(xStrLen) - 1, 8));
     end;
    end;
   end;
  end;
 end;
 InfoForm.ShowModal;
end;

procedure TKruptarForm.CopyFindItemClick(Sender: TObject);
begin
 CopyFindItem.Checked := not CopyFindItem.Checked;
end;

procedure TKruptarForm.OptionsKanjiExecute(Sender: TObject);
begin
 OptionsKanjiItem.Checked := not OptionsKanjiItem.Checked;
 KanjiButton.Down := OptionsKanjiItem.Checked;
end;

procedure TKruptarForm.FileCloseExecute(Sender: TObject);
begin
 If Created then
 begin
  If not Saved and (MessageDlg('������� ������ �� ��������. ���������?',
     mtWarning, [mbYes, mbNo, mbCancel], 0) = mrYes) then
     FileSaveExecute(Sender);
  StringsListForm.StringsListBox.Items.Clear;
  ProjectItemsForm.TreeView.Items.Clear;
  Created := False;
  Saved := false;
  Named := false;
  KruptarProject.Done;
  Gazzo := NIL;
  RefreshAll;
  FormActivate(Sender);
 end;
end;

procedure TKruptarForm.OptionsFontExecute(Sender: TObject);
begin
 FontDialog.Font := NewTextForm.Memo.Font;
 If FontDialog.Execute then
 begin
  OriginalTextEditForm.Memo.Font := FontDialog.Font;
  NewTextForm.Memo.Font := FontDialog.Font;
  HexTextForm.Memo.Font := FontDialog.Font;
 end;
end;

procedure TKruptarForm.Recent1Click(Sender: TObject);
Var FName: String;
begin
 If not TMenuItem(Sender).Visible then Exit;
 Case TMenuItem(Sender).Tag of
  1: FName := Recent1.Caption;
  2: FName := Recent2.Caption;
  3: FName := Recent3.Caption;
  4: FName := Recent4.Caption;
 end;
 While Pos('&', FName) > 0 do Delete(FName, Pos('&', FName), 1);
 Redot(FName);
 If not FileExists(FName) then
 begin
  ShowMessage('���� �� ������!');
  Exit;
 end;
 If Created and not Saved then
 Case MessageDlg('������� ������ �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
  mrYes: FileSaveExecute(Sender);
  mrCancel: Exit;
 end;
 StringsListForm.StringsListBox.Items.Clear;
 ProjectItemsForm.TreeView.Items.Clear;
 Created := False;
 Saved := false;
 Named := false;
 KruptarProject.Done;
 KruptarProject.Open(FName);
 Gazzo := KruptarProject.Groups.Root;
 RefreshAll;
 FormActivate(Sender);
end;

procedure TKruptarForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
Var S: String; CfgFile: File;
    Fnt: Record
     Height: Integer;
     Pitch: TFontPitch;
     Size: Integer;
     Style: TFontStyles;
    end;
    I: Word; B: Boolean; W: TRect2;
begin
 AssignFile(CfgFile, ExeDir + 'Kruptar6.cfg');
 Rewrite(CfgFile, 1);
 Fnt.Height := NewTextForm.Memo.Font.Height;
 Fnt.Pitch := NewTextForm.Memo.Font.Pitch;
 Fnt.Size := NewTextForm.Memo.Font.Size;
 Fnt.Style := NewTextForm.Memo.Font.Style;
 Blockwrite(CfgFile, Fnt, SizeOf(Fnt));
 S := NewTextForm.Memo.Font.Name;
 I := Length(S);
 Blockwrite(CfgFile, I, 2);
 Blockwrite(CfgFile, S[1], I);
 B := CopyFindItem.Checked;
 Blockwrite(CfgFile, B, 1);
 B := OptionsKanjiItem.Checked;
 Blockwrite(CfgFile, B, 1);
 Blockwrite(CfgFile, RecentCount, 1);
 If RecentCount > 0 then
 begin
  S := Recent1.Caption;
  While Pos('&', S) > 0 do Delete(S, Pos('&', S), 1);
  I := Length(S);
  Blockwrite(CfgFile, I, 2);
  Blockwrite(CfgFile, S[1], I);
  If RecentCount > 1 then
  begin
   S := Recent2.Caption;
   While Pos('&', S) > 0 do Delete(S, Pos('&', S), 1);
   I := Length(S);
   Blockwrite(CfgFile, I, 2);
   Blockwrite(CfgFile, S[1], I);
   If RecentCount > 2 then
   begin
    S := Recent3.Caption;
    While Pos('&', S) > 0 do Delete(S, Pos('&', S), 1);
    I := Length(S);
    Blockwrite(CfgFile, I, 2);
    Blockwrite(CfgFile, S[1], I);
    If RecentCount > 3 then
    begin
     S := Recent4.Caption;
     While Pos('&', S) > 0 do Delete(S, Pos('&', S), 1);
     I := Length(S);
     Blockwrite(CfgFile, I, 2);
     Blockwrite(CfgFile, S[1], I);
    end;
   end;
  end;
 end;
 If SavePos.Checked then
 begin
  B := True;
  Blockwrite(CfgFile, B, 1);
  B := ProjectItemsForm.Visible;
  Blockwrite(CfgFile, B, 1);
  With ProjectItemsForm do
  begin
   W.Left := Left;
   W.Top := Top;
   W.W := ClientWidth;
   W.H := ClientHeight;
  end;
  Blockwrite(CfgFile, W, SizeOf(TRect2));
  B := StringsListForm.Visible;
  Blockwrite(CfgFile, B, 1);
  With StringsListForm do
  begin
   W.Left := Left;
   W.Top := Top;
   W.W := ClientWidth;
   W.H := ClientHeight;
  end;
  Blockwrite(CfgFile, W, SizeOf(TRect2));
  B := OriginalTextEditForm.Visible;
  Blockwrite(CfgFile, B, 1);
  With OriginalTextEditForm do
  begin
   W.Left := Left;
   W.Top := Top;
   W.W := ClientWidth;
   W.H := ClientHeight;
  end;
  Blockwrite(CfgFile, W, SizeOf(TRect2));
  B := NewTextForm.Visible;
  Blockwrite(CfgFile, B, 1);
  With NewTextForm do
  begin
   W.Left := Left;
   W.Top := Top;
   W.W := ClientWidth;
   W.H := ClientHeight;
  end;
  Blockwrite(CfgFile, W, SizeOf(TRect2));
 end;
 Byte(B) := Byte(HexEditor1.Checked) + 2;
 Blockwrite(CfgFile, Byte(B), 1);
 CloseFile(CfgFile);
end;

Var bbFind, bbReplace: Boolean;
    Orig: Boolean; bbFind1, bbReplace1: Boolean;

Procedure TKruptarForm.ppFind(U: TTreeNode; S: WideString);
Var Q, R: Pointer; SS, SW: WideString; I, J, CI: Integer;
    Mmemo: TUnicodeMemo; UPS: TTreeNode; W: WideString;
Label Suda;
begin
 If U = NIL then Exit;
 If U.Level = 1 then U := U.Parent;
 If Orig then
  Mmemo := OriginalTextEditForm.Memo Else
  Mmemo := NewTextForm.Memo;
 W := '';
 With Mmemo do
 begin
  J := SelLength; SelLength := 0;
  SelStart := SelStart + J;
  SelLength := Length(WideText) - Selstart;
  J := Pos(S, SelWideText);
  If J > 0 then
  begin
   SelLength := 0;
   SelStart := SelStart + J - 1;
   SelLength := Length(S);
   SetFocus;
   Exit;
  end;
  SelLength := 0;
  W := WideText;
  Lines.Clear;
 end;
 cI := StringsListForm.StringsListBox.ItemIndex;
 If cI < 0 then cI := 0;
 If W <> '' then Inc(cI);
 SW := SJISText(S);
 R := NIL;
 While U <> NIL do With PGroup(U.Data)^ do
 begin
  If xKodir = kdSJIS then SS := SW Else SS := S;
  If not Orig then Q := PossoR.Root Else Q := Posso.Root;
  If cI > 0 then
  begin
   I := 0;
   While Q <> NIL do With PPoss(Q)^ do
   begin
    R := PtrTable^.Root;
    While R <> NIL do With PtTable(R)^ do
    begin
     If I = cI then Goto Suda;
     R := Next;
     Inc(I);
    end;
    Q := Next;
   end;
  end Else I := 0;
  Suda:
  While Q <> NIL do With PPoss(Q)^ do
  begin
   If R = NIL then R := PtrTable^.Root;
   While R <> NIL do With PtTable(R)^ do
   begin
    If Pos(SS, Text) > 0 then
    begin
     UPS := ProjectItemsForm.TreeView.Selected;
     If UPS = NIL then Exit;
     If UPS.Level = 1 then UPS := UPS.Parent;
     If (U <> UPS) then
     begin
      QuietSel := True;
      ProjectItemsForm.TreeView.Select(U, []);
      With StringsListForm.StringsListBox do
      begin
       RefreshList;
       ItemIndex := Items.Count - 1;
       While ItemIndex <> I do ItemIndex := I;
       StringsListForm.StringsListBoxClick(StringsListForm.StringsListBox);
       RefreshOriginal;
       RefreshNew;
      end;
     end Else With StringsListForm.StringsListBox do
     begin
      ItemIndex := Items.Count - 1;
      While ItemIndex <> I do ItemIndex := I;
      StringsListForm.StringsListBoxClick(StringsListForm.StringsListBox);
     end;
     With Mmemo do
     begin
      J := SelLength; SelLength := 0;
      SelStart := SelStart + J;
      SelLength := Length(WideText) - Selstart;
      J := Pos(S, SelWideText);
      If J > 0 then
      begin
       SelLength := 0;
       SelStart := SelStart + J - 1;
       SelLength := Length(S);
       SetFocus;
       Exit;
      end Else If Lines.Count > 0 then
      begin
       SelLength := 0;
       SetFocus;
       Exit;
      end;
      SelLength := 0;
     end;
    end;
    Inc(I);
    R := Next;
   end;
   Q := Next;
  end;
  U := U.GetNextSibling;
  cI := 0;
 end;
 MMemo.WideText := W;
 ShowMessage('������� ������ �� �������!');
end;

Procedure TKruptarForm.ppReplace(U: TTreeNode; S, Rs: WideString);
Var Q, R: Pointer; SS, SW: WideString; I, J, CI: Integer;
    Mmemo: TUnicodeMemo; UPS: TTreeNode; W: WideString;
Label StartSearch, Suda;
begin
 If U = NIL then Exit;
 If U.Level = 1 then U := U.Parent;
 Mmemo := NewTextForm.Memo;
 SW := SJISText(S);
 StartSearch:
 W := '';
 With Mmemo do
 begin
  J := SelLength; SelLength := 0;
  SelStart := SelStart + J;
  SelLength := Length(WideText) - Selstart;
  J := Pos(S, SelWideText);
  If J > 0 then
  begin
   SelLength := 0;
   SelStart := SelStart + J - 1;
   SelLength := Length(S);
   SetFocus;
   If FindReplaceForm.ConfirmReplaceCheck.Checked then
   begin
    Case MessageDlg('�������� ��� ������?', mtConfirmation,
     [mbYes, mbNo, mbYesToAll, mbCancel], 0) of
     mrYes:
     begin
      SelWideText := Rs;
      If FindReplaceForm.ReplaceAllCheck.Checked then
      begin
       J := SelLength; SelLength := 0;
       SelStart := SelStart + J;
       Goto StartSearch;
      end;
     end;
     mrNo: If FindReplaceForm.ReplaceAllCheck.Checked then
     begin
      J := SelLength; SelLength := 0;
      SelStart := SelStart + J;
      Goto StartSearch;
     end;
     mrYesToAll:
     begin
      SelWideText := Rs;
      J := SelLength; SelLength := 0;
      SelStart := SelStart + J;
      FindReplaceForm.ReplaceAllCheck.Checked := True;
      FindReplaceForm.ConfirmReplaceCheck.Checked := False;
      Goto StartSearch;
     end;
    end;
   end Else
   begin
    SelWideText := Rs;
    If FindReplaceForm.ReplaceAllCheck.Checked then
    begin
     J := SelLength; SelLength := 0;
     SelStart := SelStart + J;
     Goto StartSearch;
    end;
   end;
   Exit;
  end;
  SelLength := 0;
  W := WideText;
  Lines.Clear;
 end;
{ cI := StringsListForm.StringsListBox.SelectionModel.MaxSelectionIndex - 1;
 If cI < 0 then cI := 0;
 If W <> '' then Inc(cI);}
 cI := StringsListForm.StringsListBox.ItemIndex;
 If cI < 0 then cI := 0;
 If W <> '' then Inc(cI);
 SW := SJISText(S);
 R := NIL;
 While U <> NIL do With PGroup(U.Data)^ do
 begin
  If xKodir = kdSJIS then SS := SW Else SS := S;
  Q := PossoR.Root;
  If cI > 0 then
  begin
   I := 0;
   While Q <> NIL do With PPoss(Q)^ do
   begin
    R := PtrTable^.Root;
    While R <> NIL do With PtTable(R)^ do
    begin
     If I = cI then Goto Suda;    
     R := Next;
     Inc(I);
    end;
    Q := Next;
   end;
  end Else I := 0;
  Suda:
  While Q <> NIL do With PPoss(Q)^ do
  begin
   If R = NIL then R := PtrTable^.Root;
   While R <> NIL do With PtTable(R)^ do
   begin
    If Pos(SS, Text) > 0 then
    begin
     UPS := ProjectItemsForm.TreeView.Selected;
     If UPS = NIL then Exit;
     If UPS.Level = 1 then UPS := UPS.Parent;
     If (U <> UPS) then
     begin
      QuietSel := True;
      ProjectItemsForm.TreeView.Select(U, []);
      With StringsListForm.StringsListBox do
      begin
       RefreshList;
       ItemIndex := Items.Count - 1;
       ItemIndex := I;
       If ItemIndex <> I then
       begin
        While ItemIndex <> I do ItemIndex := I;
        StringsListForm.StringsListBoxClick(StringsListForm.StringsListBox);
       end;
       RefreshOriginal;
       RefreshNew;
      end;
     end Else With StringsListForm.StringsListBox do
     begin
      ItemIndex := Items.Count - 1;
      ItemIndex := I;
      If ItemIndex <> I then
      begin
       While ItemIndex <> I do ItemIndex := I;
       StringsListForm.StringsListBoxClick(StringsListForm.StringsListBox);
      end;
     end;
     With Mmemo do
     begin
      J := SelLength; SelLength := 0;
      SelStart := SelStart + J;
      SelLength := Length(WideText) - Selstart;
      J := Pos(S, SelWideText);
      If J > 0 then
      begin
       SelLength := 0;
       SelStart := SelStart + J - 1;
       SelLength := Length(S);
       SetFocus;
       If FindReplaceForm.ConfirmReplaceCheck.Checked then
       begin
        Case MessageDlg('�������� ��� ������?', mtConfirmation,
         [mbYes, mbNo, mbYesToAll, mbCancel], 0) of
         mrYes:
         begin
          SelWideText := Rs;
          If FindReplaceForm.ReplaceAllCheck.Checked then
          begin
           J := SelLength; SelLength := 0;
           SelStart := SelStart + J;
           Goto StartSearch;
          end;
         end;
         mrNo: If FindReplaceForm.ReplaceAllCheck.Checked then
         begin
          J := SelLength; SelLength := 0;
          SelStart := SelStart + J;
          Goto StartSearch;
         end;
         mrYesToAll:
         begin
          SelWideText := Rs;
          J := SelLength; SelLength := 0;
          SelStart := SelStart + J;
          FindReplaceForm.ReplaceAllCheck.Checked := True;
          FindReplaceForm.ConfirmReplaceCheck.Checked := False;
          Goto StartSearch;
         end;
        end;
       end Else
       begin
        SelWideText := Rs;
        If FindReplaceForm.ReplaceAllCheck.Checked then
        begin
         J := SelLength; SelLength := 0;
         SelStart := SelStart + J;
         Goto StartSearch;
        end;
       end;
       Exit;
      end;
      SelLength := 0;
     end;
    end;
    Inc(I);
    R := Next;
   end;
   Q := Next;
  end;
  U := U.GetNextSibling;
  cI := 0;
 end;
 If StringsListForm.StringsListBox.ItemIndex >= 0 then MMemo.WideText := W;
 If not FindReplaceForm.ReplaceAllCheck.Checked then
 ShowMessage('������� ������ �� �������!');
end;

procedure TKruptarForm.SearchFindBeforeExecute(Sender: TObject);
Var U: TTreeNode;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 FindReplaceForm.PageControl.ActivePageIndex := 0;
 If FindReplaceForm.Execute then
 begin
  Case FindReplaceForm.PageControl.ActivePageIndex of
   0:
   begin
    bbFind := True;
    bbReplace := False;
    Orig := True;
    ppFind(U, FindReplaceForm.FEdit.WideText);
   end;
   1:
   begin
    bbFind1 := True;
    bbReplace1 := True;
    ppReplace(U, FindReplaceForm.RFEdit.WideText,
                 FindReplaceForm.REdit.WideText);
   end;
  end;
 end;
end;

procedure TKruptarForm.SearchFindNextHint(Sender: TObject);
Var U: TTreeNode;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 If bbFind then
 begin
  If not bbReplace then
  begin
   Orig := True;
   ppFind(U, FindReplaceForm.FEdit.WideText);
  end;
 end Else SearchFindBeforeExecute(Sender);
end;

procedure TKruptarForm.ActionInsertExecute(Sender: TObject);
begin
 InsPressed := not InsPressed;
end;

procedure TKruptarForm.Search2FindExecute(Sender: TObject);
Var U: TTreeNode;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 FindReplaceForm.PageControl.ActivePageIndex := 0;
 If FindReplaceForm.Execute then
  Case FindReplaceForm.PageControl.ActivePageIndex of
   0:
   begin
    bbFind1 := True;
    bbReplace1 := False;
    Orig := False;
    ppFind(U, FindReplaceForm.FEdit.WideText);
   end;
   1:
   begin
    bbFind1 := True;
    bbReplace1 := True;
    ppReplace(U, FindReplaceForm.RFEdit.WideText,
                 FindReplaceForm.REdit.WideText);
   end;
  end;
end;

procedure TKruptarForm.Search2FindNextExecute(Sender: TObject);
Var U: TTreeNode;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 If bbFind1 then Case bbReplace1 of
  False:
  begin
   Orig := False;
   ppFind(U, FindReplaceForm.FEdit.WideText);
  end;
  True:
  begin
   FindReplaceForm.ReplaceAllCheck.Checked := False;
   ppReplace(U, FindReplaceForm.RFEdit.WideText,
                FindReplaceForm.REdit.WideText);
  end;
 end Else Search2FindExecute(Sender);
end;

procedure TKruptarForm.Search2ReplaceExecute(Sender: TObject);
Var U: TTreeNode;
begin
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 FindReplaceForm.PageControl.ActivePageIndex := 1;
 FindReplaceForm.ReplaceTab.Enabled := True;
 If FindReplaceForm.Execute then
  Case FindReplaceForm.PageControl.ActivePageIndex of
   0:
   begin
    bbFind1 := True;
    bbReplace1 := False;
    Orig := False;
    ppFind(U, FindReplaceForm.FEdit.WideText);
   end;
   1:
   begin
    bbFind1 := True;
    bbReplace1 := True;
    ppReplace(U, FindReplaceForm.RFEdit.WideText,
                 FindReplaceForm.REdit.WideText);
   end;
  end;
end;

procedure TKruptarForm.ToolsHexEditorExecute(Sender: TObject);
begin
 If HexEditor1.Checked then Exit;
 HexEditEm.Checked := not HexEditEm.Checked;
 HexTextForm.Visible := HexEditEm.Checked; 
 HexEditForm.Visible := HexEditEm.Checked;
 If not SavePos.Checked then InitWindows;
end;

procedure TKruptarForm.ToolsProgressExecute(Sender: TObject);
var s: string;
Function GetWending: string;
begin
 Result := '';
 if (s[length(s)] in ['5'..'9', '0']) or
    ((length(s) >= 2) and (s[length(s)] in ['1'..'4']) and
   (s[length(s) - 1] = '1')) then
  Result := '��' Else
 if s[length(s)] in ['2'..'4'] then
  Result := '�' Else
 if s[length(s)] = '1' then
  Result := '';
end;
Var
 Ot, Et: pttable; Cnt, TCnt: integer; G: PGroup; O, E: PPoss;
begin
 G := KruptarProject.Groups.Root;
 Cnt := 0;
 TCnt := 0;
 While G <> NIL do With G^ do
 begin
  O := Posso.Root;
  E := PossoR.Root;
  While O <> NIL do
  begin
   Ot := O^.PtrTable^.Root;
   Et := E^.PtrTable^.Root;
   While Ot <> NIL do
   begin
    If Ot^.Text <> Et^.Text then Inc(TCnt);
    Inc(Cnt);
    Ot := Ot^.Next;
    Et := Et^.Next;
   end;
   O := O^.Next;
   E := E^.Next;
  end;
  G := Next;
 end;
 S := IntToStr(Cnt);
 DictForm.TotalEl.Caption := '����� ' + S + ' �������' + GetWending;
 DictForm.TotalEl.Left := Abs(DictForm.Width - DictForm.TotalEl.ClientWidth) div 2;
 S := IntToStr(TCnt);
 DictForm.TranslatedEl.Caption := '�������� ' + S + ' �������' + GetWending;
 DictForm.TranslatedEl.Left := Abs(DictForm.Width - DictForm.TranslatedEl.ClientWidth) div 2;
 S := IntToStr((TCnt * 100) div Cnt);
 DictForm.PercentEl.Caption := '�������� ' + S + '%';
 DictForm.PercentEl.Left := Abs(DictForm.Width - DictForm.Percentel.ClientWidth) div 2;
 DictForm.ShowModal;
end;

function Ddir(s: string): string;
var i, l: integer;
begin
 l := 0;
 for i := length(s) downto 1 do
  if s[i] = '\' then break else inc(l);
 result := s;
 delete(result, i + 1, l);
end;

procedure TKruptarForm.ToolsEmulatorStartExecute(Sender: TObject);
begin
 If KruptarProject.Emulator = '' then
 begin
  EmulOpenDialog.FileName := '';
  If EmulOpenDialog.Execute then
  begin
   KruptarProject.Emulator := EmulOpenDialog.FileName;
   Saved := False;
  end Else Exit;
 end;
 ShellExecute(Application.Handle,
 Pchar('Open'),
 Pchar(KruptarProject.Emulator),
 Pchar('"' + KruptarProject.InputROM + '"'),
 PChar('"' + Ddir(KruptarProject.Emulator) + '"'), SW_SHOWNORMAL);
end;

procedure TKruptarForm.ToolsNEmulatorStartExecute(Sender: TObject);
begin
 If KruptarProject.Emulator = '' then
 begin
  EmulOpenDialog.FileName := '';
  If EmulOpenDialog.Execute then
  begin
   KruptarProject.Emulator := EmulOpenDialog.FileName;
   Saved := False;
  end Else Exit;
 end;
 ShellExecute(Application.Handle,
 Pchar('Open'),
 Pchar(KruptarProject.Emulator),
 Pchar('"' + KruptarProject.OutputROM + '"'),
 PChar('"' + Ddir(KruptarProject.Emulator) + '"'), SW_SHOWNORMAL);
end;

procedure TKruptarForm.ToolsSetEmulExecute(Sender: TObject);
begin
 EmulOpenDialog.FileName := KruptarProject.Emulator;
 If EmulOpenDialog.Execute then
 begin
  KruptarProject.Emulator := EmulOpenDialog.FileName;
  Saved := False;
 end;
end;

Var UpdA: Boolean;

procedure TKruptarForm.UpdateExecute(Sender: TObject);
begin
 UpdA := False;
 Upd.Body := ExeDir +'temp.$$$';
 Upd.Get('http://djinn.avs.ru/update/info.txt');
end;

Function GetcDir: string;
begin
 Result := ExeDir;
end;

procedure TKruptarForm.UpdSuccess(Cmd: CmdType);
Var f: TextFile; S, E, U: string;
Label ll;
begin
 If Upd.BytesRecvd = Upd.BytesTotal then
 begin
  AssignFile(f, Upd.Body);
{$I-}
  Reset(f);
{$I+}
  If IoResult = 0 then
  begin
   Readln(f, s);
   u := '';
   While not eof(f) do
   begin
    Readln(F, e);
    U := U + E + #13#10;
   end;
   CloseFile(f);
   Erase(f);
   if StrToInt(s) > CurVer then
   begin
    InfoForm.Caption := 'Update';
    InfoForm.InfoMemo.Text := U;
    InfoForm.ShowModal;
    SaveDialog.DefaultExt := '.rar';
    SaveDialog.Filter := 'Rar Archive|*.rar';
    Insert('.', s, 2);
    Insert('.', s, 4);
    Insert('.', s, 6);
    FaName := 'Kruptar' + s +'.rar';
    SaveDialog.FileName := FaName;
    If SaveDialog.Execute then
    begin
     ProcForm.Caption := 'http://djinn.avs.ru/progs/' + FaName;
     procform.ShowModal;
    end;
    SaveDialog.DefaultExt := '.rnm';
    SaveDialog.Filter := '*.rnm|*.rnm';
   end Else goto ll;
  end Else ll: ShowMessage('����� ������ ���� ���!');
  Upd.Disconnect;
 end;
end;

procedure TKruptarForm.SavePosClick(Sender: TObject);
begin
 SavePos.Checked := not SavePos.Checked;
end;

procedure TKruptarForm.HexEditSaveExecute(Sender: TObject);
Var F: File;
begin
 AssignFile(F, KruptarProject.OutputROM);
 Rewrite(F, 1);
 BlockWrite(F, KruptarProject.ROME^, KruptarProject.ROMEsize);
 CloseFile(F);
 CCHANGED := False;
end;

procedure TKruptarForm.HexReloadExecute(Sender: TObject);
Var F: File;
begin
 If MessageDlg('���������� ������ ����� ��������!', mtConfirmation,
    [mbOk, mbCancel], 0) = mrOk then
 begin
  FreeMem(KruptarProject.ROME, KruptarProject.ROMEsize + 4);
  AssignFile(F, KruptarProject.OutputROM);
  Reset(F, 1);
  KruptarProject.ROMEsize := FileSize(F);
  GetMem(KruptarProject.ROME, KruptarProject.ROMEsize + 4);
  BlockRead(F, KruptarProject.ROME^, KruptarProject.ROMEsize);
  CloseFile(F);
  CCHANGED := False;
  HexEditForm.RefreshHex;
 end;
end;

procedure TKruptarForm.GroupImportExecute(Sender: TObject);
Var U: TTreeNode; PtrFile: TextFile; S, SS: String; A, B: LongInt;
Label Axit, Uxit;
begin
 Goto Uxit;
 Axit:
 ShowMessage('������ � �����!');
 Exit;
 Uxit:
 U := ProjectItemsForm.TreeView.Selected;
 If U = NIL then Exit;
 If U.Level = 1 then U := U.Parent;
 If PtrDialog.Execute then
 begin
  AssignFile(PtrFile, PtrDialog.FileName);
  Reset(PtrFile);
  A := 0; B := 0;
  While not EOF(PtrFile) do With PGroup(U.Data)^ do
  begin
   Readln(PtrFile, S);
   If Pos('-', S) = 9 then
   begin
    SS := 'h' + Copy(S, 1, 8);
    If UnsCheckOut(SS) then A := GetLDW(SS) Else goto Axit;
    SS := 'h' + Copy(S, 10, 8);
    If UnsCheckOut(SS) then B := GetLDW(SS) Else goto Axit;
   end Else If UnsCheckOut('h' + S) then
   begin
    A := GetLDW('h' + S);
    If not xNoPtr then  B := A + (xPtrSize + xInterval) - 1 Else
    If xStrLen > 0 then B := A + (xStrLen + xInterval) - 1 Else goto Axit;
   end Else goto Axit;
   If xNoPtr then
   begin
    If xStrLen = 0 then Posso.Add(0, a, (b + 1) - a) Else
     Posso.Add(0, a, ((b + 1) - a) div (xStrLen + xInterval));
   end Else Posso.Add(0, a, ((b + 1) - a) div (xPtrSize + xInterval));
  end;
  PGroup(U.Data)^.PossoR.AddPosso;
  Saved := False;
  StringsListForm.StringsListBox.Items.Clear;
  ccurnode := NIL;
  Gazzo := U.Data;
  RefreshAll;
  CloseFile(PtrFile);
 end;
end;

procedure TKruptarForm.Enter1Click(Sender: TObject);
begin
 Enter1.Checked := not Enter1.Checked;
 If Enter1.Checked then
 begin
  N11.Checked := False;
  N12.Checked := False;
 end;
end;

procedure TKruptarForm.N11Click(Sender: TObject);
begin
 N11.Checked := not N11.Checked;
 If N11.Checked then
 begin
  Enter1.Checked := False;
  N12.Checked := False;
 end;
end;

procedure TKruptarForm.N12Click(Sender: TObject);
begin
 N12.Checked := not N12.Checked;
 If N12.Checked then
 begin
  N11.Checked := False;
  Enter1.Checked := False;
 end;
end;

procedure TKruptarForm.HexEditor1Click(Sender: TObject);
Var FFile: File;
begin
 If HexEditem.Checked then ToolsHexEditorExecute(Sender);
 HexEditor1.Checked := not HexEditor1.Checked;
 ProjectItemsForm.EnableButtons;
 ToolsHexEditor.Enabled := not HexEditor1.Checked;
 If not Created then Exit;
 If HexEditor1.Checked then
 begin
  If KruptarProject.ROMESize > 0 then
   Freemem(KruptarProject.ROME, KruptarProject.ROMESize + 4);
 end ELse
 begin
  Assignfile(ffile, KruptarProject.OutPutROM);
  Reset(FFile, 1);
  KruptarProject.ROMESize := FileSize(FFile);
  GetMem(KruptarProject.ROME, KruptarProject.ROMESize + 4);
  BlockRead(FFile, KruptarProject.ROME^, KruptarProject.ROMESize);
  CloseFile(ffile);
 end;
end;

Type
 PUnicodeTextFile = ^TUnicodeTextFile;
 TUnicodeTextFile = Object
  FName: String;
  Data: WideString;
  NPos: Integer;
  WriteMode: Boolean;   
  Constructor AssignFile(FileName: String);
  Procedure Rewrite;
  Procedure Readln(Var S: WideString);
  Procedure Writeln(S: WideString);
  Function EndOfFile: Boolean;
  Destructor CloseFile;
 end;

Constructor TUnicodeTextFile.AssignFile(FileName: String);
Var F: File; W, R: Integer;
begin
 FName := FileName;
 If not FileExists(FileName) then
 begin
  Data := '';
  NPos := 0;
  WriteMode := False;
 end Else
 begin
  System.AssignFile(F, FileName);
  System.Reset(F, 1);
  WriteMode := False;
  W := 0;
  BlockRead(F, W, 2, R);
  If W = $FEFF then
  begin
   W := FileSize(F) - 2;
   If W > 0 then
   begin
    SetLength(Data, W div 2);
    BlockRead(F, Data[1], W, R);
   end Else Data := '';
  end Else Data := '';
  NPos := 0;
  WriteMode := False;
  System.CloseFile(F);
 end;
end;

Function TUnicodeTextFile.EndOfFile: Boolean;
begin
 Result := NPos >= Length(Data);
end;

Procedure TUnicodeTextFile.Rewrite;
begin
 WriteMode := True;
 NPos := 0;
 Data := '';
end;

Procedure TUnicodeTextFile.Readln(Var S: WideString);
begin
 S := '';
 While not EndOfFile do
 begin
  If Data[NPos + 1] = #13 then Inc(NPos);
  If EndOfFile then Exit;
  If Data[NPos + 1] = #10 then begin Inc(NPos); Exit end;
  S := S + Data[NPos + 1];
  Inc(NPos);
 end;
end;

Procedure TUnicodeTextFile.Writeln(S: WideString);
begin
 If not WriteMode then Exit;
 Data := Data + S + #13#10;
end;

Destructor TUnicodeTextFile.CloseFile;
Var F: File; W: Integer;
begin
 If WriteMode then
 begin
  System.AssignFile(F, FName);
  System.Rewrite(F, 1);
  W := $FEFF;
  BlockWrite(F, W, 2);
  BlockWrite(F, Data[1], Length(Data) * 2);
  System.CloseFile(F);
 end;
 Data := '';
 FName := '';
 NPos := 0;
 WriteMode := False;
end;

procedure TKruptarForm.TextExtractClick(Sender: TObject);
Var
 G: PGroup; P: PPoss; T: PTTable; F: TextFile;
 FF: PUnicodeTextFile; I: Integer; SS: WideString; S: String;
begin
 If SaveTextDialog.Execute then
 begin
  If KruptarProject.Groups.Root^.xKodir = kdUNIC then
  begin
   New(FF, AssignFile(SaveTextDialog.FileName));
   With FF^ do
   begin
    Rewrite;
    G := KruptarProject.Groups.Root;
    While G <> NIL do
    begin
     P := G^.PossoR.Root;
     While P <> NIL do
     begin
      T := P^.PtrTable^.Root;
      While T <> NIL do
      begin
       SS := '';
       With T^ do For I := 1 to Length(Text) do
       begin
        If Text[I] = '/' then Break;
        SS := SS + Text[I];
        If (SS[Length(SS)] = '^') or
           (SS[Length(SS)] = '~') or
           (SS[Length(SS)] = '*') then SS := SS + #13#10;
       end;
       Writeln(SS);
       Writeln('');
       T := T^.Next;
      end;
      P := P^.Next;
     end;
     G := G^.Next;
    end;
   end;
   Dispose(FF, CloseFile);
  end Else
  begin
   AssignFile(F, SaveTextDialog.FileName);
   Rewrite(F);
   G := KruptarProject.Groups.Root;
   While G <> NIL do
   begin
    P := G^.PossoR.Root;
    While P <> NIL do
    begin
     T := P^.PtrTable^.Root;
     While T <> NIL do
     begin
      S := '';
      With T^ do For I := 1 to Length(Text) do
      begin
       If Text[I] = '/' then Break;
       S := S + Text[I];
       If S[Length(S)] in ['^', '~', '*'] then S := S + #13#10;
      end;
      Writeln(F, S);
      Writeln(F);
      T := T^.Next;
     end;
     P := P^.Next;
    end;
    G := G^.Next;
   end;
   CloseFile(F);
  end;
 end;
end;

procedure TKruptarForm.N14Click(Sender: TObject);
Var G: PGroup; P: PPoss; T: PTTable; F: TextFile; I: Integer; S: String;
begin
 If PtrDialog.Execute then
 begin
  AssignFile(F, PtrDialog.FileName);
  PtrDialog.FileName := '';
  Reset(F);
  G := KruptarProject.Groups.Root;
  While G <> NIL do
  begin
   P := G^.PossoR.Root;
   While P <> NIL do
   begin
    T := P^.PtrTable^.Root;
    While T <> NIL do
    begin
     I := 0;
     With T^ do
     begin
      Text := '';
      Repeat
       Readln(F, S);
       If S <> '' then Text := Text + S;
       Inc(I);
      Until (S = '') and (I > 1);
      Text := Text + '/';      
     end;
     T := T^.Next;
    end;
    P := P^.Next;
   end;
   G := G^.Next;
  end;
  CloseFile(F);
  RefreshAll;
 end;
end;

end.
