unit NewTextEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, UnicodeEdit, UnicodeMemo, ExtCtrls, BufForm,
  StdActns, ActnList, Menus;

type
  TNewTextForm = class(TForm)
    Memo: TUnicodeMemo;
    StatusBar: TStatusBar;
    Timer: TTimer;
    PopupMenu: TPopupMenu;
    ActionList: TActionList;
    EditCut: TEditCut;
    EditCopy: TEditCopy;
    EditPaste: TEditPaste;
    EditSelectAll: TEditSelectAll;
    Cut: TMenuItem;
    CopyI: TMenuItem;
    Paste: TMenuItem;
    Delete: TMenuItem;
    N1: TMenuItem;
    SelectAll: TMenuItem;
    N2: TMenuItem;
    PasteTilda: TMenuItem;
    PasteStar: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure MemoChange(Sender: TObject);
    procedure MemoKeyPress(Sender: TObject; var Key: Char);
    procedure MemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TimerTimer(Sender: TObject);
    procedure StatusBarClick(Sender: TObject);
    procedure EditCutExecute(Sender: TObject);
    procedure EditCopyExecute(Sender: TObject);
    procedure EditPasteExecute(Sender: TObject);
    procedure EditDeleteExecute(Sender: TObject);
    procedure EditSelectAllExecute(Sender: TObject);
    procedure PasteTildaClick(Sender: TObject);
    procedure PasteStarClick(Sender: TObject);
    procedure PopupMenuPopup(Sender: TObject);
  private
  public
    Procedure SetText;
  end;

var
  NewTextForm: TNewTextForm;
  EposText: String;
  InsPressed: Boolean;  

implementation

uses KruptarMain, ProjItems, StringListUnit, UniConv, KruptarProjectUnit,
     OriginalEdit, Japan;

{$R *.dfm}

procedure TNewTextForm.FormShow(Sender: TObject);
begin
 KruptarForm.ChangeTextItem.Checked := True;
end;

Var
 CurI: Integer;
  _Is: String;
 Addd: Boolean;

Procedure TNewTextForm.SetText;
Var Pp: PPoss; U: TTreeNode; I, J: Integer; S, Ss: WideString; Ind: Integer;
    N: PtTable;
Label Kuka;
begin
 Addd := True;
 StatusBar.Panels[0].Text := '';
 U := ProjectItemsForm.TreeView.Selected;
 Ind := StringsListForm.StringsListBox.ItemIndex;
 if (Ind < 0) or (U = NIL) or (U.Data = NIL) then
 begin
  Memo.Lines.Clear;
  Memo.ReadOnly := True;
  Addd := False;
  Exit;
 end;
 If U.Level = 1 then U := U.Parent;
 pp := PGroup(U.Data)^.PossoR.Root;
 j := 0;
 ss := '';
 With PGroup(U.Data)^ do While pp <> nil do
 begin
  n := Pp^.PtrTable^.Root;
  while n <> nil do
  begin
   if j = Ind then
   begin
    CurP := N;
    CurI := J;
     _Is := Pp^.PtrTable^.Name + '-';
    EposText := IntToHex(N^.TPtr, 8);
    StatusBar.Panels[0].Text := EposText;
    Case xKodir of
     kdANSI, kdUNIC: S := N^.text;
     kdSJIS: S := UniText(N^.Text);
    end;
    i := 0; j := 1;
    If s <> '' then
    repeat
     repeat
      inc(i);
     until (i = Length(s)) or ((s[i] = '~') or (s[i] = '^') or
                               (s[i] = '*') or (s[i] = '/') or
                               (s[i] = '\'));
     Ss := Ss + Copy(S, j, i - j + 1) + #13#10;
     j := i + 1;
    until i = Length(s);
    Goto Kuka;
   end;
   Inc(j);
   N := N^.next;
  end;
  Pp := Pp^.next;
 end;
Kuka:
 Memo.Lines.SetText(PWideChar(Ss));
 Memo.ReadOnly := False;
 Addd := False;
end;

procedure TNewTextForm.MemoChange(Sender: TObject);
Var N: PtTable; S, Ss: widestring; Ii, Ff: DWord;
    P: Pointer; U: TTreeNode; Ij: Integer;
Label Ohoho;
begin
 If Addd Then Exit;
 ss := '';
 If Memo.Lines.Count <= 0 then
 begin
  KruptarForm.Saved := False;
  Exit;
 end;
 U := ProjectItemsForm.TreeView.Selected;
 If (CurI < 0) or (U = NIL) then
 begin
  SetText;
  Exit;
 end;
 If U.Level = 1 then U := U.Parent;
 P := U.Data;
 If P = NIL then
 begin
  SetText;
  Exit;
 end;
 With PGroup(P)^ do
 begin
  N := CurP;
  For Ii := 0 to Memo.Lines.Count - 1 do
  begin
   s := Memo.Lines.Strings[Ii];
   ss := ss + s;
  end;
  Case xKodir of
   kdANSI, kdUNIC: N^.Text := Ss;
   kdSJIS: N^.Text := SJISText(Ss);
  end;
  With StringsListForm.StringsListBox do
  begin
   S := _Is + IntToHex(N^.TPtr, 8) + ' -> ' + SS;
   For Ij := SelectionModel.MinSelectionIndex to SelectionModel.MaxSelectionIndex do
   begin
    PtTable(ListArray[IJ])^.Text := N^.Text;
    Items.Strings[Ij] := S;
   end;
   Items.Strings[CurI] := S;
  end;
  Ff := Memo.SelStart;
  Ss := Memo.Lines.GetText;
  if (ss[Memo.SelStart] = '^') or
    (ss[Memo.SelStart] = '*') or
    (ss[Memo.SelStart] = '~') then
  begin
   SetText;
   Memo.SelStart := Ff;
   Memo.SelLength := 0;
  end;
 end;
 KruptarForm.Saved := False;
end;

Var
 KeyStr: String;

procedure TNewTextForm.MemoKeyPress(Sender: TObject; var Key: Char);
Function NotFound(S: String): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Pos(KeyStr, Romaji[I]) < 1) do Inc(I);
 Result := I = KanaC + 1;
end;
Function NotFoundC(C: Char): Boolean;
Var i: Integer;
begin
 I := 0;
 While (I <= KanaC) and (Romaji[I][1] <> C) do Inc(I);
 Result := I = KanaC + 1;
end;
Function DownCase(Ch: Char ): Char;
begin
 Result := Ch;
 Case Result of
  'A'..'Z': Inc(Result, Ord('a') - Ord('A'));
 end;
end;
Var I: Integer;
begin
 if Key = #13 then
  Key := KruptarForm.GetCurMN Else If (Key <> #8) and
  KruptarForm.OptionsKanjiItem.Enabled and
  KruptarForm.OptionsKanjiItem.Checked then
 begin
  If (KeyStr <> '') then
  begin
   If KeyStr[1] in LSoGL then DownCase(Key) Else
   If KeyStr[1] in BSoGL then UpCase(Key) Else
   begin
    KeyStr := '';
    Key := #0;
    Exit;
   end;
  end;
  If (KeyStr = '') and not (Key in LSogL) and not (Key in BSogL) and NotFoundC(Key)then
  begin
   Key := #0;
   Exit;
  end;
  KeyStr := KeyStr + Key;
  Key := #0;
  If (Length(KeyStr) = 2) and not(KeyStr[2] in ['+', '*']) and
     (UpCase(KeyStr[2]) = UpCase(KeyStr[1])) and
     (UpCase(KeyStr[2]) <> 'N') then
  begin
   If (KeyStr[1] in BSogL) and (UpCase(KeyStr[2]) = KeyStr[1]) then
   begin
    If InsPressed then Memo.SelLength := 1;
    Memo.SelWideText := UniText('�b');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   If (KeyStr[1] in LSogL) and (DownCase(KeyStr[2]) = KeyStr[1]) then
   begin
    If InsPressed then Memo.SelLength := 1;
    Memo.SelWideText := UniText('��');
    KeyStr := KeyStr[1];
    Exit;
   end Else
   begin
    KeyStr := '';
    Exit;
   end;
  end;
  If NotFound(KeyStr) then
  begin
   KeyStr := '';
   Exit;
  end;
  I := 0;
  While (I <= KanaC) and (KeyStr <> Romaji[I]) do Inc(I);
  If I <= KanaC then
  begin
   If InsPressed then Memo.SelLength := Length(Kana[I]) div 2;
   Memo.SelWideText := UniText(Kana[I]);
   KeyStr := '';
  end;
 end Else If InsPressed then
 begin
  Memo.SelLength := 1;
  Memo.SelWideText := '';
 end;
end;

procedure TNewTextForm.MemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
Var i: Integer;
begin
 If ssAlt in Shift then
 begin
  KruptarForm.SetFocus;
  KeyStr := '';
 end;
 If ssCtrl in Shift then
 begin
  KeyStr := '';
  Case Key of
   VK_UP:
   begin
    Key := 0;
    I := StringsListForm.StringsListBox.SelectionModel.MinSelectionIndex;
    If I > 0 then
     StringsListForm.StringsListBox.ItemIndex := I - 1;
   end;
   VK_DOWN:
   begin
    Key := 0;
    I := StringsListForm.StringsListBox.SelectionModel.MaxSelectionIndex;
    If I < StringsListForm.StringsListBox.Items.Count - 1 then
     StringsListForm.StringsListBox.ItemIndex := I + 1;
   end;
  end;
 end Else Case Key of
  VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_HOME, VK_END,
  VK_DELETE, VK_ESCAPE, VK_NEXT, VK_PRIOR, VK_BACK, VK_RETURN:
   KeyStr := ''
  Else IF (Key = VK_INSERT) and not (ssShift in Shift) then InsPressed := not InsPressed;
 end;
end;

procedure TNewTextForm.TimerTimer(Sender: TObject);
begin
 StatusBar.Panels[1].Text := 'x: ' +
 IntToStr(Memo.CaretPos.X) + '/' +
 IntToStr(Length(Memo.Lines.Strings[Memo.CaretPos.Y]));
 StatusBar.Panels[2].Text := 'y: ' +
 IntToStr(Memo.CaretPos.Y) + '/' +
 IntToStr(Memo.Lines.Count - 1 * Byte(Memo.Lines.Count > 0));
 StatusBar.Panels[4].Text := KeyStr;
 If InsPressed then
  StatusBar.Panels[3].Text := '������' Else
  StatusBar.Panels[3].Text := '�������';
end;

procedure TNewTextForm.StatusBarClick(Sender: TObject);
begin
 Form1.Edit1.Text := StatusBar.Panels[0].Text;
 Form1.Edit1.SelectAll;
 Form1.Edit1.CopyToClipboard;
end;

procedure TNewTextForm.EditCutExecute(Sender: TObject);
begin
 Memo.CutToClipboard;
end;

procedure TNewTextForm.EditCopyExecute(Sender: TObject);
begin
 Memo.CopyToClipboard;
end;

procedure TNewTextForm.EditPasteExecute(Sender: TObject);
begin
 Memo.PasteFromClipboard;
end;

procedure TNewTextForm.EditDeleteExecute(Sender: TObject);
begin
 If Memo.SelLength = 0 then Memo.SelLength := 1;
 Memo.SelWideText := '';
end;

procedure TNewTextForm.EditSelectAllExecute(Sender: TObject);
begin
 Memo.SelectAll;
end;

procedure TNewTextForm.PasteTildaClick(Sender: TObject);
begin
 If InsPressed then Memo.SelLength := 1;
 Memo.SelWideText := '~'#13#10;
end;

procedure TNewTextForm.PasteStarClick(Sender: TObject);
begin
 If InsPressed then Memo.SelLength := 1;
 Memo.SelWideText := '*'#13#10;
end;

procedure TNewTextForm.PopupMenuPopup(Sender: TObject);
begin
 If Memo.ReadOnly then
 begin
  Paste.Enabled := False;
  SelectAll.Enabled := False;
  PasteTilda.Enabled := False;
  PasteStar.Enabled := False;
  Delete.Enabled := False;
 end Else
 begin
  Paste.Enabled := True;
  SelectAll.Enabled := True;
  PasteTilda.Enabled := True;
  PasteStar.Enabled := True;
  Delete.Enabled := True;  
 end
end;

end.


