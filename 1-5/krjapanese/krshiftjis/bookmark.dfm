object bmform: Tbmform
  Left = 215
  Top = 305
  AutoSize = True
  BorderStyle = bsDialog
  Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1079#1072#1082#1083#1072#1076#1082#1080
  ClientHeight = 49
  ClientWidth = 363
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object nfield: TEdit
    Left = 0
    Top = 0
    Width = 361
    Height = 21
    TabOrder = 0
    OnChange = nfieldChange
    OnKeyPress = nfieldKeyPress
  end
  object okbtn: TButton
    Left = 0
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    TabOrder = 1
    OnClick = okbtnClick
  end
  object cncbtn: TButton
    Left = 288
    Top = 24
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = cncbtnClick
  end
end
