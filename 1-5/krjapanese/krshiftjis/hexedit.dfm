object HexEditForm: THexEditForm
  Left = 285
  Top = 152
  AutoSize = True
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'HexEditor'
  ClientHeight = 497
  ClientWidth = 569
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TabControl: TTabControl
    Left = 0
    Top = 0
    Width = 569
    Height = 449
    MultiLine = True
    TabOrder = 0
    Tabs.Strings = (
      #1054#1088#1080#1075#1080#1085#1072#1083#1100#1085#1099#1081' ROM'
      #1048#1079#1084#1077#1085#1103#1077#1084#1099#1081' ROM')
    TabIndex = 0
    OnChange = TabControlChange
    object hexgrid: TStringGrid
      Tag = 1
      Left = 72
      Top = 32
      Width = 272
      Height = 408
      BorderStyle = bsNone
      Color = clTeal
      ColCount = 16
      DefaultColWidth = 16
      DefaultRowHeight = 16
      FixedColor = clHighlight
      FixedCols = 0
      RowCount = 24
      FixedRows = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMoneyGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected]
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 1
      OnClick = hexgridClick
      OnDrawCell = hexgridDrawCell
      OnEnter = hexgridEnter
      OnKeyDown = hexgridKeyDown
      OnKeyPress = hexgridkeypress
      OnMouseDown = hexgridMouseDown
      OnMouseUp = hexgridMouseUp
      OnSelectCell = hexgridSelectCell
    end
    object chargrid: TStringGrid
      Tag = 1
      Left = 352
      Top = 32
      Width = 209
      Height = 408
      BorderStyle = bsNone
      Color = clTeal
      ColCount = 16
      DefaultColWidth = 12
      DefaultRowHeight = 16
      FixedColor = clActiveCaption
      FixedCols = 0
      RowCount = 24
      FixedRows = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMoneyGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected]
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 2
      OnClick = chargridClick
      OnEnter = chargridEnter
      OnExit = chargridExit
      OnKeyDown = hexgridKeyDown
      OnKeyPress = chargridKeyPress
      OnMouseDown = chargridMouseDown
      OnMouseUp = chargridMouseUp
      OnSelectCell = chargridSelectCell
    end
    object posgrid: TStringGrid
      Left = 8
      Top = 32
      Width = 57
      Height = 408
      BorderStyle = bsNone
      Color = clTeal
      ColCount = 1
      DefaultRowHeight = 16
      Enabled = False
      FixedColor = clBackground
      FixedCols = 0
      RowCount = 24
      FixedRows = 0
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clMoneyGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goDrawFocusSelected]
      ParentFont = False
      ScrollBars = ssNone
      TabOrder = 0
    end
  end
  object statPanel: TPanel
    Left = 0
    Top = 448
    Width = 569
    Height = 49
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 108
      Height = 13
      Caption = #1057#1084#1077#1097#1077#1085#1080#1077': 00000000'
    end
    object Label2: TLabel
      Left = 16
      Top = 24
      Width = 108
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088':      00000000'
    end
    object Label3: TLabel
      Left = 160
      Top = 8
      Width = 78
      Height = 13
      Caption = #1057#1090#1088#1086#1082#1072':   00 = 0'
    end
    object Label4: TLabel
      Left = 160
      Top = 24
      Width = 78
      Height = 13
      Caption = #1057#1090#1086#1083#1073#1077#1094': 00 = 0'
    end
    object Label5: TLabel
      Left = 264
      Top = 8
      Width = 37
      Height = 13
      Caption = 'Hex: 00'
    end
    object Label6: TLabel
      Left = 264
      Top = 24
      Width = 32
      Height = 13
      Caption = 'Dec: 0'
    end
  end
  object MainMenu1: TMainMenu
    Left = 536
    Top = 456
    object OriginalROM1: TMenuItem
      Caption = #1052#1077#1085#1102
      object ROM4: TMenuItem
        Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1086#1088#1080#1075#1080#1085#1072#1083#1100#1085#1099#1081' ROM'
        ShortCut = 117
        OnClick = ROM4Click
      end
      object ROM1: TMenuItem
        Caption = #1055#1077#1088#1077#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1079#1084#1077#1085#1103#1077#1084#1099#1081' ROM'
        ShortCut = 118
        OnClick = ROM1Click
      end
      object ROM3: TMenuItem
        Caption = #1047#1072#1087#1080#1089#1072#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1074' '#1086#1088#1080#1075#1080#1085#1072#1083#1100#1085#1099#1081' ROM'
        ShortCut = 112
        OnClick = ROM3Click
      end
      object ROM2: TMenuItem
        Caption = #1047#1072#1087#1080#1089#1072#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1074' '#1080#1079#1084#1077#1085#1103#1077#1084#1099#1081' ROM'
        ShortCut = 113
        OnClick = ROM2Click
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Change1: TMenuItem
        Caption = 'Change'
        ShortCut = 45
        OnClick = Change1Click
      end
      object N1: TMenuItem
        Caption = '-'
        Enabled = False
      end
      object N2: TMenuItem
        Caption = #1047#1072#1082#1088#1099#1090#1100
        OnClick = N2Click
      end
    end
    object N3: TMenuItem
      Caption = #1055#1086#1079#1080#1094#1080#1080
      OnClick = N3Click
      object N4: TMenuItem
        Caption = #1054#1088#1080#1075#1080#1085#1072#1083' -> '#1048#1079#1084#1077#1085#1103#1077#1084#1099#1081
        ShortCut = 122
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = #1048#1079#1084#1077#1085#1103#1077#1084#1099#1081' -> '#1054#1088#1080#1075#1080#1085#1072#1083
        ShortCut = 123
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1055#1077#1088#1077#1093#1086#1076'...'
        ShortCut = 16455
        OnClick = N6Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object N8: TMenuItem
        Caption = #1040#1074#1090#1086
        ShortCut = 16449
        OnClick = N8Click
      end
    end
  end
  object Timer: TTimer
    Enabled = False
    Interval = 10
    OnTimer = TimerTimer
    Left = 504
    Top = 456
  end
end
