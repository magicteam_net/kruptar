unit gotou;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  Tgotoform = class(TForm)
    edit1: TEdit;
    procedure edit1KeyPress(Sender: TObject; var Key: Char);
    procedure edit1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  gotoform: Tgotoform;

implementation

{$R *.dfm}

uses ranmaedit, hexedit;

procedure Tgotoform.edit1KeyPress(Sender: TObject; var Key: Char);
begin
 key := upcase(key);
 if not (key in ['0'..'9', 'A'..'F', #13, #8]) then key := #0;
 if key = #13 then
 begin
  key := #0;
  close;
 end;
end;

procedure Tgotoform.edit1DblClick(Sender: TObject);
begin
 close;
end;

procedure Tgotoform.FormShow(Sender: TObject);
begin
 edit1.Text := inttohex(poss[hexeditform.tabcontrol.tabindex] +
 dword(hexeditform.hexgrid.row) * 16 + dword(hexeditform.hexgrid.col), 8);
 edit1.SelectAll;
end;

end.
