object KruptarForm: TKruptarForm
  Left = 254
  Top = 103
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  AutoScroll = False
  BiDiMode = bdLeftToRight
  ClientHeight = 564
  ClientWidth = 697
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnActivate = FormCreate
  OnCanResize = FormCanResize
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 136
    Top = 258
    Width = 83
    Height = 20
    Caption = #1054#1088#1080#1075#1080#1085#1072#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 496
    Top = 258
    Width = 76
    Height = 20
    Caption = #1055#1077#1088#1077#1074#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 345
    Top = 296
    Width = 9
    Height = 16
    Caption = '^'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 346
    Top = 320
    Width = 7
    Height = 16
    Caption = '*'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 344
    Top = 344
    Width = 11
    Height = 16
    Caption = '~'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 576
    Top = 256
    Width = 23
    Height = 22
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8000000C8C8C8C8C8C8C8C8C8C8C8C8000000C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8000000000000C8C8C8C8C8C8C8
      C8C8C8C8C8000000000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      000000000000000000C8C8C8C8C8C8C8C8C8C8C8C8000000000000000000C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C800000000000000000000000000000000000000
      0000000000000000000000000000000000C8C8C8C8C8C8C8C8C8000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000C8C8C8C8C8C8C8C8C800000000000000000000000000000000000000
      0000000000000000000000000000000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      000000000000000000C8C8C8C8C8C8C8C8C8C8C8C8000000000000000000C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8000000000000C8C8C8C8C8C8C8
      C8C8C8C8C8000000000000C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8000000C8C8C8C8C8C8C8C8C8C8C8C8000000C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8
      C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8C8}
    Visible = False
    OnClick = SpeedButton1Click
  end
  object japl: TLabel
    Left = 664
    Top = 260
    Width = 8
    Height = 16
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Fixedsys'
    Font.Style = []
    ParentFont = False
  end
  object pbtn: TButton
    Left = 272
    Top = 1
    Width = 57
    Height = 22
    Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1082#1083#1072#1076#1082#1091
    Caption = #1055#1077#1088#1077#1093#1086#1076'->'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
    OnClick = pbtnClick
  end
  object Memo: TUnicodeMemo
    Left = 0
    Top = 280
    Width = 337
    Height = 273
    Color = clActiveBorder
    Enabled = False
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'MS Gothic'
    Font.Style = []
    ParentFont = False
    PopupMenu = memopop
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 5
    OnKeyDown = rusmemoKeyDown
  end
  object ptta: TEdit
    Left = 2
    Top = 1
    Width = 71
    Height = 21
    Hint = #1053#1072#1095#1072#1083#1100#1085#1086#1077' '#1089#1084#1077#1097#1077#1085#1080#1077' '#1090#1072#1073#1083#1080#1094#1099' '#1087#1086#1080#1085#1090#1077#1088#1086#1074
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnKeyPress = pttaKeyPress
  end
  object addptbtn: TButton
    Left = 76
    Top = 0
    Width = 50
    Height = 46
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Small Fonts'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = addptbtnClick
  end
  object pttacnt: TEdit
    Tag = 1
    Left = 2
    Top = 25
    Width = 71
    Height = 21
    Hint = #1050#1086#1085#1077#1095#1085#1086#1077' '#1089#1084#1077#1097#1077#1085#1080#1077' '#1090#1072#1073#1083#1080#1094#1099' '#1087#1086#1080#1085#1090#1077#1088#1086#1074
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnKeyPress = pttaKeyPress
  end
  object pttblbox: TListBox
    Left = 0
    Top = 48
    Width = 126
    Height = 208
    Hint = #1057#1087#1080#1089#1086#1082' '#1090#1072#1073#1083#1080#1094' '#1087#1086#1080#1085#1090#1077#1088#1086#1074
    Style = lbOwnerDrawFixed
    Enabled = False
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = pttblboxClick
    OnKeyDown = pttblboxKeyDown
  end
  object rusmemo: TUnicodeMemo
    Tag = 1
    Left = 361
    Top = 281
    Width = 337
    Height = 264
    Enabled = False
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'MS Gothic'
    Font.Pitch = fpVariable
    Font.Style = []
    ParentFont = False
    PopupMenu = Copypaste
    ScrollBars = ssVertical
    TabOrder = 6
    OnChange = rusmemoChange
    OnKeyDown = rusmemoKeyDown
    OnKeyPress = rusmemoKeyPress
  end
  object rpos: TEdit
    Left = 361
    Top = 258
    Width = 126
    Height = 21
    Color = clMenu
    Enabled = False
    ReadOnly = True
    TabOrder = 4
  end
  object justenter: TRadioButton
    Left = 342
    Top = 280
    Width = 13
    Height = 17
    TabOrder = 7
    OnClick = tildaenterClick
  end
  object umnenter: TRadioButton
    Left = 342
    Top = 304
    Width = 13
    Height = 17
    Checked = True
    TabOrder = 8
    TabStop = True
    OnClick = tildaenterClick
  end
  object starenter: TRadioButton
    Left = 342
    Top = 328
    Width = 13
    Height = 17
    TabOrder = 9
    OnClick = tildaenterClick
  end
  object tildaenter: TRadioButton
    Left = 342
    Top = 352
    Width = 13
    Height = 17
    TabOrder = 10
    OnClick = tildaenterClick
  end
  object bmlist: TComboBox
    Left = 336
    Top = 1
    Width = 137
    Height = 21
    Hint = #1047#1072#1082#1083#1072#1076#1082#1080
    Style = csDropDownList
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 545
    Width = 697
    Height = 19
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clBtnText
    Font.Height = -13
    Font.Name = 'Fixedsys'
    Font.Style = []
    Panels = <
      item
        Width = 358
      end
      item
        Width = 120
      end
      item
        Width = 90
      end
      item
        Width = 90
      end
      item
        Width = 50
      end>
    SimplePanel = False
    UseSystemFont = False
  end
  object cntbtn: TButton
    Left = 128
    Top = 1
    Width = 119
    Height = 22
    Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' (Ctrl + P)'
    Enabled = False
    TabOrder = 13
    OnClick = cntbtnClick
  end
  object epos: TEdit
    Left = 0
    Top = 258
    Width = 128
    Height = 21
    Color = clMenu
    ReadOnly = True
    TabOrder = 14
  end
  object slist: TUnicodeListBox
    Left = 128
    Top = 24
    Width = 570
    Height = 232
    ItemHeight = 16
    MultiSelect = False
    Alignment = taLeftJustify
    DragKind = dkDock
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'MS Gothic'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
    OnClick = slistClick
    OnDblClick = slistDblClick
    OnKeyDown = slistKeyDown
  end
  object Button1: TButton
    Left = 472
    Top = 0
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 17
    Visible = False
    OnClick = Button1Click
  end
  object MainMenu: TMainMenu
    Left = 122
    Top = 336
    object loaditem: TMenuItem
      Caption = #1060#1072#1081#1083
      ShortCut = 119
      OnClick = loaditemClick
      object newtrans: TMenuItem
        Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1077#1088#1077#1074#1086#1076'...'
        ShortCut = 16462
        OnClick = newtransClick
      end
      object optrans: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100' '#1087#1077#1088#1077#1074#1086#1076'...'
        ShortCut = 16463
        OnClick = optransClick
      end
      object savetrans: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1087#1077#1088#1077#1074#1086#1076
        Enabled = False
        ShortCut = 16467
        OnClick = savetransClick
      end
      object savetras: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
        Enabled = False
        ShortCut = 16449
        OnClick = savetrasClick
      end
      object N27: TMenuItem
        Caption = '-'
      end
      object N39: TMenuItem
        Caption = #1048#1084#1087#1086#1088#1090' '#1087#1086#1081#1085#1090#1077#1088#1086#1074' '#1080#1079' '#1092#1072#1081#1083#1072'...'
        ShortCut = 8315
        OnClick = N39Click
      end
      object N42: TMenuItem
        Caption = '-'
      end
      object N5: TMenuItem
        Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1087#1086#1081#1085#1090#1077#1088#1099
        Enabled = False
        ShortCut = 16464
        OnClick = N5Click
      end
      object instrans: TMenuItem
        Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1087#1077#1088#1077#1074#1086#1076' '#1074' ROM'
        Enabled = False
        ShortCut = 16457
        OnClick = instransClick
      end
      object N44: TMenuItem
        Caption = #1057#1076#1077#1083#1072#1090#1100' '#1074#1089#1105' '#1089#1088#1072#1079#1091'...'
        ShortCut = 120
        OnClick = N44Click
      end
      object N43: TMenuItem
        Caption = '-'
      end
      object N13: TMenuItem
        Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1090#1077#1082#1089#1090
        ShortCut = 116
        OnClick = N13Click
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object N1: TMenuItem
        Caption = #1048#1085#1092#1086
        Enabled = False
        ShortCut = 8304
        OnClick = N1Click
      end
      object N26: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080'...'
        ShortCut = 119
        OnClick = N26Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object exitprog: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = exitprogClick
      end
    end
    object N17: TMenuItem
      Caption = #1048#1085#1089#1090#1088#1091#1084#1077#1085#1090#1099
      OnClick = N17Click
      object N6: TMenuItem
        Caption = #1047#1072#1082#1083#1072#1076#1082#1080
        object N8: TMenuItem
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100'...'
          ShortCut = 16450
          OnClick = addbmbtnClick
        end
        object N11: TMenuItem
          Caption = #1055#1077#1088#1077#1093#1086#1076
          ShortCut = 119
          OnClick = pbtnClick
        end
        object N9: TMenuItem
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100'...'
          ShortCut = 49218
        end
        object N10: TMenuItem
          Caption = #1059#1076#1072#1083#1080#1090#1100
          ShortCut = 16452
          OnClick = delbuttnClick
        end
        object N14: TMenuItem
          Caption = '-'
        end
        object N16: TMenuItem
          Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1101#1083#1077#1084#1077#1085#1090'         Ctrl+'#1042#1074#1077#1088#1093
          OnClick = N16Click
        end
        object N15: TMenuItem
          Caption = #1057#1083#1077#1076#1091#1102#1097#1080#1081' '#1101#1083#1077#1084#1077#1085#1090'           Ctrl+'#1042#1085#1080#1079
          OnClick = N15Click
        end
      end
      object HexEditor1: TMenuItem
        Caption = 'HexEditor...'
        ShortCut = 16456
        OnClick = HexEditor1Click
      end
      object ProgressItem: TMenuItem
        Caption = #1055#1088#1086#1075#1088#1077#1089#1089' '#1087#1077#1088#1077#1074#1086#1076#1072'...'
        ShortCut = 49232
        OnClick = ProgressItemClick
      end
      object N21: TMenuItem
        Caption = '-'
      end
      object N22: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1101#1084#1091#1083#1103#1090#1086#1088#1072'...'
        Enabled = False
        ShortCut = 16465
        OnClick = N22Click
      end
      object N20: TMenuItem
        Caption = #1047#1072#1087#1091#1089#1082' '#1086#1088#1080#1075#1080#1085#1072#1083#1100#1085#1086#1075#1086' '#1088#1086#1084#1072' '#1074' '#1101#1084#1091#1083#1103#1090#1086#1088#1077
        Enabled = False
        ShortCut = 49221
        OnClick = N20Click
      end
      object emulate: TMenuItem
        Caption = #1047#1072#1087#1091#1089#1082' '#1080#1079#1084#1077#1085#1103#1077#1084#1086#1075#1086' '#1088#1086#1084#1072' '#1074' '#1101#1084#1091#1083#1103#1090#1086#1088#1077
        Enabled = False
        ShortCut = 16453
        OnClick = emulateClick
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object N19: TMenuItem
        Caption = #1071#1087#1086#1085#1089#1082#1080#1081' '#1074#1074#1086#1076
        Enabled = False
        ShortCut = 16458
        OnClick = N19Click
      end
    end
    object N35: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      object N36: TMenuItem
        Caption = #1064#1088#1080#1092#1090' '#1076#1083#1103' '#1089#1087#1080#1089#1082#1072'...'
        OnClick = N36Click
      end
      object N37: TMenuItem
        Caption = #1064#1088#1080#1092#1090' '#1076#1083#1103' '#1086#1088#1080#1075#1080#1085#1072#1083#1100#1085#1086#1075#1086' '#1090#1077#1082#1089#1090#1072'...'
        OnClick = N37Click
      end
      object N38: TMenuItem
        Caption = #1064#1088#1080#1092#1090' '#1076#1083#1103' '#1087#1077#1088#1077#1074#1086#1076#1072'...'
        OnClick = N38Click
      end
      object N40: TMenuItem
        Caption = '-'
      end
      object N45: TMenuItem
        Caption = #1048#1089#1082#1072#1090#1100' '#1086#1076#1080#1085#1072#1082#1086#1074#1099#1077' '#1089#1090#1088#1086#1082#1080
        Checked = True
        OnClick = N45Click
      end
      object QuickInsert: TMenuItem
        Caption = #1041#1099#1089#1090#1088#1072#1103' '#1074#1089#1090#1072#1074#1082#1072' '#1090#1077#1082#1089#1090#1072' '#1080' '#1087#1077#1088#1077#1089#1095#1105#1090' '#1087#1086#1081#1085#1090#1077#1088#1086#1074
        ShortCut = 16465
        Visible = False
        OnClick = QuickInsertClick
      end
    end
    object N2: TMenuItem
      Caption = #1055#1086#1080#1089#1082
      Enabled = False
      object N3: TMenuItem
        Caption = #1053#1072#1081#1090#1080'...'
        ShortCut = 16454
        OnClick = N2Click
      end
      object N23: TMenuItem
        Caption = #1047#1072#1084#1077#1085#1080#1090#1100'...'
        ShortCut = 16466
        OnClick = N23Click
      end
      object N4: TMenuItem
        Caption = #1053#1072#1081#1090#1080' '#1076#1072#1083#1077#1077
        ShortCut = 114
        OnClick = N4Click
      end
    end
    object N24: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object Update1: TMenuItem
        Caption = 'Update...'
        ShortCut = 16469
        OnClick = Update1Click
      end
      object N25: TMenuItem
        Caption = '-'
      end
      object N34: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        ShortCut = 112
        OnClick = N34Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'rnm'
    Filter = '*.rnm|*.rnm'
    Left = 154
    Top = 336
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '.rnm'
    Filter = '*.rnm|*.rnm'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofExtensionDifferent, ofEnableSizing, ofDontAddToRecent]
    Left = 186
    Top = 336
  end
  object FindDialog: TFindDialog
    Options = [frDown, frFindNext, frHideMatchCase, frHideWholeWord, frHideUpDown]
    OnFind = FindDialogFind
    Left = 218
    Top = 336
  end
  object Timer1: TTimer
    Interval = 20
    OnTimer = Timer1Timer
    Left = 304
    Top = 256
  end
  object Upd: TNMHTTP
    Host = 'http://djinn.avs.ru'
    Port = 80
    ReportLevel = 0
    Body = 'info.txt'
    Header = 'Head.txt'
    InputFileMode = True
    OutputFileMode = False
    OnSuccess = UpdSuccess
    ProxyPort = 0
    Left = 336
    Top = 376
  end
  object Copypaste: TPopupMenu
    Left = 336
    Top = 408
    object N28: TMenuItem
      Caption = #1042#1099#1088#1077#1079#1072#1090#1100
      OnClick = N28Click
    end
    object N29: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N29Click
    end
    object N30: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      OnClick = N30Click
    end
    object N31: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnClick = N31Click
    end
    object N32: TMenuItem
      Caption = '-'
    end
    object N33: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      OnClick = N33Click
    end
  end
  object memopop: TPopupMenu
    Left = 336
    Top = 440
    object MenuItem2: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N29Click
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object MenuItem6: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      OnClick = N33Click
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 336
    Top = 472
  end
end
