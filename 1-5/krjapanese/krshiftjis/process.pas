unit process;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, ExtCtrls, StdCtrls, Psock, NMHttp;

type
  Tprocform = class(TForm)
    Gauge1: TGauge;
    imptimer: TTimer;
    Button1: TButton;
    downl: TNMHTTP;
    Label1: TLabel;
    procedure imptimerTimer(Sender: TObject);
    procedure downlPacketRecvd(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure downlSuccess(Cmd: CmdType);
    procedure Button1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  procform: Tprocform; var impprogr: integer; faname: string; u: boolean;

implementation

{$R *.dfm}

Uses RanmaEdit;

procedure Tprocform.imptimerTimer(Sender: TObject);
begin
 impprogr := gauge1.Progress;
end;

procedure Tprocform.downlPacketRecvd(Sender: TObject);
begin
Gauge1.Progress := Round(downl.BytesRecvd*100/downl.BytesTotal) ;
Label1.Caption := '�������� ' + IntToStr(downl.BytesRecvd) + ' ���� �� ' +
                  IntToStr(downl.BytesTotal) + '.';
end;

procedure Tprocform.FormActivate(Sender: TObject);
begin
 u := false;
 gauge1.Progress := 0;
 label1.Caption := '';
 button1.Enabled := false; 
 downl.Body := KruptarForm.SaveDialog.Filename;
 downl.Get('http://djinn.avs.ru/progs/' + faname);
 downl.Disconnect;
 close;
end;

procedure Tprocform.downlSuccess(Cmd: CmdType);
begin
 If downl.BytesRecvd = downl.BytesTotal then
 begin
  u := true;
  Label1.Caption := Label1.Caption + ' ������!';
  button1.Enabled := true;
 end;
end;

procedure Tprocform.Button1Click(Sender: TObject);
begin
 Close;
end;

procedure Tprocform.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
 CanClose := button1.enabled;
end;

end.
