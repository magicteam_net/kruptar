unit hexedit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, ComCtrls, Menus, StdCtrls;

type
  THexEditForm = class(TForm)
    MainMenu1: TMainMenu;
    TabControl: TTabControl;
    hexgrid: TStringGrid;
    chargrid: TStringGrid;
    statPanel: TPanel;
    posgrid: TStringGrid;
    OriginalROM1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    ROM1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    ROM2: TMenuItem;
    ROM3: TMenuItem;
    ROM4: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Timer: TTimer;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    Change1: TMenuItem;
    procedure hexgridClick(Sender: TObject);
    procedure chargridClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure TabControlChange(Sender: TObject);
    procedure hexgridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure hexgridEnter(Sender: TObject);
    procedure chargridEnter(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure hexgridkeypress(Sender: TObject; var Key: Char);
    procedure chargridKeyPress(Sender: TObject; var Key: Char);
    procedure ROM4Click(Sender: TObject);
    procedure ROM1Click(Sender: TObject);
    procedure ROM3Click(Sender: TObject);
    procedure ROM2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure chargridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure TimerTimer(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure hexgridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure hexgridMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure hexgridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure chargridMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chargridMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure hexgridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure chargridExit(Sender: TObject);
    procedure Change1Click(Sender: TObject);
  private
    { Private declarations }
  public
   procedure showhex;
    { Public declarations }
  end;

var
  HexEditForm: THexEditForm;
var rms, poss: array[0..1] of dword;
hexgr: boolean;
  md: boolean;
  cs: boolean;
  hs: boolean;

implementation

{$R *.dfm}

uses ranmaedit, gotou, resultfm;

var rom: array[0..1] of ^tromdata;

procedure THexEditForm.hexgridClick(Sender: TObject);
var a, b: integer;
begin
 a := hexgrid.Row;
 b := hexgrid.Col;
  posgrid.Row := a;
 chargrid.Row := a;
 chargrid.Col := b;
end;

procedure THexEditForm.chargridClick(Sender: TObject);
var a, b: integer;
begin
 a := chargrid.Row;
 b := chargrid.Col;
 posgrid.Row := a;
 hexgrid.Col := b;
 hexgrid.Row := a;
end;

procedure THexEditForm.FormShow(Sender: TObject);
var f: file;
begin
 AssignFile(f, orom);
 reset(f, 1);
 rms[0] := filesize(f);
 getmem(rom[0], rms[0]);
 blockread(f, rom[0]^, rms[0]);
 closefile(f);
 AssignFile(f, nrom);
 reset(f, 1);
 rms[1] := filesize(f);
 getmem(rom[1], rms[1]);
 blockread(f, rom[1]^, rms[1]);
 closefile(f);
 showhex;
 hexgrid.SetFocus;
 timer.Enabled := true;
end;

procedure THexEditForm.FormHide(Sender: TObject);
begin
 timer.Enabled := false;
 freemem(rom[0], rms[0]);
 freemem(rom[1], rms[0]);
end;

procedure THexEditForm.showhex;
var i, j: dword;
begin
 if poss[tabcontrol.TabIndex] + 384 >= rms[tabcontrol.TabIndex] then
  poss[tabcontrol.TabIndex] := rms[tabcontrol.TabIndex] - 384;
 if n8.Checked then
  poss[byte(not boolean(tabcontrol.tabindex))] :=
  poss[tabcontrol.tabindex];  
 posgrid.Cols[0].Clear;
 for i := 0 to 23 do
  posgrid.cols[0].Add(inttohex(i * 16 + poss[tabcontrol.tabindex], 8));
 for i := 0 to 15 do
  hexgrid.Cols[i].Clear;
 for i := 0 to 15 do
  chargrid.Cols[i].Clear;
 for i := 0 to 23 do for j := 0 to 15 do
  hexgrid.Rows[i].Add(inttohex(rom[tabcontrol.tabindex]^[
  poss[tabcontrol.tabindex] + i * 16 + j], 2));
 if tabcontrol.tabindex = 0 then
 for i := 0 to 23 do for j := 0 to 15 do
  chargrid.Rows[i].Add(KruptarForm.table1[0,
  rom[tabcontrol.tabindex]^[poss[tabcontrol.tabindex] + i * 16 + j]]) Else
 for i := 0 to 23 do for j := 0 to 15 do
  chargrid.Rows[i].Add(KruptarForm.table2[0,
  rom[tabcontrol.tabindex]^[poss[tabcontrol.tabindex] + i * 16 + j]]);
end;

procedure THexEditForm.TabControlChange(Sender: TObject);
begin
 showhex;
 if hexgr then
  chargrid.SetFocus Else
  hexgrid.SetFocus;
end;

procedure THexEditForm.hexgridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (csDesigning in ComponentState) or not enabled then
 begin
  key := 0;
  exit;
 end;
 If (key = VK_DOWN) and (hexgrid.Row = 23) then
 begin
  if (poss[tabcontrol.tabindex] + $10) + 384 < rms[tabcontrol.tabindex] then
   inc(poss[tabcontrol.tabindex], $10) Else
  poss[tabcontrol.tabindex] := rms[tabcontrol.tabindex] - 384;
  key := 0;
  showhex;
 end Else
 if (key = VK_UP) and (hexgrid.Row = 0) then
 begin
  if longint(poss[tabcontrol.tabindex]) - $10 > 0 then
   Dec(poss[tabcontrol.tabindex], $10) Else
  poss[tabcontrol.tabindex] := 0;
  key := 0;
  showhex;
 end;
 If key = VK_NEXT then
 begin
  if poss[tabcontrol.tabindex] + 384 < rms[tabcontrol.tabindex] then
   inc(poss[tabcontrol.tabindex], 384) Else
  poss[tabcontrol.tabindex] := rms[tabcontrol.tabindex] - 384;
  key := 0;
  showhex;
 end Else
 If key = VK_PRIOR then
 begin
  if longint(poss[tabcontrol.tabindex]) - 384 > 0 then
   Dec(poss[tabcontrol.tabindex], 384) Else
  poss[tabcontrol.tabindex] := 0;
  key := 0;
  showhex;
 end;
 if ssCtrl in shift then if key = VK_END then
 begin
  poss[tabcontrol.tabindex] := rms[tabcontrol.tabindex] - 384;
   hexgrid.Col := 15;
   hexgrid.Row := 23;
  chargrid.Col := 15;
  chargrid.Row := 23;
   posgrid.Row := 23;
  key := 0;
  showhex;
 end Else if key = VK_HOME then
 begin
  poss[tabcontrol.tabindex] := 0;
   hexgrid.Col := 0;
   hexgrid.Row := 0;
  chargrid.Col := 0;
  chargrid.Row := 0;
   posgrid.Row := 0;
  key := 0;
  showhex;
 end;
end;

procedure THexEditForm.hexgridEnter(Sender: TObject);
begin
 hexgr := false;
end;

procedure THexEditForm.chargridEnter(Sender: TObject);
begin
 hexgr := true;
end;

procedure THexEditForm.N2Click(Sender: TObject);
begin
 close;
end;

procedure THexEditForm.N4Click(Sender: TObject);
begin
 poss[1] := poss[0];
 showhex;
end;

procedure THexEditForm.N5Click(Sender: TObject);
begin
 poss[0] := poss[1];
 showhex;
end;

procedure THexEditForm.N6Click(Sender: TObject);
var h: dword;
begin
 gotoform.Position := poDesktopcenter;
 gotoform.showmodal;
 h := hexval(gotoform.Edit1.Text);
 if not hexerror then
 begin
  if h + 384 >= rms[tabcontrol.tabindex] then
   h := rms[tabcontrol.tabindex] - 384;
  poss[tabcontrol.TabIndex] := h;
   hexgrid.Col := 0;
   hexgrid.Row := 0;
  chargrid.Col := 0;
  chargrid.Row := 0;
   posgrid.Row := 0;
  showhex;
 end Else showmessage('Fuck off!!!');
end;

procedure THexEditForm.hexgridkeypress(Sender: TObject; var Key: Char);
var c: byte;
begin
 if md then key := #0;
 key := upcase(key);
 if not (key in ['0'..'9', 'A'..'F']) then key := #0;
 if key > #0 then
 begin
  if length(hexgrid.Cells[hexgrid.Col, hexgrid.Row]) = 2 then
  begin
   hexgrid.Cells[hexgrid.Col, hexgrid.Row] := '';
   Enabled := false;
   KruptarForm.Enabled := false;
   if resultform.Visible then
    resultform.Enabled := false;
  end;
  hexgrid.Cells[hexgrid.Col, hexgrid.Row] :=
  hexgrid.Cells[hexgrid.Col, hexgrid.Row] + key;
  if length(hexgrid.Cells[hexgrid.Col, hexgrid.Row]) = 2 then
  begin
   Enabled := true;
   KruptarForm.Enabled := true;
   if resultform.Visible then
    resultform.Enabled := true;   
   c := hexval(hexgrid.Cells[hexgrid.Col, hexgrid.Row]);
   if tabcontrol.TabIndex = 0 then
    chargrid.Cells[chargrid.Col, chargrid.Row] :=
    KruptarForm.table1[0,c] Else
    chargrid.Cells[chargrid.Col, chargrid.Row] :=
    KruptarForm.table2[0,c];
   rom[tabcontrol.TabIndex]^[poss[tabcontrol.TabIndex] +
   dword(chargrid.Row) * 16 + dword(chargrid.Col)] := c;
   if hexgrid.Col < 15 then
    hexgrid.Col := hexgrid.Col + 1 Else
   begin
    hexgrid.Col := 0;
    if hexgrid.row < 23 then
     hexgrid.Row := hexgrid.Row + 1;
   end;
   chargrid.Col := hexgrid.Col;
   chargrid.Row := hexgrid.Row;
    posgrid.Row := hexgrid.Row;
  end;
  key := #0;
 end;
end;

{
procedure THexEditForm.hexgridGetEditText(Sender: TObject; ACol,
  ARow: Integer; var Value: String);
var c: char;
begin
 if length(value) > 2 then
 begin
  c := value[length(value)];
  setlength(value, 2);
  hexgrid.editormode := false;
  hexgridSetEditText(sender, acol, arow, value);
  if hexgrid.Col < 15 then
   hexgrid.Col := hexgrid.Col + 1 Else
  begin
   hexgrid.Col := 0;
   if hexgrid.row < 23 then
    hexgrid.Row := hexgrid.Row + 1;
  end;
  hexgrid.Cells[hexgrid.Col, hexgrid.Row] := c;
  hexgrid.editormode := true;
 end;
end;
}


{procedure THexEditForm.hexgridSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: String);
var s: string; v: byte;
begin
 if not hexgrid.EditorMode then
 begin
  s := value;
  if length(s) > 2 then setlength(s, 2);
  if s = '' then s := '00';
  if length(s) = 1 then s := '0' + s;
  v := hexval(s);
  rom[tabcontrol.TabIndex]^[poss[tabcontrol.TabIndex] +
  dword(ARow) * 16 + dword(ACol)] := v;
  if tabcontrol.TabIndex = 0 then
   chargrid.Cells[ACol, ARow] := KruptarForm.table1[v] Else
   chargrid.Cells[ACol, ARow] := KruptarForm.table2[v];
  hexgrid.Cells[ACol, ARow] := s;
 end;
end;
}

procedure THexEditForm.chargridKeyPress(Sender: TObject; var Key: Char);
var c: integer;
begin
  if tabcontrol.TabIndex = 1 then
   c := KruptarForm.antitable[key, 0] Else
   c := KruptarForm.antitable1[key, 0];
  if c >= 0 then
  begin
   chargrid.Cells[chargrid.Col, chargrid.Row] := key;
   hexgrid.Cells[chargrid.Col, chargrid.Row] := inttohex(c, 2);
   rom[tabcontrol.TabIndex]^[poss[tabcontrol.TabIndex] +
   dword(chargrid.Row) * 16 + dword(chargrid.Col)] := c;
   if hexgrid.Col < 15 then
    hexgrid.Col := hexgrid.Col + 1 Else
   begin
    hexgrid.Col := 0;
    if hexgrid.row < 23 then
     hexgrid.Row := hexgrid.Row + 1;
   end;
   chargrid.Col := hexgrid.Col;
   chargrid.Row := hexgrid.Row;
    posgrid.Row := hexgrid.Row;
   key := #0;
  end;
end;

procedure THexEditForm.ROM4Click(Sender: TObject);
var f: file;
begin
 AssignFile(f, orom);
 reset(f, 1);
 rms[0] := filesize(f);
 getmem(rom[0], rms[0]);
 blockread(f, rom[0]^, rms[0]);
 closefile(f);
 showhex;
end;


procedure THexEditForm.ROM1Click(Sender: TObject);
var f: file;
begin
 AssignFile(f, nrom);
 reset(f, 1);
 rms[1] := filesize(f);
 getmem(rom[1], rms[1]);
 blockread(f, rom[1]^, rms[1]);
 closefile(f);
 showhex;
end;

procedure THexEditForm.ROM3Click(Sender: TObject);
var f: file;
begin
 AssignFile(f, orom);
 rewrite(f, 1);
 blockwrite(f, rom[0]^, rms[0]);
 closefile(f);
end;


procedure THexEditForm.ROM2Click(Sender: TObject);
var f: file;
begin
 AssignFile(f, nrom);
 rewrite(f, 1);
 blockwrite(f, rom[1]^, rms[1]);
 closefile(f);
end;

procedure THexEditForm.FormCreate(Sender: TObject);
begin
 poss[0] := 0; poss[1] := 0;
end;

procedure THexEditForm.chargridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var a, b: integer;  
begin
 if not chargrid.Focused then exit;
 a := ARow;
 b := ACol;
 if posgrid.Row <> a then posgrid.Row := a;
 if hexgrid.Col <> b then hexgrid.Col := b;
 if hexgrid.Row <> a then hexgrid.Row := a;
end;

procedure THexEditForm.TimerTimer(Sender: TObject);
begin
 Label1.Caption := '��������: ' +
  inttohex(poss[tabcontrol.tabindex] + dword(hexgrid.Row) * 16 +
  dword(hexgrid.Col), 8);
 Label2.Caption := '������:      ' +
  inttohex(rms[tabcontrol.tabindex], 8);
 Label3.Caption := '������:   ' +
  inttohex(hexgrid.Row, 2) + ' = ' +
  inttostr(hexgrid.Row);
 Label4.Caption := '�������: ' +
  inttohex(hexgrid.Col, 2) + ' = ' +
  inttostr(hexgrid.Col);
 Label5.Caption := 'Hex: ' +
  inttohex(rom[tabcontrol.tabindex]^[
  poss[tabcontrol.tabindex] + dword(hexgrid.Row) * 16 +
  dword(hexgrid.Col)], 2);
 Label6.Caption := 'Dec: ' +
  inttostr(rom[tabcontrol.tabindex]^[
  poss[tabcontrol.tabindex] + dword(hexgrid.Row) * 16 +
  dword(hexgrid.Col)]);
end;

procedure THexEditForm.N8Click(Sender: TObject);
begin
 n8.Checked := not n8.Checked;
end;

procedure THexEditForm.N3Click(Sender: TObject);
begin
 if n8.Checked then
 begin
  n4.Enabled := false;
  n5.Enabled := false;
 end Else
 begin
  n4.Enabled := true;
  n5.Enabled := true;
 end;
end;

procedure THexEditForm.hexgridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 md := true;
 hs := true;
end;

procedure THexEditForm.hexgridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 md := false;
 hs := false;
end;

procedure THexEditForm.hexgridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var a, b: integer;
begin
 if not hexgrid.Focused then exit;
 a := ARow;
 b := ACol;
 if posgrid.Row <> a then posgrid.Row := a;
 if chargrid.Col <> b then chargrid.Col := b;
 if chargrid.Row <> a then chargrid.Row := a;
end;

procedure THexEditForm.chargridMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 cs := true;
end;

procedure THexEditForm.chargridMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 cs := false;
end;

procedure THexEditForm.hexgridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
{ if odd(arow) then
 begin
  if odd(acol) and (acol <> 0) then
   hexgrid.Font.Color := clYellow
  Else
   hexgrid.Font.Color := clWhite;
 end Else
 begin
  if not odd(acol) or (acol = 0) then
   hexgrid.Font.Color := clYellow
  Else
   hexgrid.Font.Color := clWhite;
 end   }
end;

procedure THexEditForm.chargridExit(Sender: TObject);
begin
 hexgrid.setfocus;
end;

procedure THexEditForm.Change1Click(Sender: TObject);
begin
 tabcontrol.TabIndex := byte(not boolean(tabcontrol.TabIndex));
 tabcontrolchange(sender);
end;

end.
