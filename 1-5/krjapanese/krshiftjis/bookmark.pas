unit bookmark;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  Tbmform = class(TForm)
    nfield: TEdit;
    okbtn: TButton;
    cncbtn: TButton;
    procedure cncbtnClick(Sender: TObject);
    procedure okbtnClick(Sender: TObject);
    procedure nfieldKeyPress(Sender: TObject; var Key: Char);
    procedure nfieldChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  bmform: Tbmform; bmss: string;

implementation

{$R *.dfm}

procedure Tbmform.cncbtnClick(Sender: TObject);
begin
 bmss := '';
 close;
end;

procedure Tbmform.okbtnClick(Sender: TObject);
begin
 bmss := nfield.Text;
 close;
end;

procedure Tbmform.nfieldKeyPress(Sender: TObject; var Key: Char);
begin
 if key = ' ' then key := '_';
end;

procedure Tbmform.nfieldChange(Sender: TObject);
var b: integer; s: string;
begin
 s := nfield.Text;
 b := pos(' ', s);
 if b > 0 then
 begin
  s[b] := '_';
  nfield.Text := s;
 end;
end;

end.
