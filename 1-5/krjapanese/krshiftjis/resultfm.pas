unit resultfm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TResultForm = class(TForm)
    Resmemo: TMemo;
    procedure ResmemoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ResultForm: TResultForm;

implementation

{$R *.dfm}

procedure TResultForm.ResmemoChange(Sender: TObject);
begin
 if resmemo.Lines.Count = 0 then Close;
end;

end.
