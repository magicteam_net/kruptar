unit emucfg;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Dialogs, Buttons;

type
  TEmuCfgForm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    emuedit: TLabeledEdit;
    paramedit: TLabeledEdit;
    SpeedButton1: TSpeedButton;
    eOpenDialog: TOpenDialog;
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  end;

var
  EmuCfgForm: TEmuCfgForm;

implementation

{$R *.DFM}

uses ranmaedit;

procedure TEmuCfgForm.SpeedButton1Click(Sender: TObject);
var s: string;
begin
 getdir(0, s);
 if eopendialog.Execute then
  emuedit.Text := eopendialog.Filename;
 chdir(s);
end;

procedure TEmuCfgForm.Button1Click(Sender: TObject);
var f: textfile; e, u: string;
begin
 e := savename;
 delete(e, pos('.rnm', e), 4);
 assignfile(f, e + '.emu');
 rewrite(f);
 e := emuedit.text;
 u := paramedit.Text;
 writeln(f, e);
 writeln(f, u);
 closefile(f);
end;

procedure TEmuCfgForm.FormShow(Sender: TObject);
var f: textfile; e, u: string;
begin
 e := savename;
 delete(e, pos('.rnm', e), 4);
 assignfile(f, e + '.emu');
 {$I-}
 reset(f);
 {$I+}
 if ioresult = 0 then
 begin
  readln(f, e);
  readln(f, u);
  closefile(f);
  emuedit.Text := e;
  paramedit.Text := u;
 end Else
 begin
  emuedit.Text := '';
  paramedit.Text := '%rom%';
 end;
end;

end.
