object crtrForm: TcrtrForm
  Left = 556
  Top = 158
  AutoSize = True
  BorderStyle = bsDialog
  Caption = #1048#1085#1080#1094#1080#1072#1083#1080#1079#1072#1094#1080#1103
  ClientHeight = 281
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 170
    Width = 3
    Height = 13
  end
  object Label2: TLabel
    Left = 0
    Top = 170
    Width = 22
    Height = 13
    Caption = #1058#1080#1087':'
  end
  object Label3: TLabel
    Left = 0
    Top = 248
    Width = 266
    Height = 16
    Caption = #1052#1077#1089#1090#1072' '#1076#1083#1103' '#1087#1077#1088#1077#1074#1077#1076#1105#1085#1085#1086#1075#1086' '#1090#1077#1082#1089#1090#1072'  ^'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object orome: TLabeledEdit
    Left = 0
    Top = 16
    Width = 353
    Height = 21
    EditLabel.Width = 103
    EditLabel.Height = 13
    EditLabel.Caption = #1054#1088#1080#1075#1080#1085#1072#1083#1100#1085#1099#1081' ROM'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 0
  end
  object Button1: TButton
    Left = 360
    Top = 16
    Width = 67
    Height = 21
    Caption = #1054#1090#1082#1088#1099#1090#1100'...'
    TabOrder = 1
    OnClick = Button1Click
  end
  object nrome: TLabeledEdit
    Left = 0
    Top = 56
    Width = 353
    Height = 21
    EditLabel.Width = 96
    EditLabel.Height = 13
    EditLabel.Caption = #1048#1079#1084#1077#1085#1103#1077#1084#1099#1081' ROM'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 2
  end
  object Button2: TButton
    Left = 360
    Top = 56
    Width = 67
    Height = 21
    Caption = #1054#1090#1082#1088#1099#1090#1100'...'
    TabOrder = 3
    OnClick = Button2Click
  end
  object tab1e: TLabeledEdit
    Left = 0
    Top = 96
    Width = 353
    Height = 21
    EditLabel.Width = 179
    EditLabel.Height = 13
    EditLabel.Caption = #1058#1072#1073#1083#1080#1094#1072' '#1076#1083#1103' '#1086#1088#1080#1075#1080#1085#1072#1083#1100#1085#1086#1075#1086' ROM'#39'a'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 4
  end
  object Button3: TButton
    Left = 360
    Top = 96
    Width = 67
    Height = 21
    Caption = #1054#1090#1082#1088#1099#1090#1100'...'
    TabOrder = 5
    OnClick = Button3Click
  end
  object tab2e: TLabeledEdit
    Left = 0
    Top = 136
    Width = 353
    Height = 21
    EditLabel.Width = 172
    EditLabel.Height = 13
    EditLabel.Caption = #1058#1072#1073#1083#1080#1094#1072' '#1076#1083#1103' '#1080#1079#1084#1077#1085#1103#1077#1084#1086#1075#1086' ROM'#39'a'
    LabelPosition = lpAbove
    LabelSpacing = 3
    TabOrder = 6
  end
  object Button4: TButton
    Left = 360
    Top = 136
    Width = 67
    Height = 21
    Caption = #1054#1090#1082#1088#1099#1090#1100'...'
    TabOrder = 7
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 272
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    TabOrder = 8
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 352
    Top = 256
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 9
    OnClick = Button6Click
  end
  object Edit1: TEdit
    Left = 304
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 11
    Text = '0'
    OnKeyPress = Edit1KeyPress
  end
  object ptypecombo: TComboBox
    Left = 24
    Top = 168
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 10
    OnChange = ptypecomboChange
    Items.Strings = (
      'No Pointers (sl)'
      'No Pointers (pd)'
      '2 Byte NES'
      '2 Byte NES (1B StL)'
      '2 Byte NES (1B StL2)'
      '2 Byte NES (1B StL2l)'
      '2 Byte SMD'
      '2 Byte Signed'
      '3 Byte SNES'
      '4 Byte GBA'
      '4 Byte SMD (Barver Type)'
      '4 Byte SMD'
      '4 Byte SMD (PiratesG)')
  end
  object Panel1: TPanel
    Left = 0
    Top = 200
    Width = 425
    Height = 41
    BorderStyle = bsSingle
    TabOrder = 12
    object pttaa: TEdit
      Left = 12
      Top = 8
      Width = 65
      Height = 21
      TabOrder = 0
      OnKeyPress = pttaeKeyPress
    end
    object pttae: TEdit
      Tag = 1
      Left = 84
      Top = 8
      Width = 65
      Height = 21
      TabOrder = 1
      OnKeyPress = pttaeKeyPress
    end
    object addpetbtn: TButton
      Left = 156
      Top = 8
      Width = 65
      Height = 21
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      TabOrder = 2
      OnClick = addpetbtnClick
    end
    object emplslc: TComboBox
      Left = 264
      Top = 8
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
    end
  end
  object CheckBox1: TCheckBox
    Left = 0
    Top = 264
    Width = 65
    Height = 17
    Caption = 'Shift-JIS'
    TabOrder = 13
  end
  object OpenDialog: TOpenDialog
    Filter = '*.*|*.*'
    Left = 184
    Top = 168
  end
end
