unit stopt;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Spin, Dialogs;

type
  TStartOptionsF = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel: TBevel;
    orome: TLabeledEdit;
    nrome: TLabeledEdit;
    tab1e: TLabeledEdit;
    tab2e: TLabeledEdit;
    PtrSizeR: TRadioGroup;
    SignedC: TCheckBox;
    PtrPan: TPanel;
    NoPtrC: TCheckBox;
    KratR: TRadioGroup;
    MethodsR: TRadioGroup;
    etla: TLabel;
    MotorolaC: TCheckBox;
    oromebtn: TSpeedButton;
    nromebtn: TSpeedButton;
    tab1ebtn: TSpeedButton;
    tab2ebtn: TSpeedButton;
    Fplab: TLabel;
    KodBox: TComboBox;
    KodL: TLabel;
    bpose: TEdit;
    epose: TEdit;
    addpetbtn: TButton;
    emplslc: TComboBox;
    mestl: TLabel;
    bposl: TLabel;
    eposl: TLabel;
    FirstPtrE: TEdit;
    StrLenE: TLabeledEdit;
    OpenDialog: TOpenDialog;
    Panel1: TPanel;
    Notables: TCheckBox;
    TypeGroup: TRadioGroup;
    procedure oromebtnClick(Sender: TObject);
    procedure nromebtnClick(Sender: TObject);
    procedure tab1ebtnClick(Sender: TObject);
    procedure tab2ebtnClick(Sender: TObject);
    procedure NoPtrCClick(Sender: TObject);
    procedure FirstPtrEKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure addpetbtnClick(Sender: TObject);
    procedure OKBtnClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CancelBtnClick(Sender: TObject);
    procedure TypeGroupClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StartOptionsF: TStartOptionsF; fok: boolean;

implementation

{$R *.dfm}

Uses RanmaEdit;

procedure TStartOptionsF.oromebtnClick(Sender: TObject);
begin
 opendialog.FileName := orome.Text;
 if opendialog.Execute then
  orome.Text := opendialog.FileName;
end;

procedure TStartOptionsF.nromebtnClick(Sender: TObject);
begin
 opendialog.FileName := nrome.Text;
 if opendialog.Execute then
  nrome.Text := opendialog.FileName;
end;

procedure TStartOptionsF.tab1ebtnClick(Sender: TObject);
var s: string;
begin
 opendialog.DefaultExt := '.tbl';
 s := opendialog.Filter;
 opendialog.Filter := '�������|*.tbl';
 opendialog.FileName := tab1e.Text;
 if opendialog.Execute then
  tab1e.Text := opendialog.FileName;
 opendialog.DefaultExt := '';
 opendialog.Filter := s;
end;

procedure TStartOptionsF.tab2ebtnClick(Sender: TObject);
var s: string;
begin
 opendialog.DefaultExt := '.tbl';
 s := opendialog.Filter;
 opendialog.Filter := '������� (*.tbl)|*.tbl';
 opendialog.FileName := tab2e.Text; 
 if opendialog.Execute then
  tab2e.Text := opendialog.FileName;
 opendialog.DefaultExt := '';
 opendialog.Filter := s;
end;

procedure TStartOptionsF.NoPtrCClick(Sender: TObject);
begin
 panel1.Enabled := not NoPtrC.Checked;
 StrLenE.Enabled := NoPtrC.Checked;
end;

procedure TStartOptionsF.FirstPtrEKeyPress(Sender: TObject; var Key: Char);
begin
 key := upcase(key);
 if key = #13 then key := #0;
 if not (key  in ['0'..'9', 'A'..'F', #8, '-']) then key := #0;
end;

procedure TStartOptionsF.FormActivate(Sender: TObject);
begin
 fok := false;
 orome.Text := orom;
 nrome.Text := nrom;
 tab1e.Text := tab1;
 tab2e.Text := tab2;
 emplslc.Clear;
 If xNoPtr then
 begin
  Panel1.Enabled := False;
  StrLenE.Enabled := True;
  NoPtrC.Checked := True;
  StrLenE.Text := InttoStr(xStrLen);
  FirstPtrE.Text := '';
 end Else
 begin
  Panel1.Enabled := true;
  StrLenE.Enabled := False;
  StrLenE.Text := '0';
  NoPtrC.Checked := false;
  if xPtrsize < 2 then xPtrsize := 2;
  if xPtrsize > 4 then xPtrsize := 4;  
  PtrSizeR.ItemIndex := xPtrSize - 2;
  FirstPtrE.Text := InttoHex(xFirstPtr, 8);
  Fplab.Enabled := (typegroup.ItemIndex < 2);
  FirstPtrE.Enabled := (typegroup.ItemIndex < 2);
  Case xKrat of
   1: KratR.ItemIndex := 0;
   2: KratR.ItemIndex := 1;
   4: KratR.ItemIndex := 2;
   8: KratR.ItemIndex := 3;
   Else KratR.ItemIndex := 0;
  end;
  MethodsR.ItemIndex := Byte(xMethod);
  MotorolaC.Checked := xMotorola;
  SignedC.Checked := xSigned;
  If xpntp  then TypeGroup.ItemIndex := 1 Else
  if xAutostart then TypeGroup.ItemIndex := 2 Else
                 TypeGroup.ItemIndex := 0;
 end;
 emplslc.Clear;
 KodBox.ItemIndex := xKodir;
end;

procedure TStartOptionsF.addpetbtnClick(Sender: TObject);
begin
 emplslc.Items.Add(bpose.Text);
 emplslc.Items.Add(epose.Text);
 emplslc.ItemIndex := 0;
end;

var uuu: boolean;

procedure TStartOptionsF.OKBtnClick(Sender: TObject);
var i: integer; s: string;
begin
 if (orome.Text = '') or
    (nrome.Text = '') or
    (tab1e.Text = '') or
    (tab2e.Text = '') then
 begin
  ShowMessage('����������� ���� �� ���������!');
  uuu := true;
 end Else
 begin
  uuu := false;
  orom := orome.Text;
  tab2 := tab2e.Text;
  nrom := nrome.Text;
  tab1 := tab1e.Text;
  xPtrSize := PtrSizer.ItemIndex + 2;
  xKodir := kodbox.ItemIndex;
  xMotorola := MotorolaC.Checked;
  xSigned := SignedC.Checked;
  xNoPtr :=  NoPtrC.Checked;
  xStrLen := StrToInt(StrLenE.Text);
  xNoTables := NoTables.Checked;
  Case KratR.ItemIndex of
   0: xKrat := 1;
   1: xKrat := 2;
   2: xKrat := 4;
   3: xKrat := 8;
  end;
  xPntp := (TypeGroup.ItemIndex = 1);
  xAutoStart := (TypeGroup.ItemIndex = 2);
  xMethod := tpmett(MethodsR.ItemIndex);
  if xNoPtr then
  begin
   xKrat := 1;
   xMethod := tmNormal;
  end;
  If not xNoPtr then
  begin
   if FirstPtrE.Text[1] = '-' then
   begin
    s := FirstPtrE.Text;
    Delete(s, 1, 1);
    xFirstPtr := -hexval(s);
   end else xFirstPtr := hexval(FirstPtrE.Text);
   if xGBA then Dec(xFirstPtr, $08000000);
  end;
  emptyc := emplslc.Items.Count div 2;
  for i := 0 to emptyc - 1 do
  begin
   emptys^[i].a := hexval(emplslc.Items.strings[i * 2]);
   emptys^[i].b := hexval(emplslc.Items.strings[i * 2 + 1]);
  end;
  fok := true;
  close;
 end;
end;

procedure TStartOptionsF.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
 canclose := not uuu;
 if uuu then uuu := false;
end;

procedure TStartOptionsF.CancelBtnClick(Sender: TObject);
begin
 uuu := false;
end;

procedure TStartOptionsF.TypeGroupClick(Sender: TObject);
begin
 Fplab.Enabled := (typegroup.ItemIndex < 2);
 FirstPtrE.Enabled := (typegroup.ItemIndex < 2);
end;

end.
