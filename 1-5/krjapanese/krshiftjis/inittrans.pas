unit inittrans;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TcrtrForm = class(TForm)
    orome: TLabeledEdit;
    Button1: TButton;
    nrome: TLabeledEdit;
    Button2: TButton;
    tab1e: TLabeledEdit;
    Button3: TButton;
    tab2e: TLabeledEdit;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    OpenDialog: TOpenDialog;
    Edit1: TEdit;
    ptypecombo: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    pttaa: TEdit;
    pttae: TEdit;
    addpetbtn: TButton;
    emplslc: TComboBox;
    Label3: TLabel;
    CheckBox1: TCheckBox;
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure pttaeKeyPress(Sender: TObject; var Key: Char);
    procedure addpetbtnClick(Sender: TObject);
    procedure ptypecomboChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  crtrForm: TcrtrForm; fok: boolean;

implementation

{$R *.dfm}

Uses RanmaEdit;

procedure TcrtrForm.Button5Click(Sender: TObject);
var i: integer;
begin
 if (orome.Text = '') or
    (nrome.Text = '') or
    (tab1e.Text = '') or
    (tab2e.Text = '') or
    (edit1.Text = '') then
  ShowMessage('��� ���� ������ ���� ���������!')
 Else begin
  orom := orome.Text;
  tab2 := tab2e.Text;
  nrom := nrome.Text;
  tab1 := tab1e.Text;
  ptrtype := tptrtype(ptypecombo.ItemIndex);
  dummyuse := checkbox1.Checked;
  if not dummyuse then
  begin
   with KruptarForm.slist.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
   with KruptarForm.memo.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
   with KruptarForm.rusmemo.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
  end Else
  begin
   with KruptarForm.slist.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -16;
    size := 12;
    Style := [fsBold];
   end;
   with KruptarForm.memo.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -19;
    size := 14;
    Style := [fsBold];
   end;
   with KruptarForm.rusmemo.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -19;
    size := 14;
    Style := [fsBold];
   end;
  end;
  if edit1.text[1] = '-' then
   oofs := strtoint(edit1.text) else
   oofs := hexval(edit1.Text);
  if ptrtype = pt4bGBA then inc(oofs, -$08000000);
  emptyc := emplslc.Items.Count div 2;
  for i := 0 to emptyc - 1 do
  begin
   emptys^[i].a := hexval(emplslc.Items.strings[i * 2]);
   emptys^[i].b := hexval(emplslc.Items.strings[i * 2 + 1]);
  end;
  fok := true;
  close;
 end;
end;

procedure TcrtrForm.Button6Click(Sender: TObject);
begin
 close;
end;

procedure TcrtrForm.Button1Click(Sender: TObject);
begin
 if opendialog.Execute then
 begin
  orome.Text := opendialog.FileName;
 end;
end;

procedure TcrtrForm.Button2Click(Sender: TObject);
begin
 if opendialog.Execute then
 begin
  nrome.Text := opendialog.FileName;
 end;
end;

procedure TcrtrForm.Button3Click(Sender: TObject);
begin
 if opendialog.Execute then
 begin
  tab1e.Text := opendialog.FileName;
 end;
end;

procedure TcrtrForm.Button4Click(Sender: TObject);
begin
 if opendialog.Execute then
 begin
  tab2e.Text := opendialog.FileName;
 end;
end;

procedure TcrtrForm.FormShow(Sender: TObject);
begin
 ptypecombochange(sender);
 fok := false;
 orome.Text := orom;
 nrome.Text := nrom;
 tab1e.Text := tab1;
 tab2e.Text := tab2;
 edit1.Text := '0';
 ptypecombo.ItemIndex := integer(ptrtype);
 emplslc.Clear;
 if ptypecombo.ItemIndex = 0 then
  label1.Caption := '  ����� ������:' Else
  label1.Caption := '������ �������:';
 label1.Enabled := true;
 edit1.Enabled := true;
end;

procedure TcrtrForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
 key := upcase(key);
end;

procedure TcrtrForm.pttaeKeyPress(Sender: TObject; var Key: Char);
begin
 key := upcase(key);
 if key = #13 then
 begin
  key := #0;
  Case tedit(sender).Tag of
   0: pttae.SetFocus;
   1: addpetbtn.SetFocus;
  end;
 end;
 if not (key  in ['0'..'9', 'A'..'F', #8]) then key := #0;
end;

procedure TcrtrForm.addpetbtnClick(Sender: TObject);
begin
 emplslc.Items.Add(pttaa.Text);
 emplslc.Items.Add(pttae.Text);
end;

procedure TcrtrForm.ptypecomboChange(Sender: TObject);
begin
 if ptypecombo.ItemIndex = 0 then
  label1.Caption := '  ����� ������:' Else
  label1.Caption := '������ �������:';
end;

end.
