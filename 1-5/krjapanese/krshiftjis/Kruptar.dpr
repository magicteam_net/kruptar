program Kruptar;

uses
  Forms,
  RanmaEdit in 'RanmaEdit.pas' {KruptarForm},
  bookmark in 'bookmark.pas' {bmform},
  process in 'process.pas' {procform},
  resultfm in 'resultfm.pas' {ResultForm},
  hexedit in 'hexedit.pas' {HexEditForm},
  gotou in 'gotou.pas' {gotoform},
  uniconv in 'uniconv.pas',
  FRep in 'FRep.pas' {FindReplaceForm},
  emucfg in 'emucfg.pas' {EmuCfgForm},
  stopt in 'stopt.pas' {StartOptionsF},
  opttd in 'opttd.pas' {OptDlg},
  dict in 'dict.pas' {DictForm},
  doall in 'doall.pas' {ProcessForm};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Kruptar 5.0.4.0';
  Application.CreateForm(TKruptarForm, KruptarForm);
  Application.CreateForm(Tbmform, bmform);
  Application.CreateForm(Tprocform, procform);
  Application.CreateForm(TResultForm, ResultForm);
  Application.CreateForm(THexEditForm, HexEditForm);
  Application.CreateForm(Tgotoform, gotoform);
  Application.CreateForm(TFindReplaceForm, FindReplaceForm);
  Application.CreateForm(TEmuCfgForm, EmuCfgForm);
  Application.CreateForm(TStartOptionsF, StartOptionsF);
  Application.CreateForm(TOptDlg, OptDlg);
  Application.CreateForm(TDictForm, DictForm);
  Application.CreateForm(TProcessForm, ProcessForm);
  Application.Run;
end.
