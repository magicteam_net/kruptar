unit doall;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms;

type
  TProcessForm = class(TForm)
    Bevel: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    cntlab: TLabel;
    inslab: TLabel;
    closebtn: TButton;
    Label3: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure closebtnClick(Sender: TObject);
  end;

var
  ProcessForm: TProcessForm; act: boolean;

implementation

{$R *.DFM}

uses ranmaedit, hexedit;

procedure TProcessForm.FormActivate(Sender: TObject);
begin
 if act then exit;
 act := true;
 cntlab.Caption := '';
 inslab.Caption := '';
 closebtn.Enabled := false;
 if hexeditform.Visible then hexeditform.FormHide(sender);
 kruptarform.cntbtnclick(sender);
 cntlab.Caption := '������!';
 Refresh;
 kruptarform.instransClick(sender);
 inslab.Caption := '������!';
 Refresh;
 label3.Caption := '������ ���������: ��';
 kruptarform.emulateClick(sender);
 if cantemul then label3.Caption := '���� � ��������� �� ����������!';
 closebtn.Enabled := true;
end;

procedure TProcessForm.closebtnClick(Sender: TObject);
begin
 act := false;
 close;
end;

end.
