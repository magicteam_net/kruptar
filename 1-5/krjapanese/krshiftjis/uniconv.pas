unit uniconv;

interface

type
 wstring = widestring;

function ShiftJIS(u: WideChar): string;
function SJIStext(u: WString): string;
function Unicode(sj: string): WideChar;
function Unitext(s: string): wstring;

implementation

uses Windows, sysutils;

{$I unic.pas}
{$I sjis.pas}

function ShiftJIS(u: WideChar): string;
begin
 if (word(u) >= 32) and (word(u) <= $FFE5) then
  result := aShftJIS[word(u)]
 Else if u = #9 then result := #9 else result := '';
end;

function SJIStext(u: WString): string;
var i: integer;
begin
 result := '';
 for i := 1 to length(u) do result := result + ShiftJIS(u[i]);
end;
type wd = record a, b: byte end;
function Unicode(sj: string): WideChar;
var {i: word;} bb: word;
begin
{ if sj = #9 then
 begin
  result := #9;
  Exit;
 end Else result := #0;}
 result := #0;
 if sj = '' then exit;
 wd(bb).b := byte(sj[1]);
 if length(sj) = 2 then wd(bb).a := byte(sj[2]) else wd(bb).a := 0;
 result := widechar(aUNISJIS[bb]);
{ for i := 32 to $FFE5 do if aShftJIS[i] = sj then
 begin
  result := widechar(i);
  break;
 end;                }
end;

function Unitext(s: string): wstring;
var i: integer; w: widechar; pr: boolean;
begin
 result := ''; pr := false;
 for i := 1 to length(s) do
 begin
  if pr then pr := false Else
  begin
   if s[i] >= #128 then
   begin
    w := Unicode(s[i] + s[i + 1]);
    pr := true;  
   end Else
   begin
    wd(w).b := 0;
    wd(w).a := byte(s[i]);
   end;
   result := result + w;
  end;
 end;
end;

end.
