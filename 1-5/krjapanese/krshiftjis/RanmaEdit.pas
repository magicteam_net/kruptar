unit RanmaEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, Buttons, ExtCtrls, ComCtrls,
  UnicodeEdit, UnicodeMemo, Grids, UnicodeLists, uniconv,
  ShellApi, OleCtrls, SHDocVw, Psock, NMHttp, mmsystem;

const curver = 5040;

type
  TKruptarForm = class(TForm)
    Memo: TUnicodeMemo;
    MainMenu: TMainMenu;
    loaditem: TMenuItem;
    optrans: TMenuItem;
    newtrans: TMenuItem;
    savetrans: TMenuItem;
    savetras: TMenuItem;
    instrans: TMenuItem;
    N7: TMenuItem;
    exitprog: TMenuItem;
    ptta: TEdit;
    addptbtn: TButton;
    pttacnt: TEdit;
    pttblbox: TListBox;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    N1: TMenuItem;
    rusmemo: TUnicodeMemo;
    rpos: TEdit;
    N2: TMenuItem;
    FindDialog: TFindDialog;
    N3: TMenuItem;
    N4: TMenuItem;
    cntbtn: TButton;
    epos: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    N5: TMenuItem;
    Timer1: TTimer;
    justenter: TRadioButton;
    umnenter: TRadioButton;
    starenter: TRadioButton;
    tildaenter: TRadioButton;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    bmlist: TComboBox;
    N6: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    pbtn: TButton;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    HexEditor1: TMenuItem;
    SpeedButton1: TSpeedButton;
    N18: TMenuItem;
    N19: TMenuItem;
    japl: TLabel;
    emulate: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    StatusBar: TStatusBar;
    Upd: TNMHTTP;
    N24: TMenuItem;
    N25: TMenuItem;
    Update1: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    slist: TUnicodeListBox;
    Copypaste: TPopupMenu;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    memopop: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    N38: TMenuItem;
    FontDialog: TFontDialog;
    N39: TMenuItem;
    N40: TMenuItem;
    QuickInsert: TMenuItem;
    Button1: TButton;
    N42: TMenuItem;
    N43: TMenuItem;
    N44: TMenuItem;
    N45: TMenuItem;
    ProgressItem: TMenuItem;
    procedure newtransClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure savetransClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure exitprogClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure addptbtnClick(Sender: TObject);
    procedure pttaKeyPress(Sender: TObject; var Key: Char);
    procedure pttblboxClick(Sender: TObject);
    procedure optransClick(Sender: TObject);
    procedure savetrasClick(Sender: TObject);
    procedure rusmemoChange(Sender: TObject);
    procedure instransClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FindDialogFind(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure cntbtnClick(Sender: TObject);
    procedure slistClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure loaditemClick(Sender: TObject);
    procedure rusmemoKeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    procedure addbmbtnClick(Sender: TObject);
    procedure delbuttnClick(Sender: TObject);
    procedure pbtnClick(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure HexEditor1Click(Sender: TObject);
    procedure slistKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rusmemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure emulateClick(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure slistDblClick(Sender: TObject);
    procedure Update1Click(Sender: TObject);
    procedure UpdSuccess(Cmd: CmdType);
    procedure N26Click(Sender: TObject);
    procedure tildaenterClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure N33Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N36Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure N38Click(Sender: TObject);
    procedure pttblboxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N39Click(Sender: TObject);
    procedure QuickInsertClick(Sender: TObject);
    procedure N41Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure N44Click(Sender: TObject);
    procedure N45Click(Sender: TObject);
    procedure ProgressItemClick(Sender: TObject);
   private
    { Private declarations }
  public
    procedure initall;
    procedure loadtab(tn: integer; ff: string);
    Function GetATitem(x: string; tabn: integer): dword;
    Function GetUTitem(x: string; tabn: integer): dword;
    Function GetA1Titem(x: string; tabn: integer): dword;
    Function GetTitem(tabn, x: dword): string;
    Procedure SetTitem(tabn, x: dword; value: string);
    Function GetT2item(tabn, x: dword): string;
    Function GetMLen(tabn: integer): integer;
    Function Getcc: integer;
    Function Getic: integer;
    function mvalue: integer;
    Procedure Setcc(value: integer);
    Procedure Setic(value: integer);
    Procedure SetT2item(tabn, x: dword; value: string);
    property antitable[X: string; tabn: integer]: dword read GetATitem;
    property antiutable[X: string; tabn: integer]: dword read GetUTitem;
    property antitable1[X: string; tabn: integer]: dword read GetA1Titem;
    property table1[tabn, X: dword]: string read GetTitem write SetTitem;
    property table2[tabn, X: dword]: string read GetT2item write SetT2item;
    property chmaxlen[tabn: integer]: integer read GetMLen;
    property countcounter: integer read getcc write setcc;
    property inscounter: integer read getic write setic;
    { Public declarations }
  end;

Function HexVal(s: string): LongInt;

var exedir: string;

type tfd = array[0..2] of tfontdata;

type
 tromdata = array[0..2000000] of byte;
 tlongs = record
  a, b: dword;
 end;
 tldata = array[0..8191] of tlongs;

var endsym: boolean;
var cc, ic: integer;

type
 pctable = ^tctable;
 tctable = record
  s: string;
  tl: byte;
  endstr: boolean;
  undo: boolean;
  eundo: string;  
  p: dword;
  next: pctable;
 end;
 pctables = ^tctables;
 tctables = record
  tab, cur: pctable;
  count: integer;
  cl: byte;
  fname: string;
  next: pctables;
 end;
 tctableso = object
  rootO, curO: pctables;
  rootE, curE: pctables;
  Count: Integer;
  Constructor Init;
  Procedure Add(f1, f2: string);
  Procedure CheckTables;
  Destructor Done;
 end;
 pttable = ^tttable;
 tttable = record
  tptr: dword;
  same: dword;
  text: string;
  sinl: word;
  next: pttable;
 end;
 pptrtable = ^tptrtable;
 Tptrtable = object
  root, cur: pttable;
  count, bcnt: integer;
  pos: dword;
  constructor init;
  procedure add(tp: dword; s: string);
  procedure loadadd(tp: dword; s: string);  
  destructor clear;
 end;
 pposs = ^tposs;
 tposs = record
  ptrtable: pptrtable;
  tabn: integer;
  next: pposs;
 end;
 tposso = object
  root, cur: pposs;
  count: integer;
  constructor init;
  procedure add(tn: integer; p, cnt: dword);
  procedure adde(tn: integer; p: dword; pp: pptrtable);
  destructor clear;
 end;
 tpossor = object
  root, cur: pposs;
  count: integer;
  constructor init;
  procedure addposso;
  destructor clear;
 end;

 Type
 TConfigData = Record
  quickins: boolean;
  ftop, fleft, fwidth, fheight: integer;
 end;


type
 tptrtype = (ptNo, ptNo2, pt2bNES, pt2bNESlen,
 pt2bNESlen2, pt2bNESlen3, pt2bSMD, pt2bSigned, pt3bSNES, pt4bGBA, pt4bSMD,
             pt4bSMDbt2, pt4bSMDpg);

const pnames: array[tptrtype] of string =
('NoPtr', 'NoPtr2', '2bNES', '2bNESlen', '2bNESlen2',
'2bNESlen3', '2bSMD', '2bSigned', '3bSNES', '4bGBA', '4bSMD',
 '4bSMDbt2', '4bSMDpg');

type
 tpmett = (tmNormal, tmL1T, tmL1Te, tmL1TL1T, tmC2S1eSne, tmHat, tmBkp, tmNone);

const ptcount = byte(pt4bSMDpg);
const mtcount = byte(tmNone);

Const
 kdANSI = 0;
 kdSJIS = 1;

var
//NEWb
  xGBA: Boolean;
  xPtrSize: Byte;
  xKrat: Byte;
  xMethod: tpmett;
  xSigned: Boolean;
  xMotorola: Boolean;
  xKodir: Integer;
  xFirstPtr: LongInt;
  xNoPtr: Boolean;
  xStrLen: Word;
  xNoTables: boolean;
  xPntp: boolean;
  xAutoStart: boolean;
  xNoend: boolean;
//NEWe
  KruptarForm: TKruptarForm;
  eundostr, orom, nrom, tab1, tab2: string;
  saved, named: boolean;
  created: boolean;
  rom: ^tromdata; rsz: dword;
  ffile: file;
  ps, es, ed, ee: dword;
  hexerror: boolean; oofs: longint;
  posso: tposso; possor: tpossor;
  savename: string;
  addd: boolean; eee: boolean;
  curp, curp2: pttable; cui: integer;
  emptys: ^tldata; emptyc: integer;
  ptrtype: tptrtype; dummyuse: boolean;
  bububu: boolean;
  Otables: tctableso;
  pptabl: boolean;
  maxstrlen: byte;
  cantemul: boolean;


implementation

uses bookmark, process, resultfm, hexedit, FRep, emucfg, gotou,
  stopt, opttd, dict, doall;

{$R *.dfm}
{$R SOUND.RES}

procedure loadrom;
begin
 if rsz > 0 then
 begin
  freemem(rom, rsz + 4);
  posso.clear;
  possor.clear;
 end;
 assignfile(ffile, orom);
 reset(ffile, 1);
 rsz := filesize(ffile);
 getmem(rom, rsz + 4);
 blockread(ffile, rom^, rsz);
 close(ffile);
end;

var tadlen: byte;

var tt, undsym: boolean;
procedure tKruptarForm.loadtab(tn: integer; ff: string);
var tfile: textfile; s: string; p, b: integer; c: string;
eeb, unb: boolean;
begin
 eeb := false; unb := false;
 Assignfile(tfile, ff);
 Reset(tfile);
 eundostr := '';
 while not eof(tfile) do
 begin
  readln(tfile, s);
  If s = 'undo' then unb := true Else
  if s = 'ends' then eeb := true Else
  if pos('=', s) > 0 then
  begin
   p := pos('=', s);
   c := copy(s, p + 1, length(s) - p);
   if eeb and unb then
   begin
    b := Hexval(copy(s, 2, p - 2));
    tadlen := length(copy(s, 2, p - 2)) div 2;
    endsym := true;
    undsym := false;
    eundostr := c;
    if not tt then table1[tn, b] := s[1] Else table2[tn, b] := s[1];
   end Else
   begin
    b := Hexval(copy(s, 1, p - 1));
    tadlen := length(copy(s, 1, p - 1)) div 2;
    endsym := false;
    undsym := false;
    if unb then
    begin
     undsym := true;
     unb := false;
    end;
    if not tt then table1[tn, b] := c else table2[tn, b] := c;
   end;
  end else if (length(s) >= 3) and (pos('=', s) <= 0) then
  case s[1] of
   '~':
   begin
    ps := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ps] := s[1] Else table2[tn, ps] := s[1];
   end;
   '^':
   begin
    es := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, es] := s[1] Else table2[tn, es] := s[1];
   end;
   '*':
   begin
    ed := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ed] := s[1] Else table2[tn, ed] := s[1];
   end;
   '/':
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := true;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
   '\':
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := eeb;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
   Else If eeb then
   begin
    ee := hexval(copy(s, 2, length(s) - 1));
    tadlen := length(copy(s, 2, length(s) - 1)) div 2;
    endsym := true;
    undsym := false;
    if not tt then table1[tn, ee] := s[1] Else table2[tn, ee] := s[1];
   end;
  end;
 end;
 closefile(tfile);
end;

procedure TKruptarForm.newtransClick(Sender: TObject);
begin
 if created and not saved then
 begin
  Case messagedlg('������� ������� �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
   mrCancel: Exit;
   mrYes: savetransclick(sender);
  end;
 end;
 startoptionsf.showmodal;
 if fok then
 begin
  savename := 'Noname.rnm';
  caption := Application.Title + '- Noname.rnm*';
  created := true;
  saved := false;
  named := false;
  n1.Enabled := true;
  optrans.Enabled := true;
  savetrans.Enabled := true;
  n2.Enabled := true;
  savetras.Enabled := true;
  instrans.Enabled := true;
  ptta.Enabled := true;
  pttacnt.Enabled := true;
  addptbtn.Enabled := true;
  slist.Items.Clear;
  bmlist.Clear;
  memo.Clear;
  memo.Enabled := false;
  rusmemo.Clear;
  rusmemo.Enabled := false;
  pttblbox.Clear;
  pttblbox.Enabled := false;
  rpos.Text := '';
  rpos.Enabled := false;
  eee := false;
  Otables.Done;
  Otables.Init;
  loadrom;
  if not xNoTables then Otables.Add(tab1, tab2);
{  loadtables;}
  posso.init;
  possor.init;
  Case xKodir of
   kdANSI:
   begin
   with KruptarForm.slist.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
   with KruptarForm.memo.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
   with KruptarForm.rusmemo.Font do
   begin
    Name := 'Fixedsys';
    Charset := DEFAULT_CHARSET;
    height := -13;
    size := 10;
    Style := [];
   end;
  end;
  kdSJIS:
  begin
   with KruptarForm.slist.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -16;
    size := 12;
    Style := [];
   end;
   with KruptarForm.memo.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -16;
    size := 12;
    Style := [];
   end;
   with KruptarForm.rusmemo.Font do
   begin
    Name := 'MS Gothic';
    Charset := SHIFTJIS_CHARSET;
    height := -16;
    size := 12;
    Style := [];
   end;
  end;
  end;
  slist.ItemHeight := Abs(slist.Font.Height) + 2;
  if hexeditform.Visible then
  begin
   hexeditform.FormHide(sender);
   hexeditform.FormShow(sender);
  end;
 end;
end;

var iniii, activeb: boolean;

procedure TKruptarForm.FormCreate(Sender: TObject);
var s: string; f: file of tconfigdata; fd: TConfigData;
begin
 if activeb then exit;
 activeb := true;
 s := paramstr(0);
 exedir := '';
 while pos('\', s) > 0 do
 begin
  exedir := exedir + copy(s, 1, pos('\', s));
  Delete(s, 1, pos('\', s));
 end;
 AssignFile(f, exedir + 'kruptar.cfg');
 {$I-}
 reset(f);
 {$I+}
 If ioresult = 0 then
 begin
  Read(f, fd);
  Left := fd.fleft;
  Top := fd.ftop;
  Width := fd.fwidth;
  Height := fd.fheight;
  QuickInsert.Checked := fd.quickins;
  CloseFile(f);
 end;

 Otables.Init;
 FindReplaceForm.PageControl.ActivePage := FindReplaceForm.TabSheet;
 caption := Application.Title;
 rsz := 0;
 new(emptys);
 if paramstr(1) <> '' then
 begin
  bububu := true;
  if pos('\', paramstr(1)) <= 0 then
  begin
   getdir(0, s);
   if s[length(s)] <> '\' then s := s + '\';
  end Else s := '';
  savename := s + paramstr(1);
  iniii := true;
  optransClick(Sender);
 end;
end;

procedure TKruptarForm.savetransClick(Sender: TObject);

var savef, savef1: textfile; s, ss: string; n: pposs; nn: pttable; i: integer;
tOn, tEn: pctables; fontf: file of tfd; fd: tfd;
begin
 if (not named and SaveDialog.Execute) or named then
 begin
  if not named then savename := SaveDialog.FileName;
  s := savename;
  delete(s, pos('.rnm', s), 4);
  assignfile(fontf, s + '.fnf');
  rewrite(fontf);
  fd[0].Charset := memo.Font.Charset;
  fd[0].Handle := 0;
  fd[0].Height := memo.Font.Height;
  fd[0].Pitch := memo.Font.Pitch;
  fd[0].Style := memo.Font.Style;
  fd[0].Name := memo.Font.Name;

  fd[1].Charset := rusmemo.Font.Charset;
  fd[1].Handle := 0;
  fd[1].Height := rusmemo.Font.Height;
  fd[1].Pitch := rusmemo.Font.Pitch;
  fd[1].Style := rusmemo.Font.Style;
  fd[1].Name := rusmemo.Font.Name;

  fd[2].Charset := slist.Font.Charset;
  fd[2].Handle := 0;
  fd[2].Height := slist.Font.Height;
  fd[2].Pitch := slist.Font.Pitch;
  fd[2].Style := slist.Font.Style;
  fd[2].Name := slist.Font.Name;
  write(fontf, fd);
  closefile(fontf);
  assignfile(savef, s + '.bmk');
  rewrite(savef);
  for i := 1 to bmlist.Items.Count do
   writeln(savef, bmlist.items.strings[i - 1]);
  closefile(savef);

  assignfile(savef, savename);
  rewrite(savef);
  if xKodir = kdSJIS then writeln(savef, 'jap');
  if xNoTables then writeln(savef, 'xnotables');
  writeln(savef, orom);
  writeln(savef, nrom);
  tOn := Otables.rootO;
  tEn := Otables.rootE;
  If not xNoTables then While tOn <> nil do
  begin
   if tOn = Otables.rootO then
   begin
    writeln(savef, tOn^.fname);
    writeln(savef, tEn^.fname);
    if tOn^.next <> nil then
    begin
     writeln(savef, '->MORE<-');
     writeln(savef, inttostr(Otables.Count - 1));
    end;
   end Else
   begin
    writeln(savef, tOn^.fname);
    writeln(savef, tEn^.fname);
   end;
   tOn := tOn^.next;
   tEn := tEn^.next;
  end;
{  writeln(savef, tab1);
  writeln(savef, tab2);}

  if xNoPtr then
  writeln(savef, Inttostr(0) + ' ' + Inttostr(xStrLen)) Else
  writeln(savef, Inttostr(xPtrSize + 4 * byte(xpntp)) + ' ' + Inttostr(xKrat) + ' ' +
  Inttostr(Byte(xMethod)) + ' ' + Inttostr(Byte(xSigned) + 2 * Byte(xAutostart)) + ' ' + Inttostr(Byte(xMotorola)));
  If xAutostart then
   writeln(savef, inttohex(0, 8)) Else
  begin
   if longint(xFirstPtr) < 0 then
    writeln(savef, inttostr(xFirstPtr)) Else
    writeln(savef, inttohex(xFirstPtr, 8));
  end;
  writeln(savef, inttostr(emptyc));
  for i := 0 to emptyc - 1 do
  begin
   writeln(savef, inttohex(emptys^[i].a, 8));
   writeln(savef, inttohex(emptys^[i].b, 8));
  end;
  s := savename;
  delete(s, pos('.rnm', s), 4);
  while pos('\', s) > 0 do delete(s, 1, pos('\', s));
  n := possor.root;
  while n <> nil do
  begin
   nn := n^.ptrtable^.root;
   writeln(savef, inttostr(n^.tabn) + ' ' +
   inttohex(n^.ptrtable^.pos, 8) + '=' +
   s + '_' + inttohex(n^.ptrtable^.pos, 8) + '.txt');
   assignfile(savef1, s + '_' + inttohex(n^.ptrtable^.pos, 8) + '.txt');
   rewrite(savef1);
   if xNoPtr and (xStrLen = 0) then
    writeln(savef1, inttostr(n^.ptrtable^.bcnt))
   Else
    writeln(savef1, inttostr(n^.ptrtable^.count));
   while nn <> nil do
   begin
    if xMethod = tmC2S1eSne then
     ss := inttohex(nn^.tptr, 8) + ' ' + inttostr(nn^.same)  + ' ' + inttostr(nn^.sinl)
    Else
     ss := inttohex(nn^.tptr, 8) + ' ' + inttostr(nn^.same);
    writeln(savef1, ss);
    ss := nn^.text;
    if xMethod in [tmL1T, tmL1Te, tmL1TL1T, tmHat] then
     writeln(savef1, ss + '/') Else writeln(savef1, ss);
    writeln(savef1);
    nn := nn^.next;
   end;
   closefile(savef1);
   n := n^.next;
  end;
  writeln(savef, 'mlen');
  writeln(savef, inttostr(maxstrlen));
  closefile(savef);
  named := true;
  saved := true;
  caption := Application.Title + ' - ' + savename;
 end;
end;

procedure TKruptarForm.N1Click(Sender: TObject);
begin
 if xNoPtr then
 showmessage('������������ ROM: ' + orom + #13#10 +
             '���������� ROM: ' + nrom + #13#10 +
             '����� ��������: ' + inttostr(xStrLen))
 Else
 showmessage('������������ ROM: ' + orom + #13#10 +
             '���������� ROM: ' + nrom + #13#10 +
             '������ �������: ' + inttohex(xFirstPtr, 8));
end;                       

procedure TKruptarForm.exitprogClick(Sender: TObject);
begin
 close;
end;

procedure TKruptarForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var f: file of tconfigdata; fd: tconfigdata;
begin
 canclose := true;
 if created and not saved then
 begin
  Case messagedlg('������� ������� �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
   mrCancel:
   begin
    canclose := false;
    exit;
   end;
   mrYes: savetransclick(sender);
  end;
 end;
 SAVED := TRUE;
 CREATED := FALSE;
 savename := '';
 dispose(emptys);
 if rsz > 0 then
 begin
  freemem(rom, rsz + 4);
  posso.clear;
  possor.clear;
 end;
 Otables.Done;
 AssignFile(f, exedir + 'kruptar.cfg');
 rewrite(f);
 fd.fleft := Left;
 fd.ftop := Top;
 fd.fwidth := Width;
 fd.fheight := Height;
 fd.quickins := QuickInsert.Checked;
 Write(f, fd);
 CloseFile(f);
end;

Function HexVal(s: string): LongInt;
Function sign(h: Char): Byte;
begin
 if h in ['0'..'9'] then Result := Byte(h) - 48 Else
 if h in ['A'..'F'] then Result := Byte(h) - 55 Else
                         Result := Byte(h) - 87;
end;
Const hs: Array[1..8] of LongInt = ($1,
                                    $10,
                                    $100,
                                    $1000,
                                    $10000,
                                    $100000,
                                    $1000000,
                                    $10000000);
Var i: Byte; Label Error;
begin
 hexerror := true;
 Result := 0;
 If Length(s) <= 8 then
 begin
  For i := 1 to Length(s) do If not (s[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then Goto Error;
  Result := 0;
  for i := 1 to Length(s) do Inc(Result, sign(s[i]) * hs[length(s) + 1 - i]);
  hexerror := false;
 end Else
 begin Error:
  hexerror := true;
 end;
end;

procedure TKruptarForm.addptbtnClick(Sender: TObject);
var a, b: integer;
begin
 if pos(inttohex(hexval(ptta.Text), 8), pttblbox.Items.GetText) > 0 then
 begin
  Showmessage('������� � ����� ���������� ��� ����!');
  Exit;
 end;
 if ptta.Text = '' then
 begin
  Showmessage('�� ������� ������ ��������!');
  Exit;
 end;
 a := hexval(ptta.Text);
 if pttacnt.Text = '' then b := a else b := hexval(pttacnt.Text);
 If b < a then
 begin
  ShowMessage('�������� �������� ������ ��� ���������!');
  Exit;
 end;
 If xNoPtr then
 begin
  If xStrLen = 0 then
   posso.add(0, a, (b + 1) - a) Else
  begin
   if a = b then inc(b, xStrLen - 1);
   posso.add(0, a, ((b + 1) - a) div xStrLen);
  end;
 end Else
 begin
  if a = b then inc(b, xPtrSize - 1);
  posso.add(0, a, ((b + 1) - a) div xPtrSize);
 end;
 possor.addposso;
 if not eee then
 begin
  pttblbox.enabled := true;
  memo.enabled := true;
  rpos.enabled := true;
  rusmemo.enabled := true;
  cntbtn.Enabled := true;
  eee := true;
 end;
 initall;
 slist.Refresh;
 saved := false;
 if named then caption := Application.Title + ' - ' + savename + '*';
 ptta.SetFocus;
end;

constructor Tptrtable.init;
begin
 root := nil;
 cur := nil;
 bcnt := 0;
 count := 0;
 pos := 0;
end;

procedure Tptrtable.add(tp: dword; s: string);
var n, rr: pttable;
begin
 if (cur = nil) or (cur^.tptr <> tp) then
 begin
  new(n);
  n^.tptr := tp;
  n^.text := s;
  n^.same := 0;
  n^.sinl := 0;
  n^.next := nil;
  rr := cur;
  cur := n;
  if rr <> nil then rr^.next := cur;
  if root = nil then root := n;
  inc(count);
 end else inc(cur^.same);
end;

procedure Tptrtable.loadadd(tp: dword; s: string);
var n, rr: pttable;
begin
 new(n);
 n^.tptr := tp;
 n^.text := s;
 n^.same := 0;
 n^.sinl := 0;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
end;

destructor Tptrtable.clear;
var n: pttable;
begin
 while root <> nil do
 begin
  n := root;
  root := n^.next;
  n^.text := '';
  dispose(n);
 end;
end;

constructor tposso.init;
begin
 root := nil;
 cur := nil;
 count := 0;
end;

procedure tposso.add(tn: integer; p, cnt: dword);
type dwd = record a, b, c, d: byte end;
type wd = record a, b: byte end;
var n, rr: pposs; jb, i, j, d: dword; prom, grom: ^tromdata; s, ss: string;
psz: dword; g: dwd; sinll: word; fst, ii, tw, td, pntt: dword; bki: word;
undoo: boolean;
label eloop;
Function GetSymbol(dpj: dword): string;
var uui: dword;
begin
 if xNoTables then
 begin
  if rom^[dpj] = 0 then
  begin
   Result := '';
   endsym := true;
  end Else
  begin
   Result := Char(Rom^[dpj]);
   endsym := false;
  end;
  Exit;
 end;
 tw := rom^[dpj] * $1000000;
 tw := tw + rom^[dpj + 1] * $10000;
 tw := tw + rom^[dpj + 2] * $100;
 tw := tw + rom^[dpj + 3];
 for uui := 3 downto 0 do
 begin
  result := KruptarForm.table1[tn,tw];
  if tadlen <> uui + 1 then result := '';
  if result = '' then tw := tw div $100 else break;
  if uui = 0 then break;
 end;
 td := uui;
end;
label endst;
begin
 sinll := 0;
 new(n);
 new(n^.ptrtable, init);
 n^.ptrtable^.pos := p;
 n^.ptrtable^.bcnt := cnt;
 n^.tabn := tn;
 n^.next := nil;
 prom := Addr(rom^[p]);
 resultform.Resmemo.Clear;
 KruptarForm.pttblbox.Items.Add(inttostr(KruptarForm.pttblbox.Items.Count) + '. ' + Inttohex(p, 8));
 KruptarForm.pttblbox.ItemIndex := KruptarForm.pttblbox.Items.Count - 1;
 d := p;
 if xpntp then pntt := cnt else pntt := 1;
 If not xNoPtr then for i := 0 to cnt - 1 do
 begin
  s := ''; td := 0;
  if not xPntp then fst := i * xPtrSize Else fst := i;
  Case xPtrSize of
   2: if xMotorola then
   begin
    dwd(d).d := 0;
    dwd(d).c := 0;
    dwd(d).b := prom^[fst];
    dwd(d).a := prom^[fst + 1 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := 0;
    dwd(d).d := 0;
   end;
   3: if xMotorola then
   begin
    dwd(d).d := 0;
    dwd(d).c := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).a := prom^[fst + 2 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := prom^[fst + 2 * pntt];
    dwd(d).d := 0;
   end;
   4: if xMotorola then
   begin
    dwd(d).d := prom^[fst];
    dwd(d).c := prom^[fst + 1 * pntt];
    dwd(d).b := prom^[fst + 2 * pntt];
    dwd(d).a := prom^[fst + 3 * pntt];
   end Else
   begin
    dwd(d).a := prom^[fst];
    dwd(d).b := prom^[fst + 1 * pntt];
    dwd(d).c := prom^[fst + 2 * pntt];
    dwd(d).d := prom^[fst + 3 * pntt];
   end;
  end;
  if xSigned and (xPtrSize in [2, 4]) then
  begin
   Case xPtrSize of
    2:
    begin
     longint(d) := smallint(d);
     inc(d, p + i * 2);
    end;
    4:
    begin
     inc(d, p + i * 4);
    end;
   end;
  end Else
  begin
   If xAutoStart then xFirstPtr := n^.ptrtable^.pos;
   if xFirstPtr < 0 then Dec(d, dword(abs(xFirstPtr)))
   Else                  inc(d, dword(abs(xFirstPtr)));
  end;
  Case xMethod of
   tmNormal:
   begin
    j := 0;
    undoo := false;
    repeat
     if d + j < rsz then
     begin
      ss := GetSymbol(d + j);
      if undsym then undoo := true;
      if not xNoTables and (ss = '') then
      begin
       ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
       tadlen := 1;
       KruptarForm.table1[tn,rom^[d + j]] := ss;
       if KruptarForm.table2[tn,rom^[d + j]] = '' then
        KruptarForm.table2[tn,rom^[d + j]] := ss;
      end;
      if endsym and undoo and (eundostr <> '') then
      begin
       s := s + eundostr;
       endsym := false;
      end else s := s + ss;
     end Else
     begin
      s := s + '/';
      break;
     end;
     inc(j, td + 1);
    until endsym{((ss = '/') or (ss = '\'))};
   end; //tmNormal
   tmBkp:
   begin
    jb := 0;
    wd(bki).a := rom^[d];
    wd(bki).b := rom^[d + 1];
    Repeat
     j := bki;
     undoo := false;
     repeat
      if d + j < rsz then
      begin
       ss := GetSymbol(d + j);
       if undsym then undoo := true;
       if not xNoTables and (ss = '') then
       begin
        ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
        tadlen := 1;
        KruptarForm.table1[tn,rom^[d + j]] := ss;
        if KruptarForm.table2[tn,rom^[d + j]] = '' then
         KruptarForm.table2[tn,rom^[d + j]] := ss;
       end;
       if endsym and undoo and (eundostr <> '') then
       begin
        s := s + eundostr;
        endsym := false;
       end else s := s + ss;
      end Else
      begin
       s := s + '/';
       break;
      end;
      inc(j, td + 1);
     until endsym{((ss = '/') or (ss = '\'))};
     Inc(jb, 2);
     wd(bki).a := rom^[d + jb];
     wd(bki).b := rom^[d + jb + 1];
    until bki = $FFFF;
   end; //tmBkp
   tmL1T, tmL1Te:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if undsym then undoo := true;
     if not xNoTables and (ss = '') then
     begin
      ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
      tadlen := 1;
      KruptarForm.table1[tn,rom^[d + j]] := ss;
      if KruptarForm.table2[tn,rom^[d + j]] = '' then
       KruptarForm.table2[tn,rom^[d + j]] := ss;
     end;
     if endsym and undoo and (eundostr <> '') then
     begin
      s := s + eundostr;
      endsym := false;
     end else s := s + ss;
     inc(j, td + 1);
    until j > rom^[d];
   end; //tmL1T(e)
   tmHat:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if undsym then undoo := true;
     if not xNoTables and (ss = '') then
     begin
      ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
      tadlen := 1;
      KruptarForm.table1[tn,rom^[d + j]] := ss;
      if KruptarForm.table2[tn,rom^[d + j]] = '' then
       KruptarForm.table2[tn,rom^[d + j]] := ss;
     end;
     if endsym and undoo and (eundostr <> '') then
     begin
      s := s + eundostr;
      endsym := false;
     end else s := s + ss;
     inc(j, td + 1);
    until j > rom^[d] * 2 + 1;
   end; //tmHat
   tmL1TL1T:
   begin
    j := 1;
    undoo := false;
    repeat
     ss := GetSymbol(d + j);
     if undsym then undoo := true;
     if not xNoTables and (ss = '') then
     begin
      ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
      tadlen := 1;
      KruptarForm.table1[tn,rom^[d + j]] := ss;
      if KruptarForm.table2[tn,rom^[d + j]] = '' then
       KruptarForm.table2[tn,rom^[d + j]] := ss;
     end;
     if endsym and undoo and (eundostr <> '') then
     begin
      s := s + eundostr;
      endsym := false;
     end else s := s + ss;
     inc(j, td + 1);
    until j > rom^[d];
    s := s + '^';
    psz := j; j := 1;
    repeat
     ss := GetSymbol(d + psz + j);
     if not xNoTables and (ss = '') then
     begin
      ss := '[#' + Inttohex(rom^[d + psz + j], 2) + ']';
      tadlen := 1;
      KruptarForm.table1[tn,rom^[d + psz + j]] := ss;
      if KruptarForm.table2[tn,rom^[d + psz + j]] = '' then
       KruptarForm.table2[tn,rom^[d + psz + j]] := ss;
     end;
     s := s + ss;
     inc(j, td + 1);
    until j > rom^[d + psz];
   end;
   tmC2S1eSne:
   begin
    j := 0;
    if (rom^[d + j + 1 * Byte(xMotorola)] > 0) or
    ((rom^[d + j] > 0) and (rom^[d + j + 1] = 0)) then
    begin
     wd(sinll).b := rom^[d + j + 1 * Byte(not xMotorola)];
     wd(sinll).a := rom^[d + j + 1 * Byte(xMotorola)];
     inc(j, 2);
    end Else
    begin
     while rom^[d + j] = 0 do inc(j);
     sinll := j div 2;
    end;
    for ii := 1 to sinll do
    begin
     undoo := false;
     repeat
      if d + j < rsz then
      begin
       ss := GetSymbol(d + j);
       if undsym then undoo := true;
       if not xNoTables and (ss = '') then
       begin
        ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
        tadlen := 1;
        KruptarForm.table1[tn,rom^[d + j]] := ss;
        if KruptarForm.table2[tn,rom^[d + j]] = '' then
         KruptarForm.table2[tn,rom^[d + j]] := ss;
       end;
       if endsym and undoo and (eundostr <> '') then
       begin
        s := s + eundostr;
        endsym := false;
       end else s := s + ss;
      end Else break;
      inc(j, td + 1);
     until endsym{((ss = '/') or (ss = '\'))};
    end;
   end;
  end;
 endst:
  n^.ptrtable^.add(d, s);
  if xMethod = tmC2S1eSne then n^.ptrtable^.cur^.sinl := sinll;
 end Else
 begin
  If xStrLen > 0 then for i := 0 to cnt - 1 do
  begin
   s := ''; j := 0;
   undoo := false;
   repeat
    ss := GetSymbol(d + j);
    if undsym then undoo := true;
    if not xNoTables and (ss = '') then
    begin
     ss := '[#' + Inttohex(rom^[d + j], 2) + ']';
     tadlen := 1;
     KruptarForm.table1[tn,rom^[d + j]] := ss;
     if KruptarForm.table2[tn,rom^[d + j]] = '' then
      KruptarForm.table2[tn,rom^[d + j]] := ss;
    end;
    if endsym and undoo and (eundostr <> '') then
    begin
     s := s + eundostr;
     endsym := false;
    end else s := s + ss;
    inc(j, td + 1);
   until j = xStrLen;
   n^.ptrtable^.add(d, s);
   inc(d, dword(xStrLen));
  end Else
  begin
   i := 0; j := 0;
   repeat
    if j = 0 then
    begin
     tw := prom^[i] * $1000000;
     tw := tw + prom^[i + 1] * $10000;
     tw := tw + prom^[i + 2] * $100;
     tw := tw + prom^[i + 3];
     for td := 3 downto 0 do
     begin
      ss := KruptarForm.table1[tn,tw];
      if tadlen <> td + 1 then ss := '';
      if ss = '' then tw := tw div $100 else break;
      if td = 0 then break;
     end;
     if (endsym{(((ss = '/') or (ss = '\')))} or (ss = '')) and (emptyc = 0) then
     begin
      inc(d);
      inc(i, td + 1);
      goto eloop;
     end Else j := 1;
    end;
    ss := GetSymbol(d + j - 1);
    if (not xNoTables and (ss = '')) and (emptyc > 0) then
    begin
     ss := '[#' + Inttohex(rom^[d + j - 1], 2) + ']';
     tadlen := 1;
     KruptarForm.table1[tn,rom^[d + j - 1]] := ss;
     if KruptarForm.table2[tn,rom^[d + j - 1]] = '' then
      KruptarForm.table2[tn,rom^[d + j - 1]] := ss;
    end;
    s := s + ss;
    if endsym{(((ss = '/') or (ss = '\')))} or ((ss = '') and (emptyc = 0)) then
    begin
     n^.ptrtable^.add(d, s);
     inc(d, j + td);
     j := 0;
     s := '';
    end Else inc(j, td + 1);
    inc(i, td + 1);
    eloop:
   until i >= cnt;
  end;
 end;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
 if resultform.Resmemo.Lines.Count > 0 then
 begin
  resultform.BorderIcons := [biSystemMenu];
  resultform.show;
 end;
end;

procedure tposso.adde(tn: integer; p: dword; pp: pptrtable);
var i, d: dword; prom: ^tromdata; s,ss: string;
var n, rr: pposs; tt: pttable; tw, td: dword;
label eloop;
begin
 new(n);
 new(n^.ptrtable, init);
 n^.ptrtable^.pos := p;
 n^.ptrtable^.bcnt := pp^.bcnt;
 n^.tabn := tn;
 n^.next := nil;
 KruptarForm.pttblbox.Items.Add(inttostr(KruptarForm.pttblbox.Items.Count) + '. ' + Inttohex(p, 8));
 KruptarForm.pttblbox.ItemIndex := KruptarForm.pttblbox.Items.Count - 1;
 tt := pp^.root;
 resultform.Resmemo.Lines.Clear;
 while tt <> nil do
 begin
  d := tt^.tptr;
  prom := Addr(rom^[d]);
  i := 0; s := '';
  repeat
   tw := prom^[i] * $1000000;
   tw := tw + prom^[i + 1] * $10000;
   tw := tw + prom^[i + 2] * $100;
   tw := tw + prom^[i + 3];
   for td := 3 downto 0 do
   begin
    ss := KruptarForm.table1[tn,tw];
    if tadlen <> td + 1 then ss := '';
    if ss = '' then tw := tw div $100 else break;
    if td = 0 then break;
   end;
   if ss = '' then
   begin
{    ss := '[#' + Inttohex(prom^[i], 2) + ']';
    tadlen := 1;
    KruptarForm.table1[prom^[i]] := ss;
    if KruptarForm.table2[prom^[i]] = '' then
     KruptarForm.table2[prom^[i]] := ss;     }
   end;
   s := s + ss;
   inc(i, td + 1);
  until endsym{(((ss = '/') or (ss = '\')))} or (ss = '') or (d + i >= rsz);
  n^.ptrtable^.add(d, s);
  tt := tt^.next;
 end;
 n^.next := nil;
 rr := cur;
 cur := n;
 if rr <> nil then rr^.next := cur;
 if root = nil then root := n;
 inc(count);
 if resultform.Resmemo.Lines.Count > 0 then
 begin
  resultform.BorderIcons := [biSystemMenu];
  resultform.show;
 end;
end;

destructor tposso.clear;
var n: pposs;
begin
 while root <> nil do
 begin
  n := root;
  root := n^.next;
  dispose(n^.ptrtable, clear);
  dispose(n);
 end;
end;

procedure TKruptarForm.pttaKeyPress(Sender: TObject; var Key: Char);
begin
 key := upcase(key);
 if key = #13 then
 begin
  key := #0;
  Case tedit(sender).Tag of
   0: pttacnt.SetFocus;
   1: addptbtn.SetFocus;
  end;
 end;
 if not (key  in ['0'..'9', 'A'..'F', #8]) then key := #0;
end;

function gettable(i: integer): pptrtable;
var n: pposs; j: integer;
begin
 j := 0;
 n := posso.root;
 result := nil;
 while j < i do
 begin
  n := n^.next;
  if n = nil then exit;
  inc(j);
 end;
 result := n^.ptrtable;
end;

function getrustable(i: integer): pptrtable;
var n: pposs; j: integer;
begin
 j := 0;
 n := possor.root;
 result := nil;
 while j < i do
 begin
  n := n^.next;
  if n = nil then exit;
  inc(j);
 end;
 result := n^.ptrtable;
end;

var initallb: boolean;

procedure TKruptarForm.initall;
var {t: pptrtable;} n: pttable; pp: pposs; i, j: integer;
function _is: string;
begin
 result := inttostr(i);
 while length(result) < 3 do result := '0' + result;
 result := result + '-';
end;
begin
 addd := true;
 slist.Items.Clear;
 initallb := true;
 pp := possor.root;
 i := 0; j := 0;
 while pp <> nil do
 begin
  n := pp^.ptrtable^.root;
  if i = pttblbox.ItemIndex then j := slist.items.Count;
  while n <> nil do
  begin
   Case xKodir of
    kdANSI: slist.Items.Add(_is + inttohex(n^.tptr, 8) + ' -> ' + n^.text);
    kdSJIS: slist.Items.Add(_is + inttohex(n^.tptr, 8) + ' -> ' + unitext(n^.text))
   end;
   n := n^.next;
  end;
  inc(i);
  pp := pp^.next;
 end;
 slist.ItemIndex := j;
 slistclick(slist);
 initallb := false;
 rusmemo.SelStart := 0;
 rusmemo.sellength := 0;
 addd := false;
end;

procedure TKruptarForm.pttblboxClick(Sender: TObject);
begin
 initall;
 slist.Refresh;
end;
{var  n: pttable; pp: pposs; j, i: integer;
begin
 pp := possor.root;
 i := 0; j := 0;
 while pp <> nil do
 begin
  n := pp^.ptrtable^.root;
  if strtoint(copy(slist.Items.Strings[i], 1, 3)) = pttblbox.ItemIndex then
   break;
  while n <> nil do
  begin
   inc(I);
   n := n^.next;
  end;
  inc(j);
  pp := pp^.next;
 end;
 slist.ItemIndex := j;
 slistclick(slist);
 rusmemo.SelStart := 0;
 rusmemo.sellength := 0;
end;}


constructor tpossor.init;
begin
 root := nil;
 cur := nil;
 count := 0;
end;

procedure tpossor.addposso;
var n, nn, rr: pposs; nt: pttable; i: integer;
begin
 n := posso.root;
 if root <> nil then for i := 1 to count do n := n^.next;
 while n <> nil do
 begin
  nt := n^.ptrtable^.root;
  new(nn);
  nn^.next := nil;
  nn^.tabn := n^.tabn;
  new(nn^.ptrtable, init);
  nn^.next := nil;
  while nt <> nil do
  begin
   nn^.ptrtable^.pos := n^.ptrtable^.pos;
   nn^.ptrtable^.bcnt := n^.ptrtable^.bcnt;
   nn^.ptrtable^.add(nt^.tptr, nt^.text);
   nn^.ptrtable^.cur^.same := nt^.same;
   nn^.ptrtable^.cur^.sinl := nt^.sinl;
   nt := nt^.next;
  end;
  rr := cur;
  cur := nn;
  if rr <> nil then rr^.next := nn;
  if root = nil then root := nn;
  n := n^.next;
  inc(count);
 end;
end;

destructor tpossor.clear;
var n: pposs;
begin
 while root <> nil do
 begin
  n := root;
  root := n^.next;
  dispose(n^.ptrtable, clear);
  dispose(n);
 end;
end;

type tcond = (cnHex, cnStr);

procedure TKruptarForm.optransClick(Sender: TObject);
Procedure NotFoundMes(s: string);
begin
 ShowMessage('���� "' + s + '" �� ������!' + #13#10 +
            '��������� ������!');
end;
var sf, tf: textfile; sff, s, ss: string; dd, d, sd: dword; cond: tcond; i, si: integer;
n, rr: pposs; sinll: word; tnum: integer; fontf: file of tfd; fd: tfd; fns: string;
label errore, bu;
begin
 if bububu then goto bu;
 if created and not saved then
 begin
  Case messagedlg('������� ������� �� ��������. ���������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
   mrCancel: Exit;
   mrYes: savetransclick(sender);
  end;
 end;
 if opendialog.Execute then
 begin
  savename := opendialog.FileName;
  bu:
  bububu := false;
  s := savename;
  delete(s, pos('.rnm', s), 4);
  fns := s + '.fnf';
  assignfile(sf, s + '.bmk');
  bmlist.Clear;
  {$I-}
  reset(sf);
  {$I+}
  if ioresult = 0 then
  begin
   while not eof(sf) do
   begin
    readln(sf, s);
    bmlist.Items.Add(s);
   end;
   if bmlist.Items.Count > 0 then bmlist.ItemIndex := 0;
   closefile(sf);
  end;

  assignfile(sf, savename);
  reset(sf);
{  if ioresult <> 0 then
  begin
   ShowMessage
   StartOptionsF.OpenDialog.Title := '������������ ROM';
   if StartOptionsF.OpenDialog.Execute then
   begin

   end Else Exit;
   StartOptionsF.OpenDialog.Title := '���������� ROM';
   if StartOptionsF.OpenDialog.Execute then
   begin

   end Else Exit;
   StartOptionsF.OpenDialog.Title := '';
  end;
  savename := opendialog.FileName;}
  readln(sf, orom);
  if UpperCase(orom) = 'JAP' then
  begin
   xKodir := kdSJIS;
   readln(sf, orom);
  end Else xKodir := kdANSI;
  if UpperCase(orom) = 'XNOTABLES' then
  begin
   xNoTables := true;
   readln(sf, orom);
  end Else xNoTables := false;
  if not FileExists(orom) then
  begin
   NotFoundMes(orom);
   StartOptionsF.opendialog.FileName := orom;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
   if StartOptionsF.OpenDialog.Execute then
    orom := StartOptionsF.OpenDialog.FileName Else Exit;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
  end;
  readln(sf, nrom);
  if not FileExists(nrom) then
  begin
   NotFoundMes(nrom);
   StartOptionsF.opendialog.FileName := nrom;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
   if StartOptionsF.OpenDialog.Execute then
    nrom := StartOptionsF.OpenDialog.FileName Else Exit;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
  end;
  if not xNoTables then
  begin
  readln(sf, tab1);
  StartOptionsF.opendialog.DefaultExt := '.tbl';
  sff := StartOptionsF.opendialog.Filter;
  StartOptionsF.opendialog.Filter := '�������|*.tbl';
  if not FileExists(tab1) then
  begin
   NotFoundMes(tab1);
   StartOptionsF.opendialog.FileName := tab1;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
   if StartOptionsF.OpenDialog.Execute then
    tab1 := StartOptionsF.OpenDialog.FileName Else Exit;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
  end;
  readln(sf, tab2);
  if not FileExists(tab2) then
  begin
   NotFoundMes(tab2);
   StartOptionsF.opendialog.FileName := tab2;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
   if StartOptionsF.OpenDialog.Execute then
    tab2 := StartOptionsF.OpenDialog.FileName Else Exit;
   StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
  end;
  Otables.Done;
  OTables.Init;
  Otables.Add(tab1, tab2);
  end;
  readln(sf, s);
  if not xnotables then
  begin
  if UpperCase(s) = '->MORE<-' then
  begin
   readln(sf, s);
   si := strtoint(s);
   for i := 1 to si do
   begin
    readln(sf, tab1);
    if not FileExists(tab1) then
    begin
     NotFoundMes(tab1);
     StartOptionsF.opendialog.FileName := tab1;
     StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
     if StartOptionsF.OpenDialog.Execute then
      tab1 := StartOptionsF.OpenDialog.FileName Else Exit;
     StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
    end;
    readln(sf, tab2);
    if not FileExists(tab2) then
    begin
     NotFoundMes(tab2);
     StartOptionsF.opendialog.FileName := tab2;
     StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofNoChangeDir,ofEnableSizing];
     if StartOptionsF.OpenDialog.Execute then
      tab2 := StartOptionsF.OpenDialog.FileName Else Exit;
     StartOptionsF.OpenDialog.Options := [ofHideReadOnly,ofEnableSizing];
    end;
    Otables.Add(tab1, tab2);
   end;
   readln(sf, s);
  end;
  end;
  StartOptionsF.opendialog.DefaultExt := '';
  StartOptionsF.opendialog.Filter := sff;
  StartOptionsF.opendialog.FileName := '';
  If pos(' ', s) = 0 then
  begin
   for i := 0 to ptcount do if uppercase(s) = uppercase(pnames[tptrtype(i)]) then
   begin
    ptrtype := tptrtype(i);
    break;
   end;
   readln(sf, s);
   if s <> '' then
   begin
    if s[1] <> '-' then
     oofs := hexval(s)
    Else
     oofs := strtoint(s);
    if hexerror then
    begin
    errore:
     showmessage('�������� ������ �����!');
     closefile(sf);
     Exit;
    end;
   end Else oofs := 0;
   xFirstPtr := oofs;
   xNoPtr := false;
   xKrat := 1;
   xMethod := tmNormal;
   xPntp := false;
   Case ptrtype of
    ptNo:
    begin
     xNoPtr := True;
     xStrLen := oofs;
    end;
    ptNo2:
    begin
     xNoPtr := True;
     xStrLen := 0;
    end;
    pt2bNES:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1T;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen2:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1Te;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bNESlen3:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmL1TL1T;
     xSigned := False;
     xMotorola := False;
    end;
    pt2bSMD:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt2bSigned:
    begin
     xGBA := false;
     xPtrSize := 2;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := True;
     xMotorola := True;
    end;
    pt3bSNES:
    begin
     xGBA := false;
     xPtrSize := 3;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt4bGBA:
    begin
     xGBA := True;
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := False;
    end;
    pt4bSMD:
    begin
     xGBA := false;
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt4bSMDbt2:
    begin
     xGBA := false;
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmNormal;
     xSigned := False;
     xMotorola := True;
    end;
    pt4bSMDpg:
    begin
     xGBA := false;
     xPtrSize := 4;
     xKrat := 1;
     xMethod := tmC2S1eSne;
     xSigned := False;
     xMotorola := True;
    end;
   end;
  end Else
  begin
   ss := copy(s, 1, pos(' ', s) - 1);
   delete(s, 1, pos(' ', s));
   xPtrSize := strtoint(ss);
   if xPtrSize = 0 then
   begin
    xNoPtr := True;
    xStrLen := strtoint(s);
    xKrat := 1;
    xMethod := tmNormal;
   end Else
   begin
    if xPtrSize > 4 then
    begin
     Dec(xPtrSize, 4);
     xPntp := True;
    end Else xPntp := false;
    ss := copy(s, 1, pos(' ', s) - 1);
    delete(s, 1, pos(' ', s));
    xKrat := strtoint(ss);
    ss := copy(s, 1, pos(' ', s) - 1);
    delete(s, 1, pos(' ', s));
    byte(xMethod) := strtoint(ss);
    ss := copy(s, 1, pos(' ', s) - 1);
    delete(s, 1, pos(' ', s));
    d := strtoint(ss);
    If d > 1 then
    begin
     dec(d, 2);
     xAutoStart := True;
    end Else xAutoStart := False;
    xSigned := boolean(d);
    ss := s;
    xMotorola := boolean(strtoint(ss));
   end;
   xGBA := false;
   readln(sf, s);
   if s <> '' then
   begin
    if s[1] <> '-' then
     oofs := hexval(s)
    Else
     oofs := strtoint(s);
    if hexerror then
    begin
     showmessage('�������� ������ �����!');
     closefile(sf);
     Exit;
    end;
   end Else oofs := 0;
   xFirstPtr := oofs;
  end;
  readln(sf, s);
  emptyc := strtoint(s);
  for i := 0 to emptyc - 1 do
  begin
   readln(sf, s);
   emptys^[i].a := hexval(s);
   readln(sf, s);
   emptys^[i].b := hexval(s);
  end;
  caption := Application.Title + ' - ' + savename;
  created := true; saved := true; named := true;
  n1.Enabled := true;
  optrans.Enabled := true;
  savetrans.Enabled := true;
  n2.Enabled := true;
  savetras.Enabled := true;
  instrans.Enabled := true;
  ptta.Enabled := true;
  slist.items.Clear;
  pttacnt.Enabled := true;
  addptbtn.Enabled := true;
  memo.Clear;
  rusmemo.Clear;
  memo.Enabled := false;
  rusmemo.Clear;
  rusmemo.Enabled := false;
  pttblbox.Clear;
  pttblbox.Enabled := false;
  rpos.Text := '';
  rpos.Enabled := false;
  eee := false;
  loadrom;
{  loadtables;}
  posso.init;
  possor.init;
  while not eof(sf) do
  begin
   readln(sf, s);
   if pos('=', s) > 0 then
   begin
    d := hexval(copy(s, 1, 8));
    if hexerror then
    begin
     tnum := strtoint(copy(s, 1, pos(' ', s) - 1));
     delete(s, 1, pos(' ', s));
     d := hexval(copy(s, 1, 8));
    end Else tnum := 0;
    assignfile(tf, copy(s, 10, length(s) - 9));
    reset(tf);
    cond := cnHex;
    readln(tf, s);
    i := strtoint(s);
    new(n);
    new(n^.ptrtable, init);
    n^.ptrtable^.pos := d;
    n^.tabn := tnum;
    n^.next := nil;
    dd := 0; sd := 0; si := 0;
    sinll := 0;
    maxstrlen := 0;
    while not eof(tf) do
    begin
     readln(tf, s);
     case cond of
      cnHex:
      begin
       dd := hexval(copy(s, 1, pos(' ', s) - 1));
       delete(s, 1, pos(' ', s));
       if xMethod = tmC2S1eSne then
       begin
        sd := strtoint(copy(s, 1, pos(' ', s) - 1));
        delete(s, 1, pos(' ', s));
        sinll := strtoint(s);
        inc(si, sd);
        ss := '';
        cond := cnStr;
       end Else
       begin
        sd := strtoint(s);
        inc(si, sd);
        ss := '';
        cond := cnStr;
       end;
      end;
      cnStr:
      begin
       ss := ss + s;
       case xPtrSize of
        0:
        begin
         readln(tf, s);
         n^.ptrtable^.loadadd(dd, ss);
         n^.ptrtable^.cur^.same := sd;
         n^.ptrtable^.cur^.sinl := sinll;
         n^.ptrtable^.bcnt := i;
         cond := cnHex;
        end;
        Else{ if (ss[length(ss)] in ['/', '\']) then}
        begin
         if (xMethod in [tmL1T, tmL1Te, tmL1TL1T, tmHat]) and
            (ss[length(ss)] = '/') then setlength(ss, length(ss) - 1);
         readln(tf, s);
         n^.ptrtable^.loadadd(dd, ss);
         n^.ptrtable^.cur^.same := sd;
         n^.ptrtable^.cur^.sinl := sinll;
         cond := cnHex;
        end;
       end;
      end;
     end;
    end;
    if xNoPtr and (xStrLen = 0) and (emptyc = 0) then
     posso.adde(tnum, d, n^.ptrtable)
    Else posso.add(tnum, d, i + si);
    rr := possor.cur;
    possor.cur := n;
    if rr <> nil then rr^.next := n;
    if possor.root = nil then possor.root := n;
    inc(possor.count);
    closefile(tf);
   end Else if UpperCase(s) = 'MLEN' then
   begin
    readln(sf, s);
    maxstrlen := strtoint(s);
   end;
  end;
  closefile(sf);
  AssignFile(fontf, fns);
  {$I-}
  reset(fontf);
  {$I+}
  If IOresult = 0 then
  begin
   read(fontf, fd);
   memo.font.Height  := fd[0].Height;
   memo.font.Pitch   := fd[0].Pitch;
   memo.font.Style   := fd[0].Style;
   memo.font.Charset := fd[0].Charset;
   memo.font.Name    := fd[0].Name;
   rusmemo.font.Height  := fd[1].Height;
   rusmemo.font.Pitch   := fd[1].Pitch;
   rusmemo.font.Style   := fd[1].Style;
   rusmemo.font.Charset := fd[1].Charset;
   rusmemo.font.Name    := fd[1].Name;
   slist.font.Height  := fd[2].Height;
   slist.font.Pitch   := fd[2].Pitch;
   slist.font.Style   := fd[2].Style;
   slist.font.Charset := fd[2].Charset;
   slist.font.Name    := fd[2].Name;
   CloseFile(fontf);
  end Else Case xKodir of
   kdANSI:
   begin
    with KruptarForm.slist.Font do
    begin
     Name := 'Fixedsys';
     Charset := DEFAULT_CHARSET;
     height := -13;
     size := 10;
     Style := [];
    end;
    with KruptarForm.memo.Font do
    begin
     Name := 'Fixedsys';
     Charset := DEFAULT_CHARSET;
     height := -13;
     size := 10;
     Style := [];
    end;
    with KruptarForm.rusmemo.Font do
    begin
     Name := 'Fixedsys';
     Charset := DEFAULT_CHARSET;
     height := -13;
     size := 10;
     Style := [];
    end;
   end;
   kdSJIS:
   begin
    with KruptarForm.slist.Font do
    begin
     Name := 'MS Gothic';
     Charset := SHIFTJIS_CHARSET;
     height := -16;
     size := 12;
     Style := [];
    end;
    with KruptarForm.memo.Font do
    begin
     Name := 'MS Gothic';
     Charset := SHIFTJIS_CHARSET;
     height := -16;
     size := 12;
     Style := [];
    end;
    with KruptarForm.rusmemo.Font do
    begin
     Name := 'MS Gothic';
     Charset := SHIFTJIS_CHARSET;
     height := -16;
     size := 12;
     Style := [];
    end;
   end;
  end;
  slist.ItemHeight := Abs(slist.Font.Height) + 2;
  if posso.root <> nil then
  begin
   pttblbox.ItemIndex := 0;
   if not eee then
   begin
    pttblbox.enabled := true;
    memo.enabled := true;
    rpos.enabled := true;
    rusmemo.enabled := true;
    cntbtn.Enabled := true;
    eee := true;
   end;
   initall;
   slist.Refresh;
   caption := Application.Title + ' - ' + savename;
   created := true; saved := true; named := true;
   if not iniii and hexeditform.Visible then
   begin
    hexeditform.FormHide(sender);
    hexeditform.FormShow(sender);
   end;
   iniii := false;
  end;
 end;
end;

procedure TKruptarForm.savetrasClick(Sender: TObject);
begin
 saved := false;
 named := false;
 savename := '';
 savetransClick(Sender);
end;

var inbo: boolean;

var kkk: char;

procedure TKruptarForm.rusmemoChange(Sender: TObject);
      function getcurmn: widechar;
     begin
    result := #0;
   if umnenter.Checked then result := '^' Else
  if starenter.Checked then result := '*' Else
 if tildaenter.Checked then result := '~';
end;

var n: pttable; s, ss: widestring; ii, ff: dword; bbb: boolean;
label ohoho;
begin
 if addd then Exit;
 kkk := #0;
 bbb := false;
 addd := true;
  n := curp;
  ss := '';
  if rusmemo.Lines.Count <= 0 then goto ohoho;
  for ii := 0 to rusmemo.Lines.Count - 1 do
  begin
   s := rusmemo.Lines.strings[ii];
   if (maxstrlen > 0) and (length(s) >= maxstrlen) and
  not (char(s[length(s)]) in ['^', '~', '*']) then
   begin
    s := s + getcurmn;
    bbb := true;
   end;
   ss := ss + s;
  end;
  Case xKodir of
   kdANSI: n^.text := ss;
   kdSJIS: n^.text := SJIStext(ss);
  end;
  slist.Items.strings[cui] := copy(slist.Items.strings[cui], 1, 4) +
  inttohex(n^.tptr, 8) + ' -> ' + ss;
  ff := rusmemo.SelStart + 2 * byte(bbb);
  ss := rusmemo.Lines.GetText;
  if bbb or
    (ss[rusmemo.SelStart] = '^') or
    (ss[rusmemo.SelStart] = '*') or
    (ss[rusmemo.SelStart] = '~') then
  begin
   slistclick(slist);
   rusmemo.SelStart := ff;
   rusmemo.SelLength := 0;
  end;

 ohoho:
 if saved then
 begin
  saved := false;
  if named then caption := Application.Title + ' - ' + savename + '*';
 end;
 addd := false;
end;

procedure TKruptarForm.instransClick(Sender: TObject);
var ss: string; n: pposs; nn: pttable; x, d, e, s: dword; o, i, k: integer; j, l: dword;
var lcn: word; a, b: ^tromdata; type kk = record a, b, c, d: byte end;
type wd = record a, b: byte end;
Procedure AddString(tn: Integer; ss: String);
Var sd: String; w: Integer; aa: ^TROMData;
begin
  If xMethod = tmBkp then
  begin
   GetMem(aa, $10000);
   a^[0] := 0;
   a^[1] := 0;
  end Else aa := addr(a^[0]);
  i := 1; k := 0; lcn := 0;
  if xMethod in [tmL1T, tmL1Te, tmL1TL1T, tmHat] then inc(k);
  if xMethod = tmL1Te then ss := ss + '/';
  if xMethod = tmC2S1eSne then inc(k, 2);
  repeat
   if (xMethod = tmL1TL1T) and (ss[i] = '^') then
   begin
    aa^[0] := i - 1;
    aa := addr(a^[k]);
    k := 0;
   end;
   sd := '';
   for w := 0 to chmaxlen[tn] - 1 do
   begin
    if i + w > length(ss) then break;
    sd := sd + ss[i + w];
   end;
   o := 1;
   inc(i, length(sd) + 1);
   repeat
    l := antitable[sd, tn];
    If endsym then inc(lcn);
{    if l = $FFFFFFFF then
    begin
     l := antiutable[sd, tn];
     endsym := false;
    end;          }
    SetLength(sd, length(sd) - 1);
    Dec(i);
    if (l = $FFFFFFFF) and (sd = '') then
    begin
     l := antitable[' ', tn];
     if l = $FFFFFFFF then l := 0;
     break;
    end;
   until l <> $FFFFFFFF;
   if tadlen = 1 then aa^[k] := kk(l).a Else if tadlen = 2 then
   begin
    o := 2;
    aa^[k    ] := kk(l).b;
    aa^[k + 1] := kk(l).a;
   end Else if tadlen = 3 then
   begin
    aa^[k    ] := kk(l).c;
    aa^[k + 1] := kk(l).b;
    aa^[k + 2] := kk(l).a;
    o := 3;
   end Else
   begin
    aa^[k    ] := kk(l).d;
    aa^[k + 1] := kk(l).c;
    aa^[k + 2] := kk(l).b;
    aa^[k + 3] := kk(l).a;
    o := 4;
   end;
   inc(k, o);
   If endsym and (xMethod = tmBkp) and (i <= length(ss)) then
    Word(Addr(a^[lcn * 2])^) := k;
  until i > length(ss);
  if xMethod in [tmL1T, tmL1Te] then
   aa^[0] := k - 1 - byte(xMethod = tmL1Te) else
  if xMethod = tmHat then
   aa^[0] := (k - 2) div 2
  else if xMethod = tmL1TL1T then
  begin
   aa^[0] := k - 1;
  end else if xMethod = tmC2S1eSne then
  begin
   aa^[0] := wd(nn^.sinl).b;
   aa^[1] := wd(nn^.sinl).a;
  end Else If xMethod = tmBkp then
  begin
   For w := 0 to lcn - 1 do
    Inc(Word(Addr(a^[w * 2])^), lcn * 2 + 2);
   Word(Addr(a^[lcn * 2])^) := $FFFF;
   Move(aa^, a^[lcn * 2 + 2], k);
   Freemem(aa, $10000);
  end;
  inscounter := inscounter + 1;
  if doall.act then
  begin
   processform.inslab.Caption := inttostr(inscounter);
   processform.inslab.Refresh;
  end;
end;

type kk2 = record a, b: char end;
var fst, pntt, xcntx: longint;
begin
 assignfile(ffile, nrom);
 {$I-}
 reset(ffile, 1);
 {$I+}
 if ioresult <> 0 then
 begin
  processform.inslab.Caption := '��� �����!';
  processform.closebtn.Enabled := true;
  Exit;
 end;
 impprogr := 1;
 procform.Gauge1.MinValue := 0;
 n := possor.root;
 procform.Gauge1.MaxValue := 0;
 inscounter := 0;
 while n <> nil do
 begin
  procform.Gauge1.MaxValue := procform.Gauge1.MaxValue + n^.ptrtable^.count;
  n := n^.next;
 end;
 resultform.BorderIcons := [];
 resultform.Resmemo.Lines.Clear;
 procform.imptimer.Enabled := true;
 s := filesize(ffile);
 for i := 0 to emptyc - 1 do
  if emptys^[i].b > s then
   s := emptys^[i].b + 1;
 getmem(b, s);
 blockread(ffile, b^, s, d);
 for i := 0 to emptyc - 1 do
  fillchar(b^[emptys^[i].a], emptys^[i].b - emptys^[i].a + 1, 0);
 n := possor.root;
 xcntx := 0;
 If not xNoPtr then while n <> nil do
 begin
  nn := n^.ptrtable^.root;
  j := 0;
  if xpntp then
  begin
   xcntx := 0;
   while nn <> nil do
   begin
    inc(xcntx, 1 + nn^.same);
    nn := nn^.next;
   end;
   nn := n^.ptrtable^.root;
  end;
  while nn <> nil do
  begin
   if not xpntp then fst := j * dword(xPtrSize) else fst := j;
   e := n^.ptrtable^.pos + dword(fst);
   If xAutoStart then xFirstPtr := n^.ptrtable^.pos;
   if xFirstPtr < 0 then d := nn^.tptr + dword(abs(xFirstPtr))
   Else                  d := nn^.tptr - dword(abs(xFirstPtr));
   If xSigned then
   begin
    if xPtrSize = 2 then
    begin
     Dec(d, e);
     longint(d) := smallint(d);
    end Else
    begin
     Dec(d, e);
    end;
   end;
   a := addr(b^[e]);
   inc(j);
   if not xpntp then pntt := 1 else pntt := xcntx;
   Case xPtrSize of
    2: for x := 0 to nn^.same do
    begin
     if not xpntp then fst := x * dword(xPtrSize) else fst := x;
     a^[fst + byte(xMotorola) * pntt] := kk(d).a;
     a^[fst + byte(not xMotorola) * pntt] := kk(d).b;
    end;
    3: for x := 0 to nn^.same do
    begin
     if not xpntp then fst := x * dword(xPtrSize) else fst := x;
     a^[fst + (2 * byte(xMotorola)) * pntt] := kk(d).a;
     a^[fst + 1 * pntt] := kk(d).b;
     a^[fst + (2 - 2 * byte(xMotorola)) * pntt] := kk(d).c;
    end;
    4: for x := 0 to nn^.same do
    begin
     if not xpntp then fst := x * dword(xPtrSize) else fst := x;
     a^[fst + (3 * byte(xMotorola)) * pntt] := kk(d).a;
     a^[fst + (1 + byte(xMotorola)) * pntt] := kk(d).b;
     a^[fst + (2 - byte(xMotorola)) * pntt] := kk(d).c;
     a^[fst + (3 - 3 * byte(xMotorola)) * pntt] := kk(d).d;
    end
   end;
   inc(j, nn^.same);
   ss := nn^.text;
   a := addr(b^[nn^.tptr]);
   if xNoTables then
    move(PChar(nn^.text)^, a^, length(nn^.text) + 1)
   Else
    addstring(n^.tabn, ss);
   inc(impprogr);
   nn := nn^.next;
  end;
  n := n^.next;
 end Else
 begin
  while n <> nil do
  begin
   nn := n^.ptrtable^.root;
   while nn <> nil do
   begin
    ss := nn^.text;
    a := addr(b^[nn^.tptr]);
    addstring(n^.tabn, ss);
    j := 0;
    if xStrLen > 0 then
     for i := k to xStrLen - 1 do
     begin
      l := antitable[' ', n^.tabn];
      if tadlen = 1 then
      begin
       a^[i] := kk(l).a;
       o := 1;
      end Else
      if tadlen = 2 then
      begin
       o := 2;
       a^[k + integer(j)    ] := kk(l).b;
       a^[k + integer(j) + 1] := kk(l).a;
      end Else
      if tadlen = 3 then
      begin
       a^[k + integer(j)    ] := kk(l).c;
       a^[k + integer(j) + 1] := kk(l).b;
       a^[k + integer(j) + 2] := kk(l).a;
       o := 3;
      end Else
      begin
       a^[k + integer(j)    ] := kk(l).d;
       a^[k + integer(j) + 1] := kk(l).c;
       a^[k + integer(j) + 2] := kk(l).b;
       a^[k + integer(j) + 3] := kk(l).a;
       o := 4;
      end;
      inc(j, o);
     end;
    inc(impprogr);
    nn := nn^.next;
   end;
   n := n^.next;
  end;
 end;
 closefile(ffile);
 assignfile(ffile, nrom);
 rewrite(ffile, 1);
 blockwrite(ffile, b^, s);
 closefile(ffile);
 freemem(b, s);
 procform.imptimer.Enabled := false;
 resultform.BorderIcons := [biSystemMenu];
 if sender = instrans then ShowMessage('������!');
 if hexeditform.Visible then
 begin
  hexeditform.FormHide(sender);
  hexeditform.FormShow(sender);
 end;
end;

var memof: boolean;

procedure TKruptarForm.N2Click(Sender: TObject);
begin
 if memo.Focused then memof := true else memof := false;
 FindReplaceForm.PageControl.ActivePageIndex := 0;
 FindReplaceForm.FEdit.SelectAll;
 FindReplaceForm.Showmodal;
 if FindReplaceForm.ModalResult = mrOk then
  FindDialogFind(Sender);
end;

var founded: boolean;

procedure TKruptarForm.FindDialogFind(Sender: TObject);
var s, ss: widestring; k: integer;
var tt, tt2: pposs; n, n2: pttable; j, ii: integer;
label laba, rusmemol;
begin
 founded := true;
 if FindReplaceForm.PageControl.ActivePageIndex = 0 then
  s := FindReplaceForm.FEdit.WideText Else
  s := FindReplaceForm.RFEdit.WideText;
 tt := posso.root;
 tt2 := possor.root;
 j := 0;
 ii := strtoint(copy(slist.Items.Strings[cui], 1, 3));
 while tt <> nil do
 begin
  if j = ii{pttblbox.ItemIndex} then break;
  tt := tt^.next;
  inc(j);
 end;
 j := 0;
 while tt2 <> nil do
 begin
  if j = ii {pttblbox.ItemIndex} then break;
  tt2 := tt2^.next;
  inc(j);
 end;

 k := j;
 if curp <> nil then
 begin
  n := curp2^.next;
  n2 := curp^.next;
  j := cui + 1;
  if {j >= slist.Items.Count} strtoint(copy(slist.Items.Strings[  j], 1, 3)) >
                              strtoint(copy(slist.Items.Strings[cui], 1, 3)) then
  begin
   if tt <> nil then
   begin
    tt := tt^.next;
    tt2 := tt2^.next;
    inc(k);
   end;
  end Else goto laba;
 end;

 while tt <> nil do
 begin
  n := tt^.ptrtable^.root;
  n2 := tt2^.ptrtable^.root;
{  j := 0;}
 laba:
  while n <> nil do
  begin
   if findreplaceform.PageControl.ActivePageIndex = 1 then goto rusmemol;
   Case xKodir of
    kdANSI: ss := n^.text;
    kdSJIS: ss := unitext(n^.text);
   end;
   if pos(s, ss) > 0 then
   begin
    pttblbox.ItemIndex := k;
    initall;
    slist.ItemIndex := j;
    if slist.ItemIndex < j then
     slist.ItemIndex := j + (j - slist.ItemIndex);
    slistclick(slist);
    memo.SetFocus;
    memo.SelStart := pos(s, memo.Lines.GetText) - 1;
    memo.SelLength := length(s);
    exit;
   end Else
   begin
    rusmemol:
    Case xKodir of
     kdANSI: ss := n2^.text;
     kdSJIS: ss := unitext(n2^.text);
    end;
    if pos(s, ss) > 0 then
    begin
     pttblbox.ItemIndex := k;
     initall;
     slist.Refresh;
     slist.ItemIndex := j;
     if slist.ItemIndex < j then
      slist.ItemIndex := j + (j - slist.ItemIndex);
     rusmemo.SetFocus;
     rusmemo.SelStart := pos(s, rusmemo.Lines.GetText) - 1;
     rusmemo.SelLength := length(s);
     exit;
    end
   end;
   inc(j);
   n := n^.next;
   n2 := n2^.next;
  end;
  inc(k);
  tt := tt^.next;
  tt2 := tt2^.next;
 end;
 ShowMessage('������ �� �������!');
 founded := false;
end;

procedure TKruptarForm.N4Click(Sender: TObject);
begin
 if findreplaceform.PageControl.ActivePageIndex = 0 then
 begin
  if FindReplaceForm.FEdit.Text = '' then
  begin
   n2click(sender);
   Exit;
  end;
  FindDialogFind(Sender);
 end Else
 begin
  if FindReplaceForm.RFEdit.Text = '' then
  begin
   n23click(sender);
   Exit;
  end;
  FindDialogFind(Sender);
  if founded then
  begin
   if FindReplaceForm.CheckBox.Checked then
   begin
    if messagedlg('��������?', mtWarning, [mbYes, mbNo], 0) = mrYes then
     rusmemo.SelWideText := FindReplaceForm.REdit.WideText;
   end Else rusmemo.SelWideText := FindReplaceForm.REdit.WideText;
  end;
 end;
end;

procedure TKruptarForm.cntbtnClick(Sender: TObject);
function glength(tn: integer; ss: string): integer;
var sd: string; k, i, w, slcnt: integer; l: dword;
begin
 if xNoTables then
 begin
  result := length(ss) + 1;
  Exit;
 end;
  i := 1; k := 0;
  slcnt := 0;
  repeat
   sd := '';
   for w := 0 to chmaxlen[tn] - 1 do
   begin
    if i + w > length(ss) then break;
    sd := sd + ss[i + w];
   end;
   inc(i, length(sd) + 1);
   repeat
    l := antitable[sd,tn];
    if endsym then Inc(slcnt);
{    if l = $FFFFFFFF then
    begin
     l := antiutable[sd, tn];
     endsym := false;
    end;          }
    SetLength(sd, length(sd) - 1);
    Dec(i);
    if (l = $FFFFFFFF) and (sd = '') then
    begin
     l := antitable[' ',tn];
     if l = $FFFFFFFF then l := 0;
     break;
    end;
   until l <> $FFFFFFFF;
   if l <> $FFFFFFFF then inc(k, tadlen);
  until i > length(ss);
  if xMethod = tmBkp then Inc(k, slcnt * 2 + 2);
  result := k;
end;

Function CopyFind(s: pttable; xx: pptrtable): boolean;
var n: pposs; nt: pttable;
begin
 result := false;
 if xNoPtr then Exit;
 if xAutostart then
 begin
   nt := xx^.root;
   if nt = s then exit;
   while nt <> nil do
   begin
    if nt^.text = s^.text then
    begin
     result := true;
     s^.tptr := nt^.tptr;
     exit;
    end;
    nt := nt^.next;
    if nt = s then Exit;
   end;
 end Else
 begin
  n := possor.root;
  while n <> nil do
  begin
   nt := n^.ptrtable^.root;
   if nt = s then exit;
   while nt <> nil do
   begin
    if nt^.text = s^.text then
    begin
     result := true;
     s^.tptr := nt^.tptr;
     exit;
    end;
    nt := nt^.next;
    if nt = s then Exit;
   end;
   n := n^.next;
  end;
 end;
end;

var n: pposs; nt: pttable; i, j: dword;
var nn: pttable; rm, gl: integer;
function _is: string;
begin
 result := inttostr(gl);
 while length(result) < 3 do result := '0' + result;
 result := result + '-';
end;
label eloop;
begin
 if emptyc = 0 then exit;
 if xNoPtr and (xStrLen > 0) then exit;
 if pttblbox.Items.Count <= 0 then Exit;
 rusmemo.SetFocus;
 n := possor.root;
 procform.Gauge1.MaxValue := 0;
 while n <> nil do
 begin
  procform.Gauge1.MaxValue := procform.Gauge1.MaxValue + n^.ptrtable^.count;
  n := n^.next;
 end;
 n := possor.root;
 i := emptys^[0].a; j := 0;
 countcounter := 0;
 while n <> nil do
 begin
  nt := n^.ptrtable^.root;
  if xAutostart then
  begin
   i := emptys^[j].a;
   inc(j);
  end;
  if xNoPtr and (emptyc > 1) then i := emptys^[j].a;
  while nt <> nil do if not n45.Checked or not CopyFind(nt, n^.ptrtable) then
  begin
   nt^.tptr := i;
   gl := glength(n^.tabn, nt^.text) + byte(xMethod in [tmL1T, tmL1TL1T, tmHat]) + 2 *
         byte(xMethod in [tmC2S1eSne, tmL1Te]);
   inc(i, gl);
   if not xNoPtr or ((emptyc = 0) and xNoPtr) then
    while i mod xkrat > 0 do inc(i);
   if not XAutostart and (not xNoPtr or ((emptyc = 0) and xNoPtr)) and (i > emptys^[j].b + 1) then
   begin
    inc(j);
    if j = dword(emptyc) then
    begin
     i := longint(emptys^[j + 1].b) - gl;
     nt^.tptr := i;
     goto eloop;
    end;
    i := emptys^[j].a;
    nt^.tptr := i;
    Inc(i, glength(n^.tabn, nt^.text) + byte(xMethod in [tmL1T, tmL1TL1T, tmHat]) + 2 *
         byte(xMethod in [tmC2S1eSne, tmL1Te]));
   if not xNoPtr or ((emptyc = 0) and xNoPtr) then
    while i mod xkrat > 0 do inc(i);
   end;
   nt := nt^.next;
   countcounter := countcounter + 1;
   if doall.act then
   begin
    processform.cntlab.Caption := inttostr(countcounter);
    processform.cntlab.Refresh;
   end;
  end Else
  begin
   nt := nt^.next;
   countcounter := countcounter + 1;
   if doall.act then
   begin
    processform.cntlab.Caption := inttostr(countcounter);
    processform.Refresh;
   end;
  end;
  if xNoPtr and (emptyc > 1) then inc(j);
  n := n^.next;
 end;
 eloop:
 addd := true;
 n := possor.root;
 rm := 0; gl := 0;
 while n <> nil do
 begin
  nn := n^.ptrtable^.root;
  while nn <> nil do
  begin
   Case xKodir of
    kdANSI: slist.Items.strings[rm] := _is + inttohex(nn^.tptr, 8) + ' -> ' + nn^.text;
    kdSJIS: slist.Items.strings[rm] := _is + inttohex(nn^.tptr, 8) + ' -> ' + unitext(nn^.text);
   end;
   nn := nn^.next;
   inc(rm);
  end;
  inc(gl);
  n := n^.next;
 end;
 slistclick(sender);
 if saved then
 begin
  saved := false;
  if named then caption := Application.Title + ' - ' + savename + '*';
 end;
 if (sender = cntbtn) or (sender = n5) then showmessage('������!');
end;

procedure TKruptarForm.slistClick(Sender: TObject);
var{ t: pptrtable;} n: pttable; s, ss: widestring; i, j: integer; pp: pposs; b: boolean;
begin
 addd := true;
 j := 0;
 ss := '';
 if slist.ItemIndex < 0 then slist.ItemIndex := slist.TopIndex;
{ ii := strtoint(copy(slist.Items.Strings[slist.ItemIndex], 1, 3));
 t := GetTable(pttblbox.ItemIndex);
 if t = nil then exit;}
 pp := posso.root;
 b := false;
 while pp <> nil do
 begin
  n := pp^.ptrtable^.root;
  while n <> nil do
  begin
   if j = slist.ItemIndex then
   begin
    curp2 := n;
    epos.Text := inttohex(n^.tptr, 8);
    Case xKodir of
     kdANSI: s := n^.text;
     kdSJIS: s := unitext(n^.text);
    end;
    i := 0; j := 1;
    if s <> '' then
    repeat
     repeat
      inc(i);
     until (i = length(s)) or (
     (s[i] = '~') or
     (s[i] = '^') or
     (s[i] = '*') or
     (s[i] = '/') or
     (s[i] = '\'));
     ss := ss + copy(s, j, i - j + 1) + #13#10;
     j := i + 1;
    until i = length(s);
    b := true;    
    break;
   end;
   inc(j);
   n := n^.next;
  end;
  if b then break;
  pp := pp^.next;
 end;
 b := false;
 memo.Lines.SetText(pwidechar(ss));
{ t := GetrusTable(pttblbox.ItemIndex);
 if t = nil then exit;
 n := t^.root;}
 j := 0;
 ss := '';
 pp := possor.root;
 while pp <> nil do
 begin
  n := pp^.ptrtable^.root;
  while n <> nil do
  begin
   if j = slist.ItemIndex then
   begin
    curp := n; cui := j;
    rpos.Text := inttohex(n^.tptr, 8);
    Case xKodir of
     kdANSI: s := n^.text;
     kdSJIS: s := unitext(n^.text);
    end;
    i := 0; j := 1;
    if s = '' then s := '/';
    repeat
     repeat
      inc(i);
     until (i = length(s)) or (
     (s[i] = '~') or
     (s[i] = '^') or
     (s[i] = '*') or
     (s[i] = '/') or
     (s[i] = '\'));
     ss := ss + copy(s, j, i - j + 1) + #13#10;
     j := i + 1;
    until i = length(s);
    b := true;
    break;
   end;
   inc(j);
   n := n^.next;
  end;
  if b then break;
  pp := pp^.next;
 end;
 rusmemo.Lines.SetText(pwidechar(ss));
 addd := false;
end;

procedure TKruptarForm.N5Click(Sender: TObject);
begin
 cntbtnclick(cntbtn);
end;

procedure TKruptarForm.loaditemClick(Sender: TObject);
begin
 n5.Enabled := cntbtn.Enabled;
 n13.Enabled := created and (posso.root <> nil);
 n26.Enabled := created;
 n44.Enabled := created;
 instrans.Enabled := created and (posso.root <> nil);
end;

var ii: word;

procedure TKruptarForm.rusmemoKeyPress(Sender: TObject; var Key: Char);
Procedure Insert(SubStr: WideString; Var Dest: WideString; Index: Integer);
var mm: integer;
begin
 mm := rusmemo.SelStart;
 rusmemo.SelStart := rusmemo.SelStart + (Index - (mm + 1));
 rusmemo.SelWideText := SubStr;
 rusmemo.SelStart := mm;
end;

const lsogl: set of char =
['s', 't', 'w', 'm', 'n', 't', 'r', 'k', 'p', 'h', 'b', 'd', 'z', 'g', 'f', 'y',
 'a', 'i', 'u', 'e', 'o'];
const bsogl: set of char =
['S', 'T', 'W', 'M', 'N', 'T', 'R', 'K', 'P', 'H', 'B', 'D', 'Z', 'G', 'F', 'Y',
 'A', 'I', 'U', 'E', 'O'];
var s: wstring; ss: integer;
begin
 if key in [#13, #10] then
 begin
  if umnenter.Checked then
   key := '^' Else
  if starenter.Checked then
   key := '*' Else
  if tildaenter.Checked then
   key := '~';
 end Else
 if (xKodir = kdSJIS) and n19.Checked then
 begin
  if (japl.caption = '') and (key = ' ') then
  begin
   key := #0;
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(#$3000, s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
  end Else
  if (japl.caption = '') and (key = '-') then
  begin
   key := #0;
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(#$30FC, s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
  end Else
  if (japl.caption = '') and (key = '~') then
  begin
   key := #0;
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(#$FF5E, s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
  end Else
  if (japl.caption = '') and (key = ',') then
  begin
   key := #0;
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(#$3001, s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
  end Else
  if (japl.caption = '') and (key = '.') then
  begin
   key := #0;
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(#$3002, s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
  end Else
  if (japl.caption = '') and (key in ['0'..'9']) then
  begin
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(widechar($FF10 + (byte(key) - byte('0'))), s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
   key := #0;
  end Else
  if (japl.caption = '') and
   (key in ['!','@','#','$','%', '&', '*', '(', ')','_','=']) then
  begin
   s := rusmemo.Lines.GetText;
   ss := rusmemo.SelStart;
   insert(widechar($FF00 + (byte(key) - $20)), s, rusmemo.SelStart + 1);
   rusmemo.SelStart := ss + 1;
   japl.caption := '';
   key := #0;
  end Else
  if (japl.caption = '') and ((key in lsogl) or (key in bsogl)) then
  begin
   japl.caption := lowercase(key);
   ii := $60 * Byte(key in bsogl);
   if ii = $60 then;
   if upcase(Key) in ['A', 'I', 'U', 'E', 'O'] then
   Case upcase(Key) of
    'A':
    begin
     s := rusmemo.Lines.GetText;
     ss := rusmemo.SelStart;
     insert(widechar($3042 + ii), s, rusmemo.SelStart + 1);
     rusmemo.SelStart := ss + 1;
     japl.caption := '';
    end;
    'I':
    begin
     s := rusmemo.Lines.GetText;
     ss := rusmemo.SelStart;
     insert(widechar($3044 + ii), s, rusmemo.SelStart + 1);
     rusmemo.SelStart := ss + 1;
     japl.caption := '';
    end;
    'U':
    begin
     s := rusmemo.Lines.GetText;
     ss := rusmemo.SelStart;
     insert(widechar($3046 + ii), s, rusmemo.SelStart + 1);
     rusmemo.SelStart := ss + 1;
     japl.caption := '';
    end;
    'E':
    begin
     s := rusmemo.Lines.GetText;
     ss := rusmemo.SelStart;
     insert(widechar($3048 + ii), s, rusmemo.SelStart + 1);
     rusmemo.SelStart := ss + 1;
     japl.caption := '';
    end;
    'O':
    begin
     s := rusmemo.Lines.GetText;
     ss := rusmemo.SelStart;
     insert(widechar($304A + ii), s, rusmemo.SelStart + 1);
     rusmemo.SelStart := ss + 1;
     japl.caption := '';
    end;
   end;
   key := #0;
  end Else if japl.caption <> '' then
  Case japl.caption[1] of
   's':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3057 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3055 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3057 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3057 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3059 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3057 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305B + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3057 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305D + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     's':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'z':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3058 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3056 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3058 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3058 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305A + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3058 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305C + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3058 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305E + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'z':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'k':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304D + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($304B + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($304D + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304D + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($304F + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304D + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3051 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304D + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3053 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'k':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'g':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304E + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($304C + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($304E + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304E + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3050 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304E + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3052 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($304E + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3054 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'g':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   't':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3061 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($305F + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3061 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3061 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3064 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3061 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3066 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3061 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3068 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     't':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'n':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($306B + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($306A + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($306B + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($306B + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($306C + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($306B + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($306D + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($306B + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($306E + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'n':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     ' ':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3093 + ii), s, rusmemo.SelStart + 1);
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     Else key := #0;
    end;
   end;

   'd':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3062 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3060 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3062 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3062 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3065 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3062 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3067 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3062 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3069 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'd':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'm':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($307F + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($307E + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($307F + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($307F + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3080 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($307F + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3081 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($307F + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3082 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'm':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'h':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3072 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($306F + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3072 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3072 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3072 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3078 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3072 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($307B + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'h':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'f':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);       
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(sS, 2);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.caption) <> 2 then
      begin
       insert(widechar($3041 + ii), s, rusmemo.SelStart + 2);
       inc(ss);
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.caption) <> 2 then
      begin
       insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
       inc(ss);
      end;

      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss, 2);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.caption) <> 2 then
      begin
       insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
       inc(ss);
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3075 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss, 2);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.caption) <> 2 then
      begin
       insert(widechar($3049 + ii), s, rusmemo.SelStart + 2);
       inc(ss);
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'f':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'b':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3073 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3070 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3073 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3073 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3076 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3073 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3079 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3073 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($307C + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'b':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'p':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3074 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3071 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3074 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3074 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3077 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3074 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($307A + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($3074 + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($307D + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'p':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'r', 'l':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($308A + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($3089 + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'i':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($308A + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($308A + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($308B + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'e':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($308A + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($308C + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      if (length(japl.caption) = 2) and (japl.caption[2] = 'y') then
       insert(widechar($308A + ii), s, rusmemo.SelStart + 1)
      Else
       insert(widechar($308D + ii), s, rusmemo.SelStart + 1);
      if length(japl.caption) = 2 then
      begin
       if japl.caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, rusmemo.SelStart + 2);
        inc(ss);
       end Else if japl.caption[2] = japl.caption[1] then
       begin
        insert(widechar($3063 + ii), s, rusmemo.SelStart + 1);
        inc(ss);
       end
      end;
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + 'y';
      key := #0;
     end;
     'r', 'l':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'w':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($308F + ii), s, rusmemo.SelStart + 1);

      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3092 + ii), s, rusmemo.SelStart + 1);
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     Else key := #0;
    end;
   end;

   'y':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3084 - byte(length(japl.caption) = 2) + ii),
      s, rusmemo.SelStart + 1);
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'u':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3086 - byte(length(japl.caption) = 2) + ii),
      s, rusmemo.SelStart + 1);
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'o':
     begin
      key := #0;
      s := rusmemo.Lines.GetText;
      ss := rusmemo.SelStart;
      insert(widechar($3088 - byte(length(japl.caption) = 2) + ii),
       s, rusmemo.SelStart + 1);
      rusmemo.SelStart := ss + 1;
      japl.caption := '';
     end;
     'y':
     begin
      if length(japl.caption) = 1 then
       japl.caption := japl.caption + japl.caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

  end Else if japl.caption <> '' then key := #0;
 end;
end;

procedure TKruptarForm.Timer1Timer(Sender: TObject);
begin
  if n19.Checked then
   StatusBar.Panels[1].Text := '����: ��������' Else
   StatusBar.Panels[1].Text := '����: �������';
  StatusBar.Panels[0].Text :=
  'x: ' +
   inttostr(memo.CaretPos.x) + '/' +
   inttostr(length(memo.Lines.strings[memo.CaretPos.y])) +
  ' y: ' +
   inttostr(memo.CaretPos.y + 1) + '/' +
   inttostr(memo.Lines.count - 1);
  StatusBar.Panels[3].Text := 'y: ' +
   inttostr(rusmemo.CaretPos.y + 1) + '/' +
   inttostr(rusmemo.Lines.count - 1);
  StatusBar.Panels[2].Text := 'x: ' +
   inttostr(rusmemo.CaretPos.x) + '/' +
   inttostr(length(rusmemo.Lines.strings[rusmemo.CaretPos.y]));
end;

procedure TKruptarForm.addbmbtnClick(Sender: TObject);
var s: string;
begin
 bmss := 'bookmark' + inttostr(bmlist.Items.count + 1);
 bmform.nfield.Text := bmss;
 bmform.showmodal;
 if bmss <> '' then
 begin
  s := ' ' + inttostr(pttblbox.ItemIndex) + ' ' + inttostr(slist.ItemIndex);
  if pos(s, bmlist.Items.GetText) > 0 then
   Showmessage('����� ��� ���� ��������') Else
  bmlist.Items.Add(bmss + s);
  bmlist.ItemIndex := bmlist.Items.Count - 1;
  if saved then
  begin
   saved := false;
   if named then caption := Application.Title + ' - ' + savename + '*';
  end;
 end;
end;

procedure TKruptarForm.delbuttnClick(Sender: TObject);
var i: integer;
begin
 i := bmlist.ItemIndex;
 bmlist.Items.Delete(i);
 bmlist.ItemIndex := i;
 bmlist.text := bmlist.Items.strings[i];
 if saved then
 begin
  saved := false;
  if named then caption := Application.Title + ' - ' + savename + '*';
 end;
end;

procedure TKruptarForm.pbtnClick(Sender: TObject);
var s: string; var a, b: integer;
begin
 s := bmlist.Text;
 if s = '' then exit;
 delete(s, 1, pos(' ', s));
 a := strtoint(copy(s, 1, pos(' ', s) - 1));
 delete(s, 1, pos(' ', s));
 b := strtoint(s);
 pttblbox.ItemIndex := a;
 initall;
 slist.ItemIndex := b;
 slist.ItemIndex := b;
 slist.Refresh;
 slistclick(slist);
end;

Function TKruptarForm.GetATitem(x: string; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.s = x) then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end Else
  If (n^.eundo = x) then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := false;
   undsym := false;
   eundostr := '';
   break;
  end;
  n := n^.next;
 end;
end;

Function TKruptarForm.GetUTitem(x: string; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.eundo = x then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   break;
  end;
  n := n^.next;
 end;
end;


Function TKruptarForm.GetA1Titem(x: string; tabn: integer): dword;
var n: pctable; rc: pctables; i: integer;
begin
 result := $FFFFFFFF;
 endsym := false;
 rc := Otables.rootO;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.s = x then
  begin
   result := n^.p;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end;
  n := n^.next;
 end;
end;

Function TKruptarForm.GetTitem(tabn, x: dword): string;
var n: pctable; rc: pctables; i: integer;
begin
 result := '';
 endsym := false;
 rc := Otables.rootO;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.p = x) then
  begin
   result := n^.s;
   tadlen := n^.tl;
   endsym := n^.endstr;
   undsym := n^.undo;
   eundostr := n^.eundo;
   break;
  end;
  n := n^.next;
 end;
end;

Procedure TKruptarForm.SetTitem(tabn, x: dword; value: string);
var n, rr: pctable; b: boolean; rc: pctables; i: integer;
begin
 rc := Otables.rootO;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 b := true;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.p = x) and (n^.tl = tadlen) then
  begin
   b := false;
   break;
  end;
  n := n^.next;
 end;
 if b then new(n);
 n^.s := value;
 n^.p := x;
 n^.tl := tadlen;
 n^.endstr := endsym;
 n^.undo := undsym;
 n^.eundo := eundostr;
 if n^.tl = 0 then n^.tl := 1;
 if length(value) > rc^.cl then rc^.cl := length(value);
 if b then
 begin
  n^.next := nil;
  rr := rc^.cur;
  rc^.cur := n;
  if rr <> nil then rr^.next := rc^.cur;
  if rc^.tab = nil then rc^.tab := n;
  inc(rc^.count);
 end;
end;

Function TKruptarForm.GetT2item(tabn, x: dword): string;
var n: pctable; rc: pctables; i: integer;
begin
 result := '';
 endsym := false;
 rc := Otables.rootE;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 n := rc^.tab;
 while n <> nil do
 begin
  if n^.p = x then
  begin
   result := n^.s;
   tadlen := n^.tl;
   endsym := n^.endstr;
   eundostr := n^.eundo;
   undsym := n^.undo;
   break;
  end;
  n := n^.next;
 end;
end;

Function TKruptarForm.GetMLen(tabn: integer): integer;
var rc: pctables; i: integer;
begin
 result := 0;
 rc := Otables.rootE;
 if tabn > otables.Count - 1 then exit;
 i := 0;
 While i <> tabn do
 begin
  rc := rc^.next;
  inc(i);
 end;
 result := rc^.cl;
end;

Procedure TKruptarForm.SetT2item(tabn, x: dword; value: string);
var n, rr: pctable; b: boolean; rc: pctables; i: integer;
begin
 rc := Otables.rootE;
 if integer(tabn) > otables.Count - 1 then exit;
 i := 0;
 While i <> integer(tabn) do
 begin
  rc := rc^.next;
  inc(i);
 end;
 b := true;
 n := rc^.tab;
 while n <> nil do
 begin
  if (n^.s = value){ and (n^.tl = tadlen)} then
  begin
   b := false;
   break;
  end;
  n := n^.next;
 end;
 if b then new(n);
 n^.s := value;
 n^.p := x;
 n^.tl := tadlen;
 n^.endstr := endsym;
 n^.undo := undsym;
 n^.eundo := eundostr;
 if n^.tl = 0 then n^.tl := 1;
 if length(value) > rc^.cl then rc^.cl := length(value);
 if b then
 begin
  n^.next := nil;
  rr := rc^.cur;
  rc^.cur := n;
  if rr <> nil then rr^.next := rc^.cur;
  if rc^.tab = nil then rc^.tab := n;
  inc(rc^.count);
 end;
end;

procedure TKruptarForm.N13Click(Sender: TObject);
var n, nr: pposs; nn, nnr: pttable; j: integer;
begin
 if posso.root = nil then Exit;
 j := slist.ItemIndex;
 rusmemo.SetFocus;
 n := posso.root; nr := possor.root;
 while n <> nil do
 begin
  nn := n^.ptrtable^.root;
  nnr := nr^.ptrtable^.root;
  while nn <> nil do
  begin
   nnr^.text := nn^.text;
   nnr^.tptr := nn^.tptr;
   nn := nn^.next;
   nnr := nnr^.next;
  end;
  n := n^.next;
  nr := nr^.next;
 end;
 initall;
 slist.ItemIndex := j;
 slist.ItemIndex := j;
 slist.Refresh;
 slistclick(sender);
 saved := false;
 if named then caption := Application.Title + ' - ' + savename + '*';
end;

procedure TKruptarForm.N15Click(Sender: TObject);
var i: integer;
begin
 if slist.ItemIndex < slist.items.count - 1 then
 begin
  i := slist.ItemIndex; inc(i);
  slist.ItemIndex := i;
  slistclick(sender);
 end;
end;

procedure TKruptarForm.N16Click(Sender: TObject);
var i: integer;
begin
 if slist.Focused then Exit;
 if slist.ItemIndex > 0 then
 begin
  i := slist.ItemIndex; Dec(i);
  slist.ItemIndex := i;
  slistclick(sender);
 end;
end;

procedure TKruptarForm.N17Click(Sender: TObject);
begin
 Hexeditor1.Enabled := created;
 ProgressItem.Enabled := created; 
 n6.Enabled := created;
 n19.Enabled := (xKodir = kdSJIS);
 n22.Enabled := created;
 n20.Enabled := created;
 emulate.Enabled := created;
end;

procedure TKruptarForm.HexEditor1Click(Sender: TObject);
begin
 HexEditForm.Show;
end;

procedure TKruptarForm.slistKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var ne, nr: pposs; cr, ce, tr, te: pttable; j, u: integer; b: boolean;
begin
 b := false;
 if ptrtype <> ptNo2 then exit;
 if (slist.ItemIndex >= 0) and (key = vk_delete) and
  (Messagedlg('�������?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
 begin
  ne :=  posso.root;
  nr := possor.root;
  j := 0;
  while ne <> nil do
  begin
{   if i = pttblbox.ItemIndex then
   begin    }
    te := ne^.ptrtable^.root;
    tr := nr^.ptrtable^.root;
    ce := nil;
    cr := nil;
{    j := 0;}
    while tr <> nil do
    begin
     if j = slist.ItemIndex then
     begin
      u := slist.ItemIndex;
      slist.Items.Delete(j);
      if u >= slist.Items.Count then
       slist.ItemIndex := slist.Items.Count - 1
      Else if slist.Items.Count = 0 then
       slist.ItemIndex := -1
      Else
       slist.ItemIndex := u;
      slistclick(sender);
      saved := false;
      if named then caption := Application.Title + ' - ' + savename + '*';
      dec(ne^.ptrtable^.count);
      dec(nr^.ptrtable^.count);
      if cr <> nil then
      begin
       if tr = nr^.ptrtable^.cur then
       begin
        nr^.ptrtable^.cur := cr;
        ne^.ptrtable^.cur := ce;
       end;
       cr^.next := tr^.next;
       dispose(tr);
       ce^.next := te^.next;
       dispose(te);
      end Else
      begin
       if ne^.ptrtable^.cur = ne^.ptrtable^.root then
       begin
        ne^.ptrtable^.cur := nil;
        nr^.ptrtable^.cur := nil;
       end;
       ne^.ptrtable^.root := te^.next;
       dispose(te);
       nr^.ptrtable^.root := tr^.next;
       dispose(tr);
      end;
      b := true;      
      Break;
     end;
     inc(j);
     ce := te;
     cr := tr;
     te := te^.next;
     tr := tr^.next;
    end;
   if b then break;
{    Break;
   end;}
{   inc(i);  }
   ne := ne^.next;
   nr := nr^.next;
  end;
 end;
end;

procedure TKruptarForm.rusmemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if ssCtrl in Shift then
  If key = VK_UP then
  begin
   n16click(sender);
  end Else if key = VK_DOWN then
  begin
   n15click(sender);
  end;
end;

procedure TKruptarForm.SpeedButton1Click(Sender: TObject);
{const rusb: array['A'..'Z'] of char =
(
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�',
'�'
);
var n: pposs; nt: pttable;
var t: pptrtable; nn: pttable; rm, i: integer;
type tww = record a, b: byte end;
label eloop;             }
begin
{ if xKodir = kdSJIS then exit;
 n := possor.root;
 while n <> nil do
 begin
  nt := n^.ptrtable^.root;
  while nt <> nil do
  begin
   for i := 1 to length(nt^.text) do
    if (nt^.text[i] >= 'A') and (nt^.text[i] <= 'Z') then
    begin
     nt^.text[i] := rusB[char(nt^.text[i])];
    end Else
    if (nt^.text[i] >= 'a') and (nt^.text[i] <= 'z') then
    begin
     nt^.text[i] := CHAR(BYTE(rusB[UPCASE(nt^.text[i])]) + $20);
    end;
   nt := nt^.next;
  end;
  n := n^.next;
 end;
 addd := true;
 t := GetrusTable(pttblbox.ItemIndex);
 if t = nil then exit;
 nn := t^.root; rm := 0;
 while nn <> nil do
 begin
  slist.Items.strings[rm] := inttohex(nn^.tptr, 8) + ' -> ' + nn^.text;
  nn := nn^.next;
  inc(rm);
 end;
 slistclick(sender);
 if saved then
 begin
  saved := false;
  if named then caption := Application.Title + ' - ' + savename + '*';
 end;
 showmessage('������!');             }
end;

procedure TKruptarForm.N19Click(Sender: TObject);
begin
 n19.Checked := not n19.Checked;
end;

function dir(s: string): string;
var i, l: integer;
begin
 l := 0;
 for i := length(s) downto 1 do
  if s[i] = '\' then break else inc(l);
 result := s;
 delete(result, i + 1, l);
end;

procedure TKruptarForm.emulateClick(Sender: TObject);
var f: textfile; {rf: tsearchrec; }e, u, d{, nnrom}: string; i: integer;
begin
 e := savename;
 delete(e, pos('.rnm', e), 4);
 assignfile(f, e + '.emu');
 {$I-}
 reset(f);
 {$I+}
 If ioresult <> 0 then
 begin
  cantemul := true;
  if sender = emulate then
   Showmessage('���� � ��������� �� ����������!');
  Exit;
 end else cantemul := false;
 readln(f, e);
 readln(f, u);
 u := uppercase(u);
 closefile(f);
 i := pos('%ROM%', u);
 delete(u, i, 5);
 getdir(0, d);
 if d[length(d)] <> '\' then d := d + '\';
 if pos(':', nrom) > 0 then
  insert(nrom, u, i) Else
  insert(d + nrom, u, i);
 ShellExecute(Application.Handle,
 Pchar('Open'),
 Pchar(e),
 Pchar('"' + u + '"'),
 Pchar('"' + dir(e) + '"'), SW_SHOWNORMAL);
end;

procedure TKruptarForm.N20Click(Sender: TObject);
var f: textfile; {rf: tsearchrec; }e, u, d{, nnrom}: string; i: integer;
begin
 e := savename;
 delete(e, pos('.rnm', e), 4);
 assignfile(f, e + '.emu');
 {$I-}
 reset(f);
 {$I+}
 If ioresult <> 0 then
 begin
  cantemul := true;
  if sender = emulate then
   Showmessage('���� � ��������� �� ����������!');
  Exit;
 end else cantemul := false;
 readln(f, e);
 readln(f, u);
 u := uppercase(u);
 closefile(f);
 i := pos('%ROM%', u);
 delete(u, i, 5);
 getdir(0, d);
 if d[length(d)] <> '\' then d := d + '\';
 if pos(':', orom) > 0 then
  insert(orom, u, i) Else
  insert(d + orom, u, i);
 ShellExecute(Application.Handle,
 Pchar('Open'),
 Pchar(e),
 Pchar('"' + u + '"'),
 Pchar('"' + dir(e) + '"'), SW_SHOWNORMAL);
end;

procedure TKruptarForm.N22Click(Sender: TObject);
begin
 EmuCfgForm.Showmodal;
end;

procedure TKruptarForm.N23Click(Sender: TObject);
begin
 if memo.Focused then memof := true else memof := false;
 FindReplaceForm.PageControl.ActivePageIndex := 1;
 FindReplaceForm.rFEdit.SelectAll;
 FindReplaceForm.Showmodal;
 if FindReplaceForm.ModalResult = mrOk then
 begin
  FindDialogFind(Sender);
  if founded then
  begin
   if FindReplaceForm.CheckBox.Checked then
   begin
    if messagedlg('��������?', mtWarning, [mbYes, mbNo], 0) = mrYes then
     rusmemo.SelWideText := FindReplaceForm.REdit.WideText;
   end Else rusmemo.SelWideText := FindReplaceForm.REdit.WideText;
  end;
 end;
end;

procedure TKruptarForm.slistDblClick(Sender: TObject);
begin
 showmessage(inttostr(slist.ItemIndex));
end;
var upda: boolean;

procedure TKruptarForm.Update1Click(Sender: TObject);
begin
 upda := false;
 upd.Body := 'temp.$$$';
 upd.Get('http://djinn.avs.ru/update/info.txt');
end;

Function getcdir: string;
var i, p: integer; s: string;
begin
 s := paramstr(0); p := 0;
 for i := length(s) downto 1 do
  if s[i] = '\' then
  begin
   p := i + 1;
   break;
  end;
 delete(s, p, length(s) - p + 1);
 result := s;
end;

procedure TKruptarForm.UpdSuccess(Cmd: CmdType);
Var f: textfile; s, e, u: string;
label ll;
begin
 If Upd.BytesRecvd = Upd.BytesTotal then
 begin
  AssignFile(f, getcdir + upd.body);
{$I-}
  Reset(f);
{$I+}
  If IoResult = 0 then
  begin
   Readln(f, s);
   u := '';
   while not eof(f) do
   begin
    readln(f, e);
    u := u + e + #13#10;
   end;
   CloseFile(f);
   Erase(f);
   if strtoint(s) > curver then
   begin
    ShowMessage(u);
    SaveDialog.DefaultExt := '.rar';
    SaveDialog.Filter := 'Rar Archive|*.rar';
    insert('.', s, 2);
    insert('.', s, 4);
    insert('.', s, 6);
    faname := 'Kruptar' + s +'.rar';
    SaveDialog.FileName := faname;
    if SaveDialog.Execute then
    begin
     procform.Caption := 'http://djinn.avs.ru/progs/' + faname;
     procform.ShowModal;
    end;
    SaveDialog.DefaultExt := '.rnm';
    SaveDialog.Filter := '*.rnm|*.rnm';
   end Else goto ll;
  end Else ll: ShowMessage('����� ������ ���� ���!');
  Upd.Disconnect;
 end;
end;


procedure TKruptarForm.N26Click(Sender: TObject);
begin
 OptDlg.Showmodal;
end;

procedure TKruptarForm.tildaenterClick(Sender: TObject);
begin
 if rusmemo.enabled then rusmemo.SetFocus;
end;

procedure TKruptarForm.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
var ww: integer;
begin
 If NewWidth < 635 then NewWidth := 635;
 If NewHeight < 610 then NewHeight := 610;
 slist.Width := newwidth - 135;
{ addptbtn.Left := newwidth - 73;
 pttacnt.Left := newwidth - 145;
 ptta.Left := newwidth - 217;    }
  justenter.Left := ((newwidth - 8) - justenter.Width) div 2;
   umnenter.Left := ((newwidth - 8) - justenter.Width) div 2;
  starenter.Left := ((newwidth - 8) - justenter.Width) div 2;
 tildaenter.Left := ((newwidth - 8) - justenter.Width) div 2;
 label5.Left := umnenter.Left + 2;
 label3.Left := umnenter.Left + 3;
 label4.Left := umnenter.Left + 4;
 rusmemo.Left := justenter.Left + 19;
 rpos.Left := rusmemo.Left;
 label2.Left := rpos.Left + 135;
 japl.Left := label2.Left + 168;
 rusmemo.Width := ((newwidth - 8) - rusmemo.Left);
 memo.Width := rusmemo.Width;
 memo.Height := (newheight - 65) - memo.Top;
 ww := memo.Height;
 While memo.Top + ww > Statusbar.Top do Dec(ww);
 memo.Height := ww;
 rusmemo.Height := memo.Height; 
 statusbar.Panels[0].Width := rusmemo.Left - 3;
 resize := true;
end;

procedure TKruptarForm.N33Click(Sender: TObject);
var pmemo: ^tunicodememo;
begin
 if rusmemo.Focused then pmemo := @rusmemo Else
 if    memo.Focused then pmemo := @memo Else Exit;
 pmemo^.SelectAll;
end;

procedure TKruptarForm.N29Click(Sender: TObject);
var pmemo: ^tunicodememo;
begin
 if rusmemo.Focused then pmemo := @rusmemo Else
 if    memo.Focused then pmemo := @memo Else Exit;
 pmemo^.CopyToClipboard;
end;

procedure TKruptarForm.N31Click(Sender: TObject);
begin
 rusmemo.SelWideText := '';
end;

procedure TKruptarForm.N30Click(Sender: TObject);
begin
 rusmemo.PasteFromClipboard;
end;

procedure TKruptarForm.N28Click(Sender: TObject);
begin
 rusmemo.CutToClipboard;
end;

Constructor tctableso.Init;
begin
 rootO := nil;
 curO := nil;
 rootE := nil;
 curE := nil;
 Count := 0;
end;

Procedure tctableso.CheckTables;
Procedure Nosm(s: string);
begin
 ShowMessage('� ������� "' + s +
  '" ��� �������� ��������� ������!' + #13#10 +
  '����������� �� ���������: /00');
end;
var n1: pctable; nos: boolean;
begin
 n1 := CurO^.tab;
 nos := true;
 while n1 <> nil do
 begin
  if n1^.endstr then
  begin
   nos := false;
   break;
  end;
  n1 := n1^.next;
 end;
 If nos then
 begin
  Nosm(CurO^.fname);
  tadlen := 1;
  kruptarform.table1[Count - 1, 0] := '/';
 end;
 n1 := CurE^.tab;
 nos := true;
 while n1 <> nil do
 begin
  if n1^.endstr then
  begin
   nos := false;
   break;
  end;
  n1 := n1^.next;
 end;
 If nos then
 begin
  Nosm(CurE^.fname);
  tadlen := 1;
  kruptarform.table2[Count - 1, 0] := '/';
 end;
end;

Procedure tctableso.Add(f1, f2: string);
var n, rr: pctables; i: byte;
begin
 inc(count);
 new(n);
 n^.tab := nil;
 n^.cur := nil;
 n^.fname := f1;
 n^.count := 0;
 n^.cl := 0;
 n^.next := nil;
 rr := curO;
 curO := n;
 if rr <> nil then rr^.next := curO;
 if rootO = nil then rootO := n;
 tt := false; kruptarform.loadtab(count - 1, f1);
 new(n);
 n^.tab := nil;
 n^.cur := nil;
 n^.fname := f2;
 n^.count := 0;
 n^.cl := 0;
 n^.next := nil;
 rr := curE;
 curE := n;
 if rr <> nil then rr^.next := curE;
 if rootE = nil then rootE := n;
 tt := true;
 For i := 0 to 255 do
 begin
  endsym := false;
  undsym := false;
  eundostr := '';
  KruptarForm.table2[count - 1, i] := '[#' + IntToHex(i, 2) + ']';
 end;
 kruptarform.loadtab(count - 1, f2);
end;

Destructor tctableso.Done;
var n: pctables; t, tn: pctable;
begin
 while rootO <> nil do
 begin
  n := rootO;
  rootO := n^.next;
  t := n^.tab;
  while t <> nil do
  begin
   tn := t;
   t := tn^.next;
   dispose(tn);
  end;
  dispose(n);
 end;
 while rootE <> nil do
 begin
  n := rootE;
  rootE := n^.next;
  t := n^.tab;
  while t <> nil do
  begin
   tn := t;
   t := tn^.next;
   dispose(tn);
  end;
  dispose(n);
 end;
 Count := 0;
end;


procedure TKruptarForm.N34Click(Sender: TObject);
var WaveHandle: THandle; WavePointer: pointer;
begin
 WaveHandle := FindResource(hInstance, 'KRUPTAR_WAV', RT_RCDATA);
 if WaveHandle <> 0 then
 begin
  WaveHandle := LoadResource(hInstance,WaveHandle);
  if WaveHandle <> 0 then
  begin
   WavePointer := LockResource(WaveHandle);
{   PlayResourceWave :=}
   sndPlaySound(WavePointer,snd_Memory OR SND_ASYNC);
   UnlockResource(WaveHandle);
   FreeResource(WaveHandle);
  end;
 end;
 Showmessage('Kruptar 5.0.4.0' + #13#10 +
             '   6 ��� 2005 ���� / 08:10' + #13#10 + #13#10 +
             'http://magicteam.nm.ru' + #13#10 +
             '            Djinn');
end;

procedure TKruptarForm.N36Click(Sender: TObject);
begin
 fontdialog.Font := slist.Font;
 if fontdialog.Execute then
 begin
  slist.Font := fontdialog.Font;
  slist.ItemHeight := Abs(slist.Font.Height) + 2;
 end;
end;

procedure TKruptarForm.N37Click(Sender: TObject);
begin
 fontdialog.Font := memo.Font;
 if fontdialog.Execute then
  memo.Font := fontdialog.Font;
end;

procedure TKruptarForm.N38Click(Sender: TObject);
begin
 fontdialog.Font := rusmemo.Font;
 if fontdialog.Execute then
  rusmemo.Font := fontdialog.Font;
end;

procedure TKruptarForm.pttblboxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var n, nn, p, pp: pposs; j: integer;
begin
 if (pttblbox.ItemIndex >= 0) and (key = vk_delete) and
  (Messagedlg('�������?', mtWarning, [mbYes, mbNo], 0) = mrYes) then
 begin
  j := 0;
  n := posso.root;
  nn := possor.root;
  p := nil;
  pp := nil;
  while j < pttblbox.ItemIndex do
  begin
   p := n;
   pp := nn;
   n := n^.next;
   nn := nn^.next;
   if (n = nil) or (nn = nil) then exit;
   inc(j);
  end;
  if p = nil then
  begin
   if posso.cur = posso.root then
   begin
    posso.cur := n^.next;
    possor.cur := nn^.next;
   end;
   posso.root := n^.next;
   possor.root := nn^.next;
  end Else if n = posso.cur then
  begin
   posso.cur := p;
   possor.cur := pp;
   p^.next := n^.next;
   pp^.next := nn^.next;
  end Else
  begin
   p^.next := n^.next;
   pp^.next := nn^.next;
  end;
  Dec(posso.count);
  Dec(possor.count);
  Dispose(n^.ptrtable, clear);
  Dispose(nn^.ptrtable, clear);
  Dispose(n);
  Dispose(nn);
  j := pttblbox.ItemIndex;
  pttblbox.Clear;
  n := posso.root;
  if n = nil then
  begin
   rusmemo.Clear;
   memo.Clear;
   slist.Items.Clear;
   exit;
  end;
  while n <> nil do
  begin
   pttblbox.Items.Add(inttostr(pttblbox.Items.Count) +
   '. ' + Inttohex(n^.ptrtable^.pos, 8));
   n := n^.next;
  end;
  pttblbox.ItemIndex := j;
  initall;
  slist.Refresh;
 end;
end;

procedure TKruptarForm.N39Click(Sender: TObject);
var f: textfile; s: string;
begin
 OpenDialog.Filter := '*.txt|*.txt';
 OpenDialog.DefaultExt := 'txt';
 OpenDialog.FileName := '';
 If OpenDialog.Execute then
 begin
  AssignFile(f, opendialog.FileName);
  Reset(f);
  while not eof(f) do
  begin
   readln(f, s);
   if pos('-', s) > 0 then
   begin
    ptta.Text := copy(s, 1, pos('-', s) - 1);
    delete(s, 1, pos('-', s));
    pttacnt.Text := s;
    addptbtnClick(sender);
   end Else
   begin
    ptta.Text := s;
    pttacnt.Text := '';
    addptbtnClick(sender);
   end
  end;
  Closefile(f);
 end;
 OpenDialog.FileName := '';
 OpenDialog.DefaultExt := 'rnm';
 OpenDialog.Filter := '*.rnm|*.rnm' ;
end;

procedure TKruptarForm.QuickInsertClick(Sender: TObject);
begin
 QuickInsert.Checked := not QuickInsert.Checked;
end;

procedure TKruptarForm.N41Click(Sender: TObject);
begin
 If not DictForm.Visible then DictForm.Show;
end;

Function tkruptarform.Getcc: integer;
begin
 result := cc;
end;

Function tkruptarform.Getic: integer;
begin
 result := ic;
end;

Procedure tkruptarform.Setcc(value: integer);
begin
 cc := value;
end;

Procedure tkruptarform.Setic(value: integer);
begin
 ic := value;
end;

function tkruptarform.mvalue: integer;
begin
 result := procform.Gauge1.MaxValue;
end;

procedure TKruptarForm.Button1Click(Sender: TObject);
begin
 processform.showmodal;
end;

procedure TKruptarForm.N44Click(Sender: TObject);
begin
 button1click(sender);
end;

procedure TKruptarForm.N45Click(Sender: TObject);
begin
 n45.Checked := not n45.Checked;
end;

procedure TKruptarForm.ProgressItemClick(Sender: TObject);
var s: string;
Function GetWending: string;
begin
 Result := '';
 if (s[length(s)] in ['5'..'9', '0']) or
    ((length(s) >= 2) and (s[length(s)] in ['1'..'4']) and
   (s[length(s) - 1] = '1')) then
  Result := '��' Else
 if s[length(s)] in ['2'..'4'] then
  Result := '�' Else
 if s[length(s)] = '1' then
  Result := '';
end;
var O, C: pposs; ot, ct: pttable; tcount: integer;
begin
 s := inttostr(slist.Items.count);
 DictForm.TotalEl.Caption := '����� ' + s + ' �������' + GetWending;
 DictForm.TotalEl.Left := Abs(DictForm.Width - DictForm.TotalEl.ClientWidth) div 2;
 tcount := 0;
 o := posso.root;
 c := possor.root;
 while o <> nil do
 begin
  ot := o^.ptrtable^.root;
  ct := c^.ptrtable^.root;
  While ot <> nil do
  begin

   if (ot^.text <> ct^.text) then inc(tcount);
   ot := ot^.next;
   ct := ct^.next;
  end;
  o := o^.next;
  c := c^.next;
 end;
 s := inttostr(tcount);
 DictForm.Translatedel.Caption := '���������� ' + s + ' �������' + GetWending;
 DictForm.Translatedel.Left := Abs(DictForm.Width - DictForm.Translatedel.ClientWidth) div 2;
 s := inttostr((tcount * 100) div slist.Items.count);
 DictForm.Percentel.Caption := '�������� ' + s + '%';
 DictForm.Percentel.Left := Abs(DictForm.Width - DictForm.Percentel.ClientWidth) div 2;
 DictForm.ShowModal;
end;

end.
