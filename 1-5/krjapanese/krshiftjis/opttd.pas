unit opttd;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Dialogs, Spin;

type
  TOptDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    TablL: TLabeledEdit;
    openb: TSpeedButton;
    ceb: TButton;
    Aob: TButton;
    cob: TButton;
    gb: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    OpenDialog: TOpenDialog;
    oe: TMemo;
    ee: TMemo;
    Label3: TLabel;
    Button1: TButton;
    Label4: TLabel;
    textgr: TGroupBox;
    lentex: TSpinEdit;
    Label5: TLabel;
    procedure openbClick(Sender: TObject);
    procedure AobClick(Sender: TObject);
    procedure cobClick(Sender: TObject);
    procedure cebClick(Sender: TObject);
    procedure oeClick(Sender: TObject);
    procedure eeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure oeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure OKBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OptDlg: TOptDlg;

implementation

{$R *.dfm}

Uses ranmaedit;

procedure TOptDlg.openbClick(Sender: TObject);
begin
 If opendialog.Execute then TablL.Text := opendialog.FileName;
end;

procedure TOptDlg.AobClick(Sender: TObject);
begin
 oe.Lines.Add(tabll.Text);
 ee.Lines.Add(tabll.Text);
 oe.SelStart := length(oe.Lines.GetText) - 1;
 oe.SelLength := 0;
 ee.SelStart := length(ee.Lines.GetText) - 1;
 ee.SelLength := 0; 
end;

procedure TOptDlg.cobClick(Sender: TObject);
var ii: integer;
begin
 ii := oe.CaretPos.Y;
 oe.Lines.Strings[ii] := tabll.Text;
end;

procedure TOptDlg.cebClick(Sender: TObject);
var ii: integer;
begin
 ii := ee.CaretPos.Y;
 ee.Lines.Strings[ii] := tabll.Text;
end;

procedure TOptDlg.oeClick(Sender: TObject);
begin
 ee.SelStart := oe.SelStart;
 ee.SelLength := 0;
 Label3.Caption := inttostr(oe.CaretPos.Y);
end;

procedure TOptDlg.eeClick(Sender: TObject);
begin
 oe.SelStart := ee.SelStart;
 oe.SelLength := 0;
 Label3.Caption := inttostr(ee.CaretPos.Y); 
end;

procedure TOptDlg.FormActivate(Sender: TObject);
var n: pctables; s: string; i: integer; p: pposs;
begin
 lentex.Value := maxstrlen;
  oe.Clear;
 ee.Clear;
 tabll.Text := '';
 n := Otables.rootO;
 p := posso.root;
 i := 0;
 while i <> kruptarform.pttblbox.ItemIndex do
 begin
  inc(i);
  p := p^.next;
 end;
 if p <> nil then label4.Caption := inttostr(p^.tabn);
 while n <> nil do
 begin
  oe.Lines.Add(n^.fname);
  n := n^.next;
 end;
 n := Otables.rootE;
 while n <> nil do
 begin
  ee.Lines.Add(n^.fname);
  n := n^.next;
 end;
 s := oe.Lines.GetText;
 if s[length(s)] = #10 then
 begin
  SetLength(s, length(s) - 2);
  oe.Lines.SetText(pchar(s));
 end;
 s := ee.Lines.GetText;
 if s[length(s)] = #10 then
 begin
  SetLength(s, length(s) - 2);
  ee.Lines.SetText(pchar(s));
 end;
 oe.SelStart := 0;
 oe.SelLength := 0;
 ee.SelStart := 0;
 ee.SelLength := 0;
 Label3.Caption := inttostr(oe.CaretPos.Y); 
end;

procedure TOptDlg.oeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 ee.SelStart := oe.SelStart;
 ee.SelLength := 0;
 Label3.Caption := inttostr(oe.CaretPos.Y);
end;

procedure TOptDlg.eeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 oe.SelStart := ee.SelStart;
 oe.SelLength := 0;
 Label3.Caption := inttostr(ee.CaretPos.Y);
end;

procedure TOptDlg.OKBtnClick(Sender: TObject);
type
 dwa = array[0..65535] of record tabn, pos, bcnt: integer; end;
var i, c: integer; n: pctables; nn: pposs; tnum: ^dwa;
begin
 maxstrlen := lentex.Value;
 n := otables.rootO;
 for i := 0 to otables.Count - 1 do
 begin
  n^.fname := oe.Lines.Strings[i];
  kruptarform.loadtab(i, n^.fname);
  n := n^.next;
 end;
 n := otables.rootE;
 for i := 0 to otables.Count - 1 do
 begin
  n^.fname := ee.Lines.Strings[i];
  kruptarform.loadtab(i, n^.fname);
  n := n^.next;
 end;
 if otables.Count < oe.Lines.Count then
  for i := otables.Count to oe.Lines.count - 1 do
   Otables.Add(oe.Lines.Strings[i], ee.Lines.Strings[i]);
 c := posso.count - 1;
 getmem(tnum, (c + 1) * sizeof(tposs));
 nn := posso.root;
 For i := 0 to c do
 begin
  tnum^[i].tabn := nn^.tabn;
  tnum^[i].pos := nn^.ptrtable^.pos;
  tnum^[i].bcnt := nn^.ptrtable^.bcnt;
  nn := nn^.next;
 end;
 posso.clear;
 kruptarform.pttblbox.clear;
 for i := 0 to c do
  posso.add(tnum^[i].tabn, tnum^[i].pos, tnum^[i].bcnt);
 i := kruptarform.slist.ItemIndex;
 kruptarform.initall;
 kruptarform.slist.ItemIndex := i;
 kruptarform.slistclick(kruptarform.slist);
 freemem(tnum, (c + 1) * sizeof(tposs));
end;

procedure TOptDlg.Button1Click(Sender: TObject);
var n: pposs; i, j: integer;
begin
 n := posso.root;
 i := strtoint(label3.caption);
 j := 0;
 while j <> kruptarform.pttblbox.ItemIndex do
 begin
  inc(j);
  n := n^.next;
 end;
 if n = nil then exit;
 n^.tabn := i;
 label4.Caption := inttostr(i);
end;

end.
