unit FRep;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, ComCtrls, UnicodeEdit;

type
  TFindReplaceForm = class(TForm)
    OkButton: TButton;
    CancelBtn: TButton;
    PageControl: TPageControl;
    TabSheet: TTabSheet;
    TabSheet2: TTabSheet;
    FEdit: TUnicodeEdit;
    Label1: TLabel;
    Label2: TLabel;
    RFEdit: TUnicodeEdit;
    Label3: TLabel;
    REdit: TUnicodeEdit;
    CheckBox: TCheckBox;
    JapTypeItem: TCheckBox;
    japl1: TLabel;
    japl2: TLabel;
    repall: TCheckBox;
    procedure FEditKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure RFEditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  end;

var
  FindReplaceForm: TFindReplaceForm;

implementation

{$R *.DFM}

Uses Uniconv;

Var Memo: TUnicodeEdit;
    japl: TLabel;
    ii: Word;

procedure TFindReplaceForm.FEditKeyPress(Sender: TObject; var Key: Char);
Procedure SetText(s: PWideChar);
begin
 memo.WideText := s;
end;

const lsogl: set of char =
['s', 't', 'w', 'm', 'n', 't', 'r', 'k', 'p', 'h', 'b', 'd', 'z', 'g', 'f', 'y',
 'a', 'i', 'u', 'e', 'o'];
const bsogl: set of char =
['S', 'T', 'W', 'M', 'N', 'T', 'R', 'K', 'P', 'H', 'B', 'D', 'Z', 'G', 'F', 'Y',
 'A', 'I', 'U', 'E', 'O'];
var s: wstring; ss: integer;
begin
 if JapTypeItem.Checked then
 begin
  Case TUnicodeEdit(Sender).Tag of
   0:
   begin
    Memo := FEdit;
    japl := japl1;
   end;
   1:
   begin
    Memo := RFEdit;
    japl := japl2;
   end;
   2:
   begin
    Memo := REdit;
    japl := japl2;
   end;
  end;
  if (japl.Caption = '') and (key = ' ') then
  begin
   key := #0;
   s := memo.widetext;
   ss := memo.SelStart;
   insert(#$3000, s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
  end Else
  if (japl.Caption = '') and (key = '-') then
  begin
   key := #0;
   s := memo.widetext;
   ss := memo.SelStart;
   insert(#$30FC, s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
  end Else
  if (japl.Caption = '') and (key = '~') then
  begin
   key := #0;
   s := memo.widetext;
   ss := memo.SelStart;
   insert(#$FF5E, s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
  end Else
  if (japl.Caption = '') and (key = ',') then
  begin
   key := #0;
   s := memo.widetext;
   ss := memo.SelStart;
   insert(#$3001, s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
  end Else
  if (japl.Caption = '') and (key = '.') then
  begin
   key := #0;
   s := memo.widetext;
   ss := memo.SelStart;
   insert(#$3002, s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
  end Else
  if (japl.Caption = '') and (key in ['0'..'9']) then
  begin
   s := memo.widetext;
   ss := memo.SelStart;
   insert(widechar($FF10 + (byte(key) - byte('0'))), s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
   key := #0;
  end Else
  if (japl.Caption = '') and
   (key in ['!','@','#','$','%', '&', '*', '(', ')','_','=']) then
  begin
   s := memo.widetext;
   ss := memo.SelStart;
   insert(widechar($FF00 + (byte(key) - $20)), s, memo.SelStart + 1);
   SetText(pwidechar(s));
   memo.SelStart := ss + 1;
   japl.Caption := '';
   key := #0;
  end Else
  if (japl.Caption = '') and ((key in lsogl) or (key in bsogl)) then
  begin
   japl.Caption := lowercase(key);
   ii := $60 * Byte(key in bsogl);
   if ii = $60 then;
   if upcase(Key) in ['A', 'I', 'U', 'E', 'O'] then
   Case upcase(Key) of
    'A':
    begin
     s := memo.widetext;
     ss := memo.SelStart;
     insert(widechar($3042 + ii), s, memo.SelStart + 1);
     SetText(pwidechar(s));
     memo.SelStart := ss + 1;
     japl.Caption := '';
    end;
    'I':
    begin
     s := memo.widetext;
     ss := memo.SelStart;
     insert(widechar($3044 + ii), s, memo.SelStart + 1);
     SetText(pwidechar(s));
     memo.SelStart := ss + 1;
     japl.Caption := '';
    end;
    'U':
    begin
     s := memo.widetext;
     ss := memo.SelStart;
     insert(widechar($3046 + ii), s, memo.SelStart + 1);
     SetText(pwidechar(s));
     memo.SelStart := ss + 1;
     japl.Caption := '';
    end;
    'E':
    begin
     s := memo.widetext;
     ss := memo.SelStart;
     insert(widechar($3048 + ii), s, memo.SelStart + 1);
     SetText(pwidechar(s));
     memo.SelStart := ss + 1;
     japl.Caption := '';
    end;
    'O':
    begin
     s := memo.widetext;
     ss := memo.SelStart;
     insert(widechar($304A + ii), s, memo.SelStart + 1);
     SetText(pwidechar(s));
     memo.SelStart := ss + 1;
     japl.Caption := '';
    end;
   end;
   key := #0;
  end Else if japl.Caption <> '' then
  Case japl.Caption[1] of
   's':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3057 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3055 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3057 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3057 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3059 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3057 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305B + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3057 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305D + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     's':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'z':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3058 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3056 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3058 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3058 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305A + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3058 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305C + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3058 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305E + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'z':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'k':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304D + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($304B + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($304D + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304D + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($304F + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304D + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3051 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304D + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3053 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'k':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'g':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304E + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($304C + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($304E + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304E + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3050 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304E + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3052 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($304E + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3054 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'g':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   't':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3061 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($305F + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3061 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3061 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3064 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3061 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3066 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3061 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3068 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     't':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'n':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($306B + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($306A + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3093 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($306B + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3093 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($306B + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($306C + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3093 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($306B + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($306D + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3093 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($306B + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($306E + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3093 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'n':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     ' ':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3093 + ii), s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     Else key := #0;
    end;
   end;

   'd':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3062 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3060 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3062 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3062 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3065 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3062 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3067 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3062 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3069 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'd':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'm':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($307F + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($307E + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($307F + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($307F + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3080 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($307F + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3081 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($307F + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3082 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'm':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'h':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3072 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($306F + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3072 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3072 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3072 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3078 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3072 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($307B + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'h':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'f':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);       
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(sS, 2);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.Caption) <> 2 then
      begin
       insert(widechar($3041 + ii), s, memo.SelStart + 2);
       inc(ss);
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.Caption) <> 2 then
      begin
       insert(widechar($3043 + ii), s, memo.SelStart + 2);
       inc(ss);
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss, 2);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.Caption) <> 2 then
      begin
       insert(widechar($3047 + ii), s, memo.SelStart + 2);
       inc(ss);
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3075 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss, 2);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      if length(japl.Caption) <> 2 then
      begin
       insert(widechar($3049 + ii), s, memo.SelStart + 2);
       inc(ss);
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'f':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'b':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3073 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3070 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3073 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3073 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3076 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3073 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3079 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3073 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($307C + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'b':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'p':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3074 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3071 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3074 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3074 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3077 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3074 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($307A + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($3074 + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($307D + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'p':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'r', 'l':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($308A + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($3089 + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3083 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'i':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($308A + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3043 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($308A + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($308B + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3085 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'e':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($308A + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($308C + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3047 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      if (length(japl.Caption) = 2) and (japl.Caption[2] = 'y') then
       insert(widechar($308A + ii), s, memo.SelStart + 1)
      Else
       insert(widechar($308D + ii), s, memo.SelStart + 1);
      if length(japl.Caption) = 2 then
      begin
       if japl.Caption[2] = 'y' then
       begin
        insert(widechar($3087 + ii), s, memo.SelStart + 2);
        inc(ss);
       end Else if japl.Caption[2] = japl.Caption[1] then
       begin
        insert(widechar($3063 + ii), s, memo.SelStart + 1);
        inc(ss);
       end
      end;
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + 'y';
      key := #0;
     end;
     'r', 'l':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

   'w':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($308F + ii), s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3092 + ii), s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     Else key := #0;
    end;
   end;

   'y':
   begin
    case LOWERcase(key)[1] of
     'a':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3084 - byte(length(japl.Caption) = 2) + ii),
      s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'u':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3086 - byte(length(japl.Caption) = 2) + ii),
      s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'o':
     begin
      key := #0;
      s := memo.widetext;
      ss := memo.SelStart;
      insert(widechar($3088 - byte(length(japl.Caption) = 2) + ii),
       s, memo.SelStart + 1);
      SetText(pwidechar(s));
      memo.SelStart := ss + 1;
      japl.Caption := '';
     end;
     'y':
     begin
      if length(japl.Caption) = 1 then
       japl.Caption := japl.Caption + japl.Caption[1];
      key := #0;
     end;
     Else key := #0;
    end;
   end;

  end Else key := #0;
 end;
end;

procedure TFindReplaceForm.FormShow(Sender: TObject);
begin
 if pagecontrol.ActivePage = tabsheet then
  FEdit.SetFocus Else RFEdit.SetFocus;
end;

procedure TFindReplaceForm.RFEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (ssctrl in shift) and (upcase(char(key)) = 'J') then
  japtypeitem.Checked := not japtypeitem.Checked;
end;

procedure TFindReplaceForm.FormActivate(Sender: TObject);
begin
 if FindReplaceForm.PageControl.ActivePageIndex = 0 then
  FindReplaceForm.FEdit.SetFocus Else
 FindReplaceForm.RFEdit.SetFocus;
end;

end.
