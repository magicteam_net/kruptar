unit k8_Disasm;

interface

function KVM_Disasm(BaseOffset: Integer; const Source; var Size: Integer): String;

Var
 KVM_HexDisasm: Boolean = True;
 KVM_SpaceChar: Char = ' ';

implementation

Uses SysUtils;

Const
 ARegisters: Array[0..31] of String[3] =
 ('r0', 'r1', 'r2', 'r3', 'r4', 'r5', 'r6', 'r7',
  'r8', 'r9', 'r10','r11','r12','r13','r14','r15',
  'r16','r17','r18','r19','r20','r21','r22','r23',
  'esp','emp','epc', 'sp', 'rmp','rpc','mp', 'pc');
 ARegisters64: Array[0..7] of String[3] =
 ('xr0', 'xr1', 'xr2', 'xr3', 'xr4', 'xr5', 'xr6', 'xr7');
 AAriphmeric: Array[$00..$0C] of String[4] =
 ('mov', 'add', 'sub', 'rsb', 'mul', 'div', 'divs',
  'mod', 'rmd', 'and', 'bic', 'or', 'xor');
 ALogic: Array[0..3] of String[3] =
 ('shl', 'inc', 'shr', 'sar');
 ALogic2: Array[0..7] of String[4] =
 ('sgt', 'sge', 'slt', 'sle', 'sgts', 'sges', 'slts', 'sles');
 AAriphmericR: Array[$10..$1F] of String[t] =
 ('mova', 'mvna', 'adda', 'suba', 'rsba', 'mula', 'diva', 'divas',
  'moda', 'rmda', 'swapa','swpha','anda', 'bica', 'ora', 'xora');
 AConditionalBranch: Array[$20..$29] of String[5] =
 ('beqa', 'bnea', 'bgta', 'bgea', 'blta', 'blea', 'bgtas',
  'bgeas', 'bltas', 'bleas');
 AConditionalBranch2: Array[$00..$0F] of String[4] =
 ('beq', 'bne', 'bgt', 'bge', 'blt', 'ble', 'bgt', 'bge', 'blt', 'ble',
  'beq', 'bne', 'bgt', 'bge', 'blt', 'ble');
 AMemoryOperate: Array[0..7] of String[4] =
 ('str', 'strh', 'strb', 'ldr', 'ldrh', 'ldsh', 'ldrb', 'ldsb');
 ALstPushPop: array[0..1] of string[5] =
 ('pushl', 'popl');
 AOther: Array[0..7] of String[4] =
 ('push', 'pop', 'bx', 'blx', 'blmx', 'cbd', 'cwd', 'swap');
 ASeq_Sne: Array[0..1] of String[3] =
 ('seq', 'sne');
 ALdmia_Stmia: Array[$2C..$2D] of String[5] =
 ('ldmia', 'stmia');
 ACall: Array[0..3] of String[3] =
 ('b', 'b', 'bl', 'blm');
 AAriphmetic64: Array[0..15] of String[4] =
 ('xmov', 'xmvn', 'xadd', 'xsub',
  'xrsb', 'xmul', 'xdiv', 'xrdv',
  'xmod', 'xand', 'xbic', 'xorr',
  'xxor', 'cdq',  'cwq',  'cbq');
 ALogic64: Array[0..3] of String[5] =
 ('xshl', 'fcsr', 'xshr', 'xsar');
 ACompare64: Array[0..7] of String[5] =
 ('xseq', 'xsne', 'xsgt', 'xsge', 'xslt', 'xsle', 'xseqi','xsnei');
 ACompare64s: Array[0..5] of String[6] =
 ('xseq32', 'xsne32', 'xsgt32', 'xsge32', 'xslt32', 'xsle32');
 AFloat32: Array[0..7] of String[6] =
 ('fadd32', 'fsub32', 'fmul32', 'fdiv32',
  'fseq32', 'fsne32', 'fsgt32', 'fsge32');
 AFloat32a: Array[0..7] of String[6] =
 ('fabs32', 'fneg32', 'fexp32', 'frac32',
  'fint32', 'frnd32', 'ftrn32', 'fcds32');
 AFloat64: Array[0..15] of String[5] =
 ('fadd', 'fsub', 'fmul', 'fdiv',
  'fabs', 'frnd', 'ftrn', 'fcqr',
  'fneg', 'fexp', 'frac', 'fint',
  'fln',  'frsb', 'fsqr', 'fsqrt');
 AFloatCompare: Array[0..5] of String[4] =
 ('fseq', 'fsne', 'fsgt',
  'fsge', 'fslt', 'fsle');
 ASystem: Array[0..3] of String[5] =
 ('nop', 'ret', 'raise', 'stop');

Type
 TKVMD = function(BaseOffset: Integer; const Source; var Size: Integer): String;

Function DisIntToStr(I, Digits: Integer): String;
begin
 If KVM_HexDisasm then
 begin
  if (I >= -15) and (I <= 15) then Result := IntToStr(I) else
  begin
   If (Digits < 8) and (I < 0) then
    Result := '-$' + IntToHex(-I, Digits) Else
    Result := '$' + IntToHex(I, Digits)
  end;
 end Else Result := IntToStr(I);
end;

function DAriphmetic(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PWord(PC)^;
 Inc(PC, 2);
 Size := 2;
 Result := AAriphmeric[W and $3F] + KVM_SpaceChar + ARegisters[(W shr 6) and 31] + ',';
 Case (W shr 11) and 1 of
  0: Result := Result + DisIntToStr((W shr 12) + 2, 2);
  Else
  Case W shr 13 of
   0: Result := Result + DisIntToStr(0, 2);
   1:
   begin
    Result := Result + DisIntToStr(PC^, 2);
    Inc(Size);
   end;
   2:
   begin
    Result := Result + DisIntToStr(PWord(PC)^, 4);
    Inc(Size, 2);
   end;
   3:
   begin
    Result := Result + DisIntToStr(PInteger(PC)^, 8);
    Inc(Size, 4);
   end;
   4:
   begin
    Result := Result + DisIntToStr(PShortInt(PC)^, 2);
    Inc(Size);
   end;
   5:
   begin
    Result := Result + DisIntToStr(PSmallInt(PC)^, 4);
    Inc(Size, 2);
   end;
   6: Result := Result + 'hp';
   else
  {7:}Result := Result + DisIntToStr(1, 2);
  end;
 end;
end;

function DLogic(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W, R: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PCardinal(PC)^;
 Inc(PC, 3);
 Size := 3;
 R := (W shr 16) and 7;
 Result := ALogic[R shr 1] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[(W shr 11) and 31] + ',';
 If Odd(R) then Result := Result + ARegisters[W shr 19] Else
 begin
  If R <> 2 then Result := Result + DisIntToStr(W shr 19, 2) Else
  begin //If ADD
  // R := PC^;
   Inc(Size);
   Result := Result + DisIntToStr(((W shr 19) and $FFF) * -Integer(W < 0), 2);
  { If R and 128 <> 0 then
    Result := Result + DisIntToStr(Integer(
    Cardinal($FFFFF000 or ((W shr 19) or ((R and $7F) shl 5)))), 4) Else
    Result := Result + DisIntToStr((W shr 19) or ((R and $7F) shl 5), 4);}
  end;
 end;
end;

function DCompare(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W{, R}: Integer; PC: PByte;
begin
 PC := Addr(Source);
 W := PInteger(PC)^;
 Size := 4;
 Result := ALogic2[(W shr 16) and 7] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[(W shr 11) and 31] + ',' +
           IntToStr(((W shr 19) and $FFF) * -Integer(W < 0)) + ']';
{ R := W shr 24;
 If R and 128 <> 0 then
  Result := Result + DisIntToStr(Integer(
  Cardinal($FFFFF000 or (((W shr 19) and 31) or ((R and $7F) shl 5)))), 4) Else
  Result := Result + DisIntToStr(((W shr 19) and 31) or ((R and $7F) shl 5), 4);}
end;

function DCompareR(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PWord(PC)^;
 Inc(PC, 2);
 W := W or (PC^ shl 16);
 Size := 3;
 Result := ALogic2[(W shr 16) and 7] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[(W shr 11) and 31] + ',' +
           ARegisters[W shr 19];
end;

function DAriphmeticR(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PWord(PC)^;
 Size := 2;
 Result := AAriphmericR[W and $3F] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[W shr 11];
end;

function DConditionalBranch(BaseOffset: Integer;
  const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PCardinal(PC)^;
 Size := 4;
 Result := AConditionalBranch[W and $3F] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[(W shr 11) and 31] + ',$' +
           IntToHex(BaseOffset + SmallInt(W shr 16), 8);
end;

function DConditionalBranch2(BaseOffset: Integer;
  const Source; var Size: Integer): String;
var
 W: Word; PC: PByte; R: Byte;
 Integer32: Integer;
 Float32: Single absolute Integer32;;
begin
 PC := Addr(Source);
 W := PWord(PC)^;
 Inc(PC, 2);
 Integer32 := PInteger(PC)^;
 Inc(PC, 4);
 Size := 8;
 R := (W shr 12) and $0F;
 Result := AConditionalBranch2[R];
 case (W shr 6) and 1 of
  0:
  begin
   if R in [$0A..$0F] then Result := Result + 'n';
   Result := Result + KVM_SpaceChar +
             ARegisters[(W shr 7) and 31] + ',' +
             DisIntToStr(Integer32, 1);
  end;
  1:
  case (W shr 7) and 1 of
   0:
   begin
    case R of
     $06..$0B: Result := Result + 'r32';
     $0C..$0F: Result := Result + 't32';
     else      Result := Result + '32';
    end;
    Result := 'f' + Result + KVM_SpaceChar +
              ARegisters[(W shr 8) and 15] + ',' +
              FloatToStr(Float32);
   end;
   1:
   case (W shr 8) and 1 of
    0:
    begin
     if R in [$06..$0B] then Result := Result + 's';
     Result := 'x' + Result + KVM_SpaceChar +
               ARegisters64[(W shr 9) and 7] + ',' +
               DisIntToStr(Integer32, 1);
    end;
    1:
    begin
     case R of
      $06..$0B: Result := Result + 'r';
      $0C..$0F: Result := Result + 't';
     end;
     Result := 'f' + Result + KVM_SpaceChar +
                 ARegisters64[(W shr 9) and 7] + ',' +
                 FloatToStr(Float32);
    end;
   end;
  end;
 end;
 Result := Result + ',$' + IntToHex(BaseOffset + PSmallInt(PC)^, 8);
end;

function DMemoryOperate(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Integer; PC: PByte;
begin
 PC := Addr(Source);
 W := PInteger(PC)^;
 Size := 4;
 Result := AMemoryOperate[(W shr 16) and 7] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',[' +
           ARegisters[(W shr 11) and 31] + ',' +
           IntToStr(((W shr 19) and $FFF) * -Integer(W < 0)) + ']';
end;

function DMemoryOperateR(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PWord(PC)^;
 Inc(PC, 2);
 W := W or (PC^ shl 16);
 Size := 3;
 Result := AMemoryOperate[(W shr 16) and 7] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',[' +
           ARegisters[(W shr 11) and 31] + ',' +
           ARegisters[W shr 19] + ']';
end;

function DLDMIA_STMIA(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte; Prev, Def: Boolean; I: Integer; Temp: String;
begin
 PC := Addr(Source);
 W := PC^;
 Inc(PC);
 Size := 3;
 Result := ALdmia_Stmia[W and $3F] + KVM_SpaceChar +
           '[' + ARegisters[W shr 6] + ']!,';
 W := PWord(PC)^;
 If W = 0 then Result := Result + ARegisters[20] Else
 begin
  Temp := ''; Prev := False; Def := False;
  For I := 4 to 19 do
  begin
   If W and 1 <> 0 then
   begin
    If not Prev or (I = 19) then
    begin
     Temp := Temp + ARegisters[I];
     Prev := True;
    end Else
    If not Def then
    begin
     Def := True;
     Temp := Temp + '-';
    end;
   end Else If Prev then
   begin
    If Def then
     Temp := Temp + ARegisters[I - 1] Else
    If W <> 0 then
     Temp := Temp + ',' Else Break;
    Prev := False;
   end;
   W := W shr 1;
  end;
  Result := Result + Temp;
 end;
end;

function DCall(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PC^ shr 6;
 Result := ACall[W] + KVM_SpaceChar;
 Inc(PC);
 Case W of
  0, 2:
  begin
   Size := 5;
   Result := Result + '$' + IntToHex(BaseOffset + PInteger(PC)^, 8);
  end;
  1:
  begin
   Size := 3;
   Result := Result + '$' + IntToHex(BaseOffset + PSmallInt(PC)^, 8);
  end;
  3:
  begin
   Size := 5;
   W := PWord(PC)^;
   Inc(PC, 2);
   Result := Result + DisIntToStr(W, 4) + ',' +
                      DisIntToStr(PWord(PC)^, 4);
  end;
 end;
end;

function DOther(BaseOffset: Integer; const Source; var Size: Integer): String;
Var
 W, R: Cardinal; PC: PByte; Prev, Def, Push: Boolean;
 I: Integer; Temp: String;
begin
 PC := Addr(Source);
 W := PC^ shr 6;
 If W and 1 = 0 then //push, pop
 begin
  Size := 4;
  Push := W shr 1 = 0;
  Result := ALstPushPop[W shr 1] + KVM_SpaceChar;
  W := PCardinal(PC)^ shr 8;
  if W = 0 then Result := Result + ARegisters[0] + '-' + ARegisters[7] else
  begin
   Temp := ''; Prev := False; Def := False;
   If Push then For I := 8 to 31 do
   begin
    If W and 1 <> 0 then
    begin
     If not Prev or (I = 31) then
     begin
      Temp := Temp + ARegisters[I];
      Prev := True;
     end Else
     If not Def then
     begin
      Def := True;
      Temp := Temp + '-';
     end;
    end Else If Prev then
    begin
     If Def then
      Temp := Temp + ARegisters[I - 1] Else
      If W <> 0 then
       Temp := Temp + ',' Else Break;
     Prev := False;
    end;
    W := W shr 1;
   end Else For I := 31 downto 8 do
   begin
    If W and 1 <> 0 then
    begin
     If not Prev or (I = 8) then
     begin
      Temp := ARegisters[I] + Temp;
      Prev := True;
     end Else
     If not Def then
     begin
      Def := True;
      Temp := '-' + Temp;
     end;
    end Else If Prev then
    begin
     If Def then
      Temp := ARegisters[I - 1] + Temp Else
      If W <> 0 then
       Temp := ',' + Temp Else Break;
     Prev := False;
    end;
    W := W shr 1;
   end;
   Result := Result + Temp;
  end;
 end Else If W shr 1 = 0 then
 begin
  Size := 2;
  Inc(PC);
  W := PC^;
  Result := AOther[W and 7] + KVM_SpaceChar + ARegisters[W shr 3];
 end Else
 begin
  Size := 4;
  W := PCardinal(PC)^;
  R := (W shr 8) and 3;
  Result := ASeq_Sne[R shr 1] + KVM_SpaceChar +
            ARegisters[(W shr 10) and 31] + ',' +
            ARegisters[(W shr 15) and 31] + ',';
  If Odd(R) then
  begin
   R := (W shr 25) and 63;
   If W shr 31 <> 0 then R := R or Cardinal(not 63);
   Result := Result + ARegisters[(W shr 20) and 31] + ',' + DisIntToStr(R, 2);
  end Else
  begin
   R := (W shr 20) and 2047;
   If W shr 31 <> 0 then R := R or Cardinal(not 2047);
   Result := Result + DisIntToStr(R, 4);
  end;
 end;
end;

function DAriphmetic64(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PWord;
begin
 PC := Addr(Source);
 W := PC^;
 Size := 2;
 Result := AAriphmetic64[(W shr 6) and 15] + KVM_SpaceChar +
            ARegisters64[(W shr 10) and 7] + ',' +
            ARegisters64[W shr 13];
end;

function DLogic64(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PWord;
begin
 PC := Addr(Source);
 W := PC^;
 Size := 2;
 Result := ALogic64[(W shr 6) and 3] + KVM_SpaceChar +
       ARegisters64[(W shr 8) and 7] + ',' +
         ARegisters[W shr 11];
end;

function DCompare64(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W, R: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PC^;
 Inc(PC);
 W := W or (PWord(PC)^ shl 8);
 Size := 3;
 R := (W shr 6) and 7;
 If R < 6 then
 Case (W shr 17) and 1 of
  0: Result := ACompare64[R] + KVM_SpaceChar +
               ARegisters[(W shr 9) and 31] + ',' +
             ARegisters64[(W shr 14) and 7] + ',' +
             ARegisters64[(W shr 18) and 7];
  Else
  Case (W shr 18) and 1 of
   0: Result := ACompare64[R] + KVM_SpaceChar +
                ARegisters[(W shr 9) and 31] + ',' +
              ARegisters64[(W shr 14) and 7] + ',' +
                ARegisters[W shr 19];
  {1: }else
      Result := ACompare64s[R] + KVM_SpaceChar +
                ARegisters[(W shr 9) and 31] + ',' +
              ARegisters64[(W shr 14) and 7] + ',' +
                ARegisters[W shr 19];
  end;
 end Else
 begin
  Result := ACompare64[R] + KVM_SpaceChar +
            ARegisters[(W shr 9) and 31] + ',' +
          ARegisters64[(W shr 14) and 7] + ',';
  R := W shr 18;
  If (W shr 17) and 1 = 1 then R := R or Cardinal(not 63);
  Result := Result + DisIntToStr(R, 2);
 end;
end;

function DFloat32(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PC^;
 Inc(PC);
 W := W or (PWord(PC)^ shl 8);
 Size := 3;
 Result := AFloat32[W shr 21] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 31] + ',' +
           ARegisters[(W shr 11) and 31] + ',' +
           ARegisters[(W shr 16) and 31];
end;

function DFloat32a(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PWord;
begin
 PC := Addr(Source);
 W := PC^;
 Size := 2;
 Result := AFloat32a[(W shr 6) and 7] + KVM_SpaceChar +
           ARegisters[(W shr 9) and 7] + ',' +
           ARegisters[(W shr 12) and 15];
end;

function DFloat64(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PWord;
begin
 PC := Addr(Source);
 W := PC^;
 Size := 2;
 Result := AFloat64[(W shr 6) and 15] + KVM_SpaceChar +
           ARegisters64[(W shr 10) and 7] + ',' +
           ARegisters64[(W shr 13) and 7];
end;

function DFloatCompare(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PWord;
begin
 PC := Addr(Source);
 W := PC^;
 Size := 2;
 Result := AFloatCompare[(W and 63) - $36] + KVM_SpaceChar +
           ARegisters[(W shr 6) and 7] + ',' +
           ARegisters64[(W shr 9) and 7] + ',';
 If (W shr 12) and 1 = 0 then
  Result := Result + ARegisters64[W shr 13] Else
  Result := Result + ARegisters[W shr 13];
end;

function DSystem(BaseOffset: Integer; const Source; var Size: Integer): String;
Var W: Cardinal; PC: PByte;
begin
 PC := Addr(Source);
 W := PC^ shr 6;
 Size := 1;
 Result := ASystem[W];
end;

function DUnknown(BaseOffset: Integer; const Source; var Size: Integer): String;
begin
 Result := '????';
end;

Const
 PtrTable: Array[0..63] of TKVMD =
(//$00-$0C
 DAriphmetic, DAriphmetic, DAriphmetic, DAriphmetic,
 DAriphmetic, DAriphmetic, DAriphmetic, DAriphmetic, DAriphmetic,
 DAriphmetic, DAriphmetic, DAriphmetic, DAriphmetic,
 //$0D
 DLogic,
 //$0E
 DCompare,
 //$0F
 DCompareR,
 //$10-$1F
 DAriphmeticR, DAriphmeticR, DAriphmeticR, DAriphmeticR,
 DAriphmeticR, DAriphmeticR, DAriphmeticR, DAriphmeticR,
 DAriphmeticR, DAriphmeticR, DAriphmeticR, DAriphmeticR,
 DAriphmeticR, DAriphmeticR, DAriphmeticR, DAriphmeticR,
 //$20-$29
 DConditionalBranch, DConditionalBranch,
 DConditionalBranch, DConditionalBranch,
 DConditionalBranch, DConditionalBranch,
 DConditionalBranch, DConditionalBranch,
 DConditionalBranch, DConditionalBranch,
 //$2A
 DMemoryOperate,
 //$2B
 DMemoryOperateR,
 //$2C-$2D
 DLDMIA_STMIA, DLDMIA_STMIA,
 //$2E
 DCall,
 //$2F
 DOther,
 //$30
 DAriphmetic64,
 //$31
 DLogic64,
 //$32
 DCompare64,
 //$33
 DFloat32,
 //$34
 DFloat32a,
 //$35
 DFloat64,
 //$36-$3B
 DFloatCompare, DFloatCompare, DFloatCompare,
 DFloatCompare, DFloatCompare, DFloatCompare,
 //$3D-$3E
 DConditionalBranch2,
 DUnknown, DUnknown,
 //$3F
 DSystem);

function KVM_Disasm(BaseOffset: Integer; const Source; var Size: Integer): String;
begin
 Result := PtrTable[Cardinal(Byte(Source)) and $3F](BaseOffset, Source, Size);
end;

end.
