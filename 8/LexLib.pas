(* Standard Lex library unit for TP Lex Version 3.0.
   2-11-91 AG *)

(* Modified by S.A., 23.04.2008 *)

(* The Lex library unit supplies a collection of variables and routines
   needed by the lexical analyzer routine yylex and application programs
   using Lex-generated lexical analyzers. It also provides access to the
   input/output streams used by the lexical analyzer and the text of the
   matched string, and provides some utility functions which may be used
   in actions.

   This `standard' version of the LexLib unit is used to implement lexical
   analyzers which read from and write to MS-DOS files (using standard input
   and output, by default). It is suitable for many standard applications
   for lexical analyzers, such as text conversion tools or compilers.

   However, you may create your own version of the LexLib unit, tailored to
   your target applications. In particular, you may wish to provide another
   set of I/O functions, e.g., if you want to read from or write to memory
   instead to files, or want to use different file types. *)

(* Variables:

   The variable yytext contains the current match, yyleng its length.
   The variable yyline contains the current input line, and yylineno and
   yycolno denote the current input position (line, column). These values
   are often used in giving error diagnostics (however, they will only be
   meaningful if there is no rescanning across line ends).

   The variables yyinput and yyoutput are the text files which are used
   by the lexical analyzer. By default, they are assigned to standard
   input and output, but you may change these assignments to fit your
   target application (use the Turbo Pascal standard routines assign,
   reset, and rewrite for this purpose). *)

procedure TLexer.fatal(const Msg: String);
begin
 If Assigned(FOwner) then With FOwner do
 begin
  If Assigned(FOnErrorMessage) then
   FOnErrorMessage(Self, Format('%s: %s', [LYS_FatalError, Msg]));
  ErrorCode := LYE_FatalError;
 end;
end;

(* I/O routines: *)

function TLexer.get_char: Char;
Var I, L: Integer; P, PB: PChar;
begin
 With FOwner do
 If (bufptr = 0) and (FLineNumber < Input.Count) then
 begin
  FLine := Input[FLineNumber];
  Inc(FLineNumber);
  FColNumber := 1;
  buf[1] := #10;
  L := Length(FLine);
  Inc(bufptr, L + 1);
  If L > 0 then
  begin
   P := Pointer(FLine);
   Inc(P, L - 1);
   PB := Addr(buf[2]);
   for I := 1 to L do
   begin
    PB^ := P^;
    Inc(PB);
    Dec(P);
   end;
  end;
 end;
 If bufptr > 0 then
 begin
  Result := buf[bufptr];
  Dec(bufptr);
  Inc(FColNumber);
 end else Result := #0;
end;

procedure TLexer.unget_char(C: Char);
begin
 If bufptr >= max_chars then fatal(LYS_InputBufferOverflow) Else
 begin
  Inc(bufptr);
  Dec(FColNumber);
  buf[bufptr] := C;
 end;
end;

procedure TLexer.put_char(C: Char);
begin
 Case C of
  #10:
  begin
   DebugMessage(FTempStr);
   FTempStr := '';
  end;
  #0..#8, #11..#31: { ignore };
  Else FTempStr := FTempStr + C;
 end;
end;

(* Utilities: *)

procedure TLexer.echo;
Var P: PChar;
begin
 P := Pointer(FText);
 If P <> NIL then While P^ <> #0 do
 begin
  put_char(P^);
  Inc(P);
 end;
end;

procedure TLexer.yymore;
begin
 yystext := FText;
end;

procedure TLexer.yyless(N: Integer);
Var I: Integer;
begin
 For I := Length(FText) downto N + 1 do
  unget_char(FText[I]);
 SetLength(FText, N);
end;

procedure TLexer.reject;
Var I: Integer;
begin
 FReject := True;
 For I := Length(FText) + 1 to yysleng do
  FText := FText + get_char;
 Dec(yymatches);
end;

procedure TLexer.return(N: Integer);
begin
 FRetVal := N;
 FDone := True;
end;

procedure TLexer.returnc(C: Char);
begin
 FRetVal := Byte(C);
 FDone := True;
end;

procedure TLexer.start(State: Integer);
begin
 yysstate := State;
end;

(* yywrap: *)

function TLexer.yywrap : Boolean;
begin
 Result := True;
end;

(* Internal routines: *)

procedure TLexer.yynew;
begin
 If FLastChar <> #0 then
 begin
  If FLastChar = #10 then
   yylstate := 1 else
   yylstate := 0;
 end;
 FState := yysstate + yylstate;
 FText := yystext;
 yystext := '';
 yymatches := 0;
 FDone := False;
end;

procedure TLexer.yyscan;
begin
 FActChar := get_char;
 FText := FText + FActChar;
end;

procedure TLexer.yymark(N: Integer);
begin
 If N > max_rules then
  fatal(LYS_TooManyRules) Else
  yypos[n] := Length(FText);
end;

procedure TLexer.yymatch(N: Integer);
begin
 Inc(yymatches);
 If yymatches > max_matches then
  fatal(LYS_MatchStackOverflow) Else
  yystack[yymatches] := N;
end;

function TLexer.yyfind(var N: Integer): Boolean;
Var L: Integer;
begin
 FReject := false;
 While (yymatches > 0) and (yypos[yystack[yymatches]] = 0) do Dec(yymatches);
 If yymatches > 0 then
 begin
  yysleng := Length(FText);
  N := yystack[yymatches];
  yyless(yypos[N]);
  yypos[N] := 0;
  L := Length(FText);
  If L > 0 then
   FLastChar := FText[L] else
   FLastChar := #0;
  Result := true;
 end else
 begin
  yyless(0);
  FLastChar := #0;
  Result := false;
 end;
end;

function TLexer.yydefault: Boolean;
begin
 FReject := False;
 FActChar := get_char;
 If FActChar <> #0 then
 begin
  put_char(FActChar);
  Result := true;
 end else
 begin
  yylstate := 1;
  Result := false;
 end;
 FLastChar := FActChar;
end;

procedure TLexer.yyclear;
begin
 FillChar(buf, SizeOf(buf), 0);
 FillChar(yystack, SizeOf(yystack), 0);
 FillChar(yypos, SizeOf(yypos), 0);
 bufptr := 0;
 yysstate := 0;
 yylstate := 1;
 FLastChar := #0;
 FText := '';
 yystext := '';
 FTempStr := '';
 FLine := '';
 FLineNumber := 0;
end;

constructor TLexer.Create(AOwner: TCustomParser);
begin
 FOwner := AOwner;
 yyClear;
end;

destructor TLexer.Destroy;
begin
 Finalize(FTempStr);
 Finalize(FLine);
 Finalize(FText);
 Finalize(yystext);
 inherited;
end;
