unit RegExpr;

interface

uses
 Regex;

type
 TRegExprFlag = (
  ref_singleline,
     {** This indicates that a start of line is either the
         start of the pattern or a linebreak. }
  ref_multiline,
     {** The match will be done in a case-insensitive way
          according to US-ASCII character set. }
  ref_caseinsensitive);

 tregexprflags = set of TRegExprFlag;

 TRegExprEngine = TRegexEngine;

function GenerateRegExprEngine(regexpr: PWideChar; Flags: tregexprflags;
   var RegExprEngine: TRegExprEngine): Boolean;
procedure DestroyRegExprEngine(var regexpr: TRegExprEngine);

function RegExprPos(RegExprEngine: TRegExprEngine; const p: WideString;
   var index, len: longint): Boolean;
function RegExprReplaceAll(RegExprEngine: TRegExprEngine;
 const src, newstr: WideString; var dest: WideString): Integer;

function RegExprEscapeStr(const S: WideString): WideString;

implementation

function GenerateRegExprEngine(regexpr: pWidechar; flags: tregexprflags;
  var RegExprEngine: TRegExprEngine): boolean;
var ErrorPos  : Integer;
    ErrorCode : TRegexError;
begin
  RegExprEngine := TRegExprEngine.Create(regexpr);
  if ref_multiline in flags then RegExprEngine.MultiLine:=True;
  if ref_caseinsensitive in flags then RegExprEngine.IgnoreCase:=True;
  Result := RegExprEngine.Parse(ErrorPos,ErrorCode);
end;

procedure DestroyRegExprEngine(var regexpr: TRegExprEngine);
begin
  regexpr.Free;
end;

function RegExprPos(RegExprEngine: TRegExprEngine; const p: WideString; var index,
  len: longint): boolean;
begin
 Len := Length(P);
 Index := RegExprEngine.MatchString(Len, Pointer(P));
 Result := Index >= 0;
//  Len := 1;
//  Result := RegExprEngine.MatchString(p,index,len);
//  Len := Len - index;
//  Dec(Index);
end;

function RegExprReplaceAll(RegExprEngine: TRegExprEngine; const src,
  newstr: widestring; var dest: widestring): Integer;
begin
  result := RegExprEngine.ReplaceAllString(src,newstr,Dest);
end;

function RegExprEscapeStr(const S: widestring): widestring;
var
  i, len   : integer;
begin
 Result := '';
 if S = '' then exit;

 len := Length(S);

 for i := 1 to len do
 begin
  case S[i] of
   '(','|', '.', '*', '?', '^', '$', '-', '[', '{', '}', ']', ')', '\':
    Result := Result + '\';
  end;
  Result := Result + S[i];
 end;
end;

end.
