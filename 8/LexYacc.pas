unit LexYacc;

(* Yacc Library Unit for TP Yacc Version 3.0, 6-17-91 AG *)

(* Modified by S.A., 23.04.2008 *)

interface

Uses SysUtils, Classes;

const
 yymaxdepth = 1024;
 (* default stack size of parser *)
 max_matches = 1024;
 max_rules   = 256;
 max_chars = 2048;

type
 YYSType = Integer;
 (* default value type, may be redefined in Yacc output file *)
 TYacc = Class;
 TLexer = Class;
 TYaccClass = Class of TYacc;
 TLexerClass = Class of TLexer;
 TStringsClass = class of TStrings;

 TMessageEvent = procedure(Sender: TObject; const Msg: String) of Object;

 TCustomParser = Class
  private
    FYaccClass: TYaccClass;
    FLexerClass: TLexerClass;
    FStringsClass: TStringsClass;
    FInput: TStrings;
    FErrorCode: Integer;
    FLexer: TLexer;
    FYacc: TYacc;
    FOnDebugMessage: TMessageEvent;
    FOnErrorMessage: TMessageEvent;
    FExternalInput: Boolean;
    procedure SetInput(Value: TStrings);
    function GetDebugMode: Boolean;
    procedure SetDebugMode(Value: Boolean);
  public
    property DebugMode: Boolean read GetDebugMode write SetDebugMode;
    property StringsClass: TStringsClass read FStringsClass
                                        write FStringsClass;
    property ErrorCode: Integer read FErrorCode write FErrorCode;
    property Input: TStrings read FInput write SetInput;
    property Yacc: TYacc read FYacc;
    property Lexer: TLexer read FLexer;

    property OnDebugMessage: TMessageEvent read FOnDebugMessage
                                          write FOnDebugMessage;
    property OnErrorMessage: TMessageEvent read FOnErrorMessage
                                          write FOnErrorMessage;
    procedure Initialize; virtual;
    constructor Create; overload;
    constructor Create(YaccClass: TYaccClass;
                       LexerClass: TLexerClass); overload;
    constructor Create(StringsClass: TStringsClass;
                       YaccClass: TYaccClass;
                       LexerClass: TLexerClass); overload;
    destructor Destroy; override;
    function Parse: Boolean;
 end;

 TYacc = class
  private
    FOwner: TCustomParser;
  public
    yysp: Integer;
    yys: array [1..yymaxdepth] of Integer;
    yyChar: Integer;
    (* current lookahead character *)
    yyNErrs: Integer;
    (* current number of syntax errors reported by the parser *)
    yyDebug: Boolean;
    (* set to true to enable debugging output of parser *)
    property yyOwner: TCustomParser read FOwner;

    procedure yyerror(const Msg: String);
    (* error message printing routine used by the parser *)
    procedure yyclearin;
    (* delete the current lookahead token *)
    procedure yyaccept;
    (* trigger accept action of the parser; yyparse accepts returning 0, as if
       it reached end of input *)
    procedure yyabort;
    (* like yyaccept, but causes parser to return with value 1, as if an
      unrecoverable syntax error had been encountered *)
    procedure yyerrlab;
    (* causes error recovery to be started, as if a syntax error had been
       encountered *)
    procedure yyerrok;
    (* when in error mode, resets the parser to its normal mode of
       operation *)
    function yyparse: Integer; virtual;
    constructor Create(AOwner: TCustomParser);
    destructor Destroy; override;
    procedure DebugMessage(const Msg: String);
 end;

 TLexer = Class
  private
    FOwner: TCustomParser;
    FLineNumber: Integer;
    FColNumber: Integer;
    FState: Integer;
    FActChar: Char;
    FLastChar: Char;
    FReject: Boolean;
    FDone: Boolean;
    FRetVal: Integer;
    FTempStr: String;
    FLine: String;
    FText: String;
    (* For I/O routines *)
    bufptr: Integer;
    buf: Array[1..max_chars] of Char;
(* Variables:

   Some state information is maintained to keep track with calls to yymore,
   yyless, reject, start and yymatch/yymark, and to initialize state
   information used by the lexical analyzer.
   - yystext: contains the initial contents of the yytext variable; this
     will be the empty string, unless yymore is called which sets yystext
     to the current yytext
   - yysstate: start state of lexical analyzer (set to 0 during
     initialization, and modified in calls to the start routine)
   - yylstate: line state information (1 if at beginning of line, 0
     otherwise)
   - yystack: stack containing matched rules; yymatches contains the number of
     matches
   - yypos: for each rule the last marked position (yymark); zeroed when rule
     has already been considered
   - yysleng: copy of the original yyleng used to restore state information
     when reject is used *)
    yystext: String;
    yysstate: Integer;
    yylstate: Integer;
    yymatches: Integer;
    yystack: Array [1..max_matches] of Integer;
    yypos: Array [1..max_rules] of Integer;
    yysleng: Integer;
  public
    yyRule: Integer;
    (* matched rule *)
    property yyOwner: TCustomParser read FOwner;
    property yyLine: String read FLine write FLine;
    (* current input line *)
    property yyLineNo: Integer read FLineNumber write FLineNumber;
    property yyColNo: Integer read FColNumber write FColNumber;
    (* current input position *)
    property yyText: String read FText write FText;
    (* matched text (should be considered r/o) *)
    (* I/O routines:

       The following routines get_char, unget_char and put_char are used to
       implement access to the input and output files. Since \n (newline) for
       Lex means line end, the I/O routines have to translate MS-DOS line ends
       (carriage-return/line-feed) into newline characters and vice versa. Input
       is buffered to allow rescanning text (via unput_char).

       The input buffer holds the text of the line to be scanned. When the input
       buffer empties, a new line is obtained from the input stream. Characters
       can be returned to the input buffer by calls to unget_char. At end-of-
       file a null character is returned.

       The input routines also keep track of the input position and set the
       yyline, yylineno, yycolno variables accordingly.

       Since the rest of the Lex library only depends on these three routines
       (there are no direct references to the yyinput and yyoutput files or
       to the input buffer), you can easily replace get_char, unget_char and
       put_char by another suitable set of routines, e.g. if you want to read
       from/write to memory, etc. *)
    function get_char: Char;
    (* obtain one character from the input file (null character at end-of-
       file) *)
    procedure unget_char(C: Char);
    (* return one character to the input file to be reread in subsequent calls
       to get_char *)
    procedure put_char(C: Char);
    (* write one character to the output file *)
    (* Utility routines: *)
    procedure echo;
    (* echoes the current match to the output stream *)
    procedure yymore;
    (* append the next match to the current one *)
    procedure yyless(N: Integer);
    (* truncate yytext to size n and return the remaining characters to the
       input stream *)
    procedure reject;
    (* reject the current match and execute the next one *)
    (* reject does not actually cause the input to be rescanned; instead,
       internal state information is used to find the next match. Hence
       you should not try to modify the input stream or the yytext variable
       when rejecting a match. *)
    procedure return(N: Integer);
    procedure returnc(C: Char);
    (* sets the return value of yylex *)
    procedure start(State: Integer);
    (* puts the lexical analyzer in the given start state; state=0 denotes
       the default start state, other values are user-defined *)

    (* yywrap:

       The yywrap function is called by yylex at end-of-file (unless you have
       specified a rule matching end-of-file). You may redefine this routine
       in your Lex program to do application-dependent processing at end of
       file. In particular, yywrap may arrange for more input and return false
       in which case the yylex routine resumes lexical analysis. *)
    function yywrap: Boolean;
    (* The default yywrap routine supplied here closes input and output files
       and returns true (causing yylex to terminate). *)
    (* The following are the internal data structures and routines used by the
       lexical analyzer routine yylex; they should not be used directly. *)
    property yyState: Integer read FState write FState;
    (* current state of lexical analyzer *)
    property yyActChar: Char read FActChar write FActChar;
    (* current character *)
    property yyLastChar: Char read FLastChar write FLastChar;
    (* last matched character (#0 if none) *)
    property yyReject: Boolean read FReject write FReject;
    (* current match rejected? *)
    property yyDone: Boolean read FDone write FDone;
    (* yylex return value set? *)
    property yyRetVal: Integer read FRetVal write FRetVal;
    (* yylex return value *)
    procedure yynew;
    (* starts next match; initializes state information of the lexical
       analyzer *)
    procedure yyscan;
    (* gets next character from the input stream and updates yytext and
       yyactchar accordingly *)
    procedure yymark(n: Integer);
    (* marks position for rule no. n *)
    procedure yymatch(n: Integer);
    (* declares a match for rule number n *)
    function yyfind (var n: Integer): Boolean;
    (* finds the last match and the corresponding marked position and adjusts
       the matched string accordingly; returns:
       - true if a rule has been matched, false otherwise
       - n: the number of the matched rule *)
    function yydefault: Boolean;
    (* executes the default action (copy character); returns true unless
       at end-of-file *)
    procedure yyclear;
    (* reinitializes state information after lexical analysis has been
       finished *)
    procedure fatal(const Msg: String);
    (* writes a fatal error message and halts program *)
    constructor Create(AOwner: TCustomParser);
    destructor Destroy; override;
    procedure DebugMessage(const Msg: String);
    function yylex: Integer; virtual;
 end;

(* Flags used internally by the parser routine: *)
Var
 yyflag: (yyfnone, yyfaccept, yyfabort, yyferror);
 yyerrflag: Integer;
 (* String constants *)
 LYS_ParserError: String = 'Parser Error';
 LYS_StackOverflow: String = 'Stack overflow';
 LYS_FatalError: String = 'Lex Lib Fatal Error';
 LYS_InputBufferOverflow: String = 'Input buffer overflow';
 LYS_TooManyRules: String = 'Too many rules';
 LYS_MatchStackOverflow: String = 'Match stack overflow';
 LYS_State: String = 'state';
 LYS_Char: String = 'char';
 LYS_Reduce: String = 'reduce';
 LYS_SyntaxError: String = 'Syntax error';
 LYS_ERPops: String = 'Error recovery pops state %d, uncovers %d';
 LYS_ERFails: String = 'Error recovery fails... Abort';
 LYS_ERDiscards: String = 'Error recovery discards char %d';

const
 (* Error codes *)
 LYE_CustomError = -2; 
 LYE_FatalError = -1;
 LYE_OK = 0;
 LYE_SyntaxError = 1;
 LYE_ERDiscards = 2;
 LYE_StackOverflow = 3;
 LYE_FallenFromStack = 4;

implementation

procedure TYacc.yyerror(const Msg: String);
begin
 If Assigned(FOwner) then With FOwner do
 If Assigned(FOnErrorMessage) then
  FOnErrorMessage(Self, Format('%s: %s', [LYS_ParserError, Msg]));
end;

procedure TYacc.yyclearin;
begin
 yyChar := -1;
end;

procedure TYacc.yyaccept;
begin
 yyflag := yyfaccept;
end;

procedure TYacc.yyabort;
begin
 yyflag := yyfabort;
end;

procedure TYacc.yyerrlab;
begin
 yyflag := yyferror;
end;

procedure TYacc.yyerrok;
begin
 yyerrflag := 0;
end;

constructor TYacc.Create(AOwner: TCustomParser);
begin
 FOwner := AOwner;
end;

destructor TYacc.Destroy;
begin
 inherited;
end;

constructor TCustomParser.Create;
begin
 FStringsClass := TStringList;
 FYaccClass := TYacc;
 FLexerClass := TLexer;
 FInput := FStringsClass.Create;
 Initialize;
end;

constructor TCustomParser.Create(YaccClass: TYaccCLass;
                                 LexerClass: TLexerClass);
begin
 FStringsClass := TStringList;
 FYaccClass := YaccClass;
 FLexerClass := LexerClass;
 FInput := FStringsClass.Create;
 Initialize;
end;

constructor TCustomParser.Create(StringsClass: TStringsClass;
                                 YaccClass: TYaccCLass;
                                 LexerClass: TLexerClass);
begin
 FStringsClass := StringsClass;
 FYaccClass := YaccClass;
 FLexerClass := LexerClass;
 FInput := FStringsClass.Create;
 Initialize;
end;

procedure TCustomParser.Initialize;
begin
 FYacc := FYaccClass.Create(Self);
 FLexer := FLexerClass.Create(Self);
end;

destructor TCustomParser.Destroy;
begin
 FLexer.Free;
 FYacc.Free;
 If not FExternalInput then FInput.Free;
 inherited;
end;

procedure TCustomParser.SetInput(Value: TStrings);
begin
 If Value <> NIL then
 begin
  FExternalInput := True;
  FInput.Free;
  FInput := Value;
 end else if FExternalInput then
 begin
  FExternalInput := False;
  FInput := FStringsClass.Create;
 end;
end;

function TCustomParser.Parse: Boolean;
begin
 Result := FYacc.yyparse = 0;
end;

procedure TYacc.DebugMessage(const Msg: String);
begin
 If Assigned(FOwner) then With FOwner do
 If Assigned(FOnDebugMessage) then
  FOnDebugMessage(Self, Msg);
end;

procedure TLexer.DebugMessage(const Msg: String);
begin
 If Assigned(FOwner) then With FOwner do
 If Assigned(FOnDebugMessage) then
  FOnDebugMessage(Self, Msg);
end;

{$I LEXLIB.PAS}

function TCustomParser.GetDebugMode: Boolean;
begin
 Result := FYacc.yyDebug;
end;

procedure TCustomParser.SetDebugMode(Value: Boolean);
begin
 FYacc.yyDebug := Value;
end;

function TYacc.yyparse: Integer;
begin
 Result := 0;
end;

function TLexer.yylex: Integer;
begin
 Result := 0;
end;

end.
