object Form1: TForm1
  Left = 325
  Top = 165
  Width = 870
  Height = 500
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RegistersList: TValueListEditor
    Left = 709
    Top = 0
    Width = 153
    Height = 466
    Align = alRight
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goRowSelect, goThumbTracking]
    ParentFont = False
    Strings.Strings = (
      '=')
    TabOrder = 0
    ColWidths = (
      53
      94)
  end
  object CodeMemo: TSynMemo
    Left = 0
    Top = 0
    Width = 709
    Height = 466
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    TabOrder = 1
    Gutter.AutoSize = True
    Gutter.DigitCount = 8
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    Lines.WideStrings = 
      'mov r0,$256'#13#10'shl r1,r0,2'#13#10'add r0,r1'#13#10'mul r1,r0'#13#10'push r1'#13#10'pop r0'#13 +
      #10'halt'
    ReadOnly = True
    ScrollBars = ssVertical
    SelectionMode = smLine
  end
end
