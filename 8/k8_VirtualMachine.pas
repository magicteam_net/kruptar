unit  k8_VirtualMachine;

interface

Uses SysUtils, NodeLst;

Type
 TKruptarVirtualMethod = Class;
 TKruptarVirtualModule = Class;
 TKruptarVirtualMachine = Class;

 TKruptarVirtualRegisters = Packed Record
  Case Byte of
   0: (C: Array[0..31] of Cardinal);
   1: (I: Array[0..31] of Integer);
   2: (I64: Array[0..15] of Int64);
   3: (r0: Cardinal;
       r1: Cardinal;
       r2: Cardinal;
       r3: Cardinal;
       r4: Cardinal;
       r5: Cardinal;
       r6: Cardinal;
       r7: Cardinal;
       r8: Cardinal;
       r9: Cardinal;
       r10: Cardinal;
       r11: Cardinal;
       r12: Cardinal;
       r13: Cardinal;
       r14: Cardinal;
       r15: Cardinal;
       r16: Cardinal;
       r17: Cardinal;
       r18: Cardinal;
       r19: Cardinal;
       r20: Cardinal;
       r21: Cardinal;
       r22: Cardinal;
       r23: Cardinal;
       ESP: Pointer;
       EMP: Pointer;
       EPP: Pointer;
       SP: PCardinal;
       RMP: TKruptarVirtualMethod;
       RPP: Pointer;
       MP: TKruptarVirtualMethod;
       PP: Pointer);
   4: (xr0: Int64;
       xr1: Int64;
       xr2: Int64;
       xr3: Int64;
       xr4: Int64;
       xr5: Int64;
       xr6: Int64;
       xr7: Int64);
 end;
 TCallingConvention = (ccNone,
                       ccRegister, ccPascal, ccCdecl, ccStdCall, ccSafeCall);

 TKruptarVirtualField = Class(TNode)
  private
    FModule: TKruptarVirtualModule;
    FName: String;
    FOriginalName: String;
  public
    property Name: String read FName write FName;
    property OriginalName: String read FOriginalName write FOriginalName;
    constructor Create(AOwner: TKruptarVirtualModule);
 end;

 TKruptarVirtualMethod = Class(TKruptarVirtualField)
  private
    FParamCount: Integer;
    FIsFunction: Boolean;
    FCallingConvention: TCallingConvention;
    FCode: Pointer;
    FCodeSize: Integer;
    function GetCode: Pointer;
    procedure SetCodeSize(Value: Integer);
    function GetExternalProc: Pointer;
    procedure SetExternalProc(Value: Pointer);
    procedure SetCallingConvention(Value: TCallingConvention);
  public
    property Code: Pointer read GetCode;
    property CodeSize: Integer read FCodeSize write SetCodeSize;
    property ParamCount: Integer read FParamCount;
    property IsFunction: Boolean read FIsFunction;
    property CallingConvention: TCallingConvention read FCallingConvention
                                                  write SetCallingConvention;
    property ExternalProc: Pointer read GetExternalProc write SetExternalProc;

    destructor Destroy; override;
    procedure Init(PCnt: Integer; IsFunc: Boolean; CC: TCallingConvention);
 end;

 TKruptarVirtualVariable = Class(TKruptarVirtualField)
  private
    FHeapOffset: Integer;
    FVarType: Word;
    function GetVariant: Variant;
    procedure SetVariant(Value: Variant);
  public
    property Data: Variant read GetVariant write SetVariant;
    property VarType: TVarType read FVarType write FVarType;
    property HeapOffset: Integer read FHeapOffset write FHeapOffset;
 end;

 TKruptarVirtualFields = Class(TNodeList)
  private
    function GetByName(const Name: String): TKruptarVirtualField;
    function GetMethodByIndex(Index: Integer): TKruptarVirtualMethod;
    function GetMethodByName(const Name: String): TKruptarVirtualMethod;
    function GetVarByIndex(Index: Integer): TKruptarVirtualVariable;
    function GetVarByName(const Name: String): TKruptarVirtualVariable;
  protected
    procedure Initialize; override;
  public
    property MethodItems[Index: Integer]: TKruptarVirtualMethod
        read GetMethodByIndex;
    property MethodNames[const Name: String]: TKruptarVirtualMethod
        read GetMethodByName;
    property VarItems[Index: Integer]: TKruptarVirtualVariable
        read GetVarByIndex;
    property VarNames[const Name: String]: TKruptarVirtualVariable
        read GetVarByName;

    constructor Create(AOwner: TKruptarVirtualModule);
    function AddMethod: TKruptarVirtualMethod;
    function AddVariable: TKruptarVirtualVariable;
 end;

 THeap = Array of Byte;
 TUses = Array of TKruptarVirtualModule;

 TKruptarVirtualModule = Class(TNode)
  private
    FUses: TUses;
    FHeap: THeap;
    FName: String;
    FOriginalName: String;
    FMethods: TKruptarVirtualFields;
    FInitialization: TKruptarVirtualMethod;
    FFinalization: TKruptarVirtualMethod;
    FVariables: TKruptarVirtualFields;
  protected
    procedure Initialize; override;
  public
    property Name: String read FName write FName;
    property OriginalName: String read FOriginalName write FOriginalName;
    property Heap: THeap read FHeap write FHeap;
    property UsedModules: TUses read FUses write FUses;
    property Methods: TKruptarVirtualFields read FMethods;
    property Variables: TKruptarVirtualFields read FVariables;

    constructor Create(AOwner: TKruptarVirtualMachine);
    destructor Destroy; override;
    function FindModule(const Name: String): TKruptarVirtualModule;
 end;

 TInstructionEvent = procedure of Object;

 TKruptarVirtualMachine = class(TNodeList)
  private
    FExecuted: Boolean;
    FPaused: Boolean;
    FEntryPoint: TKruptarVirtualMethod;    
  public
    FRegisters: TKruptarVirtualRegisters;
  private
    FPtrTable: Array[0..63] of TInstructionEvent;
    FMainModule: TKruptarVirtualModule;
    FStack: Array of Cardinal;
    FStackTop: Pointer;
    FLastError: Integer;
    FLastExceptionName: String;
    FLastExceptionMessage: String;
    function GetByIndex(Index: Integer): TKruptarVirtualModule;
    function GetByName(const Name: String): TKruptarVirtualModule;
    function GetStackSize: Integer;
    procedure SetStackSize(Value: Integer);
    //-----------//
    procedure Exec;
    //Instructions
    procedure IAriphmetic;
    {Bit    Expl
     0-5    Opcode
             00: mov  Rd,#Number   ;Rd = #Number
             01: add  Rd,#Number   ;Rd = Rd + #Number
             02: sub  Rd,#Number   ;Rd = Rd - #Number
             03: rsb  Rd,#Number   ;Rd = #Number - Rd
             04: mul  Rd,#Number   ;Rd = Rd * #Number
             05: div  Rd,#Number   ;Rd = Rd div #Number
             06: divs Rd,#Number   ;signed Rd = Rd div #Number
             07: mod  Rd,#Number   ;Rd = Rd mod #Number
             08: rmd  Rd,#Number   ;Rd = #Number mod Rd
             09: and  Rd,#Number   ;Rd = Rd and #Number
             0A: bic  Rd,#Number   ;Rd = Rd and not #Number
             0B: or   Rd,#Number   ;Rd = Rd or #Number
             0C: xor  Rd,#Number   ;Rd = Rd xor #Number
     6-10   Destination register
     11     Flags1
             F1_0:
     12-15   4 bit Number + 2
             F1_1:
     12     Reserved
     13-15  Flags2
             0: Number = 0
             1: 8 bit Number
             2: 16 bit Number
             3: 32 bit Number
             4: Signed 8 bit Number
             5: Signed 16 bit Number
             6: Number = Heap Pointer
             7: Number = 1
     16-47  Number}
    procedure ILogic;
    {Bit    Expl
     0-5    Must be 0D
     6-10   Destination register
     11-15  Source register
     16-18  Opcode
             00: shl Rd,Rs,#Number  ;Rd = Rs shl #Number
             01: shl Rd,Rs,Rn       ;Rd = Rs shl Rn
             02: inc Rd,Rs,#Number  ;Rd = Rs + #Number
             03: inc Rd,Rs,Rn       ;Rd = Rs + Rn
             04: shr Rd,Rs,#Number  ;Rd = Rs shr #Number
             05: shr Rd,Rs,Rn       ;Rd = Rs shr Rn
             06: sar Rd,Rs,#Number  ;Rd = Integer(Rs) shr #Number
             07: sar Rd,Rs,Rn       ;Rd = Integer(Rs) shr Rn
     19-23  Rn - Quantity register (or 5 bit Number if not ADD)
     if ADD:
     19-30  12 bit Number
     31     If bit is set then Number is Negative}
    procedure ICompare;
    {Bit    Expl
     0-5    Must be 0E
     6-10   Destination register
     11-15  Source register
     16-18  Opcode
             00: sgt  Rd,Rs,#Number ;Rd = Byte(Rs >  #Number)
             01: sge  Rd,Rs,#Number ;Rd = Byte(Rs >= #Number)
             02: slt  Rd,Rs,#Number ;Rd = Byte(Rs <  #Number)
             03: sle  Rd,Rs,#Number ;Rd = Byte(Rs <= #Number)
             04: sgts Rd,Rs,#Number ;Rd = Byte(SIGNED(Rs) >  #Number)
             05: sges Rd,Rs,#Number ;Rd = Byte(SIGNED(Rs) >= #Number)
             06: slts Rd,Rs,#Number ;Rd = Byte(SIGNED(Rs) <  #Number)
             07: sles Rd,Rs,#Number ;Rd = Byte(SIGNED(Rs) <= #Number)
     19-30  12 bit Number
     31     If bit is set then Number is Negative}
    procedure ICompareR;
    {Bit    Expl
     0-5    Must be 0F
     6-10   Destination register
     11-15  Source register
     16-18  Opcode
             00: sgt  Rd,Rs,Rn      ;Rd = Byte(Rs >  Rn)
             01: sge  Rd,Rs,Rn      ;Rd = Byte(Rs >= Rn)
             02: slt  Rd,Rs,Rn      ;Rd = Byte(Rs <  Rn)
             03: sle  Rd,Rs,Rn      ;Rd = Byte(Rs <= Rn)
             04: sgts Rd,Rs,Rn      ;Rd = Byte(SIGNED(Rs) >  Signed(Rn))
             05: sges Rd,Rs,Rn      ;Rd = Byte(SIGNED(Rs) >= Signed(Rn))
             06: slts Rd,Rs,Rn      ;Rd = Byte(SIGNED(Rs) <  Signed(Rn))
             07: sles Rd,Rs,Rn      ;Rd = Byte(SIGNED(Rs) <= Signed(Rn)
      19-23  Rn - Quantity register}
    procedure IAriphmeticR;
    {Bit    Expl
     0-5    Opcode
             10: mova  Rd,Rs         ;Rd = Rs
             11: mvna  Rd,Rs         ;Rd = not Rs
             12: adda  Rd,Rs         ;Rd = Rd + Rs
             13: suba  Rd,Rs         ;Rd = Rd - Rs
             14: rsba  Rd,Rs         ;Rd = Rs - Rd
             15: mula  Rd,Rs         ;Rd = Rd * Rs
             16: diva  Rd,Rs         ;Rd = Rd div Rs
             17: divas Rd,Rs         ;signed Rd = Rd div Rs
             18: moda  Rd,Rs         ;Rd = Rd mod Rs
             19: rmda  Rd,Rs         ;Rd = Rs mod Rd
             1A: swapa Rd,Rs         ;Rd = swap bytes order in Rs
             1B: swpha Rd,Rs         ;Rd = swap bytes order in WORD(Rs)
             1C: anda  Rd,Rs         ;Rd = Rd and Rs
             1D: bica  Rd,Rs         ;Rd = Rd and not Rs
             1E: ora   Rd,Rs         ;Rd = Rd or Rs
             1F: xora  Rd,Rs         ;Rd = Rd xor Rs
     6-10   Destination register
     11-15  Source register}
    procedure IConditionalBranch;
    {Bit    Expl
     0-5    Opcode
             20: beqa  R1,R2,#Offset ;R1 = R2
             21: bnea  R1,R2,#Offset ;R1 <> R2
             22: bgta  R1,R2,#Offset ;R1 > R2
             23: bgea  R1,R2,#Offset ;R1 >= R2
             24: blta  R1,R2,#Offset ;R1 < R2
             25: blea  R1,R2,#Offset ;R1 <= R2
             26: bgtas R1,R2,#Offset ;SIGNED R1 > R2
             27: bgeas R1,R2,#Offset ;SIGNED R1 >= R2
             28: bltas R1,R2,#Offset ;SIGNED R1 < R2
             29: bleas R1,R2,#Offset ;SIGNED R1 <= R2
     6-10   First register
     11-15  Second register
     16-31  Jump offset (Signed)}
    procedure IConditionalBranch2;
    {Bit    Expl
     0-5    Must be 3C
     6      Register mode
            00:
                 7-11   Compare register (r0-31)
            01:
                 7      Sub Mode
                         00: Float 32 bit registers
                             8-11  Compare register (r0-r16)
                         01: 64 bit registers
                             8  Data mode
                                 00: Int64
                                 01: Float64
                                 9-11   Compare register (xr0-xr7)
     12-15  Opcode
             00: (f|x)beq(32)   Rs,#Number,#Offset ;Rs = #Number
             01: (f|x)bne(32)   Rs,#Number,#Offset ;Rs <> #Number
             02: (f|x)bgt(32)   Rs,#Number,#Offset ;Rs > #Number
             03: (f|x)bge(32)   Rs,#Number,#Offset ;Rs >= #Number
             04: (f|x)blt(32)   Rs,#Number,#Offset ;Rs < #Number
             05: (f|x)ble(32)   Rs,#Number,#Offset ;Rs <= #Number
             06: (f|x)bgt(r|s)(32)  Rs,#Number,#Offset ;SIGNED Rs > #Number
             07: (f|x)bge(r|s)(32)  Rs,#Number,#Offset ;SIGNED Rs >= #Number
             08: (f|x)blt(r|s)(32)  Rs,#Number,#Offset ;SIGNED Rs < #Number
             09: (f|x)ble(r|s)(32)  Rs,#Number,#Offset ;SIGNED Rs <= #Number
             0A: (f|x)beq(r|s|n)(32)  Rs,#Number,#Offset ;Rs = #Number
             0B: (f|x)bne(r|s|n)(32)  Rs,#Number,#Offset ;Rs <> #Number
             0C: (f|x)bgt(t|n)(32)  Rs,#Number,#Offset ;Rs > #Number
             0D: (f|x)bge(t|n)(32)  Rs,#Number,#Offset ;Rs >= #Number
             0E: (f|x)blt(t|n)(32)  Rs,#Number,#Offset ;Rs < #Number
             0F: (f|x)ble(t|n)(32)  Rs,#Number,#Offset ;Rs <= #Number
             (if n-suffix then not [Rs];
              if r-suffix then Round([Rs])
              if t-suffix then Trunc([Rs]))
     16-47  Compare number
     48-63  Jump signed 16 bit offset }
    procedure IMemoryOperate;
    {Bit    Expl
     0-5    Must be 2A
     6-10   Destination register
     11-15  Source register
     16-18  Opcode
             00: str  R1,[R2,#Offset]    ;store 32 bit
             01: strh R1,[R2,#Offset]    ;store 16 bit
             02: strb R1,[R2,#Offset]    ;store 8 bit
             03: ldr  R1,[R2,#Offset]    ;load 32 bit
             04: ldrh R1,[R2,#Offset]    ;load 16 bit
             05: ldsh R1,[R2,#Offset]    ;load signed 16 bit
             06: ldrb R1,[R2,#Offset]    ;load 8 bit
             07: ldsb R1,[R2,#Offset]    ;load signed 8 bit
    19-30   12 bits Offset
    31      if set then Offset is negative }
    procedure IMemoryOperateR;
    {Bit    Expl
     0-5    Must be 2B
     6-10   Register 1
     11-15  Register 2
     16-18  Opcode
             00: str  R1,[R2,Rn]    ;store 32 bit
             01: strh R1,[R2,Rn]    ;store 16 bit
             02: strb R1,[R2,Rn]    ;store 8 bit
             03: ldr  R1,[R2,Rn]    ;load 32 bit
             04: ldrh R1,[R2,Rn]    ;load 16 bit
             05: ldsh R1,[R2,Rn]    ;load signed 16 bit
             06: ldrb R1,[R2,Rn]    ;load 8 bit
             07: ldsb R1,[R2,Rn]    ;load signed 8 bit
     19-23  Rn - Offset register}
    procedure ILDMIA;
    {Bit    Expl
     0-5    Opcode
             2C: ldmia [Rs]!,RList   ;load from memory, increase Rs
     6-7    Rs - Source register (r0-r3)
     8-23   RList - List of registers (r4-r19)
            **if all bits = 0 then load to r20}
    procedure ISTMIA;
    {Bit    Expl
     0-5    Opcode
             2D: stmia [Rd]!,RList   ;store in memory, increase Rd
     6-7    Rd - Destination register
     8-23   RList - List of registers (r4-r19)
            **if all bits = 0 then copy from r20}
    procedure ICall;
    {Bit    Expl
     0-5    Must be 2E
     6-7    Opcode
             00: b   #Offset        ;signed 32 bit offset jump
             01: b   #Offset        ;signed 16 bit offset jump
             02: bl  #Offset        ;signed 32 bit offset call
             03: blm #Module,#Method;method call
     8-39   Offset (Signed)
     8-23   Module - Module index
     24-39  Method - Method index}
    procedure IOther;
    {Bit    Expl
     0-5    Must be 2F
     6      Flag1
             F1_0:
     7      Opcode
             00: pushl RList         ;push list of registers in stack
             01: popl  RList          ;pop list of registers from stack
     8-31   RList - List of registers (r8-r31) (r31-r8 for POP)
            **if all bits = 0 then push/pop r0-r7
             F1_1:
     7      Flag2
             F2_0:
     8-10   Opcode
             00: push  Rn           ;push signle register in stack
             01: pop   Rn           ;pop single register from stack
             02: bx    Rn           ;jump
             03: blx   Rn           ;offset call
             04: blmx  Rn           ;method call
             05: cbd   Rn           ;Rn = SIGNED_BYTE[Rn]
             06: cwd   Rn           ;Rn = SIGNED_WORD[Rn]
             07: swap  Rn           ;swap bytes order in Rn
     11-15  Rn - Used register
             F2_1:
     8-9    Opcode
             00: seq Rd,Rs,#Number  ;Rd = Byte(Rs = #Number)
             01: seq Rd,Rs,Rn,#Num  ;Rd = Byte(Rs = Rn + #Num)
             02: sne Rd,Rs,#Number  ;Rd = Byte(Rs <> #Number)
             03: sne Rd,Rs,Rn,#Num  ;Rd = Byte(Rs <> Rn + #Num)
     10-14  Destination register
     15-19  Source Register
     20-24  Rn
     25-30  6 bit Num (if bit 31 is set then Num is negative)
     20-30  11 bit Number (if bit 31 is set then Number is negative)}
    procedure IAriphmetic64;
    {Bit    Expl
     0-5    Must be 30
     6-9    Opcode
             00: xmov  xRd,xRs       ;xRd = xRs
             01: xmvn  xRd,xRs       ;xRd = not xRs
             02: xadd  xRd,xRs       ;xRd = xRd + xRs
             03: xsub  xRd,xRs       ;xRd = xRd - xRs
             04: xrsb  xRd,xRs       ;xRd = xRs - xRd
             05: xmul  xRd,xRs       ;xRd = xRd * xRs
             06: xdiv  xRd,xRs       ;xRd = xRd div xRs
             07: xrdv  xRd,xRs       ;xRd = xRs div xRd
             08: xmod  xRd,xRs       ;xRd = xRd mod xRs
             09: xand  xRd,xRs       ;xRd = xRd and xRs
             0A: xbic  xRd,xRs       ;xRd = xRd and not xRs
             0B: xorr  xRd,xRs       ;xRd = xRd or xRs
             0C: xxor  xRd,xRs       ;xRd = xRd xor xRs
             0D: cdq   xRd,xRs       ;xRd = SIGNED_DWORD(xRs)
             0E: cwq   xRd,xRs       ;xRd = SIGNED_WORD(xRs)
             0f: cbq   xRd,xRs       ;xRd = SIGNED_BYTE(xRs)
     10-12   xRd - Destination register (xr0-xr7)
     13-15   xRs - Source register (xr0-xr7)}
    procedure ILogic64;
    {Bit    Expl
     0-5    Must be 31
     6-7    Opcode
             00: xshl xRd,Rs    ;xRd = xRd shl Rs
             01: fcsr xRd,Rs    ;Convert float-32 to float-64
             02: xshr xRd,Rs    ;xRd = xRd shr Rs
             03: xsar xRd,Rs    ;xRd = xRd sar Rs
     8-10   xRd - Destination register (xr0-xr7)
     11-15  Rs - Source register (r0-r31)}
    procedure ICompare64;
    {Bit    Expl
     0-5    Must be 32
     6-8    Opcode
             00: xseq(32) Rd,xR1,xR2   ;Rd = Byte(xR1 =  xR2)
             01: xsne(32) Rd,xR1,xR2   ;Rd = Byte(xR1 <> xR2)
             02: xsgt(32) Rd,xR1,xR2   ;Rd = Byte(xR1 >  xR2)
             03: xsge(32) Rd,xR1,xR2   ;Rd = Byte(xR1 >= xR2)
             04: xslt(32) Rd,xR1,xR2   ;Rd = Byte(xR1 <  xR2)
             05: xsle(32) Rd,xR1,xR2   ;Rd = Byte(xR1 <= xR2)
             06: xseqi Rd,xR1,#Number  ;Rd = Byte(xR1 = #Number)
             07: xsnei Rd,xR1,#Number  ;Rd = Byte(xR1 <> #Number)
     9-13   Destination register (r0-r31)
     14-16  First comparsion register (xr0-xr7)
     17     Register type or Negative bit for #Number
     18-20  Second comparsion register (xr0-xr7)
     18     Signed R2
     19-23  Second comparsion register (r0-r31)
     18-23  6 bit Number}
    procedure IFloat32;
    {Bit    Expl
     0-5    Must be 33
     6-10   Destination register
     11-15  First register
     16-20  Second register
     21-23  Opcode
             00: fadd32 Rd,R1,R2      ;Rd = R1 + R2
             01: fsub32 Rd,R1,R2      ;Rd = R1 - R2
             02: fmul32 Rd,R1,R2      ;Rd = R1 * R2
             03: fdiv32 Rd,R1,R2      ;Rd = R1 / R2
             04: fseq32 Rd,R1,R2      ;Rd = Cardinal(R1 = R2)
             05: fsne32 Rd,R1,R2      ;Rd = Cardinal(R1 <> R2)
             06: fsgt32 Rd,R1,R2      ;Rd = Cardinal(R1 > R2)
             07: fsge32 Rd,R1,R2      ;Rd = Cardinal(R1 >= R2)}
    procedure IFloat32a;
    {Bit    Expl
     0-5    Must be 34
     6-8    Opcode
             00: fabs32 Rd,Rs         ;Rd = Absolute value of Rs
             01: fneg32 Rd,Rs         ;Rd = -Rs
             02: fexp32 Rd,Rs         ;Rd = exponential of Rs
             03: frac32 Rd,Rs         ;Rd = fractional part of Rs
             04: fint32 Rd,Rs         ;Rd = integer part of Rs
             05: frnd32 Rd,Rs         ;Integer(Rd) = Round(Rs)
             06: ftrn32 Rd,Rs         ;Integer(Rd) = Trunc(Rs)
             07: fcds32 Rd,Rs         ;Convert Integer to Float32
     9-11   Destination register (r0-r7)
     12-15  Source register (r0-r15) }
    procedure IFloat64;
    {Bit    Expl
     0-5    Must be 35
     6-9    Opcode
             00: fadd  xRd,xRs       ;xRd = xRd + xRs
             01: fsub  xRd,xRs       ;xRd = xRd - xRs
             02: fmul  xRd,xRs       ;xRd = xRd * xRs
             03: fdiv  xRd,xRs       ;xRd = xRd / xRs
             04: fabs  xRd,xRs       ;xRd = Absolute value of xRs
             05: frnd  xRd,xRs       ;Round Float64 to Int64
             06: ftrn  xRd,xRs       ;Truncate Float64 to Int64
             07: fcqr  xRd,xRs       ;Convert Int64 to Float64
             08: fneg  xRd,xRs       ;xRd = -xRs
             09: fexp  xRd,xRs       ;xRd = exponential of xRs
             0A: frac  xRd,xRs       ;xRd = fractional part of xRs
             0B: fint  xRd,xRs       ;xRd = integer part of xRs
             0C: fln   xRd,xRs       ;xRd = natural logarithm of xRs
             0D: frsb  xRd,xRs       ;xRd = xRs - xRd
             0E: fsqr  xRd,xRs       ;xRd = square of xRs
             0F: fsqrt xRd,xRs       ;xRd = square root of xRs
     10-12  Destination register  (xr0-xr7)
     13-15  Source register  (xr0-xr7)}
    procedure IFloatCompare;
    {Bit    Expl
     0-5    Opcode
             36: fseq Rd,xR1,xR2      ;Rd = Byte(xR1 =  xR2)
             37: fsne Rd,xR1,xR2      ;Rd = Byte(xR1 <> xR2)
             38: fsgt Rd,xR1,xR2      ;Rd = Byte(xR1 >  xR2)
             39: fsge Rd,xR1,xR2      ;Rd = Byte(xR1 >= xR2)
             3A: fslt Rd,xR1,xR2      ;Rd = Byte(xR1 <  xR2)
             3B: fsle Rd,xR1,xR2      ;Rd = Byte(xR1 <= xR2)
     6-8    Destination register (r0-r7)
     9-11   First comparsion register (xr0-xr7)
     12     Second register type (0 - Real; 1 - Single)
     13-15  Second comparsion register (xr0-xr7, r0-r7)}
    procedure ISystem;
    {Bit    Expl
     0-5    Must be 3F
     6-7    Opcode
             00: nop                ;no operation
             01: ret                ;return from subroutine
             02: raise              ;raise exception (r0 - exception class ptr)
             03: stop               ;stop execution}
    procedure IUnknown;
  protected
    procedure Initialize; override;
    procedure ClearData; override;
  public
    property EntryPoint: TKruptarVirtualMethod read FEntryPoint;
    property MainModule: TKruptarVirtualModule read FMainModule;
    property Executed: Boolean read FExecuted write FExecuted;
    property Paused: Boolean read FPaused write FPaused;
    property StackSize: Integer read GetStackSize write SetStackSize;
    property LastError: Integer read FLastError;
    property LastExceptionName: String read FLastExceptionName;
    property LastExceptionMessage: String read FLastExceptionMessage;
    property ModItems[Index: Integer]: TKruptarVirtualModule read GetByIndex;
    property ModNames[const Name: String]: TKruptarVirtualModule read GetByName;

    function AddModule: TKruptarVirtualModule;
    function Proceed: Integer;
    function Run(StepMode: Boolean = False): Integer;
    function ExecuteMethod(const ModuleName, Name: String;
                           const ParamList: Array of Const;
                           StepMode: Boolean = False): Integer;
 end;

 EVirtualStackError = class(Exception);
 EVirtualCallError = class(Exception);

 TVirtualStackErrorClass = class of EVirtualStackError;
 TVirtualCallErrorClass = class of EVirtualCallError;

Const
 KVME_OK = 0;
 KVME_UnexpectedError = 1;
 KVME_InvalidParameters = 2;
 KVME_NoCode = 3;
 KVME_ExceptionRaised = 4;
 KVME_MethodNotFound = 5;
 KVME_ModuleNotFound = 6;
(* Additional variant constants*)
 varExtended = 998;
 varReal48 = 999;

Var
 KVM_StackErrorExceptionInfo: TVirtualStackErrorClass =
                                 EVirtualStackError;
 KVM_CallErrorExceptionInfo: TVirtualCallErrorClass =
                             EVirtualCallError;
 KVMS_VirtualStackOverflow: String = 'Virtual stack overflow.';
 KVMS_VirtualStackUnderflow: String = 'Virtual stack underflow.';
 KVMS_IllegalSetExternalProcCall: String = 'Illegal %s.SetExternalProc call.';
 KVMS_IllegalInitializeCall: String = 'Illegal %s.Init call.';
 KVMS_IllegalSetCodeSizeCall: String = 'Illegal %s.SetCodeSize call.';
 KVMS_InvalidCallingConvention: String = 'Invalid calling convention.';
 KVMS_TooManyParameters: String = 'Too many parameters.';
 KVMS_ReadVariableError: String = 'Read variable error.';

implementation

Type
 TModifyRegister = procedure(var A1: Cardinal; A2: Cardinal);
 TCalcRegister = function(A1, A2: Cardinal): Cardinal;
 TMemOperate = procedure(var A1, A2);
 TAriphmetic64 = procedure(var A1, A2: Int64);
 TLogic64 = procedure (var A1: Int64; A2: Integer);
 TCompare64 = function (var A1, A2: Int64): Cardinal;
 TCompare64_S32 = function (var A1: Int64; A2: Integer): Cardinal;
 TCompare64_U32 = function (var A1: Int64; A2: Cardinal): Cardinal;
 TMemOperate2 = procedure (var A1, A2, AD);

procedure _MOV(var A1: Cardinal; A2: Cardinal);
asm
 mov    [eax],edx
end;

procedure _MVN(var A1: Cardinal; A2: Cardinal);
asm
 not    edx
 mov    [eax],edx
end;

procedure _ADD(var A1: Cardinal; A2: Cardinal);
asm
 add    dword ptr [eax],edx
end;

procedure _SUB(var A1: Cardinal; A2: Cardinal);
asm
 sub    dword ptr [eax],edx
end;

procedure _RSB(var A1: Cardinal; A2: Cardinal);
asm
 mov    edx,[edx]
 sub    edx,dword ptr [eax]
 mov    [eax],edx
end;

procedure _MUL(var A1: Cardinal; A2: Cardinal);
begin
 A1 := A1 * A2;
end;

procedure _DIV(var A1: Cardinal; A2: Cardinal);
begin
 A1 := A1 div A2;
end;

procedure _DIVS(var A1: Cardinal; A2: Cardinal);
begin
 Integer(A1) := Integer(A1) div Integer(A2);
end;

procedure _MOD(var A1: Cardinal; A2: Cardinal);
begin
 A1 := A1 mod A2;
end;

procedure _RMD(var A1: Cardinal; A2: Cardinal);
begin
 A1 := A2 mod A1;
end;

procedure _SWP(var A1: Cardinal; A2: Cardinal);
asm
 bswap  edx
 mov    [eax],edx
end;

procedure _SWPH(var A1: Cardinal; A2: Cardinal);
asm
 xchg   dl,dh
 mov [eax],edx
end;

procedure _AND(var A1: Cardinal; A2: Cardinal);
asm
 and    [eax],edx
end;

procedure _BIC(var A1: Cardinal; A2: Cardinal);
asm
 not    edx
 and    [eax],edx
end;

procedure _ORR(var A1: Cardinal; A2: Cardinal);
asm
 or     [eax],edx
end;

procedure _XOR(var A1: Cardinal; A2: Cardinal);
asm
 xor    [eax],edx
end;

function _SHL(A1, A2: Cardinal): Cardinal;
asm
 mov    cl,dl
 shl    eax,cl
end;

function _ADDx(A1, A2: Cardinal): Cardinal;
asm
 add    eax,edx
end;

function _SHR(A1, A2: Cardinal): Cardinal;
asm
 mov    cl,dl
 shr    eax,cl
end;

function _SAR(A1, A2: Cardinal): Cardinal;
asm
 mov    cl,dl
 sar    eax,cl
end;

function _SEQ(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setz   al
 and    eax,1
// Result := Cardinal(A1 = A2);
end;

function _SNE(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setnz  al
 and    eax,1
// Result := Cardinal(A1 <> A2);
end;

function _SGT(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setb   al
 and    eax,1
// Result := Cardinal(A1 > A2);
end;

function _SGE(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setbe  al
 and    eax,1
// Result := Cardinal(A1 >= A2);
end;

function _SLT(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setnbe al
 and    eax,1
// Result := Cardinal(A1 < A2);
end;

function _SLE(A1, A2: Cardinal): Cardinal;
asm
 cmp    edx,eax
 setnb  al
 and    eax,1
// Result := Cardinal(A1 <= A2);
end;

function _SGTS(A1, A2: Cardinal): Cardinal;
asm
 cmp    eax,edx
 setnle al
 and    eax,1
// Result := Cardinal(Integer(A1) > Integer(A2));
end;

function _SGES(A1, A2: Cardinal): Cardinal;
asm
 cmp    eax,edx
 setnl  al
 and    eax,1
// Result := Cardinal(Integer(A1) >= Integer(A2));
end;

function _SLTS(A1, A2: Cardinal): Cardinal;
asm
 cmp    eax,edx
 setl   al
 and    eax,1
// Result := Cardinal(Integer(A1) < Integer(A2));
end;

function _SLES(A1, A2: Cardinal): Cardinal;
asm
 cmp    eax,edx
 setle  al
 and    eax,1
// Result := Cardinal(Integer(A1) <= Integer(A2));
end;

procedure _STR(var A1, A2);
asm
 mov    eax,dword ptr [eax]
 mov    dword ptr [edx],eax
end;

procedure _STRH(var A1, A2);
asm
 mov    ax,word ptr [eax]
 mov    word ptr [edx],ax
end;

procedure _STRB(var A1, A2);
asm
 mov    al,byte ptr [eax]
 mov    byte ptr [edx],al
end;

procedure _LDR(var A1, A2);
asm
 mov    edx,dword ptr [edx]
 mov    [eax],edx
end;

procedure _LDRH(var A1, A2);
asm
 movzx  edx,word ptr [edx]
 mov    [eax],edx
end;

procedure _LDSH(var A1, A2);
asm
 movsx  edx,word ptr [edx]
 mov    [eax],edx
end;

procedure _LDRB(var A1, A2);
asm
 movzx  edx,byte ptr [edx]
 mov    [eax],edx
end;

procedure _LDSB(var A1, A2);
asm
 movsx  edx,byte ptr [edx]
 mov    [eax],edx
end;

//Ariphmetic 64

procedure _XMOV(var A, B: Int64);
asm
 mov    ecx,[edx]
 mov    [eax],ecx
 mov    ecx,[edx + 4]
 mov    [eax + 4],ecx
end;

procedure _XMVN(var A, B: Int64);
asm
 mov    ecx,[edx]
 not    ecx
 mov    [eax],ecx
 mov    ecx,[edx + 4]
 not    ecx
 mov    [eax + 4],ecx
end;

procedure _XADD(var A, B: Int64);
asm
 mov    ecx,[edx]
 add    [eax],ecx
 mov    ecx,[edx + 4]
 adc    [eax + 4],ecx
end;

procedure _XSUB(var A, B: Int64);
asm
 mov    ecx,[edx]
 sub    [eax],ecx
 mov    ecx,[edx + 4]
 sbb    [eax + 4],ecx
end;

procedure _XRSB(var A, B: Int64);
asm
 mov    ecx,[edx]
 sub    ecx,[eax]
 mov    [eax],ecx
 mov    ecx,[edx + 4]
 sbb    ecx,[eax + 4]
 mov    [eax + 4],ecx
end;

procedure _XMUL(var A, B: Int64);
begin
 A := A * B;
end;

procedure _XDIV(var A, B: Int64);
begin
 A := A div B;
end;

procedure _XRDV(var A, B: Int64);
begin
 A := B div A;
end;

procedure _XMOD(var A, B: Int64);
begin
 A := A mod B;
end;

procedure _XAND(var A, B: Int64);
asm
 mov    ecx,[edx]
 and    [eax],ecx
 mov    ecx,[edx + 4]
 and    [eax + 4],ecx
end;

procedure _XBIC(var A, B: Int64);
asm
 mov    ecx,[edx]
 not    ecx
 and    [eax],ecx
 mov    ecx,[edx + 4]
 not    ecx
 and    [eax + 4],ecx
end;

procedure _XORR(var A, B: Int64);
asm
 mov    ecx,[edx]
 or     [eax],ecx
 mov    ecx,[edx + 4]
 or     [eax + 4],ecx
end;

procedure _XXOR(var A, B: Int64);
asm
 mov    ecx,[edx]
 xor    [eax],ecx
 mov    ecx,[edx + 4]
 xor    [eax + 4],ecx
end;

procedure _CDQ(var A, B: Int64);
asm
 mov    ecx,eax
 mov    eax,[edx]
 cdq
 mov    [ecx],eax
 mov    [ecx + 4],edx
end;

procedure _CWQ(var A, B: Int64);
asm
 mov    ecx,eax
 movsx  eax,word ptr [edx]
 cdq
 mov    [ecx],eax
 mov    [ecx + 4],edx
end;

procedure _CBQ(var A, B: Int64);
asm
 mov    ecx,eax
 movsx  eax,byte ptr [edx]
 cdq
 mov    [ecx],eax
 mov    [ecx + 4],edx
end;

procedure _XSHL(var A: Int64; B: Integer);
asm
 mov    cl,dl
 cmp    cl,32
 jl     @@below32
 cmp    cl,64
 jl     @@below64
 xor    edx,edx
 mov    [eax],edx
 mov    [eax + 4],edx
 ret
@@below64:
// push   ebx
 mov    ebx,eax
 mov    eax,[ebx + 4]
 mov    edx,eax
 shl    edx,cl
 xor    eax,eax
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
 ret
@@below32:
// push   ebx
 mov    ebx,eax
 mov    eax,[ebx]
 mov    edx,[ebx + 4]
 shld   edx, eax, cl
 shl    eax, cl
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
end;

procedure _XADD2(var A: Int64; B: Integer);
asm
 xchg   eax,ecx
 xchg   eax,edx
 cdq
 add    [ecx],eax
 adc    [ecx + 4],edx
end;

procedure _XSHR(var A: Int64; B: Integer);
asm
 mov    cl,dl
 cmp    cl,32
 jl     @@below32
 cmp    cl,64
 jl     @@below64
 xor    edx,edx
 mov    [eax],edx
 mov    [eax + 4],edx
 ret
@@below64:
// push   ebx
 mov    ebx,eax
 mov    eax,[ebx + 4]
 xor    edx,edx
 shr    eax,cl
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
 ret
@@below32:
 push   ebx
 mov    ebx,eax
 mov    eax,[ebx]
 mov    edx,[ebx + 4]
 shrd   eax, edx, cl
 shr    edx, cl
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
end;

procedure _XSAR(var A: Int64; B: Integer);
asm
 mov    cl,dl
 cmp    cl,32
 jl     @@below32
 cmp    cl,64
 jl     @@below64
 mov    edx,[eax + 4]
 sar    edx,31
 mov    [eax],edx
 mov    [eax + 4],edx
 ret
@@below64:
// push   ebx
 mov    ebx,eax
 mov    eax,[ebx]
 mov    edx,[ebx + 4]
 mov    eax,edx
 cdq
 sar    eax,cl
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
 ret
@@below32:
// push   ebx
 mov    ebx,eax
 mov    eax,[ebx]
 mov    edx,[ebx + 4]
 shrd   eax, edx, cl
 sar    edx, cl
 mov    [ebx],eax
 mov    [ebx + 4],edx
// pop    ebx
end;

function _XSEQ(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A = B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setz   al
 and    eax,1
end;

function _XSNE(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A <> B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setnz  al
 and    eax,1
end;

function _XSGT(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A > B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnbe al
 and    eax,1
 ret
@@Next:
 setnle al
 and    eax,1
end;

function _XSGE(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A >= B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnb  al
 and    eax,1
 ret
@@Next:
 setnl  al
 and    eax,1
end;

function _XSLT(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A < B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setb   al
 and    eax,1
 ret
@@Next:
 setl   al
 and    eax,1
end;

function _XSLE(var A, B: Int64): Cardinal;
//begin
// Result := Cardinal(A <= B);
asm
 mov    ecx,edx
 mov    edx,[eax + 4]
 mov    eax,[eax]
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setbe  al
 and    eax,1
 ret
@@Next:
 setle  al
 and    eax,1
end;

function _XSEQ_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A = B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setz   al
 and    eax,1
end;

function _XSNE_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A <> B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setnz   al
 and    eax,1
end;

function _XSGT_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A > B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setb   al
 and    eax,1
 ret
@@Next:
 setl   al
 and    eax,1
end;

function _XSGE_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A >= B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setbe  al
 and    eax,1
 ret
@@Next:
 setle  al
 and    eax,1
end;

function _XSLT_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A < B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnbe al
 and    eax,1
 ret
@@Next:
 setnle al
 and    eax,1
end;

function _XSLE_S32(var A: Int64; B: Integer): Cardinal;
//begin
// Result := Cardinal(A <= B);
asm
 mov    ecx,eax
 mov    eax,edx
 cdq
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnb  al
 and    eax,1
 ret
@@Next:
 setnl  al
 and    eax,1
end;

function _XSEQ_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A = B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setz   al
 and    eax,1
end;

function _XSNE_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A <> B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
@@Next:
 setnz  al
 and    eax,1
end;

function _XSGT_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A > B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setb   al
 and    eax,1
 ret
@@Next:
 setl   al
 and    eax,1
end;

function _XSGE_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A >= B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setbe  al
 and    eax,1
 ret
@@Next:
 setle  al
 and    eax,1
end;

function _XSLT_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A < B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnbe al
 and    eax,1
 ret
@@Next:
 setnle al
 and    eax,1
end;

function _XSLE_U32(var A: Int64; B: Cardinal): Cardinal;
//begin
// Result := Cardinal(A <= B);
asm
 mov    ecx,eax
 mov    eax,edx
 xor    edx,edx
 cmp    edx,[ecx + 4]
 jnz    @@Next
 cmp    eax,[ecx]
 setnb  al
 and    eax,1
 ret
@@Next:
 setnl al
 and    eax,1
end;

//Floating point routines

procedure _FADD32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fadd   dword ptr [edx]
 fstp   dword ptr [ecx]
 wait
end;

procedure _FSUB32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fsub   dword ptr [edx]
 fstp   dword ptr [ecx]
 wait
end;

procedure _FMUL32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fmul   dword ptr [edx]
 fstp   dword ptr [ecx]
 wait
end;

procedure _FDIV32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fdiv   dword ptr [edx]
 fstp   dword ptr [ecx]
 wait
end;

procedure _FSEQ32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jnz    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSNE32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jz     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGT32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jbe    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGE32(var R1, R2, RD);
asm
 fld    dword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jb     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FABS32(var RD, RS);
asm
 fld    dword ptr [edx]
 fabs
 fstp   dword ptr [eax]
 wait
end;

procedure _FNEG32(var RD, RS);
asm
 fld    dword ptr [edx]
 fchs
 fstp   dword ptr [eax]
 wait
end;

procedure _FEXP32(var RD, RS);
asm
 fld    dword ptr [edx]
 fldl2e
 fmul
 fld    st(0)
 frndint
 fsub   st(1),st
 fxch   st(1)
 f2xm1
 fld1
 fadd
 fscale
 fstp   dword ptr [eax]
 wait
end;

procedure _FRAC32(var RD, RS);
asm
 fld    dword ptr [edx]
 fld    st(0)
 sub    esp,4
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 frndint
 wait
 fldcw  word ptr [esp]
 add    esp,4
 fsub
 fstp   dword ptr [eax]
 wait
end;

procedure _FINT32(var RD, RS);
asm
 fld    dword ptr [edx]
 sub    esp,4
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 frndint
 wait
 fldcw  word ptr [esp]
 add    esp,4
 fstp   dword ptr [eax]
 wait
end;

procedure _FRND32(var RD, RS);
asm
 fld    dword ptr [edx]
 sub    esp,8
 fistp  qword ptr [esp]
 wait
 pop    ecx
 add    esp,4
 mov    [eax],ecx
end;

procedure _FTRN32(var RD, RS);
asm
 fld    dword ptr [edx]
 sub    esp,12
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 fistp  qword ptr [esp + 4]
 wait
 fldcw  word ptr [esp]
 pop    ecx
 pop    dword ptr [eax]
 pop    ecx
end;

procedure _FCDS32(var RD, RS);
asm
 fild   dword ptr [edx]
 fstp   dword ptr [eax]
 wait
end;

procedure _FADD64(var RD, RS);
asm
 fld    qword ptr [eax]
 fadd   qword ptr [edx]
 fstp   qword ptr [eax]
 wait
end;

procedure _FSUB64(var RD, RS);
asm
 fld    qword ptr [eax]
 fsub   qword ptr [edx]
 fstp   qword ptr [eax]
 wait
end;

procedure _FMUL64(var RD, RS);
asm
 fld    qword ptr [eax]
 fmul   qword ptr [edx]
 fstp   qword ptr [eax]
 wait
end;

procedure _FDIV64(var RD, RS);
asm
 fld    qword ptr [eax]
 fdiv   qword ptr [edx]
 fstp   qword ptr [eax]
 wait
end;

procedure _FABS64(var RD, RS);
asm
 fld    qword ptr [edx]
 fabs
 fstp   qword ptr [eax]
 wait
end;

procedure _FRND64(var RD, RS);
asm
 fld    qword ptr [edx]
 sub    esp,8
 fistp  qword ptr [esp]
 wait
 pop    ecx
 mov    [eax],ecx
 pop    ecx
 mov    [eax + 4],ecx
end;

procedure _FTRN64(var RD, RS);
asm
 fld    qword ptr [edx]
 sub    esp,12
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 fistp  qword ptr [esp + 4]
 wait
 fldcw  word ptr [esp]
 pop    ecx
 pop    dword ptr [eax]
 pop    dword ptr [eax + 4]
end;

procedure _FCQR64(var RD, RS);
asm
 fild   qword ptr [edx]
 fstp   qword ptr [eax]
 wait
end;

procedure _FNEG64(var RD, RS);
asm
 fld    qword ptr [edx]
 fchs
 fstp   qword ptr [eax]
 wait
end;

procedure _FEXP64(var RD, RS);
asm
 fld    qword ptr [edx]
 fldl2e
 fmul
 fld    st(0)
 frndint
 fsub   st(1),st
 fxch   st(1)
 f2xm1
 fld1
 fadd
 fscale
 fstp   qword ptr [eax]
 wait
end;

procedure _FRAC64(var RD, RS);
asm
 fld    qword ptr [edx]
 fld    st(0)
 sub    esp,4
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 frndint
 wait
 fldcw  word ptr [esp]
 add    esp,4
 fsub
 fstp   qword ptr [eax]
 wait
end;

procedure _FINT64(var RD, RS);
asm
 fld    qword ptr [edx]
 sub    esp,4
 fnstcw word ptr [esp]
 fnstcw word ptr [esp + 2]
 wait
 or     word ptr [esp + 2],$F00
 fldcw  word ptr [esp + 2]
 frndint
 wait
 fldcw  word ptr [esp]
 add    esp,4
 fstp   qword ptr [eax]
 wait
end;

procedure _FLN64(var RD, RS);
asm
 fld    qword ptr [edx]
 fldln2
 fxch   st(1)
 fyl2x
 fstp   qword ptr [eax]
 wait
end;

procedure _FRSB64(var RD, RS);
asm
 fld    qword ptr [edx]
 fsub   qword ptr [eax]
 fstp   qword ptr [eax]
 wait
end;

procedure _FSQR64(var RD, RS);
asm
 fld    qword ptr [edx]
 fmul   st(0),st
 fstp   qword ptr [eax]
 wait
end;

procedure _FSQRT(var RD, RS);
asm
 fld    qword ptr [edx]
 fsqrt
 fstp   qword ptr [eax]
 wait
end;

//////Compare Double with Double

procedure _FSEQ64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jnz    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSNE64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jz     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGT64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jbe    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGE64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jb     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSLT64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jnb    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSLE64(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  qword ptr [edx]
 fstsw  ax
 sahf
 jnbe   @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

//////Compare Double with Single

procedure _FSEQ64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jnz    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSNE64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jz     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGT64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jbe    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSGE64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jb     @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSLT64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jnb    @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

procedure _FSLE64x(var R1, R2, RD);
asm
 fld    qword ptr [eax]
 fcomp  dword ptr [edx]
 fstsw  ax
 sahf
 jnbe   @@Next
 mov    [ecx],1
 ret
@@Next:
 xor    eax,eax
 mov    [ecx],eax
end;

Const
 ModRegProcs: Array[0..12] of TModifyRegister =
 (_MOV, _ADD, _SUB, _RSB, _MUL, _DIV, _DIVS,_MOD,
  _RMD, _AND, _BIC, _ORR, _XOR);
 ModRegProcs2: Array[0..15] of TModifyRegister =
 (_MOV, _MVN, _ADD, _SUB, _RSB, _MUL, _DIV, _DIVS,
  _MOD, _RMD, _SWP, _SWPH,_AND, _BIC, _ORR, _XOR);
 CalcRegFuncs: Array[0..7] of TCalcRegister =
 (_SHL, _SHL, _ADDx,_ADDx,_SHR, _SHR, _SAR, _SAR);
 CompareFuncs: Array[0..9] of TCalcRegister =
 (_SEQ, _SNE,
  _SGT, _SGE, _SLT, _SLE,_SGTS,_SGES,_SLTS,_SLES);
 MemOpProcs: Array[0..7] of TMemOperate =
 (_STR, _STRH,_STRB,_LDR,_LDRH,_LDSH,_LDRB,_LDSB);
 AProcs64: Array[0..15] of TAriphmetic64 =
 (_XMOV, _XMVN, _XADD, _XSUB, _XRSB, _XMUL, _XDIV, _XRDV,
  _XMOD, _XAND, _XBIC, _XORR, _XXOR, _CDQ,  _CWQ,  _CBQ);
 LProcs64: Array[0..3] of TLogic64 =
 (_XSHL, _XADD2, _XSHR, _XSAR);
 CProcs64: Array[0..5] of TCompare64 =
 (_XSEQ, _XSNE, _XSGT, _XSGE, _XSLT, _XSLE);
 CProcs64_S32: Array[0..5] of TCompare64_S32 =
 (_XSEQ_S32, _XSNE_S32, _XSGT_S32, _XSGE_S32,
  _XSLT_S32, _XSLE_S32);
 CProcs64_U32: Array[0..5] of TCompare64_U32 =
 (_XSEQ_U32, _XSNE_U32, _XSGT_U32, _XSGE_U32,
  _XSLT_U32, _XSLE_U32);
 F32Procs: Array[0..7] of TMemOperate2 =
 (_FADD32, _FSUB32, _FMUL32, _FDIV32,
  _FSEQ32, _FSNE32, _FSGT32, _FSGE32);
 F32aProcs: Array[0..7] of TMemOperate =
 (_FABS32, _FNEG32, _FEXP32, _FRAC32,
  _FINT32, _FRND32, _FTRN32, _FCDS32);
 F64Procs: Array[0..15] of TMemOperate =
 (_FADD64, _FSUB64, _FMUL64, _FDIV64,
  _FABS64, _FRND64, _FTRN64, _FCQR64,
  _FNEG64, _FEXP64, _FRAC64, _FINT64,
  _FLN64,  _FRSB64, _FSQR64, _FSQRT);
 FC64Procs: Array[0..5] of TMemOperate2 =
 (_FSEQ64, _FSNE64, _FSGT64,
  _FSGE64, _FSLT64, _FSLE64);
 FC64xProcs: Array[0..5] of TMemOperate2 =
 (_FSEQ64x, _FSNE64x, _FSGT64x,
  _FSGE64x, _FSLT64x, _FSLE64x);


function TKruptarVirtualFields.AddMethod: TKruptarVirtualMethod;
begin
 Result := TKruptarVirtualMethod.Create(Owner as TKruptarVirtualModule);
 AddCreated(Result);
end;

function TKruptarVirtualFields.AddVariable: TKruptarVirtualVariable;
begin
 Result := TKruptarVirtualVariable.Create(Owner as TKruptarVirtualModule);
 AddCreated(Result);
end;

constructor TKruptarVirtualFields.Create(AOwner: TKruptarVirtualModule);
begin
 inherited Create;
 Owner := AOwner;
end;

function TKruptarVirtualFields.GetByName(const Name: String): TKruptarVirtualField;
var
 Item: ^TKruptarVirtualField;
 I: Integer;
begin
 Item := RootLink;
 for I := 0 to Count - 1 do
 begin
  Result := Item^;
  if Result.FName = Name then Exit;
  Inc(Item);
 end;
 Result := nil;
end;

function TKruptarVirtualFields.GetMethodByIndex(
  Index: Integer): TKruptarVirtualMethod;
begin
 Result := Nodes[Index] as TKruptarVirtualMethod;
end;

function TKruptarVirtualFields.GetMethodByName(
  const Name: String): TKruptarVirtualMethod;
begin
 try
  Result := GetByName(Name) as TKruptarVirtualMethod;
 except
  Result := nil;
 end;
end;

function TKruptarVirtualFields.GetVarByIndex(
  Index: Integer): TKruptarVirtualVariable;
begin
 Result := Nodes[Index] as TKruptarVirtualVariable;
end;

function TKruptarVirtualFields.GetVarByName(
  const Name: String): TKruptarVirtualVariable;
begin
 try
  Result := GetByName(Name) as TKruptarVirtualVariable;
 except
  Result := NIL;
 end;
end;

function TKruptarVirtualMachine.AddModule: TKruptarVirtualModule;
begin
 Result := AddNode as TKruptarVirtualModule;
end;

procedure TKruptarVirtualMachine.ClearData;
begin
 FEntryPoint.Free;
 FMainModule.Free;
 Finalize(FStack);
 inherited;
end;

procedure TKruptarVirtualMachine.Exec;
asm
 push   esi
 mov    esi,eax
@@ExecLoop:
 mov    edx,[esi + Offset FRegisters.PP]
 mov    dl, byte ptr [edx]
 and    edx,$3F
 lea    ecx,[esi + edx * 8 + Offset FPtrTable]
 mov    eax,[ecx + 4]
 call   dword ptr [ecx]
 cmp    byte ptr [esi + Offset FPaused],0
 jz     @@ExecLoop
 pop    esi
end;

function TKruptarVirtualMachine.ExecuteMethod(const ModuleName, Name: String;
  const ParamList: Array of Const; StepMode: Boolean): Integer;
Var Method: TKruptarVirtualMethod; Module: TKruptarVirtualModule; J, L: Integer;
begin
 If Assigned(FMainModule) then
 begin
  If ModuleName = '' then Method := FMainModule.FMethods.MethodNames[Name] Else
  begin
   Module := FMainModule.FindModule(ModuleName);
   If Module = NIL then
   begin
    Result := KVME_ModuleNotFound;
    Exit;
   end Else Method := Module.FMethods.MethodNames[Name];
  end;
  If Assigned(Method) then
  begin
   If Method.FCallingConvention = ccNone then
   begin
    L := Length(ParamList);
    If (L = Method.FParamCount) and (L <= 16) then
    begin
     FillChar(FRegisters, SizeOf(FRegisters), 0);
     With FRegisters do
     For J := 0 to Pred(L) do With ParamList[J] do
     Case VType of
      vtInteger: I[J] := VInteger;
      vtBoolean: C[J] := Byte(VBoolean);
      vtChar: C[J] := Byte(VChar);
      vtExtended, vtString, vtPointer, vtPChar, vtObject,
      vtClass, vtPWideChar, vtAnsiString, vtCurrency,
      vtVariant, vtInterface, vtWideString, vtInt64: C[J] := Cardinal(VPointer);
      vtWideChar: C[J] := Word(VWideChar);
     end;
     L := $FF; //stop
     With Method do If FCodeSize > 0 then
     begin
      FRegisters.ESP := NIL;
      FRegisters.EMP := NIL;
      FRegisters.EPP := NIL;
      FRegisters.RMP := NIL;
      FRegisters.RPP := Addr(L);
      FRegisters.MP := Method;
      FRegisters.PP := FCode;
      FExecuted := True;
      FPaused := StepMode;
      If not StepMode then
      try
       Result := Proceed;
      except
       Result := FLastError;
      end Else Result := KVME_OK;
     end Else Result := KVME_NoCode;
    end Else Result := KVME_InvalidParameters;
   end Else Result := KVME_UnexpectedError;
  end Else Result := KVME_MethodNotFound;
 end Else Result := KVME_UnexpectedError;
 FLastError := Result;
end;

function TKruptarVirtualMachine.GetByIndex(Index:Integer):TKruptarVirtualModule;
begin
 Result := Nodes[Index] as TKruptarVirtualModule;
end;

function TKruptarVirtualMachine.GetByName(const Name: String): TKruptarVirtualModule;
var
 Item: ^TKruptarVirtualModule;
 I: Integer;
begin
 Item := RootLink;
 for I := 0 to Count - 1 do
 begin
  Result := Item^;
  if Result.FName = Name then Exit;
  Inc(Item);
 end;
 Result := nil;
end;

function TKruptarVirtualMachine.GetStackSize: Integer;
begin
 Result := Length(FStack) shl 2;
end;

procedure TKruptarVirtualMachine.IAriphmetic;
asm
 add    eax,Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP], 2
 movzx  edx, word ptr [edx]
 push   esi
 mov    cl,dl
 and    ecx,$0F
 mov    esi,[ecx * 4 + ModRegProcs]
 shr    edx,6
 mov    cx,dx
 and    ecx,$1F
 shl    ecx,2
 add    ecx,eax
 push   ecx
 shr    edx,5
 mov    cx,dx
 and    ecx,1
 shr    edx,1
 test   cl,cl
 jnz    @@Extended
 inc    edx
 inc    edx //AProc(RC^, EDX + 2)
 pop    eax
 call   esi
 jmp    @@Exit
@@Extended:
 shr    edx,1
 jmp    dword ptr [edx * 4 + @@PtrTable]
@@PtrTable:
 dd     @@Flag0
 dd     @@Flag1
 dd     @@Flag2
 dd     @@Flag3
 dd     @@Flag4
 dd     @@Flag5
 dd     @@Flag6
 dd     @@Flag7
@@Flag0: //AProc(RC^, 0);
 xor    edx,edx
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag1: //AProc(RC^, PByte(PC^));
 mov    edx, [eax + Offset TKruptarVirtualRegisters.PP]
 inc    dword ptr [eax + Offset TKruptarVirtualRegisters.PP]
 movzx  edx, byte ptr [edx]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag2: //AProc(RC^, PWord(PC^));
 mov    edx, [eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],2
 movzx  edx, word ptr [edx]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag3: //AProc(RC^, PCardinal(PC^));
 mov    edx, [eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],4
 mov    edx, dword ptr [edx]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag4: //AProc(RC^, PShortInt(PC^));
 mov    edx, [eax + Offset TKruptarVirtualRegisters.PP]
 inc    dword ptr [eax + Offset TKruptarVirtualRegisters.PP]
 movsx  edx, byte ptr [edx]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag5: //AProc(RC^, PSmallInt(PC^));
 mov    edx, [eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],2
 movsx  edx, word ptr [edx]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag6: //AProc(RC^, HeapPointer);
 mov    edx,[eax + Offset TKruptarVirtualRegisters.MP]
 mov    edx,[edx + Offset TKruptarVirtualField.FModule]
 mov    edx,[edx + Offset TKruptarVirtualModule.FHeap]
 pop    eax
 call   esi
 jmp    @@Exit
@@Flag7: //AProc(RC^, 1);
 mov    edx,1
 pop    eax
 call   esi
@@Exit:
 pop    esi
end;

procedure TKruptarVirtualMachine.IAriphmetic64;
asm
 push   ebx
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP], 2
 movzx  edx,word ptr [ecx]
 shr    edx,6
 mov    ecx,edx
 and    ecx,15
 shr    edx,4
 mov    eax,edx
 and    eax,7
 shr    edx,3
 lea    eax,[ebx + eax * 8]
 lea    edx,[ebx + edx * 8]
 call   dword ptr [ecx * 4 + AProcs64]
 pop    ebx
end;

procedure TKruptarVirtualMachine.IAriphmeticR;
asm
 add    eax, Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP], 2
 movzx  ecx, word ptr [edx]
 push   esi
 mov    dl,cl
 and    edx,$0F
 mov    esi,[edx * 4 + ModRegProcs2]
 shr    ecx,6
 mov    edx,ecx
 shr    edx,5
 mov    edx,[eax + edx * 4]
 and    ecx,$1F
 shl    ecx,2
 add    eax,ecx
 call   esi
 pop    esi
end;

Type
 TAsmProc = procedure;

procedure KVM_CC_Register; forward;
procedure KVM_CC_Pascal; forward;
procedure KVM_CC_CDecl; forward;
procedure KVM_CC_StdCall; forward;
procedure KVM_CC_SafeCall; forward;

Const
 KVM_CC_PtrTable: Array[0..4] of TAsmProc = (
 KVM_CC_Register,
 KVM_CC_Pascal,
 KVM_CC_CDecl,
 KVM_CC_StdCall,
 KVM_CC_SafeCall);

procedure KVM_CallError; forward;
procedure KVM_CE_InvalidCC; forward;
procedure KVM_CE_TooManyParams; forward;

procedure TKruptarVirtualMachine.ICall;
asm
 add    eax,Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 mov    cl,byte ptr [edx]
 shr    cl,6
 sub    cl,1
 jb     @@_B1
 jz     @@_B2
 dec    cl
 jz     @@_BL
//BLM
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],5
 push   ebx
 mov    ebx,eax
 movzx  ecx,word ptr [edx + 1] //ECX = Module Index
 movzx  edx,word ptr [edx + 3] //EDX = Method Index
 mov    eax,[ebx + Offset TKruptarVirtualRegisters.MP]
 mov    eax,[eax + Offset TKruptarVirtualMethod.FModule]
 cmp    cx,0
 je     @@UseCurrentModule
 dec    ecx
 mov    eax,[eax + Offset TKruptarVirtualModule.FUses]
 mov    eax,[eax + ecx * 4]
@@UseCurrentModule:
 mov    eax,[eax + Offset TKruptarVirtualModule.FMethods]
 call   TKruptarVirtualFields.GetMethodByIndex
 movzx  ecx,byte ptr [eax + Offset TKruptarVirtualMethod.FCallingConvention]
 cmp    cl,0
 jne    @@ExternalCall
 mov    edx,[ebx + Offset TKruptarVirtualRegisters.MP]
 mov    [ebx + Offset TKruptarVirtualRegisters.RMP],edx //RMP = MP
 mov    edx,[ebx + Offset TKruptarVirtualRegisters.PP]
 mov    [ebx + Offset TKruptarVirtualRegisters.RPP],edx //RPP = PP
 mov    [ebx + Offset TKruptarVirtualRegisters.MP],eax //MP = Methods[EAX]
 mov    edx,[eax + Offset TKruptarVirtualMethod.FCode]
 mov    [ebx + Offset TKruptarVirtualRegisters.PP],edx //PP = FCode
 pop    ebx
 ret
@@ExternalCall:
 cmp    cl,ccSafeCall
 ja     KVM_CE_InvalidCC
 dec    ecx
 jmp    [ecx * 4 + KVM_CC_PtrTable]
@@_B1:
 mov    edx,[edx + 1]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],edx
 ret
@@_B2:
 movsx  edx,word ptr [edx + 1]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],edx
 ret
@@_BL:
 mov    ecx,[eax + Offset TKruptarVirtualRegisters.MP]
 mov    [eax + Offset TKruptarVirtualRegisters.RMP],ecx //RMP = MP
 mov    ecx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    ecx,5
 mov    [eax + Offset TKruptarVirtualRegisters.RPP],ecx //RPP = PP + 5
 mov    edx,[edx + 1]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],edx
end;
procedure KVM_CE_InvalidCC;
asm
 mov    ecx,[KVMS_InvalidCallingConvention]
 jmp    KVM_CallError
end;
procedure KVM_CE_TooManyParams;
asm
 mov    ecx,[KVMS_TooManyParameters]
 jmp    KVM_CallError
end;
procedure KVM_CallError;
asm
 mov    dl,1
 mov    eax,[KVM_CallErrorExceptionInfo]
 call   Exception.Create
 call   System.@RaiseExcept
end;
procedure KVM_CC_Register;
asm
 push   esi
 mov    ecx,[eax + Offset TKruptarVirtualMethod.FParamCount]
 mov    esi,eax
 cmp    ecx,0
 je     @@Proceed
 push   ebx
 mov    ebx,[ebx + Offset TKruptarVirtualRegisters.SP]
 cmp    ecx,4
 jb     @@NoPush
// cmp    ecx,16
// ja     KVM_CE_TooManyParams
 sub    ecx,3
 mov    edx,12
@@PushLoop:
 push   dword ptr [ebx + edx]
 add    edx,4
 loop   @@PushLoop
 mov    cl,3
@@NoPush:
 sub    cl,1
 jb     @@Proceed
 jz     @@_EAX
 dec    cl
 jz     @@_EDX
 mov    ecx,[ebx + 8]
@@_EDX:
 mov    edx,[ebx + 4]
@@_EAX:
 mov    eax,[ebx]
 pop    ebx
@@Proceed:
 call   dword ptr [esi + Offset TKruptarVirtualMethod.FCode]
 cmp    byte ptr [esi + Offset TKruptarVirtualMethod.FIsFunction],1
 jne    @@NoResult
 mov    [ebx],eax
@@NoResult:
 pop    esi
 pop    ebx
end;
procedure KVM_CC_Pascal;
asm
 push   esi
 mov    ecx,[eax + Offset TKruptarVirtualMethod.FParamCount]
 cmp    ecx,0
 je     @@Proceed
 mov    esi,[ebx + Offset TKruptarVirtualRegisters.SP]
// cmp    ecx,16
// ja     KVM_CE_TooManyParams
 mov    edx,0
@@PushLoop:
 push   dword ptr [esi + edx]
 add    edx,4
 loop   @@PushLoop
@@Proceed:
 mov    esi,eax
 call   dword ptr [esi + Offset TKruptarVirtualMethod.FCode]
 cmp    byte ptr [esi + Offset TKruptarVirtualMethod.FIsFunction],1
 jne    @@NoResult
 mov    [ebx],eax
@@NoResult:
 pop    esi
 pop    ebx
end;
procedure KVM_CC_Cdecl;
asm
 push   edi
 push   esi
 mov    ecx,[eax + Offset TKruptarVirtualMethod.FParamCount]
 cmp    ecx,0
 je     @@Proceed
// cmp    ecx,16
// ja     KVM_CE_TooManyParams
 mov    esi,ecx
 shl    esi,2
 sub    esp,esi
 mov    edi,esp
 mov    esi,[ebx + Offset TKruptarVirtualRegisters.SP]
 rep    movsd
@@Proceed:
 mov    esi,eax
 call   dword ptr [esi + Offset TKruptarVirtualMethod.FCode]
 mov    edx,[esi + Offset TKruptarVirtualMethod.FParamCount]
 lea    esp,[esp + edx * 4]
 cmp    byte ptr [esi + Offset TKruptarVirtualMethod.FIsFunction],1
 jne    @@NoResult
 mov    [ebx],eax
@@NoResult:
 pop    esi
 pop    edi
 pop    ebx
end;
procedure KVM_CC_StdCall;
asm
 push   edi
 push   esi
 mov    ecx,[eax + Offset TKruptarVirtualMethod.FParamCount]
 cmp    ecx,0
 je     @@Proceed
// cmp    ecx,16
// ja     KVM_CE_TooManyParams
 mov    esi,ecx
 shl    esi,2
 sub    esp,esi
 mov    edi,esp
 mov    esi,[ebx + Offset TKruptarVirtualRegisters.SP]
 rep    movsd
@@Proceed:
 mov    esi,eax
 call   dword ptr [esi + Offset TKruptarVirtualMethod.FCode]
 cmp    byte ptr [esi + Offset TKruptarVirtualMethod.FIsFunction],1
 jne    @@NoResult
 mov    [ebx],eax
@@NoResult:
 pop    esi
 pop    edi
 pop    ebx
end;
procedure KVM_CC_SafeCall;
asm
 push   edi
 push   esi
 mov    ecx,[eax + Offset TKruptarVirtualMethod.FParamCount]
 cmp    byte ptr [eax + Offset TKruptarVirtualMethod.FIsFunction],1
 jne    @@CheckParamCount
 push   ebx
@@CheckParamCount:
 cmp    ecx,0
 je     @@Proceed
// cmp    ecx,16
// ja     KVM_CE_TooManyParams
 mov    esi,ecx
 shl    esi,2
 sub    esp,esi
 mov    edi,esp
 mov    esi,[ebx + Offset TKruptarVirtualRegisters.SP]
 rep    movsd
@@Proceed:
 mov    esi,eax
 call   dword ptr [esi + Offset TKruptarVirtualMethod.FCode]
 call   System.@CheckAutoResult
 pop    esi
 pop    edi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ICompare;
asm
 add    eax, Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP], 3
 movzx  ecx, byte ptr [edx]
 movzx  edx, word ptr [edx + 1]
 shl    edx,8
 or     ecx,edx
 shr    ecx,6
 mov    dl,cl
 and    edx,$1F
 shr    ecx,5
 lea    edx,[eax + edx * 4]
 push   edx
 mov    dl,cl
 shr    ecx,5
 mov    dh,cl
 and    edx,$71F
 shr    ecx,3
 xchg   dh,cl
 push   esi
 mov    esi,eax
 mov    eax,[esi + Offset TKruptarVirtualRegisters.PP]
 inc    dword ptr [esi + Offset TKruptarVirtualRegisters.PP]
 movzx  eax, byte ptr [eax]
 shl    eax,13
 or     edx,eax
 mov    ecx,[ecx * 4 + CompareFuncs + 8]
 movzx  eax,dl
 mov    eax,[esi + eax * 4]
 shl    edx,11
 sar    edx,19
 pop    esi
 call   ecx
 pop    edx
 mov    [edx],eax
end;

procedure TKruptarVirtualMachine.ICompare64;
asm
 push   ebx
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],3
 movzx  edx, byte ptr [ecx]
 movzx  ecx, word ptr [ecx + 1]
 shl    ecx,8
 or     edx,ecx
 shr    edx,6
 mov    ecx,edx
 shr    edx,3
 and    ecx,7
 mov    eax,edx
 shr    edx,5
 and    eax,31
 push   edi
 lea    edi,[ebx + eax * 4]
 mov    eax,edx
 shr    edx,3
 and    eax,7
 lea    eax,[ebx + eax * 8]
 push   esi
 mov    esi,edx
 shr    edx,1
 cmp    ecx,6
 jae    @@CompareWithNumber
 test   esi,1
 jnz    @@Reg32
//@@Reg64:
 and    edx,7
 lea    edx,[ebx + edx * 8]
 call   dword ptr [ecx * 4 + CProcs64]
 mov    [edi],eax
 pop    esi
 pop    edi
 pop    ebx
 ret
@@Reg32:
 mov    esi,edx
 shr    edx,1
 mov    edx,[ebx + edx * 4]
 test   esi,1
 jz     @@Unsigned
//@@Signed:
 call   dword ptr [ecx * 4 + CProcs64_S32]
 mov    [edi],eax
 pop    esi
 pop    edi
 pop    ebx
 ret
@@Unsigned:
 call   dword ptr [ecx * 4 + CProcs64_U32]
 mov    [edi],eax
 pop    esi
 pop    edi
 pop    ebx
 ret
@@CompareWithNumber:
 sub    ecx,6
 shl    esi,6
 or     edx,esi
 shl    edx,25
 sar    edx,25
 call   dword ptr [ecx * 4 + CProcs64_S32]
 mov    [edi],eax
 pop    esi
 pop    edi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ICompareR;
asm
 add    eax, Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],3
 movzx  ecx, byte ptr [edx]
 movzx  edx, word ptr [edx + 1]
 shl    edx,8
 or     ecx,edx
 shr    ecx,6
 mov    dl,cl
 and    edx,$1F
 shr    ecx,5
 lea    edx,[eax + edx * 4]
 push   edx
 mov    dl,cl
 shr    ecx,5
 mov    dh,cl
 and    dx,$71F
 shr    ecx,3
 xchg   dh,cl
 push   esi
 mov    esi,eax
 mov    ecx,[ecx * 4 + CompareFuncs + 8]
 movzx  eax,dl
 mov    eax,[esi + eax * 4]
 movzx  edx,dh
 mov    edx,[esi + edx * 4]
 pop    esi
 call   ecx
 pop    edx
 mov    [edx],eax
end;

procedure TKruptarVirtualMachine.IConditionalBranch;
asm
 push   ebx
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 mov    ecx,[ecx]
 push   esi
 mov    dl,cl
 and    edx,$0F
 mov    esi,[edx * 4 + CompareFuncs]
 shr    ecx,6
 mov    dl,cl
 shr    ecx,5
 mov    dh,cl
 and    dx,$1F1F
 shr    ecx,5
 movsx  ecx,cx
 xchg   ecx,esi
 movzx  eax,dl
 mov    eax,[ebx + eax * 4]
 movzx  edx,dh
 mov    edx,[ebx + edx * 4]
 call   ecx
 test   al,al
 jz     @@Proceed
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],esi
 pop    esi
 pop    ebx
 ret
@@Proceed:
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],4
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.IConditionalBranch2;
asm
end;

procedure TKruptarVirtualMachine.IFloat32;
asm
 push   ebx
 push   esi
 lea    esi,[eax + offset FRegisters]
 mov    ecx,[esi + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [esi + Offset TKruptarVirtualRegisters.PP],3
 movzx  ebx, byte ptr [ecx]
 movzx  ecx, word ptr [ecx + 1]
 shl    ecx,8
 or     ebx,ecx
 shr    ebx,6
 mov    ecx,ebx
 shr    ebx,5
 and    ecx,31
 mov    eax,ebx
 shr    ebx,5
 and    eax,31
 lea    ecx,[esi + ecx * 4]
 lea    eax,[esi + eax * 4]
 mov    edx,ebx
 shr    ebx,5
 and    edx,31
 lea    edx,[esi + edx * 4]
 call   dword ptr [ebx * 4 + F32Procs]
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.IFloat32a;
asm
 push   ebx
 lea    ebx,[eax + offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
 movzx  edx,word ptr [ecx]
 shr    edx,6
 mov    ecx,edx
 shr    edx,3
 and    ecx,7
 mov    eax,edx
 shr    edx,3
 and    eax,7
 lea    eax,[ebx + eax * 4]
 lea    edx,[ebx + edx * 4]
 call   dword ptr [ecx * 4 + F32aProcs]
 pop    ebx
end;

procedure TKruptarVirtualMachine.IFloat64;
asm
 push   ebx
 lea    ebx,[eax + offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
 movzx  edx,word ptr [ecx]
 shr    edx,6
 mov    ecx,edx
 shr    edx,4
 and    ecx,15
 mov    eax,edx
 shr    edx,3
 and    eax,7
 lea    eax,[ebx + eax * 8]
 lea    edx,[ebx + edx * 8]
 call   dword ptr [ecx * 4 + F64Procs]
 pop    ebx
end;

procedure TKruptarVirtualMachine.IFloatCompare;
asm
 push   ebx
 push   esi
 lea    ebx,[eax + offset FRegisters]
 mov    edx,[ebx + offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + offset TKruptarVirtualRegisters.PP],2
 movzx  edx,word ptr [edx]
 mov    esi,edx
 shr    edx,6
 and    esi,63
 sub    esi,$36
 mov    ecx,edx
 shr    edx,3
 and    ecx,7
 lea    ecx,[ebx + ecx * 4]
 mov    eax,edx
 shr    edx,3
 and    eax,7
 lea    eax,[ebx + ecx * 8]
 test   edx,1
 jnz    @@Single
//Double:
 shr    edx,1
 lea    edx,[ebx + edx * 8]
 call   dword ptr [esi * 4 + FC64Procs]
 pop    esi
 pop    ebx
 ret
@@Single:
 shr    edx,1
 lea    edx,[ebx + edx * 4]
 call   dword ptr [esi * 4 + FC64xProcs]
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ILDMIA;
asm
 push   ebx
 push   esi
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],3
 movzx  edx,byte ptr [ecx]
 shr    edx,6
 lea    esi,[ebx + edx * 4]
 movzx  eax,word ptr [ecx + 1]
 cmp    ax,0
 jne    @@NoZero
 mov    edx,[esi]
 mov    edx,[edx]
 mov    [ebx + 20 * 4],edx
 add    dword ptr [esi],4
 pop    esi
 pop    ebx
 ret
@@NoZero:
 mov    ecx,4
@@TestAndLoad:
 test   al,1
 jz     @@Proceed
 mov    edx,[esi]
 mov    edx,[edx]
 mov    [ebx + ecx * 4],edx
 add    dword ptr [esi],4
@@Proceed:
 shr    eax,1
 cmp    ax,0
 je     @@Exit
 inc    ecx
 cmp    ecx,20
 jb     @@TestAndLoad
@@Exit:
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ILogic;
asm
 add    eax, Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],3
 movzx  ecx, byte ptr [edx]
 movzx  edx, word ptr [edx + 1]
 shl    edx,8
 or     ecx,edx
 shr    ecx,6
 mov    dl,cl
 and    edx,$1F
 shr    ecx,5
 lea    edx,[eax + edx * 4]
 push   edx
 mov    dl,cl
 shr    ecx,5
 mov    dh,cl
 and    edx,$71F
 shr    ecx,3
 xchg   dh,cl
 test   cl,1
 jz     @@NotOdd
 push   esi
 mov    esi,eax
 mov    ecx,[ecx * 4 + CalcRegFuncs]
 movzx  eax,dl
 mov    eax,[esi + eax * 4]
 movzx  edx,dh
 mov    edx,[esi + edx * 4]
 pop    esi
 call   ecx
 pop    edx
 mov    [edx],eax
 ret
@@NotOdd:
 push   esi
 mov    esi,eax
 cmp    cl,2
 je     @@_ADD
 mov    ecx,[ecx * 4 + CalcRegFuncs]
 movzx  eax,dl
 mov    eax,[esi + eax * 4]
 movzx  edx,dh
 pop    esi
 call   ecx
 pop    edx
 mov    [edx],eax
 ret
@@_ADD:
 mov    eax,[esi + Offset TKruptarVirtualRegisters.PP]
 inc    dword ptr [esi + Offset TKruptarVirtualRegisters.PP]
 movzx  eax, byte ptr [eax]
 shl    eax,13
 or     edx,eax
 mov    ecx,[ecx * 4 + CalcRegFuncs]
 movzx  eax,dl
 mov    eax,[esi + eax * 4]
 shl    edx,11
 sar    edx,19
 pop    esi
 call   ecx
 pop    edx
 mov    [edx],eax
end;

procedure TKruptarVirtualMachine.ILogic64;
asm
 push   ebx
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
 movzx  edx,word ptr [ecx]
 shr    edx,6
 mov    ecx,edx
 and    ecx,3
 shr    edx,2
 mov    eax,edx
 and    eax,7
 shr    edx,3
 lea    eax,[ebx + eax * 8]
 mov    edx,[ebx + edx * 4]
 call   dword ptr [ecx * 4 + LProcs64]
 pop    ebx
end;

procedure TKruptarVirtualMachine.IMemoryOperate;
asm
 push   ebx
 add    eax,Offset FRegisters
 mov    ecx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP],4
 mov    ecx,[ecx]
 mov    edx,ecx
 shr    edx,11
 and    edx,$1F
 mov    edx,[eax + edx * 4]
 mov    ebx,ecx
 sar    ebx,19
 add    edx,ebx
 mov    ebx,ecx
 shr    ebx,16
 and    ebx,7
 mov    ebx,[ebx * 4 + MemOpProcs]
 shr    ecx,6
 and    ecx,$1F
 lea    eax,[eax + ecx * 4]
 call   ebx
 pop    ebx
end;

procedure TKruptarVirtualMachine.IMemoryOperateR;
asm
 push   ebx
 add    eax, Offset FRegisters
 mov    edx,[eax + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [eax + Offset TKruptarVirtualRegisters.PP], 3
 movzx  ecx, byte ptr [edx]
 movzx  edx, word ptr [edx + 1]
 shl    edx,8
 or     ecx,edx
 mov    edx,ecx
 shr    edx,11
 and    edx,$1F
 mov    edx,[eax + edx * 4]
 mov    ebx,ecx
 shr    ebx,19
 add    edx,[eax + ebx * 4]
 mov    ebx,ecx
 shr    ebx,16
 and    ebx,7
 mov    ebx,[ebx * 4 + MemOpProcs]
 shr    ecx,6
 and    ecx,$1F
 lea    eax,[eax + ecx * 4]
 call   ebx
 pop    ebx
end;

procedure KVM_StackOverflow;
begin
 raise EVirtualStackError.Create(KVMS_VirtualStackOverflow);
end;

procedure KVM_StackUnderflow;
begin
 raise EVirtualStackError.Create(KVMS_VirtualStackUnderflow);
end;

procedure TKruptarVirtualMachine.Initialize;
var
 I: Integer;
begin
 NodeClass := TKruptarVirtualModule;
 FAssignableClass := ClassType;
 FMainModule := TKruptarVirtualModule.Create(Self);
 FEntryPoint := TKruptarVirtualMethod.Create(FMainModule);
 FEntryPoint.SetCodeSize(1);
 PByte(FEntryPoint.FCode)^ := 255; //stop
 SetStackSize(65536);
 for I := $00 to $0C do FPtrTable[I] := IAriphmetic;
 for I := $10 to $1F do FPtrTable[I] := IAriphmeticR;
 for I := $20 to $29 do FPtrTable[I] := IConditionalBranch;
 for I := $36 to $3B do FPtrTable[I] := IFloatCompare;
 FPtrTable[$0D] := ILogic;
 FPtrTable[$0E] := ICompare;
 FPtrTable[$0F] := ICompareR;
 FPtrTable[$2A] := IMemoryOperate;
 FPtrTable[$2B] := IMemoryOperateR;
 FPtrTable[$2C] := ILDMIA;
 FPtrTable[$2D] := ISTMIA;
 FPtrTable[$2E] := ICall;
 FPtrTable[$2F] := IOther;
 FPtrTable[$30] := IAriphmetic64;
 FPtrTable[$31] := ILogic64;
 FPtrTable[$32] := ICompare64;
 FPtrTable[$33] := IFloat32;
 FPtrTable[$34] := IFloat32a;
 FPtrTable[$35] := IFloat64;
 FPtrTable[$3C] := IUnknown;
 FPtrTable[$3D] := IUnknown;
 FPtrTable[$3E] := IUnknown;
 FPtrTable[$3F] := ISystem;
end;

procedure TKruptarVirtualMachine.IOther;
asm
 push   ebx
 lea    ebx,[eax + Offset FRegisters]
 mov    eax,[ebx + Offset TKruptarVirtualRegisters.PP]
 mov    cl,byte ptr [eax]
 shr    cl,6
 mov    dl,cl
 and    dl,1
 cmp    dl,1
 je     @@Flag2
//PUSH/POP
 add    dword [ebx + Offset TKruptarVirtualRegisters.PP],4
 shr    cl,1
 cmp    cl,1
 je     @@_POP
//PUSH
 mov    eax,[eax]
 shr    eax,8
 cmp    eax,0
 jne    @@PushNoZero
(* CHECK STACK BEGIN *)
 mov    edx, [ebx + (Offset FStack - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],edx
 je     KVM_StackOverflow
(* CHECK STACK END *)
 sub    dword ptr [ebx + 27 * 4],4
 mov    edx,[ebx + 7 * 4]
 mov    eax,[ebx + 27 * 4]
 mov    [eax],edx
 pop    ebx
 ret
@@PushNoZero:
 push   edi
 mov    ecx,8 * 4
@@TestAndStore:
 test   al,1
 jz     @@PushProceed
(* CHECK STACK BEGIN *)
 mov    edx, [ebx + (Offset FStack - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],edx
 je     KVM_StackOverflow
(* CHECK STACK END *)
 mov    edx,[ebx + ecx]
 sub    dword ptr [ebx + 27 * 4],4
 mov    edi,[ebx + 27 * 4]
 mov    [edi],edx
@@PushProceed:
 shr    eax,1
 cmp    eax,0
 je     @@PushExit
 add    ecx,4
 cmp    ecx,31 * 4
 jbe    @@TestAndStore
@@PushExit:
 pop    edi
 pop    ebx
 ret
@@_POP:
 mov    eax,[eax]
 shr    eax,8
 cmp    eax,0
 jne    @@PopNoZero
(* CHECK STACK BEGIN *)
 mov    edx, [ebx + (Offset FStackTop - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],edx
 je     KVM_StackUnderflow
(* CHECK STACK END *)
 mov    edx,[ebx + 27 * 4]
 mov    edx,[edx]
 add    dword ptr [ebx + 27 * 4],4
 mov    [ebx + 7 * 4],edx
 pop    ebx
 ret
@@PopNoZero:
 mov    ecx,31 * 4
@@TestAndLoad:
 test   al,1
 jz     @@PopProceed
(* CHECK STACK BEGIN *)
 mov    edx, [ebx + (Offset FStackTop - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],edx
 je     KVM_StackUnderflow
(* CHECK STACK END *)
 mov    edx,[ebx + 27 * 4]
 mov    edx,[edx]
 add    dword ptr [ebx + 27 * 4],4
 mov    [ebx + ecx],edx
@@PopProceed:
 shr    eax,1
 cmp    eax,0
 je     @@PopExit
 sub    ecx,4
 cmp    ecx,8 * 4
 jae    @@TestAndLoad
@@PopExit:
 pop    ebx
 ret
@@Flag2:
 shr    cl,1
 cmp    cl,1
 je     @@Other
//PUSH/POP/BX/BLX/BLMX/CBD/CWD/SWAP
 movzx  edx,byte ptr [eax + 1]
 mov    ecx,edx
 and    ecx,7 //Get Opcode
 shr    edx,3 //Get Rn
 jmp    dword ptr [ecx * 4 + @@PtrTable]
@@PtrTable:
 dd     @@_PUSH
 dd     @@_POP2
 dd     @@_BX
 dd     @@_BLX
 dd     @@_BLMX
 dd     @@_CBD
 dd     @@_CWD
 dd     @@_SWAP
@@_PUSH:
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
(* CHECK STACK BEGIN *)
 mov    eax, [ebx + (Offset FStack - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],eax
 je     KVM_StackOverflow
(* CHECK STACK END *)
 mov    edx,[ebx + edx * 4]
 sub    dword ptr [ebx + 27 * 4],4
 mov    ecx,[ebx + 27 * 4]
 mov    [ecx],edx
 pop    ebx
 ret
@@_POP2:
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
(* CHECK STACK BEGIN *)
 mov    eax, [ebx + (Offset FStackTop - Offset FRegisters)]
 cmp    dword ptr [ebx + 27 * 4],eax
 je     KVM_StackUnderflow
(* CHECK STACK END *)
 mov    eax,[ebx + 27 * 4]
 mov    eax,[eax]
 add    dword ptr [ebx + 27 * 4],4
 mov    [ebx + edx * 4],eax
 pop    ebx
 ret
@@_BX:
 mov    edx,[ebx + edx * 4]
 mov    [ebx + Offset TKruptarVirtualRegisters.PP],edx
 pop    ebx
 ret
@@_BLX:
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.MP]
 mov    [ebx + Offset TKruptarVirtualRegisters.RMP],ecx //RMP = MP
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    ecx,2
 mov    [ebx + Offset TKruptarVirtualRegisters.RPP],ecx //RPP = PP + 2
 mov    edx,[ebx + edx * 4]
 mov    [ebx + Offset TKruptarVirtualRegisters.PP],edx
 pop    ebx
 ret
@@_BLMX:
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],2
 mov    edx,[ebx + edx * 4]
 movzx  ecx,dx //ECX = Module Index
 shr    edx,16 //EDX = Method Index
 mov    eax,[ebx + Offset TKruptarVirtualRegisters.MP]
 mov    eax,[eax + Offset TKruptarVirtualMethod.FModule]
 cmp    cx,0
 je     @@UseCurrentModule
 dec    ecx
 mov    eax,[eax + Offset TKruptarVirtualModule.FUses]
 mov    eax,[eax + ecx * 4]
@@UseCurrentModule:
 mov    eax,[eax + Offset TKruptarVirtualModule.FMethods]
 call   TKruptarVirtualFields.GetMethodByIndex
 movzx  ecx,byte ptr [eax + Offset TKruptarVirtualMethod.FCallingConvention]
 cmp    cl,0
 jne    @@ExternalCall
 mov    edx,[ebx + Offset TKruptarVirtualRegisters.MP]
 mov    [ebx + Offset TKruptarVirtualRegisters.RMP],edx //RMP = MP
 mov    edx,[ebx + Offset TKruptarVirtualRegisters.PP]
 mov    [ebx + Offset TKruptarVirtualRegisters.RPP],edx //RPP = PP
 mov    [ebx + Offset TKruptarVirtualRegisters.MP],eax //MP = Methods[EAX]
 mov    edx,[eax + Offset TKruptarVirtualMethod.FCode]
 mov    [ebx + Offset TKruptarVirtualRegisters.PP],edx //PP = FCode
 pop    ebx
 ret
@@ExternalCall:
 cmp    cl,ccSafeCall
 ja     KVM_CE_InvalidCC
 dec    ecx
 jmp    dword ptr [ecx * 4 + KVM_CC_PtrTable]
@@_CBD:
 add    dword [ebx + Offset TKruptarVirtualRegisters.PP],2
 movsx  ecx,byte ptr [ebx + edx * 4]
 mov    [ebx + edx * 4],ecx
 pop    ebx
 ret
@@_CWD:
 add    dword [ebx + Offset TKruptarVirtualRegisters.PP],2
 movsx  ecx,word ptr [ebx + edx * 4]
 mov    [ebx + edx * 4],ecx
 pop    ebx
 ret
@@_SWAP:
 add    dword [ebx + Offset TKruptarVirtualRegisters.PP],2
 mov    eax,[ebx + edx * 4]
 bswap  eax
 mov    [ebx + edx * 4],eax
 pop    ebx
 ret
@@Other:
//SEQ/SNE
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],4
 mov    ecx,[eax]
 sar    ecx,8
 mov    dl,cl
 and    edx,3
 test   dl,1
 jz     @@NotOdd
 sar    ecx,2
 mov    al,cl
 sar    ecx,5
 mov    ah,cl
 and    ax,$1F1F
 sar    ecx,5
 push   esi
 shr    edx,1
 mov    esi,[edx * 4 + CompareFuncs]
 mov    edx,ecx
 and    edx,$1F
 sar    ecx,5
 mov    edx,[ebx + edx * 4]
 add    edx,ecx
 movzx  ecx,al
 movzx  eax,ah
 mov    eax,[ebx + eax * 4]
 call   esi
 mov    [ebx + ecx * 4],eax
 pop    esi
 pop    ebx
 ret
@@NotOdd:
 sar    ecx,2
 mov    al,cl
 sar    ecx,5
 mov    ah,cl
 and    ax,$1F1F
 sar    ecx,5
 push   esi
 shr    edx,1
 mov    esi,[edx * 4 + CompareFuncs]
 mov    edx,ecx
 movzx  ecx,al
 movzx  eax,ah
 mov    eax,[ebx + eax * 4]
 call   esi
 mov    [ebx + ecx * 4],eax
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ISTMIA;
asm
 push   ebx
 push   esi
 lea    ebx,[eax + Offset FRegisters]
 mov    ecx,[ebx + Offset TKruptarVirtualRegisters.PP]
 add    dword ptr [ebx + Offset TKruptarVirtualRegisters.PP],3
 movzx  edx,byte ptr [ecx]
 shr    edx,6
 lea    esi,[ebx + edx * 4]
 movzx  eax,word ptr [ecx + 1]
 cmp    ax,0
 jne    @@NoZero
 mov    edx,[esi]
 mov    ecx,[ebx + 20 * 4]
 mov    [edx],ecx
 add    dword ptr [esi],4
 pop    esi
 pop    ebx
 ret
@@NoZero:
 push   edi
 mov    ecx,4
@@TestAndStore:
 test   al,1
 jz     @@Proceed
 mov    edi,[esi]
 mov    edx,[ebx + ecx * 4]
 mov    [edi],edx
 add    dword ptr [esi],4
@@Proceed:
 shr    eax,1
 cmp    ax,0
 je     @@Exit
 inc    ecx
 cmp    ecx,20
 jb     @@TestAndStore
@@Exit:
 pop    edi
 pop    esi
 pop    ebx
end;

procedure TKruptarVirtualMachine.ISystem;
asm
 add    eax,Offset FRegisters
 mov    ecx,[eax + Offset TKruptarVirtualRegisters.PP]
 movzx  edx,byte ptr [ecx]
 shr    edx,6
 sub    dl,1
 jb     @@_NOP
 jz     @@_RET
 dec    dl
 jz     @@_RAISE
//STOP
 mov    byte ptr[eax + (Offset FExecuted - Offset FRegisters)],0
 mov    byte ptr[eax + (Offset FPaused - Offset FRegisters)],1
 ret
@@_NOP:
 inc    dword ptr [eax + Offset TKruptarVirtualRegisters.PP]
 ret
@@_RET:
 inc    dword ptr [eax + Offset TKruptarVirtualRegisters.PP]
 mov    edx, [eax + Offset TKruptarVirtualRegisters.RMP]
 mov    [eax + Offset TKruptarVirtualRegisters.MP],edx
 mov    edx, [eax + Offset TKruptarVirtualRegisters.RPP]
 mov    [eax + Offset TKruptarVirtualRegisters.PP],edx
 ret
@@_RAISE:
 mov    eax,[eax + Offset TKruptarVirtualRegisters.r0]
 call   System.@RaiseExcept
end;

procedure TKruptarVirtualMachine.IUnknown;
begin
 raise Exception.Create('Undefined opcode.');
end;

function TKruptarVirtualMachine.Proceed: Integer;
begin
 Result := KVME_OK;
 FLastError := KVME_OK;
 If FExecuted then
 try
  Exec;
 except
  on E: Exception do
  begin
   Result := KVME_ExceptionRaised;
   FLastError := KVME_ExceptionRaised;
   FLastExceptionName := E.ClassName;
   FLastExceptionMessage := E.Message;
   with FRegisters do
   begin
    SP := ESP;
    MP := EMP;
    PP := EPP;
    If PP = NIL then
    begin
     FExecuted := False;
     FPaused := True;
     raise;
    end;
   end;
  end;
 end;
end;

function TKruptarVirtualMachine.Run(StepMode: Boolean): Integer;
Var Stop: Integer;
begin
 If Assigned(FEntryPoint) and Assigned(FMainModule) then
 begin
  Stop := $FF;
  With FEntryPoint do If FCodeSize > 0 then
  begin
   FRegisters.ESP := NIL;
   FRegisters.EMP := NIL;
   FRegisters.EPP := NIL;
   FRegisters.RMP := NIL;
   FRegisters.RPP := Addr(Stop);
   FRegisters.MP := FEntryPoint;
   FRegisters.PP := FCode;
   FExecuted := True;
   FPaused := StepMode;
   If not StepMode then
   try
    Result := Proceed;
   except
    Result := FLastError;
   end Else Result := KVME_OK;
  end Else Result := KVME_NoCode;
 end Else Result := KVME_UnexpectedError;
 FLastError := Result;
end;

procedure TKruptarVirtualMachine.SetStackSize(Value: Integer);
begin
 If Value < 16384 then Value := 16384;
 If Value > 524288 then Value := 524288;
 Value := Value shr 2;
 SetLength(FStack, Value);
 FStackTop := Addr(FStack[Value]);
 FRegisters.SP := FStackTop;
end;

destructor TKruptarVirtualMethod.Destroy;
begin
 If (FCode <> NIL) and (FCallingConvention = ccNone) then
  FreeMem(FCode);
 inherited;
end;

function TKruptarVirtualMethod.GetCode: Pointer;
begin
 If FCallingConvention = ccNone then
  Result := FCode Else
  Result := NIL;
end;

function TKruptarVirtualMethod.GetExternalProc: Pointer;
begin
 If FCallingConvention <> ccNone then
  Result := FCode Else
  Result := NIL;
end;

procedure TKruptarVirtualMethod.Init(PCnt: Integer; IsFunc: Boolean;
  CC: TCallingConvention);
begin
 If (PCnt >= 0) and (PCnt <= 16) then
 begin
  FParamCount := PCnt;
  FIsFunction := IsFunc;
  If (FCode <> NIL) and (FCallingConvention = ccNone) then FreeMem(FCode);
  FCallingConvention := CC;
  FCode := NIL;
  FCodeSize := 0;
 end else Exception.CreateFmt(KVMS_IllegalInitializeCall, [ClassName]);
end;

procedure TKruptarVirtualMethod.SetCallingConvention(Value: TCallingConvention);
begin
 If FCallingConvention = Value then Exit;
 If FCallingConvention <> ccNone then
 begin
  FreeMem(FCode);
  FCodeSize := 0;
 end;
 FCode := NIL;
 FCallingConvention := Value;
end;

procedure TKruptarVirtualMethod.SetCodeSize(Value: Integer);
begin
 If FCallingConvention = ccNone then
 begin
  ReallocMem(FCode, Value);
  FCodeSize := Value;
 end else raise Exception.CreateFmt(KVMS_IllegalSetCodeSizeCall, [ClassName]);
end;

procedure TKruptarVirtualMethod.SetExternalProc(Value: Pointer);
begin
 If FCallingConvention <> ccNone then
  FCode := Value Else
  raise Exception.CreateFmt(KVMS_IllegalSetExternalProcCall, [ClassName]);
end;

constructor TKruptarVirtualModule.Create(AOwner: TKruptarVirtualMachine);
begin
 inherited Create;
 Owner := AOwner;
end;

destructor TKruptarVirtualModule.Destroy;
begin
 Finalize(FName);
 Finalize(FOriginalName);
 Finalize(FHeap);
 FillChar(FUses[0], Length(FUses) shl 2, 0);
 Finalize(FUses);
 FFinalization.Free;
 FInitialization.Free;
 FMethods.Free;
 FVariables.Free;
 inherited;
end;

function TKruptarVirtualModule.FindModule(const Name: String):
{RETURN} TKruptarVirtualModule;
var PM: ^TKruptarVirtualModule; I: Integer;
begin
 PM := Pointer(FUses);
 if PM <> NIL then For I := 0 to Length(FUses) - 1 do
 begin
  if PM^.FName = Name then
  begin
   Result := PM^;
   Exit;
  end;
  Inc(PM);
 end;
 Result := NIL;
end;

function TKruptarVirtualVariable.GetVariant: Variant;
begin
 With TVarData(Result) do
 begin
  VType := FVarType;
  If (VType and varArray <> 0) or
     (VType and varByRef <> 0) then
   VPointer := Pointer(Addr(FModule.FHeap[FHeapOffset])^) else
  case VType and varTypeMask of
   varSmallInt: VSmallInt := SmallInt(Addr(FModule.FHeap[FHeapOffset])^);
   varInteger:  VInteger := Integer(Addr(FModule.FHeap[FHeapOffset])^);
   varSingle:   VSingle := Single(Addr(FModule.FHeap[FHeapOffset])^);
   varDouble:   VDouble := Double(Addr(FModule.FHeap[FHeapOffset])^);
   varCurrency: VCurrency := Currency(Addr(FModule.FHeap[FHeapOffset])^);
   varDate:     VDate := TDateTime(Addr(FModule.FHeap[FHeapOffset])^);
   varOleStr:   VOleStr := Pointer(Addr(FModule.FHeap[FHeapOffset])^);
   varDispatch: VDispatch := Pointer(Addr(FModule.FHeap[FHeapOffset])^);
   varError:    VError := HRESULT(Addr(FModule.FHeap[FHeapOffset])^);
   varBoolean:  VBoolean := Boolean(Addr(FModule.FHeap[FHeapOffset])^);
   varUnknown:  VUnknown := Pointer(Addr(FModule.FHeap[FHeapOffset])^);
   varShortInt: VShortInt := ShortInt(Addr(FModule.FHeap[FHeapOffset])^);
   varByte:     VByte := Byte(Addr(FModule.FHeap[FHeapOffset])^);
   varWord:     VWord := Word(Addr(FModule.FHeap[FHeapOffset])^);
   varLongWord: VLongWord := LongWord(Addr(FModule.FHeap[FHeapOffset])^);
   varInt64:    VInt64 := Int64(Addr(FModule.FHeap[FHeapOffset])^);
   varString:   VString := Pointer(Addr(FModule.FHeap[FHeapOffset])^);
   varAny:      VAny := Pointer(Addr(FModule.FHeap[FHeapOffset])^);
   else raise EVariantError.Create('Not a variant type.');
  end;
 end;
end;

procedure TKruptarVirtualVariable.SetVariant(Value: Variant);
begin
 With TVarData(Value) do If VType = FVarType then
 begin
  If (VType and varArray <> 0) or
     (VType and varByRef <> 0) then
   Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VPointer else
  case VType of
   varSmallInt: SmallInt(Addr(FModule.FHeap[FHeapOffset])^) := VSmallInt;
   varInteger:  Integer(Addr(FModule.FHeap[FHeapOffset])^) := VInteger;
   varSingle:   Single(Addr(FModule.FHeap[FHeapOffset])^) := VSingle;
   varDouble:   Double(Addr(FModule.FHeap[FHeapOffset])^) := VDouble;
   varCurrency: Currency(Addr(FModule.FHeap[FHeapOffset])^) := VCurrency;
   varDate:     TDateTime(Addr(FModule.FHeap[FHeapOffset])^) := VDate;
   varOleStr:   Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VOleStr;
   varDispatch: Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VDispatch;
   varError:    HRESULT(Addr(FModule.FHeap[FHeapOffset])^) := VError;
   varBoolean:  Boolean(Addr(FModule.FHeap[FHeapOffset])^) := VBoolean;
   varUnknown:  Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VUnknown;
   varShortInt: ShortInt(Addr(FModule.FHeap[FHeapOffset])^) := VShortInt;
   varByte:     Byte(Addr(FModule.FHeap[FHeapOffset])^) := VByte;
   varWord:     Word(Addr(FModule.FHeap[FHeapOffset])^) := VWord;
   varLongWord: LongWord(Addr(FModule.FHeap[FHeapOffset])^) := VLongWord;
   varInt64:    Int64(Addr(FModule.FHeap[FHeapOffset])^) := VInt64;
   varString:   Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VString;
   varAny:      Pointer(Addr(FModule.FHeap[FHeapOffset])^) := VAny;
   else raise EVariantError.Create('Not a variant type.');
  end;
 end else raise EVariantError.Create('Incompatible types.');
end;

procedure TKruptarVirtualFields.Initialize;
begin
 NodeClass := TKruptarVirtualField;
 FAssignableClass := TKruptarVirtualFields;
end;

procedure TKruptarVirtualModule.Initialize;
begin
 FName := '';
 FMethods := TKruptarVirtualFields.Create(Self);
 FVariables := TKruptarVirtualFields.Create(Self);
 FInitialization := TKruptarVirtualMethod.Create(Self);
 FInitialization.SetCodeSize(1);
 PByte(FInitialization.FCode)^ := 255;
 FFinalization := TKruptarVirtualMethod.Create(Self);
 FFinalization.SetCodeSize(1);
 PByte(FFinalization.FCode)^ := 255;
end;

{ TKruptarVirtualField }

constructor TKruptarVirtualField.Create(AOwner: TKruptarVirtualModule);
begin
 inherited Create;
 FModule := AOwner;
end;

end.
