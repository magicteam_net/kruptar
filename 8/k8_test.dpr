program k8_test;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  Classes,
  MyUtils,
  k8_Disasm,
  k8_VirtualMachine,
  BaseParser in 'BaseParser.pas',
  regex in 'regex.pas',
  regexpr in 'regexpr.pas',
  testtrd in 'testtrd.pas',
  k8_asmcompiler in 'k8_asmcompiler.pas';

//,
//  LexYacc,
//  k8_VirtualMachine,
 // KasmParser in 'KASMParser\KAsmParser.pas',
 // KasmLexer in 'KASMParser\KAsmLexer.pas';

type
 xrec = record
  a, b, c: integer;
 end;
 TXTest = class
 private
  F1: Integer;
  F2: Integer;
 public
  constructor Create;
  destructor Destroy; override;
  class function xy: Integer;
  procedure test;
  procedure testvirt; virtual;
 end;
 TXTest2 = class(TXTest)
 public
  procedure testvirt; override;
 end;

Var
 A, B: Integer; p: pointer; x, x3: TXTest; x2: TXTest2;//Parser: TKasmParser;
 xr: xrec = (a: 1; b: 2; c: 3);
{ TXTest }

constructor TXTest.Create;
begin
 inherited;
 F1 := $F1;
 F2 := $F2;
end;

destructor TXTest.Destroy;
begin
 F1 := 0;
 F2 := 0;
 inherited;
end;

procedure TXTest.test;
begin
 //;
end;

procedure TXTest.testvirt;
begin
 F2 := 1;
end;

class function TXTest.xy: Integer;
begin
 xy := 128;
end;

{ TXTest2 }

procedure TXTest2.testvirt;
begin
 inherited;
 F1 := 3;
end;

type
 TClassStruc = packed record
  vmtSelfPtr: TObject;
  vmtIntfTable: PPointerArray;
  vmtAutoTable: PPointerArray;
  vmtInitTable: PPointerArray;
  vmtTypeInfo: Pointer;
  vmtFieldTable: PPointerArray;
  vmtMethodTable: PPointerArray;
  vmtDynamicTable: PPointerArray;
  vmtClassName: PShortString;
  vmtInstanceSize: Integer;
  vmtParent: Pointer;
  vmtSafeCallException: Pointer;
  vmtAfterConstruction: Pointer;
  vmtBeforeDestruction: Pointer;
  vmtDispatch: Pointer;
  vmtDefaultHandler: Pointer;
  vmtNewInstance: Pointer;
  vmtFreeInstance: Pointer;
  vmtDestroy: Pointer;
  vmt: array[0..0] of Pointer;
 end;

procedure testvirtX(obj: TXTest2);
begin
 obj.F1 := 4;
 obj.F2 := 5;
end;

type
 TTestSyntax = class(TCustomSyntax)
  protected
   procedure Initialize; override;
 end;

var
 virtualclass: TClassStruc; XX: TClass; XP: Integer absolute XX;
 w1, w2: PWideChar; iw1: Integer absolute w1; iw2: Integer absolute w2;
 trd: TTestThread; r: tregexprengine; ii: integer;

const
 vc_name: string[12] = 'TVirtualTest';
var
 xxx: set of Byte; WS, WS2: WideString;
 wcs: TWideCharSet; MM: Tmemorymanager;
{ TTestSyntax }

procedure TTestSyntax.Initialize;
var
 SU: ^TSyntaxUnit;
begin
 FEmptyPart.stID := 0;
 UseCRLFasToken := True;
 UseNULLasToken := True;
 SetLength(FLexicalUnits, 5);
 FLexicalUnits[0].luRegEx := TRegexEngine.Create('^[ \t]+');
 FLexicalUnits[0].luID := T_NOTHING;
 FLexicalUnits[1].luRegEx := TRegexEngine.Create('^[+]');
 FLexicalUnits[1].luID := 1;
 FLexicalUnits[2].luRegEx := TRegexEngine.Create('^[0-9]+');
 FLexicalUnits[2].luID := T_TEXT;
 FLexicalUnits[3].luRegEx := TRegexEngine.Create('^[\n]+');
 FLexicalUnits[3].luID := T_CRLF;
 FLexicalUnits[4].luRegEx := TRegexEngine.Create(#0);
 FLexicalUnits[4].luID := T_NULL;
 SetLength(FSyntaxUnits, 3);
 SU := Addr(FSyntaxUnits[0]);
 SetLength(SU.suList, 2);
 with SU.suList[0] do
 begin
  SetLength(svrTokens, 1);
  svrTokens[0].stID := 4;
 end;
 with SU.suList[1] do
 begin
  SetLength(svrTokens, 2);
  svrTokens[0].stID := 5 + 1;
  svrTokens[1].stID := 4;
 end;
 SU := Addr(FSyntaxUnits[1]);
 SetLength(SU.suList, 2);
 with SU.suList[0] do
 begin
  SetLength(svrTokens, 2);
  svrTokens[0].stID := 3;
//  svrTokens[0].stResultPush := True;
  svrTokens[1].stID := 5 + 1;
  svrTokens[1].stAllowMismatch := True;
 end;
 with SU.suList[1] do
 begin
  SetLength(svrTokens, 3);
  svrTokens[0].stID := 5 + 2;
 // svrTokens[0].stResultPush := True;
  svrTokens[1].stID := 3;
 // svrTokens[1].stResultPush := True;
  svrTokens[1].stAllowMismatch := True;
  svrTokens[2].stID := 5 + 1;
  svrTokens[2].stAllowMismatch := True;
 end;
 SU := Addr(FSyntaxUnits[2]);
 SetLength(SU.suList, 1);
 with SU.suList[0] do
 begin
  SetLength(svrTokens, 3);
  svrTokens[0].stID := 2;
 // svrTokens[0].stResultPush := True;
  svrTokens[1].stID := 1;
  svrTokens[1].stAllowMismatch := True;
  svrTokens[2].stID := 5 + 2;
 end;
end;

type
 TTestParser = class(TCustomParser)
  protected
   function GetToken(P: PWideChar; Len: Integer;
            var Info, Expected: TTokenInfo; var AText: WideString;
            var SkipCount: Integer): Integer; override;
 end;

var
 testsyn: TTestSyntax;
 testparser: TTestParser;

{ TTestParser }

function TTestParser.GetToken(P: PWideChar; Len: Integer;
  var Info, Expected: TTokenInfo; var AText: WideString;
  var SkipCount: Integer): Integer;
var I: Integer;
begin
 Result := inherited GetToken(P, Len, Info, Expected, AText, SkipCount);
 for I := 0 to FSSIndex - 1 do
 with FSyntaxStack[I] do
 begin
  writeln(Format('%d %d %d %d', [stSyntaxUnitIndex,
                                 stSyntaxVarIndex,
                                 stSyntaxTokenIndex,
                                 Byte(stSyntaxBlockMatch)]));
 end;
  writeln('----'); 
end;

begin
 testsyn := TTestSyntax.Create;
 try
  testparser := TTestParser.Create(testsyn);
  try
   testparser.Execute('', '1123 + 3456'#10'99 + 411234');
  finally
   testparser.Free;
  end;
 finally
  testsyn.Free;
 end;
 WS := 'AA';
 WS2 := 'VBRABAADDFT';
 Writeln(PCharPosW(Pointer(WS),Pointer(WS2),Length(WS),Length(WS2)));
 if GenerateRegExprEngine('(��[��]�\n?)*',[],r) then
 begin
  if RegExprPos(r, 'CXXXX����'#10'����'#10'��������'#10'����'#10'��������',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  DestroyRegExprEngine(r);
 end;                    //(([ \t\n]+)|(([ \t\n]*)(//.*[\n]*)*))
 if GenerateRegExprEngine('([ \t]+|(;|(//).*)+)+',{
 '([ \t\n\e\l\p\r\f]+|(//.*[\n\e\l\p\r\f]*)+|(/[*][^*]*[*]/)+)+',}[],r) then
 begin
  if RegExprPos(r,'xxx/ ;yyy 234.77 zzz',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r,'xxx  //yyr21y'#10' /*//$12AA'#10'*/ zzz',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
   DestroyregExprEngine(r);
 end;
 if GenerateRegExprEngine('[A-Za-z]+([.][0-9A-Za-z]+)*',[],r) then
 begin
  if RegExprPos(r, 'CXXXX',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, 'CXXXX.A',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, 'CXXXX.A9Package',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, '1CXXXX.A9Package',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  DestroyRegExprEngine(r);
 end;
 if GenerateRegExprEngine('a(bra|bc|b|x)cc',[ref_caseinsensitive],r) then
 begin
  if RegExprPos(r, 'THISISATEST1axCaBraCccabccTEST1THIS',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  DestroyRegExprEngine(r);
 end;
 if GenerateRegExprEngine('^(a)*',[],r) then
 begin
  if RegExprPos(r, 'THISISATEST1TEST1THIS',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, 'TEST1ANOTHER',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  DestroyRegExprEngine(r);
 end;
 if GenerateRegExprEngine('^TEST1',[ref_multiline],r) then
 begin
  if RegExprPos(r, 'THISISATEST1'#10'TEST12',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, 'THISISATEST1'#13'TEST12',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  if RegExprPos(r, 'THISISATEST1'#13#10'TEST12',iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  WS := 'THISISATEST1'#0;
  Word(WS[Length(WS)]) := $2028;
  WS := WS + 'TEST12';
  if WS[1] = WideChar($2028) then;
  for ii := 1 to length(WS) do
  begin
   if WS[ii] = '0' then;
  end;
  if RegExprPos(r, WS,iw1,iw2) then
   Writeln(iw1, ' ', iw2) else
   Writeln('Error');
  DestroyRegExprEngine(r);
 end;
 {@MM.GetMem := @XYGetMem;
 @MM.FreeMem := @XYFreeMem;
 @MM.ReallocMem := @XYReallocMem;
 SetMemoryManager(MM);
 SS := 'yyy';
 trd := ttestthread.Create(True);
 try
  trd.Resume;
  Writeln('Input ''!''');
  repeat
   Readln(S);
   if S = 'x' then;
  until S = '!';
  trd.Suspend;
 finally
  trd.Free;
 end; }
 wcs := TWideCharSet.Create(10, 20);
 try
  wcs.Include(3, 4);
  wcs.Exclude(4, 4);
  wcs.Include([1,1,8]);
  wcs.Include('AAZaaz_009');
  if wcs.IsOwnerOf('a') then
   Writeln('Yes') else
   Writeln('No');
  wcs.Include([Word('a')-1]); 
  wcs.Exclude(Word('a')-1,Word('z'));
  if wcs.IsOwnerOf('a') then
   Writeln('Yes') else
   Writeln('No');
 finally
  wcs.Free;
 end;
 A := 17;
 iw1 := 16;
 iw2 := 32;
 if A in [iw1..iw2] then
  Writeln('ok');
 iw1 := Succ(w2 - w1);
 Writeln(iw1);
 Cardinal(XP) := $FFF80000;
 XP := XP shr 19;
 if XP < 0 then;
 XX := TXTest2;
 Move(Pointer(XP + vmtSelfPtr)^, virtualclass, SizeOf(TClassStruc) - 4);
 virtualclass.vmtParent := Pointer(Integer(TXTest2) + vmtSelfPtr);
 virtualclass.vmtClassName := @vc_name;
 virtualclass.vmt[0] := @testvirtX;
 virtualclass.vmtSelfPtr := Addr(virtualclass.vmt);
 x := TXTest.Create;
 try
  x2 := TXTest2.Create;
  try
   asm
    mov dl,1
    mov eax,dword ptr [virtualclass]
    call TObject.Create
    mov [x3],eax
   end;
   try
    x.F1 := 1;
    x.F2 := 2;
    x.test;
    x2.testvirt;
    x3.testvirt;
    Writeln(x3.ClassName);
    Writeln(x3.ClassParent.ClassName);
    Writeln(TXTest.xy);
    with TStringList.Create do
    begin
     GetMem(p, 16);
     FreeMem(p);
     Free;
    end;
   finally
    x3.Free;
   end;
  finally
   x2.Free;
  end;
 finally
  x.Free;
 end;

// raise Exception.Create('test');
(*Parser := TKasmParser.Create(TKasmYacc, TKasmLexer);
 Parser.Input.Text :=
 'mov r0,$256   ;Test Comment'#13#10 +
 'shl r1,r0,2'#13#10 +
 'add r0,r1'#13#10 +
 'mul r1,r0'#13#10 +
 'push r1'#13#10 +
 'pop r0'#13#10 +
 'end';
 Parser.DebugMode := True;
 If Parser.Parse then
  Writeln('File parsed successfully.') Else
  Writeln('Error occured.');
{ With Parser.Output do
 For A := 0 to Count - 1 do
  writeln(Strings[A]);
 With Parser.Errors do
 For A := 0 to Count - 1 do
  writeln(Strings[A]);}
 Parser.Free;
{ Lexer := TLexer.Create(NIL);
 Parser := TParser.Create(NIL);
 Lexer.yyinput := TMemoryStream.Create;
 Str :=
 'mov r0,$256   ;Test Comment'#13#10 +
 'shl r1,r0,2'#13#10 +
 'add r0,r1'#13#10 +
 'mul r1,r0'#13#10 +
 'push r1'#13#10 +
 'pop r0'#13#10 +
 'end';
 Lexer.yyinput.Write(Str[1], Length(Str));
 Lexer.yyinput.Position := 0;
 Lexer.yyoutput := TMemoryStream.Create;
 Lexer.yyerrorfile := TMemoryStream.Create;
 Parser.yyLexer := Lexer;
 B := Parser.yyparse;
 SetLength(Str, Lexer.yyoutput.Size);
 Lexer.yyoutput.Position := 0;
 Lexer.yyoutput.Read(Str[1], Length(Str));
 System.Writeln(Str);
 SetLength(Str, Lexer.yyerrorfile.Size);
 Lexer.yyerrorfile.Position := 0;
 Lexer.yyerrorfile.Read(Str[1], Length(Str));
 System.Writeln(Format('Stopped at line %d', [Lexer.yyLineNo]));
 System.Writeln(Str);
 Lexer.yyinput.Free;
 Lexer.yyoutput.Free;
 Lexer.yyerrorfile.Free;
 Parser.Free;
 Lexer.Free;
{ Test := TParser.Create;
 yyinit;
 writeln(Test.yyparse);
 For A := 0 to yyoutput.Count - 1 do
  writeln(yyoutput.Strings[A]);
 Test.Free;                         }
 A := -6660666;
 B := -21965;
 A := A div B;
 System.Writeln(A);
 With TKruptarVirtualMachine.Create do
 begin
  Free;
 end;*)
end.
