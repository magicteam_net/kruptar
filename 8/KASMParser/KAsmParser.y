%{
unit KasmParser;

interface

uses SysUtils, Classes, NodeLst, LexYacc;

%}

%token
_ADD	_AND	_B	_BEQ	_BGT	_BGTS	_BHE
_BHES	_BIC	_BIC	_BL	_BLE	_BLES	_BLM
_BLMX	_BLT	_BLTS	_BLX	_BNE	_BX	_CBD
_CBQ	_CDQ	_CWD	_CWQ	_DIV	_DIVS	_FABS
_FADD	_FCDS	_FCQR	_FDIV	_FEXP	_FINT	_FLN
_FMUL	_FNEG	_FRAC	_FRND	_FRSB	_FSEQ	_FSGT
_FSHE	_FSLE	_FSLT	_FSNE	_FSQR	_FSQRT	_FSUB
_FTRN	_LDMIA	_LDR	_LDRB	_LDRH	_LDSB	_LDSH
_MOD	_MOV	_MUL	_MVN	_NOP	_ORR	_POP
_PUSH	_RAISE	_RET	_RMD	_RSB	_SAR	_SEQ
_SGT	_SGTS	_SHE	_SHES	_SHL	_SHR	_SLE
_SLES	_SLT	_SLTS	_SNE	_STMIA	_STOP	_STR
_STRB	_STRH	_SUB	_SWAP	_SWPH	_XADD	_XAND
_XBIC	_XDIV	_XMOD	_XMOV	_XMUL	_XMVN	_XOR
_XORR	_XRDV	_XRSB	_XSAR	_XSEQ	_XSEQS	_XSGT
_XSGTS	_XSHE	_XSHES	_XSHL	_XSHR	_XSLE	_XSLES
_XSLT	_XSLTS	_XSNE	_XSNES	_XSUB	_XXOR

%token
ID
DECIMAL
HEXADECIMAL
ILLEGAL
COMMA
LPAREN
RPAREN
EXPL
LBRAC
RBRAC
MINUS
PLUS
COLON
EOL
STAR
SLASH
DECLARE_CONST

%token
B_BL
BRANCH
END_
FLOAT_SPECIAL1
FLOAT_SPECIAL2
FLOAT_SPECIAL3
HP_REG
LDMIA_STMIA
MEM_OPERATE
OPCODE_
PUSH_POP
REG_
REG_OP
REG_REG
REG_REG_OP
REG_REG_REG
REG_XOP_XOP
REG_XREG_REG
REG_XREG_XOP
REGISTER_
SEQ_SNE
XREG_REG
XREG_XREG
XREGISTER_
XSEQ_XSNE
PADD
PSUB
PMUL
PDIV

%{
type
 TLabel = class;
 TLabelList = class;
 PInstructionInfo = ^TInstructionInfo;
 TInstructionInfo = Record
  Opcode: Integer;
  Mask: Integer;
 end;

 TKasmParser = class(TCustomParser)
 public
  yylval: YYSType;
  yyval: YYSType;
  yyv: array [1..yymaxdepth] of YYSType;
  FLabels: TLabelList;
  FLabelNames: TLabelList;
  FStringList: TStringList;
  TempLabel: TLabel;
  FInfo:  PInstructionInfo;
  FLastOperandMode: (omRegister, omNumber, omLabel, omHP);
  procedure ConsoleMessage(Sender: TObject; const Msg: String);
  procedure Initialize; override;
  destructor Destroy; override;
 end;

 TKasmYacc = Class(TYacc)
 private
 public
  function yyparse: Integer; override;
 end;

 TCParser = TKasmParser;

 TByteArray = Array of Byte;

 TCodeRec = Packed Record
  case Byte of
   0: (ByteCode: PByte);
   1: (WordCode: PWord);
   2: (DWordCode: PCardinal);
 end;

 TLabel = Class
 private
  FCode: TCodeRec;
 private
  function GetInstructionSize: Integer;
  procedure SetInstructionSize(Value: Integer);
 public
  property InstructionSize: Integer
      read GetInstructionSize
     write SetInstructionSize;
  property ByteCode: PByte read FCode.ByteCode;
  property WordCode: PWord read FCode.WordCode;
  property DWordCode: PCardinal read FCode.DWordCode;
  destructor Destroy; override;
 end;
 TLabelName = class
 private
  FName: String;
  FIndex: Integer;
 public
  constructor Create(const Name: String; Index: Integer);
  destructor Destroy; override;
 end;
 TLabelList = class(TNodeList)
 private
 public
  function LAdd: TLabel;
  function NAdd(const Name: String; Index: Integer): TLabelName;
 end;

Const
 KAE_ConstantExpressionViolatesSubrangeBounds =
 'Constant expression violates subrange bounds (%d..%d)'; 

implementation

function TLabel.GetInstructionSize: Integer;
begin
 Result := Length(TByteArray(FCode));
end;

procedure TLabel.SetInstructionSize(Value: Integer);
begin
 SetLength(TByteArray(FCode), Value);
end;

destructor TLabel.Destroy;
begin
 Finalize(TByteArray(FCode));
 inherited;
end;

constructor TLabelName.Create(const Name: String; Index: Integer);
begin
 FName := Name;
 FIndex := Index;
end;

destructor TLabelName.Destroy;
begin
 Finalize(FName);
 inherited;
end;

function TLabelList.LAdd: TLabel;
begin
 Result := TLabel.Create;
 Add.Node := Result;
end;

function TLabelList.NAdd(const Name: String; Index: Integer): TLabelName;
begin
 Result := TLabelName.Create(Name, Index);
 Add.Node := Result;
end;

function Operate(A, OP, B: Integer): Integer;
begin
 Case OP of
  PADD: Result := A + B;
  PSUB: Result := A - B;
  PMUL: Result := A * B;
  Else Result := A div B;
 end;
end;

procedure TKasmParser.ConsoleMessage(Sender: TObject; const Msg: String);
begin
 If Sender is TYacc then
  Writeln('Yacc: ', Msg) Else
 If Sender is TLexer then
  Writeln('Lex: ', Msg) Else
  Writeln(Msg);
end;

procedure TKasmParser.Initialize;
begin
 inherited;
 FStringList := TStringList.Create;
 FLabels := TLabelList.Create;
 FLabelNames := TLabelList.Create;
 TempLabel := TLabel.Create;
 OnDebugMessage := ConsoleMessage;
 OnErrorMessage := ConsoleMessage;
end;

destructor TKasmParser.Destroy;
begin
 TempLabel.Free;
 FLabelNames.Free;
 FLabels.Free;
 FStringList.Free;
 inherited;
end;

function yyparse_(Parser: TCParser): Integer; forward;

function TKasmYacc.yyparse: Integer;
begin
 Result := yyparse_(TCParser(yyOwner));
end;
%}

%%

%{var
 Num: Integer;
 PB: PByte;
%}

program
	: label_def_list program_end
	;

program_end
        : END_ eol_list
        | END_
	;

eol_list
	: EOL
	| eol_list EOL
	;

label_def
	: /* empty */
	| EOL
	| ID COLON instruction EOL
        { FLabelNames.NAdd(FStringList.Strings[$1], FLabels.Count - 1); }
	| ID COLON EOL
        { FLabelNames.NAdd(FStringList.Strings[$1], FLabels.Count); }
        | instruction EOL
        ;

label_def_list
	: label_def
        | label_def_list label_def
        ;

instruction
        : OPCODE_ { With FLabels.LAdd, FInfo^ do
                    begin
                     InstructionSize := 1;
                     ByteCode^ := Mask;
                    end; }
        | REG_OP REGISTER_ COMMA operand_hp
                 { With FLabels.LAdd, FInfo^ do
                   begin
                    InstructionSize := 2;
                    WordCode^ := Mask or ($2 shl 6);
                    Case FLastOperandMode of
                     omRegister: WordCode^ := WordCode^ or ($4 shl 11);
                     omHP: WordCode^ := WordCode^ or ((1 shl 11) or (6 shl 13));
                     omNumber:
                     begin
                      Num := $4;
                      Case Num of
                       0: WordCode^ := WordCode^ or (1 shl 11);
                       1: WordCode^ := WordCode^ or ((1 shl 11) or (7 shl 13));
                       Else If Num > 0 then
                       begin
                        If Num <= 16 then
                         WordCode^ := WordCode^ or ((Num - 1) shl 12);
                        If Num < 256 then
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (1 shl 13));
                         InstructionSize := 3;
                         PB := ByteCode; Inc(PB, 2);
                         PB^ := Num;
                        end else
                        If Num < 65536 then
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (2 shl 13));
                         InstructionSize := 4;
                         PB := ByteCode; Inc(PB, 2);
                         PWord(PB)^ := Num;
                        end else
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (3 shl 13));
                         InstructionSize := 6;
                         PB := ByteCode; Inc(PB, 2);
                         PInteger(PB)^ := Num;
                        end;
                       end Else
                       begin
                        If Num >= -128 then
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (4 shl 13));
                         InstructionSize := 3;
                         PB := ByteCode; Inc(PB, 2);
                         PShortInt(PB)^ := Num;
                        end else
                        If Num >= -32768 then
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (5 shl 13));
                         InstructionSize := 4;
                         PB := ByteCode; Inc(PB, 2);
                         PSmallInt(PB)^ := Num;
                        end else
                        begin
                         WordCode^ := WordCode^ or ((1 shl 11) or (3 shl 13));
                         InstructionSize := 6;
                         PB := ByteCode; Inc(PB, 2);
                         PInteger(PB)^ := Num;
                        end;
                       end;
                      end;
                     end;
                     omLabel: ;
                    end;
                   end; }
        | REG_REG_OP REGISTER_ COMMA REGISTER_ COMMA operand
                 {(**)With FLabels.LAdd, FInfo^ do
                  (**)begin
                  (**) InstructionSize := 4;
                  (**) DWordCode^ := Mask or ($2 shl 6) or ($4 shl 11);
                  (**) Case Opcode of
                  (**)  _SHL, _SHR, _SAR:
                  (**)  begin
                  (**)   Case FLastOperandMode of
                  (**)    omRegister:
                  (**)     DWordCode^ := DWordCode^ or Cardinal(1 shl 16) or
                  (**)               Cardinal($6 shl 19);
                  (**)    omNumber:
                  (**)    begin
                  (**)     Num := $6;
                  (**)     If (Num < 0) or (Num > 31) then
                  (**)     begin
                  (**)      yyError(Format(KAE_ConstantExpressionViolatesSubrangeBounds, [0, 31]));
                  (**)      ErrorCode := LYE_CustomError;
                  (**)      yyFlag := yyfError;
                  (**)     end else DWordCode^ := DWordCode^ or Cardinal(Num shl 19);
                  (**)    end;
                  (**)   end;
                  (**)   InstructionSize := 3;
                  (**)  end;
                  (**)  Else
                  (**)  Case FLastOperandMode of
                  (**)   omRegister:
                  (**)   begin
                  (**)    DWordCode^ := DWordCode^ or 1 or Cardinal($6 shl 19);
                  (**)    InstructionSize := 3;
                  (**)   end;
                  (**)   omNumber:
                  (**)   begin
                  (**)    Num := $6;
                  (**)    If (Num < -4096) or (Num > 4095) then
                  (**)    begin
                  (**)     yyError(Format(KAE_ConstantExpressionViolatesSubrangeBounds, [-4096, 4095]));
                  (**)     ErrorCode := LYE_CustomError;
                  (**)     yyFlag := yyfError;
                  (**)    end else DWordCode^ := DWordCode^ or Cardinal((Num and 8191) shl 19);
                  (**)   end;
                  (**)  end;
                  (**) end;
                  (**)end; }
        | BRANCH REGISTER_ COMMA REGISTER_ COMMA const
        | SEQ_SNE REGISTER_ COMMA REGISTER_ COMMA add_operands
        | MEM_OPERATE REGISTER_ COMMA LBRAC REGISTER_ COMMA operand RBRAC
        | LDMIA_STMIA LBRAC REGISTER_ RBRAC EXPL COMMA register_list
        | B_BL operand
        | PUSH_POP register_list
        | REG_ REGISTER_
        | _BLM const COMMA const
        | _ADD REGISTER_ COMMA add_operands
        | _SWAP REGISTER_ COMMA REGISTER_
        | _SWAP REGISTER_
        | _SWPH REGISTER_
        | _MVN REGISTER_ COMMA REGISTER_
        | XREG_XREG XREGISTER_ COMMA XREGISTER_
        | _XADD XREGISTER_ COMMA xoperand
        | XREG_REG XREGISTER_ COMMA REGISTER_
        | REG_XREG_XOP REGISTER_ COMMA XREGISTER_ COMMA xoperand
        | REG_XREG_REG REGISTER_ COMMA XREGISTER_ COMMA REGISTER_        
        | XSEQ_XSNE REGISTER_ COMMA XREGISTER_ COMMA const
        | FLOAT_SPECIAL1 REGISTER_ COMMA REGISTER_ COMMA REGISTER_
        | FLOAT_SPECIAL2 REGISTER_ COMMA REGISTER_ COMMA REGISTER_
        | FLOAT_SPECIAL2 REGISTER_ COMMA XREGISTER_ COMMA xoperand
        | FLOAT_SPECIAL3 REGISTER_ COMMA REGISTER_
        | FLOAT_SPECIAL3 XREGISTER_ COMMA XREGISTER_
	| DECLARE_CONST const_list
       ;

add_operands
	: REGISTER_ COMMA const
	| REGISTER_
	| const
	;

operand_hp
        : REGISTER_ { FLastOperandMode := omRegister;
                      $$ := $1; }
        | HP_REG { FLastOperandMode := omHP; }
        | const  { FLastOperandMode := omNumber;
                   $$ := $1; } 
        ;

operand
	: REGISTER_ { FLastOperandMode := omRegister;
                      $$ := $1; }
	| const { FLastOperandMode := omNumber;
                  $$ := $1; }
	;

xoperand
	: REGISTER_
	| XREGISTER_
	;

extended_operand
	: adjusted_register
        | const
	;

diap_register
	: REGISTER_ MINUS REGISTER_
	| REGISTER_
	;

register_list
	: diap_register
	| register_list COMMA diap_register
	;

adjusted_register
        : REGISTER_ sign const
        | REGISTER_
	;

const	: const_term
        { $$ := $1; }
	| sign const_term
        { $$ := Operate(0, $1, $2); }
	| const sign const_term
        { $$ := Operate($1, $2, $3); }
	;

const_term
	: const_factor
        { $$ := $1; }
	| const_term mulop const_factor
        { $$ := Operate($1, $2, $3); }
	;

const_factor
	: DECIMAL { $$ := $1; }
        | LPAREN const RPAREN
        { $$ := $2; }
	;

sign	: PLUS { $$ := PADD; }
	| MINUS { $$ := PSUB; }
	;

mulop	: STAR { $$ := PMUL; }
	| SLASH { $$ := PDIV; }
	;

const_list
	: const
	| const_list COMMA const
	;
