program	: END
	| prg_blk END
	;

prg_blk	: CRLF
	| CRLF prg_blk
	| labeldef
	| labeldef prg_blk
	| instruct CRLF
	| instruct CRLF prg_blk
	;

instruct: iarithm
	| ilogic
	| icompare
	| iarithmr
	| ibranch
	| ibranch2
	| ibranch3
	| ibranch4
	| ibranch5
	| imem
	| ipshpop1
	| ipshpop2
	| icall1
	| icall2
	| iother1
	| isetcomp
	| iarith64
	| ilogic64
	| icomp64
	| istcmp64
	| ifloat
	| ifloat_a
	| ifloat64
	| ifltcomp
	| isystem
	;

labeldef: localdef NAME labeltyp
	;

localdef: '@'
	| '@' localdef
	| /* empty */
	;

labeltyp: ':'
	| equ const CRLF
	;

equ	: ':='
	| '='
	| KW_EQU
	;

equ2	: '=='
	| '='
	;


const	: intconst
	| fltexpr 
	| fltexpr comp fltexpr
	;

intconst: boolpar compcnst ')'
	| intexpr
	| intexpr comp intexpr
	;

compcnst: intexpr comp intexpr
	| fltexpr comp fltexpr
	;

comp	: equ2 | '>' | '<' | '>=' | '<=' | notequ ;

notequ	: '<>' | '!=' ;

boolpar : '('
	| not '('
	;

intexpr	: '(' intexpr ')'
	| intterm
	| intterm addop intexpr
	;

fltexpr	: '(' fltexpr ')'
	| fltterm
	| fltterm faddop fltterm
	;

intterm : integer
	| sign integer
	| nota integer
	;

fltterm : float
	| sign float
	;

addop	: sign | ora | xora
	;

faddop	: sign ;

sign	: '+' | '-' ;

not	: '!' | KW_NOT
	;

nota	: '~' | KW_NOT
	;

ora	: '&'
	| KW_OR
	;

xora	: '^'
	| KW_XOR
	;

anda	: '&'
	| KW_AND
	;

integer	: '(' integer ')'
	| intfact
	| intfact mulop integer
	;

float	: '(' float ')'
	| flt
	| flt fmulop float
	;

flt	: FLOAT
	| FLT_CONST_NAME
	;

mulop	: '*' | divi | modi | anda | shl | shr ;

fmulop	: '*' | '/' ;

divi	: '/'
	| KW_DIV
	;

modi	: '%'
	| KW_MOD
	;

shl	: '<<'
	| KW_SHL
	;

shr	: '>>'
	| KW_SHR
	;

intfact	: CHAR
	| INTEGER
	| INT_CONST_NAME
	| boolpar compcnst ')'
	| nota intfact
	;

iarithm	: oarithm REGISTER ',' const
	;

oarithm	: KW_MOV
	| KW_ADD
	| KW_SUB
	| KW_RSB
	| KW_MUL
	| KW_DIV
	| KW_DIVS
	| KW_MOD
	| KW_RMD
	| KW_AND
	| KW_BIC
	| KW_OR
	| KW_XOR
	;

ilogic	: ologic REGISTER ',' REGISTER ',' opterm

opterm	: REGISTER
	| const
	;

ologic	: KW_SHL
	| KW_ADDL
	| KW_SHR
	| KW_SAR
	;

icompare: ocompare REGISTER ',' REGISTER ',' opterm
	;

ocompare: KW_SGT
	| KW_SGE
	| KW_SLT
	| KW_SLE
	| KW_SGTS
	| KW_SGES
	| KW_SLTS
	| KW_SLES
	;

iarithmr: oarithmr REGISTER ',' REGISTER
	;

oarithmr: KW_MOVR
	| KW_MVNR
	| KW_ADDR
	| KW_SUBR
	| KW_RSBR
	| KW_MULR
	| KW_DIVR
	| KW_DIVRS
	| KW_MODR
	| KW_RMDR
	| KW_SWAPR
	| KW_SWPHR
	| KW_ANDR
	| KW_BICR
	| KW_ORR
	| KW_XORR
	;

ibranch	: obranch REGISTER ',' REGISTER ',' INT_CONST_NAME
	;

obranch	: KW_BEQR
	| KW_BNER
	| KW_BGTR
	| KW_BGER
	| KW_BLTR
	| KW_BLER
	| KW_BGTRS
	| KW_BGERS
	| KW_BLTRS
	| KW_BLERS
	;

ibranch2: obranch2 REGISTER ',' const ',' INT_CONST_NAME 
	;

obranch2: KW_BEQ
	| KW_BNE
	| KW_BGT
	| KW_BGE
	| KW_BLT
	| KW_BLE
	| KW_BGTS
	| KW_BGES
	| KW_BLTS
	| KW_BLES
	| KW_BEQN
	| KW_BNEN
	| KW_BGTN
	| KW_BGEN
	| KW_BLTN
	| KW_BLEN
	;

ibranch3: obranch3 REG64 ',' const ',' INT_CONST_NAME
	;

obranch3: KW_XBEQ
	| KW_XBNE
	| KW_XBGT
	| KW_XBGE
	| KW_XBLT
	| KW_XBLE
	| KW_XBGTS
	| KW_XBGES
	| KW_XBLTS
	| KW_XBLES
	| KW_XBEQS
	| KW_XBNES
	| KW_XBGTN
	| KW_XBGEN
	| KW_XBLTN
	| KW_XBLEN
	;

ibranch4: obranch4 REG64 ',' float ',' INT_CONST_NAME
	;

obranch4: KW_FBEQ
	| KW_FBNE
	| KW_FBGT
	| KW_FBGE
	| KW_FBLT
	| KW_FBLE
	| KW_FBGTR
	| KW_FBGER
	| KW_FBLTR
	| KW_FBLER
	| KW_FBEQR
	| KW_FBNER
	| KW_FBGTT
	| KW_FBGET
	| KW_FBLTT
	| KW_FBLET
	;

ibranch5: obranch5 R0_R16 ',' float ',' INT_CONST_NAME
	;

obranch5: KW_FBEQ32
	| KW_FBNE32
	| KW_FBGT32
	| KW_FBGE32
	| KW_FBLT32
	| KW_FBLE32
	| KW_FBGTR32
	| KW_FBGER32
	| KW_FBLTR32
	| KW_FBLER32
	| KW_FBEQR32
	| KW_FBNER32
	| KW_FBGTT32
	| KW_FBGET32
	| KW_FBLTT32
	| KW_FBLET32
	;

imem	: omem REGISTER ',' '[' REGISTER ',' lblrcnst ']'
	;

lblrcnst: INT_CONST_NAME
	| REGISTER
	| intconst
	;

omem	: KW_STR
	| KW_STRH
	| KW_STRB
	| KW_LDR
	| KW_LDRH
	| KW_LDSH
	| KW_LDRB
	| KW_LDSB
	;
    
ipshpop1: opshpop1 '[' R0_R3 ']' '!' ',' reglist
	;

reglist	: KW_R20
	| reglst
	;

reglst	: R4_R19
	| R4_R19 '-' R4_R19
	| R4_R19 '-' R4_R19 '/' reglst
	;

opshpop1: KW_LDMIA
	| KW_STMIA
	;

ipshpop2: opshpop2 reglist2
	;

reglist2: KW_R0 '-' KW_R7
	| reglst2
	;

reglst2	: R8_R31
	| R8_R31 '-' R8_R31
	| R8_R31 '-' R8_R31 '/' reglst2

opshpop2: KW_PUSHL
	| KW_POPL
	;

icall1	: ocall1 NAME
	;

ocall1	: KW_B
	| KW_BL
	;

icall2	: KW_BLM intconst ',' intconst
	;

iother1	: oother REGISTER
	;

oother	: KW_PUSH
	| KW_POP
	| KW_BX
	| KW_BLX
	| KW_BLMX
	| KW_CBD
	| KW_CWD
	| KW_SWAP
	;

isetcomp: osetcomp REGISTER ',' RESISTER ',' setcmpcn
	;

setcmpcn: REGISTER ',' intconst
	| intconst
	;

osetcomp: KW_SEQ
	| KW_SNE
	;

iarith64: oarith64 REG64 ',' REG64
	;

oarith64: KW_XMOV
	| KW_XMVN
	| KW_XADD
	| KW_XSUB
	| KW_XRSB
	| KW_XMUL
	| KW_XDIV
	| KW_XRDV
	| KW_XMOD
	| KW_XAND
	| KW_XBIC
	| KW_XORR
	| KW_XXOR
	| KW_CDQ
	| KW_CWQ
	| KW_CBQ
	;

ilogic64: ologic64 REG64 ',' REGISTER
	;

ologic64: KW_XSHL
	| KW_FCSR
	| KW_XSHR
	| KW_XSAR
	;

icomp64	: ocomp64 REGISTER ',' REG64 ',' anyreg
	;

anyreg  : REG64
	| REGISTER
	;

ocomp64	: KW_XSEQ32
	| KW_XSNE32
	| KW_XSGT32
	| KW_XSGE32
	| KW_XSLT32
	| KW_XSLE32
	| KW_XSEQ
	| KW_XSNE
	| KW_XSGT
	| KW_XSGE
	| KW_XSLT
	| KW_XSLE
	;

istcmp64: ostcmp64 REGISTER ',' REG64 ',' intconst
	;

ostcmp64: KW_XSEQI
	| KW_XSNEI
	;

ifloat	: ofloat REGISTER ',' REGISTER ',' REGISTER
	;

ofloat	: KW_FADD32
	| KW_FSUB32
	| KW_FMUL32
	| KW_FDIV32
	| KW_FSEQ32
	| KW_FSNE32
	| KW_FSGT32
	| KW_FSGE32
	;

ifloat_a: ofloata REGISTER ',' REGISTER
	;

ofloata	: KW_FABS32
	| KW_FNEG32
	| KW_FEXP32
	| KW_FRAC32
	| KW_FINT32
	| KW_FRND32
	| KW_FTRN32
	| KW_FCDS32
	;

ifloat64: ofloat64 REG64 ',' REG64
	;

ofloat64: KW_FADD
	| KW_FSUB
	| KW_FMUL
	| KW_FDIV
	| KW_FABS
	| KW_FRND
	| KW_FTRN
	| KW_FCQR
	| KW_FNEG
	| KW_FEXP
	| KW_FRAC
	| KW_FINT
	| KW_FLN
	| KW_FRSB
	| KW_FSQR
	| KW_FSQRT
	;

ifltcomp: ofltcomp REGISTER ',' REG64 ',' anyreg2
	;

anyreg2	: REG64
	| R0_R7
	;

ofltcomp: KW_FSEQ
	| KW_FSNE
	| KW_FSGT
	| KW_FSGE
	| KW_FSLT
	| KW_FSLE
	;

isystem	: KW_NOP | KW_RET | KW_RAISE | KW_STOP ;
