%{
Unit KasmLexer;

interface

Uses SysUtils, LexYacc;

Type
 TKasmLexer = Class(TLexer)
 public
  function yylex: Integer; override;
 end;

implementation

Uses KasmParser;

function yylex_(Parser: TCParser): Integer; forward;

function TKasmLexer.yylex: Integer;
begin
 Result := yylex_(TCParser(yyOwner));
end;

type
 Ident = string[5];

(* Kruptar Virtual Assembler Opcodes *)

const
 OpcodesCount = 117;
 Opcodes: Array [1..OpcodesCount] of Ident = (
'ADD',	'AND',	'B',	'BEQ',	'BGT',	'BGTS',	'BHE',
'BHES',	'BIC',	'BL',	'BLE',	'BLES',	'BLM',
'BLMX',	'BLT',	'BLTS',	'BLX',	'BNE',	'BX',	'CBD',
'CBQ',	'CDQ',	'CWD',	'CWQ',	'DIV',	'DIVS',	'FABS',
'FADD',	'FCDS',	'FCQR',	'FDIV',	'FEXP',	'FINT',	'FLN',
'FMUL',	'FNEG',	'FRAC',	'FRND',	'FRSB',	'FSEQ',	'FSGT',
'FSHE',	'FSLE',	'FSLT',	'FSNE',	'FSQR',	'FSQRT','FSUB',
'FTRN',	'LDMIA','LDR',	'LDRB',	'LDRH',	'LDSB',	'LDSH',
'MOD',	'MOV',	'MUL',	'MVN',	'NOP',	'ORR',	'POP',
'PUSH',	'RAISE','RET',	'RMD',	'RSB',	'SAR',	'SEQ',
'SGT',	'SGTS',	'SHE',	'SHES',	'SHL',	'SHR',	'SLE',
'SLES',	'SLT',	'SLTS',	'SNE',	'STMIA','STOP',	'STR',
'STRB',	'STRH',	'SUB',	'SWAP',	'SWPH',	'XADD',	'XAND',
'XBIC',	'XDIV',	'XMOD',	'XMOV',	'XMUL',	'XMVN',	'XOR',
'XORR',	'XRDV',	'XRSB',	'XSAR',	'XSEQ',	'XSEQS','XSGT',
'XSGTS','XSHE',	'XSHES','XSHL',	'XSHR',	'XSLE',	'XSLES',
'XSLT',	'XSLTS','XSNE',	'XSNES','XSUB',	'XXOR');
 TokenCodes: Array [1..OpcodesCount] of Integer = (
_ADD,	_AND,	_B,	_BEQ,	_BGT,	_BGTS,	_BHE,
_BHES,	_BIC,	_BL,	_BLE,	_BLES,	_BLM,
_BLMX,	_BLT,	_BLTS,	_BLX,	_BNE,	_BX,	_CBD,
_CBQ,	_CDQ,	_CWD,	_CWQ,	_DIV,	_DIVS,	_FABS,
_FADD,	_FCDS,	_FCQR,	_FDIV,	_FEXP,	_FINT,	_FLN,
_FMUL,	_FNEG,	_FRAC,	_FRND,	_FRSB,	_FSEQ,	_FSGT,
_FSHE,	_FSLE,	_FSLT,	_FSNE,	_FSQR,	_FSQRT,	_FSUB,
_FTRN,	_LDMIA,	_LDR,	_LDRB,	_LDRH,	_LDSB,	_LDSH,
_MOD,	_MOV,	_MUL,	_MVN,	_NOP,	_ORR,	_POP,
_PUSH,	_RAISE,	_RET,	_RMD,	_RSB,	_SAR,	_SEQ,
_SGT,	_SGTS,	_SHE,	_SHES,	_SHL,	_SHR,	_SLE,
_SLES,	_SLT,	_SLTS,	_SNE,	_STMIA,	_STOP,	_STR,
_STRB,	_STRH,	_SUB,	_SWAP,	_SWPH,	_XADD,	_XAND,
_XBIC,	_XDIV,	_XMOD,	_XMOV,	_XMUL,	_XMVN,	_XOR,
_XORR,	_XRDV,	_XRSB,	_XSAR,	_XSEQ,	_XSEQS,	_XSGT,
_XSGTS,	_XSHE,	_XSHES,	_XSHL,	_XSHR,	_XSLE,	_XSLES,
_XSLT,	_XSLTS,	_XSNE,	_XSNES,	_XSUB,	_XXOR);
{$I INSTINFO.INC}

Var LastOpIndex: Integer;

function isOpcode(const S: String; var Index: Integer): Boolean;
var M, N, K: Integer;
begin
 m := 1; n := OpcodesCount;
 While M <= N do
 begin
  K := M + (N - M) div 2;
  if S = Opcodes[K] then
  begin
   Result := true;
   LastOpIndex := K;
   Index := TokenCodes[K];
   Exit;
  end else
  if S > Opcodes[K] then
   M := K + 1 else
   N := K - 1;
 end;
 Result := False;
end;

function isRegister(const S: String; Var Index: Integer): Boolean;
Var L: Integer; P: PChar;
begin
 L := Length(S);
 If L > 1 then
 begin
  Result := True;
  If L = 3 then
  begin
   Case PCardinal(S)^ of
    Byte('E') + Byte('S') shl 8 + Byte('P') shl 16:
    begin
     Index := 24;
     Exit;
    end;
    Byte('E') + Byte('M') shl 8 + Byte('P') shl 16:
    begin
     Index := 25;
     Exit;
    end;
    Byte('E') + Byte('P') shl 8 + Byte('P') shl 16:
    begin
     Index := 26;
     Exit;
    end;
    Byte('R') + Byte('M') shl 8 + Byte('P') shl 16:
    begin
     Index := 28;
     Exit;
    end;
    Byte('R') + Byte('P') shl 8 + Byte('P') shl 16:
    begin
     Index := 29;
     Exit;
    end;
   end;
  end;
  If S[1] = 'R' then
  begin
   P := Pointer(S);
   Inc(P);
   Val(P, Index, L);
   Result := (L = 0) and (Index in [0..31]);
  end else
  If L = 2 then
  begin
   Case PWord(S)^ of
    Byte('M') + Byte('P') shl 8: Index := 30;
    Byte('P') + Byte('P') shl 8: Index := 31;
    Byte('S') + Byte('P') shl 8: Index := 27;
    Else Result := False;
   end;
  end else Result := False;
 end else Result := False;
end;

function isXRegister(const S: String; Var Index: Integer): Boolean;
Var L: Integer; P: PChar;
begin
 L := Length(S);
 If (L = 3) and (S[1] = 'X') and (S[2] = 'R') then
 begin
  P := Pointer(S);
  Inc(P, 2);
  Val(P, Index, L);
  Result := (L = 0) and (Index in [0..7]);
 end else Result := False;
end;
%}

%%

%{
var
 Index: Integer;
 Error: Integer;
 TempStr: String;
%}

[a-zA-Z_@]([a-zA-Z_0-9])*
	begin
	 TempStr := UpperCase(yytext);
	 if TempStr = 'END' then
	 begin
	  (* do something *)
	  return(END_);
	 end else
         If TempStr = 'HP' then
         begin
	  (* do something *)
	  return(HP_REG);
         end;
	 if TempStr = 'DB' then
	 begin
          yylval := 1;
	  return(DECLARE_CONST);
	 end else
	 if TempStr = 'DW' then
	 begin
          yylval := 2;
	  return(DECLARE_CONST);
	 end else
	 if TempStr = 'DD' then
	 begin
          yylval := 4;
	  return(DECLARE_CONST);
	 end else
	 if isRegister(TempStr, Index) then
	 begin
	  (* do something *)
          yylval := Index;
	  return(REGISTER_);
	 end else
         If isXRegister(TempStr, Index) then
         begin
	  (* do something *)
          yylval := Index;
	  return(XREGISTER_);
         end else
	 if isOpcode(TempStr, Index) then
	 begin
          FInfo := Addr(InstrInfo[LastOpIndex]);
          yylval := Index;
	  case Index of
	   _MOV, _SUB, _RSB, _MUL, _DIV, _DIVS,
           _MOD, _RMD, _AND, _BIC, _ORR, _XOR:
	   	return(REG_OP);
	   _SHL, _SHR, _SAR, _SGT, _SHE, _SLT,
           _SLE, _SGTS,_SHES,_SLTS,_SLES:
                return(REG_REG_OP);
           _BEQ, _BNE, _BGT, _BHE, _BLT,
           _BLE, _BGTS,_BHES,_BLTS,_BLES:
                return(BRANCH);
	   _SEQ, _SNE:
	   	return(SEQ_SNE);
	   _STR, _STRH,_STRB,_LDR,
           _LDRH,_LDSH,_LDRB,_LDSB:
	   	return(MEM_OPERATE);
	   _LDMIA, _STMIA:
	  	return(LDMIA_STMIA);
	   _B, _BL:
	 	return(B_BL);
	   _PUSH, _POP:
	   	return(PUSH_POP);
	   _BX, _BLX, _BLMX, _CBD, _CWD:
	   	return(REG_);
           _XMOV,_XMVN,_XSUB,_XRSB,_XMUL,_XDIV,_XRDV,
           _XMOD,_XAND,_XBIC,_XORR,_XXOR,_CDQ, _CWQ, _CBQ:
                return(XREG_XREG);
           _XSHL,_XSHR,_XSAR:
                return(XREG_REG);
           _XSGT, _XSHE, _XSLT, _XSLE:
                return(REG_XREG_XOP);
           _XSEQS,_XSNES,_XSGTS,
           _XSHES,_XSLTS,_XSLES:
                return(REG_XREG_REG);
           _XSEQ,_XSNE:
                return(XSEQ_XSNE);
           _FADD,_FSUB,_FMUL,_FDIV:
                return(FLOAT_SPECIAL1);
           _FSEQ,_FSNE,_FSGT,_FSHE,_FSLT,_FSLE:
                return(FLOAT_SPECIAL2);
           _FABS,_FNEG,_FEXP,_FRAC,_FINT,_FRND,_FTRN:
                return(FLOAT_SPECIAL3);
           _FCQR,_FLN,_FRSB,_FSQR,_FSQRT:
                return(XREG_XREG);
	   _NOP, _RET, _RAISE, _STOP:
	   	return(OPCODE_);
	   Else	return(Index);
	  end;
	 end else
	 begin
          yylval := FStringList.Count;
          FStringList.Add(TempStr);
	  return(ID)
	 end;
	end;
\012	begin
         return(EOL);
        end;
"!"     return(EXPL);
","	return(COMMA);
":"	return(COLON);
";"	begin
	 while not (get_char in [#0, #10]) do;
	 return(EOL);
	end;
"-"	return(MINUS);
"+"	return(PLUS);
"("	return(LPAREN);
")"	return(RPAREN);
"*"	return(STAR);
"/"	return(SLASH);
"["	return(LBRAC);
"]"	return(RBRAC);
([0-9]+)|([$]([a-fA-F0-9])+)
        begin
         Val(yytext, yylval, Error);
         If Error = 0 then
          return(DECIMAL) else
          return(ILLEGAL);
        end;
[ \n\t\f]
	(* do something *);
.	begin
        (* do something *)
	 return(ILLEGAL);
	end;
