(* Yacc parser template (TP Yacc V4.1), V1.2 6-17-91 AG *)
(* Modified by S.A., 23.04.2008 *)

(* global definitions: *)

unit KasmParser;

interface

uses SysUtils, Classes, NodeLst, LexYacc;

const _ADD = 257;
const _AND = 258;
const _B = 259;
const _BEQ = 260;
const _BGT = 261;
const _BGTS = 262;
const _BHE = 263;
const _BHES = 264;
const _BIC = 265;
const _BL = 266;
const _BLE = 267;
const _BLES = 268;
const _BLM = 269;
const _BLMX = 270;
const _BLT = 271;
const _BLTS = 272;
const _BLX = 273;
const _BNE = 274;
const _BX = 275;
const _CBD = 276;
const _CBQ = 277;
const _CDQ = 278;
const _CWD = 279;
const _CWQ = 280;
const _DIV = 281;
const _DIVS = 282;
const _FABS = 283;
const _FADD = 284;
const _FCDS = 285;
const _FCQR = 286;
const _FDIV = 287;
const _FEXP = 288;
const _FINT = 289;
const _FLN = 290;
const _FMUL = 291;
const _FNEG = 292;
const _FRAC = 293;
const _FRND = 294;
const _FRSB = 295;
const _FSEQ = 296;
const _FSGT = 297;
const _FSHE = 298;
const _FSLE = 299;
const _FSLT = 300;
const _FSNE = 301;
const _FSQR = 302;
const _FSQRT = 303;
const _FSUB = 304;
const _FTRN = 305;
const _LDMIA = 306;
const _LDR = 307;
const _LDRB = 308;
const _LDRH = 309;
const _LDSB = 310;
const _LDSH = 311;
const _MOD = 312;
const _MOV = 313;
const _MUL = 314;
const _MVN = 315;
const _NOP = 316;
const _ORR = 317;
const _POP = 318;
const _PUSH = 319;
const _RAISE = 320;
const _RET = 321;
const _RMD = 322;
const _RSB = 323;
const _SAR = 324;
const _SEQ = 325;
const _SGT = 326;
const _SGTS = 327;
const _SHE = 328;
const _SHES = 329;
const _SHL = 330;
const _SHR = 331;
const _SLE = 332;
const _SLES = 333;
const _SLT = 334;
const _SLTS = 335;
const _SNE = 336;
const _STMIA = 337;
const _STOP = 338;
const _STR = 339;
const _STRB = 340;
const _STRH = 341;
const _SUB = 342;
const _SWAP = 343;
const _SWPH = 344;
const _XADD = 345;
const _XAND = 346;
const _XBIC = 347;
const _XDIV = 348;
const _XMOD = 349;
const _XMOV = 350;
const _XMUL = 351;
const _XMVN = 352;
const _XOR = 353;
const _XORR = 354;
const _XRDV = 355;
const _XRSB = 356;
const _XSAR = 357;
const _XSEQ = 358;
const _XSEQS = 359;
const _XSGT = 360;
const _XSGTS = 361;
const _XSHE = 362;
const _XSHES = 363;
const _XSHL = 364;
const _XSHR = 365;
const _XSLE = 366;
const _XSLES = 367;
const _XSLT = 368;
const _XSLTS = 369;
const _XSNE = 370;
const _XSNES = 371;
const _XSUB = 372;
const _XXOR = 373;
const ID = 374;
const DECIMAL = 375;
const HEXADECIMAL = 376;
const ILLEGAL = 377;
const COMMA = 378;
const LPAREN = 379;
const RPAREN = 380;
const EXPL = 381;
const LBRAC = 382;
const RBRAC = 383;
const MINUS = 384;
const PLUS = 385;
const COLON = 386;
const EOL = 387;
const STAR = 388;
const SLASH = 389;
const DECLARE_CONST = 390;
const B_BL = 391;
const BRANCH = 392;
const END_ = 393;
const FLOAT_SPECIAL1 = 394;
const FLOAT_SPECIAL2 = 395;
const FLOAT_SPECIAL3 = 396;
const HP_REG = 397;
const LDMIA_STMIA = 398;
const MEM_OPERATE = 399;
const OPCODE_ = 400;
const PUSH_POP = 401;
const REG_ = 402;
const REG_OP = 403;
const REG_REG = 404;
const REG_REG_OP = 405;
const REG_REG_REG = 406;
const REG_XOP_XOP = 407;
const REG_XREG_XOP = 408;
const REGISTER_ = 409;
const SEQ_SNE = 410;
const XREG_REG = 411;
const XREG_XREG = 412;
const XREGISTER_ = 413;
const XSEQ_XSNE = 414;
const PADD = 415;
const PSUB = 416;
const PMUL = 417;
const PDIV = 418;

type
 TLabel = class;
 TLabelList = class;
 PInstructionInfo = ^TInstructionInfo;
 TInstructionInfo = Record
  Opcode: Integer;
  Mask: Integer;
 end;

 TKasmParser = class(TCustomParser)
 public
  yylval: YYSType;
  yyval: YYSType;
  yyv: array [1..yymaxdepth] of YYSType;
  FLabels: TLabelList;
  FLabelNames: TLabelList;
  FStringList: TStringList;
  TempLabel: TLabel;
  FInfo:  PInstructionInfo;
  FLastOperandMode: (omRegister, omNumber, omLabel, omHP);
  procedure ConsoleMessage(Sender: TObject; const Msg: String);
  procedure Initialize; override;
  destructor Destroy; override;
 end;

 TKasmYacc = Class(TYacc)
 private
 public
  function yyparse: Integer; override;
 end;

 TCParser = TKasmParser;

 TByteArray = Array of Byte;

 TCodeRec = Packed Record
  case Byte of
   0: (ByteCode: PByte);
   1: (WordCode: PWord);
   2: (DWordCode: PCardinal);
 end;

 TLabel = Class
 private
  FCode: TCodeRec;
 private
  function GetInstructionSize: Integer;
  procedure SetInstructionSize(Value: Integer);
 public
  property InstructionSize: Integer
      read GetInstructionSize
     write SetInstructionSize;
  property ByteCode: PByte read FCode.ByteCode;
  property WordCode: PWord read FCode.WordCode;
  property DWordCode: PCardinal read FCode.DWordCode;
  destructor Destroy; override;
 end;
 TLabelName = class
 private
  FName: String;
  FIndex: Integer;
 public
  constructor Create(const Name: String; Index: Integer);
  destructor Destroy; override;
 end;
 TLabelList = class(TNodeList)
 private
 public
  function LAdd: TLabel;
  function NAdd(const Name: String; Index: Integer): TLabelName;
 end;

Const
 KAE_ConstantExpressionViolatesSubrangeBounds =
 'Constant expression violates subrange bounds (%d..%d)'; 

implementation

function TLabel.GetInstructionSize: Integer;
begin
 Result := Length(TByteArray(FCode));
end;

procedure TLabel.SetInstructionSize(Value: Integer);
begin
 SetLength(TByteArray(FCode), Value);
end;

destructor TLabel.Destroy;
begin
 Finalize(TByteArray(FCode));
 inherited;
end;

constructor TLabelName.Create(const Name: String; Index: Integer);
begin
 FName := Name;
 FIndex := Index;
end;

destructor TLabelName.Destroy;
begin
 Finalize(FName);
 inherited;
end;

function TLabelList.LAdd: TLabel;
begin
 Result := TLabel.Create;
 Add.Node := Result;
end;

function TLabelList.NAdd(const Name: String; Index: Integer): TLabelName;
begin
 Result := TLabelName.Create(Name, Index);
 Add.Node := Result;
end;

function Operate(A, OP, B: Integer): Integer;
begin
 Case OP of
  PADD: Result := A + B;
  PSUB: Result := A - B;
  PMUL: Result := A * B;
  Else Result := A div B;
 end;
end;

procedure TKasmParser.ConsoleMessage(Sender: TObject; const Msg: String);
begin
 If Sender is TYacc then
  Writeln('Yacc: ', Msg) Else
 If Sender is TLexer then
  Writeln('Lex: ', Msg) Else
  Writeln(Msg);
end;

procedure TKasmParser.Initialize;
begin
 inherited;
 FStringList := TStringList.Create;
 FLabels := TLabelList.Create;
 FLabelNames := TLabelList.Create;
 TempLabel := TLabel.Create;
 OnDebugMessage := ConsoleMessage;
 OnErrorMessage := ConsoleMessage;
end;

destructor TKasmParser.Destroy;
begin
 TempLabel.Free;
 FLabelNames.Free;
 FLabels.Free;
 FStringList.Free;
 inherited;
end;

function yyparse_(Parser: TCParser): Integer; forward;

function TKasmYacc.yyparse: Integer;
begin
 Result := yyparse_(TCParser(yyOwner));
end;

(* Put this variables in your TCustomParser class 
 yylval: YYSType;
 yyval: YYSType;
 yyv: array [1..yymaxdepth] of YYSType; *)
 
procedure yacc_action(Parser: TCParser; yyruleno: Integer); 
  (* local definitions: *)
var
 Num: Integer;
 PB: PByte;
begin
 With Parser, Yacc do 
 begin
  (* actions: *)
  case yyruleno of
   1 : begin
         yyval := yyv[yysp-1];
       end;
   2 : begin
         yyval := yyv[yysp-1];
       end;
   3 : begin
         yyval := yyv[yysp-0];
       end;
   4 : begin
         yyval := yyv[yysp-0];
       end;
   5 : begin
         yyval := yyv[yysp-1];
       end;
   6 : begin
       end;
   7 : begin
         yyval := yyv[yysp-0];
       end;
   8 : begin
         FLabelNames.NAdd(FStringList.Strings[yyv[yysp-3]], FLabels.Count - 1); 
       end;
   9 : begin
         FLabelNames.NAdd(FStringList.Strings[yyv[yysp-2]], FLabels.Count); 
       end;
  10 : begin
         yyval := yyv[yysp-1];
       end;
  11 : begin
         yyval := yyv[yysp-0];
       end;
  12 : begin
         yyval := yyv[yysp-1];
       end;
  13 : begin
         With FLabels.LAdd, FInfo^ do
         begin
         InstructionSize := 1;
         ByteCode^ := Mask;
         end; 
       end;
  14 : begin
         With FLabels.LAdd, FInfo^ do
         begin
         InstructionSize := 2;
         WordCode^ := Mask or (yyv[yysp-2] shl 6);
         Case FLastOperandMode of
         omRegister: WordCode^ := WordCode^ or (yyv[yysp-0] shl 11);
         omHP: WordCode^ := WordCode^ or ((1 shl 11) or (6 shl 13));
         omNumber:
         begin
         Num := yyv[yysp-0];
         Case Num of
         0: WordCode^ := WordCode^ or (1 shl 11);
         1: WordCode^ := WordCode^ or ((1 shl 11) or (7 shl 13));
         Else If Num > 0 then
         begin
         If Num <= 16 then
         WordCode^ := WordCode^ or ((Num - 1) shl 12);
         If Num < 256 then
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (1 shl 13));
         InstructionSize := 3;
         PB := ByteCode; Inc(PB, 2);
         PB^ := Num;
         end else
         If Num < 65536 then
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (2 shl 13));
         InstructionSize := 4;
         PB := ByteCode; Inc(PB, 2);
         PWord(PB)^ := Num;
         end else
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (3 shl 13));
         InstructionSize := 6;
         PB := ByteCode; Inc(PB, 2);
         PInteger(PB)^ := Num;
         end;
         end Else
         begin
         If Num >= -128 then
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (4 shl 13));
         InstructionSize := 3;
         PB := ByteCode; Inc(PB, 2);
         PShortInt(PB)^ := Num;
         end else
         If Num >= -32768 then
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (5 shl 13));
         InstructionSize := 4;
         PB := ByteCode; Inc(PB, 2);
         PSmallInt(PB)^ := Num;
         end else
         begin
         WordCode^ := WordCode^ or ((1 shl 11) or (3 shl 13));
         InstructionSize := 6;
         PB := ByteCode; Inc(PB, 2);
         PInteger(PB)^ := Num;
         end;
         end;
         end;
         end;
         omLabel: ;
         end;
         end; 
       end;
  15 : begin
         (**)With FLabels.LAdd, FInfo^ do
         (**)begin
         (**) InstructionSize := 4;
         (**) DWordCode^ := Mask or (yyv[yysp-4] shl 6) or (yyv[yysp-2] shl 11);
         (**) Case Opcode of
         (**)  _SHL, _SHR, _SAR:
         (**)  begin
         (**)   Case FLastOperandMode of
         (**)    omRegister:
         (**)     DWordCode^ := DWordCode^ or Cardinal(1 shl 16) or
         (**)               Cardinal(yyv[yysp-0] shl 19);
         (**)    omNumber:
         (**)    begin
         (**)     Num := yyv[yysp-0];
         (**)     If (Num < 0) or (Num > 31) then
         (**)     begin
         (**)      yyError(Format(KAE_ConstantExpressionViolatesSubrangeBounds, [0, 31]));
         (**)      ErrorCode := LYE_CustomError;
         (**)      yyFlag := yyfError;
         (**)     end else DWordCode^ := DWordCode^ or Cardinal(Num shl 19);
         (**)    end;
         (**)   end;
         (**)   InstructionSize := 3;
         (**)  end;
         (**)  Else
         (**)  Case FLastOperandMode of
         (**)   omRegister:
         (**)   begin
         (**)    DWordCode^ := DWordCode^ or 1 or Cardinal(yyv[yysp-0] shl 19);
         (**)    InstructionSize := 3;
         (**)   end;
         (**)   omNumber:
         (**)   begin
         (**)    Num := yyv[yysp-0];
         (**)    If (Num < -4096) or (Num > 4095) then
         (**)    begin
         (**)     yyError(Format(KAE_ConstantExpressionViolatesSubrangeBounds, [-4096, 4095]));
         (**)     ErrorCode := LYE_CustomError;
         (**)     yyFlag := yyfError;
         (**)    end else DWordCode^ := DWordCode^ or Cardinal((Num and 8191) shl 19);
         (**)   end;
         (**)  end;
         (**) end;
         (**)end; 
       end;
  16 : begin
         yyval := yyv[yysp-5];
       end;
  17 : begin
         yyval := yyv[yysp-5];
       end;
  18 : begin
         yyval := yyv[yysp-7];
       end;
  19 : begin
         yyval := yyv[yysp-6];
       end;
  20 : begin
         yyval := yyv[yysp-1];
       end;
  21 : begin
         yyval := yyv[yysp-1];
       end;
  22 : begin
         yyval := yyv[yysp-1];
       end;
  23 : begin
         yyval := yyv[yysp-3];
       end;
  24 : begin
         yyval := yyv[yysp-3];
       end;
  25 : begin
         yyval := yyv[yysp-3];
       end;
  26 : begin
         yyval := yyv[yysp-1];
       end;
  27 : begin
         yyval := yyv[yysp-1];
       end;
  28 : begin
         yyval := yyv[yysp-3];
       end;
  29 : begin
         yyval := yyv[yysp-3];
       end;
  30 : begin
         yyval := yyv[yysp-3];
       end;
  31 : begin
         yyval := yyv[yysp-3];
       end;
  32 : begin
         yyval := yyv[yysp-5];
       end;
  33 : begin
         yyval := yyv[yysp-5];
       end;
  34 : begin
         yyval := yyv[yysp-5];
       end;
  35 : begin
         yyval := yyv[yysp-5];
       end;
  36 : begin
         yyval := yyv[yysp-5];
       end;
  37 : begin
         yyval := yyv[yysp-3];
       end;
  38 : begin
         yyval := yyv[yysp-3];
       end;
  39 : begin
         yyval := yyv[yysp-1];
       end;
  40 : begin
         yyval := yyv[yysp-2];
       end;
  41 : begin
         yyval := yyv[yysp-0];
       end;
  42 : begin
         yyval := yyv[yysp-0];
       end;
  43 : begin
         FLastOperandMode := omRegister;
         yyval := yyv[yysp-0]; 
       end;
  44 : begin
         FLastOperandMode := omHP; 
       end;
  45 : begin
         FLastOperandMode := omNumber;
         yyval := yyv[yysp-0]; 
       end;
  46 : begin
         FLastOperandMode := omRegister;
         yyval := yyv[yysp-0]; 
       end;
  47 : begin
         FLastOperandMode := omNumber;
         yyval := yyv[yysp-0]; 
       end;
  48 : begin
         yyval := yyv[yysp-0];
       end;
  49 : begin
         yyval := yyv[yysp-0];
       end;
  50 : begin
         yyval := yyv[yysp-0];
       end;
  51 : begin
         yyval := yyv[yysp-0];
       end;
  52 : begin
         yyval := yyv[yysp-2];
       end;
  53 : begin
         yyval := yyv[yysp-0];
       end;
  54 : begin
         yyval := yyv[yysp-0];
       end;
  55 : begin
         yyval := yyv[yysp-2];
       end;
  56 : begin
         yyval := yyv[yysp-2];
       end;
  57 : begin
         yyval := yyv[yysp-0];
       end;
  58 : begin
         yyval := yyv[yysp-0]; 
       end;
  59 : begin
         yyval := Operate(0, yyv[yysp-1], yyv[yysp-0]); 
       end;
  60 : begin
         yyval := Operate(yyv[yysp-2], yyv[yysp-1], yyv[yysp-0]); 
       end;
  61 : begin
         yyval := yyv[yysp-0]; 
       end;
  62 : begin
         yyval := Operate(yyv[yysp-2], yyv[yysp-1], yyv[yysp-0]); 
       end;
  63 : begin
         yyval := yyv[yysp-0]; 
       end;
  64 : begin
         yyval := yyv[yysp-1]; 
       end;
  65 : begin
         yyval := PADD; 
       end;
  66 : begin
         yyval := PSUB; 
       end;
  67 : begin
         yyval := PMUL; 
       end;
  68 : begin
         yyval := PDIV; 
       end;
  69 : begin
         yyval := yyv[yysp-0];
       end;
  70 : begin
         yyval := yyv[yysp-2];
       end;
  end;
 end;
end;

(* parse table: *)

type YYARec = record
                sym, act : Integer;
              end;
     YYRRec = record
                len, sym : Integer;
              end;

const

yynacts   = 307;
yyngotos  = 99;
yynstates = 164;
yynrules  = 70;

yya : array [1..yynacts] of YYARec = (
{ 0: }
  ( sym: 257; act: 5 ),
  ( sym: 269; act: 6 ),
  ( sym: 315; act: 7 ),
  ( sym: 343; act: 8 ),
  ( sym: 344; act: 9 ),
  ( sym: 345; act: 10 ),
  ( sym: 374; act: 11 ),
  ( sym: 387; act: 12 ),
  ( sym: 390; act: 13 ),
  ( sym: 391; act: 14 ),
  ( sym: 392; act: 15 ),
  ( sym: 394; act: 16 ),
  ( sym: 395; act: 17 ),
  ( sym: 396; act: 18 ),
  ( sym: 398; act: 19 ),
  ( sym: 399; act: 20 ),
  ( sym: 400; act: 21 ),
  ( sym: 401; act: 22 ),
  ( sym: 402; act: 23 ),
  ( sym: 403; act: 24 ),
  ( sym: 405; act: 25 ),
  ( sym: 408; act: 26 ),
  ( sym: 410; act: 27 ),
  ( sym: 411; act: 28 ),
  ( sym: 412; act: 29 ),
  ( sym: 414; act: 30 ),
  ( sym: 393; act: -6 ),
{ 1: }
  ( sym: 387; act: 31 ),
{ 2: }
{ 3: }
  ( sym: 257; act: 5 ),
  ( sym: 269; act: 6 ),
  ( sym: 315; act: 7 ),
  ( sym: 343; act: 8 ),
  ( sym: 344; act: 9 ),
  ( sym: 345; act: 10 ),
  ( sym: 374; act: 11 ),
  ( sym: 387; act: 12 ),
  ( sym: 390; act: 13 ),
  ( sym: 391; act: 14 ),
  ( sym: 392; act: 15 ),
  ( sym: 393; act: 34 ),
  ( sym: 394; act: 16 ),
  ( sym: 395; act: 17 ),
  ( sym: 396; act: 18 ),
  ( sym: 398; act: 19 ),
  ( sym: 399; act: 20 ),
  ( sym: 400; act: 21 ),
  ( sym: 401; act: 22 ),
  ( sym: 402; act: 23 ),
  ( sym: 403; act: 24 ),
  ( sym: 405; act: 25 ),
  ( sym: 408; act: 26 ),
  ( sym: 410; act: 27 ),
  ( sym: 411; act: 28 ),
  ( sym: 412; act: 29 ),
  ( sym: 414; act: 30 ),
{ 4: }
  ( sym: 0; act: 0 ),
{ 5: }
  ( sym: 409; act: 35 ),
{ 6: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 7: }
  ( sym: 409; act: 44 ),
{ 8: }
  ( sym: 409; act: 45 ),
{ 9: }
  ( sym: 409; act: 46 ),
{ 10: }
  ( sym: 413; act: 47 ),
{ 11: }
  ( sym: 386; act: 48 ),
{ 12: }
{ 13: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 14: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 409; act: 53 ),
{ 15: }
  ( sym: 409; act: 54 ),
{ 16: }
  ( sym: 409; act: 55 ),
{ 17: }
  ( sym: 409; act: 56 ),
{ 18: }
  ( sym: 409; act: 57 ),
  ( sym: 413; act: 58 ),
{ 19: }
  ( sym: 382; act: 59 ),
{ 20: }
  ( sym: 409; act: 60 ),
{ 21: }
{ 22: }
  ( sym: 409; act: 63 ),
{ 23: }
  ( sym: 409; act: 64 ),
{ 24: }
  ( sym: 409; act: 65 ),
{ 25: }
  ( sym: 409; act: 66 ),
{ 26: }
  ( sym: 409; act: 67 ),
{ 27: }
  ( sym: 409; act: 68 ),
{ 28: }
  ( sym: 413; act: 69 ),
{ 29: }
  ( sym: 413; act: 70 ),
{ 30: }
  ( sym: 409; act: 71 ),
{ 31: }
{ 32: }
{ 33: }
{ 34: }
  ( sym: 387; act: 73 ),
  ( sym: 0; act: -3 ),
{ 35: }
  ( sym: 378; act: 74 ),
{ 36: }
{ 37: }
  ( sym: 388; act: 76 ),
  ( sym: 389; act: 77 ),
  ( sym: 378; act: -58 ),
  ( sym: 380; act: -58 ),
  ( sym: 383; act: -58 ),
  ( sym: 384; act: -58 ),
  ( sym: 385; act: -58 ),
  ( sym: 387; act: -58 ),
{ 38: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
{ 39: }
  ( sym: 378; act: 80 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 40: }
{ 41: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 42: }
{ 43: }
{ 44: }
  ( sym: 378; act: 82 ),
{ 45: }
  ( sym: 378; act: 83 ),
  ( sym: 387; act: -26 ),
{ 46: }
{ 47: }
  ( sym: 378; act: 84 ),
{ 48: }
  ( sym: 257; act: 5 ),
  ( sym: 269; act: 6 ),
  ( sym: 315; act: 7 ),
  ( sym: 343; act: 8 ),
  ( sym: 344; act: 9 ),
  ( sym: 345; act: 10 ),
  ( sym: 387; act: 86 ),
  ( sym: 390; act: 13 ),
  ( sym: 391; act: 14 ),
  ( sym: 392; act: 15 ),
  ( sym: 394; act: 16 ),
  ( sym: 395; act: 17 ),
  ( sym: 396; act: 18 ),
  ( sym: 398; act: 19 ),
  ( sym: 399; act: 20 ),
  ( sym: 400; act: 21 ),
  ( sym: 401; act: 22 ),
  ( sym: 402; act: 23 ),
  ( sym: 403; act: 24 ),
  ( sym: 405; act: 25 ),
  ( sym: 408; act: 26 ),
  ( sym: 410; act: 27 ),
  ( sym: 411; act: 28 ),
  ( sym: 412; act: 29 ),
  ( sym: 414; act: 30 ),
{ 49: }
  ( sym: 378; act: 87 ),
  ( sym: 387; act: -39 ),
{ 50: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 378; act: -69 ),
  ( sym: 387; act: -69 ),
{ 51: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 383; act: -47 ),
  ( sym: 387; act: -47 ),
{ 52: }
{ 53: }
{ 54: }
  ( sym: 378; act: 88 ),
{ 55: }
  ( sym: 378; act: 89 ),
{ 56: }
  ( sym: 378; act: 90 ),
{ 57: }
  ( sym: 378; act: 91 ),
{ 58: }
  ( sym: 378; act: 92 ),
{ 59: }
  ( sym: 409; act: 93 ),
{ 60: }
  ( sym: 378; act: 94 ),
{ 61: }
{ 62: }
  ( sym: 378; act: 95 ),
  ( sym: 387; act: -21 ),
{ 63: }
  ( sym: 384; act: 96 ),
  ( sym: 378; act: -53 ),
  ( sym: 387; act: -53 ),
{ 64: }
{ 65: }
  ( sym: 378; act: 97 ),
{ 66: }
  ( sym: 378; act: 98 ),
{ 67: }
  ( sym: 378; act: 99 ),
{ 68: }
  ( sym: 378; act: 100 ),
{ 69: }
  ( sym: 378; act: 101 ),
{ 70: }
  ( sym: 378; act: 102 ),
{ 71: }
  ( sym: 378; act: 103 ),
{ 72: }
  ( sym: 387; act: 104 ),
  ( sym: 0; act: -2 ),
{ 73: }
{ 74: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 409; act: 107 ),
{ 75: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
{ 76: }
{ 77: }
{ 78: }
  ( sym: 388; act: 76 ),
  ( sym: 389; act: 77 ),
  ( sym: 378; act: -59 ),
  ( sym: 380; act: -59 ),
  ( sym: 383; act: -59 ),
  ( sym: 384; act: -59 ),
  ( sym: 385; act: -59 ),
  ( sym: 387; act: -59 ),
{ 79: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
{ 80: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 81: }
  ( sym: 380; act: 111 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 82: }
  ( sym: 409; act: 112 ),
{ 83: }
  ( sym: 409; act: 113 ),
{ 84: }
  ( sym: 409; act: 115 ),
  ( sym: 413; act: 116 ),
{ 85: }
  ( sym: 387; act: 117 ),
{ 86: }
{ 87: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 88: }
  ( sym: 409; act: 119 ),
{ 89: }
  ( sym: 409; act: 120 ),
{ 90: }
  ( sym: 409; act: 121 ),
  ( sym: 413; act: 122 ),
{ 91: }
  ( sym: 409; act: 123 ),
{ 92: }
  ( sym: 413; act: 124 ),
{ 93: }
  ( sym: 383; act: 125 ),
{ 94: }
  ( sym: 382; act: 126 ),
{ 95: }
  ( sym: 409; act: 63 ),
{ 96: }
  ( sym: 409; act: 128 ),
{ 97: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 397; act: 131 ),
  ( sym: 409; act: 132 ),
{ 98: }
  ( sym: 409; act: 133 ),
{ 99: }
  ( sym: 413; act: 134 ),
{ 100: }
  ( sym: 409; act: 135 ),
{ 101: }
  ( sym: 409; act: 136 ),
{ 102: }
  ( sym: 413; act: 137 ),
{ 103: }
  ( sym: 413; act: 138 ),
{ 104: }
{ 105: }
{ 106: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -42 ),
{ 107: }
  ( sym: 378; act: 139 ),
  ( sym: 387; act: -41 ),
{ 108: }
{ 109: }
  ( sym: 388; act: 76 ),
  ( sym: 389; act: 77 ),
  ( sym: 378; act: -60 ),
  ( sym: 380; act: -60 ),
  ( sym: 383; act: -60 ),
  ( sym: 384; act: -60 ),
  ( sym: 385; act: -60 ),
  ( sym: 387; act: -60 ),
{ 110: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -23 ),
{ 111: }
{ 112: }
{ 113: }
{ 114: }
{ 115: }
{ 116: }
{ 117: }
{ 118: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 378; act: -70 ),
  ( sym: 387; act: -70 ),
{ 119: }
  ( sym: 378; act: 140 ),
{ 120: }
  ( sym: 378; act: 141 ),
{ 121: }
  ( sym: 378; act: 142 ),
{ 122: }
  ( sym: 378; act: 143 ),
{ 123: }
{ 124: }
{ 125: }
  ( sym: 381; act: 144 ),
{ 126: }
  ( sym: 409; act: 145 ),
{ 127: }
{ 128: }
{ 129: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -45 ),
{ 130: }
{ 131: }
{ 132: }
{ 133: }
  ( sym: 378; act: 146 ),
{ 134: }
  ( sym: 378; act: 147 ),
{ 135: }
  ( sym: 378; act: 148 ),
{ 136: }
{ 137: }
{ 138: }
  ( sym: 378; act: 149 ),
{ 139: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 140: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 141: }
  ( sym: 409; act: 152 ),
{ 142: }
  ( sym: 409; act: 153 ),
{ 143: }
  ( sym: 409; act: 115 ),
  ( sym: 413; act: 116 ),
{ 144: }
  ( sym: 378; act: 155 ),
{ 145: }
  ( sym: 378; act: 156 ),
{ 146: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 409; act: 53 ),
{ 147: }
  ( sym: 409; act: 115 ),
  ( sym: 413; act: 116 ),
{ 148: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 409; act: 107 ),
{ 149: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
{ 150: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -40 ),
{ 151: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -16 ),
{ 152: }
{ 153: }
{ 154: }
{ 155: }
  ( sym: 409; act: 63 ),
{ 156: }
  ( sym: 375; act: 40 ),
  ( sym: 379; act: 41 ),
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 409; act: 53 ),
{ 157: }
{ 158: }
{ 159: }
{ 160: }
  ( sym: 384; act: 42 ),
  ( sym: 385; act: 43 ),
  ( sym: 387; act: -33 ),
{ 161: }
  ( sym: 378; act: 95 ),
  ( sym: 387; act: -19 ),
{ 162: }
  ( sym: 383; act: 163 )
{ 163: }
);

yyg : array [1..yyngotos] of YYARec = (
{ 0: }
  ( sym: -7; act: 1 ),
  ( sym: -6; act: 2 ),
  ( sym: -3; act: 3 ),
  ( sym: -2; act: 4 ),
{ 1: }
{ 2: }
{ 3: }
  ( sym: -7; act: 1 ),
  ( sym: -6; act: 32 ),
  ( sym: -4; act: 33 ),
{ 4: }
{ 5: }
{ 6: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 39 ),
{ 7: }
{ 8: }
{ 9: }
{ 10: }
{ 11: }
{ 12: }
{ 13: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -14; act: 49 ),
  ( sym: -10; act: 50 ),
{ 14: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 51 ),
  ( sym: -9; act: 52 ),
{ 15: }
{ 16: }
{ 17: }
{ 18: }
{ 19: }
{ 20: }
{ 21: }
{ 22: }
  ( sym: -17; act: 61 ),
  ( sym: -12; act: 62 ),
{ 23: }
{ 24: }
{ 25: }
{ 26: }
{ 27: }
{ 28: }
{ 29: }
{ 30: }
{ 31: }
{ 32: }
{ 33: }
{ 34: }
  ( sym: -5; act: 72 ),
{ 35: }
{ 36: }
{ 37: }
  ( sym: -21; act: 75 ),
{ 38: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 78 ),
{ 39: }
  ( sym: -18; act: 79 ),
{ 40: }
{ 41: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 81 ),
{ 42: }
{ 43: }
{ 44: }
{ 45: }
{ 46: }
{ 47: }
{ 48: }
  ( sym: -7; act: 85 ),
{ 49: }
{ 50: }
  ( sym: -18; act: 79 ),
{ 51: }
  ( sym: -18; act: 79 ),
{ 52: }
{ 53: }
{ 54: }
{ 55: }
{ 56: }
{ 57: }
{ 58: }
{ 59: }
{ 60: }
{ 61: }
{ 62: }
{ 63: }
{ 64: }
{ 65: }
{ 66: }
{ 67: }
{ 68: }
{ 69: }
{ 70: }
{ 71: }
{ 72: }
{ 73: }
{ 74: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -11; act: 105 ),
  ( sym: -10; act: 106 ),
{ 75: }
  ( sym: -20; act: 108 ),
{ 76: }
{ 77: }
{ 78: }
  ( sym: -21; act: 75 ),
{ 79: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 109 ),
{ 80: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 110 ),
{ 81: }
  ( sym: -18; act: 79 ),
{ 82: }
{ 83: }
{ 84: }
  ( sym: -13; act: 114 ),
{ 85: }
{ 86: }
{ 87: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 118 ),
{ 88: }
{ 89: }
{ 90: }
{ 91: }
{ 92: }
{ 93: }
{ 94: }
{ 95: }
  ( sym: -17; act: 127 ),
{ 96: }
{ 97: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 129 ),
  ( sym: -8; act: 130 ),
{ 98: }
{ 99: }
{ 100: }
{ 101: }
{ 102: }
{ 103: }
{ 104: }
{ 105: }
{ 106: }
  ( sym: -18; act: 79 ),
{ 107: }
{ 108: }
{ 109: }
  ( sym: -21; act: 75 ),
{ 110: }
  ( sym: -18; act: 79 ),
{ 111: }
{ 112: }
{ 113: }
{ 114: }
{ 115: }
{ 116: }
{ 117: }
{ 118: }
  ( sym: -18; act: 79 ),
{ 119: }
{ 120: }
{ 121: }
{ 122: }
{ 123: }
{ 124: }
{ 125: }
{ 126: }
{ 127: }
{ 128: }
{ 129: }
  ( sym: -18; act: 79 ),
{ 130: }
{ 131: }
{ 132: }
{ 133: }
{ 134: }
{ 135: }
{ 136: }
{ 137: }
{ 138: }
{ 139: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 150 ),
{ 140: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 151 ),
{ 141: }
{ 142: }
{ 143: }
  ( sym: -13; act: 154 ),
{ 144: }
{ 145: }
{ 146: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 51 ),
  ( sym: -9; act: 157 ),
{ 147: }
  ( sym: -13; act: 158 ),
{ 148: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -11; act: 159 ),
  ( sym: -10; act: 106 ),
{ 149: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 160 ),
{ 150: }
  ( sym: -18; act: 79 ),
{ 151: }
  ( sym: -18; act: 79 ),
{ 152: }
{ 153: }
{ 154: }
{ 155: }
  ( sym: -17; act: 61 ),
  ( sym: -12; act: 161 ),
{ 156: }
  ( sym: -20; act: 36 ),
  ( sym: -19; act: 37 ),
  ( sym: -18; act: 38 ),
  ( sym: -10; act: 51 ),
  ( sym: -9; act: 162 ),
{ 157: }
{ 158: }
{ 159: }
{ 160: }
  ( sym: -18; act: 79 )
{ 161: }
{ 162: }
{ 163: }
);

yyd : array [0..yynstates-1] of Integer = (
{ 0: } 0,
{ 1: } 0,
{ 2: } -11,
{ 3: } 0,
{ 4: } 0,
{ 5: } 0,
{ 6: } 0,
{ 7: } 0,
{ 8: } 0,
{ 9: } 0,
{ 10: } 0,
{ 11: } 0,
{ 12: } -7,
{ 13: } 0,
{ 14: } 0,
{ 15: } 0,
{ 16: } 0,
{ 17: } 0,
{ 18: } 0,
{ 19: } 0,
{ 20: } 0,
{ 21: } -13,
{ 22: } 0,
{ 23: } 0,
{ 24: } 0,
{ 25: } 0,
{ 26: } 0,
{ 27: } 0,
{ 28: } 0,
{ 29: } 0,
{ 30: } 0,
{ 31: } -10,
{ 32: } -12,
{ 33: } -1,
{ 34: } 0,
{ 35: } 0,
{ 36: } -61,
{ 37: } 0,
{ 38: } 0,
{ 39: } 0,
{ 40: } -63,
{ 41: } 0,
{ 42: } -66,
{ 43: } -65,
{ 44: } 0,
{ 45: } 0,
{ 46: } -27,
{ 47: } 0,
{ 48: } 0,
{ 49: } 0,
{ 50: } 0,
{ 51: } 0,
{ 52: } -20,
{ 53: } -46,
{ 54: } 0,
{ 55: } 0,
{ 56: } 0,
{ 57: } 0,
{ 58: } 0,
{ 59: } 0,
{ 60: } 0,
{ 61: } -54,
{ 62: } 0,
{ 63: } 0,
{ 64: } -22,
{ 65: } 0,
{ 66: } 0,
{ 67: } 0,
{ 68: } 0,
{ 69: } 0,
{ 70: } 0,
{ 71: } 0,
{ 72: } 0,
{ 73: } -4,
{ 74: } 0,
{ 75: } 0,
{ 76: } -67,
{ 77: } -68,
{ 78: } 0,
{ 79: } 0,
{ 80: } 0,
{ 81: } 0,
{ 82: } 0,
{ 83: } 0,
{ 84: } 0,
{ 85: } 0,
{ 86: } -9,
{ 87: } 0,
{ 88: } 0,
{ 89: } 0,
{ 90: } 0,
{ 91: } 0,
{ 92: } 0,
{ 93: } 0,
{ 94: } 0,
{ 95: } 0,
{ 96: } 0,
{ 97: } 0,
{ 98: } 0,
{ 99: } 0,
{ 100: } 0,
{ 101: } 0,
{ 102: } 0,
{ 103: } 0,
{ 104: } -5,
{ 105: } -24,
{ 106: } 0,
{ 107: } 0,
{ 108: } -62,
{ 109: } 0,
{ 110: } 0,
{ 111: } -64,
{ 112: } -28,
{ 113: } -25,
{ 114: } -30,
{ 115: } -48,
{ 116: } -49,
{ 117: } -8,
{ 118: } 0,
{ 119: } 0,
{ 120: } 0,
{ 121: } 0,
{ 122: } 0,
{ 123: } -37,
{ 124: } -38,
{ 125: } 0,
{ 126: } 0,
{ 127: } -55,
{ 128: } -52,
{ 129: } 0,
{ 130: } -14,
{ 131: } -44,
{ 132: } -43,
{ 133: } 0,
{ 134: } 0,
{ 135: } 0,
{ 136: } -31,
{ 137: } -29,
{ 138: } 0,
{ 139: } 0,
{ 140: } 0,
{ 141: } 0,
{ 142: } 0,
{ 143: } 0,
{ 144: } 0,
{ 145: } 0,
{ 146: } 0,
{ 147: } 0,
{ 148: } 0,
{ 149: } 0,
{ 150: } 0,
{ 151: } 0,
{ 152: } -34,
{ 153: } -35,
{ 154: } -36,
{ 155: } 0,
{ 156: } 0,
{ 157: } -15,
{ 158: } -32,
{ 159: } -17,
{ 160: } 0,
{ 161: } 0,
{ 162: } 0,
{ 163: } -18
);

yyal : array [0..yynstates-1] of Integer = (
{ 0: } 1,
{ 1: } 28,
{ 2: } 29,
{ 3: } 29,
{ 4: } 56,
{ 5: } 57,
{ 6: } 58,
{ 7: } 62,
{ 8: } 63,
{ 9: } 64,
{ 10: } 65,
{ 11: } 66,
{ 12: } 67,
{ 13: } 67,
{ 14: } 71,
{ 15: } 76,
{ 16: } 77,
{ 17: } 78,
{ 18: } 79,
{ 19: } 81,
{ 20: } 82,
{ 21: } 83,
{ 22: } 83,
{ 23: } 84,
{ 24: } 85,
{ 25: } 86,
{ 26: } 87,
{ 27: } 88,
{ 28: } 89,
{ 29: } 90,
{ 30: } 91,
{ 31: } 92,
{ 32: } 92,
{ 33: } 92,
{ 34: } 92,
{ 35: } 94,
{ 36: } 95,
{ 37: } 95,
{ 38: } 103,
{ 39: } 105,
{ 40: } 108,
{ 41: } 108,
{ 42: } 112,
{ 43: } 112,
{ 44: } 112,
{ 45: } 113,
{ 46: } 115,
{ 47: } 115,
{ 48: } 116,
{ 49: } 141,
{ 50: } 143,
{ 51: } 147,
{ 52: } 151,
{ 53: } 151,
{ 54: } 151,
{ 55: } 152,
{ 56: } 153,
{ 57: } 154,
{ 58: } 155,
{ 59: } 156,
{ 60: } 157,
{ 61: } 158,
{ 62: } 158,
{ 63: } 160,
{ 64: } 163,
{ 65: } 163,
{ 66: } 164,
{ 67: } 165,
{ 68: } 166,
{ 69: } 167,
{ 70: } 168,
{ 71: } 169,
{ 72: } 170,
{ 73: } 172,
{ 74: } 172,
{ 75: } 177,
{ 76: } 179,
{ 77: } 179,
{ 78: } 179,
{ 79: } 187,
{ 80: } 189,
{ 81: } 193,
{ 82: } 196,
{ 83: } 197,
{ 84: } 198,
{ 85: } 200,
{ 86: } 201,
{ 87: } 201,
{ 88: } 205,
{ 89: } 206,
{ 90: } 207,
{ 91: } 209,
{ 92: } 210,
{ 93: } 211,
{ 94: } 212,
{ 95: } 213,
{ 96: } 214,
{ 97: } 215,
{ 98: } 221,
{ 99: } 222,
{ 100: } 223,
{ 101: } 224,
{ 102: } 225,
{ 103: } 226,
{ 104: } 227,
{ 105: } 227,
{ 106: } 227,
{ 107: } 230,
{ 108: } 232,
{ 109: } 232,
{ 110: } 240,
{ 111: } 243,
{ 112: } 243,
{ 113: } 243,
{ 114: } 243,
{ 115: } 243,
{ 116: } 243,
{ 117: } 243,
{ 118: } 243,
{ 119: } 247,
{ 120: } 248,
{ 121: } 249,
{ 122: } 250,
{ 123: } 251,
{ 124: } 251,
{ 125: } 251,
{ 126: } 252,
{ 127: } 253,
{ 128: } 253,
{ 129: } 253,
{ 130: } 256,
{ 131: } 256,
{ 132: } 256,
{ 133: } 256,
{ 134: } 257,
{ 135: } 258,
{ 136: } 259,
{ 137: } 259,
{ 138: } 259,
{ 139: } 260,
{ 140: } 264,
{ 141: } 268,
{ 142: } 269,
{ 143: } 270,
{ 144: } 272,
{ 145: } 273,
{ 146: } 274,
{ 147: } 279,
{ 148: } 281,
{ 149: } 286,
{ 150: } 290,
{ 151: } 293,
{ 152: } 296,
{ 153: } 296,
{ 154: } 296,
{ 155: } 296,
{ 156: } 297,
{ 157: } 302,
{ 158: } 302,
{ 159: } 302,
{ 160: } 302,
{ 161: } 305,
{ 162: } 307,
{ 163: } 308
);

yyah : array [0..yynstates-1] of Integer = (
{ 0: } 27,
{ 1: } 28,
{ 2: } 28,
{ 3: } 55,
{ 4: } 56,
{ 5: } 57,
{ 6: } 61,
{ 7: } 62,
{ 8: } 63,
{ 9: } 64,
{ 10: } 65,
{ 11: } 66,
{ 12: } 66,
{ 13: } 70,
{ 14: } 75,
{ 15: } 76,
{ 16: } 77,
{ 17: } 78,
{ 18: } 80,
{ 19: } 81,
{ 20: } 82,
{ 21: } 82,
{ 22: } 83,
{ 23: } 84,
{ 24: } 85,
{ 25: } 86,
{ 26: } 87,
{ 27: } 88,
{ 28: } 89,
{ 29: } 90,
{ 30: } 91,
{ 31: } 91,
{ 32: } 91,
{ 33: } 91,
{ 34: } 93,
{ 35: } 94,
{ 36: } 94,
{ 37: } 102,
{ 38: } 104,
{ 39: } 107,
{ 40: } 107,
{ 41: } 111,
{ 42: } 111,
{ 43: } 111,
{ 44: } 112,
{ 45: } 114,
{ 46: } 114,
{ 47: } 115,
{ 48: } 140,
{ 49: } 142,
{ 50: } 146,
{ 51: } 150,
{ 52: } 150,
{ 53: } 150,
{ 54: } 151,
{ 55: } 152,
{ 56: } 153,
{ 57: } 154,
{ 58: } 155,
{ 59: } 156,
{ 60: } 157,
{ 61: } 157,
{ 62: } 159,
{ 63: } 162,
{ 64: } 162,
{ 65: } 163,
{ 66: } 164,
{ 67: } 165,
{ 68: } 166,
{ 69: } 167,
{ 70: } 168,
{ 71: } 169,
{ 72: } 171,
{ 73: } 171,
{ 74: } 176,
{ 75: } 178,
{ 76: } 178,
{ 77: } 178,
{ 78: } 186,
{ 79: } 188,
{ 80: } 192,
{ 81: } 195,
{ 82: } 196,
{ 83: } 197,
{ 84: } 199,
{ 85: } 200,
{ 86: } 200,
{ 87: } 204,
{ 88: } 205,
{ 89: } 206,
{ 90: } 208,
{ 91: } 209,
{ 92: } 210,
{ 93: } 211,
{ 94: } 212,
{ 95: } 213,
{ 96: } 214,
{ 97: } 220,
{ 98: } 221,
{ 99: } 222,
{ 100: } 223,
{ 101: } 224,
{ 102: } 225,
{ 103: } 226,
{ 104: } 226,
{ 105: } 226,
{ 106: } 229,
{ 107: } 231,
{ 108: } 231,
{ 109: } 239,
{ 110: } 242,
{ 111: } 242,
{ 112: } 242,
{ 113: } 242,
{ 114: } 242,
{ 115: } 242,
{ 116: } 242,
{ 117: } 242,
{ 118: } 246,
{ 119: } 247,
{ 120: } 248,
{ 121: } 249,
{ 122: } 250,
{ 123: } 250,
{ 124: } 250,
{ 125: } 251,
{ 126: } 252,
{ 127: } 252,
{ 128: } 252,
{ 129: } 255,
{ 130: } 255,
{ 131: } 255,
{ 132: } 255,
{ 133: } 256,
{ 134: } 257,
{ 135: } 258,
{ 136: } 258,
{ 137: } 258,
{ 138: } 259,
{ 139: } 263,
{ 140: } 267,
{ 141: } 268,
{ 142: } 269,
{ 143: } 271,
{ 144: } 272,
{ 145: } 273,
{ 146: } 278,
{ 147: } 280,
{ 148: } 285,
{ 149: } 289,
{ 150: } 292,
{ 151: } 295,
{ 152: } 295,
{ 153: } 295,
{ 154: } 295,
{ 155: } 296,
{ 156: } 301,
{ 157: } 301,
{ 158: } 301,
{ 159: } 301,
{ 160: } 304,
{ 161: } 306,
{ 162: } 307,
{ 163: } 307
);

yygl : array [0..yynstates-1] of Integer = (
{ 0: } 1,
{ 1: } 5,
{ 2: } 5,
{ 3: } 5,
{ 4: } 8,
{ 5: } 8,
{ 6: } 8,
{ 7: } 12,
{ 8: } 12,
{ 9: } 12,
{ 10: } 12,
{ 11: } 12,
{ 12: } 12,
{ 13: } 12,
{ 14: } 17,
{ 15: } 22,
{ 16: } 22,
{ 17: } 22,
{ 18: } 22,
{ 19: } 22,
{ 20: } 22,
{ 21: } 22,
{ 22: } 22,
{ 23: } 24,
{ 24: } 24,
{ 25: } 24,
{ 26: } 24,
{ 27: } 24,
{ 28: } 24,
{ 29: } 24,
{ 30: } 24,
{ 31: } 24,
{ 32: } 24,
{ 33: } 24,
{ 34: } 24,
{ 35: } 25,
{ 36: } 25,
{ 37: } 25,
{ 38: } 26,
{ 39: } 28,
{ 40: } 29,
{ 41: } 29,
{ 42: } 33,
{ 43: } 33,
{ 44: } 33,
{ 45: } 33,
{ 46: } 33,
{ 47: } 33,
{ 48: } 33,
{ 49: } 34,
{ 50: } 34,
{ 51: } 35,
{ 52: } 36,
{ 53: } 36,
{ 54: } 36,
{ 55: } 36,
{ 56: } 36,
{ 57: } 36,
{ 58: } 36,
{ 59: } 36,
{ 60: } 36,
{ 61: } 36,
{ 62: } 36,
{ 63: } 36,
{ 64: } 36,
{ 65: } 36,
{ 66: } 36,
{ 67: } 36,
{ 68: } 36,
{ 69: } 36,
{ 70: } 36,
{ 71: } 36,
{ 72: } 36,
{ 73: } 36,
{ 74: } 36,
{ 75: } 41,
{ 76: } 42,
{ 77: } 42,
{ 78: } 42,
{ 79: } 43,
{ 80: } 45,
{ 81: } 49,
{ 82: } 50,
{ 83: } 50,
{ 84: } 50,
{ 85: } 51,
{ 86: } 51,
{ 87: } 51,
{ 88: } 55,
{ 89: } 55,
{ 90: } 55,
{ 91: } 55,
{ 92: } 55,
{ 93: } 55,
{ 94: } 55,
{ 95: } 55,
{ 96: } 56,
{ 97: } 56,
{ 98: } 61,
{ 99: } 61,
{ 100: } 61,
{ 101: } 61,
{ 102: } 61,
{ 103: } 61,
{ 104: } 61,
{ 105: } 61,
{ 106: } 61,
{ 107: } 62,
{ 108: } 62,
{ 109: } 62,
{ 110: } 63,
{ 111: } 64,
{ 112: } 64,
{ 113: } 64,
{ 114: } 64,
{ 115: } 64,
{ 116: } 64,
{ 117: } 64,
{ 118: } 64,
{ 119: } 65,
{ 120: } 65,
{ 121: } 65,
{ 122: } 65,
{ 123: } 65,
{ 124: } 65,
{ 125: } 65,
{ 126: } 65,
{ 127: } 65,
{ 128: } 65,
{ 129: } 65,
{ 130: } 66,
{ 131: } 66,
{ 132: } 66,
{ 133: } 66,
{ 134: } 66,
{ 135: } 66,
{ 136: } 66,
{ 137: } 66,
{ 138: } 66,
{ 139: } 66,
{ 140: } 70,
{ 141: } 74,
{ 142: } 74,
{ 143: } 74,
{ 144: } 75,
{ 145: } 75,
{ 146: } 75,
{ 147: } 80,
{ 148: } 81,
{ 149: } 86,
{ 150: } 90,
{ 151: } 91,
{ 152: } 92,
{ 153: } 92,
{ 154: } 92,
{ 155: } 92,
{ 156: } 94,
{ 157: } 99,
{ 158: } 99,
{ 159: } 99,
{ 160: } 99,
{ 161: } 100,
{ 162: } 100,
{ 163: } 100
);

yygh : array [0..yynstates-1] of Integer = (
{ 0: } 4,
{ 1: } 4,
{ 2: } 4,
{ 3: } 7,
{ 4: } 7,
{ 5: } 7,
{ 6: } 11,
{ 7: } 11,
{ 8: } 11,
{ 9: } 11,
{ 10: } 11,
{ 11: } 11,
{ 12: } 11,
{ 13: } 16,
{ 14: } 21,
{ 15: } 21,
{ 16: } 21,
{ 17: } 21,
{ 18: } 21,
{ 19: } 21,
{ 20: } 21,
{ 21: } 21,
{ 22: } 23,
{ 23: } 23,
{ 24: } 23,
{ 25: } 23,
{ 26: } 23,
{ 27: } 23,
{ 28: } 23,
{ 29: } 23,
{ 30: } 23,
{ 31: } 23,
{ 32: } 23,
{ 33: } 23,
{ 34: } 24,
{ 35: } 24,
{ 36: } 24,
{ 37: } 25,
{ 38: } 27,
{ 39: } 28,
{ 40: } 28,
{ 41: } 32,
{ 42: } 32,
{ 43: } 32,
{ 44: } 32,
{ 45: } 32,
{ 46: } 32,
{ 47: } 32,
{ 48: } 33,
{ 49: } 33,
{ 50: } 34,
{ 51: } 35,
{ 52: } 35,
{ 53: } 35,
{ 54: } 35,
{ 55: } 35,
{ 56: } 35,
{ 57: } 35,
{ 58: } 35,
{ 59: } 35,
{ 60: } 35,
{ 61: } 35,
{ 62: } 35,
{ 63: } 35,
{ 64: } 35,
{ 65: } 35,
{ 66: } 35,
{ 67: } 35,
{ 68: } 35,
{ 69: } 35,
{ 70: } 35,
{ 71: } 35,
{ 72: } 35,
{ 73: } 35,
{ 74: } 40,
{ 75: } 41,
{ 76: } 41,
{ 77: } 41,
{ 78: } 42,
{ 79: } 44,
{ 80: } 48,
{ 81: } 49,
{ 82: } 49,
{ 83: } 49,
{ 84: } 50,
{ 85: } 50,
{ 86: } 50,
{ 87: } 54,
{ 88: } 54,
{ 89: } 54,
{ 90: } 54,
{ 91: } 54,
{ 92: } 54,
{ 93: } 54,
{ 94: } 54,
{ 95: } 55,
{ 96: } 55,
{ 97: } 60,
{ 98: } 60,
{ 99: } 60,
{ 100: } 60,
{ 101: } 60,
{ 102: } 60,
{ 103: } 60,
{ 104: } 60,
{ 105: } 60,
{ 106: } 61,
{ 107: } 61,
{ 108: } 61,
{ 109: } 62,
{ 110: } 63,
{ 111: } 63,
{ 112: } 63,
{ 113: } 63,
{ 114: } 63,
{ 115: } 63,
{ 116: } 63,
{ 117: } 63,
{ 118: } 64,
{ 119: } 64,
{ 120: } 64,
{ 121: } 64,
{ 122: } 64,
{ 123: } 64,
{ 124: } 64,
{ 125: } 64,
{ 126: } 64,
{ 127: } 64,
{ 128: } 64,
{ 129: } 65,
{ 130: } 65,
{ 131: } 65,
{ 132: } 65,
{ 133: } 65,
{ 134: } 65,
{ 135: } 65,
{ 136: } 65,
{ 137: } 65,
{ 138: } 65,
{ 139: } 69,
{ 140: } 73,
{ 141: } 73,
{ 142: } 73,
{ 143: } 74,
{ 144: } 74,
{ 145: } 74,
{ 146: } 79,
{ 147: } 80,
{ 148: } 85,
{ 149: } 89,
{ 150: } 90,
{ 151: } 91,
{ 152: } 91,
{ 153: } 91,
{ 154: } 91,
{ 155: } 93,
{ 156: } 98,
{ 157: } 98,
{ 158: } 98,
{ 159: } 98,
{ 160: } 99,
{ 161: } 99,
{ 162: } 99,
{ 163: } 99
);

yyr : array [1..yynrules] of YYRRec = (
{ 1: } ( len: 2; sym: -2 ),
{ 2: } ( len: 2; sym: -4 ),
{ 3: } ( len: 1; sym: -4 ),
{ 4: } ( len: 1; sym: -5 ),
{ 5: } ( len: 2; sym: -5 ),
{ 6: } ( len: 0; sym: -6 ),
{ 7: } ( len: 1; sym: -6 ),
{ 8: } ( len: 4; sym: -6 ),
{ 9: } ( len: 3; sym: -6 ),
{ 10: } ( len: 2; sym: -6 ),
{ 11: } ( len: 1; sym: -3 ),
{ 12: } ( len: 2; sym: -3 ),
{ 13: } ( len: 1; sym: -7 ),
{ 14: } ( len: 4; sym: -7 ),
{ 15: } ( len: 6; sym: -7 ),
{ 16: } ( len: 6; sym: -7 ),
{ 17: } ( len: 6; sym: -7 ),
{ 18: } ( len: 8; sym: -7 ),
{ 19: } ( len: 7; sym: -7 ),
{ 20: } ( len: 2; sym: -7 ),
{ 21: } ( len: 2; sym: -7 ),
{ 22: } ( len: 2; sym: -7 ),
{ 23: } ( len: 4; sym: -7 ),
{ 24: } ( len: 4; sym: -7 ),
{ 25: } ( len: 4; sym: -7 ),
{ 26: } ( len: 2; sym: -7 ),
{ 27: } ( len: 2; sym: -7 ),
{ 28: } ( len: 4; sym: -7 ),
{ 29: } ( len: 4; sym: -7 ),
{ 30: } ( len: 4; sym: -7 ),
{ 31: } ( len: 4; sym: -7 ),
{ 32: } ( len: 6; sym: -7 ),
{ 33: } ( len: 6; sym: -7 ),
{ 34: } ( len: 6; sym: -7 ),
{ 35: } ( len: 6; sym: -7 ),
{ 36: } ( len: 6; sym: -7 ),
{ 37: } ( len: 4; sym: -7 ),
{ 38: } ( len: 4; sym: -7 ),
{ 39: } ( len: 2; sym: -7 ),
{ 40: } ( len: 3; sym: -11 ),
{ 41: } ( len: 1; sym: -11 ),
{ 42: } ( len: 1; sym: -11 ),
{ 43: } ( len: 1; sym: -8 ),
{ 44: } ( len: 1; sym: -8 ),
{ 45: } ( len: 1; sym: -8 ),
{ 46: } ( len: 1; sym: -9 ),
{ 47: } ( len: 1; sym: -9 ),
{ 48: } ( len: 1; sym: -13 ),
{ 49: } ( len: 1; sym: -13 ),
{ 50: } ( len: 1; sym: -15 ),
{ 51: } ( len: 1; sym: -15 ),
{ 52: } ( len: 3; sym: -17 ),
{ 53: } ( len: 1; sym: -17 ),
{ 54: } ( len: 1; sym: -12 ),
{ 55: } ( len: 3; sym: -12 ),
{ 56: } ( len: 3; sym: -16 ),
{ 57: } ( len: 1; sym: -16 ),
{ 58: } ( len: 1; sym: -10 ),
{ 59: } ( len: 2; sym: -10 ),
{ 60: } ( len: 3; sym: -10 ),
{ 61: } ( len: 1; sym: -19 ),
{ 62: } ( len: 3; sym: -19 ),
{ 63: } ( len: 1; sym: -20 ),
{ 64: } ( len: 3; sym: -20 ),
{ 65: } ( len: 1; sym: -18 ),
{ 66: } ( len: 1; sym: -18 ),
{ 67: } ( len: 1; sym: -21 ),
{ 68: } ( len: 1; sym: -21 ),
{ 69: } ( len: 1; sym: -14 ),
{ 70: } ( len: 3; sym: -14 )
);


const _error = 256; (* error token *)

function yyact(state, sym : Integer; var Act: Integer): Boolean;
  (* search action table *)
Var K: Integer;
begin
 k := yyal[state];
 While (k <= yyah[state]) and (yya[k].sym <> sym) do Inc(K);
 If k > yyah[state] then Result := false else
 begin
  act := yya[k].act;
  Result := true;
 end;
end;

function yygoto(state, sym: Integer; var NState: Integer): Boolean;
  (* search goto table *)
Var K: Integer;
begin
 K := yygl[state];
 While (k <= yygh[state]) and (yyg[k].sym <> sym) do Inc(K);
 If k > yygh[state] then Result := False else
 begin
  NState := yyg[k].act;
  Result := True;
 end;
end;

function yyparse_(Parser: TCParser): Integer;
Var yyParseState, yyn: Integer;
Label parse_, next, error, errlab, shift, reduce, accept, abort;
begin
 With Parser, Yacc do
 begin
  (* initialize: *)
  yyParseState := 0; yychar := -1; yynerrs := 0; 
  yyerrflag := 0; yysp := 0;
 parse_:
  (* push state and value: *)
  Inc(yysp);
  If yysp > yymaxdepth then
  begin
   yyerror(LYS_StackOverflow);
   ErrorCode := LYE_StackOverflow;
   goto abort;
  end;
  yys[yysp] := yyParseState; yyv[yysp] := yyval;
next:
  If (yyd[yyParseState] = 0) and (yychar = -1) then
  (* get next symbol *)
  begin
   yychar := Lexer.yylex;
   if yychar < 0 then yychar := 0;
  end;
  if yydebug then 
   DebugMessage(Format('%s %d, %s %d', [LYS_State, yyParseState,
                                        LYS_Char, yychar]));
  (* determine parse action: *)
  yyn := yyd[yyParseState];
  if yyn<>0 then goto reduce; (* simple state *)
  (* no default action; search parse table *)
  if not yyact(yyParseState, yychar, yyn) then goto error
  else if yyn>0 then                      goto shift
  else if yyn<0 then                      goto reduce
  else                                    goto accept;
error:
  (* error; start error recovery: *)
  if yyerrflag = 0 then
  begin
   yyerror(LYS_SyntaxError);
   ErrorCode := LYE_SyntaxError;
  end;
errlab:
  if yyerrflag = 0 then Inc(yynerrs);     (* new error *)
  if yyerrflag <= 2 then                  (* incomplete recovery; try again *)
  begin
   yyerrflag := 3;
   (* uncover a state with shift action on error token *)
   while (yysp>0) and not (yyact(yys[yysp], _error, yyn) and (yyn>0) ) do
   begin
    if yydebug then
    begin
     If yysp > 1 then
      DebugMessage(Format(LYS_ERPops, [yys[yysp], yys[yysp-1]])) else
      DebugMessage(LYS_ERFails);
    end;
    Dec(yysp);
   end;
   If yysp = 0 then
   begin
    ErrorCode := LYE_FallenFromStack;
    goto abort; (* parser has fallen from stack; abort *)
   end;
   yyParseState := yyn;              (* simulate shift on error *)
   goto parse_;
  end else                      (* no shift yet; discard symbol *)
  begin
   if yydebug then DebugMessage(Format(LYS_ERDiscards, [yychar]));
   if yychar=0 then
   begin
    ErrorCode := LYE_ERDiscards;
    goto abort; (* end of input; abort *)
   end;
   yychar := -1; goto next;     (* clear lookahead char and try again *)
  end;
 shift:
  (* go to new state, clear lookahead character: *)
  yyParseState := yyn; yychar := -1; yyval := yylval;
  if yyerrflag>0 then dec(yyerrflag);
  goto parse_;
 reduce:
  (* execute action, pop rule from stack, and go to next state: *)
  if yydebug then DebugMessage(Format('%s %d', [LYS_Reduce, -yyn]));
  yyflag := yyfnone; yacc_action(Parser, -yyn);
  Dec(yysp, yyr[-yyn].len);
  if yygoto(yys[yysp], yyr[-yyn].sym, yyn) then yyParseState := yyn;
  (* handle action calls to yyaccept, yyabort and yyerror: *)
  case yyflag of
   yyfaccept:	goto accept;
   yyfabort:	goto abort;
   yyferror:	goto errlab;
  end;
  goto parse_;
 accept:
  Result := 0;
  ErrorCode := LYE_OK;
  Exit;
 abort:
  Result := 1;
 end;
end;

end.
