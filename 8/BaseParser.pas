unit BaseParser;

interface

uses
 SysUtils, Classes, NodeLst, RegEx;

const
 E_UNEXPECTED_ERROR = -1;
 E_OK               = 0;
 E_TOKEN_EXPECTED   = 1;
 E_NAME_REDECLARED  = 2;
 E_UNKNOWN_NAME     = 3;
 E_UNKNOWN_TYPE     = 4;

(* Token types *)
 T_INCLUDE          = -17;
 T_TEXT             = -16;
 T_DECLARE_NAME     = -15;
 T_DECLARE_TYPE     = -14;
 T_USE_UNIT         = -13;
 T_PRESET_TYPE      = -12;
 T_SET_TYPE         = -11;
 T_SET_PARENT_TYPE  = -10;
 T_REDECLARE        = -9;
 T_EXACT_NAME       = -8;
 T_RELATIVE_NAME    = -7;
 T_PARENT_PUSH      = -6;
 T_PARENT_POP       = -5;
 T_NULL             = -4;
 T_CRLF             = -3;
 T_NOTHING          = -2;
 T_TERMINATE        = -1;
(* Name flags *)
 NF_EXTERNAL        = 1;
 NF_TYPE            = 1 shl 1;
(* Type modes *)
 TM_NOT_A_TYPE      = 0;
 TM_VOID            = 1;
 TM_POINTER         = 2;
 TM_INTEGER         = 3;
 TM_FLOAT           = 4;
 TM_STRING          = 5;
 TM_ARRAY           = 6;
 TM_SET             = 7;
 TM_RECORD          = 8;
 TM_OBJECT          = 9;
 TM_LINK            = 10;

type
 TCustomParser = class;

 TTokenInfo = packed record
  tiType: Integer;
  case Boolean of
   False: (tiValue: Integer);
   True: (tiPtr: Pointer);
 end;

 TTokenNode = class(TNode)
  private
   FData: TTokenInfo;
 end;

 PNameInfo = ^TNameInfo;
 TNameInfo = packed record
  niName: WideString;
  niParent: PNameInfo;
  niType: PNameInfo;
  niUnitID: SmallInt;
  niMode: Word;
 end;

 TNameNode = class(TNode)
  private
   FData: TNameInfo;
 end;

 TLexicalActFnc = function(Sender: TObject; var AText: WideString;
                           var Info, Expected: TTokenInfo): Integer;
 TLexicalAction =
   function(Sender: TObject; var AText: WideString;
                             var Info, Expected: TTokenInfo): Integer of object;
 TSyntaxUnitActFnc = function(Sender: TObject;
   var Info, Expected: TTokenInfo): Integer;
 TSyntaxUnitAction =
   function(Sender: TObject; var Info, Expected: TTokenInfo): Integer of object;
 TSyntaxActFnc = function(Sender: TObject): Integer;
 TSyntaxAction = function(Sender: TObject): Integer of object;

 TCustomSyntax = class;

 TLexUnitIdx = record
  REX: Integer;
  ID: Integer;
  ACT: TLexicalActFnc;
 end;

 TLexUnitMini = record
  REX: WideString;
  ID: Integer;
  ACT: TLexicalActFnc;
 end;

 TLexicalUnit = record
  luRegEx: TRegExEngine;
  luID: Integer;
  case Boolean of
   False: (luAction: TLexicalAction);
   True: (luActFnc: TLexicalActFnc;
          luActObj: TCustomSyntax);
 end;

 TSyntaxToken = record
  stID: Integer;
 // stResultPush: Boolean;
  stAllowMismatch: Boolean;
 end;

 TSyntaxTokens = array of TSyntaxToken;

 TSyntaxVarRec = record
  svrTokens: TSyntaxTokens;
  case Boolean of
   False: (svrAction: TSyntaxAction);
   True: (svrActFnc: TSyntaxActFnc;
          svrActObj: TCustomSyntax);
 end;

 TSyntaxUnit = record
  suList: array of TSyntaxVarRec;
  case Boolean of
   False: (suAction: TSyntaxUnitAction);
   True: (suActFnc: TSyntaxUnitActFnc;
          suActObj: TCustomSyntax);
 end;

 TLexicalUnits = array of TLexicalUnit;
 TSyntaxUnits = array of TSyntaxUnit;

 TSyntaxParserState = record
  stSyntaxUnitIndex:  SmallInt;
  stSyntaxVarIndex:   SmallInt;
  stSyntaxTokenIndex: SmallInt;
  stSyntaxBlockMatch: Boolean;
 end;

 TCustomSyntax = class
  private
   FUseCRLFasToken: Boolean;
   FUseNULLasToken: Boolean;
   FCaseSensitive: Boolean;
  protected
   FEmptyPart: TSyntaxToken;
   FSyntaxBaseIndex: Integer;
   FLexicalUnits: TLexicalUnits;
   FRegexFreeNot: Boolean;
   FSyntaxUnits: TSyntaxUnits;
   procedure Initialize; virtual; abstract;
  public
   constructor Create;
   destructor Destroy; override;

   property LexicalUnits: TLexicalUnits read FLexicalUnits;
   property SyntaxUnits: TSyntaxUnits read FSyntaxUnits;
   property UseCRLFasToken: Boolean read FUseCRLFasToken
                                   write FUseCRLFasToken;
   property UseNULLasToken: Boolean read FUseNULLasToken
                                   write FUseNULLasToken;
   property CaseSensitive: Boolean read FCaseSensitive
                                  write FCaseSensitive;
 end;

 TTokenStream = class(TNodeList)
  protected
   procedure Initialize; override;
 end;

 TNamesList = class(TNodeList)
  protected
   procedure Initialize; override;
 end;

 TPointers = array of Pointer;
 TStrings = array of WideString;
 TOnIncludeFile = procedure(Sender: TCustomParser;
                            const AFileName: WideString;
                            var AFileText: WideString) of object;
 TOnFindExternalName = procedure(Sender: TCustomParser;
    const AName: WideString; AParent: PNameInfo;
     var AInfo: PNameInfo) of object;

 TParserState = (psTerminal, psError, psSkipBlank,
                 psStage1, psStage2, psMatch, psMismatch, psUnitPop);

 TCustomParser = class
  private
   FSyntax: TCustomSyntax;
   FDestroySyntax: Boolean;
   FOnIncludeFile: TOnIncludeFile;
   FOnFindExternalName: TOnFindExternalName;
   procedure SetSyntax(Value: TCustomSyntax);
  protected
   FNames: TNamesList;
   FValues: TStrings;
   FTokenStream: TTokenStream;
   FStackIndex: Integer;
   FNameParent: PNameInfo;
   FLastNameInfo: PNameInfo;
   FParentStack: TPointers;
   FPSCapacity: Integer;
   FSyntaxStack: array of TSyntaxParserState;
   FSSCapacity: Integer;
   FSSIndex: Integer;
   FUnitCounter: Integer;
   FDeclareCounter: Integer;
   FLastType: PNameInfo;
   FLastFileName: WideString;
   FLastCaretX: Integer;
   FLastLineNumber: Integer;
   FLastExpected: TTokenInfo;
   FSyntaxUnitIndex: Integer;
   FSyntaxVarIndex: Integer;
   FSyntaxTokenIndex: Integer;
   FSyntaxBlockMatch: Boolean;
   FParserState: TParserState;
   procedure AddName(const AName: WideString;
                     AParent, AType: PNameInfo;
                     AUnitID: SmallInt = -1;
                     ATypeMode: Word = TM_NOT_A_TYPE);
   procedure AddValue(const AValue: WideString);
   procedure SyntaxPush;
   procedure SyntaxPop;
   procedure Initialize; virtual;
   function GetToken(P: PWideChar; Len: Integer;
            var Info, Expected: TTokenInfo; var AText: WideString;
            var SkipCount: Integer): Integer; virtual;
   function IncludeFile(const AFileName: WideString): WideString; virtual;
   function FindExternalName(const AName: WideString;
     AParent: PNameInfo): PNameInfo; virtual;
   procedure AddToken(AType, AValue: Integer); virtual;
   procedure ParentPush; virtual;
   procedure ParentPop; virtual;
  public
   function FindExactName(const AName: WideString;
                          AParent: PNameInfo): PNameInfo;
   function FindRelativeName(const AName: WideString): PNameInfo;
   constructor Create; overload;
   constructor Create(ASyntax: TCustomSyntax; FreeAfterUse: Boolean = True); overload;
   function Execute(const FileName, FileText: WideString): Integer;
   destructor Destroy; override;

   property Syntax: TCustomSyntax read FSyntax
                                 write SetSyntax;
   property DestroySyntax: Boolean read FDestroySyntax
                                  write FDestroySyntax;
   property OnIncludeFile: TOnIncludeFile read FOnIncludeFile
                                         write FOnIncludeFile;
   property OnFindExternalName: TOnFindExternalName read FOnFindExternalName
                                                   write FOnFindExternalName;
 end;

implementation

{ TCustomParser }

procedure TCustomParser.AddName(const AName: WideString;
      AParent, AType: PNameInfo; AUnitID: SmallInt; ATypeMode: Word);
begin
 FLastNameInfo := Addr(TNameNode(FNames.LastNode).FData);
 with TNameNode(FNames.AddNode).FData do
 begin
  niName := AName;
  niParent := AParent;
  niType := AType;
  niUnitID := AUnitID;
  niMode:= ATypeMode;
 end;
end;

procedure TCustomParser.AddToken(AType, AValue: Integer);
begin
 with TTokenNode(FTokenStream.AddNode).FData do
 begin
  tiType := AType;
  tiValue := AValue;
 end;
end;

procedure TCustomParser.AddValue(const AValue: WideString);
var
 L: Integer;
begin
 L := Length(FValues);
 SetLength(FValues, L + 1);
 FValues[L] := AValue;
end;

constructor TCustomParser.Create(ASyntax: TCustomSyntax; FreeAfterUse: Boolean);
begin
 FSyntax := ASyntax;
 FDestroySyntax := FreeAfterUse;
 Initialize;
end;

constructor TCustomParser.Create;
begin
 Initialize;
end;

destructor TCustomParser.Destroy;
begin
 FTokenStream.Free;
 FNames.Free;
 if FDestroySyntax then FSyntax.Free; 
 inherited;
end;

function TCustomParser.Execute(const FileName, FileText: WideString): Integer;
var
 Expected, LastExpected: TTokenInfo;
 LastFileName: WideString;
 LastCaretX, LastLineNumber: Integer;

 procedure DoParse(Level: Integer; const FileName, FileText: WideString);
 var
  P: PWideChar; Info: TTokenInfo; S: WideString;
  CaretX, LineNumber, L, TextLen, I: Integer; X: PNameInfo;
  Node: TNode; NameNode: TNameNode absolute Node;
 label
  Error;
 begin
  CaretX := 0;
  LineNumber := 0;
  P := Pointer(FileText);
  TextLen := Length(FileText) + Integer(Level = 0);
  if P <> nil then while TextLen > 0 do
  begin
   Result := GetToken(P, TextLen, Info, Expected, S, L);
   if Result = E_OK then
   begin
    case Info.tiType of
     T_INCLUDE:
     begin
      DoParse(Level + 1, S, IncludeFile(S));
      if Result <> E_OK then Exit;
     end;    
     T_TEXT:
     begin
      AddValue(S);
      AddToken(T_TEXT, Length(FValues) - 1);
     end;
     T_DECLARE_NAME, T_DECLARE_TYPE, T_USE_UNIT:
     begin
      if FindExactName(S, FNameParent) <> nil then
      begin
       Result := E_NAME_REDECLARED;
       goto Error;
      end;
      if Info.tiType = T_USE_UNIT then
       I := FUnitCounter else
       I := -1;
      AddName(S, FNameParent, FLastType, I,
       Word(Info.tiType = T_DECLARE_TYPE) * Info.tiValue);
      Info.tiPtr := FLastNameInfo;
      AddToken(Info.tiType, Info.tiValue);
      if Info.tiType = T_USE_UNIT then
      begin
       Inc(FUnitCounter);
       ParentPush;
      end else Inc(FDeclareCounter);
     end;
     T_PRESET_TYPE:
     begin
      X := FindRelativeName(S);
      if (X = nil) or (X.niMode = TM_NOT_A_TYPE) then
      begin
       Result := E_UNKNOWN_TYPE;
       goto Error;
      end;
      FLastType := X;
      AddToken(T_PRESET_TYPE, Integer(X));
     end;
     T_SET_TYPE:
     begin
      X := FindRelativeName(S);
      if (X = nil) or (X.niMode = TM_NOT_A_TYPE) then
      begin
       Result := E_UNKNOWN_TYPE;
       goto Error;
      end;
      Node := FNames.LastNode;
      while FDeclareCounter > 0 do
      begin
       NameNode.FData.niType := X;
       Node := Node.Prev;
       Dec(FDeclareCounter);
      end;
      AddToken(T_SET_TYPE, Integer(X));
     end;
     T_SET_PARENT_TYPE:
     begin
      X := FindRelativeName(S);
      if (X = nil) or (X.niMode = TM_NOT_A_TYPE) then
      begin
       Result := E_UNKNOWN_TYPE;
       goto Error;
      end;
      FNameParent.niType := X;
      AddToken(T_SET_PARENT_TYPE, Integer(X));
     end;
     T_REDECLARE:
     begin
      X := FindExactName(S, FNameParent);
      if X <> nil then FLastNameInfo := X else
      begin
       AddName(S, FNameParent, FLastType);
       X := FLastNameInfo;
      end;
      AddToken(T_REDECLARE, Integer(X));
     end;
     T_EXACT_NAME:
     begin
      X := FindExactName(S, FNameParent);
      if X <> nil then
      begin
       FLastNameInfo := X;
       AddToken(T_EXACT_NAME, Integer(X));
      end else
      begin
       Result := E_UNKNOWN_NAME;
       goto Error;
      end;
     end;
     T_RELATIVE_NAME:
     begin
      X := FindRelativeName(S);
      if X = nil then
      begin
       Result := E_UNKNOWN_NAME;
       goto Error;
      end;
      AddToken(T_RELATIVE_NAME, Integer(X));
     end;
     T_PARENT_PUSH: ParentPush;
     T_PARENT_POP: ParentPop;
     T_NULL:
     begin
      if FSyntax.FUseNULLasToken then
       AddToken(Info.tiType, Info.tiValue);
      FParserState := psTerminal;
      Exit;
     end;
     T_CRLF:
     begin
      CaretX := 0;
      Inc(LineNumber, Info.tiValue);
      if FSyntax.FUseCRLFasToken then
       AddToken(Info.tiType, Info.tiValue);
     end;
     T_NOTHING: with Info do if tiValue > 0 then
     begin
      CaretX := 0;
      Inc(LineNumber, tiValue);
     end;
     T_TERMINATE:
     begin
      FParserState := psTerminal;     
      Exit;
     end;
     else AddToken(Info.tiType, Info.tiValue);
    end;
   end else
   begin Error:
    LastFileName := FileName;
    LastCaretX := CaretX;
    LastLineNumber := LineNumber;
    LastExpected := Expected;
    Exit;
   end;
   Inc(CaretX, L);
   Inc(P, L);
   Dec(TextLen, L);
  end;
 end;

begin
 Result := E_OK;
 FPSCapacity := 64;
 FSSCapacity := 64;
 Finalize(FParentStack);
 Finalize(FSyntaxStack);
 SetLength(FParentStack, 64);
 SetLength(FSyntaxStack, 64);
 FSyntaxUnitIndex := 0;
 FSyntaxVarIndex := 0;
 FSyntaxTokenIndex := 0;
 FParserState := psSkipBlank;
 FStackIndex := -1;
 FSSIndex := 0;
 FNameParent := nil;
 FDeclareCounter := 0;
 FUnitCounter := 0;
 FLastType := nil;
 DoParse(0, FileName, FileText);
 if (Result <> E_OK) or (FParserState <> psTerminal) then
 begin
  FLastFileName := LastFileName;
  FLastCaretX := LastCaretX;
  FLastLineNumber := LastLineNumber;
  FLastExpected := LastExpected;
  if Result = E_OK then Result := E_TOKEN_EXPECTED;
 end;
end;

function TCustomParser.FindExactName(const AName: WideString;
  AParent: PNameInfo): PNameInfo;
var
 Node: TNode; NameNode: TNameNode absolute Node;
label
 Finish;
begin
 if AParent <> nil then
 begin
  if AParent.niType <> nil then
  case AParent.niMode of
   TM_RECORD, TM_OBJECT: AParent := AParent.niType;
   TM_LINK: while AParent.niType <> nil do
   begin
    AParent := AParent.niType;
    if AParent.niMode = TM_OBJECT then Break;
   end;
  end;
  if AParent.niUnitID >= 0 then
  begin
   Result := FindExternalName(AName, AParent);
   goto Finish;
  end;
 end;
 Result := nil;
 Node := FNames.LastNode;
 while Node <> nil do with NameNode, FData do
 begin
  if (AParent = niParent) and (AName = niName) then
  begin
   Result := Addr(FData);
   Break;
  end;
  Node := Prev;
 end;
 Finish:
 if (Result = nil) and (AParent.niMode <> TM_NOT_A_TYPE) then
 begin
  AParent := AParent.niType;
  if (AParent <> nil) and (AParent.niMode = TM_OBJECT) then
   Result := FindExactName(AName, AParent);
 end;
end;

function TCustomParser.FindExternalName(const AName: WideString;
  AParent: PNameInfo): PNameInfo;
begin
 if Assigned(FOnFindExternalName) then
  FOnFindExternalName(Self, AName, AParent, Result);
end;

function TCustomParser.FindRelativeName(const AName: WideString): PNameInfo;
var
 P: PPointer; L: Integer;
begin
 Result := FindExactName(AName, FNameParent);
 if Result = nil then
 begin
  P := Pointer(FParentStack);
  L := Length(FParentStack) - 1;
  Inc(P, L);
  while L >= 0 do
  begin
   Result := FindExactName(AName, P^);
   if Result <> nil then Exit;
   Dec(P);
   Dec(L);
  end;
  Result := nil;
 end;
end;

function TCustomParser.GetToken(P: PWideChar; Len: Integer;
            var Info, Expected: TTokenInfo; var AText: WideString;
            var SkipCount: Integer): Integer;

 procedure MatchStr(const ST: TSyntaxToken);
 var L: Integer;
 begin
  with ST do
  begin
   if stID >= 0 then with FSyntax.FLexicalUnits[stID] do
   begin
    L := Len;
    Expected.tiType := luID;
    Expected.tiValue := 0;
    if (luRegEx.MatchString(L, P) = 0) or
      ((L = 0) and (FParserState = psStage1)) then
    begin
     Info.tiType := luID;
     Info.tiValue := luRegEx.LFCounter;
     SkipCount := L;
     SetLength(AText, L);
     Move(P^, Pointer(AText)^, L shl 1);
     Result := E_OK;
     if Assigned(luAction) then
     begin
      Result := luAction(Self, AText, Info, Expected);
      case Result of
       E_TOKEN_EXPECTED: FParserState := psMismatch;
       E_OK: ;
       else FParserState := psError;
      end;
     end;
    end else FParserState := psMismatch;
   end;
  end;
 end; { MatchStr}

var
 STLen: Integer;
 SU: ^TSyntaxUnit;
 SV: ^TSyntaxVarRec;
 STL: ^TSyntaxTokens absolute SV;
 ST: ^TSyntaxToken;
begin
 AText := '';
 Expected.tiType := T_TERMINATE;
 Expected.tiValue := 0;
 Result := E_UNEXPECTED_ERROR;
 SU := NIL;
 SV := NIL;
 ST := NIL;
 STLen := 0;
 if FSyntax <> nil then with FSyntax do
 if (FLexicalUnits <> nil) and (FSyntaxUnits <> nil) then
 begin
  repeat
   case FParserState of
    psTerminal, psError: Exit;
    psSkipBlank:
    begin
     FParserState := psStage1;
     MatchStr(FEmptyPart);
     Exit;
    end;
    psStage1:
    begin
     SU := Addr(FSyntaxUnits[FSyntaxUnitIndex]);
     STLen := Length(SU.suList);
     if STLen = 0 then FSyntaxBlockMatch := True;
     if FSyntaxBlockMatch or (FSyntaxVarIndex >= STLen) then
      FParserState := psUnitPop else
      FParserState := psStage2;
    end;
    psStage2:
    begin
     SV := Addr(SU.suList[FSyntaxVarIndex]);
     STLen := Length(STL^);
     if FSyntaxTokenIndex < STLen then
     begin
      ST := Addr(STL^[FSyntaxTokenIndex]);
      with ST^ do
      if stID >= FSyntaxBaseIndex then
      begin
       SyntaxPush;
       FSyntaxUnitIndex := stID - FSyntaxBaseIndex;
       FSyntaxVarIndex := 0;
       FSyntaxTokenIndex := 0;
       FSyntaxBlockMatch := False;
       FParserState := psStage1;
      end else
      begin
       FParserState := psMatch;
       MatchStr(ST^);
      end;
     end;
    end;
    psMatch:
    begin
     Inc(FSyntaxTokenIndex);
     with ST^ do
     begin
      if (FSyntaxTokenIndex >= STLen) or FSyntaxBlockMatch then
      begin
       with SV^ do
       begin
        if Assigned(svrAction) then
        begin
         Result := svrAction(Self);
         if Result <> E_OK then
         begin
          FParserState := psError;
          Exit;
         end;
        end;
        FSyntaxBlockMatch := True;
       end;
      end;
     end;
     if Info.tiType < FSyntaxBaseIndex then
      FParserState := psSkipBlank else
      FParserState := psStage1;
     Exit;
    end;
    psMismatch:
    begin
     if ST.stAllowMismatch then
     begin
      FSyntaxBlockMatch := True;
      FParserState := psStage1;
     end else if FSyntaxTokenIndex > 0 then
     begin
      Result := E_TOKEN_EXPECTED;
      FParserState := psError;
      Exit;
     end else
     begin
      Inc(FSyntaxVarIndex);
      FSyntaxTokenIndex := 0;
      FParserState := psStage1;
     end;
    end;
    psUnitPop: with SU^ do
    begin
     Expected.tiType := FSyntaxUnitIndex + FSyntaxBaseIndex;
     Expected.tiValue := 0;
     if FSyntaxBlockMatch then
     begin
      Info.tiType := FSyntaxUnitIndex + FSyntaxBaseIndex;
      Info.tiValue := 0;
      Result := E_OK;
      FParserState := psMatch;
      if Assigned(suAction) then
      begin
       suAction(Self, Info, Expected);
       case Result of
        E_TOKEN_EXPECTED: FParserState := psMismatch;
        E_OK: ;
        else
        begin
         FParserState := psError;
         Exit;
        end;
       end;
      end;
     end else FParserState := psMismatch;
     SyntaxPop;
     if FSyntaxUnitIndex >= 0 then
     begin
      SU := Addr(FSyntaxUnits[FSyntaxUnitIndex]);
      STL := Addr(SU.suList[FSyntaxVarIndex]);
      STLen := Length(SV.svrTokens);
      ST := Addr(STL^[FSyntaxTokenIndex]);
     end else if FParserState = psMatch then
                 FParserState := psTerminal;
    end;
   end;
  until False;
 end;
end;

function TCustomParser.IncludeFile(const AFileName: WideString): WideString;
begin
 if Assigned(FOnIncludeFile) then
  FOnIncludeFile(Self, AFileName, Result);
end;

procedure TCustomParser.Initialize;
begin
 FNames := TNamesList.Create;
 FTokenStream := TTokenStream.Create;
end;

procedure TCustomParser.ParentPop;
begin
 FNameParent := FParentStack[FStackIndex];
 Dec(FStackIndex);
end;

procedure TCustomParser.ParentPush;
begin
 Inc(FStackIndex);
 if FStackIndex + 1 = FPSCapacity then
 begin
  Inc(FPSCapacity, 64);
  SetLength(FParentStack, FPSCapacity);
 end;
 FParentStack[FStackIndex] := FNameParent;
 FNameParent := FLastNameInfo;
end;

procedure TCustomParser.SetSyntax(Value: TCustomSyntax);
begin
 if FDestroySyntax then FSyntax.Free;
 FSyntax := Value;
end;

procedure TCustomParser.SyntaxPop;
begin
 Dec(FSSIndex);
 if FSSIndex >= 0 then with FSyntaxStack[FSSIndex] do
 begin
  FSyntaxUnitIndex  := stSyntaxUnitIndex;
  FSyntaxVarIndex   := stSyntaxVarIndex;
  FSyntaxTokenIndex := stSyntaxTokenIndex;
  FSyntaxBlockMatch := stSyntaxBlockMatch;
 end;
end;

procedure TCustomParser.SyntaxPush;
begin
 if FSSIndex >= 0 then with FSyntaxStack[FSSIndex] do
 begin
  stSyntaxUnitIndex  := FSyntaxUnitIndex;
  stSyntaxVarIndex   := FSyntaxVarIndex;
  stSyntaxTokenIndex := FSyntaxTokenIndex;
  stSyntaxBlockMatch := FSyntaxBlockMatch;
 end;
 Inc(FSSIndex);
 if FSSIndex = FSSCapacity then
 begin
  Inc(FSSCapacity, 64);
  SetLength(FSyntaxStack, FSSCapacity);
 end;
end;

{ TTokenStream }

procedure TTokenStream.Initialize;
begin
 NodeClass := TTokenNode;
 FAssignableClass := TTokenStream;
end;

{ TNamesList }

procedure TNamesList.Initialize;
begin
 NodeClass := TNameNode;
 FAssignableClass := TNamesList;
end;

{ TCustomSyntax }

constructor TCustomSyntax.Create;
begin
 FEmptyPart.stID := -1;
 Initialize;
 FSyntaxBaseIndex := Length(FLexicalUnits);
end;

destructor TCustomSyntax.Destroy;
var
 I: Integer;
begin
 if not FRegexFreeNot then
 for I := 0 to Length(FLexicalUnits) - 1 do
  FLexicalUnits[I].luRegEx.Free;
 inherited;
end;

end.
