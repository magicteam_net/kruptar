unit k8_test_form;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SynEdit, SynMemo, Grids, ValEdit, StdCtrls, ExtCtrls, ImgList,
  k8_VirtualMachine, ComCtrls, ToolWin;

type
  TMainForm = class(TForm)
    RegistersList: TValueListEditor;
    CodeMemo: TSynMemo;
    ImageList: TImageList;
    RightPanel: TPanel;
    LeftPanel: TPanel;
    StepButton: TButton;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CodeMemoSpecialLineColors(Sender: TObject; Line: Integer;
      var Special: Boolean; var FG, BG: TColor);
    procedure StepButtonClick(Sender: TObject);
    procedure CodeMemoGutterGetText(Sender: TObject; aLine: Integer;
      var aText: WideString);
  private
    FVirtualMachine: TKruptarVirtualMachine;
    FMark: TSynEditMark;
    FPtrTable: Array of Pointer;
  public
    procedure RefreshRegisters;
  end;

var
  MainForm: TMainForm;

implementation

Uses k8_Disasm;

{$R *.dfm}

function TestProc(A, B, C, D: Integer): Integer; safecall;
begin
 MainForm.Caption := Format('R0: %8.8x R1: %8.8x', [A, B]);
 Result := A + C + D;
end;

procedure TMainForm.FormCreate(Sender: TObject);
Var I, SZ: Integer; P: PByte;
begin
 For I := 0 to 23 do
  RegistersList.InsertRow(Format('R%d', [I]), '00000000', True);
 RegistersList.InsertRow('ESP', '00000000', True);
 RegistersList.InsertRow('EMP', '00000000', True);
 RegistersList.InsertRow('EPP', '00000000', True);
 RegistersList.InsertRow('SP', '00000000', True);
 RegistersList.InsertRow('RMP', '00000000', True);
 RegistersList.InsertRow('RPP', '00000000', True);
 RegistersList.InsertRow('MP', '00000000', True);
 RegistersList.InsertRow('PP', '00000000', True);
 CodeMemo.DoubleBuffered := True;
 FMark := TSynEditMark.Create(CodeMemo);
 FMark.ImageIndex := 0;
 FMark.Visible := True;
 FMark.Line := 1;
 CodeMemo.Marks.Place(FMark);
 FVirtualMachine := TKruptarVirtualMachine.Create;
 With FVirtualMachine, EntryPoint do
 begin
  CodeSize := 64;
  P := Code;
  FRegisters.RMP := NIL;
  FRegisters.RPP := NIL;
  FRegisters.MP := EntryPoint;
  FRegisters.PP := P;
  PCardinal(P)^ := 1 shl 11 + 2 shl 13 + $256 shl 16; //mov r0,$256
  Inc(P, 4);
  PCardinal(P)^ := Cardinal($0D + 1 shl 6 + 0 shl 11 + 2 shl 19); //shl r1,r0,2
  Inc(P, 3);
  PWord(P)^ := Word($12 + 0 shl 6 + 1 shl 11); //add r0,r1
  Inc(P, 2);
  PWord(P)^ := Word($12 + 2 shl 6 + 0 shl 11); //add r2,r0
  Inc(P, 2);
  PWord(P)^ := Word($15 + 1 shl 6 + 0 shl 11); //mul r1,r0
  Inc(P, 2);
  PWord(P)^ := Cardinal(3 shl 6 + 1 shl 11 + 3 shl 13); //mov r3,Addr(FRegisters)
  Inc(P, 2);
  PPointer(P)^ := Addr(FRegisters);
  Inc(P, 4);
  PCardinal(P)^ := Cardinal($2C + 3 shl 6 +  1 shl (8 + 4) +
  1 shl (10 + 4) + 1 shl (12 + 4) + 1 shl (14 + 4)); //ldmia [r3]!,r8,r10,r12,r14
  Inc(P, 3);
  PByte(P)^ := $2E + 2 shl 6;
  Inc(P);
  PInteger(P)^ := 8; //bl +8
  Inc(P, 4);
  PByte(P)^ := $2E + 1 shl 6;
  Inc(P);
  PSmallInt(P)^ := -23; //Cardinal($20 + $FFEE shl 16); //beq r0,r0,-18
  Inc(P, 2);
  PWord(P)^ := 20 shl 6 + 1 shl 11 + 0 shl 13; //mov r20,0
  Inc(P, 2);
  PWord(P)^ := $2F + 1 shl 6 + 0 shl 7 + 4 shl 8 + 20 shl 11;//blmx r20
  Inc(P, 2);
//  PCardinal(P)^ := Cardinal($2F + 1 shl 6 + 1 shl 7 + 2 shl 8 + 20 shl 10 +
//               0 shl 15 + 1 shl 20 + 2 shl 25 + 1 shl 31);//sne r20,r0,r1,2
//  Inc(P, 4);
  PWord(P)^ := $31 + $01 shl 6 + 3 shl 8 + 0 shl 11; //xadd xr3,r0
  Inc(P, 2);
  PCardinal(P)^ := ($32 + $05 shl 6 + 5 shl 9 + 3 shl 14 +
                    1 shl 17 + 0 shl 18); //xsle r5,xr3,xr0
  Inc(P, 3);
{  PWord(P)^ := $2F + 1 shl 6 + 0 shl 8 + 0 shl 11;
  Inc(P, 2);
  PWord(P)^ := $2F + 1 shl 6 + 1 shl 8 + 4 shl 11;
  Inc(P, 2);}
  PCardinal(P)^ := $2F + 0 shl 7 + 1 shl 8 + 1 shl 10 + 1 shl 12 + 1 shl 14;
  Inc(P, 4);
  PCardinal(P)^ := $2F + 1 shl 7 + Cardinal(1 shl 31 + 1 shl 29 +
                                   1 shl 27 + 1 shl 25);
  Inc(P, 4);
  PByte(P)^ := $3F + 1 shl 6; //ret
  Executed := True;
  Paused := True;
  RefreshRegisters;
  P := Code;
  KVM_SpaceChar := #9;
  SetLength(FPtrTable, 16);
  For I := 0 to Length(FPtrTable) - 1 do
  begin
   FPtrTable[I] := P;
   CodeMemo.Lines.Add(KVM_Disasm(Integer(P), P^, SZ));
   Inc(P, SZ);
  end;
  With MainModule.Methods.AddMethod do
  begin
   Initialize(4, True, ccSafeCall);
   ExternalProc := @TestProc;
  // Image1.Canvas.CopyRect();

  end;
 end;
end;

procedure TMainForm.RefreshRegisters;
Var J: Integer;
begin
 With FVirtualMachine.FRegisters do
 For J := 0 to 31 do RegistersList.Cells[2, J + 1] := IntToHex(I[J], 8);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
 FVirtualMachine.Free;
end;

procedure TMainForm.CodeMemoSpecialLineColors(Sender: TObject;
  Line: Integer; var Special: Boolean; var FG, BG: TColor);
begin
 If Line = FMark.Line then
 begin
  Special := True;
  FG := clWhite;
  BG := clNavy;
 end;
end;

procedure TMainForm.StepButtonClick(Sender: TObject);
begin
 With FVirtualMachine do
 begin
  try
   If Proceed <> KVME_OK then ShowMessage('Execution error.');
  except
   ShowMessage(Format('Virtual exception %s raised with message:'#13#10'%s',
               [LastExceptionName, LastExceptionMessage]));
  end;
  RefreshRegisters;
  CodeMemo.Invalidate;
 end;
end;

procedure TMainForm.CodeMemoGutterGetText(Sender: TObject; aLine: Integer;
  var aText: WideString);
Var Ptr: Integer;
begin
 Ptr := Integer(FPtrTable[aLine - 1]);
 aText := IntToHex(Ptr, 8);
 If Ptr = FVirtualMachine.FRegisters.I[31] then
 FMark.Line := aLine;
end;

end.
