unit testtrd;

interface

uses
  Classes;

type
  TTestThread = class(TThread)
  private
    MemoryManaged: Boolean;
  protected
    procedure Execute; override;
    Destructor Destroy; override;
  end;

threadvar
 S: AnsiString;
var
 SS: AnsiString;

function XYGetMem(Size: Integer): Pointer;
function XYFreeMem(P: Pointer): Integer;
function XYReallocMem(P: Pointer; Size: Integer): Pointer;
implementation

uses sysutils, windows;

function XYGetMem(Size: Integer): Pointer;
begin
 Result := SysGetMem(Size);
end;

function XYFreeMem(P: Pointer): Integer;
begin
 Result := SysFreeMem(P);
end;

function XYReallocMem(P: Pointer; Size: Integer): Pointer;
begin
 Result := SysReallocMem(P, Size);
end;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TTestThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TTestThread }

destructor TTestThread.Destroy;
begin
  inherited;
end;

procedure TTestThread.Execute;
begin
 repeat
  S := IntToStr(Random(256));
  SS := S;
 until Terminated;
end;

end.
