unit k8_asmcompiler;

interface

uses
 SysUtils, RegEx, BaseParser;

type
 TK8AsmSyntax = class(TCustomSyntax)
  private
   FRexList: array of TRegExEngine;
  protected
   procedure Initialize; override;
  public
   destructor Destroy; override; 
 end;

 TK8AsmParser = class(TCustomParser)
  protected
   procedure Initialize; override;
   function GetToken(P: PWideChar; Len: Integer;
            var Info, Expected: TTokenInfo; var AText: WideString;
            var SkipCount: Integer): Integer; override;
   function IncludeFile(const AFileName: WideString): WideString; override;
   function FindExternalName(const AName: WideString;
     AParent: PNameInfo): PNameInfo; override;
   procedure AddToken(AType, AValue: Integer); override;
   procedure ParentPush; override;
   procedure ParentPop; override;
 end;

implementation

{ TK8AsmSyntax }

const
 T_NAME = 1;
 T_INTEGER = 2;
 T_FLOAT = 3;
 K8_LEX_CNT = 4;
 K8_SYN_CNT = 1;
 K8_REX_CNT = 4;
 REX_NOTHING = 0;
 REX_NAME    = 1;
 REX_INTEGER = 2;
 REX_FLOAT   = 3;
 K8_REX_LST: array[0..K8_REX_CNT - 1] of WideString =
 ('^([ \t]+|(;|(//).*)+)+',        //REX_NOTHING
  '^[a-zA-Z_]([a-zA-Z_0-9]+)',     //REX_NAME
  '^([0-9]+)|([$]([a-fA-F0-9]+))', //REX_INTEGER
  '^([0-9]+)[.]([0-9]+)');         //REX_FLOAT
 K8_LEX_UNITS: array[0..K8_LEX_CNT - 1] of TLexUnitIdx =
 ((REX: REX_NOTHING;
   ID:  T_NOTHING;
   ACT: nil),
  (REX: REX_NAME;
   ID:  T_NAME;
   ACT: nil),
  (REX: REX_INTEGER;
   ID:  T_INTEGER;
   ACT: nil),
  (REX: REX_FLOAT;
   ID: T_FLOAT;
   ACT: nil));
 K8_KW_BASE = K8_LEX_CNT + K8_SYN_CNT;
 KW_ADD	  = K8_KW_BASE;
 KW_AND	  = K8_KW_BASE + 1;
 KW_BEQ	  = K8_KW_BASE + 2;
 KW_BGTS  = K8_KW_BASE + 3;
 KW_BGT   = K8_KW_BASE + 4;
 KW_BHES  = K8_KW_BASE + 5;
 KW_BHE   = K8_KW_BASE + 6;
 KW_BIC	  = K8_KW_BASE + 7;
 KW_BLES  = K8_KW_BASE + 8;
 KW_BLE   = K8_KW_BASE + 9;
 KW_BLMX  = K8_KW_BASE + 10;
 KW_BLM   = K8_KW_BASE + 11;
 KW_BLTS  = K8_KW_BASE + 12;
 KW_BLT   = K8_KW_BASE + 13;
 KW_BLX	  = K8_KW_BASE + 14;
 KW_BL    = K8_KW_BASE + 15;
 KW_BNE	  = K8_KW_BASE + 16;
 KW_BX	  = K8_KW_BASE + 17;
 KW_B	  = K8_KW_BASE + 18;
 KW_CBD	  = K8_KW_BASE + 19;
 KW_CBQ	  = K8_KW_BASE + 20;
 KW_CDQ	  = K8_KW_BASE + 21;
 KW_CWD	  = K8_KW_BASE + 22;
 KW_CWQ	  = K8_KW_BASE + 23;
 KW_DIVS  = K8_KW_BASE + 24;
 KW_DIV   = K8_KW_BASE + 25;
 KW_FABS  = K8_KW_BASE + 26;
 KW_FADD  = K8_KW_BASE + 27;
 KW_FCDS  = K8_KW_BASE + 28;
 KW_FCQR  = K8_KW_BASE + 29;
 KW_FDIV  = K8_KW_BASE + 30;
 KW_FEXP  = K8_KW_BASE + 31;
 KW_FINT  = K8_KW_BASE + 32;
 KW_FLN	  = K8_KW_BASE + 33;
 KW_FMUL  = K8_KW_BASE + 34;
 KW_FNEG  = K8_KW_BASE + 35;
 KW_FRAC  = K8_KW_BASE + 36;
 KW_FRND  = K8_KW_BASE + 37;
 KW_FRSB  = K8_KW_BASE + 38;
 KW_FSEQ  = K8_KW_BASE + 39;
 KW_FSGT  = K8_KW_BASE + 40;
 KW_FSHE  = K8_KW_BASE + 41;
 KW_FSLE  = K8_KW_BASE + 42;
 KW_FSLT  = K8_KW_BASE + 43;
 KW_FSNE  = K8_KW_BASE + 44;
 KW_FSQRT = K8_KW_BASE + 45;
 KW_FSQR  = K8_KW_BASE + 46;
 KW_FSUB  = K8_KW_BASE + 47;
 KW_FTRN  = K8_KW_BASE + 48;
 KW_LDMIA = K8_KW_BASE + 49;
 KW_LDRB  = K8_KW_BASE + 50;
 KW_LDRH  = K8_KW_BASE + 51;
 KW_LDR   = K8_KW_BASE + 52;
 KW_LDSB  = K8_KW_BASE + 53;
 KW_LDSH  = K8_KW_BASE + 54;
 KW_MOD	  = K8_KW_BASE + 55;
 KW_MOV	  = K8_KW_BASE + 56;
 KW_MUL	  = K8_KW_BASE + 57;
 KW_MVN	  = K8_KW_BASE + 58;
 KW_NOP	  = K8_KW_BASE + 59;
 KW_ORR	  = K8_KW_BASE + 60;
 KW_POP	  = K8_KW_BASE + 61;
 KW_PUSH  = K8_KW_BASE + 62;
 KW_RAISE = K8_KW_BASE + 63;
 KW_RET   = K8_KW_BASE + 64;
 KW_RMD   = K8_KW_BASE + 65;
 KW_RSB   = K8_KW_BASE + 66;
 KW_SAR   = K8_KW_BASE + 67;
 KW_SEQ   = K8_KW_BASE + 68;
 KW_SGTS  = K8_KW_BASE + 69;
 KW_SGT   = K8_KW_BASE + 70;
 KW_SHES  = K8_KW_BASE + 71;
 KW_SHE   = K8_KW_BASE + 72;
 KW_SHL	  = K8_KW_BASE + 73;
 KW_SHR	  = K8_KW_BASE + 74;
 KW_SLES  = K8_KW_BASE + 75;
 KW_SLE   = K8_KW_BASE + 76;
 KW_SLTS  = K8_KW_BASE + 77;
 KW_SLT   = K8_KW_BASE + 78;
 KW_SNE	  = K8_KW_BASE + 79;
 KW_STMIA = K8_KW_BASE + 80;
 KW_STOP  = K8_KW_BASE + 81;
 KW_STRB  = K8_KW_BASE + 82;
 KW_STRH  = K8_KW_BASE + 83;
 KW_STR	  = K8_KW_BASE + 84;
 KW_SUB	  = K8_KW_BASE + 85;
 KW_SWAP  = K8_KW_BASE + 86;
 KW_SWPH  = K8_KW_BASE + 87;
 KW_XADD  = K8_KW_BASE + 88;
 KW_XAND  = K8_KW_BASE + 89;
 KW_XBIC  = K8_KW_BASE + 90;
 KW_XDIV  = K8_KW_BASE + 91;
 KW_XMOD  = K8_KW_BASE + 92;
 KW_XMOV  = K8_KW_BASE + 93;
 KW_XMUL  = K8_KW_BASE + 94;
 KW_XMVN  = K8_KW_BASE + 95;
 KW_XORR  = K8_KW_BASE + 96;
 KW_XOR   = K8_KW_BASE + 97;
 KW_XRDV  = K8_KW_BASE + 98;
 KW_XRSB  = K8_KW_BASE + 99;
 KW_XSAR  = K8_KW_BASE + 100;
 KW_XSEQS = K8_KW_BASE + 101;
 KW_XSEQ  = K8_KW_BASE + 102;
 KW_XSGTS = K8_KW_BASE + 103;
 KW_XSGT  = K8_KW_BASE + 104;
 KW_XSHES = K8_KW_BASE + 105;
 KW_XSHE  = K8_KW_BASE + 106;
 KW_XSHL  = K8_KW_BASE + 107;
 KW_XSHR  = K8_KW_BASE + 108;
 KW_XSLES = K8_KW_BASE + 109;
 KW_XSLE  = K8_KW_BASE + 110;
 KW_XSLTS = K8_KW_BASE + 111;
 KW_XSLT  = K8_KW_BASE + 112;
 KW_XSNES = K8_KW_BASE + 113;
 KW_XSNE  = K8_KW_BASE + 114;
 KW_XSUB  = K8_KW_BASE + 115;
 KW_XXOR  = K8_KW_BASE + 116;
 KW_ESP   = K8_KW_BASE + 117;
 KW_EMP   = K8_KW_BASE + 118;
 KW_EPP   = K8_KW_BASE + 119;
 KW_SP    = K8_KW_BASE + 120;
 KW_RMP   = K8_KW_BASE + 121;
 KW_RPP   = K8_KW_BASE + 122;
 KW_MP    = K8_KW_BASE + 123;
 KW_PP    = K8_KW_BASE + 124;
 KW_HP    = K8_KW_BASE + 125;
 K8_KEY_CNT = 126;
 K8_KEY_WORDS: array[0..K8_KEY_CNT - 1] of TLexUnitMini = (
(REX: 'ADD';	ID: KW_ADD;	ACT: nil),
(REX: 'AND';	ID: KW_AND;	ACT: nil),
(REX: 'BEQ';	ID: KW_BEQ;	ACT: nil),
(REX: 'BGTS';	ID: KW_BGTS;	ACT: nil),
(REX: 'BGT';	ID: KW_BGT;	ACT: nil),
(REX: 'BHES';	ID: KW_BHES;	ACT: nil),
(REX: 'BHE';	ID: KW_BHE;	ACT: nil),
(REX: 'BIC';	ID: KW_BIC;	ACT: nil),
(REX: 'BLES';	ID: KW_BLES;	ACT: nil),
(REX: 'BLE';	ID: KW_BLE;	ACT: nil),
(REX: 'BLMX';	ID: KW_BLMX;	ACT: nil),
(REX: 'BLM';	ID: KW_BLM;	ACT: nil),
(REX: 'BLTS';	ID: KW_BLTS;	ACT: nil),
(REX: 'BLT';	ID: KW_BLT;	ACT: nil),
(REX: 'BLX';	ID: KW_BLX;	ACT: nil),
(REX: 'BL';	ID: KW_BL;	ACT: nil),
(REX: 'BNE';	ID: KW_BNE;	ACT: nil),
(REX: 'BX';	ID: KW_BX;	ACT: nil),
(REX: 'B';	ID: KW_B;	ACT: nil),
(REX: 'CBD';	ID: KW_CBD;	ACT: nil),
(REX: 'CBQ';	ID: KW_CBQ;	ACT: nil),
(REX: 'CDQ';	ID: KW_CDQ;	ACT: nil),
(REX: 'CWD';	ID: KW_CWD;	ACT: nil),
(REX: 'CWQ';	ID: KW_CWQ;	ACT: nil),
(REX: 'DIVS';	ID: KW_DIVS;	ACT: nil),
(REX: 'DIV';	ID: KW_DIV;	ACT: nil),
(REX: 'FABS';	ID: KW_FABS;	ACT: nil),
(REX: 'FADD';	ID: KW_FADD;	ACT: nil),
(REX: 'FCDS';	ID: KW_FCDS;	ACT: nil),
(REX: 'FCQR';	ID: KW_FCQR;	ACT: nil),
(REX: 'FDIV';	ID: KW_FDIV;	ACT: nil),
(REX: 'FEXP';	ID: KW_FEXP;	ACT: nil),
(REX: 'FINT';	ID: KW_FINT;	ACT: nil),
(REX: 'FLN';	ID: KW_FLN;	ACT: nil),
(REX: 'FMUL';	ID: KW_FMUL;	ACT: nil),
(REX: 'FNEG';	ID: KW_FNEG;	ACT: nil),
(REX: 'FRAC';	ID: KW_FRAC;	ACT: nil),
(REX: 'FRND';	ID: KW_FRND;	ACT: nil),
(REX: 'FRSB';	ID: KW_FRSB;	ACT: nil),
(REX: 'FSEQ';	ID: KW_FSEQ;	ACT: nil),
(REX: 'FSGT';	ID: KW_FSGT;	ACT: nil),
(REX: 'FSHE';	ID: KW_FSHE;	ACT: nil),
(REX: 'FSLE';	ID: KW_FSLE;	ACT: nil),
(REX: 'FSLT';	ID: KW_FSLT;	ACT: nil),
(REX: 'FSNE';	ID: KW_FSNE;	ACT: nil),
(REX: 'FSQRT';	ID: KW_FSQRT;	ACT: nil),
(REX: 'FSQR';	ID: KW_FSQR;	ACT: nil),
(REX: 'FSUB';	ID: KW_FSUB;	ACT: nil),
(REX: 'FTRN';	ID: KW_FTRN;	ACT: nil),
(REX: 'LDMIA';	ID: KW_LDMIA;	ACT: nil),
(REX: 'LDRB';	ID: KW_LDRB;	ACT: nil),
(REX: 'LDRH';	ID: KW_LDRH;	ACT: nil),
(REX: 'LDR';	ID: KW_LDR;	ACT: nil),
(REX: 'LDSB';	ID: KW_LDSB;	ACT: nil),
(REX: 'LDSH';	ID: KW_LDSH;	ACT: nil),
(REX: 'MOD';	ID: KW_MOD;	ACT: nil),
(REX: 'MOV';	ID: KW_MOV;	ACT: nil),
(REX: 'MUL';	ID: KW_MUL;	ACT: nil),
(REX: 'MVN';	ID: KW_MVN;	ACT: nil),
(REX: 'NOP';	ID: KW_NOP;	ACT: nil),
(REX: 'ORR';	ID: KW_ORR;	ACT: nil),
(REX: 'POP';	ID: KW_POP;	ACT: nil),
(REX: 'PUSH';	ID: KW_PUSH;	ACT: nil),
(REX: 'RAISE';	ID: KW_RAISE;	ACT: nil),
(REX: 'RET';	ID: KW_RET;	ACT: nil),
(REX: 'RMD';	ID: KW_RMD;	ACT: nil),
(REX: 'RSB';	ID: KW_RSB;	ACT: nil),
(REX: 'SAR';	ID: KW_SAR;	ACT: nil),
(REX: 'SEQ';	ID: KW_SEQ;	ACT: nil),
(REX: 'SGTS';	ID: KW_SGTS;	ACT: nil),
(REX: 'SGT';	ID: KW_SGT;	ACT: nil),
(REX: 'SHES';	ID: KW_SHES;	ACT: nil),
(REX: 'SHE';	ID: KW_SHE;	ACT: nil),
(REX: 'SHL';	ID: KW_SHL;	ACT: nil),
(REX: 'SHR';	ID: KW_SHR;	ACT: nil),
(REX: 'SLES';	ID: KW_SLES;	ACT: nil),
(REX: 'SLE';	ID: KW_SLE;	ACT: nil),
(REX: 'SLTS';	ID: KW_SLTS;	ACT: nil),
(REX: 'SLT';	ID: KW_SLT;	ACT: nil),
(REX: 'SNE';	ID: KW_SNE;	ACT: nil),
(REX: 'STMIA';	ID: KW_STMIA;	ACT: nil),
(REX: 'STOP';	ID: KW_STOP;	ACT: nil),
(REX: 'STRB';	ID: KW_STRB;	ACT: nil),
(REX: 'STRH';	ID: KW_STRH;	ACT: nil),
(REX: 'STR';	ID: KW_STR;	ACT: nil),
(REX: 'SUB';	ID: KW_SUB;	ACT: nil),
(REX: 'SWAP';	ID: KW_SWAP;	ACT: nil),
(REX: 'SWPH';	ID: KW_SWPH;	ACT: nil),
(REX: 'XADD';	ID: KW_XADD;	ACT: nil),
(REX: 'XAND';	ID: KW_XAND;	ACT: nil),
(REX: 'XBIC';	ID: KW_XBIC;	ACT: nil),
(REX: 'XDIV';	ID: KW_XDIV;	ACT: nil),
(REX: 'XMOD';	ID: KW_XMOD;	ACT: nil),
(REX: 'XMOV';	ID: KW_XMOV;	ACT: nil),
(REX: 'XMUL';	ID: KW_XMUL;	ACT: nil),
(REX: 'XMVN';	ID: KW_XMVN;	ACT: nil),
(REX: 'XORR';	ID: KW_XORR;	ACT: nil),
(REX: 'XOR';	ID: KW_XOR;	ACT: nil),
(REX: 'XRDV';	ID: KW_XRDV;	ACT: nil),
(REX: 'XRSB';	ID: KW_XRSB;	ACT: nil),
(REX: 'XSAR';	ID: KW_XSAR;	ACT: nil),
(REX: 'XSEQS';	ID: KW_XSEQS;	ACT: nil),
(REX: 'XSEQ';	ID: KW_XSEQ;	ACT: nil),
(REX: 'XSGTS';	ID: KW_XSGTS;	ACT: nil),
(REX: 'XSGT';	ID: KW_XSGT;	ACT: nil),
(REX: 'XSHES';	ID: KW_XSHES;	ACT: nil),
(REX: 'XSHE';	ID: KW_XSHE;	ACT: nil),
(REX: 'XSHL';	ID: KW_XSHL;	ACT: nil),
(REX: 'XSHR';	ID: KW_XSHR;	ACT: nil),
(REX: 'XSLES';	ID: KW_XSLES;	ACT: nil),
(REX: 'XSLE';	ID: KW_XSLE;	ACT: nil),
(REX: 'XSLTS';	ID: KW_XSLTS;	ACT: nil),
(REX: 'XSLT';	ID: KW_XSLT;	ACT: nil),
(REX: 'XSNES';	ID: KW_XSNES;	ACT: nil),
(REX: 'XSNE';	ID: KW_XSNE;	ACT: nil),
(REX: 'XSUB';	ID: KW_XSUB;	ACT: nil),
(REX: 'XXOR';	ID: KW_XXOR;	ACT: nil),
(REX: 'ESP';	ID: KW_ESP;	ACT: nil),
(REX: 'EMP';	ID: KW_EMP;	ACT: nil),
(REX: 'EPP';	ID: KW_EPP;	ACT: nil),
(REX: 'SP';	ID: KW_SP;	ACT: nil),
(REX: 'RMP';	ID: KW_RMP;	ACT: nil),
(REX: 'RPP';	ID: KW_RPP;	ACT: nil),
(REX: 'MP';	ID: KW_MP;	ACT: nil),
(REX: 'PP';	ID: KW_PP;	ACT: nil),
(REX: 'HP';	ID: KW_HP;	ACT: nil));

destructor TK8AsmSyntax.Destroy;
var
 I: Integer;
begin
 for I := 0 to Length(FRexList) - 1 do FRexList[I].Free;
 inherited;
end;

procedure TK8AsmSyntax.Initialize;
var
 I: Integer;
begin
 FRegExFreeNot := True;
 SetLength(FRexList, K8_REX_CNT);
 SetLength(FLexicalUnits, K8_LEX_CNT);
 SetLength(FSyntaxUnits, K8_SYN_CNT);
 for I := 0 to K8_REX_CNT - 1 do
  FRexList[I] := TRegExEngine.Create(K8_REX_LST[I]); 
 for I := 0 to K8_LEX_CNT - 1 do
 with K8_LEX_UNITS[I], FLexicalUnits[I] do
 begin
  luRegEx := FRexList[I];
  luID := ID;
  if @ACT <> nil then
  begin
   @luActFnc := @ACT;
   luActObj := Self;
  end;
 end;
{ for I := 0 to K8_KEY_CNT - 1 do
 with K8_KEY_WORDS[I], FLexicalUnits[I + K8_LEX_CNT] do
 begin
  luRegEx := TRegExEngine.Create(REX);
  luID := ID;
  if @ACT <> nil then
  begin
   @luActFnc := @ACT;
   luActObj := Self;
  end;
 end;                       }
end;

{ TK8AsmParser }

procedure TK8AsmParser.AddToken(AType, AValue: Integer);
begin
  inherited;
//
end;

function TK8AsmParser.FindExternalName(const AName: WideString;
  AParent: PNameInfo): PNameInfo;
begin
//
end;

function TK8AsmParser.GetToken(P: PWideChar; Len: Integer; var Info,
  Expected: TTokenInfo; var AText: WideString;
  var SkipCount: Integer): Integer;
begin
//
end;

function TK8AsmParser.IncludeFile(const AFileName: WideString): WideString;
begin
 //
end;

procedure TK8AsmParser.Initialize;
begin
 inherited Initialize;
 if Syntax = nil then
 begin
  Syntax := TK8AsmSyntax.Create;
  DestroySyntax := True;
 end; 
end;

procedure TK8AsmParser.ParentPop;
begin
  inherited;
 //
end;

procedure TK8AsmParser.ParentPush;
begin
  inherited;
  //
end;

end.
