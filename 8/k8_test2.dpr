program k8_test2;

uses
  Forms,
  SysUtils,
  k8_test_form in 'k8_test_form.pas' {MainForm},
  k8_Disasm in 'k8_Disasm.pas',
  k8_VirtualMachine in 'k8_VirtualMachine.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
