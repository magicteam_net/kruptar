unit kuScript;

interface

Uses
 SysUtils, Classes, kusCode, kuClasses, kuSystem;

Type
 TTypeMode = (tmRange, tmBase, tmArray, tmRecord, tmPointer);
 TRangeMode = (rmByte, rmShortInt, rmWord, rmSmallInt, rmCardinal, rmInteger);
 TKUSType = Class;
 TKUSVariable = Class;
 TTypeRec = Record
  FMode: TTypeMode;
  Case TTypeMode of
   tmRange: (FRangeMode: TRangeMode);
   tmBase:  (FBaseType: TKUSType);
   tmArray: (FType: TKUSType;
             FCount: Integer);
   tmRecord: (FFields: TNodeList);
   tmPointer: (FPType: TKUSType)
 end;
 TKUSType = Class
 public
  FName: String;
  FTypeRec: TTypeRec;
  Constructor Create(const Name: String; Const TypeRec: TTypeRec);
  Destructor Destroy; override;
  Function CheckType: TTypeMode;
 end;
 TKUSVariable = Class
 private
  FName: String;
  FPointer: Boolean;
  FType: TKUSType;
 public
  Constructor Create(const Name: String; KType: TKUSType; Ptr: Boolean = False);
  Destructor Destroy; override;
 end;
 TKUSConstant = Class
 private
  FName: String;
  FDescription: String;
 public
  Constructor Create(const Name, Desc: String);
  Destructor Destroy; override;
 end;
 TKUSFunction = Class
 public
  FName: String;
  FFunction: Boolean;
  FMethodType: TMethodType;
  FCallingConvention: TCallingConvention;
  FProcAddress: Pointer;
  FResultType: TKUSType;
  FArguments: TNodeList;
  Constructor Create(Const Name: String);
  Destructor Destroy; override;
 end;
 TKUScript = Class;
 TScriptModule = Class
 private
  FOwner: TKUScript;
 public
  FUses: TNodeListEx;
  FName: String;
  FVariables: TNodeList;
  FTypes: TNodeList;
  FConstants: TNodeList;
  FFunctions: TNodeList;
  Constructor Create;
  Destructor Destroy; override;
  Function FindVar(const Name: String): TKUSVariable;
  Function FindType(const Name: String): TKUSType;
  Function FindConst(const Name: String): TKUSConstant;
  Function FindProc(const Name: String): TKUSFunction;
  Function GlobalFindVar(const Name: String): TKUSVariable;
  Function GlobalFindType(const Name: String): TKUSType;
  Function GlobalFindConst(const Name: String): TKUSConstant;
  Function GlobalFindProc(const Name: String): TKUSFunction;
  Function FindName(const Name: String): Boolean;
  Function ParseConstant(Var P: PChar;
       Var A, B: Integer; EndChar: Char = ';'): String;
  Function FindModule(const Name: String): TScriptModule;
 end;
 TKUScript = Class
 public
  FScriptModules: TNodeList;
  Function GetByName(Name: String): TScriptModule;
  Constructor Create;
  Procedure Clear;
  Destructor Destroy; override;
  Function Parse(Const FileName: String): Integer;
  Procedure Compile(Const FileName: String; Const KUSProgram: TKUSProgram);
  Function AddSystemFunction(Const ModuleName, Description: String; Const ProcAddress: Pointer): TKUSFunction;
  Property Modules[Name: String]: TScriptModule read GetByName;
  Function ParseType(const ModuleName: String; Var P: PChar; Var A, B: Integer): TKUSType;
  Function AddModule(const Name: String): TScriptModule;
 end;

Function GetToken(Var P: PChar; Var Line, Cursor: Integer): String;
Function CheckIdentName(const Name: String): Boolean;

Var LastToken: String;

implementation

Function TKUScript.GetByName(Name: String): TScriptModule;
Var N: PNodeRec; Module: TScriptModule;
begin
 Result := NIL;
 If FScriptModules <> NIL then With FScriptModules do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   Module := Node as TScriptModule;
   If (Module <> NIL) and (Module.FName = Name) then
   begin
    Result := Module;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

Function TKUScript.AddSystemFunction(Const ModuleName, Description: String; Const ProcAddress: Pointer): TKUSFunction;
Var
 Module, sMod: TScriptModule; P: PChar; A, B: Integer; Args: TNodeList;
 CC: TCallingConvention; Ptr, Func: Boolean; S, Name, Vname: String;
 TT: TKUSType; Node: TKUSFunction;// N: TNode;
Label ExitFree;
begin
 Result := NIL; If Description = '' then Exit;
 If FScriptModules <> NIL then With FScriptModules do
 begin
  Module := Modules[ModuleName];
  If Module <> NIL then With Module do
  begin
   P := Addr(Description[1]);
   S := GetToken(P, A, B);
   If S <> 'EXTERNAL' then Exit;
   S := GetToken(P, A, B);
   If S = 'FUNC' then Func := True Else
   If S = 'PROC' then Func := False Else Exit;
   S := GetToken(P, A, B);
   If S = '(' then
   begin
    S := GetToken(P, A, B);
    If S = 'STDCALL' then CC := ccStdCall Else
    If S = 'REGISTER' then CC := ccRegister Else
    If S = 'PASCAL' then CC := ccPascal Else
    If S = 'CDECL' then CC := ccCDecl Else Exit;
    S := GetToken(P, A, B);
    If S <> ')' then Exit;
    S := GetToken(P, A, B);    
   end Else CC := ccRegister;
   If not CheckIdentName(S) or FindName(S) then Exit;
   Name := S;
   S := GetToken(P, A, B);
   Args := TNodeList.Create;
   If S = '(' then
   begin
    Repeat
     S := GetToken(P, A, B);
     If S = ')' then Break;
     If S = '^' then
     begin
      Ptr := True;
      S := GetToken(P, A, B);
     end Else Ptr := False;
     If not CheckIdentName(S) then Goto ExitFree;
     sMod := FindModule(S);
     If sMod <> NIL then
     begin
      S := GetToken(P, A, B);
      If S <> '.' then Goto ExitFree;
      S := GetToken(P, A, B);
      TT := sMod.FindType(S)
     end Else TT := GlobalFindType(S);
     If (TT <> NIL) and (Ptr or (TT.CheckType in [tmRange, tmPointer])) then
     Repeat
      S := GetToken(P, A, B);
      If not CheckIdentName(S) then Goto ExitFree;
      VName := S;
      S := GetToken(P, A, B);
      Args.Add^.Node := TKusVariable.Create(VName, TT, Ptr);
      If (S = ';') or (S = ')') then Break;
      If S <> ',' then Goto ExitFree;
     Until False Else
     begin ExitFree:
      Args.Free;
      Exit;
     end;
     If S = ')' then Break;
    Until False;
    S := GetToken(P, A, B);
   end;
   If S = ':' then
   begin
    If not Func then Goto ExitFree;
    S := GetToken(P, A, B);
    If not CheckIdentName(S) then Exit;
    sMod := FindModule(S);
    If sMod <> NIL then
    begin
     S := GetToken(P, A, B);
     If S <> '.' then Goto ExitFree;
     S := GetToken(P, A, B);
     TT := sMod.FindType(S)
    end Else TT := GlobalFindType(S);
    If (TT = NIL) or not (TT.CheckType in [tmRange, tmPointer]) then
     Goto ExitFree;
    S := GetToken(P, A, B); 
   end Else Goto ExitFree;
   If S <> ';' then Goto ExitFree;   
   Node := TKUSFunction.Create(Name);
   Result := Node;
   With Result do
   begin
    FFunction := Func;
    FMethodType := mtExternal;
    FCallingConvention := CC;
    FProcAddress := ProcAddress;
    FResultType := TT;
    FArguments := Args;
   end;
   FFunctions.Add^.Node := Node;
  end;
 end;
end;

Constructor TKUScript.Create;
begin
 FScriptModules := TNodeList.Create;
end;

Destructor TKUScript.Destroy;
begin
 FScriptModules.Free;
 inherited Destroy;
end;

Function GetToken(Var P: PChar; Var Line, Cursor: Integer): String;
begin
 Result := '';
 If P = NIL then Exit;
 Case P^ of
  #0: Exit;
  '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':':
  begin
   Inc(Cursor);
   Result := P^;
   Inc(P);
  end;
  Else
  begin
   If P = #13 then Inc(P);
   If P = #10 then
   begin
    Inc(Line);
    Inc(P);
    Cursor := 0;
   end;
   While P^ in [' ', #9, #13, #10] do
   begin
    Inc(Cursor);
    Inc(P);
   end;
   If P^ in ['(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'] then
   begin
    Inc(Cursor);
    Result := P^;
    Inc(P);
   end Else If P^ = '''' then
   begin
    Repeat
     Result := Result + P^;
     Inc(Cursor);
     Inc(P);
    Until P^ in [#0..#32, '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'];
   end Else If P^ <> #0 then
   begin
    Repeat
     Result := Result + UpCase(P^);
     Inc(Cursor);
     Inc(P);
    Until P^ in [#0..#32, '(', ')', '{', '}', '=', '+', '-', '*', ',', ';', '^', '@', '.', ':'];
   end;
  end;
 end;
 LastToken := Result;
end;

Function TKUScript.Parse(Const FileName: String): Integer;
Var List: TStringList; S: String; CChar: PChar; Line, Cursor: Integer; Token: String;
begin
 Result := 0;
 List := TStringList.Create;
 List.LoadFromFile(FileName);
 S := List.Text + #0;
 If Length(S) > 0 then
 begin
  CChar := Addr(S[1]);
  Line := 0;
  Cursor := 0;
  Repeat
   Token := GetToken(CChar, Line, Cursor);
   If Token <> '' then
   begin
   end Else Break;
  Until False;
 end;
 List.Free;
end;

Procedure TKUScript.Compile(Const FileName: String; Const KUSProgram: TKUSProgram);
begin
 Parse(FileName);
end;

function TScriptModule.FindName(const Name: String): Boolean;
Var N: TNode;
begin
 Result := True;
 N := FindProc(Name);
 If N <> NIL then Exit;
 N := FindVar(Name);
 If N <> NIL then Exit;
 N := FindConst(Name);
 If N <> NIL then Exit;
 N := FindType(Name);
 If N <> NIL then Exit;
 N := FindModule(Name);
 If N <> NIL then Exit;
 Result := False;
end;

Constructor TScriptModule.Create;
begin
 FVariables := TNodeList.Create;
 FTypes := TNodeList.Create;
 FConstants := TNodeList.Create;
 FFunctions := TNodeList.Create;
 FUses := TNodeListEx.Create;
end;

Destructor TScriptModule.Destroy;
begin
 FName := '';
 FUses.Free;
 FVariables.Free;
 FTypes.Free;
 FConstants.Free;
 FFunctions.Free;
end;

Procedure TKUScript.Clear;
begin
end;

Constructor TKUSFunction.Create(Const Name: String);
begin
 FName := Name;
end;

Destructor TKUSFunction.Destroy;
begin
 FArguments.Free;
 FName := '';
end;

constructor TKUSVariable.Create(const Name: String; KType: TKUSType; Ptr: Boolean = False);
begin
 FName := Name;
 FPointer := Ptr;
 FType := KType;
end;

destructor TKUSVariable.Destroy;
begin
 FName := '';
 FType := NIL;
 inherited Destroy;
end;

constructor TKUSConstant.Create(const Name, Desc: String);
begin
 FName := Name;
 FDescription := Desc;
end;

destructor TKUSConstant.Destroy;
begin
 FName := '';
 FDescription := '';
 inherited Destroy;
end;

function TKUSType.CheckType: TTypeMode;
begin
 With FTypeRec do
 begin
  Case FMode of
   tmBase:
   If FBaseType <> NIL then
    Result := FBaseType.CheckType Else
    Result := tmBase;
   Else Result := FMode;
  end; 
 end;
end;

constructor TKUSType.Create(const Name: String; Const TypeRec: TTypeRec);
begin
 FName := Name;
 FTypeRec := TypeRec;
end;

destructor TKUSType.Destroy;
begin
 FName := '';
 With FTypeRec do If FMode = tmRecord then FFields.Free;
 inherited Destroy;
end;

function TScriptModule.FindConst(const Name: String): TKUSConstant;
Var N: PNodeRec; M: TKUSConstant;
begin
 Result := NIL;
 If FConstants <> NIL then With FConstants do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   M := Node as TKUSConstant;
   If (M <> NIL) and (M.FName = Name) then
   begin
    Result := M;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

function TScriptModule.FindProc(const Name: String): TKUSFunction;
Var N: PNodeRec; M: TKUSFunction;
begin
 Result := NIL;
 If FFunctions <> NIL then With FFunctions do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   M := Node as TKUSFunction;
   If (M <> NIL) and (M.FName = Name) then
   begin
    Result := M;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

function TScriptModule.FindType(const Name: String): TKUSType;
Var N: PNodeRec; M: TKUSType;
begin
 Result := NIL;
 If FTypes <> NIL then With FTypes do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   M := Node as TKUSType;
   If (M <> NIL) and (M.FName = Name) then
   begin
    Result := M;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

function TScriptModule.FindVar(const Name: String): TKUSVariable;
Var N: PNodeRec; M: TKUSVariable;
begin
 Result := NIL;
 If FVariables <> NIL then With FVariables do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   M := Node as TKUSVariable;
   If (M <> NIL) and (M.FName = Name) then
   begin
    Result := M;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

function CheckIdentName(const Name: String): Boolean;
Var D: PChar; A: Integer;
begin
 Result := False;
 If (Length(Name) <= 0) or not (Name[1] in ['A'..'Z', '_']) then Exit;
 If Length(Name) > 1 then
 begin
  D := Addr(Name[2]);
  For A := 2 to Length(Name) do
  begin
   If not (D^ in ['A'..'Z', '_', '0'..'9']) then Exit;
   Inc(D);
  end;
 end;
 Result := True; 
end;

function TKUScript.ParseType(const ModuleName: String; Var P: PChar; Var A, B: Integer): TKUSType;
Var
 Module, sMod: TScriptModule; I: Integer; Args: TNodeListEx;
 S, Name, Vname: String; Ptr: Boolean;
 TT: TKUSType; TR: TTypeRec;
Label ExitFree, ErrExit;
begin
 Result := NIL; If P = NIL then Exit;
 If FScriptModules <> NIL then With FScriptModules do
 begin
  Module := Modules[ModuleName];
  If Module <> NIL then With Module do
  begin
   S := GetToken(P, A, B);
   If not CheckIdentName(S) or FindName(S) then Exit;
   Name := S;
   S := GetToken(P, A, B);
   VName := '';
   If S = '(' then
   begin
    S := GetToken(P, A, B);
    If not CheckIdentName(S) or FindName(S) then Exit;
    VName := S;
    S := GetToken(P, A, B);
    If S <> ')' then Exit;
   end;
   If S <> '=' then Exit;
   S := GetToken(P, A, B);
   If S = '^' then
   begin
    S := GetToken(P, A, B);
    If not CheckIdentName(S) then Exit;
    sMod := FindModule(S);
    If sMod <> NIL then
    begin
     S := GetToken(P, A, B);
     If S <> '.' then Exit;
     S := GetToken(P, A, B);
     TT := sMod.FindType(S)
    end Else TT := GlobalFindType(S);
    S := GetToken(P, A, B);
    If S <> ';' then Exit;
    If TT <> NIL then
    begin
     TR.FMode := tmPointer;
     TR.FPType := TT;
     Result := TKUSType.Create(Name, TR);
     FTypes.Add^.Node := Result;
     If VName <> '' then
     begin
      TR.FMode := tmPointer;
      TR.FPType := Result;
      FTypes.Add^.Node := TKUSType.Create(VName, TR);
     end;
    end Else Exit;
   end Else If S = 'ARRAY' then
   begin
    TR.FMode := tmArray;
    S := GetToken(P, A, B);
    If S = '[' then
    begin
     S := ParseConstant(P, A, B, ']');
     If LastToken <> ']' then Exit;
     If IntCheckOut(S) then
     begin
      TR.FCount := GetInteger(S);
      If TR.FCount < 1 then Exit;
     end Else Exit;
    end Else TR.FCount := 0;
    S := GetToken(P, A, B);
    If S <> 'OF' then Exit;
    S := GetToken(P, A, B);
    If not CheckIdentName(S) then Exit;
    sMod := FindModule(S);
    If sMod <> NIL then
    begin
     S := GetToken(P, A, B);
     If S <> '.' then Exit;
     S := GetToken(P, A, B);
     TT := sMod.FindType(S)
    end Else TT := GlobalFindType(S);
    If TT = NIL then Exit;
    TR.FPType := TT;
    Result := TKUSType.Create(Name, TR);
    FTypes.Add^.Node := Result;
    If VName <> '' then
    begin
     TR.FMode := tmPointer;
     TR.FPType := Result;
     FTypes.Add^.Node := TKUSType.Create(VName, TR);
    end;
   end Else If S = 'RECORD' then
   begin
    TR.FMode := tmRecord;
    TR.FFields := TNodeList.Create;
    Result := TKUSType.Create(Name, TR);
    FTypes.Add^.Node := Result;
    If VName <> '' then
    begin
     TR.FMode := tmPointer;
     TR.FPType := Result;
     FTypes.Add^.Node := TKUSType.Create(VName, TR);
    end;
    Repeat
     S := GetToken(P, A, B);
     If S = 'END' then
     begin
      S := GetToken(P, A, B);
      If S <> ';' then Goto ErrExit;
      Break;
     end;
     If S = '^' then
     begin
      Ptr := True;
      S := GetToken(P, A, B);
     end Else Ptr := False;
     If not CheckIdentName(S) then Goto ErrExit;
     sMod := FindModule(S);
     If sMod <> NIL then
     begin
      S := GetToken(P, A, B);
      If S <> '.' then Goto ErrExit;
      S := GetToken(P, A, B);
      TT := sMod.FindType(S)
     end Else TT := GlobalFindType(S);
     If (TT <> NIL) then
     Repeat
      S := GetToken(P, A, B);
      If not CheckIdentName(S) then Goto ErrExit;
      VName := S;
      S := GetToken(P, A, B);
      TR.FFields.Add^.Node := TKusVariable.Create(VName, TT, Ptr);
      If S = ';' then Break;
      If S = 'END' then
      begin
       S := GetToken(P, A, B);
       If S <> ';' then Goto ErrExit;
       TR.FFields.Unite;
       Exit;
      end;
      If S <> ',' then Goto ErrExit;
     Until False Else
     begin
     ErrExit:
      Result := NIL;
      TR.FFields.Clear;
      Exit;
     end;
    Until False;
   end Else If S = '(' then // Enum
   begin
    TR.FMode := tmRange;
    TR.FRangeMode := rmByte;
    Args := TNodeListEx.Create;
    I := 0;
    Repeat
     S := GetToken(P, A, B);
     If not CheckIdentName(S) or FindName(S) then
     begin
     ExitFree:
      (Args as TNodeList).Free;
      Exit;
     end;
     Args.Add^.Node := TKUSConstant.Create(S, IntToStr(I));
     Inc(I);
     S := GetToken(P, A, B);
     If S = ')' then Break;
     If S <> ',' then Goto ExitFree;
    Until False;
    S := GetToken(P, A, B);
    If S <> ';' then Goto ErrExit;
    Args.Unite;
    With Args do For I := 0 to Count - 1 do
     FConstants.Add^.Node := FObjects[I];
    Args.Free;
    Result := TKUSType.Create(Name, TR);
    FTypes.Add^.Node := Result;
    If VName <> '' then
    begin
     TR.FMode := tmPointer;
     TR.FPType := Result;
     FTypes.Add^.Node := TKUSType.Create(VName, TR);
    end;
   end;
  end;
 end;
end;

function TKUScript.AddModule(const Name: String): TScriptModule;
begin
 Result := TScriptModule.Create;
 Result.FOwner := Self;
 Result.FName := Name;
 FScriptModules.Add^.Node := Result;
end;

Function TScriptModule.GlobalFindVar(const Name: String): TKUSVariable;
Var N: PNodeRec;
begin
 Result := FindVar(Name);
 If Result = NIL then With FUses do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   If Node <> NIL then Result := (Node as TScriptModule).FindVar(Name);
   If Result <> NIL then Exit;
   N := Next;
  end;
 end;
end;

Function TScriptModule.GlobalFindType(const Name: String): TKUSType;
Var N: PNodeRec;
begin
 Result := FindType(Name);
 If Result = NIL then With FUses do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   If Node <> NIL then Result := (Node as TScriptModule).FindType(Name);
   If Result <> NIL then Exit;
   N := Next;
  end;
 end;
end;

Function TScriptModule.GlobalFindConst(const Name: String): TKUSConstant;
Var N: PNodeRec;
begin
 Result := FindConst(Name);
 If Result = NIL then With FUses do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   If Node <> NIL then
    Result := (Node as TScriptModule).FindConst(Name);
   If Result <> NIL then Exit;
   N := Next;
  end;
 end;
end;

Function TScriptModule.GlobalFindProc(const Name: String): TKUSFunction;
Var N: PNodeRec;
begin
 Result := FindProc(Name);
 If Result = NIL then With FUses do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   If Node <> NIL then Result := (Node as TScriptModule).FindProc(Name);
   If Result <> NIL then Exit;
   N := Next;
  end;
 end;
end;

Type
 TModState = (stNone, stNeg, stNot);
 TMathState = (msNone, msAdd, msSub, msMul, msDiv, msMod, msShl, msShr, msAnd, msOr, msXor, msClose);


function TScriptModule.ParseConstant(Var P: PChar;
       Var A, B: Integer; EndChar: Char = ';'): String;
Var S: String; I: Integer; State: TModState; ModState: TMathState;
Procedure Modify(D: Integer);
begin
 Case State of
  stNeg: D := -D;
  stNot: D := not D;
 end;
 Case ModState of
  msNone: I := D;
  msAdd: Inc(I, D);
  msSub: Dec(I, D);
  msMul: I := I * D;
  msDiv: If D <> 0 then I := I div D Else ModState := msClose;
  msMod: I := I mod D;
  msShl: I := I shl D;
  msShr: I := I shr D;
  msAnd: I := I and D;
  msOr: I := I or D;
  msXor: I := I xor D;
 end;
 S := GetToken(P, A, B);
 If S = '+' then ModState := msAdd Else
 If S = '-' then ModState := msSub Else
 If S = '*' then ModState := msMul Else
 If S = 'DIV' then ModState := msDiv Else
 If S = 'MOD' then ModState := msMod Else
 If S = 'SHL' then ModState := msShl Else
 If S = 'SHR' then ModState := msShr Else
 If S = 'AND' then ModState := msAnd Else
 If S = 'OR' then ModState := msOr Else
 If S = 'XOR' then ModState := msXor;
end;
Label ErrorExit;
Var sMod: TScriptModule; PP: PChar; CC: TKUSConstant; AA, BB: Integer;
begin
 I := 0;
 ModState := msNone;
 Repeat
  S := GetToken(P, A, B);
  If S = '-' then
  begin
   State := stNeg;
   S := GetToken(P, A, B);
  end Else
  If S = 'NOT' then
  begin
   State := stNot;
   S := GetToken(P, A, B);
  end Else State := stNone;
  If S = '(' then
  begin
   S := ParseConstant(P, A, B, ')');
   If not IntCheckOut(S) then Goto ErrorExit;
   If LastToken <> ')' then
   begin
   ErrorExit:
    Result := '';
    Exit;
   end;
   Modify(GetInteger(S));
   If ModState = msClose then Goto ErrorExit;
   If S = EndChar then
   begin
    Result := IntToStr(I);
    Exit;
   end;
  end Else
  begin
   If S = '' then Goto ErrorExit;
   If UnsCheckOut(S) then
   begin
    Modify(GetInteger(S));
    If ModState = msClose then Goto ErrorExit;
    If S = EndChar then
    begin
     Result := IntToStr(I);
     Exit;
    end;
   end Else
   begin
    If not CheckIdentName(S) then Goto ErrorExit;
    sMod := FindModule(S);
    If sMod <> NIL then
    begin
     S := GetToken(P, A, B);
     If S <> '.' then Goto ErrorExit;
     S := GetToken(P, A, B);
     CC := sMod.FindConst(S)
    end Else CC := GlobalFindConst(S);
    If CC = NIL then Goto ErrorExit;
    If Length(CC.FDescription) <= 0 then Goto ErrorExit;
    PP := Addr(CC.FDescription[1]);
    S := ParseConstant(PP, AA, BB, ';');
    If not IntCheckOut(S) then Goto ErrorExit;
    If LastToken <> ';' then Goto ErrorExit;
    Modify(GetInteger(S));
    If ModState = msClose then Goto ErrorExit;
    If S = EndChar then
    begin
     Result := IntToStr(I);
     Exit;
    end;
   end;
  end;
 Until False;
end;

function TScriptModule.FindModule(const Name: String): TScriptModule;
Var N: PNodeRec; M: TScriptModule;
begin
 Result := NIL;
 If FUses <> NIL then With FUses do
 begin
  N := Root;
  While N <> NIL do With N^ do
  begin
   M := Node as TScriptModule;
   If (M <> NIL) and (M.FName = Name) then
   begin
    Result := M;
    Exit;
   end;
   N := Next;
  end;
 end;
end;

end.
