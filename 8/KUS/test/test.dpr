program test;

{$APPTYPE CONSOLE}

uses
  windows,
  SysUtils,
  Graphics,
  Classes, HexUnit,
  kuClasses in '..\kuClasses.pas',
  kusCode in '..\kusCode.pas',
  kuScript in '..\kuScript.pas',
  kuSystem in '..\kuSystem.pas';

{$R *.res}

Procedure WriteInt(X: Integer); register;
begin
 Write(IntToHex(X, 1):5);
end;

Function Test1(A: Integer; B: Integer; C: Integer): Integer; register;
begin
 Result := A + B + C;
 Writeln(Format('Test1 (Register): %x + %x + %x = %x', [A, B, C, Result]));
end;

Function Test2(A: Integer; B: Integer; C: Integer): Integer; pascal;
begin
 Result := A + B + C;
 Writeln(Format('Test2 (Pascal): %x + %x + %x = %x', [A, B, C, Result]));
end;

Function Test3(A: Integer; B: Integer; C: Integer): Integer; cdecl;
begin
 Result := A + B + C;
 Writeln(Format('Test3 (Cdecl): %x + %x + %x = %x', [A, B, C, Result]));
end;

Function Test4(A: Integer; B: Integer; C: Integer): Integer; stdcall;
begin
 Result := A + B + C;
 Writeln(Format('Test4 (StdCall): %x + %x + %x = %x', [A, B, C, Result]));
end;

Const
 TestSize = $11 + 1;
 TestCode: Array[0..TestSize - 1] of TCodeRec =
 ((Instruction: $00; ParamC: $00000025), //00 mov r0, $25
  (Instruction: $A0; ParamC: $00000001), //01 push r0
  (Instruction: $00; ParamC: $000000AA), //02 mov r0, $AA
  (Instruction: $01; ParamC: $000000BB), //03 mov r1, $BB
  (Instruction: $02; ParamC: $000000CC), //04 mov r2, $CC
  (Instruction: $3C; ParamC: $00000000), //05 call Test1
  (Instruction: $81; ParamC: $00000000), //06 mov r1, r0
  (Instruction: $00; ParamC: $000000AA), //07 mov r0, $AA
  (Instruction: $02; ParamC: $000000CC), //08 mov r2, $CC
  (Instruction: $3C; ParamC: $00010000), //09 call Test2
  (Instruction: $01; ParamC: $000000BB), //0A mov r1, $BB
  (Instruction: $82; ParamC: $00000000), //0B mov r2, r0
  (Instruction: $00; ParamC: $000000AA), //0C mov r0, $AA
  (Instruction: $3C; ParamC: $00020000), //0D call Test3
  (Instruction: $01; ParamC: $000000BB), //0E mov r1, $BB
  (Instruction: $A1; ParamC: $00000004), //0F pop r2
  (Instruction: $3C; ParamC: $00020001), //10 call TestMod.MyProc
  (Instruction: $FF; ParamC: $FFFFFFFF)  //11 halt
  );
  ProcSize = $05 + 1;
  ProcCode: Array[0..ProcSize - 1] of TCodeRec = (
  (Instruction: $3C; ParamC: $00000000), //00 call Test4

  (Instruction: $3C; ParamC: $00010000), //01 call WriteInt
  (Instruction: $60; ParamC: $00000010), //02 dec r0,16
  (Instruction: $38; ParamLo: $0100;
                     ParamHi: Word(-2)), //03 bhei r0,0,$02
  (Instruction: $9E; ParamC: $0000001C), //04 mov mc, rmc
  (Instruction: $9F; ParamC: $0000001D)  //05 mov pc, rpc
  );

Var //NodeList: TNodeList; I, X, Y: Integer; S: String;
 KUSProgram: TKUSProgram; P, PP: PChar; // A, B, C, D: Integer;
 I, J: Integer; SModule, Sys: TScriptModule; S: String; TR: TTypeRec;
 Script: TKUScript; TP: TKUSType; Func: TKUSFunction; C: Boolean;

begin
 Script := TKUScript.Create;
 SModule := TScriptModule.Create;
 SModule.FName := 'SYSTEM';
 TR.FMode := tmRange;
 TR.FRangeMode := rmInteger;
 SModule.FTypes.Add^.Node := TKUSType.Create('INTEGER', TR);
 TR.FRangeMode := rmByte;
 SModule.FTypes.Add^.Node := TKUSType.Create('BYTE', TR);
 Sys := TScriptModule.Create;
 Sys.FName := 'SYS';
 S := '-((222 div not 3) - (8 shl 4)) * (-Sys.Test div 2);'#0; //-9
 P := Addr(S[1]);
 SModule.FUses.Add^.Node := Sys;
 Sys.FUses.Add^.Node := SModule;
 Sys.FConstants.Add^.Node := TKUSConstant.Create('TEST', '12 * 12;');
 Writeln(-((222 div not 3) - (8 shl 4)) * (-(12 * 12) div 2));
 Writeln(SModule.ParseConstant(P, I, J));
 Script.FScriptModules.Add^.Node := SModule;
 Script.FScriptModules.Add^.Node := Sys;
{ S := 'TRecord = Record ^Integer A, B, C; Byte D, E, F; End;'#0;
 P := Addr(S[1]);
 TP := Script.ParseType('SYS', P, I, J);
 Writeln(TP.FName);
 S := 'TRecX = Record Integer U; Sys.TRecord D, E, F; End;'#0;
 P := Addr(S[1]);
 TP := Script.ParseType('SYSTEM', P, I, J);
 Writeln(TP.FName); }
 P := 'TRecord = Record ^Integer A, B, C; Byte D, E, F; End;';
 PP := 'TRecX = Record Integer U; Sys.TRecord D, E, F; End;';
 C := (Script.ParseType('SYS', P, I, J) <> NIL) and
      (Script.ParseType('SYSTEM', PP, I, J) <> NIL);
 Writeln(C);
 Func := Script.AddSystemFunction('SYSTEM',
  'external func(stdcall) ____Test___(Integer X, Y, Z): Integer;'#0, @Test4);
 If Func <> NIl then
 Writeln(Func.FName);
 readln;
 Script.Free;
// SModule.Free;
// Sys.Free;
// Exit;
// GetMem(P, 15);
// Writeln(Length(P));
// KUSFillChar(P, 4, 0);
// P := KUSAddString(P, 'Test');
// P := KUSAddChar(P, 'C');
// Writeln(P);
// KUSClearString(P);
 KUSProgram := TKUSProgram.Create;
 With KUSProgram.Add^.Node as TKUSModule do
 begin
  Name := 'TEST';
  With Add^.Node as TKUSMethod do
  begin
   FName := 'TEST1';
   FParamCount := 3;
   FMethodType := mtExternal;
   FFunction := True;
   FCallingConvention := ccRegister;
   FProcAddress := @Test1;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'TEST2';
   FParamCount := 3;
   FMethodType := mtExternal;
   FFunction := True;
   FCallingConvention := ccPascal;
   FProcAddress := @Test2;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'TEST3';
   FParamCount := 3;
   FMethodType := mtExternal;
   FFunction := True;
   FCallingConvention := ccCdecl;
   FProcAddress := @Test3;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'TEST4';
   FParamCount := 3;
   FMethodType := mtExternal;
   FFunction := True;
   FCallingConvention := ccStdCall;
   FProcAddress := @Test4;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'MAIN';
   FParamCount := 0;
   SetLength(FCode, TestSize);
   System.Move(TestCode[0], FCode[0], TestSize * SizeOf(TCodeRec));
  end;
  Unite;
 end;
 With KUSProgram.Add^.Node as TKUSModule do
 begin
  Name := 'TESTMOD';
  With Add^.Node as TKUSMethod do
  begin
   FName := 'TEST4';
   FParamCount := 3;
   FMethodType := mtExternal;
   FFunction := True;
   FCallingConvention := ccStdCall;
   FProcAddress := @Test4;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'WRITEINT';
   FParamCount := 1;
   FMethodType := mtExternal;
   FFunction := False;
   FCallingConvention := ccRegister;
   FProcAddress := @WriteInt;
  end;
  With Add^.Node as TKUSMethod do
  begin
   FName := 'MYPROC';
   FParamCount := 3;
   SetLength(FCode, ProcSize);
   System.Move(ProcCode[0], FCode[0], ProcSize * SizeOf(TCodeRec));
  end;
  Unite;
 end;
 KUSProgram.FMainModule := KUSProgram.Root^.Node as TKUSModule;
 With KUSProgram.Root^.Node as TKUSModule do
 begin
  SetLength(FUses, 1);
  FUses[0] := KUSProgram.Cur^.Node as TKUSModule;
  Writeln(Format('Executing program "%s"...', [Name]));
 end;
 KUSProgram.Unite;
 Case KUSProgram.Execute([]) of
  KUS_NormalExec: Writeln(#13#10'Executed successfully.');
  KUS_NothingToExec: Writeln(#13#10'Nothing to execute');
  KUS_MainProcNotFound: Writeln(#13#10'Main procedure not found.');
  KUS_WrongParameters: Writeln(#13#10'Wrong main arguments.');
  KUS_NoCode: Writeln(#13#10'Main procedure is empty.');
  KUS_Exception: Writeln(Format(#13#10'Encountered an exception error: %s', [KUS_ExceptionMessage]));
 end;
 Write('Press any key to continue.'); Readln;
 For I := 0 to 31 do
 begin
  If (I > 0) and (I mod 5 = 0) then Writeln;
  Write(Format('r%d: %8x ', [I, KUSProgram.Registers.C[I]]):14);
 end;
 Writeln;
 Write(IntToHex(Test4(Test3(Test2(Test1($AA, $BB, $CC), $BB, $CC), $BB, $CC), $BB, $CC), 0) + ' ');
 Write('Press any key to exit.'); Readln;
 KUSProgram.Free;
end.
