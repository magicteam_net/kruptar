unit kuSystem;

interface

Procedure KUSGetMem(Var P: Pointer; Size: Integer);
Procedure KUSFreeMem(Var P: Pointer);
Function KUSAddString(P: PChar; SS: PChar): PChar;
Function KUSAddChar(P: PChar; C: Char): PChar;
Procedure KUSClearString(Var P: PChar);
Function KUSAddWideString(P: PWideChar; SS: PWideChar): PWideChar;
Function KUSAddWideChar(P: PWideChar; C: WideChar): PWideChar;
Procedure KUSClearWideString(Var P: PWideChar);
Function KUSWideStringToString(P: PWideChar): PChar;
Function KUSStringToWideString(P: PChar): PWideChar;
Procedure KUSMove(Source, Dest: Pointer; Size: Integer);
Procedure KUSFillChar(Dest: Pointer; Count: Integer; Value: Byte);
Function HexToInt(Value: String): Integer;
Function IntCheckOut(Value: String): Boolean;
Function UnsCheckOut(Value: string): Boolean;
Function GetInteger(Value: String): Integer;
Function GetCardinal(Value: String): Cardinal;
Function StrToInt(Value: String): Integer;
Function StrToCardinal(Value: String): Cardinal;

Var
 HexError: Boolean = False;
 IntError: Boolean = False;

implementation

Procedure KUSFillChar(Dest: Pointer; Count: Integer; Value: Byte);
begin
 FillChar(Dest^, Count, Value);
end;

Procedure KUSMove(Source, Dest: Pointer; Size: Integer);
begin
 Move(Source^, Dest^, Size);
end;

Procedure KUSGetMem(Var P: Pointer; Size: Integer);
begin
 GetMem(P, Size);
end;

Procedure KUSFreeMem(Var P: Pointer);
begin
 FreeMem(P);
 P := NIL;
end;

Function KUSAddString(P: PChar; SS: PChar): PChar;
Var S: String; L: Integer;
begin
 S := String(P) + String(SS);
 L := Length(S);
 ReallocMem(P, L + 1);
 If L > 0 then Move(S[1], P^, L);
 Result := P;
 Inc(P, L);
 P^ := #0;
 S := '';
end;

Function KUSAddChar(P: PChar; C: Char): PChar;
Var S: String; L: Integer;
begin
 S := String(P) + C;
 L := Length(S);
 ReallocMem(P, L + 1);
 If L > 0 then Move(S[1], P^, L);
 Result := P;
 Inc(P, L);
 P^ := #0;
 S := '';
end;

Procedure KUSClearString(Var P: PChar);
begin
 ReallocMem(P, 1);
 P^ := #0;
end;

Function KUSAddWideString(P: PWideChar; SS: PWideChar): PWideChar;
Var S: WideString; L: Integer;
begin
 S := WideString(P) + WideString(SS);
 L := Length(S);
 ReallocMem(P, (L + 1) shl 1);
 If L > 0 then Move(S[1], P^, L shl 1);
 Result := P;
 Inc(P, L);
 P^ := #0;
 S := '';
end;

Function KUSAddWideChar(P: PWideChar; C: WideChar): PWideChar;
Var S: WideString; L: Integer;
begin
 S := WideString(P) + C;
 L := Length(S);
 ReallocMem(P, (L + 1) shl 1);
 If L > 0 then Move(S[1], P^, L shl 1);
 Result := P;
 Inc(P, L);
 P^ := #0;
 S := '';
end;

Procedure KUSClearWideString(Var P: PWideChar);
begin
 ReallocMem(P, 1);
 P^ := #0;
end;

Function KUSWideStringToString(P: PWideChar): PChar;
begin
 Result := PChar(String(P));
end;

Function KUSStringToWideString(P: PChar): PWideChar;
begin
 Result := PWideChar(WideString(P));
end;

Function HexToInt(Value: String): Integer;
Var
 I, LS, J: Integer; PS: ^Char; H: Char;
begin
 HexError := True;
 Result := 0;
 LS := Length(Value);
 If (LS <= 0) or (LS > 8) then Exit;
 HexError := False;
 PS := Addr(Value[1]);
 I := LS - 1;
 J := 0;
 While I >= 0 do
 begin
  H := UpCase(PS^);
  If H in ['0'..'9'] then J := Byte(H) - 48 Else
  If H in ['A'..'F'] then J := Byte(H) - 55 Else
  begin
   HexError := True;
   Result := 0;
   Exit;
  end;
  Inc(Result, J shl (I shl 2));
  Inc(PS);
  Dec(I);
 end;
end;

Function IntCheckOut(Value: String): Boolean;
Var I: Integer;
begin
 Result := False;
 if Value = '' then exit;
 Result := True;
 if (Length(Value) >= 3) and (Value[1] = '0') and (Value[2] = 'x') then
 begin
  Delete(Value, 1, 2);
  if (length(Value) > 8) or (Value = '') then
  begin
   Result := false;
   Exit;
  end;
  For I := 1 to Length(Value) do
   If not (Value[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then
   begin
    Result := False;
    Exit;
   end;
 end Else
 begin
  If Value[1] = '-' then
   Delete(Value, 1, 1);
  If (Length(Value) > 10) or (Value = '') then
  begin
   Result := false;
   Exit;
  end;
  For i := 1 to Length(Value) do
   If not (Value[i] in ['0'..'9']) then
   begin
    Result := False;
    Exit;
   end;
 end;
end;

Function UnsCheckOut(Value: string): Boolean;
Var i: integer;
begin
 Result := False;
 if Value = '' then exit;
 Result := True;
 if (Length(Value) >= 3) and (Value[1] = '0') and (UpCase(Value[2]) = 'X') then
 begin
  Delete(Value, 1, 2);
  if (length(Value) > 8) or (Value = '') then
  begin
   Result := false;
   Exit;
  end;
  For i := 1 to Length(Value) do
   If not (Value[i] in ['0'..'9', 'A'..'F', 'a'..'f']) then
   begin
    Result := False;
    Exit;
   end;
 end Else
 begin
  if (length(Value) > 10) or (Value = '') then
  begin
   Result := false;
   Exit;
  end;
  For i := 1 to Length(Value) do
   If not (Value[i] in ['0'..'9']) then
   begin
    Result := False;
    Exit;
   end;
 end;
end;

Function GetInteger(Value: String): Integer;
begin
 IntError := True;
 If Value = '' then
 begin
  Result := 0;
  Exit;
 end;
 If (Length(Value) >= 3) and (Value[1] = '0') and (UpCase(Value[2]) = 'X') then
 begin
  Delete(Value, 1, 2);
  If Value = '' then
  begin
   Result := 0;
   HexError := True;
   IntError := False;
  end Else
  begin
   Result := HexToInt(Value);
   IntError := False;
  end;
 end Else
 begin
  Result := StrToInt(Value);
  HexError := False;
 end;
end;

Function GetCardinal(Value: String): Cardinal;
begin
 IntError := True;
 If Value = '' then
 begin
  Result := 0;
  Exit;
 end;
 If (Length(Value) >= 3) and (Value[1] = '0') and (UpCase(Value[2]) = 'X') then
 begin
  Delete(Value, 1, 2);
  If Value = '' then
  begin
   Result := 0;
   HexError := True;
   IntError := False;
  end Else
  begin
   Result := Cardinal(HexToInt(Value));
   IntError := False;
  end;
 end Else
 begin
  Result := StrToCardinal(Value);
  HexError := False;
 end;
end;

Function StrToInt(Value: String): Integer;
Var E: Integer;
begin
 Val(Value, Result, E);
 IntError := E <> 0;
end;

Function StrToCardinal(Value: String): Cardinal;
Var P: PChar; I: Integer; C: Integer;
begin
 Result := 0;
 IntError := True;
 If (Length(Value) < 1) or (Length(Value) > 10) then Exit;
 P := Addr(Value[Length(Value)]);
 For I := 0 to Length(Value) - 1 do
 begin
  C := Byte(P^) - Byte('0');
  If (C >= 0) and (C <= 9) then
  begin
   If I = 0 then
    Result := C Else
    Result := Result + (Byte(C) * (Byte(I) * 10));
  end Else
  begin
   Result := 0;
   Exit;
  end;
  Dec(P);
 end;
 IntError := False;
end;


end.
