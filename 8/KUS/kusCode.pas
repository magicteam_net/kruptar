unit kusCode;

interface

Uses kuClasses, SysUtils;

Type
 TCodeRec = Packed Record
  Instruction: Byte;
  Case Byte of
   0: (Param: Integer);
   1: (ParamC: Cardinal);
   2: (ParamLo: Word;
       ParamHi: Word);
   3: (Params: Array[0..3] of Byte);
 end;
 TKUSProgram = Class;
 TKUSModule = Class;
 TMethodType = (mtNormal, mtExternal);
 TCallingConvention = (ccRegister, ccPascal, ccCdecl, ccStdCall);
 TKUSMethod = Class
 private
  FOwner: TKUSModule;
  FIndex: Integer;
 public
  FName: String;
  FParamCount: Integer;
  FFunction: Boolean;
  FMethodType: TMethodType;
  FCallingConvention: TCallingConvention;
  FProcAddress: Pointer;
  FCode: Array of TCodeRec;
  Constructor Create;
  Destructor Destroy; override;
  Property Name: String read FName;
 end;
 TKUSModule = Class(TNodeList)
 private
  FOwner: TKUSProgram;
  FName: String;
  FData: Array of Byte;
  Function GetMethod(Index: Integer): TKUSMethod;
  Function GetByName(Name: String): TKUSMethod;
 public
  FHeap: Array of Byte;
  FUses: Array of TKUSModule; 
  Constructor Create; override;
  Destructor Destroy; override;
  Function Add: PNodeRec; override;
  Property Methods[Index: Integer]: TKUSMethod read GetMethod;
  Property Names[Name: String]: TKUSMethod read GetByName;
  Property Name: String read FName write FName;
  Property Owner: TKUSProgram read FOwner;
 end;
 TRegisters = Packed Record
  Case Byte of
   0: (C: Array[0..31] of Cardinal);
   1: (I: Array[0..31] of Integer);
   2: (r0: Cardinal;
       r1: Cardinal;
       r2: Cardinal;
       r3: Cardinal;
       r4: Cardinal;
       r5: Cardinal;
       r6: Cardinal;
       r7: Cardinal;
       r8: Cardinal;
       r9: Cardinal;
       r10: Cardinal;
       r11: Cardinal;
       r12: Cardinal;
       r13: Cardinal;
       r14: Cardinal;
       r15: Cardinal;
       r16: Cardinal;
       r17: Cardinal;
       r18: Cardinal;
       r19: Cardinal;
       r20: Cardinal;
       r21: Cardinal;
       r22: Cardinal;
       r23: Cardinal;
       r24: Cardinal;
       r25: Cardinal;
       r26: Cardinal;
       SP: ^Cardinal;
       RMC: ^TKUSMethod;
       RPC: ^TCodeRec;
       MC: ^TKUSMethod;
       PC: ^TCodeRec);
 end;
 TKUSProgram = Class(TNodeList)
 private
  FExecuted: Boolean;
  FRegisters: TRegisters;
  FStack: Array of Byte;
  Function GetModule(Index: Integer): TKUSModule;
  Function GetByName(Name: String): TKUSModule;
 public
  FMainModule: TKUSModule;
  Function Execute(Const ParamList: Array of Cardinal): Integer;
  Procedure InstructionExec;
  Constructor Create; override;
  Procedure Clear; override;
  Function Add: PNodeRec; override;
  Property Modules[Index: Integer]: TKUSModule read GetModule;
  Property Names[Name: String]: TKUSModule read GetByName;
  Property Registers: TRegisters read FRegisters;
  Procedure SetStack(Size: Integer);  
 end;

Const
 KUS_NormalExec = 0;
 KUS_NothingToExec = 1;
 KUS_MainProcNotFound = 2;
 KUS_WrongParameters = 3;
 KUS_NoCode = 4;
 KUS_Exception = 5;
// KUS_DivByZero = 6;
// KUS_AccessViolation = 7;
Var
 KUS_ExceptionMessage: String = '';

implementation

Var Exx: Exception;

Procedure TKUSProgram.InstructionExec;
Var
 B: ^Byte; W: ^Word; D: ^Cardinal; C1, C2: Cardinal; I1, I2: Integer;
 Method: TKUSMethod; Module: TKUSModule;
Label MethodCall; 
begin
 With FRegisters, PC^ do
 begin
  Case Instruction of
   0..31: // mov
   begin
    C[Instruction] := ParamC;
    Inc(PC);
   end;
   32:
   begin
    If Params[0] shr 7 = 1 then // addi
     I[Params[0] and 31] := I[Params[1] and 31] + I[Params[2] and 31];
     I[Params[0] and 31] := I[Params[1] and 31] + SmallInt(ParamHi);
    Inc(PC);
   end;
   33:
   begin
    If Params[0] shr 7 = 1 then // add
     C[Params[0] and 31] := C[Params[1] and 31] + C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] + ParamHi;
    Inc(PC);
   end;
   34:
   begin
    If Params[0] shr 7 = 1 then // subi
     I[Params[0] and 31] := I[Params[1] and 31] - I[Params[2] and 31] Else
     I[Params[0] and 31] := I[Params[1] and 31] - SmallInt(ParamHi);
    Inc(PC);
   end;
   35:
   begin
    If Params[0] shr 7 = 1 then // sub
     C[Params[0] and 31] := C[Params[1] and 31] + C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] - ParamHi;
    Inc(PC);
   end;
   36:
   begin
    If Params[0] shr 7 = 1 then // muli
     I[Params[0] and 31] := I[Params[1] and 31] * I[Params[2] and 31] Else
     I[Params[0] and 31] := I[Params[1] and 31] * SmallInt(ParamHi);
    Inc(PC);
   end;
   37:
   begin
    If Params[0] shr 7 = 1 then // mul
     C[Params[0] and 31] := C[Params[1] and 31] * C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] * ParamHi;
    Inc(PC);
   end;
   38:
   begin
    If Params[0] shr 7 = 1 then // divi
     I[Params[0] and 31] := I[Params[1] and 31] div I[Params[2] and 31] Else
     I[Params[0] and 31] := I[Params[1] and 31] div SmallInt(ParamHi);
    Inc(PC);
   end;
   39:
   begin
    If Params[0] shr 7 = 1 then // div
     C[Params[0] and 31] := C[Params[1] and 31] div C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] div ParamHi;
    Inc(PC);
   end;
   40:
   begin
    If Params[0] shr 7 = 1 then // and
     C[Params[0] and 31] := C[Params[1] and 31] and C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] and ParamHi;
   Inc(PC);
   end;
   41:
   begin
    If Params[0] shr 7 = 1 then // orr
     C[Params[0] and 31] := C[Params[1] and 31] or C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] or ParamHi;
    Inc(PC);
   end;
   42:
   begin
    If Params[0] shr 7 = 1 then // xor
     C[Params[0] and 31] := C[Params[1] and 31] xor C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] xor ParamHi;
    Inc(PC);
   end;
   43:
   begin
    If Params[0] shr 7 = 1 then // nor
     C[Params[0] and 31] := C[Params[1] and 31] or not C[Params[2] and 31] Else
     C[Params[0] and 31] := C[Params[1] and 31] or not ParamHi;
    Inc(PC);
   end;
   44: //ldrb
   begin
    If Params[0] shr 7 = 1 then
    begin
     B := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     C[Params[0]] := B^;
    end Else
    begin
     B := Pointer(C[Params[1] and 31] + ParamHi);
     C[Params[0]] := B^;
    end;
    Inc(PC);
   end;
   45: //ldrh
   begin
    If Params[0] shr 7 = 1 then
    begin
     W := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     C[Params[0]] := W^;
    end Else
    begin
     W := Pointer(C[Params[1] and 31] + ParamHi);
     C[Params[0]] := W^;
    end;
    Inc(PC);
   end;
   46: //ldr
   begin
    If Params[0] shr 7 = 1 then
    begin
     D := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     C[Params[0]] := D^;
    end Else
    begin
     D := Pointer(C[Params[1] and 31] + ParamHi);
     C[Params[0]] := D^;
    end;
    Inc(PC);
   end;
   47: //shl
   begin
    If Params[0] shr 7 = 1 then
     C[Params[0] and 31] := C[Params[1] and 31] shl (C[Params[2] and 31] and 31) Else
     C[Params[0] and 31] := C[Params[1] and 31] shl (Params[2] and 31);
    Inc(PC);
   end;
   48: //shr
   begin
    If Params[0] shr 7 = 1 then
     C[Params[0] and 31] := C[Params[1] and 31] shr (C[Params[2] and 31] and 31) Else
     C[Params[0] and 31] := C[Params[1] and 31] shr (Params[2] and 31);
    Inc(PC);
   end;
   49: Inc(PC, Param); //jmp
   50..55:
   begin
    C1 := C[Params[0] and 31];
    C2 := Params[1];
    If C2 and 1 = 0 then
     C2 := C[(C2 shr 1) and 31] Else
     C2 := C2 shr 1;
    Case Instruction of
     50: If C1 = C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC);  //50 beq
     51: If C1 <> C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC); //51 bne
     52: If C1 >= C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC); //52 bhe
     53: If C1 < C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC);  //53 bl
     54: If C1 <= C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC); //54 ble
     55: If C1 > C2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC);  //55 bh
    end;
   end;
   56..59:
   begin
    I1 := I[Params[0] and 31];
    I2 := Params[1];
    If I2 and 1 = 0 then
     I2 := I[(Byte(I2) shr 1) and 31] Else
     I2 := Byte(I2) shr 1;
    Case Instruction of
     56: If I1 >= I2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC); //56 bhei
     57: If I1 < I2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC);  //57 bli
     58: If I1 <= I2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC); //58 blei
     59: If I1 > I2 then Inc(PC, SmallInt(ParamHi)) Else Inc(PC);  //59 bhi
    end;
   end;
   60: //call
   begin
    I1 := ParamLo;
    If (MC <> NIL) and (MC^ <> NIL) then With MC^ do
    begin
     If I1 > 0 then With FOwner do
     begin
      Dec(I1);
      If I1 < Length(FUses) then
      begin
       Module := FUses[I1];
       If Module <> NIL then With Module do
       begin
        Method := Methods[ParamHi];
       MethodCall:
        If Method <> NIL then
        begin
         With Method do If FMethodType = mtNormal then
         begin
          Integer(RMC) := Integer(MC);
          Integer(RPC) := Integer(PC);
          MC := Addr(Method);
          If Length(FCode) > 0 then
           PC := Addr(FCode[0]) Else
           raise Exx;
         end Else
         begin
          If FProcAddress = NIL then raise Exx;
          Case FCallingConvention of
           ccRegister:
           Asm
            push eax
            push ebx
            push edx
            push ecx
            push esi
            mov esi, [Self]
            add esi, Offset FRegisters            
            mov ebx, dword ptr [Method]
            mov ebx, dword ptr [ebx + Offset TKUSMethod.FParamCount]
            cmp ebx, 4
            jl @@NoPush
            mov ecx, ebx
            sub ecx, 3
            cld
            mov eax, 12
           @@PushR:
             mov edx, dword ptr [esi + eax]
             push edx
             add eax, 4
            Loop @@PushR
           @@NoPush:
            cmp ebx, 1
            jge @@Next1
            jmp @@None
           @@Next1:
            mov eax, dword ptr [esi]
            cmp ebx, 2
            jge @@Next2
            jmp @@None
           @@Next2:
            mov edx, dword ptr [esi + 4]
            cmp ebx, 3
            jge @@Next3
            jmp @@None
           @@Next3:
            mov ecx, dword ptr [esi + 8]
           @@None:
            mov ebx, dword ptr [Method]
            call dword ptr [ebx + Offset TKUSMethod.FProcAddress]
            mov ebx, dword ptr [Method]
            movzx ebx, byte ptr [ebx + Offset TKUSMethod.FFunction]
            cmp ebx, 1
            jne @@NoResult
            mov ebx, [Self]
            add ebx, Offset FRegisters
            mov dword ptr [ebx], eax
           @@NoResult:
            pop esi
            pop ecx
            pop edx
            pop ebx
            pop eax
           end;
           ccPascal:
           Asm
            push eax
            push ebx
            push edx
            push ecx
            push esi
            mov ecx, dword ptr [Method]
            mov ecx, dword ptr [ecx + Offset TKUSMethod.FParamCount]
            cmp ecx, 1
            jl @@NoPush
            cld
            mov eax, 0
            mov esi, [Self]
            add esi, Offset FRegisters
           @@PushR:
             mov edx, dword ptr [esi + eax]
             push edx
             add eax, 4
            Loop @@PushR
           @@NoPush:
            mov ebx, dword ptr [Method]
            call dword ptr [ebx + Offset TKUSMethod.FProcAddress]
            mov ebx, dword ptr [Method]
            movzx ebx, byte ptr [ebx + Offset TKUSMethod.FFunction]
            cmp ebx, 1
            jne @@NoResult
            mov ebx, [Self]
            add ebx, Offset FRegisters
            mov dword ptr [ebx], eax
           @@NoResult:
            pop esi
            pop ecx
            pop edx
            pop ebx
            pop eax
           end;
           ccCdecl:
           Asm
            push eax
            push ebx
            push edx
            push ecx
            push esi
            mov ecx, dword ptr [Method]
            mov ecx, dword ptr [ecx + Offset TKUSMethod.FParamCount]
            cmp ecx, 1
            jl @@NoPush
            cld
            mov eax, ecx
            sub eax, 1
            shl eax, 2
            mov esi, [Self]
            add esi, Offset FRegisters
           @@PushR:
             mov edx, dword ptr [esi + eax]
             push edx
             sub eax, 4
            Loop @@PushR
           @@NoPush:
            mov ebx, dword ptr [Method]
            call dword ptr [ebx + Offset TKUSMethod.FProcAddress]
            mov ecx, dword ptr [Method]
            mov ebx, dword ptr [ecx + Offset TKUSMethod.FParamCount]
            shl ebx, 2
            add esp, ebx
            movzx ebx, byte ptr [ecx + Offset TKUSMethod.FFunction]
            cmp ebx, 1
            jne @@NoResult
            mov ebx, [Self]
            add ebx, Offset FRegisters
            mov dword ptr [ebx], eax
           @@NoResult:
            pop esi
            pop ecx
            pop edx
            pop ebx
            pop eax
           end;
           ccStdCall:
           Asm
            push eax
            push ebx
            push edx
            push ecx
            push esi
            mov ecx, dword ptr [Method]
            mov ecx, dword ptr [ecx + Offset TKUSMethod.FParamCount]
            cmp ecx, 1
            jl @@NoPush
            cld
            mov eax, ecx
            sub eax, 1
            shl eax, 2
            mov esi, [Self]
            add esi, Offset FRegisters
           @@PushR:
             mov edx, dword ptr [esi + eax]
             push edx
             sub eax, 4
            Loop @@PushR
           @@NoPush:
            mov ebx, dword ptr [Method]
            call dword ptr [ebx + Offset TKUSMethod.FProcAddress]
            mov ebx, dword ptr [Method]
            movzx ebx, byte ptr [ebx + Offset TKUSMethod.FFunction]
            cmp ebx, 1
            jne @@NoResult
            mov ebx, [Self]
            add ebx, Offset FRegisters
            mov dword ptr [ebx], eax
           @@NoResult:
            pop esi
            pop ecx
            pop edx
            pop ebx
            pop eax
           end;
          end;
          Inc(PC);
         end;
        end;
       end;
      end;
     end Else
     begin
      Method := FOwner.Methods[ParamHi];
      Goto MethodCall;
     end;
    end;
   end;
   61: //strb
   begin
    If Params[0] shr 7 = 1 then
    begin
     B := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     B^ := Byte(C[Params[0]]);
    end Else
    begin
     B := Pointer(C[Params[1] and 31] + ParamHi);
     B^ := Byte(C[Params[0]]);
    end;
    Inc(PC);
   end;
   62: //strh
   begin
    If Params[0] shr 7 = 1 then
    begin
     W := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     W^ := Word(C[Params[0]]);
    end Else
    begin
     W := Pointer(C[Params[1] and 31] + ParamHi);
     W^ := Word(C[Params[0]]);
    end;
    Inc(PC);
   end;
   63: //str
   begin
    If Params[0] shr 7 = 1 then
    begin
     D := Pointer(C[Params[1] and 31] + C[Params[2] and 31]);
     D^ := C[Params[0]];
    end Else
    begin
     D := Pointer(C[Params[1] and 31] + ParamHi);
     D^ := C[Params[0]];
    end;
    Inc(PC);
   end;
   64..95: //increg
   begin
    Inc(C[Instruction - 64], ParamC);
    Inc(PC);
   end;
   96..127: //decreg
   begin
    Dec(C[Instruction - 96], ParamC);
    Inc(PC);
   end;
   128..159: //movreg
   begin
    C[Instruction - 128] := C[Params[0] and 31];
    Inc(PC);
   end;
   160: //push
   begin
    C1 := Param;
    I1 := 0;
    While C1 > 0 do
    begin
     If C1 and 1 = 1 then
     begin
      Dec(SP);
      SP^ := C[I1];
     end;
     C1 := C1 shr 1;
     Inc(I1);
    end;
    Inc(PC);
   end;
   161: //pop
   begin
    C1 := Param;
    I1 := 0;
    While C1 > 0 do
    begin
     If C1 and 1 = 1 then
     begin
      C[I1] := SP^;
      Inc(SP);
     end;
     C1 := C1 shr 1;
     Inc(I1);
    end;
    Inc(PC);
   end;
   162: //lhp
   begin
    I1 := ParamLo;
    If (MC <> NIL) and (MC^ <> NIL) then With MC^ do
    begin
     If I1 > 0 then With FOwner do
     begin
      Dec(I1);
      If I1 < Length(FUses) then
      begin
       Module := FUses[I1];
       If Module <> NIL then With Module do
       begin
        If Length(FHeap) > 0 then
         C[ParamHi and 31] := Integer(Addr(FHeap[0])) Else
         C[ParamHi and 31] := 0;
       end;
      end Else C[ParamHi and 31] := 0;
     end Else With FOwner do
     begin
      If Length(FHeap) > 0 then
       C[ParamHi and 31] := Integer(Addr(FHeap[0])) Else
       C[ParamHi and 31] := 0;
     end;
    end;
    Inc(PC);
   end;
   $FE: Inc(PC); //nop
   $FF: FExecuted := True;
   Else raise Exx;
  end;
 end;
end;

Function TKUSProgram.Execute(Const ParamList: Array of Cardinal): Integer;
Var
 Main: TKUSMethod; L: Integer; Module: TKUSModule;
begin
 If Count > 0 then
 begin
  Result := KUS_NormalExec;
  Module := FMainModule; //Root^.Node as TKUSModule;
  If Module = NIL then
  begin
   Result := KUS_NothingToExec;
   Exit;
  end;
  With Module do
  begin
   Main := Names['MAIN'];
   If Main <> NIL then With Main do
   begin
    L := Length(ParamList);
    If L = FParamCount then
    begin
     If Length(FCode) > 0 then
     begin
      If L > 0 then With FRegisters do
       For L := 0 to FParamCount - 1 do C[L] := ParamList[L];
      FRegisters.MC := Addr(Main);
      FRegisters.PC := Addr(FCode[0]);
      FExecuted := False;
      try
       Repeat InstructionExec Until FExecuted;
      except
       on E: Exception do
       begin
        Result := KUS_Exception;
        KUS_ExceptionMessage := E.Message;
       end;
      end;
     end Else Result := KUS_NoCode;
    end Else Result := KUS_WrongParameters;
   end Else Result := KUS_MainProcNotFound;
  end;
 end Else Result := KUS_NothingToExec;
end;

Constructor TKUSMethod.Create;
begin
end;

Destructor TKUSMethod.Destroy;
begin
 FName := '';
 SetLength(FCode, 0);
 inherited Destroy;
end;

Constructor TKUSModule.Create;
begin
 inherited Create;
 FName := '';
end;

Destructor TKUSModule.Destroy;
begin
 FName := '';
 SetLength(FData, 0);
 SetLength(FHeap, 0);
 SetLength(FUses, 0);
 inherited Destroy;
end;

Function TKUSModule.Add: PNodeRec;
begin
 Result := Inherited Add;
 With Result^ do
 begin
  Node := TKUSMethod.Create;
  With (Node as TKUSMethod) do
  begin
   FOwner := Self;
   FIndex := Count - 1;
  end;
 end;
end;

Function TKUSModule.GetMethod(Index: Integer): TKUSMethod;
begin
 If (Index >= 0) and (Index < Length(FObjects)) then
  Result := FObjects[Index] as TKUSMethod Else
  Result := NIL;
end;

Function TKUSModule.GetByName(Name: String): TKUSMethod;
Var N: PNodeRec;
begin
 N := Root; Result := NIL;
 While N <> NIL do With N^ do
 begin
  If (Node as TKUSMethod).FName = Name then
  begin
   Result := Node as TKUSMethod;
   Exit;
  end;
  N := Next;
 end;
end;

Function TKUSProgram.GetModule(Index: Integer): TKUSModule;
begin
 If (Index >= 0) and (Index < Length(FObjects)) then
  Result := FObjects[Index] as TKUSModule Else
  Result := NIL;
end;

Function TKUSProgram.GetByName(Name: String): TKUSModule;
Var N: PNodeRec;
begin
 N := Root; Result := NIL;
 While N <> NIL do With N^ do
 begin
  If (Node as TKUSModule).FName = Name then
  begin
   Result := Node as TKUSModule;
   Exit;
  end;
  N := Next;
 end;
end;

Procedure TKUSProgram.SetStack(Size: Integer);
begin
 SetLength(FStack, Size);
 FRegisters.SP := Addr(FStack[Size]);
end;

Constructor TKUSProgram.Create;
begin
 inherited Create;
 SetStack(16384);
end;

Function TKUSProgram.Add: PNodeRec;
begin
 Result := inherited Add;
 With Result^ do
 begin
  Node := TKUSModule.Create;
  (Node as TKUSModule).FOwner := Self;
 end;
end;

Procedure TKUSProgram.Clear;
begin
 inherited Clear;
 SetLength(FStack, 0);
 FMainModule := NIL; 
end;

initialization
 Exx := Exception.Create('Execute Error');
finalization
 Exx.Free;
end.
