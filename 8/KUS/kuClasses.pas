unit kuClasses;

interface

Type
 TNode = TObject;
 PNodeRec = ^TNodeRec;
 TNodeRec = Record
  Node: TNode;
  Next: PNodeRec;
 end;
 TNodeList = Class
  private
   FRoot: PNodeRec;
   FCur: PNodeRec;
   FCount: Integer;
   Function Get(Index: Integer): PNodeRec;
  public
   FObjects: Array of TNode;
   Procedure Unite; Virtual;
   Constructor Create; Virtual;
   Destructor Destroy; override;
   Function Add: PNodeRec; virtual;
   Procedure Clear; virtual;
   Procedure Remove(Item: PNodeRec); virtual;
   Procedure Exchange(Index1, Index2: Integer); virtual;
   Procedure Move(CurIndex, NewIndex: Integer); virtual;
   Property Count: Integer read FCount;
   Property Root: PNodeRec read FRoot;
   Property Cur: PNodeRec read FCur;
   Property Items[Index: Integer]: PNodeRec read Get;
 end;
 TNodeListEx = Class(TNodeList)
 public
  Procedure Clear; override;
 end;

implementation

Procedure TNodeList.Unite;
Var N: PNodeRec; I: Integer;
begin
 SetLength(FObjects, FCount);
 N := FRoot;
 I := 0;
 While N <> NIL do
 begin
  FObjects[I] := N^.Node;
  Inc(I);
  N := N^.Next;
 end;
end;

Function TNodeList.Get(Index: Integer): PNodeRec;
Var I: Integer; N: PNodeRec;
begin
 I := 0; N := FRoot; Result := NIL;
 While N <> NIL do
 begin
  If I = Index then
  begin
   Result := N;
   Exit;
  end;
  Inc(I);
  N := N^.Next;
 end;
end;

Constructor TNodeList.Create;
begin
 FRoot := NIL;
 FCur := NIL;
 FCount := 0;
end;

Destructor TNodeList.Destroy;
begin
 SetLength(FObjects, 0);
 Clear;
 inherited Destroy;
end;

Function TNodeList.Add: PNodeRec;
begin
 New(Result);
 If FRoot = NIL then FRoot := Result Else FCur^.Next := Result;
 FCur := Result;
 FillChar(Result^, SizeOf(TNodeRec), 0);
 Inc(FCount);
end;

Procedure TNodeList.Clear;
Var N: PNodeRec;
begin
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  With FRoot^ do
  begin
   Node.Free;
   Node := NIL;
  end;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

Procedure TNodeList.Remove(Item: PNodeRec);
Var P, PR: PNodeRec;
begin
 P := FRoot; PR := NIL;
 While P <> NIL do
 begin
  If P = Item then Break;
  PR := P;
  P := P^.Next;
 end;
 If P = NIL then Exit;
 With P^ do
 begin
  If FRoot = P then
  begin
   If FRoot = FCur then FCur := Next;
   FRoot := Next;
  end Else If Next = NIL then
  begin
   PR^.Next := NIL;
   FCur := PR;
  end Else PR^.Next := P^.Next;
  With P^ do 
  begin
   Node.Free;
   Node := NIL;
  end;
  Dispose(P);
  Dec(FCount);
 end;
end;

Procedure TNodeList.Exchange(Index1, Index2: Integer);
Var N1, N2: PNodeRec; Node: TNode;
begin
 N1 := Get(Index1);
 N2 := Get(Index2);
 If (N1 <> NIL) and (N2 <> NIL) then
 begin
  Node := N1^.Node;
  N1^.Node := N2^.Node;
  N2^.Node := Node;
 end;
end;

Procedure TNodeList.Move(CurIndex, NewIndex: Integer);
Var C1, C2, N1, NN: PNodeRec; N: TNode;
begin
 If (CurIndex = NewIndex) or (FCount < 2) or (NewIndex >= FCount) then Exit;
 C1 := Get(CurIndex);
 If C1 = NIL then Exit;
 N := C1^.Node;
 C1^.Node := NIL;
 Remove(C1);
 If NewIndex > 0 then
 begin
  N1 := Get(NewIndex - 1);
  If N1 = NIL then Exit;  
  C1 := N1;
  C2 := C1^.Next;
  New(NN);
  NN^.Node := N;
  NN^.Next := C2;
  If C2 = NIL then FCur := NN;
  C1^.Next := NN;
  Inc(FCount);
 end Else If NewIndex = 0 then
 begin
  New(NN);
  NN^.Node := N;
  NN^.Next := FRoot;
  FRoot := NN;
  Inc(FCount);
 end;
end;

Procedure TNodeListEx.Clear;
Var N: PNodeRec;
begin
 While FRoot <> NIL do
 begin
  N := FRoot^.Next;
  Dispose(FRoot);
  FRoot := N;
 end;
 FCur := NIL;
 FCount := 0;
end;

end.
